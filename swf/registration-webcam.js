function onImageUpload() {

}

function onImageError() {
	
}

function onSizeError() {
	
}

function onUploadStart() {
	
}

function onUploadComplete() {

}

function webcamProblem() {
	popup.open({
		title : "Sorry",
		align : "center",
		message : 'Webcam is not detected'
	});
}

function onSaveBA(image) {
	$("#captured-image-ba").val(image);
	$(".upload-preview").html("");
	$("#giid").val("");
	$("#giid").siblings('.browse').children().val("");
	$("#captured-alert").removeClass("hide");
	hasUploadedFile = false;
}

function onRedirect() {
	// do something
}

function enableUpload() {
	
}

function getFlash(id) {
	if (window.document[id]) {
		return window.document[id];
	}
	if (navigator.appName.indexOf("Microsoft Internet") == -1) {
		if (document.embeds && document.embeds[id]) {
			return document.embeds[id];
		}
	} else {
		return document.getElementById(id);
	}
	return null;
}

function initFlash() {
	console.log( BASE_URL );
	
	var minimum_flashversion = "10";
	var so = new SWFObject(BASE_URL + "swf/app.swf?debug=true&forceWebcam=true", "flash", "100%", "100%", minimum_flashversion, "#ffffff");
	so.addParam("quality", "high");
	so.addParam("wmode", "transparent");
	so.addParam("salign", "tl");
	so.addParam("scaleMode", "scale");
	so.addParam("allowscriptaccess","always");
	so.useExpressInstall(BASE_URL + "swf/expressInstall.swf");
	so.write("flashcontent");
	
	var pat = /expressInstall/;
	var client_flash_version = deconcept.SWFObjectUtil.getPlayerVersion();

	if(client_flash_version.major < minimum_flashversion ){
		jQuery('.preloader').hide();
		jQuery("#flashcontent").html('<div style="padding:250px 0 0; color:#fff; text-align:center;"><strong>Please download the latest flash player</strong><br /><a href="http://www.adobe.com/go/getflashplayer" target="_top"><img src="https://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></div>');
		return false;
	}
	
	/*
	if(!pat.test(jQuery('#flashcontent embed, object').attr("src"))) {
		if(jQuery("#flashcontent").html() == "") {
			jQuery('.preloader').hide();
			jQuery("#flashcontent").html('<div style="padding:250px 0 0; text-align:center;"><strong>Please download the latest flash player</strong><br /><a href="http://www.adobe.com/go/getflashplayer" target="_top"><img src="https://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></div>');
		}
	}*/
}
