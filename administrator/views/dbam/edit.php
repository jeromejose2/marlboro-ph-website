<!--start main content -->
<?php $this->load->view('editor.php') ?>
 <div class="container main-content">
			<div class="page-header">
					 <h3>Edit Dont Be A Maybe</h3>
		   <div class="actions">
			  <a href="<?= SITE_URL ?>/dbam" class="btn btn-primary">Back</a>
		   </div>
			</div>
			<?php if (!$error): ?>
			<div class="alert alert-danger" style="display:none;"></div>
			<?php else: ?>
			<div class="alert alert-danger"><?= $error ?></div>
			<?php endif ?>
			 <form class="form-horizontal" role="form" method="post" action="<?= SITE_URL ?>/dbam/edit?id=<?= $this->input->get('id') ?>" onsubmit="return submitFrm()" enctype="multipart/form-data">
					 <div class="form-group">
								<label class="col-md-1 control-label">Title</label>
								<div class="col-md-4" id="title-input">
									<input type="text" id="title" name="title" value="<?= $record->dbam_title ?>" data-name="title" class="required form-control">									 
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Media</label>
								<div class="col-md-4">
									<input type="file" id="dbam-content" data-name="media" name="dbam_upload" value="" class="form-control">
									<br>
                     				<strong>Width: 1160px. Height: 600px.</strong>
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Media Preview</label>
								<div class="col-md-4" id="dbam-content-preview">
									<?php if ($record->dbam_type == 1): ?>
									<img src="<?= BASE_URL ?>uploads/dbam/576_296_<?= $record->dbam_content ?>" class="img-thumbnail">
									<?php elseif ($record->dbam_type == 2): ?>
									<video width="576" height="296" controls>
										<source src="<?= BASE_URL ?>uploads/dbam/<?= $record->dbam_content ?>" type="video/mp4">
									Your browser does not support the video tag.
									</video>
									<?php endif ?>
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Thumbnail</label>
								<div class="col-md-4">
									<input type="file" id="dbam-video-thumbnail" data-name="media" name="dbam_thumbnail" value="" class="form-control">
									<br>
                     				<strong>Width: 300px. Height: 300px.</strong>
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Thumbnail Preview</label>
								<div class="col-md-4" id="dbam-thumbnail-preview">
									<?php if ($record->dbam_thumbnail): ?>
									<img src="<?= BASE_URL ?>uploads/dbam/150_150_<?= $record->dbam_thumbnail ?>" class="img-thumbnail">
									<?php endif ?>
								</div>
					 </div>
					 <div class="form-group">
					 	<div class="col-md-offset-1 col-md-4">
					 		<button type="button" id="remove-thumb" class="btn btn-small">Remove</button>
					 	</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Status</label>
								<div class="col-md-4">
										<select class="form-control" name="status">
											<option <?= (string) $record->dbam_status == '0' ? 'selected="selected"' : '' ?> value="0">Unpublished</option>
											<option <?= (string) $record->dbam_status == '1' ? 'selected="selected"' : '' ?> value="1">Published</option>   
										</select>
								</div>
					 </div>
					 <div class="form-group">
						<div class="col-md-offset-1 col-md-4">
						 <button type="submit" class="btn btn-primary">Submit</button>
					 </div>
					</div>
					<input type="hidden" name="remove_thumb" id="dbam-thumbnail-remove">
					<input type="hidden" name="back_url" value="<?= $this->input->server('HTTP_REFERER') ?>">
			</form>
			
 </div>

 <script>
 	$("#dbam-content").change( function() {
 		var file = $(this).prop("files")[0];
 		if (file.type.indexOf("image") == 0) {
 			var oFReader = new FileReader();
			oFReader.onload = function (oFREvent) {
				$("#dbam-content-preview").html("<img style='width: 576px; height: 296px;' class='img-thumbnail' src='" + oFREvent.target.result + "'>");
			};
			oFReader.readAsDataURL(file);
		}
 	});

 	$("#remove-thumb").click( function () {
 		$("#dbam-thumbnail-preview").html("");
 		$("#dbam-thumbnail-remove").val("1");
 	});

 	$("#dbam-video-thumbnail").change( function() {
 		var file = $(this).prop("files")[0];
 		if (file.type.indexOf("image") == 0) {
 			var oFReader = new FileReader();
			oFReader.onload = function (oFREvent) {
				$("#dbam-thumbnail-preview").html("<img style='width: 150px; height: 150px;' class='img-thumbnail' src='" + oFREvent.target.result + "'>");
			};
			oFReader.readAsDataURL(file);
		}
 	});
 </script>