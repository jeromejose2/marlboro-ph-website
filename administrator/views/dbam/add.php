<script src="<?= BASE_URL ?>admin_assets/js/angular-js/angular-file-upload-shim.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.js"></script>		 
<script src="<?= BASE_URL ?>admin_assets/js/angular-js/angular-file-upload.js"></script>
<script type="text/javascript">
var uploadURL = '<?= SITE_URL ?>/dbam/upload/';
</script>
<script src="<?= BASE_URL ?>admin_assets/js/angular-js/upload-dbam.js"></script> 
<!--start main content -->
<?php $this->load->view('editor.php') ?>
 <div class="container main-content" ng-app="fileUpload" ng-controller="MyCtrl">
			<div class="page-header">
					 <h3>Add Dont Be A Maybe</h3>
		   <div class="actions">
			  <a href="<?= SITE_URL ?>/dbam" class="btn btn-primary">Back</a>
		   </div>
			</div>
			<?php if (!$error): ?>
			<div class="alert alert-danger" style="display:none;"></div>
			<?php else: ?>
			<div class="alert alert-danger"><?= $error ?></div>
			<?php endif ?>
			 <form class="form-horizontal" role="form" id="db
			 am-form" method="post" action="<?= SITE_URL ?>/dbam/add" onsubmit="return submitFrm()" enctype="multipart/form-data">
					 <!-- <div class="form-group">
								<label class="col-md-1 control-label">Title</label>
								<div class="col-md-4">
										 <input type="text" name="title" value="" id="title" data-name="title" class="required form-control">
								</div>
					 </div> -->
					 <div class="form-group">
								<label class="col-md-1 control-label">Media</label>
								<div class="col-md-4">
									<button type="button" class="btn btn-warning" id="dbam-browse-media">Browse</button>
									<br><br>
                     				<strong>Width: 1160px. Height: 600px.</strong>
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Media Preview</label>
								<div class="col-md-8">
									<div class="row">
										<div class="col-xs-12" ng-repeat="(fkey, fval) in selectedFiles">

										  	<div class="">
										  		 <div class="col-xs-12" style="margin-bottom:20px;">
											  		 <div class="thumbnail col-sm-3 col-xs-4">
													      <img ng-show="dataUrls[$index]" ng-src="{{dataUrls[$index]}}">
													      <div class="caption">
													        <h5>{{f.name}}</h5>
													        <input name="dbam_title[{{$index}}]" type="text" ng-class="{'dbam-title' : !uploadError[$index], 'hide' : uploadError[$index]}" style="width: 100%;" placeholder="Enter title here">
													        <div class="alert alert-danger" ng-show="uploadError[$index]">{{uploadError[$index]}}</div>
													        <input name="dbam_upload[{{$index}}]" type="hidden" ng-show="uploadResult[$index]" value="{{uploadResult[$index]}}">
													        <p>size: {{f.size}}B - type: {{f.type}}</p>
													        <div class="progress progress-striped" ng-class="{ active : !uploadResult[$index] }" ng-show="progress[$index] >= 0">
																  <div class="progress-bar"  role="progressbar" aria-valuemin="0" aria-valuenow="{{progress[$index]}}" aria-valuemax="100" style="width: {{progress[$index]}}%">
																    <span class="sr-only">{{progress[$index]}}% Complete</span>
																  </div>
															</div>
													        <p><a href="javascript:void(0)" class="btn btn-primary" role="button" ng-click="start($index)" ng-show="progress[$index] < 0">Start</a> 
													        <a href="javascript:void(0)" class="btn btn-warning" ng-click="abort($index)" ng-show="hasUploader($index) && !uploadResult[$index]" role="button">Abort</a> 
													    	<a href="javascript:void(0)" class="btn btn-danger" ng-click="remove($index)" ng-show="uploadResult[$index]" role="button">&times;&nbsp;&nbsp;Remove</a></p>
													      </div>
													  </div>
												 </div>

											      <div class="caption col-xs-3" ng-repeat="i in thumbnails[$index]" style="margin-bottom:20px;">
											       	 <center>
											       	 	<img ng-src="<?php echo BASE_URL ?>uploads/dbam/{{i}}"> <br>	
											       	 	<input type="radio" id="id{{fkey}}" name="thumbnail[{{fkey}}]" value="{{i}}" />
											       	 </center>
											      </div>

											  </div>
											 
								  		</div>
									</div>

								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Status</label>
								<div class="col-md-4">
										<select class="form-control" name="status">
											<option value="0">Unpublished</option>
											<option value="1">Published</option>   
										</select>
								</div>
					 </div>
					 <div class="form-group">
						<div class="col-md-offset-1 col-md-4">
						 <button type="submit" class="btn btn-primary">Submit</button>
					 </div>
					</div>
					<!-- <input type="hidden" id="remove-files-index" name="remove-file" value=""> -->
					<input type="hidden" ng-model="httpMethod" ng-init="httpMethod = 'POST'" value="POST"/>
					<input type="hidden" name="howToSend" ng-model="howToSend" value="1" ng-init="howToSend = 1">
			</form>
			<input type="file" id="dbam-media" data-name="media" ng-file-select="onFileSelect($files)" name="dbam_upload" value="" class="required form-control hide" multiple="true">
 </div>

 <script>
 	
 	$("#dbam-browse-media").click( function () {
		$("#dbam-media").click();
	});

 	$("#dbam-form").submit( function() {
 		valid = true;
 		$(".dbam-title").each( function(i, item) {
 			item = $(item);
 			if (!item.val()) {
 				bootbox.alert("Title is required");
 				item.focus();
 				valid = false;
 				return false;
 			}
 		});
 		if (!valid) {
 			return false;
 		}
 	});

 </script>