<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3>Module</h3>
      </div>
       <div class="alert alert-danger" <?php if(!$error) echo 'style="display:none;"' ?>><?php echo $error; ?></div>
       <form class="form-horizontal" role="form" method="post" onsubmit="return submitFrm()">
           <div class="form-group">
                <label class="col-sm-2 control-label">Module Name</label>
                <div class="col-sm-4">
                     <input type="text" name="module_name" value="<?php echo isset($module['module_name']) ? $module['module_name'] : '' ?>" data-name="module name" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Group</label>
                <div class="col-sm-4">
                    <select class="required form-control" name="group">
                    	<option value="1" <?php echo isset($module['group']) && $module['group'] == USER_MANAGEMENT ? 'selected="selected"' : '' ?>>User Management</option>
                        <option value="2" <?php echo isset($module['group']) && $module['group'] == CONTENT_MANAGEMENT ? 'selected="selected"' : '' ?>>Content Management</option>
                        <option value="3" <?php echo isset($module['group']) && $module['group'] == MODERATOR ? 'selected="selected"' : '' ?>>Moderator</option>
                        <option value="4" <?php echo isset($module['group']) && $module['group'] == REPORTS ? 'selected="selected"' : '' ?>>Report</option>
                    </select>
                </div>
           </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">URI</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo isset($module['uri']) ? $module['uri'] : ''?>" name="uri" data-name="module uri" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <input type="hidden" name="submit" value="1" />
                     <input type="hidden" name="id" value="<?php echo isset($module['module_id']) ? $module['module_id'] : '' ?>" />
                     <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                </div>
           </div>
      </form>
      
 </div>
 <!--end main content -->
 