<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Points Accumulation</h3>
                <div class="actions">
                      <a href="<?php echo SITE_URL ?>/points/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>   
                 </div>   
		  </div>

          <!-- Nav tabs --> 
          <ul class="nav nav-tabs">
            <li class="active"><a href="#graph" data-toggle="tab">Graph</a></li>
            <li class=""><a href="#summary" data-toggle="tab">Summary</a></li>
            <li class=""><a href="#stats" data-toggle="tab">Stats</a></li>
          </ul>  

          <!-- Tab panes -->
          <div class="tab-content">
               <div class="tab-pane" id="summary">
                    <table class="table table-bordered" style="border-top:none;">
                         <thead>
                              <tr>
                                   <th width="1%">#</th>
                                   <th>Week</th>
                                   <th>Duration</th>
                                   <th>Points</th>
                                   <td>Operation</td>
                              </tr>
                         </thead>
                         <tbody>
                              <form>
                                   <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                          From:<input name="fromdate" class="form-control from" value="<?php echo isset($_GET['fromdate']) ? $_GET['fromdate'] : '' ?>" /><br> 
                                          To:<input name="todate" class="form-control to" value="<?php echo isset($_GET['todate']) ? $_GET['todate'] : '' ?>" /><br>
                                          Field: 
                                          <select name="field" class="form-control">
                                              <option></option>
                                              <option value="week" <?php echo $this->input->get('field') && $this->input->get('field') == 'week' ? 'selected' :'' ?>>Week</option>
                                              <option value="total_points" <?php echo $this->input->get('field') && $this->input->get('field') == 'total_points' ? 'selected' :'' ?>>Points</option>
                                          </select>
                                        Sort: 
                                            <select name="sort" class="form-control">
                                              <option></option>
                                              <option value="asc" <?php echo $this->input->get('sort') && $this->input->get('sort') == 'asc' ? 'selected' :'' ?>>Ascending</option>
                                              <option value="desc" <?php echo $this->input->get('sort') && $this->input->get('sort') == 'desc' ? 'selected' :'' ?>>Descending</option>
                                          </select><br><input name="type" type="hidden" value="<?php echo $this->input->get('type') ? $this->input->get('type') : 'age' ?>" /><br><button class="btn btn-primary" name="search" value="1">Go</button></td>
                                   </tr>
                              </form>
                         		<?php if($records): 
                            $total = 0;
                    foreach($records as $k => $v): $total += $v['total_points'];  
                      $week_start = $k == 0 ? date('F d, Y', strtotime($this->input->get('fromdate'))) : $v['week_start'];
                    ?>
                              <tr>
                                   <td><?php echo $k + 1 ?></td>
                                   <td> Week <?php echo $v['week_no'] ?></td>
                                   <td> <?php echo $week_start . ' - ' . $v['week_end'] ?></td>
                                   <td><a href="<?php echo BASE_URL ?>admin/user_points?name=&from_date_earned=<?php echo date('Y-m-d', strtotime($week_start)) ?>&to_date_earned=<?php echo date('Y-m-d', strtotime($v['week_end'])) ?>"><?php echo $v['total_points'] ?></td>
                                   <td></td>
                              </tr>
                              <?php 
          					endforeach; ?>
                               <tr>
                                  <td></td>
                                  <td colspan="2"><strong>Total</strong></td>
                                  <td><strong><?php echo $total ?></strong></td>
                                  <td></td>
                             </tr>
                              <?php else:
          						echo '<tr><td colspan="7">No records found</td></tr>';
          					endif; ?>
                         </tbody>
                    </table>
               </div>
               <div class="tab-pane active" id="graph">
                    <div id="chart_entries" style="border:1px solid #DDDDDD; border-top:none;height:300px;">
                         
                    </div>
                    <br >
                 </div>
              <div class="tab-pane" id="stats">
                    <div style="padding:20px;border:1px solid #DDDDDD; border-top:none;">
                      <table cellpadding="10">
                        <tr>
                          <td>Mean Points:</td>
                          <td><strong><?php echo ($stats['mean']) ? $stats['mean'] : 0  ?></strong></td>
                        </tr>
                        <tr>
                          <td>Median Points:</td>
                          <td><strong><?php echo ($stats['median']) ? $stats['median'] : 0;  ?></strong></td>
                        </tr>
                      <tr>
                          <td>Max Points:</td>
                          <td><strong><?php echo ($stats['max']) ?></strong></td>
                        </tr>
                      </table>
                    </div>
                    <br >
                 </div>
           </div>
           
     </div>
     <!--end main content -->

     <script type="text/javascript" src="<?php echo BASE_URL ?>admin_assets/js/google-api.js"></script>
     <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Week', 'Points'],
          <?php foreach($records as $k => $v): $week_start = $k == 0 ? date('F d, Y', strtotime($this->input->get('fromdate'))) : $v['week_start']; ?>
          ['Week <?php echo $v["week_no"] ?> (<?php echo $week_start ?> - <?php echo $v["week_end"] ?>)',  <?php echo $v['total_points'] ?>],
          <?php endforeach; ?>
        ]);

        var options = {
          title: 'Points Accumulation <?php echo $this->input->get("fromdate") && $this->input->get("todate") ? "(" . date("F d, Y", strtotime($this->input->get("fromdate"))) . " - " .  date("F d, Y", strtotime($this->input->get("todate"))) . ")" : ""?>'
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_entries'));
        chart.draw(data, options);
      }
    </script>

  
