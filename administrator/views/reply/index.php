<?php 
  $controller = $this->uri->segment(1);
  $label = 'Comment Replies';
?>
<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3><?php echo $label ?> <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>
               <div class="actions">
                <button id="approve-all" class="btn btn-success">Approve</button>
                <button type="button" onclick="return multiple_disapproval()" class="btn btn-warning" target="_blank">Reject</button>
                <a href="<?php echo SITE_URL ?>/reply/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>       
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th><input type="checkbox" id="check-all" /></th>
                         <th>#</th>
                         <th>Name</th>
                         <th>Comment</th>
                         <th>Comment Photo</th>
                         <th>Item Title</th>
                         <th>URL</th>
                         <th>Status</th>
                         <th>Date</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
                    <form action="<?php echo SITE_URL ?>/reply">
                     <tr>
                         <td></td>
                         <td></td>
                         <td><input name="first_name" class="form-control" value="<?php echo isset($_GET['first_name']) ? $_GET['first_name'] : '' ?>" /></td>
                         <td><input name="comment_reply" class="form-control" value="<?php echo isset($_GET['comment_reply']) ? $_GET['comment_reply'] : '' ?>" /></td>
                         <td></td>
                         <td><input name="title" class="form-control" value="<?php echo isset($_GET['title']) ? $_GET['title'] : '' ?>" /></td>
                         <td></td>
                         <td>
                          <select name="comment_reply_status" class="form-control">
                            <option></option>
                            <option value="0" <?php echo $this->input->get('comment_reply_status') != '' && $this->input->get('comment_reply_status') == '0' ? 'selected' : '' ?>>Pending</option>
                            <option value="1" <?php echo $this->input->get('comment_reply_status') != '' && $this->input->get('comment_reply_status') == '1' ? 'selected' : '' ?>>Approved</option>
                            <option value="2" <?php echo $this->input->get('comment_reply_status') != '' && $this->input->get('comment_reply_status') == '2' ? 'selected' : '' ?>>Rejected</option>
                          </select>
                        </td>
                         
                         <td>
                          <div class="row">
                            <div class="col-md-6 col-lg-4">
                                 <input name="fromavail" placeholder="From" class="form-control from" value="<?php echo isset($_GET['fromavail']) ? $_GET['fromavail'] : '' ?>" />
                            </div>
                            <div class="col-md-6 col-lg-4">
                                 <input name="toavail" placeholder="To" class="form-control to" value="<?php echo isset($_GET['toavail']) ? $_GET['toavail'] : '' ?>" />
                            </div>
                          </div>
                        </td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    <!-- <input type="hidden" value="<?php echo @$_GET['comment_id'] ?>" name="comment_id" /> -->
                    </form>
               		  <?php if($categories): 
					foreach($categories as $k => $v): ?>
                    <tr>
                         <td><input type="checkbox" name="checkbox[]" value="<?php echo $v['comment_reply_id'] ?>" class="comment-ids" /></td>                          
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><a href="#" onclick="showRegistrantDetails(<?php echo $v['registrant_id'] ?>)"><?php echo $v['first_name'] . ' ' . $v['third_name'] ?></a></td>
                         <td><?php echo htmlentities($v['comment_reply']) ?></td>
                         <td>
                          <?php if($v['comment_reply_image']): ?>
                          <a class="modal-pop" href="#" data-title="Comment Photo"><img src="<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/comments/150_150_<?php echo $v['comment_reply_image'] ?>" /></a>
                          <?php else:
                            echo '';
                          endif; ?>
                          <div class="modal">
                                <div style="text-align:center">  
                                    
                                    <img src="<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/comments/<?php echo $v['comment_reply_image'] ?>" width="500" />  
                                    
                                </div>
                            </div>

                         </td>
                         <td>
                           <?php echo $v['item_title'] ?>
                         </td>
                         <td>
                           <a href="<?php echo $v['url'] ?>"><?php echo $v['url'] ?></a>
                         </td>
                         <td>
                           <?php echo $status[$v['comment_reply_status']] ?>
                         </td>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['comment_reply_date_created'])) ?></td>
                         <td>
                            <?php if($v['comment_reply_status'] == 0 && $edit): ?>
                              <a href="<?php echo SITE_URL ?>/reply/approve/<?php echo $v['comment_reply_id'] ?>/<?php echo md5($v['comment_reply_id'] . ' ' . $this->config->item('encryption_key')) ?>" class="btn btn-success" onclick="return confirmAction(this, 'approve')">Approve</a>
                              <a href="#" class="btn btn-warning" onclick="return rejectEntry(<?php echo $v['comment_reply_id'] ?>, '<?php echo md5($v['comment_reply_id'] . ' ' . $this->config->item('encryption_key')) ?>')">Reject</a>
                            <?php endif; ?>
                            <?php #if($v['comment_reply_status'] != 1 && $edit): ?>
                            <!-- <a href="<?php echo SITE_URL ?>/reply/approve/<?php echo $v['comment_reply_id'] ?>/<?php echo md5($v['comment_reply_id'] . ' ' . $this->config->item('encryption_key')) ?>" class="btn btn-success" onclick="return confirmAction(this, 'approve')">Approve</a> -->
                            <?php #endif; ?>
                            <?php #if($v['comment_reply_status'] != 2 && $edit): ?>
                            <!-- <a href="#" class="btn btn-warning" onclick="return rejectEntry(<?php echo $v['comment_reply_id'] ?>, '<?php echo md5($v['comment_reply_id'] . ' ' . $this->config->item('encryption_key')) ?>')">Reject</a> -->
                            <?php #endif; ?> 
                            <a class="btn btn-primary" href="<?php echo SITE_URL ?>/comment?id=<?php echo $v['comment_id'] ?>&first_name=&comment=&origin_id=&comment_status=&fromavail=&toavail=&title=&search=1">Back to Comment</a>
                    </tr>
                    <?php 
					endforeach;
          else:
            echo '<tr><td colspan="7">No records found</td></tr>';
					endif; ?> 
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>
          
          <div class="modal" id="reason">
            <div style="text-align:center">
              <form id="reason-form" method="post">
                    <textarea name="message" class="form-control" rows="5"></textarea>
                    <br>
                    <div style="float:right">
                        <button type="button" class="btn close-bootbox" name="search">Cancel</button>
                        <button class="btn btn-primary" name="search" value="1">Submit</button>
                    </div>
                    <br style="clear:both;">
              </form>
            </div>
          </div>

          

     </div>
     <!--end main content -->

     <script>

     function multiple_disapproval(){
 
        if($('input[name="checkbox[]"]').is(':checked')){

          var form = document.getElementById('reason-form');
            
          $.each($('input[name="checkbox[]"]'),function(i,val){
            
            if($(this).is(':checked')){

              var input = document.createElement('input');
              input.type = 'hidden';
              input.name = 'ids[]';
              input.value = val.value;
              form.appendChild(input);

            }

          });
          
          $("#reason form").prop("action", "<?=SITE_URL?>/reply/multiple_disapprove");
          bootbox.dialog({'message': $('#reason').html(), 'title': 'Reason for disapproval'});
           
        }else{

          bootbox.alert('Please select entries to disapprove.');
          return false;

        }       

     }

      function rejectEntry(id, token) {
      $('#reason').find('form').attr('action', '<?php echo SITE_URL ?>/reply/disapprove/' + id + '/' + token); 
      bootbox.dialog({'message': $('#reason').html(),
              'title': 'Reason for Rejection'});
      return false;
     }

     $('#check-all').click(function() {
        if($(this).is(':checked'))
          $('input[type="checkbox"]').prop('checked', true)
        else
          $('input[type="checkbox"]').prop('checked', false)
     });

     $('#approve-all').click(function() {
      ids = [];
      $('.comment-ids').each(function() {
        if($(this).is(':checked')) {
          ids.push($(this).val());
        }
      });
      if(ids.length < 1) {
        bootbox.alert('Sorry, you must select at least 1 entry');  
      } else {
          bootbox.confirm('Are you sure you want approve all selected comments?', function(result) {
            if(result == 1) {
              $.post('<?php echo SITE_URL ?>/reply/approve_all', {ids: ids}, function() {
                window.location.reload();
              })
              return true;
            }
          });

      }
     });

     <?php if($this->session->flashdata('error')): ?>
      setTimeout(function(){
        bootbox.alert("<?php echo $this->session->flashdata('error') ?>");
      }, 1000);
     <?php endif; ?>
     </script>