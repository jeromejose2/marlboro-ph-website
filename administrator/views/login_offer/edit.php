<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3>Edit Offer</h3>
      </div>

      <?php
        if(@$error){
        ?>
        <div class="alert alert-danger">
          <ul>
            <?php for($i=0; $i<count($error); $i++) { ?>
              <li><?= $error[$i]; ?></li>
            <?php } ?>
          </ul>
        </div>
        <?php
        }else if(@$success){
        ?>
        <div class='alert alert-success'>
          <?= $success ?>
        </div>
        <?php
        }
      ?>

       <form class="form-horizontal" action='' role="form" method="post" enctype="multipart/form-data">
        <div class="form-group">
         <label class='col-sm-2 control-label'>Title:</label>
         <p class='col-sm-4'>
           <input type='text' class='form-control' name='title' value="<?= isset($_POST['title']) ? $_POST['title'] : $offer->title ?>" />
         </p>
        </div>
        <div class="form-group">
           <label class='col-sm-2 control-label'>Content:</label>
           <p class='col-sm-4'>
             <textarea class='form-control' name='content' id="description"><?= isset($_POST['content']) ? $_POST['content'] : $offer->content ?></textarea>
           </p>
         </div>
        <div class="form-group">
         <label class='col-sm-2 control-label'>Points:</label>
         <p class='col-sm-4'>
           <input type='text' class='form-control' name='points' value="<?= isset($_POST['points']) ? $_POST['points'] : $offer->points ?>" />
         </p>
        </div>
        <div class="form-group">
         <label class='col-sm-2 control-label'>Start Date:</label>
         <p class='col-sm-4'>
           <input type='text' class='form-control from required' name='start_date' value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $offer->start_date ?>" />
         </p>
        </div>
        <div class="form-group">
         <label class='col-sm-2 control-label'>End Date:</label>
         <p class='col-sm-4'>
           <input type='text' class='form-control to required' name='end_date' value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : $offer->end_date ?>" />
         </p>
        </div>
        <div class="form-group">
         <label class='col-sm-2 control-label'>Required Days of Inactivity:</label>
         <p class='col-sm-4'>
           <input type='text' class='form-control' name='days_inactive' value="<?= isset($_POST['days_inactive']) ? $_POST['days_inactive'] : $offer->days_inactive ?>" />
         </p>
        </div>
        <div class="form-group">
         <label class='col-sm-2 control-label'></label>
         <p class='col-sm-4'>
          <input type='submit' name='submit' value='Save' class='btn btn-primary pull-right' style="margin-left: 10px;" /> 
          <a href="<?= site_url() ?>/login_offer" class='btn btn-default pull-right'>Back</a>
         </p>
        </div>
      </form>
      
 </div>
 <!--end main content -->
 
<script src="<?php echo base_url(); ?>/admin_assets/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
  tinymce.init({
    selector: "textarea",
    height: 300,
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste "
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | unlink link image"
  });
</script>