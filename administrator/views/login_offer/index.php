     <div class="container main-content">
          <div class="page-header">
               <h3>Login Offer <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <?php //if($add): ?>
                    <a href="<?php echo SITE_URL ?>/login_offer/add" class="btn btn-primary">Add Offer</a>
                    <!-- <a href="<?php echo SITE_URL ?>/export/login_offer/?<?=@http_build_query($this->input->get())?>" class="btn btn-primary">Export</a> -->
                    <?php //endif; ?>
               </div>
               <br />
          </div>

        <?php if(@$success){
        ?>
        <div class='alert alert-success'>
          <?= $success ?>
        </div>
        <?php
        }
        ?>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Title</th>
                         <th>Content</th>
                         <th>Points</th>
                         <th>Start Date</th>
                         <th>End Date</th>
                         <th>Days Inactive</th>
                         <th>Status</th>
                         <th>Date Created</th>
                         <th>Action</th>
                    </tr>
               </thead>
               <tbody>
                    <form method='GET'>
                      <tr>
                        <td></td>  
                        <td><input type="text" name="title" class="form-control" value="<?= (@isset($_GET['title'])) ? $_GET['title'] : ''; ?>" /></td>  
                        <td></td>  
                        <td></td>  
                        <td><input type='text' name='start_date' class="form-control from" value="<?= (@isset($_GET['start_date'])) ? $_GET['start_date'] : ''; ?>" /></td>  
                        <td><input type='text' name='end_date' class="form-control to" value="<?= (@isset($_GET['end_date'])) ? $_GET['end_date'] : ''; ?>" /></td>  
                        <td></td>  
                        <td>
                          <select name='status' class="form-control">
                            <option></option>
                            <option value="1"<?= (@isset($_GET['status']) and $_GET['status']==1) ? 'selected="selected"' : ''; ?> >Active</option>
                            <option value="2"<?= (@isset($_GET['status']) and $_GET['status']==2) ? 'selected="selected"' : ''; ?>>Inactive</option>
                          </select>
                        </td>  
                        <td></td>
                        <td>
                          <input type='submit' value="Go" class="btn btn-primary" />
                        </td>
                      </tr>
                    </form>
               		 <form action="<?php echo SITE_URL ?>/login_offer">
                    <?php $ctr=0; foreach($content as $offer) { ?>
                      <tr>
                        <td><?= ++$ctr; ?></td>
                        <td><?= $offer->title ?></td>
                        <td><?= substr(strip_tags($offer->title), 0, 200) ?></td>
                        <td><?= $offer->points ?></td>
                        <td><?= $offer->start_date ?></td>
                        <td><?= $offer->end_date ?></td>
                        <td><?= $offer->days_inactive ?></td>
                        <td>
                          <?php
                            if($offer->is_active==1){
                              echo "<span style='background: green; color: white'>Active</span>";
                            }else{
                              echo "<span style='background: orange; color: white'>Inactive</span>";
                            }
                          ?>
                        </td>
                        <td><?= $offer->date_created ?></td>
                        <td>
                          <!-- 1:Active - 2:InActive - 0:Deleted -->
                          <?php if($offer->is_active==2) { ?>
                            <a href="<?= site_url() ?>/login_offer/activate/<?= $offer->id ?>" class='btn btn-success'>Activate</a>
                          <?php }else{ ?>
                            <a href="<?= site_url() ?>/login_offer/deactivate/<?= $offer->id ?>" class='btn btn-warning'>Deactive</a>
                          <?php } ?>
                          <a href="<?= site_url() ?>/login_offer/edit/<?= $offer->id ?>" class='btn btn-primary'>Edit</a>
                          <a href="<?= site_url() ?>/login_offer/delete/<?= $offer->id ?>" class='btn btn-danger' onclick="var a = confirm('Are you sure you want to continue?'); if(a) return true; else return false;">Delete</a>
                        </td>
                      </tr>
                    <?php } ?>
                    </form>
               </tbody>
          </table>
          <ul class="pagination pagination-sm pull-right">
          </ul>

     </div>
     <!--end main content 