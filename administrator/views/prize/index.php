<?php 
  $controller = $this->uri->segment(1);
  $label = '';
  switch($controller) {
    case 'birthday_offer':
      $label = 'Birthday Offer';
      break;
    case 'flash_offer':
      $label = 'Flash Offer';
      break;
    default:
      $label = 'Prize';
      break;

  }
?>


<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3><?php echo $label ?>s <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/add" class="btn btn-primary">Add <?php echo $label ?></a>
                    <?php endif; ?>
                    <a href="<?php echo SITE_URL ?>/export/prizes?<?php echo @http_build_query($this->input->get()) ?>" class="btn btn-primary">Export</a>
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Thumbnail</th>
                         <!-- <th>Stock</th> -->
                         <th>Status</th>
                         <th>Origin</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/<?php echo $controller ?>">
                     <tr>
                         <td></td>
                         <td><input name="prize_name" class="form-control" value="<?php echo isset($_GET['prize_name']) ? $_GET['prize_name'] : '' ?>" /></td>
                         <td></td>
                         <!-- <td></td> -->
                         <td></td>
                         <td></td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
                    <?php if($categories): 
					foreach($categories as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['prize_name'] ?></td>
                         <td><img src="<?php echo $v['prize_image'] ? BASE_URL . 'uploads/prize/' . $v['prize_image'] : BASE_URL . 'images/no_image.jpg' ?>?uniqid=<?php echo uniqid() ?>" width="150" /></td>
                         <!-- <td><?php echo $v['stock'] ?></td> -->
                         <td><?php echo $v['status'] == 1 ? 'Published' : 'Unpublished' ?></td>
                         <td><?php echo @$origins[$v['origin_id']]; ?></td>
                         <td>
                            <?php if($edit): ?>
                            <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/edit/<?php echo $v['prize_id'] ?>" class="btn btn-primary">Edit</a>
                            <?php endif; ?>
                            <?php if($delete): ?>
                            <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/delete/<?php echo $v['prize_id'] ?>/<?php echo md5($v['prize_id'] . ' ' . $this->config->item('encryption_key')) ?>" onclick="return confirmDeletion(this, '<?php echo str_replace('_', ' ', $controller) ?>')" class="btn btn-danger">Delete</a>
                            <?php endif; ?>
                         </td>
                    </tr>
                    <?php 
					endforeach;
          else:
            echo '<tr><td colspan="7">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->