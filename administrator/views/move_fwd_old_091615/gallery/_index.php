<?php 
	$controller = $this->uri->segment(1);
	$label = 'MoveFWD Gallery';
?>
<!--start main content -->
	 <div class="container main-content">
			<div class="page-header">
				 <h3><?php echo $label ?> <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>
				 <div class="actions">
				 	<button id="approve-all" class="btn btn-success">Approve</button>				 	
                	<button type="button" onclick="return multiple_disapproval()" class="btn btn-warning" target="_blank">Reject</button>
                	<button id="exempt-all" class="btn btn-default">Exempted</button>
					<a href="<?php echo SITE_URL ?>/export/move_fwd_gallery?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>   	
				 </div>
			</div>
			
			<table class="table table-bordered">
				 <thead>
					<tr>
						 <th><input type="checkbox" id="check-all" /></th>
						 <th>Name</th>
						 <th>Entry</th>
						 <th>Task</th>
						 <th>Activity</th>
						 <th>Status</th>
						 <th>Source</th>
						 <th>Date Submitted</th>
						 <th>Date Approved/Rejected</th>
						 <th>Operation</th>
					</tr>
				 </thead>
				 <tbody>
					<form action="<?php echo SITE_URL ?>/move_fwd_gallery">
					 <tr>
						 <td></td>
						 <td><input name="first_name" class="form-control" value="<?php echo isset($_GET['first_name']) ? $_GET['first_name'] : '' ?>" /></td>
						 <td></td>
						 <td><input name="challenge" class="form-control" value="<?php echo isset($_GET['challenge']) ? $_GET['challenge'] : '' ?>" /></td>
						 <td><input name="move_forward_title" class="form-control" value="<?php echo isset($_GET['move_forward_title']) ? $_GET['move_forward_title'] : '' ?>" /></td>
						 <td>
							<select name="mfg_status" class="form-control">
								<option></option>
								<option value="0" <?php echo $this->input->get('mfg_status') != '' && $this->input->get('mfg_status') == '0' ? 'selected' : '' ?>>Pending</option>
								<option value="1" <?php echo $this->input->get('mfg_status') != '' && $this->input->get('mfg_status') == '1' ? 'selected' : '' ?>>Approved</option>
								<option value="2" <?php echo $this->input->get('mfg_status') != '' && $this->input->get('mfg_status') == '2' ? 'selected' : '' ?>>Rejected</option>
								<option value="3" <?php echo $this->input->get('mfg_status') != '' && $this->input->get('mfg_status') == '3' ? 'selected' : '' ?>>Exempted</option>
							</select>
						</td>
						 <td></td>
						 <td>
						 	<div class="row">
                                <div class="col-md-6 col-lg-4">
                                     <input name="fromavail" placeholder="From" class="form-control from" value="<?php echo isset($_GET['fromavail']) ? $_GET['fromavail'] : '' ?>" />
                                </div>
                                <div class="col-md-6 col-lg-4">
                                     <input name="toavail" placeholder="To" class="form-control to" value="<?php echo isset($_GET['toavail']) ? $_GET['toavail'] : '' ?>" />
                                </div>
                            </div>
						 </td>
						  <td>
						 	<div class="row">
                                <div class="col-md-6 col-lg-4">
                                     <input name="fromapp" placeholder="From" class="form-control from" value="<?php echo isset($_GET['fromapp']) ? $_GET['fromapp'] : '' ?>" />
                                </div>
                                <div class="col-md-6 col-lg-4">
                                     <input name="toapp" placeholder="To" class="form-control to" value="<?php echo isset($_GET['toapp']) ? $_GET['toapp'] : '' ?>" />
                                </div>
                              </div>
						 </td>
						 <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
					</tr>
					</form>
						<?php if($categories): 
					foreach($categories as $k => $v): ?>
					<tr>
						 <td><input type="checkbox" name="checkbox[]" value="<?php echo $v['move_forward_gallery_id'] ?>" class="comment-ids" /></td>
						 <td><a href="#" onclick="showRegistrantDetails(<?php echo $v['registrant_id'] ?>)"><?php echo $v['first_name'] . ' ' . $v['third_name'] ?></a></td>
						 <?php if($v['type'] != 'Text') : ?>
						 <td>
							<?php if($v['mfg_content']): ?>
							<a class="modal-pop" href="#" data-title="Media">
							<?php if(preg_match('![?&]{1}v=([^&]+)!', $v['mfg_content'] . '&', $data)){ ?>
							<img onclick="loadVideo('<?=$data[1]?>')" src="http://img.youtube.com/vi/<?=$data[1]?>/mqdefault.jpg" width="115">
							<?php } else { ?>
							<img onerror="this.src='<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/move_fwd/<?php echo $v['mfg_content'] ?>'" src="<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/move_fwd/115_71_<?php echo $v['mfg_content'] ?>" width="115" /></a>
 							<?php } ?>
							<?php else:
							echo '';
							endif; ?>
							<div class="modal">
								<div style="text-align:center">  
									<?php if(preg_match('![?&]{1}v=([^&]+)!', $v['mfg_content'] . '&', $data)){ ?>
									<div class="video-container">
										<iframe width="560" height="315" frameborder="0" allowfullscreen></iframe>
									</div>
									<?php } else { ?>
								<img onerror="this.src='<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/move_fwd/<?php echo $v['mfg_content'] ?>'" src="<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/move_fwd/<?php echo $v['mfg_content'] ?>" width="500" />  
								<?php } ?>
								</div>
							</div>

						 </td>
						<?php else: ?>
						<td><?php echo nl2br($v['mfg_content']); ?></td>
						<?php endif ?>
						 <td><?php echo $v['challenge'] ?></td>
						 <td><?php echo $v['move_forward_title'] ?></td>
						 <td>
							 <?php echo $status[$v['mfg_status']] ?>
						 </td>
						 <td><?php echo $v['source'] == '1' ? 'Web' : 'Mobile'?></td>
						 <td><?php echo date('F d, Y H:i:s', strtotime($v['mfg_date_created'])) ?></td>
						 <td><?php echo $v['mfg_status'] != 0 ? date('F d, Y H:i:s', strtotime($v['mfg_date_approved'])) : 'N/A'; ?></td>
						 <td>
							<?php if($v['mfg_status'] != 1 && $edit): ?>
							<a href="<?php echo SITE_URL ?>/move_fwd_gallery/approve/<?php echo $v['move_forward_gallery_id'] ?>/<?php echo md5($v['move_forward_gallery_id'] . ' ' . $this->config->item('encryption_key')) ?>" class="btn btn-success" onclick="return confirmAction(this, 'approve')">Approve</a>
							<?php endif; ?>
							<?php if($v['mfg_status'] != 2 && $edit): ?>
							<a href="#" class="btn btn-warning" onclick="return rejectEntry(<?php echo $v['move_forward_gallery_id'] ?>, '<?php echo md5($v['move_forward_gallery_id'] . ' ' . $this->config->item('encryption_key')) ?>')">Reject</a>
							<?php endif; ?> 

						 </td>
					</tr>
					<?php 
					endforeach;
					else:
						echo '<tr><td colspan="8">No records found</td></tr>';
					endif; ?>
				 </tbody>
			</table>

			<ul class="pagination pagination-sm pull-right">
				 <?php echo $pagination ?>
			</ul>
			
			<div class="modal" id="reason">
			<div style="text-align:center">
				<form id="reason-form" method="post">
					<textarea name="message" class="form-control" rows="5"></textarea>
					<br>
					<div style="float:right">
						<button type="button" class="btn close-bootbox" name="search">Cancel</button>
						<button class="btn btn-primary" name="search" value="1">Submit</button>
					</div>
					<br style="clear:both;">
				</form>
			</div>
			</div>

			

	 </div>
	 <!--end main content -->

	 <script>

	 function multiple_disapproval(){
 
        if($('input[name="checkbox[]"]').is(':checked')){

          var form = document.getElementById('reason-form');
            
          $.each($('input[name="checkbox[]"]'),function(i,val){
            
            if($(this).is(':checked')){

              var input = document.createElement('input');
              input.type = 'hidden';
              input.name = 'ids[]';
              input.value = val.value;
              form.appendChild(input);

            }

          });
          
          $("#reason form").prop("action", "<?=SITE_URL?>/move_fwd_gallery/multiple_disapprove");
          bootbox.dialog({'message': $('#reason').html(), 'title': 'Reason for disapproval'});
           
        }else{

          bootbox.alert('Please select entries to disapprove.');
          return false;

        }       

     }

		function rejectEntry(id, token) {
		$('#reason').find('form').attr('action', '<?php echo SITE_URL ?>/move_fwd_gallery/disapprove/' + id + '/' + token); 
		bootbox.dialog({'message': $('#reason').html(),
				'title': 'Reason for Rejection'});
		return false;
	 }
	function loadVideo(src) {
		//alert(src);
		$('.video-container').find('iframe').attr('src', '//www.youtube.com/embed/' + src + '?html5=1');  
	 }
	 function videoPreview(str){

		var start = str.indexOf("?v=");
		start = start+3;
		var end = str.indexOf("&");
		var videoID = end<0 ? str.substring(start) : str.substring(start,end);

		$('.video-container').html('');

		var obj = '<object width="100%" height="100%">';
		obj +='<param name="movie" value="//www.youtube.com/v/'+videoID+'?hl=en_US&version=3"></param>';
		obj +='<param name="allowFullScreen" value="true"></param>';
		obj +='<param name="allowscriptaccess" value="always"></param>';
		obj +='<embed src="//www.youtube.com/v/'+videoID+'?hl=en_US&version=3" type="application/x-shockwave-flash" width="100%" height="100%" allowscriptaccess="always" allowfullscreen="true">';
		obj +='</embed>';
		obj +='</object>';

		$('.video-container').html(obj);
	}


	$('#check-all').click(function() {
        if($(this).is(':checked'))
          $('input[type="checkbox"]').prop('checked', true)
        else
          $('input[type="checkbox"]').prop('checked', false)
     });

	 $('#approve-all').click(function() {

	      ids = [];
	      $('.comment-ids').each(function() {
	        if($(this).is(':checked')) {
	          ids.push($(this).val());
	        }
	      });
	      if(ids.length < 1) {
	        bootbox.alert('Sorry, you must select at least 1 entry');  
	      } else {
	          bootbox.confirm('Are you sure you want approve all selected move forward gallery entries?', function(result) {
	            if(result == 1) {
	              $.post('<?php echo SITE_URL ?>/move_fwd_gallery/multiple_approve', {ids: ids}, function() {
	                window.location.reload();
	              })
	              return true;
	            }
	          });

	      }

     });

     $('#exempt-all').click(function() {

	      ids = [];
	      $('.comment-ids').each(function() {
	        if($(this).is(':checked')) {
	          ids.push($(this).val());
	        }
	      });
	      if(ids.length < 1) {
	        bootbox.alert('Sorry, you must select at least 1 entry');  
	      } else {
	          bootbox.confirm('Are you sure you want to exempt all selected move forward gallery entries?', function(result) {
	            if(result == 1) {
	            	
	              $.post('<?php echo SITE_URL ?>/move_fwd_gallery/set_exempted_all', {ids: ids}, function() {
	                window.location.reload();
	              })
	              return true;
	            }
	          });

	      }

     });


	 </script>