<div class="modal-header">
   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   <h4 class="modal-title"><?=$row ? ucwords(strtolower($row->title)) : '' ?></h4>
  </div>

<div class="modal-body text-center"> 

	<table class="table table-bordered">

		<tr>
			<th><center>#</center></th>
 			<th>User Name</th>
 			<th>Status</th>
			<th>Date Submitted</th>
		</tr>
		<?php if($rows->num_rows()){ 
				$c = 0;
			    $statuses = array('Pending','Approved','Rejected');
				foreach($rows->result() as $v){
					$c++;?>
					<tr>
						<td><?=$c?></td>
 						<td align="left"><?=($v->registrant_id) ? ucwords(strtolower($v->first_name.' '.$v->third_name)) : '(<i>Not found</i>)'?></td>
 						<td align="left"><?=@$statuses[$v->mfg_status]?></td>
						<td align="left"><?=$v->mfg_date_created?></td>
					</tr>
		<?php  }
			} ?>

</div>