<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>User MoveFWD Visits <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/user_movefwd_visits/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>   
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>Name</th>
                         <th>Count</th>
                         <th></th>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/user_movefwd_visits">
                     <tr>
                         <td><input name="name" class="form-control" value="<?php echo isset($_GET['name']) ? $_GET['name'] : '' ?>" /></td>
                         <td>
                          
                         </td>
                          <td>
                              From:<input name="fromdate" class="form-control from" value="<?php echo isset($_GET['fromdate']) ? $_GET['fromdate'] : '' ?>" /><br> 
                              To:<input name="todate" class="form-control to" value="<?php echo isset($_GET['todate']) ? $_GET['todate'] : '' ?>" /><br>
                              Field: 
                              <select name="field" class="form-control">
                                  <option></option>
                                  <option value="first_name" <?php echo $this->input->get('field') && $this->input->get('field') == 'first_name' ? 'selected' :'' ?>>Name</option>
                                  <option value="count" <?php echo $this->input->get('field') && $this->input->get('field') == 'count' ? 'selected' :'' ?>>Count</option>
                              </select>
                            Sort: 
                                <select name="sort" class="form-control">
                                  <option></option>
                                  <option value="asc" <?php echo $this->input->get('sort') && $this->input->get('sort') == 'asc' ? 'selected' :'' ?>>Ascending</option>
                                  <option value="desc" <?php echo $this->input->get('sort') && $this->input->get('sort') == 'desc' ? 'selected' :'' ?>>Descending</option>
                              </select><br><input name="type" type="hidden" value="<?php echo $this->input->get('type') ? $this->input->get('type') : 'age' ?>" /><br><button class="btn btn-primary" name="search" value="1">Go</button>
                            </td>
                    </tr>
                    </form>
                    <?php if($record): 
					foreach($record as $k => $v): $game = str_replace('games/', '', $v['section']); ?>
                    <tr>
                         <td><a href="#" onclick="showRegistrantDetails(<?php echo $v['registrant_id'] ?>)"><?php echo $v['first_name'] . ' ' . $v['third_name'] ?></a></td>
                         <td><a href="<?php echo SITE_URL ?>/user_movefwd_visits/view?id=<?php echo $v['registrant_id'] ?>&fromdate=<?php echo $this->input->get('fromdate') ?>&todate=<?php echo $this->input->get('fromdate') ?>" class="view"><?php echo $v['count']?></a></td>
                         <td></td>
                    </tr>
                    <?php 
					endforeach;
          else:
            echo '<tr><td colspan="7">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->
      <div id="view-content" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>
            </div>
          </div> 
      </div>

     <script>    

     $('.view').on('click',function(e){
          var href = $(this).attr('href');
          $('.modal-content').html('<div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>');
          $('#view-content').modal('show');
          $('.modal-content').load(href);
          e.preventDefault();
     });

     $('#view-content').on('hidden.bs.modal',function(){
        $('.modal-content').html('<img src="<?=BASE_URL?>images/spinner.gif"/></div>');
     });

     </script>