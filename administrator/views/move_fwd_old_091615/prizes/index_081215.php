<?php 
  $controller = $this->uri->segment(1);
  $label = 'MoveFWD Accomplished Activities';
?>
<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3><?php echo $label ?> <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>
               <div class="actions">
               <a href="<?php echo SITE_URL ?>/export/<?php echo $controller ?>?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>     
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Move FWD Title</th>
                         <th>Prize</th>
                         <th>Thumbnail</th>
                         <th>Status</th>
                         <th>Type</th>
                         <th>Mode</th>
                         <th>Date Accomplished</th>
                         <th>Date Redeemed/Delivered</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
                    <form action="<?php echo SITE_URL . '/' . $controller ?>">
                     <tr>
                         <td></td>
                         <td><input name="first_name" class="form-control" value="<?php echo isset($_GET['first_name']) ? $_GET['first_name'] : '' ?>" /></td>
                         <td><input name="move_forward_title" class="form-control" value="<?php echo isset($_GET['move_forward_title']) ? $_GET['move_forward_title'] : '' ?>" /></td>
                         <td><input name="prize_name" class="form-control" value="<?php echo isset($_GET['prize_name']) ? $_GET['prize_name'] : '' ?>" /></td>
                         <td></td>
                         <td>
                          <select name="prize_delivered" class="form-control">
                            <option></option>
                            <option value="0" <?php echo $this->input->get('prize_status') != '' && $this->input->get('prize_status') == '0' ? 'selected' : '' ?>>Pending</option>
                            <option value="1" <?php echo $this->input->get('prize_status') != '' && $this->input->get('prize_status') == '1' ? 'selected' : '' ?>>Delivered/Redeemed</option>
                          </select>
                        </td>
                        <td>
                          <select name="send_type" class="form-control">
                            <option></option>
                            <option <?php echo $this->input->get('send_type') != '' && $this->input->get('send_type') == 'Delivery' ? 'selected' : '' ?>>Delivery</option>
                            <option <?php echo $this->input->get('send_type') != '' && $this->input->get('send_type') == 'Redemption' ? 'selected' : '' ?>>Redemption</option>
                          </select>
                         </td>
                         <td>
                          <select name="move_forward_choice" class="form-control">
                            <option></option>
                            <option value="1" <?php echo $this->input->get('move_forward_choice') != '' && $this->input->get('move_forward_choice') == 1 ? 'selected' : '' ?>>Play</option>
                            <option value="2" <?php echo $this->input->get('move_forward_choice') != '' && $this->input->get('move_forward_choice') == 2 ? 'selected' : '' ?>>Pledge</option>
                          </select>
                         </td>
                        <td>
                          <div class="row">
                              <div class="col-md-6 col-lg-4">
                                   <input name="fromaccom" placeholder="From" class="form-control from" value="<?php echo isset($_GET['fromaccom']) ? $_GET['fromaccom'] : '' ?>" />
                              </div>
                              <div class="col-md-6 col-lg-4">
                                   <input name="toaccom" placeholder="To" class="form-control to" value="<?php echo isset($_GET['toaccom']) ? $_GET['toaccom'] : '' ?>" />
                              </div>
                         </div>
                         </td>
                         <td>
                          <div class="row">
                              <div class="col-md-6 col-lg-4">
                                   <input name="fromredeem" placeholder="From" class="form-control from" value="<?php echo isset($_GET['fromredeem']) ? $_GET['fromredeem'] : '' ?>" />
                              </div>
                              <div class="col-md-6 col-lg-4">
                                   <input name="toredeem" placeholder="To" class="form-control to" value="<?php echo isset($_GET['toredeem']) ? $_GET['toredeem'] : '' ?>" />
                              </div>
                         </div>
                         </td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
               		  <?php if($categories): 
					foreach($categories as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><a href="#" onclick="showRegistrantDetails(<?php echo $v['registrant_id'] ?>)"><?php echo $v['first_name'] . ' ' . $v['third_name'] ?></a></td>
                         <td><?php echo $v['move_forward_title'] ?></td>
                         <td><?php echo $v['prize_name'] ?></td>
                         <td><img src="<?php echo $v['prize_image'] ? BASE_URL . 'uploads/prize/' . $v['prize_image'] : BASE_URL . 'images/no_image.jpg' ?>?uniqid=<?php echo uniqid() ?>" width="150" /></td>
                         <td>
                            <?php 
                              $status = 'Pending';
                              if($v['prize_delivered'] == 1) {
                                if($v['send_type'] == 'Delivery')
                                  $status = 'Delivered';
                                else
                                  $status = 'Redeemed';                                    
                              } 
                              echo $status;
                            ?>
                         </td>
                         <td><?php echo $v['send_type'] ?></td>
                         <td><?php echo $v['move_forward_choice'] == 1 ? 'Play' : 'Pledge' ?></td>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['move_forward_choice_done'])) ?></td>
                         <td><?php echo $v['prize_delivered_date'] == 0 ? 'N/A' : date('F d, Y H:i:s', strtotime($v['prize_delivered_date'])) ?></td>
                         <td>
                            <?php if($edit): ?>
                            <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/edit/<?php echo $v['move_forward_choice_id'] ?>" class="btn btn-primary">Edit</a>
                            <?php endif; ?>
                            <?php if($edit && $v['is_winner'] == 1): ?>
                            <a class="btn btn-warning" href="<?php echo SITE_URL ?>/move_fwd_prizes/unset_winner/<?php echo $v['move_forward_choice_id'] ?>/<?php echo md5($v['move_forward_choice_id'] . ' ' . $this->config->item('encryption_key')) ?>" class="btn btn-success" onclick="return confirmAction(this, 'approve')">Unset Winner</a>
                            <?php endif; ?>
                            <?php if($edit && $v['is_winner'] == 0): ?>
                            <a href="<?php echo SITE_URL ?>/move_fwd_prizes/set_winner/<?php echo $v['move_forward_choice_id'] ?>/<?php echo md5($v['move_forward_choice_id'] . ' ' . $this->config->item('encryption_key')) ?>" class="btn btn-success" onclick="return confirmAction(this, 'approve')">Set Winner</a>
                            <?php endif; ?>
                         </td>
                    </tr>
                    <?php 
					endforeach;
          else:
            echo '<tr><td colspan="9">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
             <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->