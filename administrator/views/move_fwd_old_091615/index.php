
<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>MoveFWD <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL ?>/move_fwd/add" class="btn btn-primary">Add MoveFWD</a>
                    <?php endif; ?>
                    <a href="<?php echo SITE_URL ?>/export/move_fwd?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>   
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Thumbnail</th>
                         <th>Category</th>
                         <th>Challenges</th>
                         <!-- <th>Slots</th> -->
                         <th>Pledge Points</th>
                         <th>Status</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/move_fwd">
                     <tr>
                        <td></td>
                        <td><input name="move_forward_title" class="form-control" value="<?php echo isset($_GET['move_forward_title']) ? $_GET['move_forward_title'] : '' ?>" /></td>
                        <td></td>
                        <td>
                          <select name="category_id" class="form-control">
                            <option></option>
                            <?php if($main_categories): ?>
                            <?php foreach($main_categories as $k => $v): ?>
                              <?php if($v['origin_id'] == MOVE_FWD): ?>
                              <option <?php echo isset($_GET['category_id']) && $_GET['category_id'] == $v['category_id'] ? 'selected' : '' ?> value="<?php echo $v['category_id'] ?>"><?php echo $v['category_name'] ?></option>
                            <?php endif; ?>
                          <?php endforeach; ?>
                           <?php endif; ?>
                          </select>
                         </td>
                         <td><input name="challenges" class="form-control" value="<?php echo isset($_GET['challenges']) ? $_GET['challenges'] : '' ?>" /></td>
                         <!-- <td><input name="slots" class="form-control" value="<?php echo isset($_GET['slots']) ? $_GET['slots'] : '' ?>" /></td> -->
                         <td><input name="pledge_points" class="form-control" value="<?php echo isset($_GET['pledge_points']) ? $_GET['pledge_points'] : '' ?>" /></td>
                         <td>
                          <select name="status" class="form-control">
                            <option></option>
                            <option value="1" <?php echo isset($_GET['status']) && $_GET['status'] == '1' ? 'selected' : '' ?>>Active</option>
                            <option value="0" <?php echo isset($_GET['status']) && $_GET['status'] == '0' ? 'selected' : '' ?>>Inactive</option>
                          </select>
                         </td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
                    <?php if($categories): 
					foreach($categories as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['move_forward_title'] ?></td>
                         <td><img src="<?php echo file_exists('uploads/move_fwd/thumb_' . $v['move_forward_image']) ? BASE_URL . 'uploads/move_fwd/thumb_' . $v['move_forward_image'] : BASE_URL . 'uploads/move_fwd/' . $v['move_forward_image'] ?>?uniqid=<?php echo uniqid() ?>" width="150" /></td>
                         <td><?php echo $main_categories[$v['category_id']]['category_name'] ?></td>
                         <td><?php echo $v['challenges'] ?></td>
                         <!-- <td><?php echo $v['slots'] ?></td> -->
                         <td><?php echo $v['pledge_points'] ?></td>
                         <td><?php echo $v['status'] == '1' ? 'Active' : 'Inactive' ?></td>
                         <td>
                            <?php if($edit): ?>
                            <a href="<?php echo SITE_URL ?>/move_fwd/edit/<?php echo $v['move_forward_id'] ?>" class="btn btn-primary">Edit</a>
                            <?php endif; ?>
                            <?php if($delete): ?>
                            <a href="<?php echo SITE_URL ?>/move_fwd/delete/<?php echo $v['move_forward_id'] ?>/<?php echo md5($v['move_forward_id'] . ' ' . $this->config->item('encryption_key')) ?>" onclick="return confirmDeletion(this, 'move fwd')" class="btn btn-danger">Delete</a>
                            <?php endif; ?>
                         </td>
                    </tr>
                    <?php 
					endforeach;
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->