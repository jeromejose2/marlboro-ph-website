
<script src="<?=BASE_URL?>admin_assets/js/angular-js/angular-file-upload-shim.js"></script>
<script src="<?=BASE_URL?>admin_assets/js/angular-js/angular.js"></script>
<script src="<?=BASE_URL?>admin_assets/js/angular-js/angular-file-upload.js"></script>
<script src="<?=BASE_URL?>admin_assets/js/angular-js/upload-move-fwd-gallery.js"></script>
 


<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <h4 class="modal-title"><?=$header_title?></h4>
</div>

<div class="modal-body text-center"  ng-controller="MyCtrl">

   <form class="form-horizontal" action="<?=SITE_URL?>/move_fwd_new_gallery/save_media" role="form" method="post" enctype="multipart/form-data" id="form">
    <input type="hidden" name="myModel" ng-model="myModel" value="1"/>
    <input type="hidden" name="comment" value="<?=$row->comment_id?>"/>


    <div class="form-group">
            <label class="col-sm-3 control-label">MoveFWD Offer:</label>
            <div class="col-sm-5">
                 
                 <select name="move_fwd_offer" class="form-control" >
                            <?php if($move_fwd_offers->num_rows()){
                              foreach($move_fwd_offers->result() as $e){
                                      if($row->suborigin_id==$e->move_forward_id){ ?>
                                        <option value="<?=$e->move_forward_id?>" selected><?=$e->move_forward_title?></option>
                                <?php }else{ ?>
                                        <option value="<?=$e->move_forward_id?>"><?=$e->move_forward_title?></option>
                                <?php } 
                                 }
                            } ?>
                          </select>
            </div>
       </div>
 
 
       <?php if(file_exists('uploads/profile/admin_'.$row->admin_id.'/comments/'.$row->comment_video) && $row->comment_video){ ?>
       <div class="form-group">

            <div class="col-md-12">
              <div class="thumbnail"><?php 
                     $video_mime_type = get_mime_by_extension('uploads/profile/admin_'.$row->admin_id.'/comments/'.$row->comment_video);  ?>
                        <video width="100%" height="100%" controls>
                            <source src="<?=BASE_URL.'uploads/profile/admin_'.$row->admin_id.'/comments/'.$row->comment_video?>" type="<?=$video_mime_type?>">
                          Your browser does not support the video tag. 
                        </video>
               </div>
            </div>
       </div>

       <div class="form-group">
            <label class="col-sm-4  control-label">Change Thumbnail:</label>
            <div class="col-sm-5">
                 <input type="file" ng-file-select="onFileSelect($files)">
            </div>
       </div>

      <?php }else{ ?>
 
       <div class="form-group">
            <label class="col-sm-3 control-label"><?=$file_label?></label>
            <div class="col-sm-5">
                 <input type="file" ng-file-select="onFileSelect($files)">
            </div>
       </div> 

       <?php } ?>

       <div class="form-group preview" style="display:none">
           <div class="col-md-12" >

            <div class="row" ng-show="selectedFiles == null">

              <div class="col-md-12 col-xs-12">
                <div class="thumbnail">
                  <img src="<?=BASE_URL.'uploads/profile/admin_'.$row->admin_id.'/comments/150_150_'.$row->comment_image?>" style="max-height:250px">
                  <caption><?=$row->comment_video!='' ? 'Video Thumbnail' : ''?></caption>
                </div>
              </div>

            </div>

            <div class="row" ng-show="selectedFiles != null"> 

              <!-- End ng-start -->
              <div class="col-md-12 col-xs-12" ng-repeat="f in selectedFiles">
                <div class="thumbnail">
                  <img ng-show="dataUrls[$index]" ng-src="{{dataUrls[$index]}}" >
                  <div class="caption">
                      <p class="text-danger">{{uploadError[$index]}} {{removeError[$index]}}</p>
                      <input type="text" name="media[]" ng-model="uploadResult[$index]" style="visibility:hidden;" >
                      <span class="progress progress-striped" ng-show="progress[$index] >= 0">           
                        <div class="progress-bar" style="width:{{progress[$index]}}%">{{progress[$index]}}%</div>
                      </span>
                      <div class="clearfix"></div>
                    <p>size: {{f.size}}B<br/>type: {{f.type}}</p>
                  </div>

                </div>
                 
              </div> 
              <!-- End ng-repeat -->               
              
            </div> 

            <div class="clearfix"></div>
              <div class="col-md-12"><button type="submit" class="btn btn-primary">Update</div>
          </div>

       </div>

         <input type="text" ng-model="uploadURL" ng-init="uploadURL='<?=SITE_URL.'/move_fwd_new_gallery/upload_media/'.$row->comment_id?>'" style="visibility:hidden;"> 
          
   </form>

</div>


<script>
 
    angular.element(document).ready(function() {
      angular.bootstrap(document, ['fileUpload']);
      $('.preview').show();
    });
 
</script>
  
     
      