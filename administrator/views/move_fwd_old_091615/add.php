
<script src="<?php echo BASE_URL ?>admin_assets/js/bootstrap-tagsinput.js"></script>
<?php $this->load->view('editor.php') ?>
<script src="<?php echo BASE_URL ?>admin_assets/js/jquery.Jcrop.js"></script>
<script type="text/javascript">
var jscrop_api;
function initJCrop(elem) {
	<?php if(isset($record['thumbnail_meta'])):  
		$points = isset($record['thumbnail_meta']) ? unserialize($record['thumbnail_meta']) : null; ?>
		x1 = <?php echo $points['x1'] != '' ? $points['x1'] : 0 ?>;
		x2 = <?php echo $points['x2'] != '' ? $points['x2'] : 0 ?>;
		y1 = <?php echo $points['y1'] != '' ? $points['y1'] : 0 ?>;
		y2 = <?php echo $points['y2'] != '' ? $points['y2'] : 0 ?>;
	<?php else: ?>  
		x1 = 0;
		y1 = 0;
		x2 = 200;
		y2 = 200;			
	<?php endif; ?>;
	
	setTimeout(function() {
		if(jscrop_api)
			jcrop_api.destroy();
		$('#preview').Jcrop({
		  onChange:   showCoords,
		  onSelect:   showCoords,
		  setSelect: [x1, y1, x2, y2],
		  maxSize: [ 300, 300 ]
		},function(){
		  jcrop_api = this;
		});	
		$('#width').val($('#preview').width());
		$('#height').val($('#preview').height());
	}, 500);
}

function showCoords(c) {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
};

$(function() {
	<?php if(isset($record['move_forward_image']) && $record['move_forward_image'] != ''): ?>
	initJCrop($('#photo'));
	<?php endif; ?>

  $('#type').change(function() {
    if($(this).val() == 'Redemption') {
      $('#r-address-container').show();
    } else {
      $('#r-address-container').hide();
    }
  });

});

</script>
<link rel="stylesheet" href="<?php echo BASE_URL ?>admin_assets/css/jquery.Jcrop.css" type="text/css" />
<link rel="stylesheet" href="<?php echo BASE_URL ?>admin_assets/css/bootstrap-tagsinput.css" type="text/css" />
<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3>MoveFWD</h3>
      </div>
       <div class="alert alert-danger" <?php if(!$error) echo 'style="display:none;"' ?>><?php echo $error; ?></div>
       <form class="form-horizontal" role="form" method="post" onsubmit="return submitFrm()" enctype="multipart/form-data">
           <div class="form-group">
                <label class="col-sm-2 control-label">Title</label>
                <div class="col-sm-4">
                     <input type="text" name="move_forward_title" value="<?php echo isset($record['move_forward_title']) ? $record['move_forward_title'] : '' ?>" data-name="title" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Category</label>
                <div class="col-sm-4">
                    <select class="form-control" name="category_id">
                    	<?php foreach($categories as $k => $v): ?>
                        <option value="<?php echo $v['category_id'] ?>" <?php echo isset($record['category_id']) && $record['category_id'] == $v['category_id'] ? 'selected="selected"' : '' ?>><?php echo $v['category_name']; ?></option>
                        <?php endforeach;  ?>
                    </select>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Image</label>
                <div class="col-sm-4">
                     <input type="file" id="photo" name="photo" value="" data-name="module name" class="form-control" onchange="previewImage('photo', 'preview', initJCrop, this);"><br>
                     <strong>Width: 500px. Height: 410px.</strong>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Image Preview</label>
                <div class="col-sm-4">
                   <img id="preview" class="preview" <?php echo isset($record['move_forward_image']) ? "style='max-width:500px;' src='" . BASE_URL . 'uploads/move_fwd/' . $record['move_forward_image'] . "'" : '' ?>  />
                </div>
           </div>
          <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-8">
                     <textarea name="move_forward_description"><?php echo isset($record['move_forward_description']) ? $record['move_forward_description'] : '' ?></textarea>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Status</label>
                <div class="col-sm-4">
                    <select class="form-control" name="status">
                      <option <?php echo isset($record['status']) && $record['status'] == 1 ? 'selected' : '' ?> value="1">Active</option>
                      <option <?php echo isset($record['status']) && $record['status'] == 0 ? 'selected' : '' ?> value="0">Inactive</option>   
                    </select>
                </div>
           </div>
           <div class="form-group">
                    <label class="col-sm-2 control-label">Prize</label>
                    <div class="col-sm-4">
                         <select class="form-control" name="prize_id">
                          <option value="0"></option>
                          <?php foreach($prizes as $k => $v): ?>
                            <option value="<?php echo $v['prize_id'] ?>" <?php echo isset($record['prize_id']) && $record['prize_id'] == $v['prize_id'] ? 'selected="selected"' : '' ?>><?php echo $v['prize_name']; ?></option>
                            <?php endforeach;  ?>
                        </select>
                    </div>
               </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Tags</label>
                <div class="col-sm-4">
                  <input type="text" name="tags" value="<?php echo isset($record['tags']) ? $record['tags'] : '' ?>" data-role="tagsinput" placeholder="Add tags" class="form-control" />
                </div>
           </div>
          <!--  <div class="form-group">
                <label class="col-sm-2 control-label">Slots</label>
                <div class="col-sm-4">
                  <input type="text" onkeydown="return checkDigit(event)" name="slots" value="<?php echo isset($record['slots']) ? $record['slots'] : '' ?>" data-name="slots" class="required number form-control">
                </div>
           </div> -->
           <div class="form-group">
                      <label class="col-sm-2 control-label">Required Points</label>
                      <div class="col-sm-4">
                        <input type="text" onkeydown="return checkDigit(event)" name="pledge_points" value="<?php echo isset($record['pledge_points']) ? $record['pledge_points']: '' ?>" data-name="required points" class="required form-control">
                      </div>
                 </div>
          <div class="form-group">
                      <label class="col-sm-2 control-label">Is Premium</label>
                      <div class="col-sm-4">
                        <input type="checkbox" name="is_premium_offer" value="1" <?php echo !empty($record['is_premium_offer']) ? 'checked="checked"' : '' ?>>
                      </div>
                 </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">Map Location</label>
            <div class="col-sm-4">
                      <div id="google_map_canvas" style="width: 480px; height: 480px"></div><br>
                     <input type="text" id="longitude" name="lng" value="<?php echo isset($record['lng']) ? $record['lng'] : '' ?>" data-name="longitude" class="required form-control" placeholder="Longitude"><br>
                     <input type="text" id="latitude" name="lat" value="<?php echo isset($record['lat']) ? $record['lat'] : '' ?>" data-name="latitude" class="required form-control" placeholder="Latitude">
                </div>
             </div>
             <div class="form-group">
                <label class="col-sm-2 control-label">Platform Restriction</label>
                <div class="col-sm-4">
                    <select class="form-control" name="platform_restriction">
                      <option <?php echo isset($record['platform_restriction']) && $record['platform_restriction'] == WEB_ONLY ? 'selected' : '' ?> value="<?= WEB_ONLY ?>">Website</option>
                      <option <?php echo isset($record['platform_restriction']) && $record['platform_restriction'] == MOBILE_ONLY ? 'selected' : '' ?> value="<?= MOBILE_ONLY ?>">Mobile App</option>   
                      <option <?php echo isset($record['platform_restriction']) && $record['platform_restriction'] == BOTH_PLATFORM ? 'selected' : '' ?> value="<?= BOTH_PLATFORM ?>">Mobile App & Website</option>   
                    </select>
                </div>
           </div>
          <!-- Nav tabs --> 
          <ul class="nav nav-tabs">
            <li class="active"><a href="#task1" data-toggle="tab">Task 1</a></li>
            <li><a href="#task2" data-toggle="tab">Task 2</a></li>
            <li><a href="#task3" data-toggle="tab">Task 3</a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div class="tab-pane active" id="task1">
                <br >
               <div class="form-group">
                  <label class="col-sm-2 control-label">Task</label>
                  <div class="col-sm-4">
                       <?php if(!$error): ?>
                        <input type="text" name="task[]" value="<?php echo isset($record['challenges'][0]['challenge']) ? $record['challenges'][0]['challenge'] : '' ?>" data-name="task" class="required form-control">
                       <?php else: ?>
                       <input type="text" name="task[]" value="<?php echo isset($_POST['task'][0]) ? $_POST['task'][0] : '' ?>" data-name="task" class="required form-control">
                     <?php endif; ?>
                  </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Type</label>
                    <div class="col-sm-4">
                         <select class="form-control" name="type[]">
                          <?php if(!$error): ?>
                          <option value="Photo" <?php echo isset($record['challenges'][0]['type']) && $record['challenges'][0]['type'] == 'Photo' ? 'selected="selected"' : '' ?>>Photo</option>
                          <option value="Video" <?php echo isset($record['challenges'][0]['type']) && $record['challenges'][0]['type'] == 'Video' ? 'selected="selected"' : '' ?>>Video</option>
                          <option value="Text" <?php echo isset($record['challenges'][0]['type']) && $record['challenges'][0]['type'] == 'Text' ? 'selected="selected"' : '' ?>>Text</option>
                          <?php else: ?>
                          <option value="Photo" <?php echo isset($_POST['type'][0]) && $_POST['type'][0] == 'Photo' ? 'selected="selected"' : '' ?>>Photo</option>
                          <option value="Video" <?php echo isset($_POST['type'][0]) && $_POST['type'][0] == 'Video' ? 'selected="selected"' : '' ?>>Video</option>
                          <option value="Text" <?php echo isset($_POST['type'][0]) && $_POST['type'][0] == 'Text' ? 'selected="selected"' : '' ?>>Text</option>
                        <?php endif; ?> 
                        </select>
                       <?php if(!$error): ?>
                          <input type="hidden" name="challenge_id[]" value="<?php echo isset($record['challenges'][0]['challenge_id']) ? $record['challenges'][0]['challenge_id'] : '' ?>">
                          <?php else: ?>  
                          <input type="hidden" name="challenge_id[]" value="<?php echo isset($_POST['challenge_id'][0]) ? $_POST['challenge_id'][0] : "" ?>">
                          <?php endif; ?> 
                    </div>
               </div>
              <!-- <div class="form-group">
                    <label class="col-sm-2 control-label">Prize</label>
                    <div class="col-sm-4">
                         <select class="form-control" name="prize[]">
                          <option value="0"></option>
                          <?php foreach($prizes as $k => $v): ?>
                            <option value="<?php echo $v['prize_id'] ?>" <?php echo isset($record['challenges'][0]['prize_id']) && $record['challenges'][0]['prize_id'] == $v['prize_id'] ? 'selected="selected"' : '' ?>><?php echo $v['prize_name']; ?></option>
                            <?php endforeach;  ?>
                        </select>
                    </div>
               </div> -->
            </div>
            <div class="tab-pane" id="task2">
              <br>
              <div class="form-group">
                    <label class="col-sm-2 control-label">Task</label>
                    <div class="col-sm-4">
                         <?php if(!$error): ?>
                          <input type="text" name="task[]" value="<?php echo isset($record['challenges'][1]['challenge']) ? $record['challenges'][1]['challenge'] : '' ?>" data-name="task" class="form-control">
                         <?php else: ?>
                         <input type="text" name="task[]" value="<?php echo isset($_POST['task'][1]) ? $_POST['task'][1] : '' ?>" data-name="task" class="form-control">
                       <?php endif; ?>
                    </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-2 control-label">Type</label>
                      <div class="col-sm-4">
                           <select class="form-control" name="type[]">
                            <?php if(!$error): ?>
                            <option value="Photo" <?php echo isset($record['challenges'][1]['type']) && $record['challenges'][1]['type'] == 'Photo' ? 'selected="selected"' : '' ?>>Photo</option>
                            <option value="Video" <?php echo isset($record['challenges'][1]['type']) && $record['challenges'][1]['type'] == 'Video' ? 'selected="selected"' : '' ?>>Video</option>
                            <option value="Text" <?php echo isset($record['challenges'][1]['type']) && $record['challenges'][1]['type'] == 'Text' ? 'selected="selected"' : '' ?>>Text</option>
                            <?php else: ?>
                            <option value="Photo" <?php echo isset($_POST['type'][1]) && $_POST['type'][1] == 'Photo' ? 'selected="selected"' : '' ?>>Photo</option>
                            <option value="Video" <?php echo isset($_POST['type'][1]) && $_POST['type'][1] == 'Video' ? 'selected="selected"' : '' ?>>Video</option>
                            <option value="Text" <?php echo isset($_POST['type'][1]) && $_POST['type'][1] == 'Text' ? 'selected="selected"' : '' ?>>Text</option>
                          <?php endif; ?> 
                          </select>
                          <?php if(!$error): ?>
                          <input type="hidden" name="challenge_id[]" value="<?php echo isset($record['challenges'][1]['challenge_id']) ? $record['challenges'][1]['challenge_id'] : '' ?>">
                          <?php else: ?>  
                          <input type="hidden" name="challenge_id[]" value="<?php echo isset($_POST['challenge_id'][1]) ? $_POST['challenge_id'][1] : "" ?>">
                          <?php endif; ?> 
                      </div>
                 </div>
                <!-- <div class="form-group">
                      <label class="col-sm-2 control-label">Prize</label>
                      <div class="col-sm-4">
                           <select class="form-control" name="prize[]">
                            <option value="0"></option>
                            <?php foreach($prizes as $k => $v): ?>
                              <option value="<?php echo $v['prize_id'] ?>" <?php echo isset($record['challenges'][1]['prize_id']) && $record['challenges'][1]['prize_id'] == $v['prize_id'] ? 'selected="selected"' : '' ?>><?php echo $v['prize_name']; ?></option>
                              <?php endforeach;  ?>
                          </select>
                      </div>
                 </div> -->
            </div>
            <div class="tab-pane" id="task3">
              <br >
              <div class="form-group">
                  <label class="col-sm-2 control-label">Task</label>
                  <div class="col-sm-4">
                       <?php if(!$error): ?>
                        <input type="text" name="task[]" value="<?php echo isset($record['challenges'][2]['challenge']) ? $record['challenges'][2]['challenge'] : '' ?>" data-name="task" class="form-control">
                       <?php else: ?>
                       <input type="text" name="task[]" value="<?php echo isset($_POST['task'][2]) ? $_POST['task'][2] : '' ?>" data-name="task" class="form-control">
                     <?php endif; ?>
                  </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Type</label>
                    <div class="col-sm-4">
                         <select class="form-control" name="type[]">
                          <?php if(!$error): ?>
                          <option value="Photo" <?php echo isset($record['challenges'][2]['type']) && $record['challenges'][2]['type'] == 'Photo' ? 'selected="selected"' : '' ?>>Photo</option>
                          <option value="Video" <?php echo isset($record['challenges'][2]['type']) && $record['challenges'][2]['type'] == 'Video' ? 'selected="selected"' : '' ?>>Video</option>
                          <option value="Text" <?php echo isset($record['challenges'][2]['type']) && $record['challenges'][2]['type'] == 'Text' ? 'selected="selected"' : '' ?>>Text</option>
                          <?php else: ?>
                          <option value="Photo" <?php echo isset($_POST['type'][2]) && $_POST['type'][2] == 'Photo' ? 'selected="selected"' : '' ?>>Photo</option>
                          <option value="Video" <?php echo isset($_POST['type'][2]) && $_POST['type'][2] == 'Video' ? 'selected="selected"' : '' ?>>Video</option>
                          <option value="Text" <?php echo isset($_POST['type'][2]) && $_POST['type'][2] == 'Text' ? 'selected="selected"' : '' ?>>Text</option>
                        <?php endif; ?> 
                        </select>
                        <?php if(!$error): ?>
                          <input type="hidden" name="challenge_id[]" value="<?php echo isset($record['challenges'][2]['challenge_id']) ? $record['challenges'][2]['challenge_id'] : '' ?>">
                          <?php else: ?>  
                          <input type="hidden" name="challenge_id[]" value="<?php echo isset($_POST['challenge_id'][2]) ? $_POST['challenge_id'][2] : "" ?>">
                          <?php endif; ?> 
                    </div>
               </div>
              <!-- <div class="form-group">
                    <label class="col-sm-2 control-label">Prize</label>
                    <div class="col-sm-4">
                         <select class="form-control" name="prize[]">
                          <option value="0"></option>
                          <?php foreach($prizes as $k => $v): ?>
                            <option value="<?php echo $v['prize_id'] ?>" <?php echo isset($record['challenges'][2]['prize_id']) && $record['challenges'][2]['prize_id'] == $v['prize_id'] ? 'selected="selected"' : '' ?>><?php echo $v['prize_name']; ?></option>
                            <?php endforeach;  ?>
                        </select>
                    </div>
               </div> -->
            </div>
          </div>

           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <input type="hidden" name="submit" value="1" />
                     <input type="hidden" id="x" name="x" />
                     <input type="hidden" id="y" name="y" />
                     <input type="hidden" id="x2" name="x2" />
                     <input type="hidden" id="y2" name="y2" />
                     <input type="hidden" id="width" name="width" />
                     <input type="hidden" id="height" name="height"/>
                     <input type="hidden" name="move_forward_image" value="<?php echo isset($record['move_forward_image']) ? $record['move_forward_image'] : '' ?>" />
                     <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                </div>
           </div>
      </form>
      
 </div>
 <!--end main content -->
 
 <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjxtjNyjATUWGLx-1v0zehvr0bkc0ACnE&sensor=false"></script>
 <script type="text/javascript">
  $(function(){

        var LatitudeLongitudeCoordinates,
            DefaultLongitude = 122.72092948635861,
            DefaultLatitude = 11.646338452790836;
        
        <?php if( isset($record['lat']) && $record['lat'] ): ?>
          LatitudeLongitudeCoordinates = new google.maps.LatLng(<?= $record['lat'] ?>, <?= $record['lng'] ?>);
        <?php else: ?>
          LatitudeLongitudeCoordinates = new google.maps.LatLng(DefaultLatitude, DefaultLongitude);
        <?php endif; ?>

        var mapOptions = {
            zoom: <?php echo isset($record['lat']) && $record['lat'] ? 18 : 6?>,
            center: LatitudeLongitudeCoordinates,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById('google_map_canvas'), mapOptions);
        var infowindow = new google.maps.InfoWindow();
        var bounds = new google.maps.LatLngBounds();

        var marker = new google.maps.Marker({
              draggable: true, position: LatitudeLongitudeCoordinates, map: map
          });

        google.maps.event.addListener(marker, 'drag', function(event){
           document.getElementById("latitude").value = event.latLng.lat();
           document.getElementById("longitude").value = event.latLng.lng();
    });

        var listener = google.maps.event.addListener(map, "idle", function () {
            map.setZoom(<?php echo isset($record['lat']) && $record['lat'] ? 18 : 6?>);
            google.maps.event.removeListener(listener);
        });
    });
</script>
