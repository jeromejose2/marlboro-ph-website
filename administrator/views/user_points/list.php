<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3> <?php echo $header_text; ?> <span class="badge badge-important"><?php echo (int)$total_rows; ?></span></h3>

               <div class="actions">
                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/export'.$query_strings; ?>" class="btn btn-primary">Export</a>   
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                          <th>Person ID</th>
                          <th>Name</th>
                          <!-- <th>Points</th> -->
                          <!-- <th>Starting Points</th> -->
                          <th>Current Points</th>
                          <th></th>                        
                         
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>">
                     <tr>
                        <th></th>
                        <th><input type="text" name="name" class="form-control" value="<?php echo $this->input->get('name'); ?>"></th>
                        <!-- <th></th> -->
                       
                        <!-- <th></th> -->
                        <th></th>
                      <th>From:<input name="from_date_earned" class="form-control from" value="<?php echo $this->input->get('from_date_earned'); ?>" /><br> 
                            To:<input name="to_date_earned" class="form-control to" value="<?php echo $this->input->get('to_date_earned'); ?>" /></th>                       
                        <th><button type="submit" class="btn btn-primary">Go</button></th>
                       
                     </tr>
                  </form>
                  <?php if($rows->num_rows()){

                            $edit_url = SITE_URL.'/'.$this->uri->segment(1).'/edit/';
                            $query_strings = $query_strings!='?' ? $query_strings.'&' : $query_strings;

                            foreach($rows->result() as $v){ 
                               $temp = '' ;
                               $temp = $query_strings.'registrant_id='.$v->registrant_id;  ?>
                              <tr>
                                  <td><?php echo $v->person_id ?></td>
                                  <td><a href="#" onclick="showRegistrantDetails(<?php echo $v->registrant_id ?>)"><?=ucwords(strtolower($v->first_name.' '.$v->third_name))?></a></td>
                                  <!-- <td><a href="<?=SITE_URL?>/user_points/view_points<?=$temp?>" class="view"><?php echo $v->points; ?></a></td> -->
                                  <!-- <td><?php echo $v->starting_points; ?></td> -->
                                  <td><?php echo $v->total_points; ?></td>
                                  <!-- <td><a href="<?=SITE_URL?>/user_points/view_points_new<?=$temp?>" class="view"><?php echo $v->total_points; ?></a></td> -->
                                  <td></td>                                 
                              </tr>
                  <?php     } 

                  }else{ ?>
                          <tr><td colspan="5">No records found.</td></tr>
                  <?php  } ?>
                  
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->

     <div id="view-content" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>
            </div>
          </div> 
      </div>

     <script>    

     $('.view').on('click',function(e){
          var href = $(this).attr('href');
          $('.modal-content').html('<div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>');
          $('#view-content').modal('show');
          $('.modal-content').load(href);
          e.preventDefault();
     });

     $('#view-content').on('hidden.bs.modal',function(){
        $('.modal-content').html('<img src="<?=BASE_URL?>images/spinner.gif"/></div>');
     });

     </script>