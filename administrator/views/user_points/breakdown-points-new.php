<div class="modal-header">
   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   <h4 class="modal-title"><?=$registrant ? ucwords($registrant->first_name.' '.$registrant->third_name).'\'s Point(s)' : ''?></h4>
  </div>

<div class="modal-body text-center">
   <table class="table table-bordered">
	   	<thead>
	        <tr>
 	              <th>Points</th>
 	              <th>Remarks</th>
	              <th>Date Earned</th>                      
	             
	        </tr>
	    </thead>
	    <tbody>
	        <?php if($rows->num_rows()){
	        		$total_points = 0;
	        		foreach($rows->result() as $v){
	        			$i = 0;
	        			$total_points += $v->points; ?>
	        		<tr>
	        				<td><?=$v->points?></th>
		        			<td><?=str_replace('{{ name }}', '', $v->remarks)?></td>
		        			<td><?=$v->date_earned?></td>
	        		</tr>
	        <?php } ?>
	        		<tr>
	        			<td><?=$registrant->credit_km_points?></td>
	        			<td>Crossover Remaining Points</td>
	        			<td></td>
	        		</tr>
	        		 <tr><td colspan="3" align="left">Total Points : <strong><?=$total_points?></strong></td></tr>
	        		 <!-- <tr><td colspan="3" align="left">Mean Points : <strong><?=$mean?></strong></td></tr>
	        		 <tr><td colspan="3" align="left">Median Points : <strong><?=$median?></strong></td></tr>
	        		 <tr><td colspan="3" align="left">Max Points : <strong><?=$max?></strong></td></tr> -->
	        <?php }else{ ?>
	        	<tr><td colspan="3">No results found.</th></tr>
	    	<?php } ?>
	    </tbody>
	</table>
</div>