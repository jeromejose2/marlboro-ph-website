<!--start main content -->
<?php $this->load->view('editor.php') ?>
 <div class="container main-content">
			<div class="page-header">
					 <h3>Edit Brand History</h3>
		   <div class="actions">
			  <a href="<?= SITE_URL ?>/brand_history" class="btn btn-primary">Back</a>
		   </div>
			</div>
			<?php if (!$error): ?>
			<div class="alert alert-danger" style="display:none;"></div>
			<?php else: ?>
			<div class="alert alert-danger"><?= $error ?></div>
			<?php endif ?>
			 <form class="form-horizontal" role="form" method="post" action="<?= SITE_URL ?>/brand_history/edit?id=<?= $this->input->get('id') ?>" onsubmit="return submitFrm()" enctype="multipart/form-data">
					 <div class="form-group">
								<label class="col-md-1 control-label">Title</label>
								<div class="col-md-4" id="title-input">
									<?php if ($history->brand_history_title_type == 1): ?>
										 <input type="text" id="title" name="title" value="<?= $history->brand_history_title ?>" data-name="title" class="required form-control">
									<?php else: ?>
										<input type="file" id="title" name="brand_history_title" data-name="title photo" data-filename="<?= $history->brand_history_title ?>" class="form-control">
									<?php endif ?>										 
								</div>
					 </div>
					 <div class="form-group <?= $history->brand_history_title_type == 1 ? 'hide' : '' ?>" id="brand-history-title-preview">
								<label class="col-md-1 control-label">Title Preview</label>
								<div class="col-md-4" id="brand-history-preview-img">
									<img src="<?= $history->brand_history_title_type == 1 ? BASE_URL.DEFAULT_IMAGE : BASE_URL.'uploads/brand_history/'.$history->brand_history_title ?>" class="img-thumbnail">
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Year</label>
								<div class="col-md-4">
										 <input type="text" id="year" name="year" value="<?= $history->brand_history_year ?>" data-name="year" class="required form-control" maxlength="4">
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Year Description</label>
								<div class="col-md-4">
										 <input type="text" id="year-desc" name="year_desc" value="<?= $history->brand_history_year_desc ?>" class="form-control">
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Description</label>
								<div class="col-md-4">
										 <textarea name="description" data-name="description" class="form-control"><?= $history->brand_history_description ?></textarea>
								</div>
					 </div>
					 <div class="form-group">
					 	<label class="col-md-1 control-label">Explore Label</label>
					 	<div class="col-md-4">
					 		<input type="text" class="form-control" name="brand_history_explore_copy" value="<?= $history->brand_history_explore_copy ?>">
					 	</div>
					 </div>
					 <div class="form-group">
					 	<label class="col-md-1 control-label">Explore Link</label>
					 	<div class="col-md-4">
					 		<input type="text" class="form-control" name="brand_history_explore_link" value="<?= $history->brand_history_explore_link ?>">
					 	</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Media</label>
								<div class="col-md-4">
									<input type="file" id="brand-history-media" data-name="media" name="brand_history_upload" value="" class="form-control">
									<br>
                     				<strong>Width: 375px. Height: 250px.</strong>
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Media Preview</label>
								<div class="col-md-4" id="brand-history-media-preview">
									<?php if ($history->brand_history_type == 1): ?>
									<img src="<?= BASE_URL ?>uploads/brand_history/483_262_<?= $history->brand_history_media ?>" class="img-thumbnail">
									<?php elseif ($history->brand_history_type == 2): ?>
									<video width="483" height="262" controls>
										<source src="<?= BASE_URL ?>uploads/brand_history/<?= $history->brand_history_media ?>" type="video/mp4">
									Your browser does not support the video tag.
									</video>
									<?php endif ?>
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Status</label>
								<div class="col-md-4">
										<select class="form-control" name="status">
											<option <?= (string) $history->brand_history_status == '0' ? 'selected="selected"' : '' ?> value="0">Unpublished</option>
											<option <?= (string) $history->brand_history_status == '1' ? 'selected="selected"' : '' ?> value="1">Published</option>   
										</select>
								</div>
					 </div>
					 <div class="form-group">
						<div class="col-md-offset-1 col-md-4">
						 <button type="submit" class="btn btn-primary">Submit</button>
					 </div>
					</div>
					<!-- <input type="hidden" id="remove-files-index" name="remove-file" value=""> -->
			</form>
			
 </div>

 <script>
 	$("#year").bind('keypress', function (e) {
        return !(e.which != 8 && e.which != 0 &&
                (e.which < 48 || e.which > 57) && e.which != 46);
    });
 	$("#brand-history-media").change( function() {
 		var file = $(this).prop("files")[0];
 		var oFReader = new FileReader();
 		if (file.type.indexOf("image") == 0) {
			oFReader.onload = function (oFREvent) {
				$("#brand-history-media-preview").html("<img style='width: 483px; height: 262px;' class='img-thumbnail' src='" + oFREvent.target.result + "'>");
			};
		}
 		// } else {
 		// 	oFReader.onload = function (oFREvent) {
			// 	$("#brand-history-media-preview").html("<video src='" + oFREvent.target.result + "'></video>");
			// };
 		// }
 		oFReader.readAsDataURL(file);
 	});
 	$("body").on("change", "#title[type='file']", function() {
 		var file = $(this).prop("files")[0];
 		var oFReader = new FileReader();
 		if (file.type.indexOf("image") == 0) {
			oFReader.onload = function (oFREvent) {
				$("#brand-history-preview-img").html("<img class='img-thumbnail' src='" + oFREvent.target.result + "'>");
			};
 		}
 		oFReader.readAsDataURL(file);
 	});
 	var titleElem = $("#title");
 	var titleParent = titleElem.parent();
 	var origType = titleElem.prop("type");
 	var origValue = '';
 	if (origType == "text") {
 		origValue = titleElem.val();
 	}

 	$(".title-type").change( function() {
 		var value = $(this).val();
 		if (value == 2) {
 			titleParent.html("<input type='file' id='title' name='brand_history_title' data-name='title photo' class='" + (origType == "text" ? 'required' : '' ) + " form-control'>");
 			$("#brand-history-title-preview").removeClass("hide");
 		} else if (value == 1) {
 			titleParent.html("<input type='text' id='title' name='title' value='" + origValue + "' data-name='title' class='required form-control'>");
 			$("#brand-history-title-preview").addClass("hide");
 		}
 	});
 </script>