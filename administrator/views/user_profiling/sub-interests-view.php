<div class="modal-header">
   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   <h4 class="modal-title">Sub Interest</h4>
  </div>

<div class="modal-body">
   <table class="table table-bordered">
	   	<thead>
	        <tr>
   	              <th><?=$row ? ucwords($row->category_name) : ''?></th>
  	              <th>Count</th>
 	             
	        </tr>
	    </thead>
	    <tbody>
	        <?php if($items){
	        		foreach($items as $v){ 
	        			 ?>
	        		<tr>
 	        			<td><?=$v['category_name']?></td>
	        			<td><a href="<?=SITE_URL?>/profiling?interest=<?=$v['interest']?>&sub_interest=<?=$v['sub_interest']?>&from_date_added=<?=$this->input->get('from_date_added')?>&to_date_added=<?=$this->input->get('to_date_added')?>" target="_blank"><?=$v['user_count']?></td> 	        			
 	        		</tr>
	        <?php } 
	    }else{ ?>
	        	<tr><td colspan="3">No results found.</th></tr>
	    	<?php } ?>
	    </tbody>
	</table>
</div>