<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3>Dashboard</h3>
      </div>
      <div class="row">
          <div class="col-md-8">
              <div class="col-md-6">
                <h4>Quick Links</h4>
                <table class="table table-bordered">
                   <tbody>
                        <tr>
                            <td colspan="2"><strong>User Management</strong></td>
                        </tr>
                        <tr>
                             <td><a href="<?php echo SITE_URL ?>/registrant">User Profile</a></td>
                             <td><?php echo $stats['all']  ?></td>
                        </tr>
                        <tr>
                             <td><a href="<?php echo SITE_URL ?>/profile">User Profile - Photos</a></td>
                             <td><?php echo $profiles; ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><strong>Content Management</strong></td>
                        </tr>
                        <tr>
                             <td><a href="<?php echo SITE_URL ?>/statement">Maybe Statement</a></td>
                             <td><?php echo $maybe_statements ?></td>
                        </tr>
                         <tr>
                            <td colspan="2"><strong>Report</strong></td>
                        </tr>
                        <tr>
                             <td><a href="<?php echo SITE_URL ?>/activity_log">Admin Activity</a></td>
                             <td><?php echo $audit_trails ?></td>
                        </tr>
                   </tbody>
                 </table>
                 
              </div>
              <div class="col-md-6">
                <h4>User Management</h4>
                <table class="table table-bordered">
                   <tbody>
                         <tr>
                             <td>Total Number of Registrants</td>
                             <td><?php echo $stats['all'] ?></td>
                        </tr>
                        <?php foreach($status as $k => $v): ?>
                        <tr>
                             <td><?php echo $v ?></td>
                             <td><?php echo isset($stats[$k]) ? $stats[$k] : 0 ?> <?= isset($arclight[$k]) ? '( '.$arclight[$k].' In Process )' : '' ?></td>
                        </tr>
                        <?php endforeach; ?>
                   </tbody>
              </table>
              </div>
              
              <div class="col-md-6">
             	<h4>Moderator</h4>
                <table class="table table-bordered">
                   <tbody>
                        <tr>
                            <td colspan="2"><strong>About</strong></td>
                        </tr>
                        <tr>
                             <td>Approved</td>
                             <td><?=$about_approved?></td>
                        </tr>
                        <tr>
                             <td>Disapproved</td>
                             <td><?=$about_disapproved?></td>
                        </tr>
                        <tr>
                             <td>Pending</td>
                             <td><?=$about_pending?></td>
                        </tr>
                   </tbody>
                 </table>
             </div> 	
              <div class="col-md-6">
             	<h4>&nbsp;</h4>
                <table class="table table-bordered">
                   <tbody>
                        <tr>
                            <td colspan="2"><strong>Move Forward</strong></td>
                        </tr>
                        <tr>
                             <td>Approved</td>
                             <td><?php echo isset($movefwd[1]) ? $movefwd[1] : 0 ?></td>
                        </tr>
                        <tr>
                             <td>Dispproved</td>
                             <td><?php echo isset($movefwd[2]) ? $movefwd[2] : 0 ?></td>
                        </tr>
                        <tr>
                             <td>Pending</td>
                             <td><?php echo isset($movefwd[0]) ? $movefwd[0] : 0 ?></td>
                        </tr>
                   </tbody>
                 </table>
             </div> 	
              
        	</div>
            <div class="col-md-4">
            	<div class="col-md-12">
                <h4>Recent Admin Activity</h4>
                <table class="table table-bordered">
                   <thead>
                        <tr>
                             <th>#</th>
                             <th>User</th>
                             <th>Activity</th>
                             <th>Date</th>
                        </tr>
                   </thead>
                   <tbody>
                        <?php if($audit_trail): 
                        foreach($audit_trail as $k => $v): ?>
                        <tr>
                             <td><?php echo $k + 1 ?></td>
                             <td><?php echo $v['cms_user_name'] ?></td>
                             <td><?php echo $v['description'] ?> <?php echo isset($v['record_name']) ? '<a href="#" onclick="showLogDetails(' . $v['audit_trail_id'] . ')">Show Details</a>' : '' ?></td>
                             <td><?php echo date('Y-m-d H:i:s', strtotime($v['timestamp'])) ?></td>
                             <td class="modal">
                                 <div class="modal" id="log-details-<?php echo $v['audit_trail_id'] ?>">
                                    <?php $changes = unserialize($v['field_changes']);?>
                                    <strong>Record ID:</strong> <?php echo $v['record_id'] ?><br>
                                    <strong>Name:</strong> <?php echo $v['record_name'] ?><br>
                                    <?php if($v['message']): ?><strong>Message:</strong> <?php echo $v['message'] ?><br><?php endif; ?><br>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Field</th>
                                                <th>Old Value</th>
                                                <th>New Value</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if($changes['old']): 
                                                foreach($changes['old'] as $k => $v): ?>
                                                <tr>
                                                    <td><?php echo $k  ?></td>
                                                    <td><?php echo $changes['old'][$k]  ?></td>
                                                    <td><?php echo $changes['new'][$k]  ?></td>
                                                </tr>
                                            <?php 
                                            endforeach;
                                            else:
                                            echo '<tr><td colspan="3">No changes found</td></tr>';
                                            endif; ?>	
                                        </tbody>
                                    </table>
                                 </div>
                             </td>
                        </tr>
                        <?php 
                        endforeach;
                        endif; ?>
                   </tbody>
              </table>
              <a href="<?php echo SITE_URL ?>/activity_log">View All</a><br ><br >
              </div>
            </div>
        </div>
        
        <div class="row">
       		 
        </div>
 </div>
 <!--end main content -->
 
 <script>
 	function showLogDetails(id) {
		bootbox.dialog({'message': $('#log-details-' + id).html(),
						'title': 'Admin Activity Log Details'});
	}
 </script>