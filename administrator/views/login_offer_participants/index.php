<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Login Offer <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <?php //if($add): ?>
                    <!-- <a href="<?php echo SITE_URL ?>/login_offer/add" class="btn btn-primary">Add Offer</a> -->
                    <a target="_blank" href="<?php echo SITE_URL ?>/export/login_offer_confirm/?<?=@http_build_query($this->input->get())?>" class="btn btn-primary">Export</a>
                    <?php //endif; ?>
               </div>
               <br />
          </div>

        <?php if(@$success){
        ?>
        <div class='alert alert-success'>
          <?= $success ?>
        </div>
        <?php
        }
        ?>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Person ID</th>
                         <th>First Name</th>
                         <th>Last Name</th>
                         <th>Previous Login Date</th>
                         <th>Last Login Date</th>
                         <th>Promo</th>
                         <th>Points Awarded</th>
                         <th>Participated in Perks?</th>
                         <th>Action</th>
                    </tr>
               </thead>
               <tbody>
                  <form method="GET">
                    <tr>
                          <td></td>
                          <td><input type='text' name='person_id' class='form-control' value="<?= (isset($_GET['person_id']) and $_GET['person_id']!="") ? $_GET['person_id'] : '' ?>" /></td>
                          <td><input type='text' name='first_name' class='form-control' value="<?= (isset($_GET['first_name']) and $_GET['first_name']!="") ? $_GET['first_name'] : '' ?>" /></td>
                          <td><input type='text' name='last_name' class='form-control' value="<?= (isset($_GET['last_name']) and $_GET['last_name']!="") ? $_GET['last_name'] : '' ?>" /></td>
                          <td><input type='text' name='previous_login_date' class='form-control from' value="<?= (isset($_GET['previous_login_date']) and $_GET['previous_login_date']!="") ? $_GET['previous_login_date'] : '' ?>" /></td>
                          <td><input type='text' name='last_login_date' class='from form-control' value="<?= (isset($_GET['last_login_date']) and $_GET['last_login_date']!="") ? $_GET['last_login_date'] : '' ?>" /></td>
                          <td><input type='text' name='promo' class='form-control' value="<?= (isset($_GET['promo']) and $_GET['promo']!="") ? $_GET['promo'] : '' ?>" /></td>
                          <td><input type='text' name='points_awarded' class='form-control' value="<?= (isset($_GET['points_awarded']) and $_GET['points_awarded']!="") ? $_GET['points_awarded'] : '' ?>" /></td>
                          <td>
                          </td>
                          <td>
                            <input type='submit' value="Go" class="btn btn-primary" />
                          </td>
                    </tr>
                  </form>

               		
                    <?php 
                    if($content){
                      ?>
                       <form action="<?php echo SITE_URL ?>/login_offer">
                      <?php  $ctr=0; foreach($content as $confirm) { ?>
                      <tr>
                        <td><?= ++$ctr; ?></td>
                        <td><?= $confirm->person_id ?></td>
                        <td><?= $confirm->first_name ?></td>
                        <td><?= $confirm->third_name ?></td>
                        <td><?= $confirm->date_confirmed ?></td>
                        <td><?= $confirm->last_date_login ?></td>
                        <td><?= $confirm->offer_title ?></td>
                        <td><?= $confirm->points ?></td>
                        <td>
                          <?php
                            if(in_array($confirm->rid, $reg_ids)){
                            ?>
                            <a class='btn btn-primary' href="<?php echo SITE_URL ?>/user_buys?name=<?php echo str_replace(" ", "+", $confirm->first_name." ".$confirm->third_name) ?>&buy_item_id=&specs=&from_buy=&to_buy=&search=1">Yes</a>
                            <?php
                            }else{
                            ?>
                            <span style='color: #333;
    background-color: #fff;
    border: 1px solid #CCC;
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.428571429;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    background-image: none;
    border-radius: 4px;
    -webkit-user-select: none;'>No</span>
                            <?php
                            }
                          ?>
                        </td>
                        <td></td>
                      </tr>
                    <?php } ?>
                    </form>
                      <?php
                    }else{
                      ?>
                      <tr>
                        <td colspan="9">
                          <b>No Result Found</b>
                        </td>
                      </tr>
                      <?php
                    }
                    ?>
               </tbody>
          </table>
          <ul class="pagination pagination-sm pull-right">
          </ul>

     </div>
     <!--end main content -->