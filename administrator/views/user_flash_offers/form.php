
<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3><?php echo $header_text; ?></h3>
      </div> 
       <form action="<?=SITE_URL?>/user_flash_offers/edit/<?=$id?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">

        <div class="form-group">
                <label class="col-sm-2 control-label">Name</label>
                <div class="col-sm-4"> <?php echo $row->first_name.' '.$row->third_name; ?>

                </div>
           </div>


           <div class="form-group">
                <label class="col-sm-2 control-label">Prize Name</label>
                <div class="col-sm-4">
                     <?php echo $row->prize_name;  ?> 
                </div>
           </div> 
           

           <div class="form-group">
                <label class="col-sm-2 control-label">Prize Description</label>
                <div class="col-sm-4">
                   <?php echo $row->description; ?>
                 </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Prize Media</label>
                <div class="col-sm-4">
                <?php if($row->media_type=='Photo' && file_exists('uploads/prize/'.$row->media)){ ?>
                      <img src="<?=BASE_URL.'uploads/prize/'.$row->media?>"/>
                <?php } ?>
                <?php if($row->media_type=='Video' && file_exists('uploads/prize/'.$row->media)){ 
                        $video_mime_type = get_mime_by_extension('uploads/prize/'.$row->media);?>
                        <video width="100%" height="100%" controls>
                            <source src="<?=BASE_URL.'uploads/prize/'.$row->media?>" type="<?=$video_mime_type?>">
                          Your browser does not support the video tag. 
                        </video>
                 <?php } ?>
                </div>
           </div>

           <?php if($row->send_type=='Redemption'){ ?>

           <div class="form-group">
                <label class="col-sm-2 control-label">Redemption Address</label>
                <div class="col-sm-4">
                     <?php echo nl2br($row->redemption_address); ?> 
                </div>
           </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Redemption Instructions</label>
                <div class="col-sm-4">
                      <?php echo nl2br($row->redemption_instruction); ?>
                </div>
           </div>

           <?php } ?>

           <div class="form-group">
                <label class="col-sm-2 control-label">Prize Status</label>
                <div class="col-sm-4">
                	<select class="form-control" name="prize_status">
                     	 <option <?php echo $row->prize_status == 0 ? 'selected' : ''; ?> value="0">Pending</option>
                      <?php if($row->send_type=='Delivery'){ ?>
                            <option <?php echo $row->prize_status == 1 ? 'selected' : ''; ?> value="1">Delivered</option>
                      <?php }else{ ?>
                            <option <?php echo $row->prize_status == 2 ? 'selected' : ''; ?> value="2">Redeemed</option>
                      <?php }  ?>
                  </select>
                </div>
           </div> 
           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>" class="pull-left"><< Back</a>
                     <button type="submit" class="btn btn-primary pull-right">Submit</button>

                </div>
           </div>
           
           
      </form>
      
 </div>
 <!--end main content -->
 