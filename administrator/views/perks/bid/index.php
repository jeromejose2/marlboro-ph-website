
<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Bid Items <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL ?>/bid_items/add" class="btn btn-primary">Add Bid Item</a>
                    <?php endif; ?>
                    <a href="<?php echo SITE_URL ?>/bid_items/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>   
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Thumbnail</th>
                         <!-- <th>Category</th> -->
                         <th>Start</th>
                         <!-- <th>Slots</th> -->
                         <th>End</th>
                         <th>Status</th>
                         <th>Platform Restriction</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/bid_items">
                     <tr>
                        <td></td>
                        <td><input name="bid_item_name" class="form-control" value="<?php echo isset($_GET['bid_item_name']) ? $_GET['bid_item_name'] : '' ?>" /></td>
                        <td></td>
                        <!-- <td>
                          <select name="category_id" class="form-control">
                            <option></option>
                            <?php if($main_categories): ?>
                            <?php foreach($main_categories as $k => $v): ?>
                              <?php if($v['origin_id'] == MOVE_FWD): ?>
                              <option <?php echo isset($_GET['category_id']) && $_GET['category_id'] == $v['category_id'] ? 'selected' : '' ?> value="<?php echo $v['category_id'] ?>"><?php echo $v['category_name'] ?></option>
                            <?php endif; ?>
                          <?php endforeach; ?>
                           <?php endif; ?>
                          </select>
                         </td> -->
                         <td><input name="start_date" class="from form-control" value="<?php echo isset($_GET['start_date']) ? $_GET['start_date'] : '' ?>" /></td>
                         <!-- <td><input name="slots" class="form-control" value="<?php echo isset($_GET['slots']) ? $_GET['slots'] : '' ?>" /></td> -->
                         <td><input name="end_date" class="from form-control" value="<?php echo isset($_GET['end_date']) ? $_GET['end_date'] : '' ?>" /></td>
                         <td>
                          <select name="status" class="form-control">
                            <option></option>
                            <option value="1" <?php echo isset($_GET['status']) && $_GET['status'] == '1' ? 'selected' : '' ?>>Active</option>
                            <option value="0" <?php echo isset($_GET['status']) && $_GET['status'] == '0' ? 'selected' : '' ?>>Inactive</option>
                          </select>
                         </td>
                         <td>
                            <select name="platform_restriction" class="form-control">
                              <option></option>
                              <option value="<?php echo WEB_ONLY ?>" <?php echo isset($_GET['platform_restriction']) && $_GET['platform_restriction'] == WEB_ONLY ? 'selected' : '' ?>>Web</option>
                              <option value="<?php echo MOBILE_ONLY ?>" <?php echo isset($_GET['platform_restriction']) && $_GET['platform_restriction'] == MOBILE_ONLY ? 'selected' : '' ?>>Mobile</option>
                              <option value="<?php echo BOTH_PLATFORM ?>" <?php echo isset($_GET['platform_restriction']) && $_GET['platform_restriction'] == BOTH_PLATFORM ? 'selected' : '' ?>>Mobile App & Website</option>
                            </select>
                         </td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
                    <?php if($categories): 
					foreach($categories as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['bid_item_name'] ?></td>
                         <td><img src="<?php echo file_exists('uploads/bid/thumb_' . $v['bid_item_image']) ? BASE_URL . 'uploads/bid/thumb_' . $v['bid_item_image'] : BASE_URL . 'uploads/bid/' . $v['bid_item_image'] ?>?uniqid=<?php echo uniqid() ?>" width="150" /></td>
                         <!-- <td><?php echo $main_categories[$v['category_id']]['category_name'] ?></td> -->
                         <!-- <td><?php echo $v['slots'] ?></td> -->
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['start_date']))?></td>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['end_date'])) ?></td>
                         <td><?php echo $v['status'] == '1' ? 'Active' : 'Inactive' ?></td>
                         <td>
                            <?php if($v['platform_restriction'] == WEB_ONLY): ?>
                              Website
                            <?php endif; if($v['platform_restriction'] == MOBILE_ONLY): ?>
                              Mobile App
                            <?php endif; if($v['platform_restriction'] == BOTH_PLATFORM): ?>
                              Mobile App & Website
                            <?php endif; ?>
                         </td>
                         <td>
                            <?php if($edit): ?>
                            <a href="<?php echo SITE_URL ?>/bid_items/edit/<?php echo $v['bid_item_id'] ?>" class="btn btn-primary">Edit</a>
                            <?php endif; ?>
                            <?php if($delete): ?>
                            <a href="<?php echo SITE_URL ?>/bid_items/delete/<?php echo $v['bid_item_id'] ?>/<?php echo md5($v['bid_item_id'] . ' ' . $this->config->item('encryption_key')) ?>" onclick="return confirmDeletion(this, 'bid item')" class="btn btn-danger">Delete</a>
                            <?php endif; ?>
                         </td>
                    </tr>
                    <?php 
					endforeach;
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->