<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>User Bids <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/user_bids/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Person ID</th>
                         <th>Name</th>
                         <th>Item</th>
                         <th>Bid</th>
                         <th>Current Points</th>
                         <th>Bid Start</th>
                         <th>Bid End</th>
                         <th>Bid Date</th>
                         <?php if($edit || $delete) : ?>
                         <th>Operation</th>
                         <?php endif; ?>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/user_bids">
                         <tr>
                             <td></td>
                             <td></td>
                             <td><input name="name" class="form-control" value="<?php echo isset($_GET['name']) ? $_GET['name'] : '' ?>" /></td>
                             <td>
                                <select name="bid_item_id" class="form-control">
                                    <option></option>
                                    <?php if($items) {
                                      foreach($items as $k => $v) {
                                        $selected = isset($_GET['bid_item_id']) && $_GET['bid_item_id'] == $v['bid_item_id'] ? 'selected' : '';
                                        echo '<option ' . $selected . ' value="' . $v['bid_item_id'] . '">' . $v['bid_item_name'] . '</option>';
                                      }
                                    } ?>
                                </select>
                             </td>
                             <td><input name="bid" class="form-control" value="<?php echo isset($_GET['bid']) ? $_GET['bid'] : '' ?>" /></td>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td>
                             
                              <div class="row">
                                <div class="col-md-4">
                                     <input name="from_bid" placeholder="From" class="form-control from" value="<?php echo isset($_GET['from_bid']) ? $_GET['from_bid'] : '' ?>" />
                                </div>
                                <div class="col-md-4">
                                     <input name="to_bid" placeholder="To" class="to form-control" value="<?php echo isset($_GET['to_bid']) ? $_GET['to_bid'] : '' ?>" />
                                </div>
                              </div>
                            </td>
                             <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                        </tr>
                        </form>
                        
                    <?php if($users): 
          					foreach($users as $k => $v): 
                     
                     ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['person_id'] ?></td>
                         <td><a href="#" onclick="showRegistrantDetails(<?php echo $v['registrant_id'] ?>)"><?php echo $v['first_name'] . ' ' . $v['third_name'] ?></a></td>
                         <td><?php echo $v['bid_item_name'] ?></td>
                         <td><?php echo number_format($v['bid']) ?></td>
                         <td><?php echo number_format($v['total_points']) ?></td>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['start_date'])) ?></td>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['end_date'])) ?></td>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['bid_date']))  ?></td>
                         <td>
                            <?php if($v['is_winner'] != 1 && $edit): ?>
                            <a href="<?php echo SITE_URL ?>/user_bids/set_winner/<?php echo $v['bid_id'] ?>/<?php echo md5($v['bid_id'] . ' ' . $this->config->item('encryption_key')) ?>" class="btn btn-success" onclick="return confirmAction(this, 'set', 'user as bid winner' )">Set as winner</a>
                            <?php endif; ?>
                            <?php if($v['is_winner'] != 0 && $edit): ?>
                            <a href="<?php echo SITE_URL ?>/user_bids/unset_winner/<?php echo $v['bid_id'] ?>/<?php echo md5($v['bid_id'] . ' ' . $this->config->item('encryption_key')) ?>" class="btn btn-warning" onclick="return confirmAction(this, 'unset', 'user as bid winner' )">Unset as winner</a>
                            <?php endif; ?> 
                         </td>
                     </tr>
                    <?php 
					endforeach;
					else:
						echo '<tr><td colspan="5">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->

     <script>
     function confirm() {
      bootbox.confirm('Are you sure you want approve all selected comments?', function(result) {
          if(result == 1) {
            $.post('<?php echo SITE_URL ?>/comment/approve_all', {ids: ids}, function() {
              window.location.reload();
            })
            return true;
          }
        });

     }
     </script>