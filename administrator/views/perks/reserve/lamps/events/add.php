<noscript>
  <style>
  .col-xs-4 {
    display: none;
  }
  </style>
</noscript>
<script src="<?php echo BASE_URL ?>admin_assets/js/bootstrap-tagsinput.js"></script>
<!--start main content -->
 <div class="container main-content" ng-app="fileUpload" ng-controller="MyCtrl">
      <div class="page-header">
           <h3>LAMP Events</h3>
      </div>
       <div class="alert alert-danger" <?php if(!$error) echo 'style="display:none;"' ?>><?php echo $error; ?></div>
       <form class="form-horizontal" role="form" method="post" id="add-form-product-info" action="<?= $action ?>" onsubmit="return submitFrm()" enctype="multipart/form-data">
           <div class="form-group">
                <label class="col-sm-2 control-label">Event Name</label>
                <div class="col-sm-4">
                     <input type="text" name="event_title" value="<?php echo isset($record['event_title']) ? $record['event_title'] : '' ?>" data-name="promotion title" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">LAMP Name</label>
                <div class="col-sm-4">
                    <select class="required form-control" name="lamp_id" data-name="lamp name">
                      <option></option>
                      <?php foreach($lamps as $k => $v): ?>
                          <option value="<?= $v['lamp_id'] ?>" <?= isset($record['lamp_id']) && $record['lamp_id'] == $v['lamp_id'] ? 'selected' : '' ?>><?= $v['lamp_name'] ?></option>
                      <?php endforeach; ?>
                    </select>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Status</label>
                <div class="col-sm-4">
                    <input type="radio" value="1" class="prop-check" name="status" <?= isset($record['status']) && $record['status'] == 1 ? 'checked' : '' ?>> Yes
                    <input type="radio" value="0" class="prop-check" name="status" <?= isset($record['status']) && $record['status'] == 0 || !isset($record['status']) ? 'checked' : '' ?>> No
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                      <a href="<?= SITE_URL ?>/lamp_events" class="btn btn-primary">Back</a>
                     <button type="submit" class="btn btn-primary" name="submit" value="1">Submit</button>
                </div>
           </div>
      </form>
      
 </div>
 <!--end main content -->
 