<noscript>
  <style>
  .col-xs-4 {
    display: none;
  }
  </style>
</noscript>
<!--start main content -->
<!-- <link type="text/css" rel="stylesheet" href="<?= BASE_URL ?>admin_assets/css/angular-js/common.css"> -->
<script src="<?= BASE_URL ?>admin_assets/js/angular-js/angular-file-upload-shim.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.js"></script>    
<script src="<?= BASE_URL ?>admin_assets/js/angular-js/angular-file-upload.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjxtjNyjATUWGLx-1v0zehvr0bkc0ACnE&sensor=false">></script>
<script type="text/javascript">
var uploadURL = '<?= SITE_URL ?>/buy_items/upload_media/';
</script>
<script src="<?= BASE_URL ?>admin_assets/js/angular-js/buy-items.js"></script> 

<?php $this->load->view('editor.php') ?>
 <div class="container main-content" ng-app="fileUpload" ng-controller="MyCtrl">
      <div class="page-header">
           <h3>Buy Item</h3>
       <div class="actions">
        <!-- <a href="<?= SITE_URL ?>/product_info" class="btn btn-primary">Back</a> -->
       </div>
      </div>
      <?php if (!$error): ?>
      <div class="alert alert-danger" style="display:none;"></div>
      <?php else: ?>
      <div class="alert alert-danger"><?= $error ?></div>
      <?php endif ?>
       <form class="form-horizontal" role="form" method="post" id="add-form-product-info" action="<?= $action ?>" onsubmit="return submitFrm()" enctype="multipart/form-data">
           <div class="form-group">
                <label class="col-md-1 control-label">Name</label>
                <div class="col-md-4">
                     <input type="text" id="title" name="buy_item_name" value="<?php echo isset($record['buy_item_name']) ? $record['buy_item_name'] : '' ?>" data-name="buy item name" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-md-1 control-label">Description</label>
                <div class="col-md-4">
                     <textarea name="description" data-name="description" class="form-control"><?php echo isset($record['description']) ? $record['description'] : '' ?></textarea>
                </div>
           </div>
           <div class="form-group">
                <label class="col-md-1 control-label">Details [App]</label>
                <div class="col-md-4">
                     <textarea name="details" class="mceNoEditor form-control"><?php echo isset($record['details']) ? $record['details'] : '' ?></textarea>
                </div>
           </div>
           <div class="form-group">
                <label class="col-md-1 control-label">Image</label>
                <div class="col-md-4">
                     <input type="file" id="product-info-photo" data-name="image" name="photo" value="" class="form-control" onchange=""><br>
                     <strong>Width: 400px. Height: 400px.</strong>

                     <!-- <strong>Width: 193px. Height: 225px.</strong> -->
                </div>
           </div>
           <div class="form-group">
                <label class="col-md-1 control-label">Image Preview</label>
                <div class="col-md-4">
                   <img src="<?php echo isset($record['buy_item_image']) ? BASE_URL . 'uploads/buy/'. $record['buy_item_image'] : BASE_URL.DEFAULT_IMAGE ?>" style="width: 193px;" id="product-info-preview" class="img-thumbnail preview"  />
                </div>
           </div>
           <div class="form-group">
                <label class="col-md-1 control-label">Points Value</label>
                <div class="col-md-4">
                     <input type="text" id="credit_value" name="credit_value"  onkeydown="return checkDigit(event)" value="<?php echo isset($record['credit_value']) ? $record['credit_value'] : '' ?>" data-name="credit value" class="required form-control">
                </div>
           </div>
           <div class="form-group without-property">
                <label class="col-md-1 control-label">Stock</label>
                <div class="col-md-4">
                     <input type="text" id="stock" name="stock"  onkeydown="return checkDigit(event)" value="<?php echo isset($record['stock']) ? $record['stock'] : '' ?>" data-name="stock" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-md-1 control-label">Status</label>
                <div class="col-md-4">
                    <select class="form-control" name="status">
                      <option value="0" <?php echo isset($record['status']) && $record['status'] == 0 ? 'selected' : '' ?>>Unpublished</option>
                      <option value="1" <?php echo isset($record['status']) && $record['status'] == 1 ? 'selected' : '' ?>>Published</option>   
                      <option value="2" <?php echo isset($record['status']) && $record['status'] == 2 ? 'selected' : '' ?>>Teaser</option>   
                    </select>
                </div>
           </div>

       <div class="form-group" id="with-btn-add-media">
      <label class="col-md-1 control-label">Media</label>
      <div class="col-md-4">
        <!-- <button type="button" class="btn btn-warning" id="add-media-btn">Add Media</button>&nbsp;&nbsp; -->
        <!-- <button type="button" class="btn btn-warning" id="add-multiple-media-btn">Browse</button>
        <button type="button" class="btn btn-danger" id="clear-uploads">Clear</button> -->
        <input type="file" name="multiple_images[]" class="form-control" ng-file-select="onFileSelect($files)" id="tmp-file-upload" multiple="true">
      </div>
    </div>
      <div class="form-group" ng-show="selectedFiles != null">
        <label class="col-md-1 control-label">Media Preview</label>
        <div class="col-md-8">
          <div class="row">

          <?php if(isset($media)) {
            foreach($media as $k => $v) { ?>
            <div class="col-xs-4 col-md-3 product-media-item-container">
            <div class="thumbnail">
                <img src="<?= BASE_URL ?>uploads/buy/media/150_150_<?= $v['media_content'] ?>">
                <div class="caption">
                  <input value="" type="hidden" > 
                  <p>
                    <a onclick="removeValue(this)" href="javascript:void(0)" class="btn btn-danger" role="button">&times;&nbsp;&nbsp;Remove</a>
                    <input type="hidden" name="media_ids[]" value="<?= $v['media_id'] ?>"> 
                  </p>
                </div>
              </div>
            </div>
            <?php }
          } ?>  
          <div class="col-xs-4 col-md-3" ng-repeat="f in selectedFiles">
            <div class="thumbnail">
              <img ng-show="dataUrls[$index]" ng-src="{{dataUrls[$index]}}">
              <div class="caption">
                <h5>{{f.name}}</h5>
                <input name="media_title[{{$index}}]" type="text" style="width: 100%;" placeholder="Enter title here">
                <input name="media_upload[{{$index}}]" type="hidden" ng-show="uploadResult[$index]" value="{{uploadResult[$index]}}">
                <br><br>
                <p>size: {{f.size}}B - type: {{f.type}}</p>
                <div class="progress progress-striped" ng-show="progress[$index] >= 0">
                <div class="progress-bar"  role="progressbar" aria-valuemin="0" aria-valuenow="{{progress[$index]}}" aria-valuemax="100" style="width: {{progress[$index]}}%">
                  <span class="sr-only">{{progress[$index]}}% Complete</span>
                </div>
              </div>
                <p><a href="javascript:void(0)" class="btn btn-primary" role="button" ng-click="start($index)" ng-show="progress[$index] < 0">Start</a> 
                <a href="javascript:void(0)" class="btn btn-warning" ng-click="abort($index)" ng-show="hasUploader($index) && progress[$index] < 100" role="button">Abort</a> 
              <a href="javascript:void(0)" class="btn btn-danger" ng-click="remove($index)" ng-show="progress[$index] >= 100" role="button">&times;&nbsp;&nbsp;Remove</a></p>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
      <div class="form-group">
          <label class="col-md-1 control-label">Map Location</label>
                <div class="col-md-4">
                  <div id="google_map_canvas" style="width: 480px; height: 480px"></div><br>
                  <input type="text" id="longitude" name="lng" value="<?php echo isset($record['lng']) ? $record['lng'] : '' ?>" data-name="longitude" class="form-control" placeholder="Longitude"><br>
                  <input type="text" id="latitude" name="lat" value="<?php echo isset($record['lat']) ? $record['lat'] : '' ?>" data-name="latitude" class="form-control" placeholder="Latitude">
                </div>
      </div>
      <div class="form-group">
          <label class="col-md-1 control-label">Platform Restriction</label>
          <div class="col-sm-4">
              <select class="required form-control" name="platform_restriction">
                <option <?php echo isset($record['platform_restriction']) && $record['platform_restriction'] == WEB_ONLY ? 'selected' : '' ?> value="<?= WEB_ONLY ?>">Website</option>
                <option <?php echo isset($record['platform_restriction']) && $record['platform_restriction'] == MOBILE_ONLY ? 'selected' : '' ?> value="<?= MOBILE_ONLY ?>">Mobile App</option>   
                <option <?php echo isset($record['platform_restriction']) && $record['platform_restriction'] == BOTH_PLATFORM ? 'selected' : '' ?> value="<?= BOTH_PLATFORM ?>">Mobile App & Website</option>   
              </select>
          </div>
      </div>
      <div class="form-group">
        <label class="col-md-1 control-label">With Property</label>
        <div class="col-md-4">
            <input type="radio" value="1" class="prop-check" name="prop_check"> Yes
            <input type="radio" value="0" class="prop-check" name="prop_check" checked="checked"> No
        </div>
   </div>


      <br ><br >
      <div class="form-group">
      <!-- Nav tabs --> 
          <ul class="nav nav-tabs with-property" style="display:none;">
            <li class="active"><a href="#property1" data-toggle="tab">Property 1</a></li>
            <li><a href="#property2" data-toggle="tab">Property 2</a></li>
            <li><a href="#property-stocks" data-toggle="tab">Stocks</a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content with-property" style="display:none;">
            <div class="tab-pane active" id="property1">
               <br >
               <div class="form-group">
                  <label class="col-md-1 control-label">Name</label>
                  <div class="row">
                    <div class="col-md-4">
                      <input type="text" class="form-control" name="property1" value="<?php echo isset($properties[1]) ? $properties[1][0]['property_name'] : '' ?>">
                    </div>
                  </div><!-- /.row -->
             </div>
             <div class="form-group">
                  <label class="col-md-1 control-label">Values</label>
                  <div class="row">
                    <div class="col-md-4">
                      <button type="button" class="btn btn-primary add-btn" data-propindex="1">+</button>
                    </div>
                  </div><!-- /.row -->
             </div>

             <?php if(isset($properties[1])) {
              foreach ($properties[1] as $key => $value) { ?>
                <div class="form-group">
                  <label class="col-md-1 control-label">&nbsp;</label>
                    <div class="col-md-4">
                      <div class="input-group">
                      <input type="text" class="form-control value" name="value1[]" value="<?php echo $value['value'] ?>">
                      <input type="hidden" class="property-value1-id"  name="property_value1_id[]" value="<?php echo $value['property_value_id'] ?>">
                      <span class="input-group-btn">
                        <button class="btn btn-danger" type="button" onclick="removeValue(this)">x</button>
                      </span>
                    </div>
                  </div>
                </div>
              <?php }
             } ?>
              
            </div>
            <div class="tab-pane" id="property2">
              <br>
              <div class="form-group">
                  <label class="col-md-1 control-label">Name</label>
                  <div class="row">
                    <div class="col-md-4">
                      <input type="text" class="form-control" name="property2" value="<?php echo isset($properties[2]) ? $properties[2][0]['property_name'] : '' ?>">
                    </div>
                  </div><!-- /.row -->
             </div>
             <div class="form-group">
                  <label class="col-md-1 control-label">Values</label>
                  <div class="row">
                    <div class="col-md-4">
                      <button type="button" class="btn btn-primary add-btn" data-propindex="2">+</button>
                    </div>
                  </div><!-- /.row -->
             </div>
             <?php if(isset($properties[2])) {
              foreach ($properties[2] as $key => $value) { ?>
                <div class="form-group">
                  <label class="col-md-1 control-label">&nbsp;</label>
                    <div class="col-md-4">
                      <div class="input-group">
                      <input type="text" class="form-control value" name="value2[]" value="<?php echo $value['value'] ?>">
                      <input type="hidden" class="property-value2-id" name="property_value2_id[]" value="<?php echo $value['property_value_id'] ?>">
                      <span class="input-group-btn">
                        <button class="btn btn-danger" type="button" onclick="removeValue(this)">x</button>
                      </span>
                    </div>
                  </div>
                </div>
              <?php }
             } ?>
          </div>
          <div class="tab-pane" id="property-stocks">

          </div>
        </div>
          <br><br>
           <div class="form-group">
            <div class="col-md-offset-1 col-md-4">
             <button type="submit" class="btn btn-primary" name="submit" value="1" id="submit-form">Submit</button>
             <a class="btn btn-warning" href="<?php echo BASE_URL ?>admin/buy_items">Cancel</a>
           </div>
          </div>
          <input type="hidden" ng-model="httpMethod" ng-init="httpMethod = 'POST'" value="POST"/>
          <input type="hidden" name="howToSend" ng-model="howToSend" value="1" ng-init="howToSend = 1">
          <!-- <input type="hidden" id="remove-files-index" name="remove-file" value=""> -->
      </form>
        <!-- <div > -->
        <!-- <div class="sel-file img-thumbnail" >
          {{($index + 1) + '.'}}
          <img ng-show="dataUrls[$index]" ng-src="{{dataUrls[$index]}}">
          <button class="button" ng-click="start($index)" ng-show="progress[$index] < 0">Start</button>
          <span class="progress" ng-show="progress[$index] >= 0">           
            <div style="width:{{progress[$index]}}%">{{progress[$index]}}%</div>
          </span>       
          <button class="button" ng-click="abort($index)" ng-show="hasUploader($index) && progress[$index] < 100">Abort</button>
          {{f.name}} - size: {{f.size}}B - type: {{f.type}}
        </div> -->
      <!-- </div>
      </div> -->

      <div id="property-value" style="display:none">
        <div class="form-group">
              <label class="col-md-1 control-label">&nbsp;</label>
                <div class="col-md-4">
                  <div class="input-group">
                  <input type="text" class="form-control value" name="PROPERTY_NAME" value="" onblur="refreshStocksContent()">
                  <span class="input-group-btn">
                    <button class="btn btn-danger" type="button" onclick="removeValue(this)">x</button>
                  </span>
                </div>
              </div>
            </div>
      </div>
      <div id="property-stocks-value" style="display:none">
        <div class="form-group">
              <label class="col-md-1 control-label">PROPERTY_LABEL</label>
                <div class="row">
                  <div class="col-md-4">
                    <div class="input-group">
                    <input type="text" class="form-control" name="property_stocks[]" value="" id="DATAID"  onkeydown="return checkDigit(event)" onblur="calculateStock()">
                    <input type="hidden" name="p1[]" value="P1">
                    <input type="hidden" name="p2[]" value="P2">
                  </div>
                </div>
              </div>
            </div>
      </div>
      
      <?php if(isset($property_stocks)) {
        foreach ($property_stocks as $key => $value) {
          echo '<input type="hidden" class="stock' . $key . '" value="' . $value . '">';
        }
      } ?>
 </div>

 <script>

 $(function() {
   refreshStocksContent();
    $(document).on('click', '.add-btn', function() {
        content = $('#property-value').html();
        $(this).parent().parent().parent().parent().append(content.replace('PROPERTY_NAME', 'value' + $(this).data('propindex') + '[]'));
        refreshStocksContent();
    });
    $('.prop-check').click(function() {
      if($(this).val() == 1) {
        $('.with-property').show();
        $('.without-property').hide();
      } else {
        $('.with-property').hide();
        $('.without-property').show();
      }
    });

    <?php if(isset($property_stocks) && $property_stocks) { ?>
    $('.with-property').show();
    $('.without-property').hide();
    $('.prop-check').prop('checked', false);
    $('.prop-check[value="1"]').prop('checked', true);
    <?php } ?>
 });

 function refreshStocksContent() {
    $('#property-stocks').html('<br>');
        $('#property1').find('input[name="value1[]"]').each(function() {
          value1 = $(this);
          nextValue1  = value1.nextAll('.property-value1-id').val();
          if(value1.val() == '')
            return;
          stocksContent = $('#property-stocks-value').html();
          stocksContent = stocksContent.replace('PROPERTY_LABEL', value1.val());
          if($('#property2').find('input[name="value2[]"]').length < 1) {
            if(value1.nextAll('.property-value1-id').val()) {
                stocksContent = stocksContent.replace('P1', nextValue1); 
                if(typeof nextValue1 != 'undefined')
                 stocksContent = stocksContent.replace('DATAID', nextValue1 + '_0');
            }

            $('#property-stocks').append(stocksContent);
            return;  
          }
          $('#property2').find('input[name="value2[]"]').each(function() {
            value2 = $(this);
            nextValue2 = value2.nextAll('.property-value2-id').val();
            if(value2.val() == '')
              return;
            stocksContent = $('#property-stocks-value').html();
            stocksContent = stocksContent.replace('PROPERTY_LABEL', value1.val() + ' ' +value2.val());
            if(value1.nextAll('.property-value1-id').val()) {
                stocksContent = stocksContent.replace('P1', nextValue1); 
            }
            if(value2.nextAll('.property-value2-id').val()) {
                stocksContent = stocksContent.replace('P2', nextValue2); 
            }
            if(typeof nextValue1 != 'undefined' && typeof nextValue2 != 'undefined')
              stocksContent = stocksContent.replace('DATAID', nextValue1 + '_' + nextValue2);
            $('#property-stocks').append(stocksContent);
          });
        });
    changeStockValue();
    calculateStock();
 }

 function calculateStock() {
  totalStocks = 0;
  $('#property-stocks').find('input[name="property_stocks[]"]').each(function() {
    theVal = $(this).val();
    if(theVal != '') {
      totalStocks += parseInt(theVal);
    }
  });
  if($('#stock').is(':visible'))
    return;
  $('#stock').val(totalStocks);
 }

 function changeStockValue() {
  $('#property-stocks').find('input[name="property_stocks[]"]').each(function() {
    theVal = $(this).val();
    theId = $(this).attr('id');
    if(theId != 'DATAID')
      $(this).val($('.stock' + theId).val());
  });
 }

// $("#add-form-product-info").submit( function() {
//  bootbox.dialog({
//    message: "<center><strong>Loading</strong></center>",
//    show: true,
//    backdrop: true,
//    closeButton: false
//  });
// });

function removeValue(el) {
  elem = $(el);
  elem.parent().parent().parent().parent().remove();
  refreshStocksContent();
}

$(function(){

        var LatitudeLongitudeCoordinates,
            DefaultLongitude = 122.72092948635861,
            DefaultLatitude = 11.646338452790836;
        
        <?php if( isset($record['lat']) && $record['lat'] ): ?>
          LatitudeLongitudeCoordinates = new google.maps.LatLng(<?= $record['lat'] ?>, <?= $record['lng'] ?>);
        <?php else: ?>
          LatitudeLongitudeCoordinates = new google.maps.LatLng(DefaultLatitude, DefaultLongitude);
        <?php endif; ?>

        var mapOptions = {
            zoom: <?php echo isset($record['lat']) && $record['lat'] ? 18 : 6?>,
            center: LatitudeLongitudeCoordinates,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById('google_map_canvas'), mapOptions);
        var infowindow = new google.maps.InfoWindow();
        var bounds = new google.maps.LatLngBounds();

        var marker = new google.maps.Marker({
              draggable: true, position: LatitudeLongitudeCoordinates, map: map
          });

        google.maps.event.addListener(marker, 'drag', function(event){
           document.getElementById("latitude").value = event.latLng.lat();
           document.getElementById("longitude").value = event.latLng.lng();
    });

        var listener = google.maps.event.addListener(map, "idle", function () {
            map.setZoom(<?php echo isset($record['lat']) && $record['lat'] ? 18 : 6?>);
            google.maps.event.removeListener(listener);
        });
    });
 </script>
 <!--end main content 