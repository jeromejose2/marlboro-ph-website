
<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Buy Items <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL ?>/buy_items/add" class="btn btn-primary">Add Buy Item</a>
                    <?php endif; ?>
                    <a href="<?php echo SITE_URL ?>/buy_items/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>   
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Thumbnail</th>
                         <!-- <th>Category</th> -->
                         <th>Stock</th>
                         <!-- <th>Slots</th> -->
                         <th>Value</th>
                         <th>Status</th>
                         <th>Platform Restriction</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
                   <form action="<?php echo SITE_URL ?>/buy_items">
                     <tr>
                        <td></td>
                        <td><input name="buy_item_name" class="form-control" value="<?php echo isset($_GET['buy_item_name']) ? $_GET['buy_item_name'] : '' ?>" /></td>
                        <td></td>
                        <!-- <td>
                          <select name="category_id" class="form-control">
                            <option></option>
                            <?php if($main_categories): ?>
                            <?php foreach($main_categories as $k => $v): ?>
                              <?php if($v['origin_id'] == MOVE_FWD): ?>
                              <option <?php echo isset($_GET['category_id']) && $_GET['category_id'] == $v['category_id'] ? 'selected' : '' ?> value="<?php echo $v['category_id'] ?>"><?php echo $v['category_name'] ?></option>
                            <?php endif; ?>
                          <?php endforeach; ?>
                           <?php endif; ?>
                          </select>
                         </td> -->
                         <td><input name="stock" class="form-control" value="<?php echo isset($_GET['stock']) ? $_GET['stock'] : '' ?>" /></td>
                         <!-- <td><input name="slots" class="form-control" value="<?php echo isset($_GET['slots']) ? $_GET['slots'] : '' ?>" /></td> -->
                         <td><input name="credit_value" class="form-control" value="<?php echo isset($_GET['credit_value']) ? $_GET['credit_value'] : '' ?>" /></td>
                         <td>
                          <select name="status" class="form-control">
                            <option></option>
                            <option value="1" <?php echo isset($_GET['status']) && $_GET['status'] == '1' ? 'selected' : '' ?>>Active</option>
                            <option value="0" <?php echo isset($_GET['status']) && $_GET['status'] == '0' ? 'selected' : '' ?>>Inactive</option>
                            <option value="2" <?php echo isset($_GET['status']) && $_GET['status'] == '2' ? 'selected' : '' ?>>Teaser</option>
                          </select>
                         </td>
                         <td>
                            <select name="platform_restriction" class="form-control">
                              <option></option>
                              <option value="<?php echo WEB_ONLY ?>" <?php echo isset($_GET['platform_restriction']) && $_GET['platform_restriction'] == WEB_ONLY ? 'selected' : '' ?>>Website</option>
                              <option value="<?php echo MOBILE_ONLY ?>" <?php echo isset($_GET['platform_restriction']) && $_GET['platform_restriction'] == MOBILE_ONLY ? 'selected' : '' ?>>Mobile App</option>
                              <option value="<?php echo BOTH_PLATFORM ?>" <?php echo isset($_GET['platform_restriction']) && $_GET['platform_restriction'] == BOTH_PLATFORM ? 'selected' : '' ?>>Mobile App & Website</option>
                            </select>
                         </td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
                    <?php if($categories): 
                    $status = array('Inactive', 'Active', 'Teaser');
                    $plat_restriction = array('','Website', 'Mobile App', 'Mobile App & Website');
          foreach($categories as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['buy_item_name'] ?></td>
                         <td><img src="<?php echo file_exists('uploads/buy/thumb_' . $v['buy_item_image']) ? BASE_URL . 'uploads/buy/thumb_' . $v['buy_item_image'] : BASE_URL . 'uploads/buy/' . $v['buy_item_image'] ?>?uniqid=<?php echo uniqid() ?>" width="150" /></td>
                         <!-- <td><?php echo $main_categories[$v['category_id']]['category_name'] ?></td> -->
                         <!-- <td><?php echo $v['slots'] ?></td> -->
                         <td><?php echo $v['stock']?></td>
                         <td><?php echo $v['credit_value'] ?></td>
                         <td><?php echo $status[$v['status']] ?></td>
                         <td><?php echo $plat_restriction[$v['platform_restriction']] ?></td>
                         <td>
                            <?php if($edit): ?>
                            <a href="<?php echo SITE_URL ?>/buy_items/edit/<?php echo $v['buy_item_id'] ?>" class="btn btn-primary">Edit</a>
                            <?php endif; ?>
                            <?php if($delete): ?>
                            <a href="<?php echo SITE_URL ?>/buy_items/delete/<?php echo $v['buy_item_id'] ?>/<?php echo md5($v['buy_item_id'] . ' ' . $this->config->item('encryption_key')) ?>" onclick="return confirmDeletion(this, 'buy item')" class="btn btn-danger">Delete</a>
                            <?php endif; ?>
                         </td>
                    </tr>
                    <?php 
          endforeach;
          endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->