<!--start main content -->
     <div class="container main-content">
          <form action="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>">
          <div class="page-header">
               <h3> <?php echo $header_text; ?> <span class="badge badge-important"><?php echo (int)$total_rows; ?></span></h3>

               <div class="actions">
                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/export'.$query_strings; ?>" class="btn btn-primary">Export</a>   
               </div>
          </div>
          <div class="col-sm-12">
            <div class="col-sm-2">
              From:<input name="from_date_login" class="form-control from" value="<?php echo $this->input->get('from_date_login'); ?>" />
            </div>
            <div class="col-sm-2">
              To:<input name="to_date_login" class="form-control to" value="<?php echo $this->input->get('to_date_login'); ?>" />
            </div>
            <div class="col-sm-2">
              Field: 
                <select name="field" class="form-control">
                    <option></option>
                    <option value="first_name" <?php echo $this->input->get('field') && $this->input->get('field') == 'first_name' ? 'selected' :'' ?>>Name</option>
                    <option value="logins" <?php echo $this->input->get('field') && $this->input->get('field') == 'logins' ? 'selected' :'' ?>>Logins</option>
                </select>
            </div>
            <div class="col-sm-2">
              Sort: 
                  <select name="sort" class="form-control">
                    <option></option>
                    <option value="asc" <?php echo $this->input->get('sort') && $this->input->get('sort') == 'asc' ? 'selected' :'' ?>>Ascending</option>
                    <option value="desc" <?php echo $this->input->get('sort') && $this->input->get('sort') == 'desc' ? 'selected' :'' ?>>Descending</option>
                </select>
            </div>
            <div class="col-sm-2">
              <br>
              <button type="submit" class="btn btn-primary">Go</button>
            </div>
          </div>

          <table class="table table-bordered" style="margin-top:100px;">
               <thead>
                    <tr>
                          <th style="width: 100px">Person ID</th>
                          <th style="width:200px">Name</th>
                          <th style="width:200px">Email</th>
                          <?php if(isset($chart[0])) {
                            foreach ($chart[0] as $header) {
                              echo '<th>' . $header . '</th>';
                            }
                          } ?>
                    </tr>
               </thead>
               <tbody>
               		</form>
                  <?php if($chart){
                            foreach($chart as $k => $v){ 
                              if($k == 0) continue;
                               ?>
                              <tr>
                                  <td>
                                    <?php
                                      $data = $this->db->select()->from('tbl_registrants')->where('registrant_id', $v[0])->get()->result_array();
                                      if($data) {
                                        echo $data[0]['person_id'];
                                      }
                                    ?>
                                  </td>
                                  <td><a href="#" onclick="showRegistrantDetails(<?php echo $v[0] ?>)"><?=ucwords(strtolower($v[1]))?></a></td>
                                  <td><?=$v[2]?></td>
                                  <?php 
                                  if($v) {
                                    foreach ($v as $kv => $logins) {
                                      if($kv < 3) continue;
                                      echo '<td>' . $logins . '</td>';
                                    }
                                  }
                                  ?>
                              </tr>
                    <?php
                    } ?> 
                    <tr><th>Day Total</th><th></th>
                    <?php      
                     foreach($totals as $kt => $tv) {
                        echo '<th>' . $tv . '</th>';
                    }
                    echo '</tr>';
                ?> 
                      <tr>
                        <td></td>
                        <td></td>
                      </tr>  
                      <tr>
                        <td><strong>Total</strong></td>
                        <td><strong><?= $total ?></strong></td>
                      </tr>
                      <tr>
                        <td><strong>Average</strong></td>
                        <td><strong><?= $ave ?></strong></td>
                      </tr>

                  <?php } else{ ?>
                          <tr><td colspan="5">No records found.</td></tr>
                  <?php  } ?>
                  
               </tbody>
          </table>
          <div class="pull-left">
          Note: The recommended range of data to be exported is <strong>3 months</strong>.
          </div>
          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>


     </div>
     <!--end main content -->

     <div id="view-content" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>
            </div>
          </div> 
      </div>

     <script>    

     $('.view').on('click',function(e){
          var href = $(this).attr('href');
          $('.modal-content').html('<div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>');
          $('#view-content').modal('show');
          $('.modal-content').load(href);
          e.preventDefault();
     });

     $('#view-content').on('hidden.bs.modal',function(){
        $('.modal-content').html('<img src="<?=BASE_URL?>images/spinner.gif"/></div>');
     });

     </script>