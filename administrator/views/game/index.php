<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Games <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL ?>/game/add" class="btn btn-primary">Add Game</a>
                    <?php endif; ?>
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Thumbnail</th>
                         <th>Description</th>
                         <th>Status</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/game">
                     <tr>
                         <td></td>
                         <td><input name="name" class="form-control" value="<?php echo isset($_GET['name']) ? $_GET['name'] : '' ?>" /></td>
                         <td></td>
                         <td><input name="description" class="form-control" value="<?php echo isset($_GET['description']) ? $_GET['description'] : '' ?>" /></td>
                         <td>
                            <select class="form-control" name="status">
                              <option value=""></option>
                              <option value="1" <?php echo isset($_GET['status']) && $_GET['status'] == 1 ? 'selected' : '' ?>>Published</option>
                              <option value="0" <?php echo isset($_GET['status']) && $_GET['status'] == 0 ? 'selected' : '' ?>>Unpublished</option>
                            </select>
                         </td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
                    <?php if($games): 
					foreach($games as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['name'] ?></td>
                         <td><img src="<?php echo BASE_URL . 'uploads/game/400_400_' . $v['image'] ?>" width="150" /></td>
                         <td><?php echo $v['description'] ?></td>
                         <td><?php echo $v['status'] == 1 ? 'Published' : 'Unpublished' ?></td>
                         <td>
                            <?php if($edit): ?>
                            <a href="<?php echo SITE_URL ?>/game/edit/<?php echo $v['game_id'] ?>" class="btn btn-primary">Edit</a>
                            <?php endif; ?>
                         </td>
                    </tr>
                    <?php 
					endforeach;
          else:
            echo '<tr><td colspan="7">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->