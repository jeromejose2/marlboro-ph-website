<?php $this->load->view('editor.php') ?>
<script type="text/javascript">
var jscrop_api;
function initJCrop(elem) {
	<?php if(isset($record['thumbnail_meta'])):  
		$points = isset($record['thumbnail_meta']) ? unserialize($record['thumbnail_meta']) : null; ?>
		x1 = <?php echo $points['x1'] != '' ? $points['x1'] : 0 ?>;
		x2 = <?php echo $points['x2'] != '' ? $points['x2'] : 0 ?>;
		y1 = <?php echo $points['y1'] != '' ? $points['y1'] : 0 ?>;
		y2 = <?php echo $points['y2'] != '' ? $points['y2'] : 0 ?>;
	<?php else: ?>  
		x1 = 0;
		y1 = 0;
		x2 = 200;
		y2 = 200;			
	<?php endif; ?>;
	
	setTimeout(function() {
		if(jscrop_api)
			jcrop_api.destroy();
		$('#preview').Jcrop({
		  onChange:   showCoords,
		  onSelect:   showCoords,
		  setSelect: [x1, y1, x2, y2],
		  maxSize: [ 300, 300 ]
		},function(){
		  jcrop_api = this;
		});	
		$('#width').val($('#preview').width());
		$('#height').val($('#preview').height());
	}, 500);
}

function showCoords(c) {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
};

$(function() {
	<?php if(isset($record['prize_image']) && $record['prize_image'] != ''): ?>
	initJCrop($('#photo'));
	<?php endif; ?>

  $('#type').change(function() {
    if($(this).val() == 'Redemption') {
      $('#r-address-container').show();
    } else {
      $('#r-address-container').hide();
    }
  });

});

</script>
<link rel="stylesheet" href="<?php echo BASE_URL ?>admin_assets/css/jquery.Jcrop.css" type="text/css" />

<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3>Game</h3>
      </div>
       <div class="alert alert-danger" <?php if(!$error) echo 'style="display:none;"' ?>><?php echo $error; ?></div>
       <form class="form-horizontal" role="form" method="post" onsubmit="return submitFrm()" enctype="multipart/form-data">
           <div class="form-group">
                <label class="col-sm-2 control-label">Name</label>
                <div class="col-sm-4">
                     <input type="text" name="name" value="<?php echo isset($record['name']) ? $record['name'] : '' ?>" data-name="game name" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Image</label>
                <div class="col-sm-4">
                     <input type="file" id="photo" name="photo" value="" data-name="module name" class="" onchange="previewImage('photo', 'preview', initJCrop, this);"><br>
                     <strong>Width: 500px. Height: 410px.</strong>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Image Preview</label>
                <div class="col-sm-4">
                   <img id="preview" class="preview" <?php echo isset($record['image']) ? "style='max-width:500px;' src='" . BASE_URL . 'uploads/game/' . $record['image'] . "'" : '' ?>  />
                </div>
           </div>
          <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-8">
                     <textarea name="description"><?php echo isset($record['description']) ? $record['description'] : '' ?></textarea>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Mechanics</label>
                <div class="col-sm-8">
                     <textarea name="mechanics"><?php echo isset($record['mechanics']) ? $record['mechanics'] : '' ?></textarea>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Status</label>
                <div class="col-sm-4">
                    <select class="form-control" name="status">
                      <option <?php echo isset($record['status']) && $record['status'] == 1 ? 'selected' : '' ?> value="1">Published</option>
                      <option <?php echo isset($record['status']) && $record['status'] == 0 ? 'selected' : '' ?> value="0">Unpublished</option>   
                    </select>
                </div>
           </div>
           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <input type="hidden" name="submit" value="1" />
                     <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                </div>
           </div>
      </form>
      
 </div>
 <!--end main content -->
 