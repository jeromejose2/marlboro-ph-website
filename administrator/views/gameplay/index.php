<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Game Plays Summary</h3>
               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/gameplay/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
               </div>
		  </div>

          <!-- Nav tabs --> 
          <ul class="nav nav-tabs">
            <li class="active"><a href="#graph" data-toggle="tab">Graph</a></li>
            <li class=""><a href="#summary" data-toggle="tab">Summary</a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div class="tab-pane" id="summary">
                    <table style="border-top:none;" class="table table-bordered">
                    <thead>
                         <tr>
                              <th width="1%">#</th>
                              <th>Game</th>
                              <th>Count</th>
                              <th>Percentage</th>
                              <td>Operation</td>
                         </tr>
                    </thead>
                    <tbody>
                          <form>
                              <tr>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td>From:<input name="fromdate" class="form-control from" value="<?php echo isset($_GET['fromdate']) ? $_GET['fromdate'] : '' ?>" /><br> 
                                    To:<input name="todate" class="form-control to" value="<?php echo isset($_GET['todate']) ? $_GET['todate'] : '' ?>" /><br>
                                    Field: 
                                            <select name="field" class="form-control">
                                                <option></option>
                                                <option value="name" <?php echo $this->input->get('field') && $this->input->get('field') == 'name' ? 'selected' :'' ?>>Game</option>
                                                <option value="count" <?php echo $this->input->get('field') && $this->input->get('field') == 'count' ? 'selected' :'' ?>>Count</option>
                                                <option value="count" <?php echo $this->input->get('field') && $this->input->get('field') == 'count' ? 'selected' :'' ?>>Percentage</option>
                                            </select><br>
                                          Sort: 
                                              <select name="sort" class="form-control">
                                                <option></option>
                                                <option value="asc" <?php echo $this->input->get('sort') && $this->input->get('sort') == 'asc' ? 'selected' :'' ?>>Ascending</option>
                                                <option value="desc" <?php echo $this->input->get('sort') && $this->input->get('sort') == 'desc' ? 'selected' :'' ?>>Descending</option>
                                            </select>
                                    <input name="type" type="hidden" value="<?php echo $this->input->get('type') ? $this->input->get('type') : 'age' ?>" /><br><button class="btn btn-primary" name="search" value="1">Go</button></td>
                              </tr>
                         </form>                         
                              <?php if($records): 
                              foreach($records as $k => $v): ?>
                         <tr>
                              <td><?php echo $k + 1 ?></td>
                              <td><a href="<?php echo SITE_URL ?>/game_log?name=&game_id=<?php echo $v['game_id'] ?>&is_cheater=&score=&from=&to=&search=1"><?php echo $v['name'] ?></a></td>
                              <td><?php echo $v['count'] ?></td>
                              <td><?php echo  $total > 0 ? number_format(($v['count'] / $total) * 100, 2) : 0 ?>%</td>
                              <td></td>
                         </tr>
                         <?php 
                              endforeach; ?>
                               <tr>
                                  <td></td>
                                  <td><strong>Total</strong></td>
                                  <td><strong><?php echo $total ?></strong></td>
                                  <td><strong>100%</strong></td>
                                  <td></td>
                             </tr>
                              <?php else:
                                   echo '<tr><td colspan="7">No records found</td></tr>';
                              endif; ?>
                    </tbody>
               </table>   
            </div>
            <div class="tab-pane active" id="graph">
               <div id="chart_entries" style="border:1px solid #DDDDDD; border-top:none;height:300px;">
                    
               </div>
               <br >
            </div>
          </div>
     <!--end main content -->
     <script type="text/javascript" src="<?php echo BASE_URL ?>admin_assets/js/google-api.js"></script>
     <script type="text/javascript">
     google.load("visualization", "1", {packages:["corechart"]});
     function drawChart() {
          var data = google.visualization.arrayToDataTable([
          ['Status', 'Value'],
          
           <?php foreach($records as $k => $v): ?>
          ['<?php echo $v["name"] ?>', <?php echo $v['count'] ?>],
          <?php endforeach; ?>
        ]);
          
          var options = {
          title: 'Game Plays <?php echo $this->input->get("fromdate") && $this->input->get("todate") ? "(" . date("F d, Y", strtotime($this->input->get("fromdate"))) . " - " .  date("F d, Y", strtotime($this->input->get("todate"))) . ")" : ""?>',
            is3D: true,
            /*colors:['#DDDDDD', '#90BC56','#3366CC', '#f00000'],*/
            sliceVisibilityThreshold: 0       
          };

        var chart = new google.visualization.PieChart(document.getElementById('chart_entries'));
        chart.draw(data, options);
     }
     google.setOnLoadCallback(drawChart);
     $(window).resize(drawChart);
</script> 
  
