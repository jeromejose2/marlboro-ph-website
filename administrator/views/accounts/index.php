 <div class="page-header clearfix">
	<h2 class="pull-left">User Accounts <span class="badge badge-important"><?php echo count($items) ?></span></h2>
    <a href="<?php echo base_url() ?>accounts/add" class="btn btn-inverse pull-right btn-export-data hidden-phone">Add Account</a>
</div>
<table class="table table-hover table-bordered table-heading">
    <thead>
		<tr>
            <td>Userid</td>
            <td>Account Name</td>
            <td>Username</td>
            <td>Level</td>
            <td>Action</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<?php 
        	if( $items ) : 
        		foreach( $items as $k => $v ) :
        	?>
        	 <tr>
                <td><?=$v['user_id']?></td>
                <td><?=$v['account_name']?></td>
                <td><?=$v['uname']?></td>
                <td><?=$status[$v['level']]?></td>
                <td>
                	<div class="btn-group">
                    		<a href="<?php echo base_url() ?>accounts/edit/<?php echo $v['user_id'] ?>" class="btn"><i class="icon-pencil"></i></a>
                            <a href="<?php echo base_url() ?>accounts/delete/<?php echo $v['user_id'] ?>/<?php echo md5($this->config->item('encryption_key') . $v['user_id']) ?>" class="btn" onclick="return confirmDeletion(this, 'user account')"><i class="icon-trash"></i></a>
                    </div>
                	
                </td>
            </tr>
        <?php endforeach; else: ?>
        <tr>
            <td colspan="10"><center>No Result</center></td>
        </tr>
    	<?php endif;?>
    </tbody>

</table>

