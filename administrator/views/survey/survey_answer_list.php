<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3> <?php echo $header_text; ?> <span class="badge badge-important"><?php echo (int)$total_rows; ?></span></h3>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                      <th>Title</th>
                      <th>Source</th>
                      <th>Total # of users who answered the survey</th>
                    </tr>
               </thead>
               <tbody>
                       <?php if($total_rows){ 
                              $controller = $this->router->class;
                              $base_path = SITE_URL.'/'.$controller.'/';
                              foreach($rows as $v){  
                                if($v->count != 0){?>
                                <tr>                            
                                     <td><a href="<?=site_url()?>/survey_answers/view/<?=$v->id?>"><?=$v->title?></a></td>
                                     <td><?=$v->source?></th>
                                     <td><a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/export/'.$v->id; ?>"><?=$v->count?></a></td>
                                </tr>
                              <?php }}
                      }else{ ?>
                      <tr>
                          <td colspan="9">No result found.</td>
                      </tr>
                      <?php } ?>                     
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->