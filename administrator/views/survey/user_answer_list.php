<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3> <?php echo $header_text; ?> <span class="badge badge-important"><?php echo (int)$total_rows; ?></span></h3>

               <div class="actions">
                   <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/export_answers/'.$this->uri->segment(3).$query_strings; ?>" class="btn btn-primary">Export</a> 
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                      <th>Person ID</th>
                      <th>Name</th>
                      <th>Question</th>
                      <th>Response</th>
                      <th>Date</th>
                      <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
                    <form action="<?php echo SITE_URL ?>/survey_answers/view/<?=$this->uri->segment(3)?>">
                     <tr>
                        <td><input name="person_id" class="form-control" value="<?php echo $this->input->get('person_id') ?>" /></td>
                        <td><input name="name" class="form-control" value="<?php echo $this->input->get('name') ?>" /></td>
                        <td><input name="question" class="form-control" value="<?php echo $this->input->get('question') ?>" /></td>
                        <td><input name="answer" class="form-control" value="<?php echo $this->input->get('answer') ?>" /></td>
                        <td>
                            <div class="row">
                              <div class="col-md-4">
                                   <input name="from" placeholder="From" class="form-control from" value="<?php echo isset($_GET['from']) ? $_GET['from'] : '' ?>" />
                              </div>
                              <div class="col-md-4">
                                   <input name="to" placeholder="To" class="form-control to" value="<?php echo isset($_GET['to']) ? $_GET['to'] : '' ?>" />
                              </div>
                            </div>
                        </td>
                        <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>

                       <?php if($rows->num_rows()){ 
                              $controller = $this->router->class;
                              $base_path = SITE_URL.'/'.$controller.'/';
                              foreach($rows->result() as $v){  ?>
                                <tr>
                                    <td><?php echo $v->person_id ?></td>
                                     <td><a href="#" onclick="showRegistrantDetails(<?php echo $v->registrant_id; ?>)"><?=ucwords($v->first_name.' '.$v->third_name)?></a></td>
                                     <td><?=$v->question?></td>
                                     <td>
                                        <?php if($v->options == 2){
                                              $answer = substr($v->answer, 4);;
                                              $unserialize = @unserialize($answer);

                                              if ($unserialize !== false) {
                                                  $answers = unserialize($answer);
                                                    foreach($answers as $k => $val){ ?>
                                                     <?=$val?><br>
                                              <?php }}}else{ 
                                                    if((strpos($v->answer, 'Others') !== false)){ ?>
                                                      <?=str_replace('Others ', 'Others: ', $v->answer)?>
                                                  <?php }else{ ?>
                                                    <?=$v->answer?>
                                        <?php }} ?>
                                    </td>
                                     <td><?=date('F d, Y',strtotime($v->timestamp))?></td>
                                     <td>
                                        
                                        </td>
                                </tr>
                              <?php }
                      }else{ ?>
                      <tr>
                          <td colspan="9">No result found.</td>
                      </tr>
                      <?php } ?>                     
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->