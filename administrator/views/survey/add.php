<style type="text/css">
  .remove_field{
    position: relative;
    left: 600px;
    bottom: 25px;
  }  
</style>

<div class="container main-content">
<div class="page-header">
   <h3>Survey Question</h3>
    </div>
<form class="form-horizontal" role="form" method="post" action="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>/add" enctype="multipart/form-data">
  <input type="hidden" name="survey_title" value="<?=$this->uri->segment(3)?>">

  <div class="form-group">
    <label class="col-sm-1 control-label">&nbsp;</label>
     <div class="col-sm-4"> 
     	<button class="add_field_button btn btn-info">Add More Questions</button>
     </div>
  </div>
  
  <div class="input_fields_wrap">
    <div>
    		<div class="form-group row-1">
    			<label class="col-sm-1 control-label">Question 1:</label>
  				<div class="col-sm-4">
            <input type="text" name="question-1" class="form-control" required>  
          </div>
		      <label class="col-sm-1 control-label"> Type: </label>
      		<div class="col-sm-2">	
            <select name="type-1" id="type-1" class="form-control" onchange="return openChoices('type-1');" required>
     			    <option value="">Please Select</option>
              <option value="1">Radio</option>
              <option value="2">Checkbox</option>
              <option value="3">Dropdown</option>
              <option value="4">Text</option>
            </select>
          </div>
        </div>
          
        <br>
        <div class="choices-radio-1" style="display:none;">
          <div class="form-group">
            <label class="col-sm-1 control-label">&nbsp;</label>
             <div class="col-sm-4"> 
              <a class="add-choices-button-1 btn btn-info">Add More Choices</a>
             </div>
          </div>

          <div class="form-group" >
    		      <label class="col-sm-1 control-label"> Choices:</label> 
                <div class="col-sm-4 choices-input">
    	            <div><input type="text" name="label-radio-1[]" class="form-control"><br></div>
    	            <div><input type="text" name="label-radio-1[]" class="form-control"><br></div>
                </div> 
          </div>

          <div class="form-group">
    			  <label class="col-sm-1 control-label"> Other Field: </label> 
              <div class="col-sm-4">
      	        <select name="typeother-radio-1" class="form-control">
      	          <option value="">Please Select</option>
      	          <option value="text">Text</option>
      	        </select>
              </div>

          	<label class="col-sm-1 control-label"> Label :</label>
            <div class="col-sm-4">
              <input type="text" name="labelother-radio-1" class="form-control">
            </div>
          </div>
        </div>

        <div class="choices-checkbox-1" style="display:none;">
          <div class="form-group">
            <label class="col-sm-1 control-label">&nbsp;</label>
             <div class="col-sm-4"> 
              <a class="add-choices-button-1 btn btn-info">Add More Choices</a>
             </div>
          </div>

          <div class="form-group" >
              <label class="col-sm-1 control-label"> Choices:</label> 
                <div class="col-sm-4 choices-input">
                  <div><input type="text" name="label-checkbox-1[]" class="form-control"><br></div>
                  <div><input type="text" name="label-checkbox-1[]" class="form-control"><br></div>
                </div> 
          </div>

          <div class="form-group">
            <label class="col-sm-1 control-label"> Other Field: </label> 
              <div class="col-sm-4">
                <select name="typeother-checkbox-1" class="form-control">
                  <option value="">Please Select</option>
                  <option value="text">Text</option>
                </select>
              </div>

            <label class="col-sm-1 control-label"> Label :</label>
            <div class="col-sm-4">
              <input type="text" name="labelother-checkbox-1" class="form-control">
            </div>
          </div>
        </div>

        <div class="choices-dropdown-1" style="display:none;">
          <div class="form-group">
            <label class="col-sm-1 control-label">&nbsp;</label>
             <div class="col-sm-4"> 
              <a class="add-choices-button-1 btn btn-info">Add More Choices</a>
             </div>
          </div>

          <div class="form-group" >
              <label class="col-sm-1 control-label"> Choices:</label> 
                <div class="col-sm-4 choices-input">
                  <div><input type="text" name="label-dropdown-1[]" class="form-control"><br></div>
                  <div><input type="text" name="label-dropdown-1[]" class="form-control"><br></div>
                </div> 
          </div>

          <div class="form-group">
            <label class="col-sm-1 control-label"> Other Field: </label> 
              <div class="col-sm-4">
                <select name="typeother-dropdown-1" class="form-control">
                  <option value="">Please Select</option>
                  <option value="text">Text</option>
                </select>
              </div>

            <label class="col-sm-1 control-label"> Label :</label>
            <div class="col-sm-4">
              <input type="text" name="labelother-dropdown-1" class="form-control">
            </div>
          </div>
        </div>

    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-4">
        <input type="hidden" name="submit" value="1" />
        <input type="hidden" id="x" name="x" value="1" />
      <button type="submit" id="submit" class="btn btn-primary">Submit</button>
      <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>/view_question/<?=$this->uri->segment(3)?>" class="btn btn-default">Cancel</a>
    </div>
  </div>

</form>


<script type="text/javascript">
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
  
    var x = 1; //initlal text box count

    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment 
              $('#x').val(x);
               var choices = "return openChoices('type-"+x+"');";
            $(wrapper).append('<hr style="border-top: 1px solid #aaa"><br><div><div class="form-group row-'+x+'"><label class="col-sm-1 control-label">Question '+x+':</label><div class="col-sm-4"><input type="text" name="question-'+x+'" class="form-control" required></div><label class="col-sm-1 control-label"> Type: </label><div class="col-sm-2"><select id="type-'+x+'" name="type-'+x+'" class="form-control" onchange="'+choices+'" required><option value="">Please Select</option><option value="1">Radio</option><option value="2">Checkbox</option><option value="3">Dropdown</option><option value="4">Text</option></select></div></div><br><div class="choices-radio-'+x+'" style="display:none;"><div class="form-group"><label class="col-sm-1 control-label">&nbsp;</label><div class="col-sm-4"><a class="add-choices-button-'+x+' btn btn-info">Add More Choices</a></div></div><div class="form-group"><label class="col-sm-1 control-label"> Choices:</label><div class="col-sm-4 choices-input"><div><input type="text" name="label-radio-'+x+'[]" class="form-control"><br></div><div><input type="text" name="label-radio-'+x+'[]" class="form-control"><br></div></div></div><div class="form-group"><label class="col-sm-1 control-label"> Other Field : </label><div class="col-sm-4"><select name="typeother-radio-'+x+'" class="form-control"> <option value="">Please Select</option><option value="text">Text</option></select></div><label class="col-sm-1 control-label"> Label :</label><div class="col-sm-4"><input type="text" name="labelother-radio-'+x+'" class="form-control"></div></div></div><div class="choices-checkbox-'+x+'" style="display:none;"><div class="form-group"><label class="col-sm-1 control-label">&nbsp;</label><div class="col-sm-4"><a class="add-choices-button-'+x+' btn btn-info">Add More Choices</a></div></div><div class="form-group"><label class="col-sm-1 control-label"> Choices:</label><div class="col-sm-4 choices-input"><div><input type="text" name="label-checkbox-'+x+'[]" class="form-control"><br></div><div><input type="text" name="label-checkbox-'+x+'[]" class="form-control"><br></div></div></div><div class="form-group"><label class="col-sm-1 control-label"> Other Field : </label><div class="col-sm-4"><select name="typeother-checkbox-'+x+'" class="form-control"> <option value="">Please Select</option><option value="text">Text</option></select></div><label class="col-sm-1 control-label"> Label :</label><div class="col-sm-4"><input type="text" name="labelother-checkbox-'+x+'" class="form-control"></div></div></div><div class="choices-dropdown-'+x+'" style="display:none;"><div class="form-group"><label class="col-sm-1 control-label">&nbsp;</label><div class="col-sm-4"><a class="add-choices-button-'+x+' btn btn-info">Add More Choices</a></div></div><div class="form-group"><label class="col-sm-1 control-label"> Choices:</label><div class="col-sm-4 choices-input"><div><input type="text" name="label-dropdown-'+x+'[]" class="form-control"><br></div><div><input type="text" name="label-dropdown-'+x+'[]" class="form-control"><br></div></div></div><div class="form-group"><label class="col-sm-1 control-label"> Other Field : </label><div class="col-sm-4"><select name="typeother-dropdown-'+x+'" class="form-control"> <option value="">Please Select</option><option value="text">Text</option></select></div><label class="col-sm-1 control-label"> Label :</label><div class="col-sm-4"><input type="text" name="labelother-dropdown-'+x+'" class="form-control"></div></div></div></div>'); //add input box <br><a href="#" class="remove_field">Remove</a></div>        }
      }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--; 
          $('#x').val(x);
    });
});
   
function openChoices(type)
{
    var el = $('#'+type).val();
    var t = type;
    var number = t.substr(t.indexOf("-") + 1);
    var add = $('.add-choices-button-'+number+''); //Add button Choices
    var max_field = 20;
    var row = $('div.row-'+number+''); //Fields wrapper

    if(el == 1){
      var wrap = $('.choices-radio-'+number+' > div > div.choices-input'); //Fields wrapper
      var x = $('.choices-radio-'+number+' > div > div.choices-input > div > input').length;
     
      $('.choices-radio-'+number+'').show();
      $('.choices-checkbox-'+number+'').hide();
      $('.choices-dropdown-'+number+'').hide();
    }else if(el == 2){
      var wrap = $('.choices-checkbox-'+number+' > div > div.choices-input'); //Fields wrapper
      var x = $('.choices-checkbox-'+number+' > div > div.choices-input > div >input').length;
    
      $('.choices-checkbox-'+number+'').show();
      $('.choices-radio-'+number+'').hide();
      $('.choices-dropdown-'+number+'').hide();
    }else if(el == 3){
      var wrap = $('.choices-dropdown-'+number+' > div > div.choices-input'); //Fields wrapper
      var x = $('.choices-dropdown-'+number+' > div > div.choices-input > div > input').length;
     
      $('.choices-dropdown-'+number+'').show();
      $('.choices-radio-'+number+'').hide();
      $('.choices-checkbox-'+number+'').hide();
    }else{
      $('.choices-radio-'+number+'').hide();
      $('.choices-checkbox-'+number+'').hide();
      $('.choices-dropdown-'+number+'').hide();
    }

    if(el == 2){
      $(row).append('<div class="col-sm-1 cb-limit-'+number+'"><input name="checkbox-limit-'+number+'" type="number" min="1" placeholder="Min" class="form-control required"></div><div class="col-sm-1 cb-limit-'+number+'"><input name="checkbox-max-'+number+'" type="number" min="1" placeholder="Max" class="form-control required"></div>');
    }else{
      $('.cb-limit-'+number+'').remove();
    }

    console.log(x);

    $(add).click(function(){ //on add input button click
        if(x < max_field){ //max input box allowed
            x++; //text box increment 

            if(el == 1){
              $(wrap).append('<div><input type="text" name="label-radio-'+number+'[]"  class="form-control"> <a href="#" class="remove_field">&times;</a><br></div>');
            }else if(el == 2){
              $(wrap).append('<div><input type="text" name="label-checkbox-'+number+'[]" class="form-control"> <a href="#" class="remove_field">&times;</a><br></div>');
            }else if(el == 3){
              $(wrap).append('<div><input type="text" name="label-dropdown-'+number+'[]" class="form-control"> <a href="#" class="remove_field">&times;</a><br></div>');
            }
      }

    $(wrap).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;

        if(el == 1){
          var x = $('.choices-radio-'+number+' > div > div.choices-input > div > input').length;
          console.log(x);
        }else if(el == 2){
          var x = $('.choices-checkbox-'+number+' > div > div.choices-input > div >input').length;
        }else if(el == 3){ 
          var x = $('.choices-dropdown-'+number+' > div > div.choices-input > div > input').length;
        }

      })
    });
}


</script>

<div>