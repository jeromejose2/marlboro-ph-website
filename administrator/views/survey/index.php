<div class="container main-content">


<div class="page-header">
               <h3>Survey Question<span class="badge badge-important"></span></h3>
                <?php if($this->session->flashdata('success') != '') { ?>
                      <div class="alert alert-success"> <?=$this->session->flashdata('success');?> <a href="#" class="close" data-dismiss="alert">&times;</a></div>
                <?php } ?>

                <?php if($this->session->flashdata('success-edit') != '') { ?>
                      <div class="alert alert-success"> <?=$this->session->flashdata('success-edit');?> <a href="#" class="close" data-dismiss="alert">&times;</a></div>
                <?php } ?>

                <?php if($this->session->flashdata('success-delete') != '') { ?>
                      <div class="alert alert-success"> <?=$this->session->flashdata('success-delete');?> <a href="#" class="close" data-dismiss="alert">&times;</a></div>
                <?php } ?>
               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/survey/add_survey" class="btn btn-primary">Add Survey</a>
                </div>
          </div>


	<table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Title</th>
                         <th>Source</th>
                         <th>Points</th>
                         <th>Status</th>
                         <th>Start Date</th>
                         <th>End Date</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
                  <form action="<?= SITE_URL ?>/survey">
                    <tr>
                      <td></td>
                      <td><input name="title" placeholder="Title" class="form-control" value="<?= $this->input->get('title') ? $this->input->get('title') : '' ?>" /></td>
                      <td></td>
                      <td></td>
                      <td>
                      <select class="form-control" name="is_active">
                        <option value=""></option>
                        <option <?= (string) $this->input->get('is_active') == '1' ? 'selected="selected"' : '' ?> value="1">Active</option>
                        <option <?= (string) $this->input->get('is_active') == '0' ? 'selected="selected"' : '' ?> value="0">Inactive</option>
                      </select>
                      </td>
                      <td><input name="start_date" class="from form-control" value="<?php echo isset($_GET['start_date']) ? $_GET['start_date'] : '' ?>" /></td>
                      <td><input name="end_date" class="from form-control" value="<?php echo isset($_GET['end_date']) ? $_GET['end_date'] : '' ?>" /></td>
                      <td><button class="btn btn-primary" name="search" type="submit">Go</button></td>
                    </tr>
                  </form>

               	<?php
               	 $offset = 0;
               	foreach ($records as $key => $value) { ?>

               			<tr>
           				<td><?php echo $offset + $key + 1 ?></td>
                     	<td><?php echo $value->title; ?></td>
                     	<td><?php echo $value->source; ?></td>
                     	<td><?php echo $value->points; ?></td>
                     	<td><?php echo ($value->is_active == 1) ? "<span style='background: green; color: white'>Active</span>"  : "<span style='background: orange; color: white'>Inactive</span>"; ?></td>
                      <td><?php echo date('M d, y', strtotime($value->start_date)); ?></td>
                      <td><?php echo date('M d, y', strtotime($value->end_date)); ?></td>
                     	<td>
                         <a href="<?php echo SITE_URL ?>/survey/view_question/<?php echo $value->id ?>" class="btn btn-primary">Questions</a>
                        <?php echo ($value->is_active == 0) ? '<a href="'.SITE_URL.'/survey/set_active/'.$value->id.'/'.$value->source.'" class="btn btn-warning" >Set as Active </a> '  : ''; ?>
                     		 <a href="<?php echo SITE_URL ?>/survey/edit/<?php echo $value->id ?>" class="btn btn-primary">Edit</a>
                     		 <a id="<?=$value->id?>" class="btn btn-danger delete">Delete</a>
                     	</td>
                     	</tr>

          <?php } ?>



               <tbody>

               </table>

               <ul class="pagination pagination-sm pull-right">
                  <?= !empty($pagination) ? $pagination : '' ?>
                </ul>

</div>

<script type="text/javascript">

$(".delete").click( function() {
  var id = $(this).attr("id");
  bootbox.confirm("Are you sure you want to delete?", function(confirm) {
    if (confirm) {
      window.location = "<?= SITE_URL ?>/survey/delete/" + id;
    }
  });
});


</script>