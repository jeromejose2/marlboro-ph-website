<style type="text/css">
  .remove_field{
    position: relative;
    left: 600px;
    bottom: 25px;
  }  
</style>

<div class="container main-content">
<div class="page-header">
   <h3>Survey Question</h3>
    </div>
<form class="form-horizontal" role="form" method="post" action="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>/edit_question/<?=$record->id?>" enctype="multipart/form-data">
  <input type="hidden" name="survey_title" value="<?=$this->uri->segment(3)?>">
  <input type="hidden" name="survey_id" value="<?=$record->survey_id?>">
  
  <div class="input_fields_wrap">
    <div>
    		<div class="form-group row-1">
    			<label class="col-sm-1 control-label">Question:</label>
  				<div class="col-sm-4">
            <input type="text" name="question-1" class="form-control" required value="<?=isset($record->question) ? $record->question : "" ?> ">  
          </div>
		      <label class="col-sm-1 control-label"> Type: </label>
      		<div class="col-sm-2">	
            <select name="type-1" id="type-1" class="form-control" onchange="return openChoices('type-1');" required>
              <option value="1" <?=isset($record->options) && ($record->options == 1) ? 'selected' : "" ?> >Radio</option>
              <option value="2" <?=isset($record->options) && ($record->options == 2) ? 'selected' : "" ?> >Checkbox</option>
              <option value="3" <?=isset($record->options) && ($record->options == 3) ? 'selected' : "" ?> >Dropdown</option>
              <option value="4" <?=isset($record->options) && ($record->options == 4) ? 'selected' : "" ?> >Text</option>
            </select>
          </div>
          <?php if(isset($record->options) && ($record->options == 2)) { ?>
          <div class="col-sm-1 cb-limit-1">
            <input name="checkbox-limit-1" type="number" min="1" placeholder="Min" class="form-control required" value="<?=$record->checkbox_limit?>">
          </div>
          <div class="col-sm-1 cb-limit-1">
            <input name="checkbox-max-1" type="number" min="1" placeholder="Max" class="form-control required" value="<?=$record->checkbox_max?>">
          </div>
          <?php } ?>
        </div>
          
        <br>
      <?php $choices = unserialize($record->choices) ?>

      <div class="choices-radio-2" style="display:none;">
        <div class="form-group">
          <label class="col-sm-1 control-label">&nbsp;</label>
           <div class="col-sm-4"> 
            <a class="add-choices-button-1 btn btn-info">Add More Choices</a>
           </div>
        </div>

        <div class="form-group" >
            <label class="col-sm-1 control-label"> Choices:</label> 
              <div class="col-sm-4 choices-input">
                <input type="text" name="label-radio-1[]" class="form-control"><br>
                <input type="text" name="label-radio-1[]" class="form-control"><br>
              </div> 
        </div>

        <div class="form-group">
          <label class="col-sm-1 control-label"> Other Field: </label> 
            <div class="col-sm-4">
              <select name="typeother-radio-1" class="form-control">
                <option value="">Please Select</option>
                <option value="text">Text</option>
              </select>
            </div>

          <label class="col-sm-1 control-label"> Label :</label>
          <div class="col-sm-4">
            <input type="text" name="labelother-radio-1" class="form-control">
          </div>
        </div>
      </div>

      <div class="choices-checkbox-2" style="display:none;">
        <div class="form-group">
          <label class="col-sm-1 control-label">&nbsp;</label>
           <div class="col-sm-4"> 
            <a class="add-choices-button-1 btn btn-info">Add More Choices</a>
           </div>
        </div>

        <div class="form-group" >
            <label class="col-sm-1 control-label"> Choices:</label> 
              <div class="col-sm-4 choices-input">
                <input type="text" name="label-checkbox-1[]" class="form-control"><br>
                <input type="text" name="label-checkbox-1[]" class="form-control"><br>
              </div> 
        </div>

        <div class="form-group">
          <label class="col-sm-1 control-label"> Other Field: </label> 
            <div class="col-sm-4">
              <select name="typeother-checkbox-1" class="form-control">
                <option value="">Please Select</option>
                <option value="text">Text</option>
              </select>
            </div>

          <label class="col-sm-1 control-label"> Label :</label>
          <div class="col-sm-4">
            <input type="text" name="labelother-checkbox-1" class="form-control">
          </div>
        </div>
      </div>

      <div class="choices-dropdown-2" style="display:none;">
        <div class="form-group">
          <label class="col-sm-1 control-label">&nbsp;</label>
           <div class="col-sm-4"> 
            <a class="add-choices-button-1 btn btn-info">Add More Choices</a>
           </div>
        </div>

        <div class="form-group" >
            <label class="col-sm-1 control-label"> Choices:</label> 
              <div class="col-sm-4 choices-input">
                <input type="text" name="label-dropdown-1[]" class="form-control"><br>
                <input type="text" name="label-dropdown-1[]" class="form-control"><br>
              </div> 
        </div>

        <div class="form-group">
          <label class="col-sm-1 control-label"> Other Field: </label> 
            <div class="col-sm-4">
              <select name="typeother-dropdown-1" class="form-control">
                <option value="">Please Select</option>
                <option value="text">Text</option>
              </select>
            </div>

          <label class="col-sm-1 control-label"> Label :</label>
          <div class="col-sm-4">
            <input type="text" name="labelother-dropdown-1" class="form-control">
          </div>
        </div>
      </div>

      <!-- RADIO -->

      <?php if(isset($record->options) && ($record->options == 1)) { ?>
        <div class="choices-radio-1 active" style="display:block;">
          <div class="form-group">
            <label class="col-sm-1 control-label">&nbsp;</label>
             <div class="col-sm-4"> 
              <a class="add-choices-button-1 btn btn-info">Add More Choices</a>
             </div>
          </div>

          <div class="form-group" >
    		      <label class="col-sm-1 control-label"> Choices:</label> 
                <div class="col-sm-4 choices-input">
      <?php if(!empty($choices)) {
                foreach ($choices as $k => $val){ 
                  if( ($val != '') && (strpos($val, 'Others') !== false) ){ }else{ if($val != '') {?>
                <div>
    	            <input type="text" name="label-radio-1[]" value="<?=$val?>" class="form-control" ><a href="#" class="remove_field">&times;</a><br>
                </div>
      <?php }}}} ?>
                </div> 
          </div>
        
                <div class="form-group">
          			  <label class="col-sm-1 control-label"> Other Field: </label> 
                    <div class="col-sm-4">
            	        <select name="typeother-radio-1" class="form-control">
            	          <option value="">Please Select</option>
            	          <option value="text"
                          <?php if(!empty($choices)) {
                                  foreach ($choices as $k => $val){ 
                                    if( ($val != '') && (strpos($val, 'Others') !== false) ){ ?>
                                        selected 
                          <?php }}} ?>
                        >Text</option>
            	        </select>
                    </div>

                	<label class="col-sm-1 control-label"> Label :</label>
                  <div class="col-sm-4">
                    <input type="text" name="labelother-radio-1" class="form-control" 
                      <?php if(!empty($choices)) {
                            foreach ($choices as $k => $val){ 
                             if( ($val != '') && (strpos($val, 'Others') !== false) ){ ?>
                         value="<?=str_replace('Others ', '', $val)?>"
                      <?php }}} ?>
                    >
                  </div>
                </div>
              </div>
        
      <?php }else if(isset($record->options) && ($record->options == 2)) { ?>
        <div class="choices-checkbox-1 active" style="display:block;">
          <div class="form-group">
            <label class="col-sm-1 control-label">&nbsp;</label>
             <div class="col-sm-4"> 
              <a class="add-choices-button-1 btn btn-info">Add More Choices</a>
             </div>
          </div>

          <div class="form-group" >
              <label class="col-sm-1 control-label"> Choices:</label> 
                <div class="col-sm-4 choices-input">
      <?php if(!empty($choices)) {
                foreach ($choices as $k => $val){ 
                  if( ($val != '') && (strpos($val, 'Others') !== false) ){ }else{ if($val != '') {?>
                <div>
                  <input type="text" name="label-checkbox-1[]" value="<?=$val?>" class="form-control"><a href="#" class="remove_field">&times;</a><br>
                </div>
      <?php }}}} ?>
                </div> 
          </div>

      
            <div class="form-group">
              <label class="col-sm-1 control-label"> Other Field: </label> 
                <div class="col-sm-4">
                  <select name="typeother-checkbox-1" class="form-control">
                    <option value="">Please Select</option>
                    <option value="text"
                      <?php if(!empty($choices)) {
                              foreach ($choices as $k => $val){ 
                                if( ($val != '') && (strpos($val, 'Others') !== false) ){ ?>
                                    selected 
                      <?php }}} ?>
                    >Text</option>
                  </select>
                </div>

              <label class="col-sm-1 control-label"> Label :</label>
              <div class="col-sm-4">
                <input type="text" name="labelother-checkbox-1" class="form-control" 
                      <?php if(!empty($choices)) {
                            foreach ($choices as $k => $val){ 
                             if( ($val != '') && (strpos($val, 'Others') !== false) ){ ?>
                         value="<?=str_replace('Others ', '', $val)?>"
                      <?php }}}?>
                >
              </div>
            </div>
        </div>

      <?php }else if(isset($record->options) && ($record->options == 3)) { ?>
        <div class="choices-dropdown-1 active" style="display:block;">
          <div class="form-group">
            <label class="col-sm-1 control-label">&nbsp;</label>
             <div class="col-sm-4"> 
              <a class="add-choices-button-1 btn btn-info">Add More Choices</a>
             </div>
          </div>

          <div class="form-group" >
              <label class="col-sm-1 control-label"> Choices:</label> 
                <div class="col-sm-4 choices-input">
      <?php if(!empty($choices)) {
                foreach ($choices as $k => $val){ 
                  if( ($val != '') && (strpos($val, 'Others') !== false) ){ }else{ if($val != '') { ?>
                <div>
                  <input type="text" name="label-dropdown-1[]" value="<?=$val?>" class="form-control"><a href="#" class="remove_field">&times;</a><br>
                </div>

      <?php }}}} ?>
                </div> 
          </div>
            <div class="form-group">
              <label class="col-sm-1 control-label"> Other Field: </label> 
                <div class="col-sm-4">
                  <select name="typeother-dropdown-1" class="form-control">
                    <option value="">Please Select</option>
                    <option value="text"
                      <?php if(!empty($choices)) {
                              foreach ($choices as $k => $val){ 
                                if( ($val != '') && (strpos($val, 'Others') !== false) ){ ?>
                                    selected 
                      <?php }}} ?>
                    >Text</option>
                  </select>
                </div>

              <label class="col-sm-1 control-label"> Label :</label>
              <div class="col-sm-4">
                <input type="text" name="labelother-dropdown-1" class="form-control" 
                      <?php if(!empty($choices)) {
                            foreach ($choices as $k => $val){ 
                             if( ($val != '') && (strpos($val, 'Others') !== false) ){ ?>
                         value="<?=str_replace('Others ', '', $val)?>"
                      <?php }}}?>
                    >
              </div>
            </div>
        </div>
        <?php } ?>
    </div>

    
  </div>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-4">
      <input type="hidden" name="submit" value="1">
      <button type="submit" id="submit" class="btn btn-primary">Update</button>
      <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>/view_question/<?=$record->survey_id?>" class="btn btn-default">Cancel</a>
    </div>
  </div>

</form>


<script type="text/javascript">
$(function(){

  var el = $('#type-1').val();
  var wrap = $('div.active > div > div.choices-input');
  var x = $('div.active > div > div.choices-input > div > input').length;
  var add = $('.add-choices-button-1'); //Add button Choices.
  var max_field = 20;

  $(add).click(function(){ //on add input button click
      if(x < max_field){ //max input box allowed
          x++; //text box increment 

          if(el == 1){
            $(wrap).append('<div><input type="text" name="label-radio-1[]" class="form-control"><a href="#" class="remove_field">&times;</a><br></div>');
          }else if(el == 2){
            $(wrap).append('<div><input type="text" name="label-checkbox-1[]" class="form-control"><a href="#" class="remove_field">&times;</a><br></div>');
          }else if(el == 3){
            $(wrap).append('<div><input type="text" name="label-dropdown-1[]" class="form-control"><a href="#" class="remove_field">&times;</a><br></div>');
          }
    }
  });

  $(wrap).on("click",".remove_field", function(e){ //user click on remove text
      e.preventDefault(); $(this).parent('div').remove(); x--;
    })

})

function openChoices(type)
{
  var el = $('#'+type).val();
  var option = '<?=$record->options?>';
  var row = $('div.row-1'); //Fields wrapper
  
  if(el == 1){
    if(option == el){      
      $('.choices-radio-1').show();
      $('.choices-radio-1').addClass('active');

      $('.choices-radio-2').hide();
      $('.choices-radio-2').removeClass('active');

      $('.choices-checkbox-2').hide();
      $('.choices-checkbox-2').removeClass('active');

      $('.choices-dropdown-2').hide();
      $('.choices-dropdown-2').removeClass('active');
    }else{
      $('.choices-radio-2').show();
      $('.choices-radio-2').addClass('active');

      $('.choices-radio-1').hide();
      $('.choices-radio-1').removeClass('active');

      $('.choices-checkbox-1').hide();
      $('.choices-checkbox-1').removeClass('active');
      
      $('.choices-dropdown-1').hide();
      $('.choices-dropdown-1').removeClass('active');
      
      $('.choices-checkbox-2').hide();
      $('.choices-checkbox-2').removeClass('active');

      $('.choices-dropdown-2').hide();
      $('.choices-dropdown-2').removeClass('active');
    }
  }else if(el == 2){   
    if(option == el){
      $('.choices-checkbox-1').show();
      $('.choices-checkbox-1').addClass('active');

      $('.choices-radio-2').hide();
      $('.choices-radio-2').removeClass('active');

      $('.choices-checkbox-2').hide();
      $('.choices-checkbox-2').removeClass('active');

      $('.choices-dropdown-2').hide();
      $('.choices-dropdown-2').removeClass('active');
    }else{
      $('.choices-checkbox-2').show();
      $('.choices-checkbox-2').addClass('active');

      $('.choices-radio-1').hide();
      $('.choices-radio-1').removeClass('active');

      $('.choices-checkbox-1').hide();
      $('.choices-checkbox-1').removeClass('active');

      $('.choices-dropdown-1').hide();
      $('.choices-dropdown-1').removeClass('active');

      $('.choices-radio-2').hide();
      $('.choices-radio-2').removeClass('active');

      $('.choices-dropdown-2').hide();
      $('.choices-dropdown-2').removeClass('active');
    }
  }else if(el == 3){
    if(option == el){
      $('.choices-dropdown-1').show();
      $('.choices-dropdown-1').addClass('active');

      $('.choices-radio-2').hide();
      $('.choices-radio-2').removeClass('active');

      $('.choices-checkbox-2').hide();
      $('.choices-checkbox-2').removeClass('active');

      $('.choices-dropdown-2').hide();
      $('.choices-dropdown-2').removeClass('active');
    }else{
      $('.choices-dropdown-2').show();
      $('.choices-dropdown-2').addClass('active');

      $('.choices-radio-1').hide();
      $('.choices-radio-1').removeClass('active');

      $('.choices-checkbox-1').hide();
      $('.choices-checkbox-1').removeClass('active');

      $('.choices-dropdown-1').hide();
      $('.choices-dropdown-1').removeClass('active');

      $('.choices-radio-2').hide();
      $('.choices-radio-2').removeClass('active');

      $('.choices-checkbox-2').hide();
      $('.choices-checkbox-2').removeClass('active');
    }
  }else{
    $('.choices-radio-1').hide();
    $('.choices-radio-1').removeClass('active');

    $('.choices-checkbox-1').hide();
    $('.choices-checkbox-1').removeClass('active');

    $('.choices-dropdown-1').hide();
    $('.choices-dropdown-1').removeClass('active');

    $('.choices-radio-2').hide();
    $('.choices-radio-2').removeClass('active');

    $('.choices-checkbox-2').hide();
    $('.choices-checkbox-2').removeClass('active');

    $('.choices-dropdown-2').hide();
    $('.choices-dropdown-2').removeClass('active');
  }

  if(el == 2){
    $(row).append('<div class="col-sm-1 cb-limit-1"><input name="checkbox-limit-1" type="number" min="1" placeholder="Min" class="form-control required"></div><div class="col-sm-1 cb-limit-1"><input name="checkbox-max-1" type="number" min="1" placeholder="Max" class="form-control required"></div>');
  }else{
    $('.cb-limit-1').remove();
  }

  var wrap = $('div.active > div > div.choices-input');
  var x = $('div.active > div > div.choices-input > div > input').length;
  var add = $('.add-choices-button-1'); //Add button Choices.
  var max_field = 20;

  console.log(wrap);
  console.log(x);

  $(add).click(function(){ //on add input button click
      if(x < max_field){ //max input box allowed
          x++; //text box increment 

          if(el == 1){
            $(wrap).append('<div><input type="text" name="label-radio-1[]" class="form-control"><a href="#" class="remove_field">&times;</a><br></div>');
          }else if(el == 2){
            $(wrap).append('<div><input type="text" name="label-checkbox-1[]" class="form-control"><a href="#" class="remove_field">&times;</a><br></div>');
          }else if(el == 3){
            $(wrap).append('<div><input type="text" name="label-dropdown-1[]" class="form-control"><a href="#" class="remove_field">&times;</a><br></div>');
          }
    }
  });

  $(wrap).on("click",".remove_field", function(e){ //user click on remove text
      e.preventDefault(); $(this).parent('div').remove(); x--;

      if(el == 1){
        var x = $('.choices-radio-1 > div > div.choices-input > div > input').length;
        console.log(x);
      }else if(el == 2){
        var x = $('.choices-checkbox-1 > div > div.choices-input > div >input').length;
      }else if(el == 3){ 
        var x = $('.choices-dropdown-1 > div > div.choices-input > div > input').length;
      }

    })

}


</script>

<div>