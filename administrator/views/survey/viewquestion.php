
<div class="container main-content">

  <div class="page-header">
    <?php if($this->session->flashdata('success') != '') { ?>
            <div class="alert alert-success"> <?=$this->session->flashdata('success');?> <a href="#" class="close" data-dismiss="alert">&times;</a></div>
    <?php } ?>

    <?php if($this->session->flashdata('success-edit') != '') { ?>
          <div class="alert alert-success"> <?=$this->session->flashdata('success-edit');?> <a href="#" class="close" data-dismiss="alert">&times;</a></div>
    <?php } ?>

    <?php if($this->session->flashdata('success-delete') != '') { ?>
          <div class="alert alert-success"> <?=$this->session->flashdata('success-delete');?> <a href="#" class="close" data-dismiss="alert">&times;</a></div>
    <?php } ?>

    <div class="actions" style="float:left">
      <a href="<?php echo SITE_URL ?>/survey" class="btn btn-default" style="float:left;"> < Back</a>
    </div>
    <div class="actions">
      <a href="<?php echo SITE_URL ?>/survey/add/<?=$this->uri->segment(3)?>" class="btn btn-primary">Add Question</a>
    </div>
  </div>
  <br><br>

  <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Survey Category Title</th>
                         <th>Questions</th>
                         <th>Choices</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
                <form action="<?= SITE_URL ?>/survey/view_question/<?=$this->uri->segment(3)?>">
                  <tr>
                    <td></td>
                    <td></td>
                    <td><input name="question" placeholder="Question" class="form-control" value="<?= $this->input->get('question') ? $this->input->get('question') : '' ?>" /></td>
                    <td></td>
                    <td><button class="btn btn-primary" name="search" type="submit">Go</button></td>
                  </tr>
                </form>

                <?php
                  $offset = 0;
                  foreach ($records as $key => $value) {
                    $choices = unserialize($value->choices); 
                      ?>
                      <tr>
                      <td><?php echo $offset + $key + 1 ?></td>
                      <td><?php echo $value->title; ?></td>
                      <td><?php echo $value->question; ?></td>
                      <td>
                      <?php if(!empty($value->options)) {
                              if($value->options == 1){  
                                if(!empty($choices)) { 
                                  foreach($choices as $k => $val) { 
                                    if( ($val != '') && (strpos($val, 'Others') !== false) ){ ?>
                                      <br><span><input type="radio" value="" class="_req" name=""> <?=str_replace('Others ', '', $val)?> </span>
                                      <br><span><input type="text" value="" name=""></span>
                                    <?php }else if($val != '') { ?> 
                                      <br><span><input type="radio" value="" class="_req" name=""> <?=$val?> </span>
                                <?php }}} ?>
                        <?php }else if($value->options == 2){ 
                                if(!empty($choices)) { 
                                  foreach($choices as $k => $val) { 
                                    if( ($val != '') && (strpos($val, 'Others') !== false) ){ ?>
                                      <br><span><input type="checkbox" value="" class="_req" name=""> <?=str_replace('Others ', '', $val)?> </span>
                                      <br><span><input type="text" value="" name=""></span>
                                    <?php }else if($val != '') { ?> 
                                        <br><span><input type="checkbox" value="" class="_req" name=""> <?=$val?> </span>
                                <?php }}} ?>
                        <?php }else if($value->options == 3){ ?>
                                  <select class="_req">
                                    <option>Please select</option>
                           <?php if(!empty($choices)) { 
                                  foreach($choices as $k => $val) { 
                                    if( ($val != '') && (strpos($val, 'Others') !== false) ){ ?>
                                    <option><?=str_replace('Others ', '', $val)?></option>
                                  <?php }else if($val != '') { ?> 
                                    <option><?=$val?></option>
                           <?php }}} ?>
                                  </select>
                        <?php }else if( ($value->options == 4) ) {
                                if(!empty($choices)) { 
                                  foreach($choices as $k => $val) { 
                                    if( ($val != '') && ($val == 'text') ){ ?>                            
                                     <br><span><textarea style="resize:none;height:100px;width:100%;" name="" class="_req"></textarea>
                              <?php }}}} ?>
                      <?php } ?>
                      </td>
                      <td>
                         <a href="<?php echo SITE_URL ?>/survey/edit_question/<?php echo $value->q_id ?>" class="btn btn-primary">Edit</a>
                         <a id="<?=$this->uri->segment(3)?>/<?=$value->q_id ?>" class="btn btn-danger delete">Delete</a>
                      </td>
                      </tr>

               <?php } ?>



               <tbody>

               </table>

               <ul class="pagination pagination-sm pull-right">
                  <?= !empty($pagination) ? $pagination : '' ?>
                </ul>

</div>
<script type="text/javascript">
$(function(){

$("textarea").prop('disabled', true);

});

$(".delete").click( function() {
  var id = $(this).attr("id");
  bootbox.confirm("Are you sure you want to delete?", function(confirm) {
    if (confirm) {
      window.location = "<?= SITE_URL ?>/survey/delete_question/" + id;
    }
  });
});
</script>
