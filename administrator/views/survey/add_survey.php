<link rel="stylesheet" href="<?=base_url()?>admin_assets/css/timepicker.css">
<script src="<?=base_url()?>admin_assets/js/timepicker.js" type="text/javascript"></script>

<div class="container main-content">
  <div class="page-header">
             <h3>Add Survey </h3>
  </div>

    <?php if(isset($error) && $error != ''){ ?>
      <div class="alert alert-danger"> <?=$error?> </div>
    <?php } ?>

    <form class="form-horizontal" role="form" method="post" action="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>/add_survey" enctype="multipart/form-data">
      <div class="form-group">
          <label class="col-sm-1 control-label">Survey Title:</label>
          <div class="col-sm-4">
            <input type="text" name="survey_title" placeholder="Title"  value="<?php echo isset($records[0]->title) ? $records[0]->title : '' ?>" class="form-control required" >
          </div>
      </div>

      <div class="form-group">
          <label class="col-sm-1 control-label">Survey Points:</label>
          <div class="col-sm-4">
            <input name="survey_points" type="number" placeholder="100" min="1" value="<?php echo isset($records[0]->points) ? $records[0]->points : '' ?>"  class="form-control required" >
          </div>
      </div>

      <div class="form-group">
           <label class="col-sm-1 control-label">Survey Source:</label>
          <div class="col-sm-4">
          <!-- <input type="text" name="survey_source"  value="<?php echo isset($records[0]->source) ? $records[0]->source : '' ?>"  placeholder="Buy"> -->
          <select name="survey_source" class="form-control required">
            <option value="">Please Select</option>
            <option value="buy" <?php echo (isset($records[0]->source) && $records[0]->source == 'buy')  ? 'selected' : ''; ?> >Buy</option>
            <option value="bid" <?php echo (isset($records[0]->source) && $records[0]->source == 'bid')  ? 'selected' : ''; ?> >Bid</option>
            <option value="movefwd" <?php echo (isset($records[0]->source) && $records[0]->source == 'movefwd')  ? 'selected' : ''; ?> >MoveFWD</option>
            <option value="games" <?php echo (isset($records[0]->source) && $records[0]->source == 'games')  ? 'selected' : ''; ?> >Games</option>
          </select>
          </div>
      </div>

      <div class="form-group">
          <label class="col-sm-1 control-label">&nbsp;</label>
          <div class="col-sm-4">
          	<input type="hidden"  name="id" value="<?php echo isset($records[0]->id) ? $records[0]->id : '' ?>"/>
          </div>
      </div>

      <div class="form-group">
            <label class="col-sm-1 control-label">Start Date :</label>
            <div class="col-sm-4">
                 <input type="text" value="<?php echo isset($records[0]->start_date) ? $records[0]->start_date : '' ?>" name="start_date" class="form-control required from" id="start_date" >
            </div>
       </div>

       <div class="form-group">
            <label class="col-sm-1 control-label">End Date :</label>
            <div class="col-sm-4">
                 <input type="text" value="<?php echo isset($records[0]->end_date) ? $records[0]->end_date : '' ?>" name="end_date" class="form-control required to" id="end_date" >
            </div>
            <br>  
       </div>

       <div class="form-group">
          <div class="col-sm-offset-2 col-sm-4">
            <button class="button btn btn-primary" type="submit"><i>Submit</i></button>
            <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>" class="btn btn-default">Cancel</a>
          </div>
       </div>

      
    </form>

</div>

<script>
    $(function() {
        $("#start_date").datetimepicker({dateFormat:'yy-mm-dd'});
        $("#end_date").datetimepicker({dateFormat:'yy-mm-dd'});
    });
 </script>