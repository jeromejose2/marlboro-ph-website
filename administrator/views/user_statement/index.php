<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>User Statements <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/user_statement/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Person ID</th>
                         <th>Name</th>
                         <th>Statement</th>
                         <th>Date Added</th>
                         <?php if($edit || $delete) : ?>
                         <th>Operation</th>
                         <?php endif; ?>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/user_statement">
                         <tr>
                             <td></td>
                             <td></td>
                             <td><input name="name" class="form-control" value="<?php echo isset($_GET['name']) ? $_GET['name'] : '' ?>" /></td>
                             <td><input name="statement" class="form-control" value="<?php echo isset($_GET['statement']) ? $_GET['statement'] : '' ?>" /></td>
                             <td>
                             
                              <div class="row">
                                <div class="col-md-4">
                                     <input name="from_publish" placeholder="From" class="form-control from" value="<?php echo isset($_GET['from_publish']) ? $_GET['from_publish'] : '' ?>" />
                                </div>
                                <div class="col-md-4">
                                     <input name="to_publish" placeholder="To" class="form-control to" value="<?php echo isset($_GET['to_publish']) ? $_GET['to_publish'] : '' ?>" />
                                </div>
                              </div>
                            </td>
                             <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                        </tr>
                        </form>
                        
                    <?php if($users): 
					foreach($users as $k => $v): 
            $statements = explode(',', $v['statement_id']);
          if($statements) {
            $statement_str = '<ul>';
            foreach ($statements as $key => $value) {
                if($value)
                  $statement_str .= '<li>' . $statement_arr[$value] . '</li>';
            }
            $statement_str .= '</ul>';
          }
           ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['person_id'] ?></td>
                         <td><a href="#" onclick="showRegistrantDetails(<?php echo $v['registrant_id'] ?>)"><?php echo $v['first_name'] . ' ' . $v['third_name'] ?></a></td>
                         <td><?php echo  $statement_str ?></td>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['date_created']))  ?></td>
                         <td></td>
                     </tr>
                    <?php 
					endforeach;
					else:
						echo '<tr><td colspan="5">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->