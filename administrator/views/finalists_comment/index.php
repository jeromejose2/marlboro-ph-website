<?php 
  $controller = $this->uri->segment(1);
  $label = '';
  switch($controller) {
    case 'finalists':
      $label = 'Finalist';
      break;
    case 'birthday_offer':
      $label = 'Birthday Offer';
      break;
    case 'flash_offer':
      $label = 'Flash Offer';
      break;
    case 'finalists_comments':
      $label = 'Finalist Comments';
      break;
    default:
      $label = 'Prize';
      break;

  }
?>


<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3><?php echo $label ?> <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>
               <div class="actions">
                  <button id="approve-all" class="btn btn-success">Approve</button>
                  <button type="button" onclick="return multiple_disapproval()" class="btn btn-warning" target="_blank">Reject</button>
                    <?php if($add): ?>
                    <?php /* <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/add" class="btn btn-primary">Add <?php echo $label ?></a> */ ?>
                    <?php endif; ?>
                    <?php /* <a href="<?php echo SITE_URL ?>/export/prizes?<?php echo @http_build_query($this->input->get()) ?>" class="btn btn-primary">Export</a> */ ?>
                     <a href="<?php echo SITE_URL ?>/finalists_comments/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>      
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th># <input type="checkbox" id="check-all"></th>
                         <th>Name</th>
                         <th>Nickname</th>
                         <th>Photo</th>
                         <th>Comment</th>
                         <th>Finalist</th>
                         <th>URL</th>
                         <th>Status</th>
                         <th>Date</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
               		<form action="<?php echo SITE_URL ?>/finalists_comments">
                     <tr>
                      <td></td>
                         <!-- <td><input type="checkbox" id="check-all" /></td> -->
  <td><input name="first_name" class="form-control" value="<?php echo isset($_GET['first_name']) ? $_GET['first_name'] : '' ?>" /></td> 
    <td></td>
                            <td></td> 
                               <td></td>
                                       <td></td>
                                       <td></td>
 <td>
                          <select name="status" class="form-control">
                            <option></option>
                            <option value="0" <?php echo $this->input->get('status') != '' && $this->input->get('status') == '0' ? 'selected' : '' ?>>Pending</option>
                            <option value="1" <?php echo $this->input->get('status') != '' && $this->input->get('status') == '1' ? 'selected' : '' ?>>Approved</option>
                            <option value="2" <?php echo $this->input->get('status') != '' && $this->input->get('status') == '2' ? 'selected' : '' ?>>Rejected</option>
                          </select>
                        </td>

               
                         
                         <td>
                          <div class="row">
                                <div class="col-md-4">
                                     <input name="date_status_approved_from" placeholder="From" class="form-control from" value="<?php echo isset($_GET['date_status_approved']) ? $_GET['date_status_approved'] : '' ?>" />
                                </div>
                                <div class="col-md-4">
                                     <input name="date_status_approved_to" placeholder="To" class="form-control to" value="<?php echo isset($_GET['date_status_approved']) ? $_GET['date_status_approved'] : '' ?>" />
                                </div>
                              </div>
                         </td>

                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
                    <?php if($records): 
					foreach($records as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?> <input type="checkbox" name="checkbox[]" value="<?php echo $v['finalist_comment_id'] ?>" class="comment-ids"></td>
                         <td><a href="#" onclick="showRegistrantDetails(<?php echo $v['registrant_id'] ?>)"><?php echo $v['first_name'] .' '. $v['third_name'] ?></a></td>
                         <td><?php echo $v['nick_name'] ?></td>
                         <td><img width="200" src="<?=BASE_URL.$v['primary_photo']?>" /></td>
                         <td><?php echo $v['comment'] ?></td>
                         <td><?php echo $v['name'] ?></td>
                         <td><a href="<?php echo str_replace('admin', '', SITE_URL) .'finalists/'. $v['finalist_id'] .'/'. toSlug($v['name']) ?>" target="_blank"><?php echo str_replace('admin', '', SITE_URL) .'finalists/'. $v['finalist_id'] .'/'. toSlug($v['name']) ?></a></td>
                         <td class="status-<?php echo $v['finalist_comment_id'] ?>">
                            <?php switch ($v['status']) {
                              case 2:
                                echo 'Rejected';
                                break;

                              case 1:
                                echo 'Approved';
                                break;
                              
                              default:
                                echo 'Pending';
                                break;
                            } ?>
                         </td>
                         <td><?php echo $v['date_added'] ?></td>

                         <td>
                            <?php /* if($edit): ?>
                            <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/edit/<?php echo $v['finalist_id'] ?>" class="btn btn-primary">Edit</a>
                            <?php endif; ?>
                            <?php if($delete): ?>
                            <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/delete/<?php echo $v['finalist_id'] ?>/<?php echo md5($v['finalist_id'] . ' ' . $this->config->item('encryption_key')) ?>" onclick="return confirmDeletion(this, '<?php echo str_replace('_', ' ', $controller) ?>')" class="btn btn-danger">Delete</a>
                            <?php endif; */ ?>
                            <?php if($edit): ?>
                              <?php if($v['status'] == 0): ?>
                                <a style="<?php echo $v['status'] == 1 ? 'display: none' : '' ?>" class="btn btn-success approve-<?php echo $v['finalist_comment_id'] ?>" onclick="changeStatus('approve', <?php echo $v['finalist_comment_id']?>)">Approve</a>
                                <a href="javascript:void(0)" style="<?php echo $v['status'] == 2 ? 'display: none' : '' ?>" class="btn btn-warning reject-<?php echo $v['finalist_comment_id'] ?>" onclick="changeStatus('reject', <?php echo $v['finalist_comment_id']?>)">Reject</a>
                              <?php endif; ?>
                              
                            <?php endif; ?>
                             <a class="btn btn-primary" href="<?php echo SITE_URL ?>/finalists_comments/replies?finalist_id=<?php echo $v['finalist_id'] ?>&first_name=&comment_reply=&comment_reply_status=&fromavail=&toavail=&search=1">View Replies</a>
                         </td>
                    </tr>
                    <?php 
					endforeach;
          else:
            echo '<tr><td colspan="9">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

          <div class="modal" id="reason">
            <div style="text-align:center">
              <form id="reason-form" method="POST">
                <input type="hidden" name="action" value="reject">
                <input type="hidden" name="id" value="" id="reject-name">
                    <textarea name="message" class="form-control" rows="5"></textarea>
                    <br>
                    <div style="float:right">
                        <button type="button" class="btn close-bootbox" name="search">Cancel</button>
                        <button class="btn btn-primary" name="search" value="1">Submit</button>
                    </div>
                    <br style="clear:both;">
              </form>
            </div>
          </div>

          <div class="modal" id="reason-multiple">
            <div style="text-align:center">
              <form id="reason-form-2" method="POST">
                <input type="hidden" name="action" value="multiple-reject">
                    <textarea name="message" class="form-control" rows="5"></textarea>
                    <br>
                    <div style="float:right">
                        <button type="button" class="btn close-bootbox" name="search">Cancel</button>
                        <button class="btn btn-primary" name="search" value="1">Submit</button>
                    </div>
                    <br style="clear:both;">
              </form>
            </div>
          </div>

     </div>
     <!--end main content -->



<script>
  function changeStatus(action, id) {

 if(action == 'approve')
                            {

    bootbox.confirm('Are you sure you want to '+action+' this comment?', function(result) {
        if(result) {
            var approveBtn = $(".approve-"+id);
            var rejectBtn = $(".reject-"+id);
            if(action == 'approve')
            {
               var data = {action:action, id:id}
                   $.post("<?php echo SITE_URL .'/finalists_comments/changeStatus' ?>", data, function(response) {
                      window.location.reload();
                      // switch(response) {
                      //   case 'approve':
                      //     rejectBtn.show();
                      //     approveBtn.hide();
                      //     $('.status-'+id).html('Approved');
                      //     window.location.reload();
                      //     break;
                      //   case 'reject':
                      //     approveBtn.show();
                      //     rejectBtn.hide();
                      //     $('.status-'+id).html('Rejected');
                      //     break;
                      //   default:
                      //     break;
                      // }
                    });
            }
       }
    });

}

   if(action == 'reject')
                            {
                                          $("#reject-name").val(id);
                                          $("#reason form").prop("action", "<?php echo site_url() ?>/finalists_comments/changeStatus");
                                          bootbox.dialog({'message': $('#reason').html(), 'title': 'Reason for disapproval'});
                                          // approveBtn.show();
                                          // rejectBtn.hide();
                                          // $('.status-'+id).html('Rejected');
                                          // window.location.reload();
                            }

  
    // switch(action) {
    //   case 'approve':
         
      
    //     break;
    //   case 'reject':
    //     bootbox.confirm('Are you sure you want to reject this comment ?', function(result) {
    //       if(result == true) {
    //     $("#reject-name").val(id);
    //     $("#reason form").prop("action", "<?php echo site_url() ?>/finalists_comments/changeStatus");
    //     bootbox.dialog({'message': $('#reason').html(), 'title': 'Reason for disapproval'});
    //     // var data = {action:action, id:id}
    //     return false;
    //                     }
    //            });
    //     break;
    //   default:
    //     break;
    // }

  }

  $("#check-all").click(function(){
    if($(this).is(':checked')) {
      $('input[type=checkbox]').prop('checked', true);
    } else {
      $('input[type=checkbox]').prop('checked', false);
    }
  });

  $('#approve-all').click(function() {
      ids = [];
      $('.comment-ids').each(function() {
        if($(this).is(':checked')) {
          ids.push($(this).val());
        }
      });
      if(ids.length < 1) {
        bootbox.alert('Sorry, you must select at least 1 entry');
      } else {
          bootbox.confirm('Are you sure you want approve all selected comments?', function(result) {
            if(result == 1) {
              $.post('<?php echo SITE_URL ?>/finalists_comments/changeStatus', {ids: ids, action:'multiple-approve'}, function() {
                window.location.reload();
              })
              return true;
            }
          });

      }
     });

  function multiple_disapproval(){
   
          if($('input[name="checkbox[]"]').is(':checked')){

            var form = document.getElementById('reason-form-2');
              
            $.each($('input[name="checkbox[]"]'),function(i,val){
              
              if($(this).is(':checked')){

                var input = document.createElement('input');
                input.type = 'hidden';
                input.name = 'ids[]';
                input.value = val.value;
                form.appendChild(input);

              }

            });
            
            $("#reason-multiple form").prop("action", "<?=SITE_URL?>/finalists_comments/changeStatus");
            bootbox.dialog({'message': $('#reason-multiple').html(), 'title': 'Reason for disapproval'});
            
             
          }else{

            bootbox.alert('Please select entries to disapprove.');
            return false;

          }       

       }


       <?php if($this->session->userdata('error')): ?>
      setTimeout(function(){
        bootbox.alert("<?php echo $this->session->userdata('error') ?>");
        <?php $this->session->unset_userdata('error') ?>
      }, 1000);
     <?php endif; ?>
</script>