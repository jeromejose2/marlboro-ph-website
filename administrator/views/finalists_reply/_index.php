<?php 
  $controller = $this->uri->segment(1);
  $label = '';
  switch($controller) {
    case 'reply_finalists':
      $label = 'Comment Replies';
      break;
    case 'birthday_offer':
      $label = 'Birthday Offer';
      break;
    case 'flash_offer':
      $label = 'Flash Offer';
      break;
    case 'finalists_comments':
      $label = 'Finalist Comments';
      break;
    default:
      $label = 'Prize';
      break;

  }
?>


<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3><?php echo $label ?> <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/add" class="btn btn-primary">Add <?php echo $label ?></a>
                    <?php endif; ?>
                    <a href="<?php echo SITE_URL ?>/export/prizes?<?php echo @http_build_query($this->input->get()) ?>" class="btn btn-primary">Export</a>
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Nickname</th>
                         <th>Comment</th>
                         <th>Comment Origin</th>
                         <th>Finalist</th>
                         <th>URL</th>
                         <th>Status</th>
                         <th>Date</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/<?php echo $controller ?>">
                     <tr>
                         <td></td>
                         <td><input name="prize_name" class="form-control" value="<?php echo isset($_GET['prize_name']) ? $_GET['prize_name'] : '' ?>" /></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
                    <?php if($records): 
					foreach($records as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><a href="#" onclick="showRegistrantDetails(<?php echo $v['registrant_id'] ?>)"><?php echo $v['first_name'] .' '. $v['third_name'] ?></a></td>
                         <td><?php echo $v['nick_name'] ?></td>
                         <td><?php echo $v['comment'] ?></td>
                         <td><?php echo $v['comment_origin'] ?></td>
                         <td><?php echo $v['name'] ?></td>
                         <td><a href="<?php echo str_replace('admin', '', SITE_URL) .'finalists/'. $v['finalist_id'] .'/'. toSlug($v['name']) ?>" target="_blank"><?php echo str_replace('admin', '', SITE_URL) .'finalists/'. $v['finalist_id'] .'/'. toSlug($v['name']) ?></a></td>
                         <td class="status-<?php echo $v['finalist_comment_reply_id'] ?>">
                            <?php switch ($v['status']) {
                              case 2:
                                echo 'Rejected';
                                break;

                              case 1:
                                echo 'Approved';
                                break;
                              
                              default:
                                echo 'Pending';
                                break;
                            } ?>
                         </td>
                         <td><?php echo $v['date_added'] ?></td>

                         <td>
                            <?php /* if($edit): ?>
                            <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/edit/<?php echo $v['finalist_id'] ?>" class="btn btn-primary">Edit</a>
                            <?php endif; ?>
                            <?php if($delete): ?>
                            <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/delete/<?php echo $v['finalist_id'] ?>/<?php echo md5($v['finalist_id'] . ' ' . $this->config->item('encryption_key')) ?>" onclick="return confirmDeletion(this, '<?php echo str_replace('_', ' ', $controller) ?>')" class="btn btn-danger">Delete</a>
                            <?php endif; */ ?>
                            <?php if($edit): ?>
                              <a style="<?php echo $v['status'] == 1 ? 'display: none' : '' ?>" class="btn btn-primary approve-<?php echo $v['finalist_comment_reply_id'] ?>" onclick="changeStatus('approve', <?php echo $v['finalist_comment_reply_id']?>)">Approve</a>
                              <a style="<?php echo $v['status'] == 0 || $v['status'] == 2 ? 'display: none' : '' ?>" class="btn btn-danger reject-<?php echo $v['finalist_comment_reply_id'] ?>" onclick="changeStatus('reject', <?php echo $v['finalist_comment_reply_id']?>)">Reject</a>
                            <?php endif; ?>
                         </td>
                    </tr>
                    <?php 
					endforeach;
          else:
            echo '<tr><td colspan="7">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->

<script>
  function changeStatus(action, id) {
    var approveBtn = $(".approve-"+id);
    var rejectBtn = $(".reject-"+id);
    switch(action) {
      case 'approve':
        var data = {action:action, id:id}
        break;
      case 'reject':
        var data = {action:action, id:id}
        break;
      default:
        break;
    }
    $.post("<?php echo SITE_URL .'/reply_finalists/changeStatus' ?>", data, function(response) {
      switch(response) {
        case 'approve':
          rejectBtn.show();
          approveBtn.hide();
          $('.status-'+id).html('Approved');
          break;
        case 'reject':
          approveBtn.show();
          rejectBtn.hide();
          $('.status-'+id).html('Rejected');
          break;
        default:
          break;
      }
    });
  }
</script>