<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Time of Day for Logins</h3>
               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/logins/export?<?php echo http_build_query($query_string); ?>" class="btn btn-primary">Export</a>
               </div>
		  </div>

          <!-- Nav tabs --> 
          <ul class="nav nav-tabs">
            <li><a href="#graph" id="graph-btn" data-toggle="tab">Graph</a></li>
            <li><a href="#summary" id="summary-btn" data-toggle="tab">Summary</a></li>
          </ul>  

          <!-- Tab panes -->
          <div class="tab-content">
               <div class="tab-pane" id="summary">
                    <table class="table table-bordered" style="border-top:none;">
                         <thead>
                              <tr>
                                   <th width="1%">#</th>
                                   <th>
                                   <?php /* if($this->input->get('sort_field')=='tlog'){ 
                                            if($this->input->get('sort_order')=='ASC'){?>
                                            <a title="sort by descending order" href="<?=SITE_URL?>/logins?<?=http_build_query($query_string)?>&sort_field=tlog&sort_order=DESC">Time <i class="glyphicon glyphicon-chevron-up"></i></a>
                                            <?php }else{ ?>
                                            <a title="sort by ascending order" href="<?=SITE_URL?>/logins?<?=http_build_query($query_string)?>&sort_field=tlog&sort_order=ASC">Time <i class="glyphicon glyphicon-chevron-down"></i></a>
                                            <?php }
                                           }else{ ?>
                                            <a title="sort by descending order" href="<?=SITE_URL?>/logins?<?=http_build_query($query_string)?>&sort_field=tlog&sort_order=DESC">Time 
                                              <?php if(!$this->input->get()){ ?>
                                                    <i class="glyphicon glyphicon-chevron-up"></i>
                                              <?php } ?></a>
                                    <?php } */ ?>
                                    Time
                                  </th>
                                   <th>
                                    <?php /*if($this->input->get('sort_field')=='count'){ 
                                            if($this->input->get('sort_order')=='ASC'){?>
                                            <a href="<?=SITE_URL?>/logins?<?=http_build_query($query_string)?>&sort_field=count&sort_order=DESC">Visits <i class="glyphicon glyphicon-chevron-up"></i></a>
                                            <?php }else{ ?>
                                            <a href="<?=SITE_URL?>/logins?<?=http_build_query($query_string)?>&sort_field=count&sort_order=ASC">Visits <i class="glyphicon glyphicon-chevron-down"></i></a>
                                            <?php }
                                           }else{ ?>
                                            <a href="<?=SITE_URL?>/logins?<?=http_build_query($query_string)?>&sort_field=count&sort_order=ASC">Visits</a>
                                    <?php }*/ ?>
                                    Visits
                                    
                                  </th>
                                   <th>Operation</th>
                              </tr>
                         </thead>
                         <tbody>
                              <form>
                                   <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>From:<input name="fromdate" class="form-control from" value="<?php echo $from; ?>" /><br> 
                                          To:<input name="todate" class="form-control to" value="<?php echo $to; ?>" /><br>
                                          Sort By:
                                          <select name="sort_by" class="form-control">
                                            <option value="tlog" <?=$this->input->get('sort_by')=='tlog' ? 'selected="selected"' : ''?>>Time</option>
                                            <option value="count" <?=$this->input->get('sort_by')=='tlog' ? 'selected="selected"' : ''?>>Visits</option>
                                          </select><br>
                                          Sort Order:
                                          <select name="sort_order" class="form-control">
                                            <option value="asc" <?=$this->input->get('sort_order')=='asc' ? 'selected="selected"' : ''?>>ASC</option>
                                            <option value="desc" <?=$this->input->get('sort_order')=='desc' ? 'selected="selected"' : ''?>>DESC</option>
                                          </select>
                                          <input name="type" type="hidden" value="<?php echo $this->input->get('type') ? $this->input->get('type') : 'age' ?>" /><br><button class="btn btn-primary" name="search" value="1">Go</button></td>
                                   </tr>
                              </form>
                         		<?php
                            $total_logins = 0;
                             if($records): 
                      					foreach($records as $k => $v): 
                                        $total_logins += $v['count']; ?>
                                          <tr>
                                               <td><?php echo $k + 1 ?></td>
                                               <td> 
                                                <?php 
                                                  if($v['tlog'] <= 11) {
                                                      $time =  $v['tlog'] == 0 ? '12AM' : $v['tlog'] . 'AM';
                                                    } else {
                                                      $time = ($v['tlog'] - 12) == 0 ? '12PM' : ($v['tlog'] - 12) . 'PM';
                                                    }
                                                  echo $time;
                                                ?>
                                               </td>
                                               <td><?php echo $v['count'] ?></td>
                                               <td></td>
                                          </tr>
                                          <?php 
                      					endforeach; ?>
                            <tr><td></td><td><strong>Total </strong></td><td><?=$total_logins?></td></tr>
          				<?php	else:
          						echo '<tr><td colspan="7">No records found</td></tr>';
          					endif; ?>
                         </tbody>
                    </table>
                    <div><strong>Date :</strong> <?=date("F d, Y", strtotime($from)) . " - " .  date("F d, Y", strtotime($to))?></div>
               </div>
               <div class="tab-pane active" id="graph">
                    <div id="chart_entries" style="border:1px solid #DDDDDD; border-top:none;height:300px;">
                         
                    </div>
                    <br >
                 </div>
           </div>
           
     </div>
     <!--end main content -->

     <script type="text/javascript" src="<?php echo BASE_URL ?>admin_assets/js/google-api.js"></script>
     <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Count'],
          <?php foreach($records as $k => $v):  
            if($v['tlog'] <= 11) {
              $time =  $v['tlog'] == 0 ? '12AM' : $v['tlog'] . 'AM';
            } else {
              $time = ($v['tlog'] - 12) == 0 ? '12PM' : ($v['tlog'] - 12) . 'PM';
            }
          ?>
          ['<?php echo $time ?>',  <?php echo $v['count'] ?>],
          <?php endforeach; ?>
        ]);

        var options = {
          title: 'Site Logins <?php echo $from && $to ? "(" . date("F d, Y", strtotime($from)) . " - " .  date("F d, Y", strtotime($to)) . ")" : ""?>'
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_entries'));
        chart.draw(data, options);

        <?php if(!$this->input->get()){ ?>
              $('#graph-btn').trigger('click');
        <?php }else{ ?>
              $('#summary-btn').trigger('click');
        <?php } ?>
      }
 
    </script>

  
