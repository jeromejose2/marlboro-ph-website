<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Login Spread (Days)</h3>
               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/day_logins/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
               </div>
		  </div>

          <!-- Nav tabs --> 
          <ul class="nav nav-tabs">
            <li class="active"><a href="#graph" data-toggle="tab">Graph</a></li>
            <li class=""><a href="#summary" data-toggle="tab">Summary</a></li>
          </ul>  

          <!-- Tab panes -->
          <div class="tab-content">
               <div class="tab-pane" id="summary">
                    <table class="table table-bordered" style="border-top:none;">
                         <thead>
                              <tr>
                                   <th width="1%">#</th>
                                   <th>Day</th>
                                   <th>Visits</th>
                                   <th>Visitors</th>
                                   <td>Operation</td>
                              </tr>
                         </thead>
                         <tbody>
                              <form>
                                   <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                          From:<input name="fromdate" class="form-control from" value="<?php echo isset($_GET['fromdate']) ? $_GET['fromdate'] : '' ?>" /><br> 
                                          To:<input name="todate" class="form-control to" value="<?php echo isset($_GET['todate']) ? $_GET['todate'] : '' ?>" /><br>
                                            Field: 
                                              <select name="field" class="form-control">
                                                  <option></option>
                                                  <option value="dlog" <?php echo $this->input->get('field') && $this->input->get('field') == 'dlog' ? 'selected' :'' ?>>Day</option>
                                                  <option value="visits" <?php echo $this->input->get('field') && $this->input->get('field') == 'visits' ? 'selected' :'' ?>>Visits</option>
                                                  <option value="visitors" <?php echo $this->input->get('field') && $this->input->get('field') == 'visitors' ? 'selected' :'' ?>>Visitors</option>
                                              </select>
                                            Sort: 
                                                <select name="sort" class="form-control">
                                                  <option></option>
                                                  <option value="asc" <?php echo $this->input->get('sort') && $this->input->get('sort') == 'asc' ? 'selected' :'' ?>>Ascending</option>
                                                  <option value="desc" <?php echo $this->input->get('sort') && $this->input->get('sort') == 'desc' ? 'selected' :'' ?>>Descending</option>
                                              </select><br>
                                          <input name="type" type="hidden" value="<?php echo $this->input->get('type') ? $this->input->get('type') : 'age' ?>" /><br><button class="btn btn-primary" name="search" value="1">Go</button></td>
                                   </tr>
                              </form>
                         		<?php if($records): $totalVisits = 0; $totalVisitors = 0;
          					foreach($records as $k => $v): $totalVisits += (isset($v['visits'][0]['count']) ? $v['visits'][0]['count'] : 0);  $totalVisitors += $v['visitors']; ?>
                              <tr>
                                   <td><?php echo $k + 1 ?></td>
                                   <td><?php echo $days[$v['dlog']] ?></td>
                                   <td><?php echo $v['visits'] ?></td>
                                   <td><?php echo $v['visitors'] ?></td>
                                   <td></td>
                              </tr>
                              <?php 
          					endforeach; ?>
                    <tr>
                      <td></td>
                      <td><strong>Total</strong></td>
                      <td><strong><?= $totalVisits ?></strong></td>
                      <td><strong><?= $totalVisitors ?></strong></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><strong>Average</strong></td>
                      <td><strong><?= round($totalVisits / count($records)) ?></strong></td>
                      <td><strong><?= round($totalVisitors / count($records)) ?></strong></td>
                      <td></td>
                    </tr>
          					<?php else:
          						echo '<tr><td colspan="7">No records found</td></tr>';
          					endif; ?>
                         </tbody>
                    </table>
               </div>
               <div class="tab-pane active" id="graph">
                    <div id="chart_entries" style="border:1px solid #DDDDDD; border-top:none;height:300px;">
                         
                    </div>
                    <br >
                 </div>
           </div>
           
     </div>
     <!--end main content -->

     <script type="text/javascript" src="<?php echo BASE_URL ?>admin_assets/js/google-api.js"></script>
     <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Visits', 'Visitors'],
          <?php foreach($records as $k => $v): ?>
          ['<?php echo $days[$v["dlog"]] ?>',  <?php echo $v['visits']?>, <?php echo $v['visitors']?>],
          <?php endforeach; ?>
        ]);

        var options = {
          title: 'Site Logins <?php echo $this->input->get("fromdate") && $this->input->get("todate") ? "(" . date("F d, Y", strtotime($this->input->get("fromdate"))) . " - " .  date("F d, Y", strtotime($this->input->get("todate"))) . ")" : ""?>'
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_entries'));
        chart.draw(data, options);
      }
    </script>

  
