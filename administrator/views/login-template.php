<!DOCTYPE html>
<html>
<head>
     <title>CMS</title>
     <!-- Bootstrap -->
     <link rel="stylesheet" href="<?php echo BASE_URL ?>admin_assets/css/bootstrap.min.css" >
     <link rel="stylesheet" href="<?php echo BASE_URL ?>admin_assets/css/style.css" >
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
     <![endif]-->
</head>
<body>

     


     <!--start main content -->
     <div style="margin:0 auto;" class="login-content">
          <div class="page-header">
               <h3>Login</h3>
          </div>

          <?php if($error_msg) { ?>
          <div class="alert alert-danger"><?php echo $error_msg; ?></div>
		  <?php } ?>
          <?php if(isset($_GET['ref'])) { ?>
          <div class="alert alert-danger">Sorry, your account has been disabled. Please contact Philip Morris, Inc. Admin for more details.</div>
		  <?php } ?>
          <form method="post" class="form-horizontal" role="form">
               <div class="form-group">
                    <label class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                         <input type="email" autocomplete="off" name="username" class="form-control">
                    </div>
               </div>
               <div class="form-group">
                    <label class="col-sm-3 control-label">Password</label>
                    <div class="col-sm-9">
                         <input type="password" autocomplete="off" name="password" class="form-control">
                    </div>
               </div>
               
              <?php /*?> <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                         <div class="checkbox">
                              <label>
                                   <input type="checkbox"> Remember me
                              </label>
                         </div>
                    </div>
               </div><?php */?>
               <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                         <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
               </div>
          </form>


     </div>
     <!--end main content -->



     <script src="<?php echo BASE_URL ?>admin_assets/js/jquery.js"></script>
     <script src="<?php echo BASE_URL ?>admin_assets/js/bootstrap.min.js"></script>
</body>
</html>