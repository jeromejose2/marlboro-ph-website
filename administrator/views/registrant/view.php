<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" onsubmit="return submitFrm()" target="upload-giid-target">
<div class="alert alert-danger" style="display:none;"></div>
<h4>GIID Details</h4>
<div class="form-group">
    <label class="col-sm-3 control-label">Image</label>
    <div class="col-sm-6">
       <p class="form-control-static">
        <?php if($v['giid_file']): ?>
          <?php if(strrpos($v['giid_file'], 'pdf')) :  ?>
            <a target="_blank" href="<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/giid/<?php echo $v['giid_file'] ?>">View PDF</a> 
          <?php else: ?> 
          <a class="modal-pop" href="#" data-title="Name: <?php echo $val->PersonDetails->FirstName . ' ' . $v['third_name'] . '<br > GIID Type: ' . @$giid[$v['government_id_type']] . '<br> GIID Number: ' .  $v['government_id_number'] ?>"><img src="<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/giid/<?php echo $v['giid_file'] ?>" width="150" /></a> 
        <?php endif; ?>
        <br ><br >
        <?php else: 
        echo 'No GIID';
        endif; ?>
        </p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">GIID Type</label>
    <div class="col-sm-6">
       <p class="form-control-static">
        <?php 
        $giid_value = 'No GIID';
          if ($val->IdDocumentTypeCode == '1'){
            $giid_value = 'NBI';
          }
          else if($val->IdDocumentTypeCode == '2'){
            $giid_value = 'SSS';
          }
          else if($val->IdDocumentTypeCode == '3'){
            $giid_value = 'TIN';
          }
          else if($val->IdDocumentTypeCode == '4'){
            $giid_value = "Driver's License";
          }
          echo $giid_value;
        ?>
        
        </p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">GIID Number</label>
    <div class="col-sm-6">
       <p class="form-control-static">
        <?php echo $val->IdDocumentNumber?>
       </p>
    </div>
</div><br >
<h4>Profile Details</h4>
<div class="form-group">
    <label class="col-sm-3 control-label">Profile Picture</label>
    <div class="col-sm-4">
      <?php if($v['current_picture']): ?>
      <img src="<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/<?php echo $v['current_picture'] ?>" width="200" />
      <?php else: 
        echo 'No profile picture';
        endif; ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Nickname</label>
    <div class="col-sm-6">
      <p class="form-control-static"><?=@$val->Derivations[NICKNAME]->AttributeValue ? @$val->Derivations[NICKNAME]->AttributeValue : 'n/a' ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">First Name</label>
    <div class="col-sm-6">
         <p class="form-control-static"><?php echo $val->PersonDetails->FirstName ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Middle Name</label>
    <div class="col-sm-6">
        <p class="form-control-static"><?php echo $val->PersonDetails->MiddleName ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Last Name</label>
    <div class="col-sm-6">
       <p class="form-control-static"><?php echo $val->PersonDetails->LastName ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Birthday</label>
    <div class="col-sm-6">
        <?php
            $date_time = $val->PersonDetails->DateOfBirth;

                $match = preg_match('/\/Date\((\d+)([-+])(\d+)\)\//', $date_time, $date);

                $timestamp = $date[1]/1000;
                $operator = $date[2];
                $hours = $date[3]*36; // Get the seconds

                $datetime = new DateTime();

                $datetime->setTimestamp($timestamp);
                $datetime->modify($operator . $hours . ' seconds');
          ?>
       <p class="form-control-static"><?=$datetime->format('M d, Y')?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Source</label>
    <div class="col-sm-6">
       <p class="form-control-static">
        <?php 
          if($v['from_site'] == 0) {
            echo 'MYM';
          } else {
            if($v['from_migration'] == 1) {
              echo 'KANA';
            } else {
              echo 'Website';
            }
          }
        ?>
       </p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Mobile Number</label>
    <div class="col-sm-6">
        <p class="form-control-static"><?php echo $val->PersonDetails->MobileTelephoneNumber ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Gender</label>
    <div class="col-sm-6">
        <p class="form-control-static"><?php echo $val->PersonDetails->Gender ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Email Address</label>
    <div class="col-sm-6">
        <p class="form-control-static"><?php echo $val->Login[0]->LoginName ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">House and Street</label>
    <div class="col-sm-6">
        <p class="form-control-static"><?php echo $val->Addresses[0]->AddressLine2 ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Barangay</label>
    <div class="col-sm-6">
        <p class="form-control-static"><?php echo $val->Addresses[0]->Area ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Province</label>
    <div class="col-sm-6">
         <p class="form-control-static">
         <?php 
         $prv = '';
         foreach($provinces as $pk => $pv): 
              if($pv['province_id'] == $v['province'])
                $prv = $pv['province'];
          endforeach; 
          echo $val->Addresses[0]->Locality;
        ?>
      </p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">City</label>
    <div class="col-sm-6">
         <p class="form-control-static"><?php echo $val->Addresses[0]->City ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Zip Code</label>
    <div class="col-sm-6">
        <p class="form-control-static"><?php echo $val->Addresses[0]->PostalCode ?></p>
    </div>
</div>
<br >
<h4>Survey Answers</h4>
<div class="form-group">
    <span class="col-sm-6 help-block">What brand do you smoke most frequently?</span>
<p class="form-control-static"><?php echo $primary[0]['brand_name'] ?></p>  
</div>
<div class="form-group">
    <span class="col-sm-6 help-block">What other brands do you smoke aside from your regular brand?</span>
<p class="form-control-static"><?php echo $secondary[0]['brand_name'] ?></p>  
</div>
<div class="form-group">
    <span class="col-sm-6 help-block">What would you do if your regular brand is unavailable?</span>
<p class="form-control-static"><?=@$val->Derivations[REACTION]->AttributeValue ? @$val->Derivations[REACTION]->AttributeValue : 'n/a' ?></p> 
</div>
<div class="form-group control-label">
    <div class="col-sm-offset-2 col-sm-10">
         <input type="hidden" name="submit" value="1" />
         <input type="hidden" name="id" value="<?php echo $this->uri->segment(3) ?>" />
         <button name="search" class="btn close-bootbox" type="button">Cancel</button>
    </div>
</div>
