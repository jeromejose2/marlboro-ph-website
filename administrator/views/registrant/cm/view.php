<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" onsubmit="return submitFrm()" target="upload-giid-target">
<div class="alert alert-danger" style="display:none;"></div>
<h4>GIID Details</h4>
<div class="form-group">
    <label class="col-sm-3 control-label">Image</label>
    <div class="col-sm-6">
       <p class="form-control-static">
        <?php if($v['giid_file']): ?>
          <?php if(strrpos($v['giid_file'], 'pdf')) :  ?>
            <a target="_blank" href="<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/giid/<?php echo $v['giid_file'] ?>">View PDF</a> 
          <?php else: ?> 
          <a class="modal-pop" href="#" data-title="Name: <?php echo $v['first_name'] . ' ' . $v['third_name'] . '<br > GIID Type: ' . @$giid[$v['government_id_type']] . '<br> GIID Number: ' .  $v['government_id_number'] ?>"><img src="<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/giid/<?php echo $v['giid_file'] ?>" width="150" /></a> 
        <?php endif; ?>
        <br ><br >
        <?php else: 
        echo 'No GIID';
        endif; ?>
        </p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">GIID Type</label>
    <div class="col-sm-6">
       <p class="form-control-static">
        <?php 
        $giid_value = 'No GIID';
        foreach($giid as $gk => $gv):
          if ($v['government_id_type'] == $gk):
            $giid_value = $gv;
          endif;
        endforeach; 
        echo $gv;
        ?>
        
        </p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">GIID Number</label>
    <div class="col-sm-6">
       <p class="form-control-static">
        <?php echo $v['government_id_number'] ?>
       </p>
    </div>
</div><br >
<h4>Profile Details</h4>
<div class="form-group">
    <label class="col-sm-3 control-label">Profile Picture</label>
    <div class="col-sm-4">
      <?php if($v['current_picture']): ?>
      <img src="<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/<?php echo $v['current_picture'] ?>" width="200" />
      <?php else: 
        echo 'No profile picture';
        endif; ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Nickname</label>
    <div class="col-sm-6">
      <p class="form-control-static"><?php echo $v['nick_name'] ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">First Name</label>
    <div class="col-sm-6">
         <p class="form-control-static"><?php echo $v['first_name'] ?></p>
        <!-- <p class="form-control-static"><?php echo $v['first_name'] ?></p> -->
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Middle Name</label>
    <div class="col-sm-6">
        <p class="form-control-static"><?php echo $v['middle_initial'] ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Last Name</label>
    <div class="col-sm-6">
       <p class="form-control-static"><?php echo $v['third_name'] ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Birthday</label>
    <div class="col-sm-6">
       <p class="form-control-static"><?php echo date('F d, Y', strtotime($v['date_of_birth'])) ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Mobile Number</label>
    <div class="col-sm-6">
        <p class="form-control-static"><?php echo $v['mobile_phone'] ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Gender</label>
    <div class="col-sm-6">
        <p class="form-control-static"><?php echo $v['gender'] ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Email Address</label>
    <div class="col-sm-6">
        <p class="form-control-static"><?php echo $v['email_address'] ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">House and Street</label>
    <div class="col-sm-6">
        <p class="form-control-static"><?php echo $v['street_name'] ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Barangay</label>
    <div class="col-sm-6">
        <p class="form-control-static"><?php echo $v['barangay'] ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Province</label>
    <div class="col-sm-6">
         <p class="form-control-static">
         <?php 
         $prv = '';
         foreach($provinces as $pk => $pv): 
              if($pv['province_id'] == $v['province'])
                $prv = $pv['province'];
          endforeach; 
          echo $prv;
        ?>
      </p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">City</label>
    <div class="col-sm-6">
         <p class="form-control-static"><?php echo $city ?></p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Zip Code</label>
    <div class="col-sm-6">
        <p class="form-control-static"><?php echo $v['zip_code'] ?></p>
    </div>
</div>
<br >
<h4>Survey Answers</h4>
<div class="form-group">
    <span class="col-sm-6 help-block">What brand do you smoke most frequently?</span>
<p class="form-control-static"><?php echo @$brands[$v['current_brand']]['brand_name'] ?></p>  
</div>
<div class="form-group">
    <span class="col-sm-6 help-block">What other brands do you smoke aside from your regular brand?</span>
<p class="form-control-static"><?php echo @$brands[$v['first_alternate_brand']]['brand_name'] ?></p>  
</div>
<div class="form-group">
    <span class="col-sm-6 help-block">What would you do if your regular brand is unavailable?</span>
<p class="form-control-static"><?php echo @$alternate[$v['alternate_purchase_indicator']]['alternate_value'] ?></p> 
</div>
<div class="form-group control-label">
    <div class="col-sm-offset-2 col-sm-10">
         <input type="hidden" name="submit" value="1" />
         <input type="hidden" name="id" value="<?php echo $this->uri->segment(3) ?>" />
         <button name="search" class="btn close-bootbox" type="button">Cancel</button>
    </div>
</div>
