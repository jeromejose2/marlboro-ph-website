<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3>Community Manager</h3>
      </div>
       <div class="alert alert-danger" <?php if(!$error) echo 'style="display:none;"' ?>><?php echo $error; ?></div>
       <form class="form-horizontal" role="form" method="post" onsubmit="return submitFrm()" enctype="multipart/form-data">
           <div class="form-group">
                <label class="col-sm-2 control-label">First Name</label>
                <div class="col-sm-4">
                     <input type="text" name="first_name" value="<?php echo isset($record['first_name']) ? $record['first_name'] : '' ?>" data-name="first name" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Last Name</label>
                <div class="col-sm-4">
                     <input type="text" name="third_name" value="<?php echo isset($record['third_name']) ? $record['third_name'] : '' ?>" data-name="last name" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Nickname</label>
                <div class="col-sm-4">
                     <input type="text" name="nick_name" value="<?php echo isset($record['nick_name']) ? $record['nick_name'] : '' ?>" data-name="nick name" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-4">
                     <input type="text" name="email_address" id="email" value="<?php echo isset($record['email_address']) ? $record['email_address'] : '' ?>" data-name="email address" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Password</label>
                <div class="col-sm-4">
                     <input type="password" id="password1" name="password" value="" data-name="password" class="<?php if(!isset($is_edit)){?> required <?php } ?> form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Confirm Password</label>
                <div class="col-sm-4">
                     <input type="password" id="password2" data-name="confirm password" class="<?php if(!isset($is_edit)){?> required <?php } ?> form-control">
                </div>
           </div> 
           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <input type="hidden" name="id" value="<?php echo isset($record['registrant_id']) ? $record['registrant_id'] : '' ?>">
                     <button type="submit" id="submit" name="submit" class="btn btn-primary" value="1">Submit</button>
                </div>
           </div>
      </form>
      
 </div>
 <!--end main content -->
 