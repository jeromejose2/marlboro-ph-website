<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3> <?php echo $header_text; ?> <span class="badge badge-important"><?php echo (int)$total_rows; ?></span></h3>

               <div class="actions">
                   <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/export'.$query_strings; ?>" class="btn btn-primary">Export</a> 
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                          <th>Person ID</th>
                          <th>Name</th>
                         <th>Prize</th>
                         <th>Thumbnail</th>
                         <th>Type</th>
                         <th>Status</th>
                          <th>Date Availed</th>
                         <th>Date Claimed</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
                    <form action="<?php echo SITE_URL ?>/user_flash_offers">
                     <tr>
                        <td></td>
                          <td><input name="name" class="form-control" value="<?php echo $this->input->get('name') ?>" /></td>
                         <td><input name="prize_name" class="form-control" value="<?php echo $this->input->get('prize_name') ?>" /></td>
                         <td></td>
                          <td>
                          <select name="prize_type" class="form-control">
                            <option></option>
                            <option value="Text" <?php echo $this->input->get('prize_type') === 'Text' ? 'selected' : '' ?>>Text</option>
                            <option value="Photo" <?php echo $this->input->get('prize_type') == 'Photo' ? 'selected' : '' ?>>Photo</option>
                            <option value="Video" <?php echo $this->input->get('prize_type') == 'Video' ? 'selected' : '' ?>>Video</option>
                          </select>
                        </td>
                         <td>
                          <select name="prize_status" class="form-control">
                            <option></option>
                            <option value="0" <?php echo $this->input->get('prize_status') === '0' ? 'selected' : '' ?>>Pending</option>
                            <option value="1" <?php echo $this->input->get('prize_status') == '1' ? 'selected' : '' ?>>Delivered</option>
                            <option value="1" <?php echo $this->input->get('prize_status') == '2' ? 'selected' : '' ?>>Redeemed</option>
                          </select>
                        </td>
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                     <input name="fromavail" placeholder="From" class="form-control from" value="<?php echo isset($_GET['fromavail']) ? $_GET['fromavail'] : '' ?>" />
                                </div>
                                <div class="col-md-4">
                                     <input name="toavail" placeholder="To" class="form-control to" value="<?php echo isset($_GET['toavail']) ? $_GET['toavail'] : '' ?>" />
                                </div>
                              </div>
                        </td>
                        <td>
                            <div class="row">
                              <div class="col-md-4">
                                   <input name="fromredeem" placeholder="From" class="form-control from" value="<?php echo isset($_GET['fromredeem']) ? $_GET['fromredeem'] : '' ?>" />
                              </div>
                              <div class="col-md-4">
                                   <input name="toredeem" placeholder="To" class="form-control to" value="<?php echo isset($_GET['toredeem']) ? $_GET['toredeem'] : '' ?>" />
                              </div>
                            </div>
                        </td>
                        <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
                       <?php if($rows->num_rows()){ 
                              $controller = $this->router->class;
                              $base_path = SITE_URL.'/'.$controller.'/';
                              foreach($rows->result() as $v){  ?>
                                <tr>
                                    <td><?php echo $v->person_id ?></td>
                                     <td><a href="#" onclick="showRegistrantDetails(<?php echo $v->registrant_id; ?>)"><?=ucwords($v->first_name.' '.$v->third_name)?></a></td>
                                     <td><?=$v->prize_name?></td>
                                     <td>  
                                      <?php if(file_exists('uploads/prize/'.$v->media) && strtolower($v->prize_type)=='photo'){ ?>
                                                <img src="<?=BASE_URL.'uploads/prize/'.$v->media?>"/>
                                      <?php }else if(file_exists('uploads/prize/'.$v->media) && strtolower($v->prize_type)=='video'){
                                              $video_mime_type = get_mime_by_extension('uploads/prize/'.$v->media);
                                              $file = explode('.',$v->media);
                                              $filename = 'thumb_'.$file[0].'.jpg'; ?> 
                                              <img width="150" src="<?=BASE_URL.'uploads/prize/'.$filename?>"/>
                                      <?php } ?>
                                      </td>
                                     <td><?=$v->prize_type?></th>
                                     <td>
                                      <?php if($v->prize_status=='1'){
                                            echo 'Delivered';
                                      }else if($v->prize_status=='2'){
                                            echo 'Redeemed';
                                      }else{
                                          echo 'Pending';
                                      } ?>
                                     </td>
                                     
                                     <td><?=date('F d, Y',strtotime($v->date_created))?></td>
                                     <td><?=$v->prize_status ? date('F d, Y H:i',strtotime($v->date_claimed)) : 'N/A'?></td>
                                     <td>
                                        <a class="btn btn-primary" href="<?=$base_path?>edit/<?=$v->user_flash_prize_id?>">Edit</a>
                                        <?php if(!$v->prize_status && $v->send_type=='Delivery'){ ?>
                                            <a class="btn btn-warning" href="<?=SITE_URL?>/user_flash_offers/update_status/<?=$v->user_flash_prize_id?>/<?=md5($v->user_flash_prize_id. ' ' . $this->config->item('encryption_key'))?>/1" onclick="return confirmAction(this,'redeem');">Deliver</a>
                                        <?php } ?>

                                        <?php if(!$v->prize_status && $v->send_type=='Redemption'){ ?>
                                            <a class="btn btn-warning" href="<?=SITE_URL?>/user_flash_offers/update_status/<?=$v->user_flash_prize_id?>/<?=md5($v->user_flash_prize_id. ' ' . $this->config->item('encryption_key'))?>/2" onclick="return confirmAction(this,'redeem');">Redeem</a>
                                        <?php } ?>
                                        
                                        </td>
                                </tr>
                      <?php }
                      }else{ ?>
                      <tr>
                          <td colspan="9">No result found.</td>
                      </tr>
                      <?php } ?>                     
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->