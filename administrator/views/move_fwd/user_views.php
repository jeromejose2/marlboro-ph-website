<div class="modal-header">
   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   <h4 class="modal-title"><?=$registrant ? ucwords($registrant->first_name.' '.$registrant->third_name).'\'s View(s)' : ''?></h4>
  </div>

<div class="modal-body text-center">
   <table class="table table-bordered">
	   	<thead>
	        <tr>
 	              <th>#</th>
 	              <th>Date Visited</th>                      
	             
	        </tr>
	    </thead>
	    <tbody>
	        <?php if($rows->num_rows()){
	        		foreach($rows->result() as $k => $v){ 
	        			 ?>
	        		<tr>
	        			<td><?=$k + 1?></th>
	        			<td><?=date('F d, Y H:i:s', strtotime($v->date_visited))?></td>
	        		</tr>
	        <?php } 
	    }else{ ?>
	        	<tr><td colspan="3">No results found.</th></tr>
	    	<?php } ?>
	    </tbody>
	</table>
</div>