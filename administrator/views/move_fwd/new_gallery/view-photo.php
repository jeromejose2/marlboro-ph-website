<div class="modal-header">
   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   <h4 class="modal-title"> </h4>
  </div>

<div class="modal-body text-center">
  <?php if($row){ 

  		if($row->comment_video){
  			$file_source = 'uploads/profile/admin_'.$row->admin_id.'/comments/'.$row->comment_video;
  			$mime_type = get_mime_by_extension($file_source); ?>    
  			<video width="100%" height="100%" controls>
  				<source src="<?=BASE_URL.$file_source?>" type="<?=$mime_type?>">
  			</video>
  	<?php }else{ 
  			$file_source = BASE_URL.'uploads/profile/admin_'.$row->admin_id.'/comments/'.$row->comment_image;?>
  			<img style="max-width:100%;" src="<?=$file_source?>" />
  	<?php }

  	} ?>
</div>