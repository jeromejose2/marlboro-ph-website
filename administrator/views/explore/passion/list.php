<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3> <?php echo $header_text; ?> <span class="badge badge-important"><?php echo (int)$total_rows; ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/add'; ?>" class="btn btn-primary">Add</a>
                   	<?php endif; ?>
                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/export'; ?>" class="btn btn-primary">Export</a>   
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th><center>Photo</center></th>
                         <th>Title</th>
                         <th>Description</th>
                         <th>Category</th>
                         <th>Status</th>
                          <?php if($edit || $delete) : ?>
                         <th>Operation</th>
                         <?php endif; ?>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>">
                     <tr>
                       <th></th>
                       <th><input type="text" name="title" class="form-control" value="<?php echo $this->input->get('title'); ?>"></th>
                       <th><input type="text" name="description" class="form-control" value="<?php echo $this->input->get('description'); ?>"></th>
                       <th><select name="category_id" class="form-control">
                            <option value=""></option>
                            <?php if($categories->num_rows()){ 
                                    foreach($categories->result() as $v){ 
                                      if($this->input->get('category_id')==$v->category_id){ ?>
                                            <option value="<?php echo $v->category_id; ?>" selected="selected"><?php echo $v->category_name; ?></option>
                                    <?php }else{ ?>
                                            <option value="<?php echo $v->category_id; ?>"><?php echo $v->category_name; ?></option>
                                    <?php }
                                    }
                                  } ?>
                                  </th>
                       <th><select name="status" class="form-control">
                              <option value=""></option>
                              <option value="2" <?php echo ($this->input->get('status')==2) ? 'selected' : ''; ?> >Unpublished</option>
                              <option value="1" <?php echo ($this->input->get('status')==1) ? 'selected' : ''; ?> >Published</option>
                            </select></th>
                       <?php if($edit || $delete) : ?>
                       <th><button type="submit" class="btn btn-primary">Filter</button></th>
                       <?php endif; ?>
                     </tr>
                  </form>
                  <?php if($rows->num_rows()){

                            $edit_url = SITE_URL.'/'.$this->uri->segment(1).'/edit/';
 
                            foreach($rows->result() as $v){ ?>
                              <tr>
                                  <td><center><img src="<?php echo BASE_URL.'/uploads/explore/'.$this->uri->segment(1).'/50_50_'.$v->image; ?>" /></center></td>
                                  <td><?php echo $v->title; ?></td>
                                  <td><?php echo $v->description; ?></td>
                                  <td><?php echo $v->category_name; ?></td>
                                  <td><?php echo ($v->status==1) ? 'Published' : 'Unpublished'; ?></td>
                                  <?php if($edit || $delete) : ?>
                                  <td>
                                    <a href="<?php echo $edit_url.$v->{$this->uri->segment(1).'_id'}; ?>" class="btn btn-primary">Edit</a>
                                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/delete/'.$v->{$this->uri->segment(1).'_id'} .'/'.md5($v->{$this->uri->segment(1).'_id'}. ' ' . $this->config->item('encryption_key')); ?>" onclick="return confirmDeletion(this, '<?php echo $this->uri->segment(1); ?>')" class="btn btn-danger">Delete</a>
                                  </td>
                                <?php endif; ?>
                              </tr>
                  <?php     }
                        }else{ ?>
                          <tr><td colspan="5">No records found.</td></tr>
                  <?php  } ?>
                  
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->