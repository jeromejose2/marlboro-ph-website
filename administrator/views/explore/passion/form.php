<?php $this->load->view('editor.php') ?>
<script src="<?php echo BASE_URL ?>admin_assets/js/jquery.Jcrop.js"></script>
<script type="text/javascript">
var jscrop_api;
function initJCrop(elem) {
  <?php if($row && !empty($row->image_crop_coordinates)):  
    $points = unserialize($row->image_crop_coordinates); ?>
    x1 = <?php echo $points['x1'] ?>;
    x2 = <?php echo $points['x2'] ?>;
    y1 = <?php echo $points['y1'] ?>;
    y2 = <?php echo $points['y2'] ?>;
  <?php else: ?>  
    x1 = 0;
    y1 = 0;
    x2 = 200;
    y2 = 200;     
  <?php endif; ?>
  
setTimeout(function() {
    if(jscrop_api)
      jcrop_api.destroy();
    $('#preview').Jcrop({
      onChange:   showCoords,
      onSelect:   showCoords,
      setSelect: [x1, y1, x2, y2],
      maxSize: [ 300, 300 ]
    },function(){
      jcrop_api = this;
    }); 
    $('#width').val($('#preview').width());
    $('#height').val($('#preview').height());
  }, 500);

}

function showCoords(c) {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
};

$(function() {
  <?php if($row && $row->image): ?>
  initJCrop($('#photo'));
  <?php endif; ?>
});

</script>
<link rel="stylesheet" href="<?php echo BASE_URL ?>admin_assets/css/jquery.Jcrop.css" type="text/css" />
<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3><?php echo $header_text; ?></h3>
      </div>

      <?php if($error){ ?>
       <div class="alert alert-danger"> <?php echo $error; ?> </div>
      <?php } ?>

       <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
           <div class="form-group">
                <label class="col-sm-2 control-label">Title</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo ($row) ? $row->title : '';  ?>" name="title" class="form-control required" data-name="statement">
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Category</label>
                <div class="col-sm-4">
                  <select name="category_id" class="form-control">
                    <option value=""></option>
                  <?php if($categories->num_rows()){ 
                          foreach($categories->result() as $v){ 
                              if($row && $v->category_id==$row->category_id){?>
                                  <option value="<?php echo $v->category_id; ?>" selected="selected"><?php echo $v->category_name; ?></option>
                          <?php }else{ ?>
                                  <option value="<?php echo $v->category_id; ?>"><?php echo $v->category_name; ?></option>
                          <?php }
                          }
                        } ?>
                  </select>

                </div>
           </div>


           <div class="form-group">
                <label class="col-sm-2 control-label">Image</label>
                <div class="col-sm-4">
                     <input type="file" id="photo" name="image" value="" data-name="module name" class="form-control" onchange="previewImage('photo', 'preview', initJCrop, this);">
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Image Preview</label>
                <div class="col-sm-4">
                   <img id="preview" class="preview" <?php echo ($row) ? "style='max-width:500px;' src='" . BASE_URL . 'uploads/explore/'.$this->uri->segment(1).'/' . $row->image. "'" : '' ?>  />
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-4">
                     <textarea name="description"><?php echo ($row) ? $row->description : ''; ?></textarea>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Status</label>
                <div class="col-sm-4">
                	<select class="form-control" name="status">
                     	<option value="1" <?php echo ($row && $row->status==APPROVED) ? 'selected' : '' ?>>Published</option>
                        <option value="2" <?php echo ($row && $row->status==DISAPPROVED) ? 'selected' : '' ?>>Unpublished</option>
                    </select>
                </div>
           </div>
           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                  
                     <input type="hidden" id="x" name="x"  />
                     <input type="hidden" id="y" name="y" />
                     <input type="hidden" id="x2" name="x2" />
                     <input type="hidden" id="y2" name="y2" />
                     <input type="hidden" id="width" name="width" />
                     <input type="hidden" id="height" name="height"/>
                     <input type="hidden" name="current_image" value="<?php echo $row && $row ? $row->image : ''; ?>"/>
                     <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>" class="pull-left"><< Back</a>
                      <button type="submit" class="btn btn-primary pull-right">Submit</button>

                </div>
           </div>
           
           
      </form>
      
 </div>
 <!--end main content -->
 