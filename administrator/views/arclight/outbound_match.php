<!--start main content -->
<div class="container main-content">
	<div class="page-header">
			 <h3>Arclight Outbound Match<span class="badge badge-important"><?= $outbound_records_count ?></span></h3>
	</div>
	
	<div class="col-md-12">
		<form class="form-inline" role="form" id="reg-form" method="GET" action="<?= SITE_URL ?>/arclight/outbound_match">
	        <div class="form-group col-md-6">
	            <label class="sr-only" for="login-username">Filename</label>
	            <input type="text" name="outbound_filename" class="form-control" placeholder="Enter filename" value="<?= $this->input->get('outbound_filename') ?>">
	        </div>
	        <div class="form-group">
	        	<button type="submit" class="btn btn-default">Submit</button>
	        	<?php if ($success_inserted): ?>
	        	<span class="label label-success">Insert success - <?= $date_inserted ?></span>
	        	<?php elseif ($registrants_not_inserted): ?>
	        	<button type="submit" name="insert" value="1" class="btn btn-primary">Insert</button>
	        	<?php endif ?>
	        </div>
	    </form>
	    <br>
	    <?php if ($not_found === false): ?>
	    Not Found!
		<?php endif ?>
	</div>
	<?php if ($duplicates): ?>
	    <h4>Records with duplicate emails : <?= array_sum($duplicates) ?></h4>
		<?php endif ?>
	<table class="table table-bordered">
		<tbody>
<!-- 			<form action="<?= SITE_URL ?>/arclight/outbound">
				<tr>
					<td></td>
					<td><input name="filename" placeholder="Filename" class="form-control" value="<?= $this->input->get('filename') ? $this->input->get('filename') : '' ?>" /></td>
					<td>
						<div class="row">
							<div class="col-xs-5 col-lg-4">
								<input name="from_date" placeholder="From" class="form-control from" value="<?= $this->input->get('from_date') ? $this->input->get('from_date') : '' ?>" />
							</div>
							<div class="col-xs-5 col-lg-4">
								<input name="to_date" placeholder="To" class="form-control to" value="<?= $this->input->get('to_date') ? $this->input->get('to_date') : '' ?>" />
							</div>
						</div>
					</td>
					<td><button class="btn btn-primary" name="search" type="submit" value="1">Go</button></td>
				</tr>
			</form> -->
			<?php foreach ($duplicates as $k => $record): ?>
			<tr>
				<td><?= $k ?></td>
				<td><?= $record ?></td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	<?php if ($duplicates_info): ?>
	    <h4>Duplicates Info</h4>
		<?php endif ?>
	<table class="table table-bordered">
		<thead>
			<?php if ($duplicates_info): $first = current($duplicates_info)[0];?>
			<tr>
				<?php foreach (array_keys($first) as $field): ?>
				 <th><?= $field ?></th>
				 <?php endforeach ?>
			</tr>
			<?php endif ?>
		</thead>
		<tbody>
<!-- 			<form action="<?= SITE_URL ?>/arclight/outbound">
				<tr>
					<td></td>
					<td><input name="filename" placeholder="Filename" class="form-control" value="<?= $this->input->get('filename') ? $this->input->get('filename') : '' ?>" /></td>
					<td>
						<div class="row">
							<div class="col-xs-5 col-lg-4">
								<input name="from_date" placeholder="From" class="form-control from" value="<?= $this->input->get('from_date') ? $this->input->get('from_date') : '' ?>" />
							</div>
							<div class="col-xs-5 col-lg-4">
								<input name="to_date" placeholder="To" class="form-control to" value="<?= $this->input->get('to_date') ? $this->input->get('to_date') : '' ?>" />
							</div>
						</div>
					</td>
					<td><button class="btn btn-primary" name="search" type="submit" value="1">Go</button></td>
				</tr>
			</form> -->
			<?php foreach ($duplicates_info as $k => $record): ?>
				<?php foreach ($record as $key => $val): ?>
				<tr>
				<?php foreach ($val as $v): ?>
				<td><?= $v ? $v : '' ?></td>
				<?php endforeach ?>
				</tr>
				<?php endforeach ?>
			<?php endforeach ?>
		</tbody>
	</table>
	<?php if ($registrants_not_inserted): ?>
	    <h4>Non existing registrants : <?= count($registrants_not_inserted) ?></h4>
		<?php endif ?>
	<table class="table table-bordered">
		<thead>
			<?php if ($registrants_not_inserted): ?>
			<tr>
				<?php foreach (array_keys($registrants_not_inserted[0]) as $field): ?>
				 <th><?= $field ?></th>
				 <?php endforeach ?>
			</tr>
			<?php endif ?>
		</thead>
		<tbody>
<!-- 			<form action="<?= SITE_URL ?>/arclight/outbound">
				<tr>
					<td></td>
					<td><input name="filename" placeholder="Filename" class="form-control" value="<?= $this->input->get('filename') ? $this->input->get('filename') : '' ?>" /></td>
					<td>
						<div class="row">
							<div class="col-xs-5 col-lg-4">
								<input name="from_date" placeholder="From" class="form-control from" value="<?= $this->input->get('from_date') ? $this->input->get('from_date') : '' ?>" />
							</div>
							<div class="col-xs-5 col-lg-4">
								<input name="to_date" placeholder="To" class="form-control to" value="<?= $this->input->get('to_date') ? $this->input->get('to_date') : '' ?>" />
							</div>
						</div>
					</td>
					<td><button class="btn btn-primary" name="search" type="submit" value="1">Go</button></td>
				</tr>
			</form> -->
			<?php foreach ($registrants_not_inserted as $k => $record): ?>
			<tr>
				<?php foreach ($record as $key => $val): ?>
				<td><?= $val ? $val : '' ?></td>
				<?php endforeach ?>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>

	<?php if ($registrants_inserted): ?>
	    <h4>Existing registrants : <?= count($registrants_inserted) ?></h4>
		<?php endif ?>
	<table class="table table-bordered">
		<thead>
			<?php if ($registrants_inserted): ?>
			<tr>
				<?php foreach (array_keys($registrants_inserted[0]) as $field): ?>
				 <th><?= $field ?></th>
				 <?php endforeach ?>
			</tr>
			<?php endif ?>
		</thead>
		<tbody>
<!-- 			<form action="<?= SITE_URL ?>/arclight/outbound">
				<tr>
					<td></td>
					<td><input name="filename" placeholder="Filename" class="form-control" value="<?= $this->input->get('filename') ? $this->input->get('filename') : '' ?>" /></td>
					<td>
						<div class="row">
							<div class="col-xs-5 col-lg-4">
								<input name="from_date" placeholder="From" class="form-control from" value="<?= $this->input->get('from_date') ? $this->input->get('from_date') : '' ?>" />
							</div>
							<div class="col-xs-5 col-lg-4">
								<input name="to_date" placeholder="To" class="form-control to" value="<?= $this->input->get('to_date') ? $this->input->get('to_date') : '' ?>" />
							</div>
						</div>
					</td>
					<td><button class="btn btn-primary" name="search" type="submit" value="1">Go</button></td>
				</tr>
			</form> -->
			<?php foreach ($registrants_inserted as $k => $record): ?>
			<tr>
				<?php foreach ($record as $key => $val): ?>
				<td><?= $val ? $val : '' ?></td>
				<?php endforeach ?>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>

</div>