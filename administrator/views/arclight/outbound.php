<!--start main content -->
		 <div class="container main-content">
					<div class="page-header">
							 <h3>Arclight Outbound <span class="badge badge-important"><?= $total ?></span></h3>

							 <?php /*<div class="actions">
								<button class="btn btn-primary" id="btn-update-arclight-outbound">Pull Data</button>
								<button class="btn btn-primary" id="btn-match-arclight-outbound">Match Records</button>
							 </div>*/ ?>
					</div>
					
					<table class="table table-bordered">
						<thead>
							<tr>
									 <th>#</th>
									 <th>Filename</th>
									 <th>Date Fetched</th>
									 <th></th>
							</tr>
						</thead>
						<tbody>
							<form action="<?= SITE_URL ?>/arclight/outbound">
								<tr>
									<td></td>
									<td><input name="filename" placeholder="Filename" class="form-control" value="<?= $this->input->get('filename') ? $this->input->get('filename') : '' ?>" /></td>
									<td>
										<div class="row">
											<div class="col-xs-5 col-lg-4">
												<input name="from_date" placeholder="From" class="form-control from" value="<?= $this->input->get('from_date') ? $this->input->get('from_date') : '' ?>" />
											</div>
											<div class="col-xs-5 col-lg-4">
												<input name="to_date" placeholder="To" class="form-control to" value="<?= $this->input->get('to_date') ? $this->input->get('to_date') : '' ?>" />
											</div>
										</div>
									</td>
									<td><button class="btn btn-primary" name="search" type="submit" value="1">Go</button></td>
								</tr>
							</form>
							<?php foreach ($records as $k => $record): ?>
							<tr>
								<td><?= $offset + $k + 1 ?></td>
								<td><?= $record->outbound_filename ?></td>
								<td><?= date('F d, Y l', strtotime($record->date_fetched)) ?></td>
								<td></td>
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>

					<ul class="pagination pagination-sm pull-right">
							 <?= !empty($pagination) ? $pagination : '' ?>
					</ul>

		 </div>
		 <div id="match-records-modal" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Match Records</h4>
					</div>
					<div class="modal-body">
						<div class="alert alert-danger hide" id="error-match-records">Please fill in filename</div>
						<form id="match-records-form" role="form" method="GET" action="<?= SITE_URL.'/arclight/outbound_match' ?>">
							<div class="form-group">
								<label for="outbound-filename">Filename</label>
								<input type="text" class="form-control" name="outbound_filename" id="match-records-filename" placeholder="Enter filename">
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" id="btn-match-records" class="btn btn-primary">OK</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		 <div id="pull-data-modal" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Pull Data</h4>
					</div>
					<div class="modal-body">
						<div class="alert alert-danger hide" id="error-pull-data">Please fill in filename</div>
						<form id="pull-data-form" role="form" method="POST" action="<?= SITE_URL.'/arclight/outbound_fetch' ?>">
							<div class="form-group">
								<label for="outbound-filename">Filename</label>
								<input type="text" class="form-control" name="outbound_filename" id="outbound-filename" placeholder="Enter filename">
							</div>
							<div class="checkbox">
								<label>
									<input id="send-with-email" type="checkbox" value="1" name="outbound_with_email" checked="checked"> Send email with designated password
								</label>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" id="btn-pull-data" class="btn btn-primary">OK</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		 <!--end main content -->
		 <script>
			$("#btn-update-arclight-outbound").click( function() {
				$("#pull-data-modal").modal();
			});
			$("#btn-match-arclight-outbound").click( function() {
				$("#match-records-modal").modal();
			});
			$("#btn-match-records").click( function() {
				var filename = $("#match-records-filename").val();
				if (!filename) {
					$("#error-match-records").removeClass("hide");
					return;
				}
				$("#error-match-records").addClass("hide");
				$("#match-records-form").submit();
			});
			$("#btn-pull-data").click( function() {
				var filename = $("#outbound-filename").val();
				if (!filename) {
					$("#error-pull-data").removeClass("hide");
					return;
				}
				$("#error-pull-data").addClass("hide");
				if ($("#send-with-email").is(":checked")) {
					$("#pull-data-modal").modal('hide');
					bootbox.confirm("This will send email to each pulled users", function(yes) {
						if (yes) {
							$("#pull-data-form").submit();
						} else {
							$("#pull-data-modal").modal();
						}
					});
				} else {
					$("#pull-data-form").submit();
				}
			});
		</script>
