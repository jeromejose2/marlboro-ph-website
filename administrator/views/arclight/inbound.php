<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Arclight Inbound <span class="badge badge-important"><?= $total ?></span></h3>

               <div class="actions">
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Filename</th>
                         <th>Records</th>
                         <th>Success</th>
                         <th>Process Date</th>
                         <th></th>
                    </tr>
               </thead>
               <tbody>
                   <form action="<?= SITE_URL ?>/arclight/inbound">
                         <tr>
                             <td></td>
                             <td><input name="filename" placeholder="Filename" class="form-control" value="<?= $this->input->get('filename') ? $this->input->get('filename') : '' ?>" /></td>
                             <td></td>
                             <td></td>
                             <td>
                              <div class="row">
                                   <div class="col-xs-5 col-lg-4">
                                        <input name="from_date" placeholder="From" class="form-control from" value="<?= $this->input->get('from_date') ? $this->input->get('from_date') : '' ?>" />
                                   </div>
                                   <div class="col-xs-5 col-lg-4">
                                        <input name="to_date" placeholder="To" class="form-control to" value="<?= $this->input->get('to_date') ? $this->input->get('to_date') : '' ?>" />
                                   </div>
                              </div>
                              </td>
                             <td><button class="btn btn-primary" name="search" type="submit" value="1">Go</button></td>
                        </tr>
                    </form>
                    <?php foreach ($records as $k => $record): ?>
                      <tr>
                             <td><?= $offset + $k + 1 ?></td>
                             <td><a target="_blank" href="<?= BASE_URL ?>uploads/inbound/<?= $record->inbound_filename.'.xml' ?>"><?= $record->inbound_filename.'.xml' ?></a></td>
                             <td><?= $record->inbound_records ?></td>
                             <td><?= !$record->inbound_failed ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>' ?></td>
                             <td><?= date('F d, Y l', strtotime($record->date_of_processing)) ?></td>
                             <td></td>
                        </tr>
                    <?php endforeach ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?= !empty($pagination) ? $pagination : '' ?>
          </ul>

     </div>
     <!--end main content -->