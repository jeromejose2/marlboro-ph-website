
     
<!--start main content -->
     <div class="container main-content" ng-app="demographic_age" ng-controller="ageController">
          <div class="page-header">
               <h3>Age Demographics</h3>
               <div class="actions">
                    <a href="{{ exportTest }}" class="btn btn-primary">Export</a>  
                   </div>
		      </div>
      <!-- Nav tabs --> 
          <ul class="nav nav-tabs">
            <li><a href="#graph" ng-click="setExportButton('<?=SITE_URL?>/demographics/export?<?php echo http_build_query($query_string); ?>')" id="graph-btn" data-toggle="tab">Graph</a></li>
            <li><a href="#summary" ng-click="setExportButton('<?=SITE_URL?>/demographics/export?<?php echo http_build_query($query_string); ?>')" id="summary-btn" data-toggle="tab">Summary</a></li>
            <li><a href="#summary_by_range" ng-click="setExportButton('<?=SITE_URL?>/demographics/export_age_by_range?<?php echo http_build_query($query_string); ?>')" id="summary_by_range-btn" data-toggle="tab">Summary by Range</a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">


            <div class="tab-pane" id="summary">

                <table class="table table-bordered" style="border-top:none;">
                     <thead>
                          <tr>
                               <th>#</th>
                               <th>Age</th>
                               <th>Count</th>
                               <th>Percentage</th>
                               <th>Operation</th>
                          </tr>
                     </thead>
                     <tbody>
                          <form>
                            <input type="hidden" name="current_tab" value="summary">
                          <tr>
                              <td></td>
                              <td>
                                  From: <input style="width:10%;display:inline;" name="fromage" class="form-control" value="<?php echo @$query_string['fromage']; ?>" />
                                  To: <input style="width:10%;display:inline;" name="toage" class="form-control" value="<?php echo @$query_string['toage'] ?>" />
                                  

                                  </select>
                              </td>
                              <td></td>
                              <td></td>
                              <td>From: <input name="fromdate" class="form-control from" value="<?php echo $query_string['from']; ?>" /><br> 
                                To: <input name="todate" class="form-control to" value="<?php echo $query_string['to']; ?>" />
                                <input name="type" type="hidden" value="<?php echo $this->input->get('type') ? $this->input->get('type') : 'age' ?>" /><br>
                                <input name="type" type="hidden" value="<?php echo $this->input->get('type') ? $this->input->get('type') : 'age' ?>" /><br>

                                Sort By:
                                  <select name="sort_by" class="form-control from">
                                    <option <?=$this->input->get('sort_by')=='age' ? 'selected' : ''?> value="age">Age</option>
                                    <option <?=$this->input->get('sort_by')=='count' ? 'selected' : ''?> value="count">Count</option>
                                    <option <?=$this->input->get('sort_by')=='percentage' ? 'selected' : ''?> value="percentage">Percentage</option>
                                  </select><br>
                                  Sort Order:  
                                  <select name="sort_order" class="form-control from">
                                    <option <?=$this->input->get('sort_order')=='ASC' ? 'selected' : ''?>>ASC</option>
                                    <option <?=$this->input->get('sort_order')=='DESC' ? 'selected' : ''?>>DESC</option>
                                   </select><br>
                                <button class="btn btn-primary" name="search" value="1">Go</button></td>
                          </tr>
                          </form>
                     		<?php if($records): 
                        $total_percentage = 0;
                        $percentage = 0;
                        $range = array('18-24'=>array('18','24'),'25-29'=>array('25','29'),'30-39'=>array('30','39'),'40-49+'=>array('40','200'));
                        $age_ranges = array('18-24'=>0,'25-29'=>0,'30-39'=>0,'40-49+'=>0);
                        foreach($records as $k => $v):
                          $percentage = round(($v['count'] / $total) * 100, 2);
                          $total_percentage += $percentage;
                          foreach($range as $key=>$r){
                            
                            if($v['age'] >=$r[0] && $v['age'] <= $r[1]){
                               $age_ranges[$key] += $v['count'] ;
                              break;
                            }

                          }?>
                          <tr>
                               <td><?php echo $k + 1 ?></td>
                               <td><?php echo $v['age']==date('Y') ? 'N/A' : $v['age'] ?></td>
                               <td><?php echo $v['count'] ?></td>
                               <td><?php echo $percentage; ?>%</td>
                               <td></td>
                          </tr>
                          <?php 
      					endforeach; ?>
                        <tr>
                            <td></td>
                            <td><strong>Total</strong></td>
                            <td><?=$total?></td>
                            <td><?=round($total_percentage,0)?>%</td>
                        </tr>
      					<?php else:
      						echo '<tr><td colspan="7">No records found</td></tr>';
      					endif; ?>
                     </tbody>
                </table>
            </div>

            <div class="tab-pane active" id="summary_by_range">
              <table class="table table-bordered" style="border-top:none;">
                     <thead>
                          <tr>
                               <th>#</th>
                               <th>Age</th>
                               <th>Count</th>
                               <th>Percentage</th>
                               <th>Operation</th>
                          </tr>
                          <form>
                            <input type="hidden" name="current_tab" value="summary_by_range">
                          <tr>
                              <td></td>
                              <td>
                                  From: <input style="width:10%;display:inline;" name="fromage" class="form-control" value="<?php echo @$query_string['fromage']; ?>" />
                                  To: <input style="width:10%;display:inline;" name="toage" class="form-control" value="<?php echo @$query_string['toage'] ?>" />
                                  

                                  </select>
                              </td>
                              <td></td>
                              <td></td>
                              <td>From: <input name="fromdate" class="form-control from" value="<?php echo $query_string['from']; ?>" /><br> 
                                To: <input name="todate" class="form-control to" value="<?php echo $query_string['to']; ?>" />
                                <input name="type" type="hidden" value="<?php echo $this->input->get('type') ? $this->input->get('type') : 'age' ?>" /><br>
                                <input name="type" type="hidden" value="<?php echo $this->input->get('type') ? $this->input->get('type') : 'age' ?>" /><br>

                                Sort By:
                                  <select name="sort_by" class="form-control from">
                                    <option <?=$this->input->get('sort_by')=='age' ? 'selected' : ''?> value="age">Age</option>
                                    <option <?=$this->input->get('sort_by')=='count' ? 'selected' : ''?> value="count">Count</option>
                                    <option <?=$this->input->get('sort_by')=='percentage' ? 'selected' : ''?> value="percentage">Percentage</option>
                                  </select><br>
                                  Sort Order:  
                                  <select name="sort_order" class="form-control from">
                                    <option <?=$this->input->get('sort_order')=='ASC' ? 'selected' : ''?>>ASC</option>
                                    <option <?=$this->input->get('sort_order')=='DESC' ? 'selected' : ''?>>DESC</option>
                                   </select><br>
                                <button class="btn btn-primary" name="search" value="1">Go</button></td>
                          </tr>
                          </form>
                          <?php 
                          if($records){ 
                            $i = 0;
                            foreach($age_ranges as $key=>$ar){ 
                                  $i++;
                                  $percentage = round(($ar / $total) * 100, 2);?>
                                <tr>
                                     <td><?=$i?></td>
                                     <td><?=$key?></td>
                                     <td><?=$ar?></td>
                                     <td><?=$percentage?></td>
                                     <td></td>
                                </tr>
                            <?php } ?>
                                <tr>
                                  <td></td>
                                  <td><strong>Total</strong></td>
                                  <td><?=$total?></td>
                                  <td><?=round($total_percentage,0)?>%</td>
                                </tr>
                        <?php }else{ ?>
                                <tr><td colspan="7">No records found</td></tr>
                        <?php } ?>
                     </thead>
                     <tbody>
                     </tbody>
              </table>    
           </div>

            <div class="tab-pane active" id="graph">
                  <div id="graph_1" style="border:1px solid #DDDDDD; border-top:none;height:300px;">
                       
                  </div>

                   <div id="graph_2" style="border:1px solid #DDDDDD; border-top:none;height:300px;">
                       
                  </div>
                  <br >
                  <?php if($has_null) {
                      echo 'Disclaimer: These null values are users without Birthdays recorded <br><br>';
                    } ?>
               </div>

        </div>
     </div>
     <!--end main content -->
     <script type="text/javascript" src="<?php echo BASE_URL?>/admin_assets/js/angular-js/angular.js"></script> 
     <script type="text/javascript" src="<?php echo BASE_URL ?>admin_assets/js/google-api.js"></script>

     <script type="text/javascript">
     google.load("visualization", "1", {packages:["corechart"]});
     function drawGraph1() {
          var data = google.visualization.arrayToDataTable([
          ['Age', 'Value'],
          
           <?php foreach($records as $k => $v): ?>
          ["<?php echo  $v['age']==date('Y') ? 'N/A' : $v['age'] ?>", <?php echo $v['count'] ?>],
          <?php endforeach; ?>
        ]);
          
          var options = {
          title: 'Age Demographics <?php echo $query_string["from"] && $query_string["to"] ? "(" . date("F d, Y", strtotime($query_string["from"])) . " - " .  date("F d, Y", strtotime($query_string["to"])) . ")" : ""?>',
            is3D: true,
             sliceVisibilityThreshold: 0       
          };

        var chart = new google.visualization.PieChart(document.getElementById('graph_1'));
        chart.draw(data, options);

       
     }

     function drawGraph2() {
          var data = google.visualization.arrayToDataTable([
          ['Age Range', 'Value'],
          
           <?php foreach($age_ranges as $k => $v): ?>
          ["<?php echo  $v['age']==date('Y') ? 'N/A' : $k ?>", <?php echo $v ?>],
          <?php endforeach; ?>
        ]);
          
          var options = {
          title: 'Age by Range Demographics <?php echo $query_string["from"] && $query_string["to"] ? "(" . date("F d, Y", strtotime($query_string["from"])) . " - " .  date("F d, Y", strtotime($query_string["to"])) . ")" : ""?>',
            is3D: true,
             sliceVisibilityThreshold: 0       
          };

        var chart = new google.visualization.PieChart(document.getElementById('graph_2'));
        chart.draw(data, options);

         <?php if(!$this->input->get()){ ?>
              $('#graph-btn').trigger('click');
        <?php }else{ ?>
              $('#<?=$this->input->get("current_tab")?>-btn').trigger('click');
        <?php } ?>

     } 

     google.setOnLoadCallback(drawGraph1);
     google.setOnLoadCallback(drawGraph2);
     $(window).resize(drawGraph2); 


     var app = angular.module('demographic_age',[]);

      app.controller('ageController',['$scope',function($scope){
                
        $scope.setExportButton = function(href){
            $scope.exportTest = href;
        };

      }]);
       


 </script> 
 
  
