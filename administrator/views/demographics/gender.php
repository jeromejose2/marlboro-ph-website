<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Gender Demographics</h3>
               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/demographics/export?<?php echo http_build_query($query_string) ?>" class="btn btn-primary">Export</a>   
               </div>
		  </div>

          <!-- Nav tabs --> 
          <ul class="nav nav-tabs">
            <li class="active"><a href="#graph" data-toggle="tab">Graph</a></li>
            <li class=""><a href="#summary" data-toggle="tab">Summary</a></li>
          </ul>  

          <!-- Tab panes -->
          <div class="tab-content">
               <div class="tab-pane" id="summary">
                    <table class="table table-bordered" style="border-top:none;">
                         <thead>
                              <tr>
                                   <th width="1%">#</th>
                                   <th>Gender</th>
                                   <th>Count</th>
                                   <th>Percentage</th>
                                   <td>Operation</td>
                              </tr>
                         </thead>
                         <tbody>
                              <form>
                                   <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>From:<input name="fromdate" class="form-control from" value="<?php echo $query_string['from']; ?>" /><br> 
                                          To:<input name="todate" class="form-control to" value="<?php echo $query_string['to']; ?>" /><br>
                                          Sort By:
                                          <select name="sort_by" class="form-control from">
                                            <option <?=$this->input->get('sort_by')=='gender' ? 'selected' : ''?> value="gender">Gender</option>
                                            <option <?=$this->input->get('sort_by')=='count' ? 'selected' : ''?> value="count">Count</option>
                                            <option <?=$this->input->get('sort_by')=='percentage' ? 'selected' : ''?> value="percentage">Percentage</option>
                                          </select><br>
                                          Sort Order:  
                                          <select name="sort_order" class="form-control from">
                                            <option <?=$this->input->get('sort_order')=='ASC' ? 'selected' : ''?>>ASC</option>
                                            <option <?=$this->input->get('sort_order')=='DESC' ? 'selected' : ''?>>DESC</option>
                                           </select><br>
                                          <input name="type" type="hidden" value="<?php echo $this->input->get('type') ? $this->input->get('type') : 'age' ?>" /><br><button class="btn btn-primary" name="search" value="1">Go</button></td>
                                   </tr>
                              </form>
                         		<?php if($records): 
                            $total_count = 0;
                            $total_percentage = 0;
                            $percentage = 0;
          					foreach($records as $k => $v): 
                            $total_count += $v['count'];
                            $percentage = round(($v['count'] / $total) * 100,2);
                            $total_percentage += $percentage;?>
                              <tr>
                                   <td><?php echo $k + 1 ?></td>
                                   <td><?php echo ($v['gender']) ? $v['gender'] : '' ?></td>
                                   <td><?php echo $v['count'] ?></td>
                                   <td><?php echo $percentage; ?>%</td>
                                   <td></td>
                              </tr>
                              <?php 
          					endforeach; ?>
                            <tr>
                                <td></td>
                                <td><strong>Total</strong></td>
                                <td><?=$total_count?></td>
                                <td><?=round($total_percentage,0)?>%</td>
                            </tr>
          				<?php	else:
          						echo '<tr><td colspan="7">No records found</td></tr>';
          					endif; ?>
                         </tbody>
                    </table>
               </div>
               <div class="tab-pane active" id="graph">
                    <div id="chart_entries" style="border:1px solid #DDDDDD; border-top:none;height:300px;">
                         
                    </div>
                    <br >
                    <?php if($has_null) {
                      echo 'Disclaimer: These null values are users without Gender recorded<br><br>';
                    } ?>
                 </div>
           </div>
           
     </div>
     <!--end main content -->

     <script type="text/javascript" src="<?php echo BASE_URL ?>admin_assets/js/google-api.js"></script>
     <script type="text/javascript">
     google.load("visualization", "1", {packages:["corechart"]});
     function drawChart() {
          var data = google.visualization.arrayToDataTable([
          ['Status', 'Value'],
          
           <?php foreach($records as $k => $v): ?>
          ['<?php echo $v["gender"] ?>', <?php echo $v['count'] ?>],
          <?php endforeach; ?>
        ]);
          
          var options = {
          title: 'Gender Demographics <?php echo $query_string["from"] && $query_string["to"] ? "(" . date("F d, Y", strtotime($query_string["from"])) . " - " .  date("F d, Y", strtotime($query_string["to"])) . ")" : ""?>',
            is3D: true,
            /*colors:['#DDDDDD', '#90BC56','#3366CC', '#f00000'],*/
            sliceVisibilityThreshold: 0       
          };

        var chart = new google.visualization.PieChart(document.getElementById('chart_entries'));
        chart.draw(data, options);

        <?php if(count($this->input->get()) < 2){ ?>
              $('#graph-btn').trigger('click');
        <?php }else{ ?>
              $('#summary-btn').trigger('click');
        <?php } ?>
     }
     google.setOnLoadCallback(drawChart);
     $(window).resize(drawChart);
</script> 
  
