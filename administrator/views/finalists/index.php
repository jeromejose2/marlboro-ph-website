<?php 
  $controller = $this->uri->segment(1);
  $label = '';
  switch($controller) {
    case 'finalists':
      $label = 'Finalist';
      break;
    case 'birthday_offer':
      $label = 'Birthday Offer';
      break;
    case 'flash_offer':
      $label = 'Flash Offer';
      break;
    default:
      $label = 'Prize';
      break;

  }
?>


<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3><?php echo $label ?>s <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/add" class="btn btn-primary">Add <?php echo $label ?></a>
                    <?php endif; ?>
                    <?php /* <a href="<?php echo SITE_URL ?>/export/prizes?<?php echo @http_build_query($this->input->get()) ?>" class="btn btn-primary">Export</a> */ ?>
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Photos</th>
                         <th>Videos</th>
                         <th>Write-ups</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/<?php echo $controller ?>">
                     <?php /*<tr>
                         <td></td>
                         <td><input name="prize_name" class="form-control" value="<?php echo isset($_GET['prize_name']) ? $_GET['prize_name'] : '' ?>" /></td>
                         <td></td>
                         <!-- <td></td> -->
                         <td></td>
                         <td></td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>*/ ?>
                    </form>
                    <?php if($records): 
					foreach($records as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['name'] ?></td>
                         <td><?php
//photos
                         if(unserialize($v['photos'])) {
                  foreach (unserialize($v['photos']) as $key => $a) 
                  {
                              foreach ($a as $ka => $value) 
                                {
                                  if($key == 'coverphoto'){
                              
                            ?>
                           
         <img style="height:auto;" width="150" class="photo " src="<?php echo base_url().'/'.$value ?>">
                      <?php
                           
                                }
                              }

                  }
}
                         ?>


                         </td>


                         <td> <div class="clearfix"></div>
                          
<?php
//videos
$has_vid = unserialize($v['videos']);
if($has_vid) {


 foreach (unserialize($v['videos']) as $kv => $vid) 
                  {
                  
                    foreach ($vid as $kab => $video) 
                                {
                                  if($kv =='url'){
 ?>

  <video width="320" height="240" controls>
  <source src="<?php echo base_url().'/'.$video; ?>" type="video/mp4">
Your browser does not support the video tag.
</video>

                      
                         <!--  <video width="200" style="height:auto;" controls>
                                        <source  src="<?php echo base_url().'/'.$video; ?>" type="video/mp4">
                          </video> -->
                        
                      <?php 
                                }
                              }
                    
                  }
}
?>                        
 
 </td>
                         <td><?php echo $v['write_ups'] ?></td>
                         <td>
                            <?php if($edit): ?>
                            <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/edit/<?php echo $v['finalist_id'] ?>" class="btn btn-primary">Edit</a>
                            <?php endif; ?>
                            <?php if($delete): ?>
                            <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/delete/<?php echo $v['finalist_id'] ?>/<?php echo md5($v['finalist_id'] . ' ' . $this->config->item('encryption_key')) ?>" onclick="return confirmDeletion(this, '<?php echo str_replace('_', ' ', $controller) ?>')" class="btn btn-danger">Delete</a>
                            <?php endif; ?>
                         </td>
                    </tr>
                    <?php 
					endforeach;
          else:
            echo '<tr><td colspan="7">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->