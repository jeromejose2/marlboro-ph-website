<?php 
  $controller = $this->uri->segment(1);
  $label = '';
  switch($controller) {
    case 'finalists':
      $label = 'Finalist';
      break;
    case 'birthday_offer':
      $label = 'Birthday Offer';
      break;
    case 'flash_offer':
      $label = 'Flash Offer';
      break;
    default:
      $label = 'Prize';
      break;

  }
?>

<?php $this->load->view('editor.php') ?>
<script src="<?php echo BASE_URL ?>admin_assets/js/jquery.Jcrop.js"></script>
<script type="text/javascript">
var jscrop_api;
function initJCrop(elem) {
  <?php if(isset($record['thumbnail_meta'])):  
    $points = isset($record['thumbnail_meta']) ? unserialize($record['thumbnail_meta']) : null; ?>
    x1 = <?php echo $points['x1'] != '' ? $points['x1'] : 0 ?>;
    x2 = <?php echo $points['x2'] != '' ? $points['x2'] : 0 ?>;
    y1 = <?php echo $points['y1'] != '' ? $points['y1'] : 0 ?>;
    y2 = <?php echo $points['y2'] != '' ? $points['y2'] : 0 ?>;
  <?php else: ?>  
    x1 = 0;
    y1 = 0;
    x2 = 200;
    y2 = 200;     
  <?php endif; ?>;
  
  setTimeout(function() {
    if(jscrop_api)
      jcrop_api.destroy();
    $('#preview').Jcrop({
      onChange:   showCoords,
      onSelect:   showCoords,
      setSelect: [x1, y1, x2, y2],
      maxSize: [ 300, 300 ]
    },function(){
      jcrop_api = this;
    }); 
    $('#width').val($('#preview').width());
    $('#height').val($('#preview').height());
  }, 500);
}

function showCoords(c) {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
};

$(function() {
  <?php if(isset($record['prize_image']) && $record['prize_image'] != ''): ?>
  initJCrop($('#photo'));
  <?php endif; ?>

  $('#type').change(function() {
    if($(this).val() == 'Redemption') {
      $('#r-address-container').show();
    } else {
      $('#r-address-container').hide();
    }
  });

});

</script>
<link rel="stylesheet" href="<?php echo BASE_URL ?>admin_assets/css/jquery.Jcrop.css" type="text/css" />

<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3><?php echo $label ?></h3>
      </div>
       <div class="alert alert-danger" <?php if(!$error) echo 'style="display:none;"' ?>><?php echo $error; ?></div>
       <form class="form-horizontal" role="form" method="post" onsubmit="return submitFrm()" enctype="multipart/form-data">
          <?php if(isset($record) && $record['finalist_id']): ?>
            <input type="hidden" name="id" value="<?php echo $record['finalist_id'] ?>">
          <?php endif; ?>
           <div class="form-group">
                <label class="col-sm-2 control-label">Finalist Name</label>
                <div class="col-sm-4">
                     <input type="text" name="name" value="<?php echo isset($record['name']) ? $record['name'] : '' ?>" data-name="finalist name" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Person ID</label>
                <div class="col-sm-4">
                     <input type="text" name="person_id" value="<?php echo isset($record['person_id']) ? $record['person_id'] : '' ?>" data-name="finalist name" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Location</label>
                <div class="col-sm-4">
                     <input type="text" name="location" value="<?php echo isset($record['location']) ? $record['location'] : '' ?>" data-name="finalist name" class="required form-control">
                </div>
           </div>

          <div class="form-group">
          <label class="col-sm-2 control-label">Interest</label>
              <div class="col-sm-4">
                    <input type="text" name="interest" value="<?php echo isset($record['interest']) ? $record['interest'] : '' ?>" data-name="finalist name" class="required form-control">
                
              </div>
          </div>

          <div class="form-group " id="primaryphoto">
                <label class="col-sm-2 control-label">Primary Photo [630x582]</label>
                <div class="col-sm-4">
                    <input type="file" id="primaryphoto" onchange="Image(this, 'primaryphoto-preview')" name="primaryphoto" value="" class="required">
                </div>

           </div>

           <div class="form-group">
            <label class="col-sm-2 control-label"></label>
              <div class="col-sm-4">
                    <img id="primaryphoto-preview" src="">
              </div>
           </div>

           <div class="form-group " id="coverphotoreal">
                <label class="col-sm-2 control-label">Cover Photo [976x267]</label>
                <div class="col-sm-4">
                    <input type="file" id="coverphotoreal" onchange="Image(this, 'coverphotoreal-preview')" name="coverphotoreal" value="" class="required">
                </div>

           </div>

           <div class="form-group">
            <label class="col-sm-2 control-label"></label>
              <div class="col-sm-4">
                    <img id="coverphotoreal-preview" src="">
              </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Gallery Photo(s) [562x392]</label>
                <div class="col-sm-4">
                     <input type="file" id="photo" name="photo[]" value="" data-name="finalist photos" class="required" multiple="multiple">
                </div>
           </div>
          <?php if(isset($record) && $record['photos']): ?>
            <div class="form-group">
                <!-- <label class="col-sm-2 control-label">Image Preview</label> -->
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                  <div id="dvPreview">
                    <?php
                      if(isset($record) && $record['photos']) {
                        $photos = unserialize($record['photos']);
                      }
                    ?>
                    <?php if($photos): ?>
                      <?php foreach($photos as $k => $v): ?>
                        <img height="150" width="150" src="<?php echo str_replace('~path~', BASE_URL, $v) ?>">
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </div>
                   <!-- <img id="dvPreview" class="preview" <?php echo isset($record['prize_image']) ? "style='max-width:500px;' src='" . BASE_URL . 'uploads/prize/' . $record['prize_image'] . "'" : '' ?>  /> -->
                </div>
           </div>
          <?php else: ?>
            <div class="form-group">
                <!-- <label class="col-sm-2 control-label">Image Preview</label> -->
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-9">
                  <div id="dvPreview"></div>
                  <div id="checkbox"></div>
                   <!-- <img id="dvPreview" class="preview" <?php echo isset($record['prize_image']) ? "style='max-width:500px;' src='" . BASE_URL . 'uploads/prize/' . $record['prize_image'] . "'" : '' ?>  /> -->
                </div>
           </div>
          <?php endif; ?>
          <div class="form-group " id="coverphoto">
                <label class="col-sm-2 control-label">Gallery Primary Photo</label>
                <div class="col-sm-4" id="cover_photo">
                </div>
           </div>

           
           
           <div class="form-group">
                <label class="col-sm-2 control-label">Videos</label>
                <div class="col-sm-4">
                     <input type="file" id="video" name="video[]" value="" data-name="finalist videos" multiple="multiple">
                </div>
           </div>

               <div class="form-group videoclass " id="vidprev">

                    <label class="col-sm-2 control-label"></label> 

                 <!--  <div class="col-sm-4"> -->


                  <div id="left-image">

              </div>
                  <!-- </div> -->

               </div>

          <div class="form-group">
                <label class="col-sm-2 control-label">Short Description [70 characters]</label>
                <div class="col-sm-8">
                     <textarea name="short_description" data-name="Short Description" class=""><?php echo isset($record['short_description']) ? $record['short_description'] : '' ?></textarea>
                </div>
           </div>

          <div class="form-group">
                <label class="col-sm-2 control-label">Write-ups</label>
                <div class="col-sm-8">
                     <textarea name="write_ups" data-name="Write Ups" class=""><?php echo isset($record['write_ups']) ? $record['write_ups'] : '' ?></textarea>
                </div>
           </div>
           <!-- <div class="form-group">
                <label class="col-sm-2 control-label">Prize Stock</label>
                <div class="col-sm-4">
                     <input type="text" name="stock" value="<?php echo isset($record['stock']) ? $record['stock'] : '' ?>" onkeydown="return checkDigit(event)" data-name="prize stock" class="form-control">
                </div>
           </div> -->
          
           <div class="form-group">
              <div class="col-sm-offset-2 col-sm-4">
                <input type="hidden" name="submit" value="1">
                <button type="submit" id="submit" class="btn btn-primary">Submit</button>
              </div>
           </div>

           <!-- <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <input type="hidden" name="submit" value="1" />
                     <input type="hidden" id="x" name="x" />
                     <input type="hidden" id="y" name="y" />
                     <input type="hidden" id="x2" name="x2" />
                     <input type="hidden" id="y2" name="y2" />
                     <input type="hidden" id="width" name="width" />
                     <input type="hidden" id="height" name="height"/>
                     <input type="hidden" name="id" value="<?php echo isset($record['prize_id']) ? $record['prize_id'] : '' ?>" />
                     <input type="hidden" name="filename" value="<?php echo isset($record['prize_image']) ? $record['prize_image'] : '' ?>" />
                     <?php if($controller == 'birthday_offer') :?>
                     <input type="hidden" name="origin_id" value="<?php echo isset($record['origin_id']) ? $record['origin_id'] : BIRTHDAY_OFFERS ?>" />
                     <?php endif; ?>
                     <?php if($controller == 'flash_offer') :?>
                     <input type="hidden" name="origin_id" value="<?php echo isset($record['origin_id']) ? $record['origin_id'] : FLASH_OFFERS ?>" />
                     <?php endif; ?>
                     <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                </div>
           </div> -->
      </form>
      
 </div>
 <!--end main content -->
 
  <style>
    #dvPreview img {
      margin: 0 5px 5px 0;
    }
  </style>

 <script language="javascript" type="text/javascript">
  window.onload = function () 
    {
        //$('.videoclass').hide();

       document.getElementById("coverphoto").style.display = "none";
      var fileUpload = document.getElementById("photo");
      fileUpload.onchange = function () 
      {
              if (typeof (FileReader) != "undefined")
              {
                  var dvPreview = document.getElementById("dvPreview");
                  dvPreview.innerHTML = "";
                  var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                            for (var i = 0; i < fileUpload.files.length; i++) 
                            {
                              var coverphoto_no = i;
                                var file = fileUpload.files[i];
                                  var hid = document.createElement("input");
                                                        hid.type = "hidden";
                                                        hid.name = "imagename[]";
                                                        hid.value = file.name;
                                                       // dvPreview.appendChild(img);
                              
                                        if (regex.test(file.name.toLowerCase())) 
                                        {
                                            var reader = new FileReader();
                                            reader.onload = function (e) 
                                                    {
                                                        var img = document.createElement("IMG");
                                                        img.height = "150";
                                                        img.width = "150";
                                                        img.src = e.target.result;

                                                     
                                                        dvPreview.appendChild(img);
                                                    }
                                            reader.readAsDataURL(file);
                                            if(i != 0){

                                             document.getElementById("coverphoto").style.display = "block";
                                           }

                                        } else
                                        {
                                            alert(file.name + " is not a valid image file. Please select valid image type only.");
                                            dvPreview.innerHTML = "";
                                            fileUpload.value = "";
                                              $( "#cover_photo" ).empty();
                                            return false;
                                        }
                            }
              } 
              else {
                  alert("This browser does not support HTML5 FileReader.");
              }

              setTimeout(countimage, 700);
              
      }



      // for video

       var fileUpload_video = document.getElementById("video");
       var left_image = document.getElementById("vidprev");
      fileUpload_video.onchange = function () 
      {
         $( "#vidprev" ).empty();
              if (typeof (FileReader) != "undefined")
              {
                 // var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.mp4|.jpeg|.gif|.png|.bmp)$/;
                 
                  console.log('count'+fileUpload_video.files.length);

                  if(fileUpload_video.files.length != 0)
                  {



                       for (var i = 0; i < fileUpload_video.files.length; i++) 
                            {
                              var coverphoto_no = i;
                                var file = fileUpload_video.files[i];
                                 
                               var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.mp4)$/;
                                        if (regex.test(file.name.toLowerCase())) 
                                        {

                                          //video

                                             
                                              //video preview start
                                                  var reader = new window.FileReader(),i =i,
                                                          file = file,
                                                          url;
 console.log('name i'+i);

                                                          reader = window.URL || window.webKitURL;
                                                            if (reader && reader.createObjectURL) {
                                                                url = reader.createObjectURL(file);
                                                                reader.revokeObjectURL(url);  //free up memory
                                                            }
                                                              if (!window.FileReader) {
                                                                  console.log('Sorry, not so much');
                                                                  return;
                                                              }

                                                            reader = new window.FileReader();
                                                            reader.onload = function(evt) 
                                                          {
                                                              var divid = document.createElement("div");
                                                              divid.className = "col-sm-4";

                                                              var videlem = document.createElement("video");
                                                              videlem.name = "video_name[]";
                                                              videlem.className = "video_class";
                                                              
                                                              videlem.width = "400";
                                                              videlem.controls = true;
                                                              videlem.id = "video_id"+i;
                                                            /// ... some setup like poster image, size, position etc. goes here...
                                                            /// now, add sources:
                                                            var sourceMP4 = document.createElement("source"); 
                                                            sourceMP4.type = "video/mp4";
                                                            sourceMP4.src = evt.target.result;
                                                            left_image.appendChild(divid);
                                                            divid.appendChild(videlem);
                                                            videlem.appendChild(sourceMP4);

                                                          //$('#video_id'+i).attr('src', evt.target.result);
                                                          };
                                                      reader.readAsDataURL(file);
                                                      $('.videoclass').show();
                                                 //video preview end
                                              

                                          //video
                                          
                                            

                                        } else
                                        {
                                            alert(file.name + " is not a valid video file. Please select valid video type only.");
                                            
                                            fileUpload_video.value = "";
                                           $( "#vidprev" ).empty();
                                            return false;
                                        }
                            }


                  

                  }
                           
              } 
              else {
                  alert("This browser does not support HTML5 FileReader.");
              }

            
              
      }

      // for video
  };

  function nextproccess(fileUpload_video)
  {
    console.log('nextproccess');
  
  }
  function countimage(){

             var counter = $("#dvPreview > img").length;
              var dvPreview = document.getElementById("cover_photo");
             
 console.log('length '+$('#somediv1').length);
          if($('#somediv1').length != 0) 
                      {
                          $( "#cover_photo" ).empty();
                           $('.somediv').innerHTML = "";
                           $('.radio_class').remove();
                      }

             for (var i = 0; i < counter; i++) 
                            {
                                                    var bb = i +1;
                                                    var radio = document.createElement("input");
                                                        radio.id = "radio_photo";
                                                        radio.className  = "radio_class";
                                                        radio.type = "radio";
                                                        radio.value = i;
                                                        radio.name = "coverphoto[]";

                                                    var html=document.createElement('div');
                                                        html.id = 'somediv'+bb;
                                                        html.className  = 'somediv';
                                                        dvPreview.appendChild(html);
                                                        dvPreview.appendChild(radio);
                            }

                              for (var i = 0; i < counter ; i++) 
                            {
                              var bb = i +1;
                          document.getElementById('somediv'+bb).innerHTML='<span>Image '+bb+'</span><br>'; 
                            }

              
  }

function Image(input, target) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+target).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
</script>