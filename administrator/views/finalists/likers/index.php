<!-- start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Finalist Likers <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                  <a href="<?php echo SITE_URL ?>/finalist_likers/export?<?php echo @http_build_query($this->input->get()) ?>" class="btn btn-primary">Export</a>
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Person ID</th>
                         <th>Name</th>
                         <th>Like Date</th>
                         <th>Finalist Like</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/finalist_likers">
                     <tr>
                         <td></td>
                         <td></td>
                         <td><input name="name" class="form-control" value="<?php echo isset($_GET['name']) ? $_GET['name'] : '' ?>" /></td>
                         
                         <td>
                           <div class="row">
                                   <div class="col-xs-1 col-lg-2">
                                        <input name="from_date" placeholder="From" class="form-control from" value="<?= $this->input->get('from_date') ? $this->input->get('from_date') : '' ?>" />
                                   </div>
                                   <div class="col-xs-1 col-lg-2">
                                        <input name="to_date" placeholder="To" class="form-control to" value="<?= $this->input->get('to_date') ? $this->input->get('to_date') : '' ?>" />
                                   </div>
                              </div>
                            </td>
                         <td>
                                          <?php  $CI =& get_instance();
                                          $CI->load->model('global_model');
                                          $param['table'] = 'tbl_finalists';
                                          $result = $CI->global_model->get_rows($param)->result_array();
                                            ?>

                        <select class="form-control" name="finalist_id">
                           <option value=""></option>
                            <?php 
                           for ($i=0; $i < count($result); $i++) 
                           { 
                              $selected =  isset($_GET['finalist_id']) && $_GET['finalist_id'] != '' && $_GET['finalist_id'] == $result[$i]['finalist_id'] ? 'selected' : '';
                              echo '<option value="'.$result[$i]['finalist_id'].'" '.$selected.'> '.$result[$i]['name'].'</option>';
                            }
                          ?>
                           </select>


                          <!--   <select class="form-control" name="status">
                              <option value=""></option>
                              <option value="1" <?php echo isset($_GET['status']) && $_GET['status'] == 1 ? 'selected' : '' ?>>Published</option>
                              <option value="0" <?php echo isset($_GET['status']) && $_GET['status'] == 0 ? 'selected' : '' ?>>Unpublished</option>
                           
                            </select> -->
                         </td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
                    <?php if($likers): 
					foreach($likers as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['person_id'] ?></td>
                         <td><a href="javascript:void(0)" onclick="showRegistrantDetails(<?php echo $v['registrant_id'] ?>)"><?php echo $v['first_name'].' '.$v['third_name'] ?></a></td>
                         <td><?php echo date('Y-m-d H:i:s', strtotime($v['like_date_created'])) ?></td>
                         <!-- get finalist details -->
                        <?php  //$CI =& get_instance();
                         // $CI->load->model('global_model');
                         // $result = $CI->module_model->functionname();
                          $param['where'] = array('finalist_id'=>$v['finalist_id']);
                          $param['table'] = 'tbl_finalists';
                            $result = $CI->global_model->get_rows($param)->result_array()[0];
                          ?>
                         <!-- get finalist details -->
                         <td><?php echo $result['name'] ?></td>
                         <!-- <td>
                            <?php if($edit): ?>
                            <a href="<?php echo SITE_URL ?>/game/edit/<?php echo $v['game_id'] ?>" class="btn btn-primary">Edit</a>
                            <?php endif; ?>
                         </td> -->
                         <td></td>
                    </tr>
                    <?php 
					endforeach;
          else:
            echo '<tr><td colspan="7">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content 