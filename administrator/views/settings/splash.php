<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3><?php echo $title ?></h3>
      </div>

       <?php if($this->input->post()) { ?>
       <div class="alert alert-success">Settings has been successfully updated.</div>
       <?php } ?>
       <form class="form-horizontal" roe="form" method="post" onsubmit="return submitFrm()">
           <div class="form-group">
                <label class="col-sm-2 control-label">MoveFWD Splash Count</label>
                <div class="col-sm-2">
                     <input class="form-control" name="movefwd" onkeydown="return checkDigit(event)" value="<?php echo $movefwd['description'] ?>" />
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Perks Splash Count</label>
                <div class="col-sm-2">
                     <input class="form-control" name="perks" onkeydown="return checkDigit(event)" value="<?php echo $perks['description'] ?>" />
                </div>
           </div>
          <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <input type="hidden" name="submit" value="1" />
                     <input type="hidden" name="id" value="<?php echo isset($module['module_id']) ? $module['module_id'] : '' ?>" />
                     <button type="submit" id="submit" class="btn btn-primary">Update</button>
                     <button type="button" class="btn btn-warning" onclick="window.location='<?php echo BASE_URL ?>admin'">Cancel</button>
                </div>
           </div>
      </form>
      
 </div>
 <!--end main content -->
 