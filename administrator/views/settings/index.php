<?php $this->load->view('editor.php') ?>
</script>

<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3><?php echo $title ?></h3>
      </div>

       <?php if($this->input->post()) { ?>
       <div class="alert alert-success"><?php echo $title ?> content has been successfully updated.</div>
       <?php } ?>
       <form class="form-horizontal" roe="form" method="post" onsubmit="return submitFrm()">
           <div class="form-group">
                <div class="col-sm-8">
                     <textarea name="description"><?php echo $settings['description'] ?></textarea>
                </div>
           </div>
          <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <input type="hidden" name="submit" value="1" />
                     <input type="hidden" name="id" value="<?php echo isset($module['module_id']) ? $module['module_id'] : '' ?>" />
                     <button type="submit" id="submit" class="btn btn-primary">Update</button>
                     <button type="button" class="btn btn-warning" onclick="window.location='<?php echo BASE_URL ?>admin'">Cancel</button>
                </div>
           </div>
      </form>
      
 </div>
 <!--end main content -->
 