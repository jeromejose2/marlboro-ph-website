<link rel="stylesheet" href="<?=base_url()?>admin_assets/css/timepicker.css">
<script src="<?=base_url()?>admin_assets/js/timepicker.js" type="text/javascript"></script>

<script src="https://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<style type="text/css">

.clinic-containter-form{
  width:175px;
  height:auto;
  float:left;
}

</style>
<?php /*
<script type="text/javascript">
<?php 
$lat = 0;
$lng = 0;
if($row && $row->google_lat_long){
  $lat_lng = explode(',',str_replace(array('(',')'), '', $row->google_lat_long));
  $lat = $lat_lng[0];
  $lng = $lat_lng[1];
}

?> 

var map;
var markers = [];
var infoWindow;
<?php // echo "var form_data = ".json_encode(array('name'=>strip_tags($name),'address'=>strip_tags($address),'branch_area'=>strip_tags($branch_area),'contact'=>strip_tags($contact))).";"; ?>

function initialize() {
 
   var mapOptions = {
    zoom: 6,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
  center: new google.maps.LatLng(12, 121)
  };
   
  
  map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
  //infoWindow = new google.maps.InfoWindow();
  
  <?php if((float)$lat > 0 && (float)$lng > 0){ ?>
   var latlng = new google.maps.LatLng(parseFloat(<?=$lat?>),parseFloat(<?=$lng?>));
     createMarker(latlng);
  <?php } ?>
  
}
 
function showAddress(address) {
     var geocoder = new google.maps.Geocoder();
   
   geocoder.geocode({address: address}, function(results, status) {
       if (status == google.maps.GeocoderStatus.OK) {
        // searchLocationsNear(results[0].geometry.location);
    createMarker(results[0].geometry.location);
    //console.log(results[0].geometry.location);
       } else {
         document.forms['form']['google_lat_long'].value='';
         alert(address + ' Not found');
       }
     });
  
} 

function createMarker(latlng) {
    var bounds = new google.maps.LatLngBounds();
    var marker = new google.maps.Marker({
    map: map,
    position: latlng,
    draggable: true
    });
 
    document.forms['form']['google_lat_long'].value=marker.position.toString(0);
        
    google.maps.event.addListener(marker, 'click', function() {
      //infoWindow.setContent(getClinicForm(latlng));
      document.forms['form']['google_lat_long'].value=latlng;
      //infoWindow.open(map, marker);
     });
      
     
     google.maps.event.addListener(marker, 'dragend', function() {
      //infoWindow.setContent(getClinicForm(marker.position.toString(0)));
      document.forms['form']['google_lat_long'].value=marker.position.toString(0);
      //infoWindow.open(map,marker);
     });
     
      bounds.extend(latlng);
    map.fitBounds(bounds);
    markers.push(marker);
}
  
 
google.maps.event.addDomListener(window, 'load', initialize);
 
</script>
*/ ?>

<script type="text/javascript">
  function GetLocation() {
      <?php foreach($venues->result() as $r){ ?>
          var geocoder = new google.maps.Geocoder();
          var address = '<?php echo $r->name; ?>';
          geocoder.geocode({ 'address': address }, function (results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                  var latitude = results[0].geometry.location.lat();
                  var longitude = results[0].geometry.location.lng();
                  $(".<?php echo $r->venue_id ?>").attr('data-latitude', latitude);
                  $(".<?php echo $r->venue_id ?>").attr('data-longitude', longitude);
                  console.log("Latitude: " + latitude + "\nLongitude: " + longitude);
              } else {
                  $(".<?php echo $r->venue_id ?>").attr('data-latitude', '11.646338452790836');
                  $(".<?php echo $r->venue_id ?>").attr('data-longitude', '122.72092948635861');
                  console.log("cannot get <?php echo $r->name?> location.");
              }
          });
      <?php } ?>
    };

  $(function(){
        GetLocation();

        var LatitudeLongitudeCoordinates,
            DefaultLongitude = 122.72092948635861,
            DefaultLatitude = 11.646338452790836;
        
        <?php if( isset($row->lat) && $row->lng ): ?>
          LatitudeLongitudeCoordinates = new google.maps.LatLng(<?= $row->lat ?>, <?= $row->lng ?>);
        <?php else: ?>
          LatitudeLongitudeCoordinates = new google.maps.LatLng(DefaultLatitude, DefaultLongitude);
        <?php endif; ?>

        var mapOptions = {
            zoom: <?php echo isset($row->lat) && $row->lng ? 18 : 6?>,
            center: LatitudeLongitudeCoordinates,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        function panTo(lat, lng) {
            var panToLocation = new google.maps.LatLng(lat, lng);
            map.panTo(panToLocation);
            if(lat == '11.646338452790836' && lng == '122.72092948635861') {
                map.setZoom(6);
            } else {
                map.setZoom(18);
            }
        }

        var map = new google.maps.Map(document.getElementById('google_map_canvas'), mapOptions);
        var infowindow = new google.maps.InfoWindow();
        var bounds = new google.maps.LatLngBounds();

        var marker = new google.maps.Marker({
              draggable: true, position: LatitudeLongitudeCoordinates, map: map
          });

        google.maps.event.addListener(marker, 'drag', function(event){
           document.getElementById("latitude").value = event.latLng.lat();
           document.getElementById("longitude").value = event.latLng.lng();
           document.getElementById("google_lat_long").value = "(" + event.latLng.lat() + ", " + event.latLng.lng() + ")";
        });

        google.maps.event.addDomListener(select_venue, 'change', function() {
            var selected_venue_latitude = $(this).find(':selected').data('latitude');
            var selected_venue_longitude = $(this).find(':selected').data('longitude');
            document.getElementById("latitude").value = selected_venue_latitude;
            document.getElementById("longitude").value = selected_venue_longitude;
            panTo(selected_venue_latitude, selected_venue_longitude);
            // var LatLngPanTo = new google.maps.LatLng(selected_venue_latitude, selected_venue_longitude);
            marker.setPosition(new google.maps.LatLng(selected_venue_latitude, selected_venue_longitude));
            // var marker = new google.maps.Marker({
            //     draggable: true, position: LatLngPanTo, map: map
            // });
        });

        var listener = google.maps.event.addListener(map, "idle", function () {
            map.setZoom(<?php echo isset($row->lat) && $row->lng ? 18 : 6?>);
            google.maps.event.removeListener(listener);
        });
        
    });
</script>
 
<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3><?php echo $header_text; ?></h3>
      </div>

      <?php if($error){ ?>
       <div class="alert alert-danger"> <?php echo $error; ?> </div>
      <?php } ?>

       <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" id="form">

        <input type="hidden"  name="google_lat_long" id="google_lat_long" value="<?php echo ($row) ? $row->google_lat_long : '' ?>" /> 
  
           <div class="form-group">
                <label class="col-sm-2 control-label">Event :</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo ($row) ? $row->title : '';  ?>" name="title" class="form-control required" data-name="statement">
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Description :</label>
                <div class="col-sm-4">
                     <textarea name="description" class="form-control required"><?php echo ($row) ? $row->description : '';  ?></textarea>
                </div>
           </div> 
 
           <div class="form-group">
                <label class="col-sm-2 control-label">Region :</label>
                <div class="col-sm-4">
                       <select name="region_id" class="form-control">
                          <option value=""></option>
                          <?php if($regions->num_rows()){

                                  foreach($regions->result() as $r){ 
                                    if($row && $row->region_id==$r->region_id){?>
                                      <option selected="selected" value="<?=$r->region_id?>"><?=$r->name?></option>
                          <?php     }else{ ?>
                                      <option value="<?=$r->region_id?>"><?=$r->name?></option>
                          <?php     }
                                  }

                                } ?>
                        </select>
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Image :</label>
                <div class="col-sm-4">
                     <input type="file" name="image" class="form-control required"><br>
                     <strong>Width: 600px. Height: 300px.</strong>
                </div>
           </div>

           <?php if($row && file_exists('uploads/backstage/events/331_176_'.$row->image)){ ?>

                <div class="form-group">
                  <label class="col-sm-2 control-label"></label>
                  <div class="col-sm-4">
                       <img src="<?php echo BASE_URL.'/uploads/backstage/events/'.'/331_176_'.$row->image.'?id='.uniqid(); ?>?" />
                  </div>
                </div>

          <?php } ?>
           

           <div class="form-group">
                <label class="col-sm-2 control-label">Start Date :</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo $row ? $row->start_date : '';  ?>" name="start_date" class="form-control required from" id="start_date">
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">End Date :</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo $row ? $row->end_date : '';  ?>" name="end_date" class="form-control required" id="end_date">
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Venue: </label>
                <div class="col-sm-4">

                  <select name="venue" id="select_venue" class="form-control">
                          <option value="" data-latitude="11.646338452790836" data-longitude="122.72092948635861"></option>
                          <?php if($venues->num_rows()){

                                  foreach($venues->result() as $r){ 
                                    if($row && strtolower($row->venue)==strtolower($r->name)){?>
                                      <option selected="selected" value="<?=$r->name?>" class="<?php echo $r->venue_id ?> select_venue"><?=$r->name?></option>
                          <?php     }else{ ?>
                                      <option value="<?=$r->name?>" class="<?php echo $r->venue_id ?> select_venue"><?=$r->name?></option>
                          <?php     }
                                  }

                                } ?>
                        </select><br/>
                        <?php /* <div id="map-canvas" style="width: 600px; height: 400px"></div> */ ?>
                </div>
           </div>

           <div class="form-group">
              <label class="col-sm-2 control-label">Map Location</label>
              <div class="col-sm-4">
                  <div id="google_map_canvas" style="width: 480px; height: 480px"></div><br>
                  <input type="text" id="longitude" name="lng" value="<?php echo $row ? $row->lng : '';  ?>" data-name="longitude" class="form-control" placeholder="Longitude"><br>
                  <input type="text" id="latitude" name="lat" value="<?php echo $row ? $row->lat : '';  ?>" data-name="latitude" class="form-control" placeholder="Latitude">
              </div>
           </div>

           <div class="form-group">
              <label class="col-sm-2 control-label">Radius</label>
              <div class="col-sm-4">
                  <input type="text" name="radius" value="<?php echo $row ? $row->radius : '';  ?>" data-name="radius" class="required form-control" onkeydown="return checkDigit(event)"><br>
              </div>
           </div>

           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4"> 
                 </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Featured:</label>
                <div class="col-sm-4">
                  <?php if($row && $row->is_featured=='1'){ ?>
                          <input type="radio" name="is_featured" checked value="1"> Yes &nbsp; &nbsp;
                          <input type="radio" name="is_featured" value="0"> No
                  <?php }else{ ?>
                          <input type="radio" name="is_featured" value="1"> Yes
                          <input type="radio" name="is_featured" checked value="0"> No
                  <?php } ?>
                </div>
           </div>
 


           <div class="form-group">
                <label class="col-sm-2 control-label">Event Type :</label>
                <div class="col-sm-4">
                       <select name="event_type" class="form-control">
                          <option value=""></option>
                          <?php  if($event_types->num_rows()){

                                 foreach($event_types->result() as $r){ 
                                    if($row && strtolower($row->event_type)==strtolower($r->name)){?>
                                      <option selected="selected" value="<?=$r->name?>"><?=$r->name?></option>
                          <?php     }else{ ?>
                                      <option value="<?=$r->name?>"><?=$r->name?></option>
                          <?php     }
                                  } 

                                } ?>
                        </select>
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Status:</label>
                <div class="col-sm-4">
                  <select class="form-control" name="status">
                      <option value="1" <?php echo ($row && $row->status==APPROVED) ? 'selected' : '' ?>>Published</option>
                      <option value="2" <?php echo ($row && $row->status==DISAPPROVED) ? 'selected' : '' ?>>Unpublished</option>
                  </select>
                </div>
           </div>

           

           <div class="form-group">
              <label class="col-sm-2 control-label">Platform Restriction</label>
              <div class="col-sm-4">
                <select class="form-control" name="platform_restriction">
                      <option <?php echo isset($row->platform_restriction) && $row->platform_restriction == WEB_ONLY ? 'selected' : '' ?> value="<?= WEB_ONLY ?>">Website</option>
                      <option <?php echo isset($row->platform_restriction) && $row->platform_restriction == MOBILE_ONLY ? 'selected' : '' ?> value="<?= MOBILE_ONLY ?>">Mobile App</option>   
                      <option <?php echo isset($row->platform_restriction) && $row->platform_restriction == BOTH_PLATFORM ? 'selected' : '' ?> value="<?= BOTH_PLATFORM ?>">Mobile App & Website</option>   
                    </select>
              </div>
           </div>

           <div class="form-group">
              <label class="col-sm-2 control-label">Button Type</label>
              <div class="col-sm-4">
                <select class="form-control" name="is_gallery">
                      <option <?php echo isset($row->is_gallery) && $row->is_gallery == 1 ? 'selected' : '' ?> value="1">Gallery</option>
                      <option <?php echo isset($row->is_gallery) && $row->is_gallery == 0 ? 'selected' : '' ?> value="0">Who's There</option>   
                    </select>
              </div>
           </div>

           

           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4"> 
                     <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>" class="pull-left"><< Back</a>
                      <button type="submit" class="btn btn-primary pull-right">Submit</button>
                </div>
           </div>
           
           
      </form>
      
 </div>
 <!--end main content -->

 <script>
    $(function() {
        $("#start_date").datetimepicker({dateFormat:'yy-mm-dd'});
        $("#end_date").datetimepicker({dateFormat:'yy-mm-dd'});
    });
 </script>