<!--start main content -->
<script type="text/javascript" src="<?= BASE_URL ?>admin_assets/rotator/js/easeljs-0.7.1.min.js"></script>
<script type="text/javascript" src="<?= BASE_URL ?>admin_assets/rotator/js/domfile.js"></script>


     <div class="container main-content"> 

       <form id="form-list" action="<?php echo SITE_URL.'/backstage_event_photos'; ?>" method="GET">

          <div class="page-header">
               <h3>Backstage Pass User Photos<span class="badge badge-important"><?php echo (int)$total_rows; ?></span></h3>

               <div class="actions">
                     <button type="submit" onclick="return confirm_multi_approval()" class="btn btn-success" target="_blank">Approve</button>
                     <button type="button" onclick="return multiple_disapproval()" class="btn btn-danger" target="_blank">Disapprove</button>
                      <a href="<?php echo SITE_URL.'/backstage_events/export_photos'.$query_strings; ?>" class="btn btn-primary" target="_blank">Export</a>
                </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                       <th></th>
                       <th><center>Photo</center></th>
                          <th>Event</th>                         
                          <th>Submitted By</th>
                          <th>Status</th>
                          <th>Source</th>
                          <th>Date Added</th>                       
                          <th>Operation</th> 
                         
                    </tr>
               </thead>
               <tbody>
               		 
                     <tr>
                        <th><input type="checkbox"  onclick=" $(this).is(':checked') ? $('input[name=\'checkbox[]\']').prop('checked',true) : $('input[name=\'checkbox[]\']').prop('checked',false);"></th>
                        <th></th>
                        <th><select name="backstage_event_id" class="form-control" >
                            <option value=""></option>
                            <?php if($events->num_rows()){
                              foreach($events->result() as $e){
                                      if($this->input->get('backstage_event_id')==$e->backstage_event_id){ ?>
                                        <option value="<?=$e->backstage_event_id?>" selected><?=$e->title?></option>
                                <?php }else{ ?>
                                        <option value="<?=$e->backstage_event_id?>"><?=$e->title?></option>
                                <?php } 
                                 }
                            } ?>
                          </select></th>
                        <th><input type="text" class="form-control" name="submitted_by" value="<?=$this->input->get('submitted_by')?>"></th>
                        <th><select name="status" class="form-control">
                              <option value=""></option>
                              <option value="0" <?php echo ($this->input->get('status')==='0') ? 'selected' : ''; ?> >Pending</option>
                              <option value="1" <?php echo ($this->input->get('status')==1) ? 'selected' : ''; ?> >Approved</option>
                              <option value="2" <?php echo ($this->input->get('status')==2) ? 'selected' : ''; ?> >Disapproved</option>
                              
                            </select></th>
                            <th>
                                <select name="source" class="form-control">
                                    <option value=""></option>
                                    <option value="1" <?php echo isset($_GET['source']) && $_GET['source'] == '1' ? 'selected' : '' ?>>Web</option>
                                    <option value="2" <?php echo isset($_GET['source']) && $_GET['source'] == '2' ? 'selected' : '' ?>>Mobile</option>
                                </select>
                            </th>
                       <th>
                          <div class="row">
                            <div class="col-md-6 col-lg-4">
                                 <input name="from_date_added" placeholder="From" class="form-control from" value="<?php echo isset($_GET['from_date_added']) ? $_GET['from_date_added'] : '' ?>" />
                            </div>
                            <div class="col-md-6 col-lg-4">
                                 <input name="to_date_added" placeholder="To" class="form-control to" value="<?php echo isset($_GET['to_date_added']) ? $_GET['to_date_added'] : '' ?>" />
                            </div>
                          </div>
                      </th>         
                      <th><button type="submit" class="btn btn-primary">Go</button></th>                     
                     </tr>
                 

                  <?php



                   if($rows->num_rows()){

                            $edit_url = SITE_URL.'/'.$this->uri->segment(1).'/edit/';
                            $status = array(0=>'Pending',1=>'Approved',2=>'Disapproved');
                          
 
                            foreach($rows->result() as $v){  ?>
                              <tr>
                                  <td>
                                    <input type="checkbox" name="checkbox[]" value="<?=$v->photo_id?>-<?=$v->backstage_event_id?>">
                                  </td>
                                  <td><center><a href="<?=SITE_URL.'/backstage_event_photos/view_photo/'.$v->photo_id?>" title="Click to Full view" class="view-video"><img src="<?=BASE_URL.'uploads/backstage/photos/thumbnails/100_100_'.$v->media_image_filename?>" /></a></center></td>
                                  <td><?=$v->event_title?></td>
                                  <td><?=$v->uploader_name?></td>
                                  <td><?=$status[$v->status]?></td>
                                  <td><?php echo $v->source == '1' ? 'Web' : 'Mobile' ?></td>
                                  <td><?=$v->date_added?></td> 
                                                                
                                  <td>
                                    <?php if($edit){ ?>
                                      <a data-href="<?php echo SITE_URL.'/backstage_event_photos/edit_photo/'.$v->photo_id ?>?rand=<?= rand() ?>" class="edit-photo-btn btn btn-default">Edit Photo</a>
                                      <?php if($v->status==1){?>
                                           <!--  <a href="<?php echo SITE_URL.'/backstage_event_photos/disapprove?photo_id='.$v->photo_id.'&backstage_event_id='.$v->backstage_event_id.'&token='.md5($v->photo_id. ' ' . $this->config->item('encryption_key')); ?>" class="btn btn-danger" onclick="return confirmAction(this,'disapprove');">Disapprove</a> -->
                                           <a href="javascript:void" class="btn btn-danger" onclick="return askReason('<?php echo SITE_URL.'/backstage_event_photos/disapprove?per_page='.$this->input->get('per_page').'&photo_id='.$v->photo_id.'&backstage_event_id='.$v->backstage_event_id.'&token='.md5($v->photo_id. ' ' . $this->config->item('encryption_key')); ?>')">Disapprove</a>
                                      <?php }else if($v->status==2){ ?>
                                              <a href="<?php echo SITE_URL.'/backstage_event_photos/approve?per_page='.$this->input->get('per_page').'&photo_id='.$v->photo_id.'&backstage_event_id='.$v->backstage_event_id.'&token='.md5($v->photo_id. ' ' . $this->config->item('encryption_key')); ?>" class="btn btn-success" onclick="return confirmAction(this,'approve');">Approve</a>
                                     <?php }else{ ?>
                                            <a href="<?php echo SITE_URL.'/backstage_event_photos/approve?per_page='.$this->input->get('per_page').'&photo_id='.$v->photo_id.'&backstage_event_id='.$v->backstage_event_id.'&token='.md5($v->photo_id. ' ' . $this->config->item('encryption_key')); ?>" class="btn btn-success" onclick="return confirmAction(this,'approve');">Approve</a>
                                            <a href="javascript:void" class="btn btn-danger" onclick="return askReason('<?php echo SITE_URL.'/backstage_event_photos/disapprove?per_page='.$this->input->get('per_page').'&photo_id='.$v->photo_id.'&backstage_event_id='.$v->backstage_event_id.'&token='.md5($v->photo_id. ' ' . $this->config->item('encryption_key')); ?>')">Disapprove</a>
                                    <?php } 
                                    } ?>
                                   </td>
                                   
                                
                              </tr>
                  <?php     } 
                        }else{ ?>
                          <tr><td colspan="6">No records found.</td></tr>
                  <?php  } ?>
                  
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>


        </form>



          

     </div>
     <!--end main content -->
 

       <div id="play-video" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>
            </div>
          </div> 
      </div>


      <div class="modal" id="reason">
        <div style="text-align:center">
          <form id="reason-form" method="post">
                 <textarea name="message" class="form-control" rows="5"></textarea>
                <br>
                <div style="float:right">
                    <button type="button" class="btn close-bootbox" name="search">Cancel</button>
                    <button class="btn btn-primary" name="search" value="1">Submit</button>
                </div>
                <br style="clear:both;">
          </form>
        </div>
      </div>

     <script>
      

     function confirm_multi_approval(){

       
        if($('input[name="checkbox[]"]').is(':checked')){
          var is_valid = false;

          bootbox.confirm('Are you sure you want to approve?',function(result){ 

            if(result== 1){
              $('#form-list')
                .attr('method', 'POST')
                .attr('action','<?=SITE_URL?>/backstage_event_photos/multiple_approve?per_page=<?=$this->input->get('per_page')?>')
                .submit();
            }      

          });
          return false;
 
        }else{

          bootbox.alert('Please select entries to approve.');
          return false;

        }       

     }


     function multiple_disapproval(){

 
        if($('input[name="checkbox[]"]').is(':checked')){

           var form = document.getElementById('reason-form');
            
          $.each($('input[name="checkbox[]"]'),function(i,val){

            if($(this).is(':checked')){
             
              var input = document.createElement('input');
              input.type = 'hidden';
              input.name = 'disapproved-entries[]';
              input.value = val.value;
              form.appendChild(input);

            }

          });

          
          $("#reason form").prop("action", "<?=SITE_URL?>/backstage_event_photos/multiple_disapprove?per_page=<?=$this->input->get('per_page')?>");
          bootbox.dialog({'message': $('#reason').html(), 'title': 'Reason for disapproval'});
           
        }else{

          bootbox.alert('Please select entries to disapprove.');
          return false;

        }       

     }


      function askReason(href) {
        $("#reason form").prop("action", href);
        bootbox.dialog({'message': $('#reason').html(),
              'title': 'Reason for Rejection'});
      }

     $('.view-video').on('click',function(e){
          var href = $(this).attr('href');
          $('.modal-content').html('<div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>');
          $('#play-video').modal('show');
          $('.modal-content').load(href);
          e.preventDefault();
     }); 

     $(".edit-photo-btn").click( function (e) {
          var href = $(this).data('href');
          $('.modal-content').html('<div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>');
          $('#play-video').modal('show');
          $('.modal-content').load(href);
          e.preventDefault();
     });

     $('#play-video').on('hidden.bs.modal',function(){
        $('.modal-content').html('<img src="<?=BASE_URL?>images/spinner.gif"/></div>');
        $('#img-container-rotator').remove();
     });
     </script>