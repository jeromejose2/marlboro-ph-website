<div class="modal-header">
   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   <h4 class="modal-title"><?=($row) ? ucwords($row->title) : ''?></h4>
  </div>

<div class="modal-body text-center">
  <?php if($row){
  $file = file_exists('uploads/backstage/photos/'.$row->media_image_filename) ? BASE_URL.'uploads/backstage/photos/'.$row->media_image_filename : ''; ?>    
  <img style="max-width:100%;" src="<?=$file?>" />
  <?php } ?>
</div>