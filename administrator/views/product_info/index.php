<!--start main content -->
<div class="container main-content">
	<div class="page-header">
		<h3>Product Information<span class="badge badge-important"><?= $total ?></span></h3>

		<div class="actions">
			<?php if ($access['add']): ?>
			<a href="<?= SITE_URL ?>/product_info/add" class="btn btn-primary">Add</a>
			<?php endif ?>
			<a href="<?= SITE_URL ?>/product_info/export?<?= http_build_query($this->input->get() ? $this->input->get() : array()) ?>" class="btn btn-primary">Export</a>
		</div>
	</div>
	 
	<table class="table table-bordered">
		<thead>
				<tr>
					<th>#</th>
					<th>Image</th>
					<th>Content Image</th>
					<th>Title</th>
					<th>Description</th>
					<th>Status</th>
					<th>Date Created</th>
					<th></th>
				</tr>
		</thead>
		<tbody>
			<form action="<?= SITE_URL ?>/product_info">
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td><input name="title" placeholder="Title" class="form-control" value="<?= $this->input->get('title') ? $this->input->get('title') : '' ?>" /></td>
						<td></td>
						<td>
						<select class="form-control" name="status">
							<option value=""></option>
							<option <?= (string) $this->input->get('status') == '1' ? 'selected="selected"' : '' ?> value="1">Published</option>
							<option <?= (string) $this->input->get('status') == '0' ? 'selected="selected"' : '' ?> value="0">Unpublished</option>
						</select>
						</td>
						<td></td>
						<td><button class="btn btn-primary" name="search" type="submit">Go</button></td>
					</tr>
				</form>
				<?php foreach ($records as $k => $record): ?>
				<tr>
						<td><?= $offset + $k + 1 ?></td>
						<td><img class="img-thumbnail" src="<?= BASE_URL ?>uploads/product_info/150_150_<?= $record->product_info_image ?>"></td>
						<td>
							<?php if ($record->product_info_content_image): ?>
							<img class="img-thumbnail" src="<?= BASE_URL ?>uploads/product_info/150_150_<?= $record->product_info_content_image ?>">
							<?php endif ?>
						</td>
						<td><?= $record->product_info_title_type == 1 ? $record->product_info_title : '<img class="img-thumbnail" src="'.BASE_URL.'uploads/product_info/200_30_'.$record->product_info_title.'">' ?></td>
						<td><?= $record->product_info_description ?></td>
						<td><?= $record->product_info_status ? '<span class="label label-success">Published</span>' : '<span class="label label-warning">Unpublised</span>' ?></td>
                        <td><?= date('F d, Y l', strtotime($record->product_info_date_created)) ?></td>
						<td>
						<?php if ($access['edit']): ?>
						<a class="btn btn-primary" href="<?= SITE_URL ?>/product_info/edit?id=<?= $record->product_info_id ?>">Edit</a>&nbsp;&nbsp;
						<?php endif ?>
						<?php if ($access['delete']): ?>
						<a class="btn btn-danger product-info-delete" href="javascript:void(0)" data-id="<?= $record->product_info_id ?>">Delete</a>&nbsp;&nbsp;
						<?php endif ?>
						<?php if (!$record->product_info_status): ?>
						<a class="btn btn-success change-status-btn" href="<?= SITE_URL ?>/product_info/change_status?id=<?= $record->product_info_id ?>&status=1">Publish</a>
						<?php else: ?>
						<a class="btn btn-warning change-status-btn" href="<?= SITE_URL ?>/product_info/change_status?id=<?= $record->product_info_id ?>&status=0">Unpublish</a>
						<?php endif ?>
						</td>
					</tr>
				<?php endforeach ?>
		</tbody>
	</table>

	<ul class="pagination pagination-sm pull-right">
		<?= !empty($pagination) ? $pagination : '' ?>
	</ul>

</div>
<!--end main content -->
<script>
	$(".change-status-btn").click( function(e) {
		var href = $(this).prop("href");
		bootbox.confirm("Are you sure?", function(confirm) {
			if (confirm) {
				window.location = href;
			}
		});
		e.preventDefault();
	});
	$(".product-info-delete").click( function() {
		var id = $(this).data("id");
		bootbox.confirm("Are you sure?", function(confirm) {
			if (confirm) {
				window.location = "<?= SITE_URL ?>/product_info/delete?id=" + id;
			}
		});
	});
</script>