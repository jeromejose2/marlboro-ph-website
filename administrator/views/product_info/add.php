<!--start main content -->
<!-- <link type="text/css" rel="stylesheet" href="<?= BASE_URL ?>admin_assets/css/angular-js/common.css"> -->
<script src="<?= BASE_URL ?>admin_assets/js/angular-js/angular-file-upload-shim.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.js"></script>		 
<script src="<?= BASE_URL ?>admin_assets/js/angular-js/angular-file-upload.js"></script>
<script type="text/javascript">
var uploadURL = '<?= SITE_URL ?>/product_info/upload/';
</script>
<script src="<?= BASE_URL ?>admin_assets/js/angular-js/upload-product-info.js"></script> 

<?php $this->load->view('editor.php') ?>
 <div class="container main-content" ng-app="fileUpload" ng-controller="MyCtrl">
			<div class="page-header">
					 <h3>Add Product Info</h3>
		   <div class="actions">
			  <a href="<?= SITE_URL ?>/product_info" class="btn btn-primary">Back</a>
		   </div>
			</div>
			<?php if (!$error): ?>
			<div class="alert alert-danger" style="display:none;"></div>
			<?php else: ?>
			<div class="alert alert-danger"><?= $error ?></div>
			<?php endif ?>
			 <form class="form-horizontal" role="form" method="post" id="add-form-product-info" action="<?= SITE_URL ?>/product_info/add" onsubmit="return submitFrm()" enctype="multipart/form-data">
					 <div class="form-group">
								<label class="col-md-1 control-label">Title</label>
								<div class="col-md-4">
										 <input type="text" id="title" name="title" value="" data-name="title" class="required form-control">
								</div>
								<div class="col-md-4">
									<input type="radio" class="title-type" checked="checked" name="title_type" value="1">&nbsp;&nbsp;Text&nbsp;&nbsp;
									<input type="radio" class="title-type" name="title_type" value="2">&nbsp;&nbsp;Image
								</div>
					 </div>
					 <div class="form-group hide" id="product-info-title-preview">
								<label class="col-md-1 control-label">Title Preview</label>
								<div class="col-md-4" id="product-info-preview-img">
									<img src="<?= BASE_URL.DEFAULT_IMAGE ?>" class="img-thumbnail">
								</div>
							</div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Description</label>
								<div class="col-md-4">
										 <textarea name="description" data-name="description" class="form-control"></textarea>
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Image</label>
								<div class="col-md-4">
										 <input type="file" id="product-info-photo" data-name="image" name="product_info_upload" value="" class="required form-control" onchange=""><br>
										 <strong>Width: 193px. Height: 225px.</strong>
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Image Preview</label>
								<div class="col-md-4">
									 <img src="<?= BASE_URL.DEFAULT_IMAGE ?>" style="width: 193px; height: 225px;" id="product-info-preview" class="img-thumbnail preview"  />
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Content Image</label>
								<div class="col-md-4">
										 <input type="file" id="product-info-content-image" name="product_info_content_image_upload" value="" class="form-control" onchange=""><br>
										 <strong>Width: 570px. Height: 450px.</strong>
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Content Image Preview</label>
								<div class="col-md-4">
									 <img src="<?= BASE_URL.DEFAULT_IMAGE ?>" id="product-info-content-image-preview" class="img-thumbnail preview"  />
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Status</label>
								<div class="col-md-4">
										<select class="form-control" name="status">
											<option value="0">Unpublished</option>
											<option value="1">Published</option>   
										</select>
								</div>
					 </div>

		   <div class="form-group" id="with-btn-add-media">
			<label class="col-md-1 control-label">Media</label>
			<div class="col-md-4">
			  <!-- <button type="button" class="btn btn-warning" id="add-media-btn">Add Media</button>&nbsp;&nbsp; -->
			  <button type="button" class="btn btn-warning" id="product-info-browse-media">Browse</button><br>
			  <strong>Width: 1160px. Height: 600px.</strong>
			  <!-- <button type="button" class="btn btn-danger" id="clear-uploads">Clear</button> -->
			  
			</div>
		</div>
			<div class="form-group" ng-show="selectedFiles != null">
				<label class="col-md-1 control-label">Media Preview</label>
				<div class="col-md-8">
					<div class="row">
					<div class="col-xs-4 col-md-3" ng-repeat="f in selectedFiles">
				  	<div class="thumbnail">
				      <img ng-show="dataUrls[$index]" ng-src="{{dataUrls[$index]}}">
				      <div class="caption">
				        <h5>{{f.name}}</h5>
				        <input name="product_media_title[{{$index}}]" type="text" style="width: 100%;" placeholder="Enter title here">
				        <input name="product_media_upload[{{$index}}]" type="hidden" ng-show="uploadResult[$index]" value="{{uploadResult[$index]}}">
				        <br><br>
				        <p>size: {{f.size}}B - type: {{f.type}}</p>
				        <div class="progress progress-striped" ng-class="{ active : !uploadResult[$index]}" ng-show="progress[$index] >= 0">
							  <div class="progress-bar"  role="progressbar" aria-valuemin="0" aria-valuenow="{{progress[$index]}}" aria-valuemax="100" style="width: {{progress[$index]}}%">
							    <span class="sr-only">{{progress[$index]}}% Complete</span>
							  </div>
							</div>
				        <p><a href="javascript:void(0)" class="btn btn-primary" role="button" ng-click="start($index)" ng-show="progress[$index] < 0">Start</a> 
				        <a href="javascript:void(0)" class="btn btn-warning" ng-click="abort($index)" ng-show="hasUploader($index) && !uploadResult[$index]" role="button">Abort</a> 
				    	<a href="javascript:void(0)" class="btn btn-danger" ng-click="remove($index)" ng-show="uploadResult[$index]" role="button">&times;&nbsp;&nbsp;Remove</a></p>
				      </div>
				    </div>
				  </div>
				</div>
				</div>
			</div>
					 <div class="form-group">
						<div class="col-md-offset-1 col-md-4">
						 <button type="submit" class="btn btn-primary">Submit</button>
					 </div>
					</div>
					<input type="hidden" ng-model="httpMethod" ng-init="httpMethod = 'POST'" value="POST"/>
					<input type="hidden" name="howToSend" ng-model="howToSend" value="1" ng-init="howToSend = 1">
					<!-- <input type="hidden" id="remove-files-index" name="remove-file" value=""> -->
			</form>
			<input type="file" name="product_multiple_images[]" class="form-control hide" ng-file-select="onFileSelect($files)" id="tmp-file-upload" multiple="true">
				<!-- <div > -->
				<!-- <div class="sel-file img-thumbnail" >
					{{($index + 1) + '.'}}
					<img ng-show="dataUrls[$index]" ng-src="{{dataUrls[$index]}}">
					<button class="button" ng-click="start($index)" ng-show="progress[$index] < 0">Start</button>
					<span class="progress" ng-show="progress[$index] >= 0">						
						<div style="width:{{progress[$index]}}%">{{progress[$index]}}%</div>
					</span>				
					<button class="button" ng-click="abort($index)" ng-show="hasUploader($index) && progress[$index] < 100">Abort</button>
					{{f.name}} - size: {{f.size}}B - type: {{f.type}}
				</div> -->
			<!-- </div>
			</div> -->
 </div>

 <script>

// $("#add-form-product-info").submit( function() {
// 	bootbox.dialog({
// 	  message: "<center><strong>Loading</strong></center>",
// 	  show: true,
// 	  backdrop: true,
// 	  closeButton: false
// 	});
// });

$("#product-info-browse-media").click( function () {
	$("#tmp-file-upload").click();
});

$("body").on("change", "#title[type='file']", function() {
	var file = $(this).prop("files")[0];
	if (file.type.indexOf("image") == 0) {
		var oFReader = new FileReader();
	oFReader.readAsDataURL(file);
		oFReader.onload = function (oFREvent) {
			$("#product-info-preview-img").html("<img class='img-thumbnail' src='" + oFREvent.target.result + "'>");
		};
	}
});
	var titleParent = $("#title").parent();
	$(".title-type").change( function() {
		var value = $(this).val();
		if (value == 2) {
			titleParent.html("<input type='file' id='title' name='product_info_title' data-name='title photo' class='required form-control'>");
			$("#product-info-title-preview").removeClass("hide");
		} else if (value == 1) {
			titleParent.html("<input type='text' id='title' name='title' value='' data-name='title' class='required form-control'>");
			$("#product-info-title-preview").addClass("hide");
		}
	});

 $("#product-info-photo").change( function() {
  var oFReader = new FileReader();
  oFReader.readAsDataURL($(this).prop("files")[0]);
  oFReader.onload = function (oFREvent) {
	$("#product-info-preview").prop("src", oFREvent.target.result);
  };
 });

 $("#product-info-content-image").change( function() {
  var oFReader = new FileReader();
  oFReader.readAsDataURL($(this).prop("files")[0]);
  oFReader.onload = function (oFREvent) {
	$("#product-info-content-image-preview").prop("src", oFREvent.target.result);
  };
 });


 </script>
 <!--end main content 