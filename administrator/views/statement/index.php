<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Maybe Statements <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL ?>/statement/add" class="btn btn-primary">Add Statement</a>
                   	<?php endif; ?>
                    <a href="<?php echo SITE_URL ?>/export/statements?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>   
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Statement</th>
                         <th>Status</th>
                         <th>Publish Date</th>
                         <th>Unpublish Date</th>
                         <?php if($edit || $delete) : ?>
                         <th>Operation</th>
                         <?php endif; ?>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/statement">
                         <tr>
                             <td></td>
                             <td><input name="statement" class="form-control" value="<?php echo isset($_GET['statement']) ? $_GET['statement'] : '' ?>" /></td>
                             <td>
                             	<select class="form-control" name="status">
                                	<option value=""></option>
                                    <option value="1" <?php echo isset($_GET['status']) && $_GET['status'] == 1 ? 'selected' : '' ?>>Published</option>
                                    <option value="2" <?php echo isset($_GET['status']) && $_GET['status'] == 2 ? 'selected' : '' ?>>Unpublished</option>
                                </select>
                             </td>
                             <td>
                              <div class="row">
                                   <div class="col-xs-5 col-lg-4">
                                        <input name="from_publish" placeholder="From" class="form-control from" value="<?php echo isset($_GET['from_publish']) ? $_GET['from_publish'] : '' ?>" />
                                   </div>
                                   <div class="col-xs-5 col-lg-4">
                                        <input name="to_publish" placeholder="To" class="form-control to" value="<?php echo isset($_GET['to_publish']) ? $_GET['to_publish'] : '' ?>" />
                                   </div>
                              </div>
                              </td>
                             <td>
                              <div class="row">
                                   <div class="col-xs-5 col-lg-4">
                                        <input name="from_unpublish" placeholder="From" class="form-control from" value="<?php echo isset($_GET['from_unpublish']) ? $_GET['from_unpublish'] : '' ?>" />
                                   </div>
                                   <div class="col-xs-5 col-lg-4">
                                        <input name="to_unpublish" placeholder="To" class="form-control to" value="<?php echo isset($_GET['to_unpublish']) ? $_GET['to_unpublish'] : '' ?>" />
                                   </div>
                              </div>
                              </td>
                             <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                        </tr>
                        </form>
                        
                    <?php if($users): 
					foreach($users as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['statement'] ?></td>
                         <td><?php echo $v['status'] == 1 ? 'Published' : 'Unpublished' ?></td>
                         <td><?php echo $v['status'] == APPROVED ?  date('F d, Y H:i:s', strtotime($v['date_published'])) : 'N/A' ?></td>
                         <td><?php echo $v['status'] == DISAPPROVED ?  date('F d, Y H:i:s', strtotime($v['date_unpublished'])) : 'N/A' ?></td>
                         <?php if($edit || $delete) : ?>
                         <td>
                         	<?php if($edit): ?>
                            <a href="<?php echo SITE_URL ?>/statement/edit/<?php echo $v['statement_id'] ?>" class="btn btn-primary">Edit</a>
                          <?php endif; ?>
                            <?php if($delete) : ?>
                            <a href="<?php echo SITE_URL ?>/statement/delete/<?php echo $v['statement_id'] ?>/<?php echo md5($v['statement_id'] . ' ' . $this->config->item('encryption_key')) ?>" onclick="return confirmDeletion(this, 'statement')" class="btn btn-danger">Delete</a>
                            <?php endif; ?>
                         </td>
                         <?php endif; ?>
                    </tr>
                    <?php 
					endforeach;
					else:
						echo '<tr><td colspan="3">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->