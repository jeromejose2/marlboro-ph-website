<?php 
  $controller = $this->uri->segment(1);
  $label = '';
  switch($controller) {
    case 'birthday_offer':
      $label = 'Birthday Offer';
      break;
    case 'flash_offer':
      $label = 'Flash Offer';
      break;
    default:
      $label = 'Prize';
      break;

  }

?>
<?php $this->load->view('editor.php') ?>
<script src="<?php echo BASE_URL ?>admin_assets/js/jquery.Jcrop.js"></script>
<script type="text/javascript">
var jscrop_api;
function initJCrop(elem) {
  <?php if(isset($record['thumbnail_meta'])):  
    $points = isset($record['thumbnail_meta']) ? unserialize($record['thumbnail_meta']) : null; ?>
    x1 = <?php echo $points['x1'] != '' ? $points['x1'] : 0 ?>;
    x2 = <?php echo $points['x2'] != '' ? $points['x2'] : 0 ?>;
    y1 = <?php echo $points['y1'] != '' ? $points['y1'] : 0 ?>;
    y2 = <?php echo $points['y2'] != '' ? $points['y2'] : 0 ?>;
  <?php else: ?>  
    x1 = 0;
    y1 = 0;
    x2 = 200;
    y2 = 200;     
  <?php endif; ?>;
  
  setTimeout(function() {
    if(jscrop_api)
      jcrop_api.destroy();
    $('#preview').Jcrop({
      onChange:   showCoords,
      onSelect:   showCoords,
      setSelect: [x1, y1, x2, y2],
      maxSize: [ 300, 300 ]
    },function(){
      jcrop_api = this;
    }); 
    $('#width').val($('#preview').width());
    $('#height').val($('#preview').height());
  }, 500);
}

function showCoords(c) {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
};

$(function() {
  <?php if(isset($record['prize_image']) && $record['prize_image'] != ''): ?>
  initJCrop($('#photo'));
  <?php endif; ?>

  $('#type').change(function() {
    if($(this).val() == 'Redemption') {
      $('.r-address-container').show();
    } else {
      $('.r-address-container').hide();
    }
  });
  $('#media-type').change(function() {
    $('.photo, .video').hide();
    $('.photo, .video').find('input').val('');
    if($(this).val() == 'Video') {
      $('.video').show();
    } else if($(this).val() == 'Photo')  {
     $('.photo').show();
     $('.jcrop-holder').show();
    }
  });

  $('.photo, .video').hide();
  <?php if(isset($record['type']) && $record['type'] == 'Video'):  ?>
  $('.video').show()
  <?php endif; ?>
  <?php if(isset($record['type']) && $record['type'] == 'Photo'):  ?>
  $('.photo').show()
  <?php endif; ?>
});

</script>
<link rel="stylesheet" href="<?php echo BASE_URL ?>admin_assets/css/jquery.Jcrop.css" type="text/css" />

<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3><?php echo $label ?></h3>
      </div>
       <div class="alert alert-danger" <?php if(!$error) echo 'style="display:none;"' ?>><?php echo $error; ?></div>
       <form class="form-horizontal" role="form" method="post" onsubmit="return submitFrm()" enctype="multipart/form-data">
           <div class="form-group">
                <label class="col-sm-2 control-label">Name</label>
                <div class="col-sm-4">
                     <input type="text" name="prize_name" value="<?php echo isset($record['prize_name']) ? $record['prize_name'] : '' ?>" data-name="name" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Category</label>
                <div class="col-sm-4">
                    <select class="form-control" name="category">
                      <option <?php echo isset($record['category']) && $record['category'] == 1 ? 'selected' : '' ?> value="1">Flash Offer</option>
                      <option <?php echo isset($record['category']) && $record['category'] == 2 ? 'selected' : '' ?> value="2">Referral Offer</option>   
                    </select>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Media Type</label>
                <div class="col-sm-4">
                    <select class="form-control" name="type" id="media-type">
                      <option <?php echo isset($record['type']) && $record['type'] == 'Text' ? 'selected' : '' ?>>Text</option> 
                      <option <?php echo isset($record['type']) && $record['type'] == 'Photo' ? 'selected' : '' ?>>Photo</option> 
                      <option <?php echo isset($record['type']) && $record['type'] == 'Video' ? 'selected' : '' ?>>Video</option>  
                    </select>
                </div>
           </div>
           <div class="form-group photo">
                <label class="col-sm-2 control-label">Image</label>
                <div class="col-sm-4">
                     <input type="file" id="photo" name="photo" value="" data-name="module name" class="" onchange="previewImage('photo', 'preview', initJCrop, this);">
                     <br>
                     <strong>Width: 400px. Height: 300px.</strong>
                </div>
           </div>
           <div class="form-group photo" >
                <label class="col-sm-2 control-label">Image Preview</label>
                <div class="col-sm-4">
                   <img id="preview" class="preview" <?php echo isset($record['media']) && $record['type'] == 'Photo' ? "style='max-width:500px;' src='" . BASE_URL . 'uploads/prize/' . $record['media'] . "'" : '' ?>  />
                </div>
           </div>
           <div class="form-group video">
                <label class="col-sm-2 control-label">Video</label>
                <div class="col-sm-4">
                     <input type="file" id="video" name="video" value="" data-name="module name" class="">
                </div>
           </div>
           <?php if(isset($record['media'])): ?>
           <div class="form-group video" >
                <label class="col-sm-2 control-label">Video Preview</label>
                <div class="col-sm-4">
                  <?php 
                  $video_mime_type = '';
                  if(file_exists('uploads/prize/'. $record['media'])) {
                      $video_mime_type = get_mime_by_extension($record['media']);
                    }
                    ?>
                     <video width="400" height="240" controls>
                      <source src="<?php echo BASE_URL . 'uploads/prize/' . $record['media'] ?>" type="<?php echo $video_mime_type ?>">
                      </video>
                </div>
           </div>
         <?php endif; ?>
          <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-8">
                     <textarea name="description"><?php echo isset($record['description']) ? $record['description'] : '' ?></textarea>
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Stock</label>
                <div class="col-sm-4">
                     <input type="text" name="stock" value="<?php echo isset($record['stock']) ? $record['stock'] : '' ?>" onkeydown="return checkDigit(event)" data-name="stock" class="form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Status</label>
                <div class="col-sm-4">
                    <select class="form-control" name="status">
                      <option <?php echo isset($record['status']) && $record['status'] == 1 ? 'selected' : '' ?> value="1">Published</option>
                      <option <?php echo isset($record['status']) && $record['status'] == 0 ? 'selected' : '' ?> value="0">Unpublished</option>   
                    </select>
                </div>
           </div>
          <div class="form-group">
                <label class="col-sm-2 control-label">Send Type</label>
                <div class="col-sm-4">
                    <select class="form-control" name="send_type" id="type">
                      <option <?php echo isset($record['send_type']) && $record['send_type'] == 'Delivery' ? 'selected' : '' ?>>Delivery</option> 
                      <option <?php echo isset($record['send_type']) && $record['send_type'] == 'Redemption' ? 'selected' : '' ?>>Redemption</option>  
                    </select>
                </div>
           </div>
           <div class="form-group r-address-container" <?php if(!isset($record['send_type']) || (isset($record['send_type']) && $record['send_type'] == 'Delivery')): ?>style="display:none;" <?php endif; ?>>
                <label class="col-sm-2 control-label">Redemption Address</label>
                <div class="col-sm-8">
                     <textarea name="redemption_address"><?php echo isset($record['redemption_address']) ? $record['redemption_address'] : '' ?></textarea>
                </div>
           </div>

           <div class="form-group r-address-container" <?php if(!isset($record['send_type']) || (isset($record['send_type']) && $record['send_type'] == 'Delivery')): ?>style="display:none;" <?php endif; ?>>
                <label class="col-sm-2 control-label">Redemption Instructions</label>
                <div class="col-sm-8">
                     <textarea name="redemption_instruction"><?php echo isset($record['redemption_instruction']) ? $record['redemption_instruction'] : '' ?></textarea>
                </div>
            </div>

          <div class="form-group">
                <label class="col-sm-2 control-label">Start Date</label>
                <div class="col-sm-4">
                     <input type="text" name="start_date" value="<?php echo isset($record['start_date']) ? $record['start_date'] : '' ?>" data-name="start_date" class="form-control from">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">End Date</label>
                <div class="col-sm-4">
                     <input type="text" name="end_date" value="<?php echo isset($record['end_date']) ? $record['end_date'] : '' ?>" data-name="end_date" class="form-control from">
                </div>
           </div>
           
           <div class="form-group">
               <label class="col-sm-2 control-label">Has Prize?</label>
                <div class="col-sm-4">
                    <input type="radio" name="has_prize" value="1" <?php echo (isset($record['has_prize']) && $record['has_prize'])  == 1 ? 'checked' : '' ?> /> Yes 
                    <input type="radio" name="has_prize" value="0" <?php echo (isset($record['has_prize'])  && $record['has_prize']  == 0) || !isset($record['has_prize']) ? 'checked' : '' ?> />  No  
                </div> 
           </div>

           <div class="form-group">
               <label class="col-sm-2 control-label">Number of logins</label>
                <div class="col-sm-4">
                  <input type="text" name="display_number" class="form-control" onkeydown="return checkDigit(event)"  value="<?=isset($record['display_number']) ? $record['display_number'] : ''?>"/>
                </div> 
           </div>

           <div class="form-group">
               
               <label class="col-sm-2 control-label">Provinces to display</label>
               <div class="col-sm-4">
                 
                 <select name="display_province[]" multiple>
                   <option></option>
                   <?php if($provinces){
                          $selected_provinces = isset($record['display_province']) && $record['display_province'] ? explode(',',$record['display_province']) : array();
                          foreach($provinces->result_array() as $k=>$p){ ?>

                            <option <?=in_array($p['province_id'],$selected_provinces) ? 'selected' : ''?> value="<?=$p['province_id']?>"><?=$p['province']?></option>
                    <?php }
                        } ?>
                 </select>  
                 <br>
                 <strong>Press CTRL & click on the Provinces to select multiple.</strong>
               </div>

           </div>

           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <input type="hidden" name="submit" value="1" />
                     <input type="hidden" id="x" name="x" />
                     <input type="hidden" id="y" name="y" />
                     <input type="hidden" id="x2" name="x2" />
                     <input type="hidden" id="y2" name="y2" />
                     <input type="hidden" id="width" name="width" />
                     <input type="hidden" id="height" name="height"/>
                     <input type="hidden" name="id" value="<?php echo isset($record['prize_id']) ? $record['prize_id'] : '' ?>" />
                     <input type="hidden" name="filename" value="<?php echo isset($record['prize_image']) ? $record['prize_image'] : '' ?>" />
                     <?php if($controller == 'birthday_offer') :?>
                     <input type="hidden" name="origin_id" value="<?php echo isset($record['origin_id']) ? $record['origin_id'] : BIRTHDAY_OFFERS ?>" />
                     <?php endif; ?>
                     <?php if($controller == 'flash_offer') :?>
                     <input type="hidden" name="origin_id" value="<?php echo isset($record['origin_id']) ? $record['origin_id'] : FLASH_OFFERS ?>" />
                     <?php endif; ?>
                     <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                </div>
           </div>
      </form>
      
 </div>
 <!--end main content -->
 
