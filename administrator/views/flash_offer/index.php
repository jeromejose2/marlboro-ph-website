<?php 
  $controller = $this->uri->segment(1);
  $label = '';
  switch($controller) {
    case 'birthday_offer':
      $label = 'Birthday Offer';
      break;
    case 'flash_offer':
      $label = 'Flash Offer';
      break;
    default:
      $label = 'Prize';
      break;

  }
?>
<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3><?php echo $label ?>s <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/add" class="btn btn-primary">Add <?php echo $label ?></a>
                    <a href="<?php echo SITE_URL ?>/export/<?php echo $controller ?>/?<?=@http_build_query($this->input->get())?>" class="btn btn-primary">Export</a>
                    <?php endif; ?>
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Category</th>
                         <th>Type</th>
                         <th>Stock</th>
                         <th>Status</th>
                         <th>Send Type</th>
                         <th>Has Prize</th>
                         <th>Duration</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/<?php echo $controller ?>">
                     <tr>
                         <td></td>
                         <td><input name="prize_name" class="form-control" value="<?php echo isset($_GET['prize_name']) ? $_GET['prize_name'] : '' ?>" /></td>
                         <td>
                          <select class="form-control" name="category">
                              <option value=""></option>
                              <option value="1" <?php echo isset($_GET['category']) && $_GET['category'] == 1 ? 'selected' : '' ?>>Flash Offer</option>
                              <option value="2" <?php echo isset($_GET['category']) && $_GET['category'] == 2 ? 'selected' : '' ?>>Referral Offer</option>
                            </select>
                         </td>
                         <td>
                          <select class="form-control" name="type">
                              <option value=""></option>
                              <option value="Photo" <?php echo isset($_GET['type']) && $_GET['type'] == 'Photo' ? 'selected' : '' ?>>Photo</option>
                              <option value="Video" <?php echo isset($_GET['type']) && $_GET['type'] == 'Video' ? 'selected' : '' ?>>Video</option>
                              <option value="Text" <?php echo isset($_GET['type']) && $_GET['type'] == 'Text' ? 'selected' : '' ?>>Text</option>
                            </select>
                         </td>
                         </td>
                         <td><input name="stock" class="form-control" value="<?php echo isset($_GET['stock']) ? $_GET['stock'] : '' ?>" /></td>
                         <td>
                          <select class="form-control" name="status">
                              <option value=""></option>
                              <option value="1" <?php echo isset($_GET['status']) && $_GET['status'] == 1 ? 'selected' : '' ?>>Published</option>
                              <option value="0" <?php echo isset($_GET['status']) && $_GET['status'] == '0' ? 'selected' : '' ?>>Unpublished</option>
                            </select> 
                         </td>
                         </td>
                         <td>
                          <select class="form-control" name="send_type">
                              <option value=""></option>
                              <option value="Delivery" <?php echo isset($_GET['send_type']) && $_GET['send_type'] == 'Delivery' ? 'selected' : '' ?>>Delivery</option>
                              <option value="Redemption" <?php echo isset($_GET['send_type']) && $_GET['send_type'] == 'Redemption' ? 'selected' : '' ?>>Redemption</option>
                            </select>
                         </td>
                         <td>
                          <select class="form-control" name="has_prize">
                              <option value=""></option>
                              <option value="1" <?php echo isset($_GET['has_prize']) && $_GET['has_prize'] == 1 ? 'selected' : '' ?>>Yes</option>
                              <option value="0" <?php echo isset($_GET['has_prize']) && $_GET['has_prize'] == '0' ? 'selected' : '' ?>>No</option>
                            </select> 
                         </td>
                         <td>
                          <div class="row">
                            <div class="col-xs-6 col-md-4">
                              <input name="fromdate" placeholder="From" class="form-control from" value="<?php echo isset($_GET['fromdate']) ? $_GET['fromdate'] : '' ?>" />
                            </div>
                            <div class="col-xs-6 col-md-4">
                              <input name="todate" placeholder="To" class="form-control to" value="<?php echo isset($_GET['todate']) ? $_GET['todate'] : '' ?>" />
                            </div>
                          </div>
                         </td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
                    <?php if($categories): 
					foreach($categories as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['prize_name'] ?></td>
                         <td><?php echo $v['category']==1 ? 'Flash Offer' : 'Referral Offer' ?></td>
                         <td><?php echo $v['type'] ?></td>
                         <td><?php echo $v['stock'] ?></td>
                         <td><?php echo $v['status'] == 1 ? 'Published' : 'Unpublished' ?></td>
                         <td><?php echo $v['send_type'] ?></td>
                         <td><?php echo $v['has_prize'] == 1 ? 'Yes' : 'No' ?></td>
                         <td><?php echo date('F d, Y', strtotime($v['start_date']))  . ' - ' . date('F d, Y', strtotime($v['end_date']))?></td>
                         <td>
                            <?php if($edit): ?>
                            <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/edit/<?php echo $v['prize_id'] ?>" class="btn btn-primary">Edit</a>
                            <?php endif; ?>
                            <?php if($delete): ?>
                            <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/delete/<?php echo $v['prize_id'] ?>/<?php echo md5($v['prize_id'] . ' ' . $this->config->item('encryption_key')) ?>" onclick="return confirmDeletion(this, '<?php echo str_replace('_', ' ', $controller) ?>')" class="btn btn-danger">Delete</a>
                            <?php endif; ?>
                         </td>
                    </tr>
                    <?php 
					endforeach;
          else:
            echo '<tr><td colspan="7">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->