<?php if($row && $row->user_type=='administrator'){ ?>
<script type="text/javascript" src="<?=BASE_URL?>scripts/jwplayer/jwplayer.js"></script>
<?php } ?>
<?php $this->load->view('editor.php') ?>
<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3><?php echo $header_text; ?></h3>
      </div>

      <?php if($error){ ?>
       <div class="alert alert-danger"> <?php echo $error; ?> </div>
      <?php } ?>

       <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
          <input type="hidden" value="<?=($row) ? $row->user_type : ''?>" name="user_type"/>
           <div class="form-group">
                <label class="col-sm-2 control-label">Title</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo ($row) ? $row->title : '';  ?>" name="title" class="form-control required" data-name="statement">
                </div>
           </div>
           <?php
           if($this->router->method == 'add' || ($row && $row->user_type=='administrator')) { ?>
           <div class="form-group">
                <label class="col-sm-2 control-label">Video</label>
                <div class="col-sm-4">
                     <input type="file" name="video" data-name="module name" class="form-control">
                 </div>
           </div>
           <div class="form-group">
              <label class="col-sm-2 control-label"></label>
              <div class="col-sm-4">Recommended Video Formats: MP4 or FLV</div>
           </div>
           <?php } ?>


           <?php if($row){

                  if(file_exists('uploads/about/videos/'.@$row->video) && @$row->video){
                      $video_mime_type = get_mime_by_extension('uploads/about/videos/'.@$row->video); 
                      //$key = strpos($row->video,'.');
                      //$raw = substr($row->video,0,$key);
                      $thumbnail = ''; //BASE_URL.'uploads/about/videos/'.$raw.'.jpg'; ?>

                      <div class="form-group">
                      <label class="col-sm-2 control-label">Video Preview</label>
                          <div class="col-sm-4">
                             <video width="100%" height="100%"  id="video"  controls="controls" >
                             <source src="<?=BASE_URL.'uploads/about/videos/'.$row->video?>" type="<?=$video_mime_type?>" />
                               <script type="text/javascript">
                              function tryFlash(){
                                // flash fallback
                                jwplayer("video").setup({ flashplayer: "<?=BASE_URL?>scripts/jwplayer/player.swf",
                                            modes: [{ type: "html5" }, { type: "flash", src: "<?=BASE_URL?>scripts/jwplayer/player.swf"}]});  
                              }
                              function initVideo(){
                                var video = document.getElementById("video");
                                // check if html5 video tag is supported if not fallback to flash
                                if(!video.play ? true : false)
                                  tryFlash();
                              }
                              initVideo();
                              </script>
                              </video>

                          </div>
                      </div>

               <?php }else if($video_embed){ ?>

                    <div class="form-group">
                    <label class="col-sm-2 control-label">Video Preview</label>
                      <div class="col-sm-4">
                            <?=$video_embed?>
                            <input type="hidden" name="video_url" value="<?=$video_url?>"/>
                      </div>
                    </div>

               <?php } 
               } ?>
             
           <div class="form-group">
                <label class="col-sm-2 control-label">Status</label>
                <div class="col-sm-4">
                	<select class="form-control" name="status">
                      <option value="1" <?php echo ($row && $row->status==PENDING) ? 'selected' : '' ?>>Pending</option>
                     	<option value="1" <?php echo ($row && $row->status==APPROVED) ? 'selected' : '' ?>>Approved</option>
                      <option value="2" <?php echo ($row && $row->status==DISAPPROVED) ? 'selected' : '' ?>>Disapproved</option>
                  </select>
                </div>
           </div> 

           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>" class="pull-left"><< Back</a>
                     <button type="submit" class="btn btn-primary pull-right">Submit</button>

                </div>
           </div>
           
           
      </form>
      
 </div>
 <!--end main content -->
 