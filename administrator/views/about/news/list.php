<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3> <?php echo $header_text; ?> <span class="badge badge-important"><?php echo (int)$total_rows; ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/add'; ?>" class="btn btn-primary">Add</a>
                   	<?php endif; ?>
                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/export'.$query_strings; ?>" class="btn btn-primary">Export</a>   
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th><center>Photo</center></th>
                         <th>Title</th>
                         <th>Description</th>
                         <th>Status</th>
                         <th>Date Added</th>                         
                         <th> <?php if($edit || $delete) : ?>Operation <?php endif; ?></th>
                         
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>">
                     <tr>
                       <th></th>
                       <th><input type="text" name="title" class="form-control" value="<?php echo $this->input->get('title'); ?>"></th>
                       <th><input type="text" name="description" class="form-control" value="<?php echo $this->input->get('description'); ?>"></th>
                       <th><select name="status" class="form-control">
                              <option value=""></option>
                              <option value="1" <?php echo ($this->input->get('status')==1) ? 'selected' : ''; ?> >Published</option>
                              <option value="2" <?php echo ($this->input->get('status')==2) ? 'selected' : ''; ?> >Unpublished</option>
                            </select></th>
                       <th>
                         <div class="row">
                              <div class="col-md-6">
                                   <input name="from_date_added" placeholder="From" class="form-control from" value="<?php echo isset($_GET['from_date_added']) ? $_GET['from_date_added'] : '' ?>" />
                              </div>
                              <div class="col-md-6">
                                   <input name="to_date_added" placeholder="To" class="form-control to" value="<?php echo isset($_GET['to_date_added']) ? $_GET['to_date_added'] : '' ?>" />
                              </div>
                         </div>
                       </th>                       
                       <th><?php if($edit || $delete) : ?><button type="submit" class="btn btn-primary">Go</button><?php endif; ?></th>
                       
                     </tr>
                  </form>
                  <?php if($rows->num_rows()){

                            $edit_url = SITE_URL.'/'.$this->uri->segment(1).'/edit/';
 
                            foreach($rows->result() as $v){ ?>
                              <tr>
                                  <td><center><a class="view-video" href="<?=SITE_URL.'/'.$this->uri->segment(1).'/view_photo/'.$v->about_news_id?>"><img src="<?php echo BASE_URL.'/uploads/about/'.$this->uri->segment(1).'/50_50_'.$v->image.'?id='.uniqid(); ?>?" /></a></center></td>
                                  <td><?php echo $v->title; ?></td>
                                  <td><?php echo character_limiter($v->description,150); ?></td>
                                  <td><?php echo ($v->status==1) ? 'Published' : 'Unpublished'; ?></td>
                                  <td><?php echo $v->date_added; ?></td>
                                  <td>
                                    <?php if($edit) : ?>
                                    <a href="<?php echo $edit_url.$v->about_news_id; ?>" class="btn btn-primary">Edit</a>
                                    <?php endif; ?>

                                    <?php if($delete) : ?>
                                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/delete/'.$v->about_news_id .'/'.md5($v->about_news_id. ' ' . $this->config->item('encryption_key')); ?>" onclick="return confirmDeletion(this, '<?php echo $this->uri->segment(1); ?>')" class="btn btn-danger">Delete</a>
                                    <?php endif; ?>
                                  </td>
                               </tr>
                  <?php     }
                        }else{ ?>
                          <tr><td colspan="5">No records found.</td></tr>
                  <?php  } ?>
                  
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->

     <div id="play-video" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>
            </div>
          </div> 
      </div>

     <script>
    

     $('.view-video').on('click',function(e){
          var href = $(this).attr('href');
          $('.modal-content').html('<div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>');
          $('#play-video').modal('show');
          $('.modal-content').load(href);
          e.preventDefault();
     });

      


     $('#play-video').on('hidden.bs.modal',function(){
        $('.modal-content').html('<img src="<?=BASE_URL?>images/spinner.gif"/></div>');
     });
     </script>