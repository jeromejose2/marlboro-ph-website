<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3> <?php echo ($event)? $event->title.' - Photos ' : 'Event not found'; ?> <span class="badge badge-important"><?php echo (int)$total_rows; ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/add_photos/'.$about_event_id; ?>" class="btn btn-primary">Add Photo(s)</a>
                     <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/export_photos'.$query_strings; ?>" class="btn btn-primary" target="_blank">Export</a>   
                   	<?php endif; ?>
                </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                          <th><center>Photo</center></th>
                          <th>About Event</th>
                          <th>Submitted By</th>
                          <th>Status</th>
                           <th>Date Added</th>                                               
                          <th>Operation</th> 
                         
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/photos'; ?>">
                      <tr>
                        <th></th>
                        <th><select name="about_event_id" class="form-control" >
                             <?php if($events->num_rows()){
                              foreach($events->result() as $e){
                                      if($this->input->get('about_event_id')==$e->about_event_id){ ?>
                                        <option value="<?=$e->about_event_id?>" selected><?=$e->title?></option>
                                <?php }else{ ?>
                                        <option value="<?=$e->about_event_id?>"><?=$e->title?></option>
                                <?php } 
                                 }
                            } ?>
                          </select></th>

                        <th><input type="text" class="form-control" name="submitted_by" value="<?=$this->input->get('submitted_by')?>"></th>
                        <th><select name="status" class="form-control">
                              <option value=""></option>
                              <option value="0" <?php echo ($this->input->get('status')==='0') ? 'selected' : ''; ?> >Pending</option>
                              <option value="1" <?php echo ($this->input->get('status')=='1') ? 'selected' : ''; ?> >Approved</option>
                              <option value="2" <?php echo ($this->input->get('status')=='2') ? 'selected' : ''; ?> >Disapproved</option>
                           </select></th>
                          <th><select name="winner_status" class="form-control">
                              <option value=""></option>
                              <option value="1" <?=($this->input->get('winner_status')=='1') ? 'selected' : ''?> >Yes</option>
                              <option value="0" <?=($this->input->get('winner_status')==='0') ? 'selected' : ''?>>No</option>
                           </select></th>
                      <th>
                        <div class="row">
                          <div class="col-md-4">
                               <input name="from_date_added" placeholder="From" class="form-control from" value="<?=$this->input->get('from_date_added')?>" />
                          </div>
                          <div class="col-md-4">
                               <input name="to_date_added" placeholder="To" class="form-control to" value="<?=$this->input->get('to_date_added')?>" />
                          </div>
                        </div>
                    </th>            
                      <th><button type="submit" class="btn btn-primary">Go</button></th>
                     </tr>
                  </form>

                  <?php

                   if($rows->num_rows()){
                            $edit_url = SITE_URL.'/'.$this->uri->segment(1).'/edit/';
                            $status = array(0=>'Pending',1=>'Approved',2=>'Disapproved');
                            $about_event_id = ($event) ? $event->about_event_id : 0;
                            foreach($rows->result() as $v){  ?>
                              <tr>
                                  <td><center><a href="<?=BASE_URL.'admin/'.$this->uri->segment(1).'/view_photo/'.$v->photo_id?>" class="view-video" title="Click to Full view"><img src="<?=BASE_URL.'uploads/about/photos/thumbnails/100_100_'.$v->image?>" /></a></center></td>
                                  <td><?=$v->event_title?></td>
                                  <td><?=$v->uploader_name?></td>
                                  <td><?=$status[$v->status]?></td>
                                   <td><?=$v->date_added?></td>
                                  <td>
                                    <?php /* if($edit){ ?>


                                          <?php  if(!@$v->winner_status){ ?>                                    
                                                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/win/'.$v->photo_id.'/'.$this->input->get('about_event_id').'/'.md5($v->photo_id. ' ' . $this->config->item('encryption_key')); ?>" class="btn btn-success" onclick="return confirmAction(this,'win');">Winner</a>
                                           <?php } else{ ?>
                                                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/not_win/'.$v->photo_id.'/'.$this->input->get('about_event_id').'/'.md5($v->photo_id. ' ' . $this->config->item('encryption_key')); ?>" class="btn btn-success" onclick="return confirmAction(this,'reset as NOT winner');">Reset Winner</a>
                                           <?php }   ?>

                                    <?php } */ ?>
                                    <?php if($delete) : ?>
                                    <a href="<?php echo SITE_URL.'/events/delete_photo/'.$v->photo_id .'/'.$about_event_id.'/'.md5($v->photo_id. ' ' . $this->config->item('encryption_key')); ?>" onclick="return confirmDeletion(this, 'photo')" class="btn btn-danger">Delete</a>
                                    <?php endif; ?>
                                  </td>                                                                   
                              </tr>
                  <?php     } 
                        }else{ ?>
                          <tr><td colspan="7">No records found.</td></tr>
                  <?php  } ?>
                  
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->

     <div id="play-video" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>
            </div>
          </div> 
      </div>

     <script>
    

     $('.view-video').on('click',function(e){
          var href = $(this).attr('href');
          $('.modal-content').html('<div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>');
          $('#play-video').modal('show');
          $('.modal-content').load(href);
          e.preventDefault();
     }); 

     $('#play-video').on('hidden.bs.modal',function(){
        $('.modal-content').html('<img src="<?=BASE_URL?>images/spinner.gif"/></div>');
     });
     </script>