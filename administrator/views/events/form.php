<link rel="stylesheet" href="<?=base_url()?>admin_assets/css/timepicker.css">
<script src="<?=base_url()?>admin_assets/js/timepicker.js" type="text/javascript"></script>

<script src="https://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<style type="text/css">

.clinic-containter-form{
  width:175px;
  height:auto;
  float:left;
}

</style>
<script type="text/javascript">
<?php 
$lat = 0;
$lng = 0;
if($row && $row->google_lat_long){
  $lat_lng = explode(',',str_replace(array('(',')'), '', $row->google_lat_long));
  $lat = $lat_lng[0];
  $lng = $lat_lng[1];
}

?> 

var map;
var markers = [];
var infoWindow;
<?php // echo "var form_data = ".json_encode(array('name'=>strip_tags($name),'address'=>strip_tags($address),'branch_area'=>strip_tags($branch_area),'contact'=>strip_tags($contact))).";"; ?>

function initialize() {
 
   var mapOptions = {
    zoom: 6,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
  center: new google.maps.LatLng(12, 121)
  };
   
  
  map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
  //infoWindow = new google.maps.InfoWindow();
  
  <?php if((float)$lat > 0 && (float)$lng > 0){ ?>
   var latlng = new google.maps.LatLng(parseFloat(<?=$lat?>),parseFloat(<?=$lng?>));
     createMarker(latlng);
  <?php } ?>
  
}
 
function showAddress(address) {
     var geocoder = new google.maps.Geocoder();
   
   geocoder.geocode({address: address}, function(results, status) {
       if (status == google.maps.GeocoderStatus.OK) {
        // searchLocationsNear(results[0].geometry.location);
    createMarker(results[0].geometry.location);
    //console.log(results[0].geometry.location);
       } else {
         document.forms['form']['google_lat_long'].value='';
         alert(address + ' Not found');
       }
     });
  
} 

function createMarker(latlng) {
    var bounds = new google.maps.LatLngBounds();
    var marker = new google.maps.Marker({
    map: map,
    position: latlng,
    draggable: true
    });
 
    document.forms['form']['google_lat_long'].value=marker.position.toString(0);
        
    google.maps.event.addListener(marker, 'click', function() {
      //infoWindow.setContent(getClinicForm(latlng));
      document.forms['form']['google_lat_long'].value=latlng;
      //infoWindow.open(map, marker);
     });
      
     
     google.maps.event.addListener(marker, 'dragend', function() {
      //infoWindow.setContent(getClinicForm(marker.position.toString(0)));
      document.forms['form']['google_lat_long'].value=marker.position.toString(0);
      //infoWindow.open(map,marker);
     });
     
      bounds.extend(latlng);
    map.fitBounds(bounds);
    markers.push(marker);
}
  
 
google.maps.event.addDomListener(window, 'load', initialize);
 
</script>

 
<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3><?php echo $header_text; ?></h3>
      </div>

      <?php if($error){ ?>
       <div class="alert alert-danger"> <?php echo $error; ?> </div>
      <?php } ?>

       <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" id="form">

        <input type="hidden"  name="google_lat_long" value="<?php echo ((int)$lat && (int)$lng) ? $lat.','.$lng : ''; ?>" /> 
  
           <div class="form-group">
                <label class="col-sm-2 control-label">Event :</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo ($row) ? $row->title : '';  ?>" name="title" class="form-control required" data-name="statement">
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Description :</label>
                <div class="col-sm-4">
                     <textarea name="description" class="form-control required"><?php echo ($row) ? $row->description : '';  ?></textarea>
                </div>
           </div> 
 
           <div class="form-group">
                <label class="col-sm-2 control-label">Region :</label>
                <div class="col-sm-4">
                       <select name="region_id" class="form-control">
                          <option value=""></option>
                          <?php if($regions->num_rows()){

                                  foreach($regions->result() as $r){ 
                                    if($row && $row->region_id==$r->region_id){?>
                                      <option selected="selected" value="<?=$r->region_id?>"><?=$r->name?></option>
                          <?php     }else{ ?>
                                      <option value="<?=$r->region_id?>"><?=$r->name?></option>
                          <?php     }
                                  }

                                } ?>
                        </select>
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Image :</label>
                <div class="col-sm-4">
                     <input type="file" name="image" class="form-control required">
                </div>
           </div>

           <?php if($row && file_exists('uploads/backstage/events/331_176_'.$row->image)){ ?>

                <div class="form-group">
                  <label class="col-sm-2 control-label"></label>
                  <div class="col-sm-4">
                       <img src="<?php echo BASE_URL.'/uploads/backstage/events/'.'/331_176_'.$row->image.'?id='.uniqid(); ?>?" />
                  </div>
                </div>

          <?php } ?>
           

           <div class="form-group">
                <label class="col-sm-2 control-label">Start Date :</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo $row ? $row->start_date : '';  ?>" name="start_date" class="form-control required from" id="start_date">
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">End Date :</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo $row ? $row->end_date : '';  ?>" name="end_date" class="form-control required" id="end_date">
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Venue: </label>
                <div class="col-sm-4">

                  <select name="venue" class="form-control" onchange="showAddress(document.forms['form']['venue'].value);">
                          <option value=""></option>
                          <?php if($venues->num_rows()){

                                  foreach($venues->result() as $r){ 
                                    if($row && strtolower($row->venue)==strtolower($r->name)){?>
                                      <option selected="selected" value="<?=$r->name?>"><?=$r->name?></option>
                          <?php     }else{ ?>
                                      <option value="<?=$r->name?>"><?=$r->name?></option>
                          <?php     }
                                  }

                                } ?>
                        </select><br/>
                        <div id="map-canvas" style="width: 600px; height: 400px"></div>
                </div>
           </div>

           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4"> 
                 </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Featured:</label>
                <div class="col-sm-4">
                  <?php if($row && $row->is_featured=='1'){ ?>
                          <input type="radio" name="is_featured" checked value="1"> Yes &nbsp; &nbsp;
                          <input type="radio" name="is_featured" value="0"> No
                  <?php }else{ ?>
                          <input type="radio" name="is_featured" value="1"> Yes
                          <input type="radio" name="is_featured" checked value="0"> No
                  <?php } ?>
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Highlighted:</label>
                <div class="col-sm-4">
                  <?php if($row && $row->is_highlighted=='1'){ ?>
                          <input type="radio" name="is_highlighted" checked value="1"> Yes &nbsp; &nbsp;
                          <input type="radio" name="is_highlighted" value="0"> No
                  <?php }else{ ?>
                          <input type="radio" name="is_highlighted" value="1"> Yes
                          <input type="radio" name="is_highlighted" checked value="0"> No
                  <?php } ?>
                </div>
           </div>


           <div class="form-group">
                <label class="col-sm-2 control-label">Event Type :</label>
                <div class="col-sm-4">
                       <select name="event_type" class="form-control">
                          <option value=""></option>
                          <?php  if($event_types->num_rows()){

                                 foreach($event_types->result() as $r){ 
                                    if($row && strtolower($row->event_type)==strtolower($r->name)){?>
                                      <option selected="selected" value="<?=$r->name?>"><?=$r->name?></option>
                          <?php     }else{ ?>
                                      <option value="<?=$r->name?>"><?=$r->name?></option>
                          <?php     }
                                  } 

                                } ?>
                        </select>
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Status:</label>
                <div class="col-sm-4">
                  <select class="form-control" name="status">
                      <option value="1" <?php echo ($row && $row->status==APPROVED) ? 'selected' : '' ?>>Published</option>
                      <option value="2" <?php echo ($row && $row->status==DISAPPROVED) ? 'selected' : '' ?>>Unpublished</option>
                  </select>
                </div>
           </div>

           

           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4"> 
                     <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>" class="pull-left"><< Back</a>
                      <button type="submit" class="btn btn-primary pull-right">Submit</button>
                </div>
           </div>
           
           
      </form>
      
 </div>
 <!--end main content -->

 <script>
    $(function() {
        $("#start_date").datetimepicker({dateFormat:'yy-mm-dd'});
        $("#end_date").datetimepicker({dateFormat:'yy-mm-dd'});
    });
 </script>