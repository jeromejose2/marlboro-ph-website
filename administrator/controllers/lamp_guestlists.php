<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Lamp_Guestlists extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->_table = 'tbl_lamp_guestlists';

	}

	public function index() {

		$data = array(
			'main_content' 	=> $this->main_content(),
			'nav'			=> $this->nav_items()
		);
		$this->load->view('main-template', $data);

	}

	private function nav_items() {

		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, TRUE);

	}

	public function main_content() {

		$access = $this->module_model->check_access('lamp_guestlists');
		$page = $this->uri->segment(2, 1);
		$like = array();
		$where = 'lamp_guest_id LIKE \'%%\'';
		$data['offset'] = ( $page - 1 ) * PER_PAGE;

		if( isset($_GET['search']) ) {
			$where .= isset( $_GET['name'] ) && $_GET['name'] ? ' AND name LIKE \'%' . $_GET['name'] . '%\'' : FALSE;
		}

		$param = array(
			'offset'	=> $data['offset'],
			'limit'	=> PER_PAGE,
			'table'	=> $this->_table,
			'fields'	=> '*, tbl_lamp_guestlists.date_created as date_created',
			'join'	=> array(
						'tbl_lamp_events' => 'tbl_lamp_events.lamp_event_id = tbl_lamp_guestlists.lamp_event_id'
						),
			'where'	=> $where
		);
		$data['users'] = $this->global_model->get_rows( $param )->result_array();
		$records = $this->global_model->get_total_rows( $param );
		$data['pagination'] = $this->global_model->pagination( $records, $page, SITE_URL . '/lamp_guestlists' );
		$data['total'] = $records;

		return $this->load->view('perks/reserve/lamps/guestlists/lamp-guest-lists', $data, TRUE);

	}

	public function export() {

		$where = 'lamp_guest_id LIKE \'%%\'';

		if( isset($_GET['search']) ) {
			$where .= isset( $_GET['name'] ) && $_GET['name'] ? ' AND name LIKE \'%' . $_GET['name'] . '%\'' : FALSE;
		}

		$param = array(
			'offset'	=> 0,
			'table'	=> $this->_table,
			'fields'	=> '*',
			'join'	=> array(
						'tbl_lamp_events' => 'tbl_lamp_events.lamp_event_id = tbl_lamp_guestlists.lamp_event_id'
					),
			'where'	=> $where
		);

		$records = $this->global_model->get_rows( $param )->result_array();

		$row[] = array(
			'#',
			'Name',
			'Event',
			'Date'
		);

		if( $records ) {
			foreach( $records as $k => $v ) {
				$row[] = array(
					$k + 1,
					$v['name'],
					$v['event_title'],
					date('F d, Y H:i:s', strtotime($v['date_created']))
				);
			}
		}

		$this->load->library('to_excel_array');
		$this->to_excel_array->to_excel($row, 'lamp_guestlists_' . date('YmdHis'));

	}

	private function graph() {

 		$function_get_days = function($param) {
 			$date = date("t", strtotime($param));

			for($i = 1; $i <= $date; $i++) {
				$every_date = date("Y") . "-" . date("m") . "-" . str_pad($i, 2, "0", STR_PAD_LEFT);

				$data[] = array($every_date);
			}

			return $data;
 		};

 		$param = array(
 			'table' => $this->_table,
 			'where' => 'type LIKE \'%lamp%\'',
 		);
 		$raw_result = $this->global_model->get_rows($param)->result_array();

 		foreach($raw_result as $k => $v) {
 			$final[$k]['date_created'] = $v['date_created'];
 		}

 		$date1 = @date('Y-m-d', strtotime(min($final[0])));
 		$date2 = @date('Y-m-d', strtotime(max($final[0])));

 		$dates_count = $function_get_days($date1);

 		for($i = 0; $i < count($dates_count); $i++) {
 			$param = array(
 				'table' => $this->_table,
 				'where' => "type LIKE '%lamp%' AND date(date_created) = '" . $dates_count[$i][0] . "'",
 				'fields' => 'COUNT(*) as total'
 			);
 			$items = $this->global_model->get_rows($param)->result_array();
 			if($items) {
 				$checkins_daily['daily'][] = array($dates_count[$i][0], $items[0]['total']);
 			} else {
 				$checkins_daily["daily"][] = array($dates_count[$i][0], 0);
 			}
 		}

 		return $checkins_daily;

 	}

	public function _remap($method) {

		if( is_numeric( $method ) ) {
			$this->index();
		} else {
			$this->{$method}();
		}

	}

}