<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends CI_Controller {
	
	var $_origins;
	public function __construct() {
		parent::__construct();
		$this->load->model('module_model');
		$this->load->model('category_model');
		$this->_origins = $this->module_model->get_origins(array(BIRTHDAY_OFFERS, FLASH_OFFERS));
	}
	
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$where = '';
		if(isset($_GET['search'])) {
			$where =  " AND category_name LIKE '%$_GET[name]%'";
			$where .= $_GET['origin'] != '' ? " AND origin_id = '$_GET[origin]'" : "";
			$where .= $_GET['parent'] != '' ? " AND parent = '$_GET[parent]'" : "";
		}

		$page = $this->uri->segment(2, 1);
		$data['offset'] = ($page - 1) * PER_PAGE;
		$data['categories'] = $this->category_model->get_category($where, $page, PER_PAGE, $records);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/category');
		$data['main_categories'] = $this->get_main_categories();
		$data['origins'] = $this->_origins;
		$data['parent_categories'] = $this->category_model->get_parent_categories();
		$access = $this->module_model->check_access('category');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = $records;
		return $this->load->view('category/index', $data, true);		
	}
	
	private function get_main_categories() {
		$where['parent'] = 0;
		$where['is_deleted'] = 0;
		$parent_categories = $this->category_model->get_category($where, null, null,$count, true);
		$parents = array();
		if($parent_categories) {
			foreach($parent_categories as $k => $v) {
				$parents[$v['category_id']] = $v;
			}
		}
		return $parents;
	}
	
	public function add() {
		$data['main_content'] = $this->add_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function add_content() {
		$error = '';
		if($this->input->post('submit')) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules($this->get_rules());
			$valid = $this->form_validation->run();
			if($valid) {
				$this->category_model->save_category(false, $error);	
				if(!$error)
					redirect('category');
			} else {
				$error = validation_errors();
			}
		}
		$data['error'] = $error;
		$data['record'] = $_POST;
		$where['parent'] = 0;
		$where['is_deleted'] = 0;
		$data['main_categories'] = $this->category_model->get_category($where, null, null,$count, true);
		$data['origins'] = $this->_origins;
		return $this->load->view('category/add', $data, true);			
	}

	public function parent_categories(){

		$count = '';
		$where = array();
		$where['origin_id'] = $this->input->get('origin_id');
		$where['parent'] = 0;
		$where['is_deleted'] = 0;		
		$this->db->select('category_id,category_name');
		$data =  $this->db->get_where('tbl_categories', $where)->result_array();
		echo json_encode($data);
 
	}
	
	public function edit() {
		$id = $this->uri->segment(3);
		$data['main_content'] = $this->edit_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function edit_content($id) {
		$error = '';
		if($this->input->post('submit')) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules($this->get_rules());
			$valid = $this->form_validation->run();
			if($valid) {
				$this->category_model->save_category(true, $error);	
				if(!$error)
					redirect('category');
			} else {
				$error = validation_errors();
			}
		}
		$data['error'] = $error;
		$data['category'] = $_POST ? $_POST : $this->category_model->get_category(array('category_id'=> $id))[0];
		$where['parent'] = 0;
		$where['is_deleted'] = 0;
		$data['main_categories'] = $this->category_model->get_category($where, null, null,$count, true);
		$data['origins'] = $this->_origins;
		return $this->load->view('category/add', $data, true);			
	}
	
	
	public function delete() {
		$table = 'tbl_categories';
		$id = $this->uri->segment(3);
		$field = 'category_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/category') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$where[$field] = $id;
			$this->global_model->delete_record($table, $where);
		}
		redirect('category');
	}
	
	private function get_rules() {
		$config = array(
		   array(
				 'field'   => 'category_name',
				 'label'   => 'category name',
				 'rules'   => 'required'
			  )
		);
		return $config;
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		elseif($method == 'parent_categories')
			$this->parent_categories();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */