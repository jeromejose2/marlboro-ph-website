<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class move_fwd_new_gallery extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
		
 	} 

 	 
 	public function index()
	{
		error_reporting(E_ALL);
 		ini_set('display_errors', '1');
 		
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

  	}
 	 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);

	}


	public function main_content()
	{
		$this->load->helper('paging');
 		//$this->output->enable_profiler(TRUE);

		$backstage_event_id = $this->input->get('backstage_event_id');
		$offset = (int)$this->input->get('per_page');
	    $limit = PER_PAGE;
	    $filters = $this->get_filters();

		$access = $this->module_model->check_access('move_fwd_new_gallery');
		$total_rows = $this->explore_model->get_rows(array('table'=>'tbl_comments as c',
															'where'=>array_merge($filters['where_filters'],array('origin_id'=>MOVE_FWD,'c.is_deleted != ' => 1, "comment_image != '' "=>null,'c.admin_id !='=>0)),
															'join'=>array(array('table'=>'tbl_move_forward mf','on'=>'mf.move_forward_id=c.suborigin_id'),
		 												     				array('table'=>'tbl_registrants r','on'=>'r.registrant_id=c.registrant_id','type'=>'left'),
		 												     				array('table'=>'tbl_cms_users cu','on'=>'cu.cms_user_id=c.admin_id','type'=>'left'),
		 												     				),
														    'like'=>$filters['like_filters'],
															'fields'=>'c.comment_id'
			 													)
														    )->num_rows();
		$data['backstage_event_id'] = $backstage_event_id;
 		$data['header_text'] = 'Events';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total_rows'] = $total_rows;

		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_comments as c',
															 'where'=>array_merge($filters['where_filters'],array('c.origin_id'=>MOVE_FWD,'c.is_deleted != ' => 1,"c.comment_image != '' "=>null,'c.admin_id !='=>0)),
														     'like'=>$filters['like_filters'],
		 												     'order_by'=>array('field'=>'c.comment_date_created','order'=>'desc'),
		 												     'join'=>array(array('table'=>'tbl_move_forward mf','on'=>'mf.move_forward_id=c.suborigin_id'),
		 												     				array('table'=>'tbl_registrants r','on'=>'r.registrant_id=c.registrant_id','type'=>'left'),
		 												     				array('table'=>'tbl_cms_users cu','on'=>'cu.cms_user_id=c.admin_id','type'=>'left'),
		 												     				),
														     'limit'=>$limit,
														     'offset'=>$offset,
														     'fields'=>"c.comment_id,mf.move_forward_title,r.first_name,r.third_name,cu.name as admin_name,c.comment_image,c.registrant_id,c.admin_id,c.comment_date_created"
		  													)
												);
		$data['move_fwd_offers'] = $this->explore_model->get_rows(array('table'=>'tbl_move_forward mf',
															 'where'=> isset($filters['where_filters']['mf.move_forward_id']) ? $filters['where_filters'] : '',
														     // 'like'=>$filters['like_filters'],
		 												     'order_by'=>array('field'=>'mf.move_forward_title','order'=>'asc'),
		 												     'fields'=>'mf.move_forward_id,mf.move_forward_title'
		  													)
												);
		$data['query_strings'] = $filters['query_strings'];  
 		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']);
/* 		echo '<pre>';
 		print_r($data['move_fwd_offers']->result_array());
 		exit;*/
 		// echo '<pre>';
 		// print_r($data['pagination']);
 		// exit;
 		return $this->load->view('move_fwd/new_gallery/photo-lists',$data,TRUE);

	}

	public function save_media()
	{
 		$move_fwd_offer = $this->input->post('move_fwd_offer');
		$medias =  $this->input->post('media');
		$user = $this->login_model->extract_user_details();
 		$row = $this->explore_model->get_row(array('table'=>'tbl_comments','where'=>array('comment_id'=>$this->input->post('comment'))));
 
 		if($medias){ 
			
			foreach($medias as $media){

				$data = array();
				$output = json_decode($media); 

				if($output){

					if(!$row){

						$data = array_filter(array('admin_id'=>$user['cms_user_id'],
												'origin_id'=>MOVE_FWD,
												'suborigin_id'=>$move_fwd_offer,
												'comment_image'=>$output->image_file,
												'comment_video'=>$output->video_file,
												'comment_status'=>1,
												'date_added'=>array(array('field'=>'comment_date_created','value'=>'NOW()'))
								  			)
										);
						$this->explore_model->insert('tbl_comments',$data);

					}else if($row){

						$user = $this->login_model->extract_user_details();
 						$upload_path = 'uploads/profile/admin_'.$row->admin_id.'/comments/';

						$this->explore_model->update('tbl_comments',array('suborigin_id'=>$move_fwd_offer,'comment_image'=>$output->image_file),array('comment_id'=>$row->comment_id));
						
						if(file_exists($upload_path.$row->comment_image)){
							unlink($upload_path.$row->comment_image);
						}

						if(file_exists($upload_path.'150_150_'.$row->comment_image)){
							unlink($upload_path.'150_150_'.$row->comment_image);
						}

					}
				 }

			} # end foreach

		}else if($row){

			$this->explore_model->update('tbl_comments',array('suborigin_id'=>$move_fwd_offer),array('comment_id'=>$row->comment_id));

		}

	    redirect($this->router->class);
 
	}


	public function upload_media()
	{

		$this->load->helper('upload');

		$user = $this->login_model->extract_user_details();
		$row = $this->explore_model->get_row(array('table'=>'tbl_comments',
													'where'=>array('comment_id'=>$this->uri->segment(3))
													)
											);
		if($row){
			$upload_path = 'uploads/profile/admin_'.$row->admin_id.'/comments/';
		}else{
			$upload_path = 'uploads/profile/admin_'.$user['cms_user_id'].'/comments/';
		}
 		
 		$upload_path_thumbnail = $upload_path;
  		
 		$response = array('upload_error'=>null,'image_thumbnail'=>null,'upload_result'=>'');
 		$max_size = (strpos($_FILES['myFile']['type'],'image') === false) ? VIDEO_UPLOAD_LIMIT : FILE_UPLOAD_LIMIT;

 
		if(!is_dir($upload_path)){
			mkdir($upload_path,0777,true);
		}

		$params = array('upload_path'=>$upload_path,'allowed_types'=>FILE_UPLOAD_ALLOWED_TYPES.'|mp4','file_name'=>uniqid(),'max_size'=>$max_size,'max_width'=>0,'max_height'=>0,'do_upload'=>'myFile');
   		$file = upload($params);

    	if(is_array($file)){    		

	    	$image_file = $file['file_name'];
	    	$video_file = '';

	    	if(!$file['is_image']){

	    		$dir = dirname(__FILE__) . '/../../';
		    	$ret = system('ffmpeg -i ' . $dir . $upload_path . $file['file_name'] . ' -ss 00:00:03.000 -f image2 -vframes 1 ' . $dir . $upload_path. $file['raw_name'] . '.jpg');
 
		    	$image_file = $file['raw_name'].'.jpg';
		    	$video_file = $file['file_name'];

	    	}

	    	$this->load->helper('resize');

			$params = array('width'=>150,'height'=>150,'source_image'=>$upload_path.$image_file,'new_image_path'=>$upload_path_thumbnail,'file_name'=>$image_file);
			resize($params);	 

			$response['image_thumbnail'] = BASE_URL.$upload_path_thumbnail.'150_150_'.$image_file;
			$response['upload_result'] = array('image_file'=>$image_file,'video_file'=>$video_file);
		
	    }else{

	    	$response['upload_error'] = strip_tags($file);

	    }

		echo json_encode($response);		

	}

	public function add_media()
	{
		$data['header_title'] = 'Add Media';
		$data['file_label'] = 'Select Media: ';
		$data['move_fwd_offers'] = $this->explore_model->get_rows(array('table'=>'tbl_move_forward',
		 												     'order_by'=>array('field'=>'move_forward_title','order'=>'asc'),
		 												     'fields'=>'move_forward_id,move_forward_title'
		  													)
												);
 		$this->load->view('move_fwd/new_gallery/add-media',$data);

	}

	public function edit_media()
	{
		$this->load->helper('file');
		$data['header_title'] = 'Edit Media';
		$data['file_label'] = 'Change Media: ';
		$data['move_fwd_offers'] = $this->explore_model->get_rows(array('table'=>'tbl_move_forward',
		 												     'order_by'=>array('field'=>'move_forward_title','order'=>'asc'),
		 												     'fields'=>'move_forward_id,move_forward_title'
		  													)
												);
		$data['comment_id'] = $this->uri->segment(3);
 		$data['row'] = $this->explore_model->get_row(array('table'=>'tbl_comments','where'=>array('comment_id'=>$this->uri->segment(3))));		
		$this->load->view('move_fwd/new_gallery/edit-media',$data);
	}

	public function delete_media_files()
	{
		$table = 'tbl_comments';
		$id = $this->uri->segment(3);
		$field = 'comment_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/move_fwd_new_gallery') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$where[$field] = $id;
			$this->global_model->delete_record($table, $where);
		}
		redirect('move_fwd_new_gallery');

		 // $data = file_get_contents("php://input");
		 // $postData = json_decode($data);
		 // $user = $this->login_model->extract_user_details();
 		//  $upload_path = 'uploads/profile/admin_'.$user['cms_user_id'].'/comments/';

		 // if(file_exists($upload_path.$postData->image_file)){
		 // 	unlink($upload_path.$postData->image_file);
		 // }

		 // if(file_exists($upload_path.'150_150_'.$postData->image_file)){
		 // 	unlink($upload_path.'150_150_'.$postData->image_file);
		 // }

		 // if(file_exists($upload_path.$postData->video_file) && $postData->video_file){
		 // 	unlink($upload_path.$postData->video_file);
		 // }


	}


	public function get_filters()
	   {
		   
		   $where_filters = array('DATE(c.comment_date_created) >='=>'from_date_added','DATE(c.comment_date_created) <='=>'to_date_added', 'mf.move_forward_id' => 'move_fwd_offer');
		   $like_filters = array('cu.name'=>'submitted_by');
		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
		   
	   } 


	   public function export()
	   {
		    $this->load->library('to_excel_array');
		   	$filters = $this->get_filters();
 		   	$query = $this->explore_model->get_rows(array('table'=>'tbl_comments as c',
															 'where'=>array_merge($filters['where_filters'],array('c.origin_id'=>MOVE_FWD,'c.is_deleted != ' => 1,"c.comment_image != '' "=>null,'c.admin_id !='=>0)),
														     'like'=>$filters['like_filters'],
		 												     'order_by'=>array('field'=>'c.comment_date_created','order'=>'desc'),
		 												     'join'=>array(array('table'=>'tbl_move_forward mf','on'=>'mf.move_forward_id=c.suborigin_id'),
		 												     				array('table'=>'tbl_registrants r','on'=>'r.registrant_id=c.registrant_id','type'=>'left'),
		 												     				array('table'=>'tbl_cms_users cu','on'=>'cu.cms_user_id=c.admin_id','type'=>'left'),
		 												     				),
														     'fields'=>"c.comment_id,mf.move_forward_title,r.first_name,r.third_name,cu.name as admin_name,c.comment_image,c.registrant_id,c.admin_id,c.comment_date_created"
		  													)
												);
		    $res[] = array('Move FWD Offer','Uploded By','Date Added');
		    if($query->num_rows()){
		    	foreach($query->result() as $v){
			    	$res[] = array($v->move_forward_title,$v->admin_name,$v->comment_date_created);

		    	}
		    	
		    }

		    $this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));
	   }

	   public function view_photo()
	   {
	   	 $this->load->helper('file');
	   	 $id = $this->uri->segment(3);
	   	 $data['row'] = $this->explore_model->get_row(array('table'=>'tbl_comments as c',
															'where'=>array('c.comment_id'=>$this->uri->segment(3)),
															'join'=>array('table'=>'tbl_move_forward as mf','on'=>'c.suborigin_id=mf.move_forward_id','type'=>'left'),
															'fields'=>'c.*,mf.move_forward_title'
														)
													);
 	   	 $this->load->view('move_fwd/new_gallery/view-photo',$data);

	   }

 
}