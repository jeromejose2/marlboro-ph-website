<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Move_fwd_visits extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('report_model');
	}
	
	public function index()
	{
		if(!$this->input->get('fromdate') || !$this->input->get('todate')) {
			redirect('move_fwd_visits?fromdate=' . date('Y-m-d', strtotime('-30 days')) .  '&todate=' . date('Y-m-d'));
		}
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	public function export()
	{
		$this->load->library('to_excel_array');
		$records = $this->report_model->get_page_demographics('move_forward', $from, $to)->result_array();
		$row[] = $from && $to ? array('MoveFWD Visits (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')') : array('MoveFWD Visits');

		$row[] = array('Day', 'Visits', 'visitors');
		if($records) {
			foreach($records as $k => $v) {
				$row[] = array(date('F d, Y', strtotime($v['dv'])),
							   $v['count'],
							   $v['visitors']);
			}
		}
		$this->to_excel_array->to_excel($row, 'movefwd_visits_'.date("YmdHis"));
	}

	private function get_total_row() {
		$param['table'] = 'tbl_page_visits';
		$param['fields'] = 'DATE(date_visited) AS dv';
		$param['group_by'] = array('dv');
		return $this->global_model->get_rows($param)->num_rows();
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$total_visits = $total_visitors = 0;
		$field = $this->input->get('field') ? $this->input->get('field') : 'date_visited';
 		$sort = $this->input->get('sort') ? $this->input->get('sort') : 'asc';

		$data['records'] = $this->report_model->get_page_demographics('move_forward', $from, $to, $field, $sort)->result_array();
		if($data['records']) {
			foreach ($data['records'] as $key => $value) {
				$total_visits += $value['count'];
				$total_visitors += $value['visitors'];
			}
		}
		$total_rows = count($data['records']);
		$ave_visits = number_format($total_visits / $total_rows, 2);
		$ave_visitors = number_format($total_visitors / $total_rows, 2);
		$data['ave_visitors'] = $ave_visitors;
		$data['ave_visits'] = $ave_visits;
		$data['total_visits'] = $total_visits;
		$data['total_visitors'] = $total_visitors;
		return $this->load->view('visits/move_fwd', $data, true);		
	}

	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */