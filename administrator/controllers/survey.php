<?php if(! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Created By: Jerome Jose 
* Date created: 11-03-2015
*/
class Survey extends CI_Controller {


	public function __construct() 
	{
		parent::__construct();
		$this->load->model('explore_model');
	} 
	
	// NAV
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);

	}

	public function main_content()
	{
		$data = array();

		$this->db->start_cache();

		$search = array();
		$search['title'] = (string) $this->input->get('title');
		$search['is_active'] = (string) $this->input->get('is_active');
		$search['start_date'] = (string) $this->input->get('start_date');
		$search['end_date'] = (string) $this->input->get('end_date');

		$page = $this->uri->segment(2, 1);
		$this->db->select()
			->from('tbl_survey')->where('is_deleted','0');

		if ($search['title']) {
			$this->db->like('title', $search['title']);
		}

		if ($search['is_active'] != '') {
			$this->db->where('is_active', $search['is_active']);
		}

		if ($search['start_date']) {
			$this->db->like('start_date', $search['start_date']);
		}

		if ($search['end_date']) {
			$this->db->like('end_date', $search['end_date']);
		}

		$data['offset'] = ($page - 1) * PER_PAGE;
		$data['total'] = $this->db->count_all_results();
		$this->db->limit(PER_PAGE, $data['offset']);
		$data['records'] = $this->db->get()->result();

		$this->db->stop_cache();
		$this->db->flush_cache();

		$data['pagination'] = $this->global_model->pagination($data['total'], $page, SITE_URL.'/survey');
		

 		return $this->load->view('survey/index',$data,TRUE);
	}

  	// SURVEY
  	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
  	}
	
	public function add_survey()
	{
		$data['main_content'] = $this->add_survey_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
  	}

  
  	public function edit($id)
	{
		$data['main_content'] = $this->edit_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
  	}

  	public function delete($id)
	{
		$data['main_content'] = $this->delete_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
  	}
 	
 	//  QUESTIONS
 	public function add()
	{
		$data['main_content'] = $this->add_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
  	}

  	public function edit_question($id)
	{
		$data['main_content'] = $this->edit_question_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
  	}
	
	public function delete_question($uri, $id)
	{
		$data['main_content'] = $this->delete_question_content($uri, $id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
  	}

  	public function view_question($id)
	{
		$data['main_content'] = $this->view_question_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
  	}

	// CONTENTS - SURVEY
	public function add_survey_content()
	{
		$error ='';

		if($_POST) 
		{
			if( !$this->input->post('id'))
			{
				if($this->validate_survey_form()){
					$data2 = array(
								
								'title' => $this->input->post('survey_title'),
								'points' => $this->input->post('survey_points'),
								'source' => $this->input->post('survey_source'),
								'start_date' => $this->input->post('start_date'),
								'end_date' => $this->input->post('end_date'),
								'timestamp' => date('Y-m-d H:i:s'),
							);

					$inserted =  $this->db->insert('tbl_survey', $data2);
					if($inserted)
					{
						$this->session->set_flashdata('success', 'Survey has been successfully added.');
						redirect('survey');
					}
				}else{
					$error = validation_errors();
				}

			}else {
				if($this->validate_survey_form()){
					$data2 = array(
								
								'title' => $this->input->post('survey_title'),
								'points' => $this->input->post('survey_points'),
								'source' => $this->input->post('survey_source'),
								'start_date' => $this->input->post('start_date'),
								'end_date' => $this->input->post('end_date'),
								'timestamp' => date('Y-m-d H:i:s'),
							);

					$this->db->where('id',$this->input->post('id'));
	       			$this->db->update('tbl_survey', $data2);

	       			$this->session->set_flashdata('success-edit', 'Survey has been successfully updated.');
	       			redirect('survey');
	       		}else{
	       			$error = validation_errors();
	       		}
			}

		}

		
		$data = '';
		$data['error'] = $error;

		return $this->load->view('survey/add_survey',$data,TRUE);
  	}

	private function edit_content($id)
	{

		$this->db->select()->from('tbl_survey')->where('id',$id);
		$data['records'] = $this->db->get()->result();
		
 		return $this->load->view('survey/add_survey',$data,TRUE);
	}

	public function delete_content($id)
	{
		if($id)
		{
			$this->db->where('id',$id);
       		$this->db->update('tbl_survey', array('is_deleted' => 1));

       		$this->session->set_flashdata('success-delete', 'Survey has been successfully deleted.');
       		redirect('survey');
		}
	}

	public function set_active($id, $source)
	{

			if($id)
			{
				$this->db->start_cache();
				$this->db->where('source',$source);
				$this->db->update('tbl_survey', array('is_active' => 0));
				$this->db->stop_cache();
				$this->db->flush_cache();
				$this->db->where('id',$id);
				$this->db->update('tbl_survey', array('is_active' => 1));
				redirect('survey');
			}
	}

	
	// CONTENTS - QUESTION
	public function add_content()
	{	
		if($this->input->post('submit')) 
		{
			
			$counter = $this->input->post('x');

			$survey_title = $this->input->post('survey_title');
			
			if(isset($survey_title))
			{
				for ($i=1; $i < $counter +1 ; $i++) 
				{ 
					$label = array();
					$label2 = array();
					$question = $this->input->post('question-'.$i);
					$type = $this->input->post('type-'.$i);
					
				  	if($type == 1){
				  		$label = $this->input->post('label-radio-'.$i);
						$typeother = $this->input->post('typeother-radio-'.$i);
						$labelother = $this->input->post('labelother-radio-'.$i);

				  		if($label != ''){
							if($typeother == 'text')
							{
								$label[] = 'Others '.$labelother;
							}
						
						
							$label = serialize($label);
						}

						$data = array(
										'survey_id' => $survey_title,
										'question' => $question,
										'options' => $type,
										'choices' => $label,
										'checkbox_limit' => '',
										'checkbox_max' => '',
										'timestamp' => date('Y-m-d H:i:s'),
									);
					
						$question_inserted =  $this->db->insert('tbl_survey_questions', $data);

				  	}else if($type == 2){
				  		$label = $this->input->post('label-checkbox-'.$i);
						$typeother = $this->input->post('typeother-checkbox-'.$i);
						$labelother = $this->input->post('labelother-checkbox-'.$i);
						$checkbox_limit = $this->input->post('checkbox-limit-'.$i);
						$checkbox_max = $this->input->post('checkbox-max-'.$i);

				  		if($label != ''){
					  		if($typeother == 'text')
							{
								$label[] = 'Others '.$labelother;
							}
					
					
							$label = serialize($label);
						}

						$data = array(
										'survey_id' => $survey_title,
										'question' => $question,
										'options' => $type,
										'choices' => $label,
										'checkbox_limit' => $checkbox_limit,
										'checkbox_max' => $checkbox_max,
										'timestamp' => date('Y-m-d H:i:s'),
									);
					
						$question_inserted =  $this->db->insert('tbl_survey_questions', $data);
				  	}else if($type == 3){
					  	$label = $this->input->post('label-dropdown-'.$i);
						$typeother = $this->input->post('typeother-dropdown-'.$i);
						$labelother = $this->input->post('labelother-dropdown-'.$i);

				  		if($label != ''){	
					  		if($typeother == 'text')
							{
								$label[] = 'Others '.$labelother;
							}
						
						
							$label = serialize($label);
						}

						$data = array(
										'survey_id' => $survey_title,
										'question' => $question,
										'options' => $type,
										'choices' => $label,
										'checkbox_limit' => '',
										'checkbox_max' => '',
										'timestamp' => date('Y-m-d H:i:s'),
									);
					
						$question_inserted =  $this->db->insert('tbl_survey_questions', $data);
				  	}else if($type == 4){
				  		$text = array("text");

						$data = array(
										'survey_id' => $survey_title,
										'question' => $question,
										'options' => $type,
										'choices' => serialize($text),
										'checkbox_limit' => '',
										'checkbox_max' => '',
										'timestamp' => date('Y-m-d H:i:s'),
									);
					
						$question_inserted =  $this->db->insert('tbl_survey_questions', $data);
				  	}
					

				}

				if($question_inserted) {
					$this->session->set_flashdata('success', 'Question has been successfully added.');
					$this->db->start_cache();
					$this->db->select()->from('tbl_survey');
					$data['records'] = $this->db->select()->from('tbl_survey')->where('is_deleted','0');
					$this->db->stop_cache();
					$this->db->flush_cache();

					redirect('survey/view_question/'.$survey_title);
				}else{
					echo "error";
					redirect('survey/view_question/'.$survey_title);
				}
			
		 		return $this->load->view('survey/index',$data,TRUE);
			}	
		}
		

		$this->db->start_cache();
		$this->db->select()->from('tbl_survey_questions')->join('tbl_survey', 'tbl_survey.id = tbl_survey_questions.survey_id')->order_by('options','asc');
		$data['records'] = $this->db->get()->result();
		$this->db->stop_cache();
		$this->db->flush_cache();
		$data['surveys'] = $this->db->select()->from('tbl_survey')->where('is_deleted','0')->get()->result();

		return $this->load->view('survey/add',$data,TRUE);
	}


	public function edit_question_content($id)
	{	
		
		if($this->input->post('submit')) 
		{
		
			if(isset($id))
			{
				$label = array();
				$label2 = array();
				$question = $this->input->post('question-1');
				$type = $this->input->post('type-1');
				
			  	if($type == 1){
			  		$label = $this->input->post('label-radio-1');
					$typeother = $this->input->post('typeother-radio-1');
					$labelother = $this->input->post('labelother-radio-1');
					
			  		if($label != ''){
						if($typeother == 'text')
						{
							$label[] = 'Others '.$labelother;
						}
					
					
						$label = serialize($label);
					}

					$data = array(
									'question' => $question,
									'choices' => $label,
									'options' => $type,
								);


					
					$this->db->where('id',$id);
					$question_updated =  $this->db->update('tbl_survey_questions', $data);

			  	}else if($type == 2){
			  		$label = $this->input->post('label-checkbox-1');
					$typeother = $this->input->post('typeother-checkbox-1');
					$labelother = $this->input->post('labelother-checkbox-1');
					$checkbox_limit = $this->input->post('checkbox-limit-1');
					$checkbox_max = $this->input->post('checkbox-max-1');

			  		if($label != ''){
				  		if($typeother == 'text')
						{
							$label[] = 'Others '.$labelother;
						}
				
				
						$label = serialize($label);
					}

					$data = array(
									'question' => $question,
									'choices' => $label,
									'options' => $type,
									'checkbox_limit' => $checkbox_limit,
									'checkbox_max' => $checkbox_max,
								);
				
					$this->db->where('id',$id);
					$question_updated =  $this->db->update('tbl_survey_questions', $data);
			  	}else if($type == 3){
				  	$label = $this->input->post('label-dropdown-1');
					$typeother = $this->input->post('typeother-dropdown-1');
					$labelother = $this->input->post('labelother-dropdown-1');

			  		if($label != ''){	
				  		if($typeother == 'text')
						{
							$label[] = 'Others '.$labelother;
						}
					
					
						$label = serialize($label);
					}

					$data = array(
									'question' => $question,
									'choices' => $label,
									'options' => $type,
								);
				
					$this->db->where('id',$id);
					$question_updated =  $this->db->update('tbl_survey_questions', $data);
			  	}else if($type == 4){
			  		$text = array("text");

					$data = array(
									'question' => $question,
									'options' => $type,
								);
				
					$this->db->where('id',$id);
					$question_updated =  $this->db->update('tbl_survey_questions', $data);
			  	}

	
				if($question_updated) {
					$this->session->set_flashdata('success-edit', 'Question has been successfully updated.');
					$this->db->select()->from('tbl_survey');
					$data['records'] = $this->db->select()->from('tbl_survey')->where('is_deleted','0');

					redirect('survey/view_question/'.$_POST['survey_id']);
				}else{
					echo "error";
					redirect('survey');
				}
			
		 		return $this->load->view('survey/index',$data,TRUE);
			}	
		}

		$this->db->select()->from('tbl_survey_questions')->where('id', $id);
		$data['record'] = $this->db->get()->row();

		return $this->load->view('survey/edit_question',$data,TRUE);
	}

	public function delete_question_content($uri, $id)
	{
		if($id)
		{
			$this->db->where('id',$id);
       		$this->db->update('tbl_survey_questions', array('is_deleted' => 1));

       		$this->session->set_flashdata('success-delete', 'Question has been successfully deleted.');
       		redirect('survey/view_question/'.$uri);
		}
	}

	public function view_question_content($id)
	{
		if($id)
		{
			$data = array();

			$this->db->start_cache();

			$search = array();
			$search['question'] = (string) $this->input->get('question');
			// $search['is_active'] = (string) $this->input->get('is_active');
			// $search['start_date'] = (string) $this->input->get('start_date');
			// $search['end_date'] = (string) $this->input->get('end_date');

			$page = $this->uri->segment(2, 1);
			
			$this->db->select('tbl_survey_questions.id as q_id,tbl_survey_questions.*,tbl_survey.*')->from('tbl_survey_questions')->join('tbl_survey', 'tbl_survey.id = tbl_survey_questions.survey_id')
			->where('tbl_survey_questions.survey_id',$id)
			->where('tbl_survey_questions.is_deleted','0')->order_by('tbl_survey_questions.options','asc');
			

			if ($search['question']) {
				$this->db->like('question', $search['question']);
			}

			// if ($search['is_active'] != '') {
			// 	$this->db->where('is_active', $search['is_active']);
			// }

			// if ($search['start_date']) {
			// 	$this->db->like('start_date', $search['start_date']);
			// }

			// if ($search['end_date']) {
			// 	$this->db->like('end_date', $search['end_date']);
			// }

			// $data['offset'] = ($page - 1) * PER_PAGE;
			$data['total'] = $this->db->count_all_results();
			// $this->db->limit(PER_PAGE, $data['offset']);
			$data['records'] = $this->db->get()->result();

			$this->db->stop_cache();
			$this->db->flush_cache();

			$data['pagination'] = $this->global_model->pagination($data['total'], $page, SITE_URL.'/survey/view_question/'.$id);

		}
 		return $this->load->view('survey/viewquestion',$data,TRUE);
	}


	// OTHERS
	public function validate_survey_form()
	{

		$this->load->library('form_validation');

		$rules = array(
		   array(
				 'field'   => 'survey_title',
				 'label'   => 'Survey Title',
				 'rules'   => 'required'
			  ),

		   array(
				 'field'   => 'survey_points',
				 'label'   => 'Survey Points',
				 'rules'   => 'required'
			  ),

		   array(
				 'field'   => 'survey_source',
				 'label'   => 'Survey Source',
				 'rules'   => 'required'
			  ),

		   array(
				 'field'   => 'start_date',
				 'label'   => 'Start Date',
				 'rules'   => 'required'
			  ),

		   array(
				 'field'   => 'end_date',
				 'label'   => 'End Date',
				 'rules'   => 'required'
			  )
		);

		$this->form_validation->set_rules($rules);
		
		return $this->form_validation->run();
	}

	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'delete_question')
			$this->delete_question($this->uri->segment(3), $this->uri->segment(4));
		elseif($method == 'view_question')
			$this->view_question($this->uri->segment(3));
		elseif($method == 'edit_question')
			$this->edit_question($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		elseif($method == 'add_survey')
			$this->add_survey();
		elseif($method == 'set_active')
			$this->set_active($this->uri->segment(3), $this->uri->segment(4));
		elseif($method == 'test')
			$this->test();
		else
			$this->index();
	}


}
	
