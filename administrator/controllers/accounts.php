<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accounts extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function index() {
		if($this->session->userdata('user_level') != 1) {
			redirect('accounts/my_account', 'location');
		}
		$data = array( 'title'			=> "User Accounts",
					   'main_content'	=> $this->main_content());
 		$this->load->view('main-template', $data);
	}


	public function add(){
		$data = array( 'title'			=> "Add User",
					   'main_content'	=> $this->add_content());
 		$this->load->view('main-template', $data);
	}

	public function edit($id){
		$data = array( 'title'			=> "Edit User",
					   'main_content'	=> $this->edit_content($id));
 		$this->load->view('main-template', $data);
	}

	public function delete($id, $token = ''){
		$user = $this->db->get_where('tbl_cms_users', array('user_id'	=> $id))->result_array();	
		if(!is_numeric($id)) {
			die('Oops... Sorry, the page you are looking for is not available at the moment');
		}
		
		if($token == md5($this->config->item('encryption_key') . $id)) {
			$this->db->where('user_id', $id);
			$this->db->delete('tbl_cms_users');
		}
		
		$params['page'] = 'accounts';
		$params['action_text'] = $this->session->userdata('user_name') . ' deleted an account: ' . $user[0]['uname'];
		$this->main_model->save_audit_log($params);
		
 		redirect('accounts');
	}
	
	public function my_account() {
		$data = array( 'title'			=> "Edit User",
					   'main_content'	=> $this->my_account_content(),
					   'nav'			=> $this->nav_items());
 		$this->load->view('main-template', $data);	
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	/******************************************************/

	public function main_content() {
		/*$params = array('table' => 'tbl_cms_users',
						'where' => "user_id!='".$this->session->userdata('user_id')."'"
						);

		$items = $this->mysql_queries->get_data($params);*/
		$items = $this->db->get('tbl_cms_users')->result_array();
		$status = array('', 'Super Admin', 'CMS Manager', 'Viewer');
		$data = array('items' => $items,
					   'status'	=> $status);

		$main = $this->load->view('accounts/index', $data, TRUE);
		return $main;
	}

	public function add_content() {

		if($this->input->post('submit')) {
			$params['page'] = 'accounts';
			$params['action_text'] = $this->session->userdata('user_name') . ' added a user:  ' . $this->input->post('account_name');
			$this->main_model->save_audit_log($params);
			$this->save_account(false);	
		}
		// $params = array('table'=>'tbl_user_levels');
		// $data['user_levels'] = $this->mysql_queries->get_data($params);
		$data['title'] = 'Add User';
		$main = $this->load->view('accounts/add', $data, TRUE);
		return $main;
	}

	public function edit_content($id) {
		$data['title'] = 'Edit User';
		$user = $this->db->get_where('tbl_cms_users', array('user_id'	=> $id))->result_array();
		$data['user']  = @$user[0];
		
		if(isset($_POST['submit'])) {
			$params['page'] = 'accounts';
			$params['action_text'] = $this->session->userdata('user_name') . ' updated ' . $data['user']['account_name'] . '\'s account';
			$this->main_model->save_audit_log($params);
			
			$this->save_account(true);
		}
		
		$main = $this->load->view('accounts/add', $data, TRUE);
		return $main;
	}
	
	public function my_account_content() {
		$data = array();
		$user_details = $this->login_model->extract_user_details();
		$error = false;
		if($this->input->post('submit')) {
			$old_pass = $this->input->post('opassword');
			$n_pass = $this->input->post('npassword');
			$c_pass = $this->input->post('cpassword');
			$name = $this->input->post('name');
			if(!empty($c_pass) || !empty($n_pass) || !empty($old_pass)) {
				if($this->login_model->encrypt_password($old_pass) != $user_details['password']) {
					$error = 'Sorry, the old password you entered did not match our records';
				}
				
				if($c_pass != $n_pass) {
					$error = 'Sorry, the new passwords you entered did not match';
				}
				
				if($this->login_model->is_used_password($user_details['cms_user_id'], $n_pass)) {
					$error = 'Sorry, your new password must not be the same as your previous ones';
				}
				
				if(!$error) {
					$post['name'] = $name;
					$post['password'] = $this->login_model->encrypt_password($n_pass);
					$this->login_model->update_account($post, $user_details['cms_user_id']);
					redirect('auth/logout');	
				}
			} else {
				$post['name'] = $name;
				$this->login_model->update_account($post, $user_details['cms_user_id']);
				redirect('auth/logout');		
			}
		}
		
		
		$data['user_details'] = $user_details;
		$data['error'] = $error;
		$main = $this->load->view('accounts/myaccount', $data, TRUE);
		return $main;
	}
}

?>
