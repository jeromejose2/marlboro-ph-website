<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Profiling_graph extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
		//$this->output->enable_profiler(TRUE);
 	} 

 	 
 	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

 	}
 	 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
		$access = $this->module_model->check_access('profiling_graph');
		$filters = $this->get_filters();
 		$data['header_text'] = 'Profiling';
 		$data['query_strings'] = $filters['query_strings'];

		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_profiling as p',
															 'group_by'=>'p.interest',
															 'where'=>$filters['where_filters'],
															 'order_by'=>array('field'	=> $this->input->get('sort_by') ? $this->input->get('sort_by') : 'interest_count' , 'order'	=> $this->input->get('sort_order') ? $this->input->get('sort_order') :'ASC'),
														     'join'=>array('table'=>'tbl_categories as c','on'=>'c.category_id = p.interest'),
														     'fields'=>'p.profiling_id, c.category_name as interest, p.interest as interest_id, COUNT(p.interest) as interest_count, COUNT(interest) as percentage, p.date_added'
 														     )
														);

		$data['total_rows'] = $this->explore_model->get_rows(array('table'=>'tbl_profiling as p',
															 'where'=>$filters['where_filters'],
														     'fields'=>'p.profiling_id'
 														     )
														);
 

 		return $this->load->view('user_profiling/list-graph',$data,TRUE);


	}

	public function sub_interests(){

		 
		$filters = $this->get_filters();
		$data['query_strings'] = $filters['query_strings'];
 		$data['row'] = $this->explore_model->get_row(array('table'=>'tbl_categories',
													 'where'=>array('category_id'=>$this->input->get('interest')),
													 'fields'=>'category_name'
													 )
											 );
		  
		$rows = $this->explore_model->get_rows(array('table'=>'tbl_profiling as p',
 															 'where'=>$filters['where_filters'],
															 'order_by'=>array('field'	=> 'sub_interest' , 'order'	=> 'ASC'),
															 'group_by'=>'p.sub_interest',
														     'join'=>array('table'=>'tbl_categories as c','on'=>'c.category_id = p.sub_interest'),
														     'fields'=>'c.category_name, p.interest,p.sub_interest'
 														     )
														);
		$items = array();

		if($rows->num_rows()){

			foreach($rows->result() as $r){

				$where = array_merge($filters['where_filters'],array('sub_interest'=>$r->sub_interest));
				$user = $this->explore_model->get_row(array('table'=>'tbl_profiling as p',
															 'where'=>$where,
														     'fields'=>'COUNT(p.sub_interest) as total_count'
 														     )
															);

				$items[] = array('category_name'=>$r->category_name,'interest'=>$r->interest,'sub_interest'=>$r->sub_interest,'user_count'=>$user->total_count);

			}


		}
		$data['items'] = $items;

		$this->load->view('user_profiling/sub-interests-view',$data);
	}

	 

	public function get_filters()
   {
	   
	   $where_filters = array('DATE(p.date_added) >='=>'from_date_added',
	   						  'DATE(p.date_added) <='=>'to_date_added',
	   						  'p.interest'=>'interest',
	   						  'p.sub_interest'=>'sub_interest');
	   $like_filters = array("CONCAT(r.first_name,' ', r.third_name)"=>'registrant_name');
	   $query_strings = array();
	   
	   $valid_where_filters = array();
	   $valid_like_filters = array();
	   
	   foreach($where_filters as $column_field=>$filter){
		    
				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
					$valid_where_filters[$column_field] = trim($this->input->get($filter));
					$query_strings[$filter] = $this->input->get($filter);
				}
								 
			  
		   } 
	    
	   
	   foreach($like_filters as $column_field=>$filter){
		   
				if($this->input->get($filter)){
					$valid_like_filters[$column_field] = trim($this->input->get($filter));
					$query_strings[$filter] = $this->input->get($filter);
				}
				
			 
			   
	   } 

		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
		   
	   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
	   
	   } 


	   public function export()
	   {
	   		$this->load->library('to_excel_array');

		   	$filters = $this->get_filters();
		   	$params = 
		   	$query = $this->explore_model->get_rows(array('table'=>'tbl_profiling as p',
															 'where'=>$filters['where_filters'],
														     'like'=>$filters['like_filters'],
														     'order_by' => array('field'=>'p.date_added','order'=>'DESC'),
														     'join'=>array(array('table'=>'tbl_categories as c','on'=>'c.category_id = p.interest'),
														     			   array('table'=>'tbl_categories as c2','on'=>'c2.category_id = p.sub_interest'),
														     			   array('table'=>'tbl_registrants as r','on'=>'r.registrant_id = p.registrant_id')
														     			   ),
														     'fields'=>'p.profiling_id, c.category_name as interest, c2.category_name as sub_interest, r.first_name, r.third_name, p.date_added'
 														     )
														);

		   	$res[] = array('Registrant','Interest','Sub Date','Interest Added');

		   	if($query->num_rows()){
		   		foreach($query->result() as $v){
 		   			$res[] = array($v->first_name.' '.$v->third_name,$v->interest,$v->sub_interest, $v->date_added);
		   		}
		   	}
		   	$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));

	   }


 
}