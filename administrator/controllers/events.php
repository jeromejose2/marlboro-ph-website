<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Events extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
		//$this->output->enable_profiler(TRUE);
 	} 

 	 
 	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

 	}
 	 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
		$this->load->helper('paging');
		$offset = (int)$this->input->get('per_page');
	    $limit = PER_PAGE;
	    $filters = $this->get_filters(); 

		$access = $this->module_model->check_access('events');
		$total_rows = $this->explore_model->get_rows(array('table'=>'tbl_about_events',
																 'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
																 'like'=>$filters['like_filters']																 
																 )
														    )->num_rows();
 		$data['header_text'] = 'Events';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total_rows'] = $total_rows;
 		
		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_about_events as p',
															 'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
														     'like'=>$filters['like_filters'],
														     'order_by' => array('field'=>'date_added','order'=>'DESC'),
														     'offset'=>$offset,
														     'limit'=>$limit
 														     )
														);
  
 		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']);
 		return $this->load->view('about/'.$this->uri->segment(1).'/list',$data,TRUE);


	}

	public function photos()
	{
		$this->load->helper('paging');

		$about_event_id = $this->input->get('about_event_id');
		$offset = (int)$this->input->get('per_page');
	    $limit = PER_PAGE;
	    $filters = $this->get_filters();

		$access = $this->module_model->check_access('events');
		$total_rows = $this->explore_model->get_rows(array('table'=>'tbl_about_events_photos as p',
																 'where'=>array_merge($filters['where_filters'],array('p.is_deleted'=>0,'p.about_event_id'=>$about_event_id)),
															     'like'=>$filters['like_filters'],
															     'fields'=>'p.photo_id'
			 													)
														    )->num_rows();

		$data['about_event_id'] = $about_event_id;
 		$data['header_text'] = 'Events';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total_rows'] = $total_rows;
		$data['event'] = $this->explore_model->get_row(array('table'=>'tbl_about_events','where'=>array('about_event_id'=>$about_event_id,'is_deleted'=>0)));
		$data['events'] = $this->explore_model->get_rows(array('table'=>'tbl_about_events',
															 'where'=>array('is_deleted'=>0)
 														     )
														);
		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_about_events_photos as p',
													 'where'=>array_merge($filters['where_filters'],array('p.is_deleted'=>0,'p.about_event_id'=>$about_event_id)),
													 'join'=>array('table'=>'tbl_about_events as e','on'=>'e.about_event_id=p.about_event_id'),
												     'like'=>$filters['like_filters'],
												     'limit'=>$limit,
												     'offset'=>$offset,
												     'order_by'=>array('field'=>'p.date_added','order'=>'DESC'),
												     'fields'=>'p.photo_id,p.uploader_id,p.uploader_name,p.about_event_id,p.image,p.user_type,p.status,p.winner_status,p.date_added,e.title as event_title'
 													)
												);
		$data['query_strings'] = $filters['query_strings'];
  		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).'/photos/'.$filters['query_strings']);
 		$data['main_content'] = $this->load->view('about/events/photos_list',$data,TRUE);
 		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

	}


	public function add_photos()
	{

		$id = (int)$this->uri->segment(3);

		$error = '';
 
		if($this->input->post()) {

			$post_data = $this->input->post();
			$event = $this->explore_model->get_row(array('table'=>'tbl_about_events','where'=>array('about_event_id'=>$id)));

			if($this->validate_photo_form() && $event) {
 
				 
				$photos = $this->do_upload();
				$temp = 1;

				if($photos){
					foreach($photos as $v){
						$this->explore_model->insert('tbl_about_events_photos',$v);
						$temp++;
					}
				}		   
				

	   		    $post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/add';
				$post['description'] = ($temp > 1) ? 'added a new photos in '.$event->title.' event' : 'added a new photo '.$event->title.' event';
				$post['table'] = 'tbl_about_events';
				$post['record_id'] = $id;
				$post['type'] = 'add';
				$this->module_model->save_audit_trail($post);

	   		    redirect($this->uri->segment(1).'/photos?about_event_id='.$this->uri->segment(3)); 

			} else {

				$post_data['image'] = '';
				$error = validation_errors();
				$row = json_decode(json_encode($post_data),false);

			}
 
		} 
		

		$data['error'] = $error;
		$data['event'] = $this->explore_model->get_row(array('table'=>'tbl_about_events','where'=>array('about_event_id'=>$id),'fields'=>'title,about_event_id'));
		$data['row_photos'] =  '';
 		$data['header_text'] = 'Event';
 		$data['about_event_id'] = $id;

 		$data['main_content'] = $this->load->view('about/events/photos_form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
 
	} 


	public function validate_photo_form(){
		/// Temporary
		return true;
	}

	public function print_array($data)
	{
		echo "<pre>";
		print_r($data);
		echo "</pre>";
	}


	public function add()
	{

		$error = '';
		$row = '';

		if($this->input->post()) {

			$post_data = $this->input->post();	
 

			if($this->validate_form()) {
 
				
 				$data = $post_data; 
				$data['date_added'] = TRUE; // Set NOW() value to date_added column field
				$data['url_title'] = url_title(ucwords(strtolower(trim($post_data['title']))));
 				$this->explore_model->insert('tbl_about_events',$data); 

	   		    $post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/add';
				$post['description'] = 'added a new event';
				$post['table'] = 'tbl_about_events';
				$post['record_id'] = $id;
				$post['type'] = 'add';
				$this->module_model->save_audit_trail($post);

	   		    redirect($this->uri->segment(1)); 

			} else {

				$post_data['image'] = '';
				$error = validation_errors();
				$row = json_decode(json_encode($post_data),false);

			}
 
		} 
		

		$data['error'] = $error;
		$data['row'] = $row;
		$data['row_photos'] =  '';
 		$data['header_text'] = 'Event'; 

 		$data['main_content'] = $this->load->view('about/'.$this->uri->segment(1).'/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
 
	} 



	public function edit()
	{

		$error = '';
		$id = $this->uri->segment(3);
		$row = $this->explore_model->get_row(array('table'=>'tbl_about_events','where'=>array('about_event_id'=>$id,'is_deleted'=>0)));

		if($this->input->post())
		{
			$post_data = $this->input->post();

			if($this->validate_form()){

				$photo = json_decode(json_encode($row),TRUE);


				/* Insert action history */

				$fields = array('title','status');
				$status = array('', 'Published', 'Unpublished');

				$new_content = array();
				$old_content = array();

				foreach($photo as $k => $v) {
					if(in_array($k, $fields)) {
						if($photo[$k] != $this->input->post($k)) {

							if($k == 'status') {
								
								$new_content[$k] = $status[$this->input->post($k)];
								$old_content[$k] = $status[$photo[$k]];

							}else {
								$new_content[$k] = $this->input->post($k);
								$old_content[$k] = $photo[$k];
							}

						}
					}
				}

				$post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/edit/' . $id;
				$post['description'] = 'updated an event';
				$post['table'] = 'tbl_about_events';
				$post['record_id'] = $id;
				$post['type'] = 'edit';
				$post['field_changes'] = serialize(array('old'	=> $old_content,
														 'new'	=> $new_content));
				$this->module_model->save_audit_trail($post);

				/* End Insert action history */



				$post_data['date_updated']	= TRUE; // Set NOW() value to column date_updated
				$post_data['url_title'] = url_title(ucwords(strtolower(trim($post_data['title']))));
				$this->explore_model->update('tbl_about_events',$post_data,array('about_event_id'=>$id,'is_deleted'=>0));
				redirect($this->uri->segment(1));

			}else{

				$error = validation_errors();
 				$row = json_decode(json_encode($post_data),false);

			}


		}
 		
  
		$data['row'] = $row; 
		$data['error'] = $error;
		$data['header_text'] = 'Event';
		$data['main_content'] = $this->load->view('about/'.$this->uri->segment(1).'/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

		//$this->output->enable_profiler(TRUE);

	}


	public function delete()
	{

 		$id = $this->uri->segment(3);
 		$token = $this->uri->segment(4);

		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
 			$this->explore_model->update('tbl_about_events',array('is_deleted'=>1),array('about_event_id'=>$id));

 			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'deleted an event';
			$post['table'] = 'tbl_about_events';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);

		}
		redirect($this->uri->segment(1));

	}

	public function delete_photo()
	{

 		$id = $this->uri->segment(3);
 		$about_event_id = $this->uri->segment(4);
 		$token = $this->uri->segment(5);
 		$event = $this->explore_model->get_row(array('table'=>'tbl_about_events','where'=>array('about_event_id'=>$about_event_id)));

		if($event && strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
 			$this->explore_model->update('tbl_about_events_photos',array('is_deleted'=>1),array('photo_id'=>$id));
 			

 			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'deleted a photo in '.$event->title.' event';
			$post['table'] = 'tbl_about_events_photos';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);
 		}
	   redirect($this->uri->segment(1).'/photos?about_event_id='.$about_event_id);

	}

	public function validate_form()
	{

		$this->load->library('form_validation');

		$rules = array(
		   array(
				 'field'   => 'title',
				 'label'   => 'Title',
				 'rules'   => 'required|callback_validate_title'
			  )
		);

		$this->form_validation->set_rules($rules);
		
		return $this->form_validation->run();
	}

	public function validate_title($title)
	{	
		$url_title = url_title(ucwords(strtolower(trim($title))));
		$row = $this->explore_model->get_row(array('table'=>'tbl_about_events',
												   'where'=>array('url_title'=>$url_title,'is_deleted'=>0)
												   )
											);

		if($row && !$this->uri->segment(3)){
			$this->form_validation->set_message('validate_title','Title already exists!');
			return false;
		}else{
			return true;
		}

	}



	

	public function get_filters()
	   {
		   
		   $where_filters = array('status'=>'event_status','DATE(date_added) >='=>'event_from_date_added','DATE(date_added) <='=>'event_to_date_added','p.about_event_id'=>'about_event_id','p.status'=>'status','p.winner_status'=>'winner_status','DATE(p.date_added) >='=>'from_date_added','DATE(p.date_added) <='=>'to_date_added');
		   $like_filters = array('p.uploader_name'=>'submitted_by','title'=>'title');
		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
		   
	   } 


	   public function export()
	   {
	   		$this->load->library('to_excel_array');

		   	$filters = $this->get_filters();
		   	$params = 
		   	$query = $this->explore_model->get_rows(array('fields'=>"title,status,date_added",
		   														'table'=>'tbl_about_events',
											   					'where'=>array_merge(array('is_deleted'=>0),$filters['where_filters']),
											   					'like'=>$filters['like_filters'],
 											   					)
		   													);
		   	$res[] = array('Title','Status','Date Added');

		   	if($query->num_rows()){
		   		foreach($query->result() as $v){
		   			$status = ($v->status) ? 'Published' : 'Unpublished';
		   			$res[] = array($v->title,$status,$v->date_added);
		   		}
		   	}
		   	$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));

	   }


	   public function export_photos()
	   {

		   	$filters = $this->get_filters();
		   	$where_filters = $filters['where_filters'];
		   	$like_filters = $filters['like_filters'];
		   	$where = '';
		   	$like = '';

		   	$sql = " SELECT CONCAT('".BASE_URL."/uploads/about/photos/','',p.image) as Image, e.title as Event_title, p.uploader_name as Submitted_by, p.date_added as Date_Added FROM tbl_about_events_photos as p JOIN tbl_about_events as e ON p.about_event_id=e.about_event_id ";
		   	if($where_filters){	   		

		   		
		   		foreach($where_filters as $key=>$v){
		   			$where[] = $key.'='.$v;
		   		}


		   		$where = implode(' AND ',$where);

		   		
		   	}
		   	
		   	
		   	


		   	if($like_filters){
 
		   		foreach($like_filters as $key=>$v){
		   			$like[] = $key." LIKE '%".$v."%' ";
		   		}

		   		$like = ($where) ? ' AND '.implode(' AND ',$like) : implode(' AND ',$like);
		   	}
 

		   	if($where || $like){
		   		$sql = $sql.' WHERE '.$where.$like;
		   	}
 

		    

		   	$data['query'] = $this->explore_model->custom_query($sql);
		   	$data['filename'] = $this->uri->segment(1).'_'.date('M-d-Y');
		    $this->load->view('about/export-to-excell',$data);
	   }


	   public function do_upload()
	{

		   $this->load->helper(array('resize','upload'));

		   $max = (int)$this->input->post('image_counter');
  
		   

		   $i = 1;
		   $temp = 1;
		   $thumb = '_';		   
		   $id = (int)$this->explore_model->get_last_id('tbl_about_events_photos','photo_id') + 1;	   

		   $images = array();
		   $about_event_id = $this->uri->segment(3);
 		   $user = $this->login_model->extract_user_details();
 		   $upload_path = 'uploads/about/photos/';
 		   $upload_path_thumbnail = 'uploads/about/photos/thumbnails/';

 		   if(!is_dir($upload_path)){
 		   		mkdir($upload_path,0777,true);
 		   }

 		   if(!is_dir($upload_path_thumbnail)){
 		   		mkdir($upload_path_thumbnail,0777,true);
 		   }
		   

  		   while($i<=$max){
			   

			   if(isset($_FILES["slider-image".$i]["error"]) && $_FILES["slider-image".$i]["error"] < 1){ 	   
 

 				   $params = array('upload_path'=>$upload_path,
 				   					'allowed_types'=>'png|jpg|gif',
 				   					'file_name'=>'',
 				   					'max_size'=>'2097152',
 				   					'max_width'=>0,
 				   					'max_height'=>0,
 				   					'do_upload'=>'slider-image'.$i);
				   $file = upload($params);    

				   if(is_array($file)){


					   $new_filename = strtolower($id.$thumb.$i.$file['file_ext']);	/// New filename			   
					   while(file_exists($upload_path.$new_filename)){ //Check file exists
					 	  $temp++;
					 	  $new_filename = strtolower($id.$thumb.$temp.$file['file_ext']);
					    }		   

					   rename($upload_path.$file['file_name'],'uploads/about/photos/'.$new_filename); // Rename file



 				   	  $params = array('width'=>100,'height'=>100,'source_image'=>$upload_path.$new_filename,'new_image_path'=>$upload_path_thumbnail,'file_name'=>$new_filename);
					  resize($params);	

					  $params = array('width'=>331,'height'=>176,'source_image'=>$upload_path.$new_filename,'new_image_path'=>$upload_path_thumbnail,'file_name'=>$new_filename);
					  resize($params);

					  $data[] = array('about_event_id'=>$about_event_id,
				   					'uploader_id'=>$user['cms_user_id'],
				   					'uploader_name'=>$user['name'],
				   					'user_type'=>'administrator',
 				   					'is_deleted'=>0,
				   					'status'=>1,
				   					'image'=>$new_filename,
				   					'date_added'=>TRUE);
 				   }

			   }

			   $i++;			

		   }

		   return $data;


 	   } 


 	public function win()
	{
		$id = $this->uri->segment(3);
		$about_event_id = $this->uri->segment(4);
 		$token = $this->uri->segment(5);
 		$row = $this->explore_model->get_row(array('table'=>'tbl_about_events','where'=>array('about_event_id'=>$about_event_id)));

		if($row && strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
 			$this->explore_model->update('tbl_about_events_photos',array('winner_status'=>1),array('photo_id'=>$id));

 			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'set a photo entry winner in '.ucwords($row->title).' event';
			$post['table'] = 'tbl_about_events_photos';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);

			$this->Points_Model->earn(ABOUT_PHOTOS, array('suborigin' => $id));
			
  		}

		redirect($this->uri->segment(1).'/photos?about_event_id='.$about_event_id);
	}

	public function not_win()
	{

		$id = $this->uri->segment(3);
		$about_event_id = $this->uri->segment(4);
 		$token = $this->uri->segment(5);
 		$row = $this->explore_model->get_row(array('table'=>'tbl_about_events','where'=>array('about_event_id'=>$about_event_id)));

		if($row && strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
 			$this->explore_model->update('tbl_about_events_photos',array('winner_status'=>0),array('photo_id'=>$id));
 			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'reset a photo entry as not winner in '.ucwords($row->title).' event';
			$post['table'] = 'tbl_about_events_photos';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);
  		}

		redirect($this->uri->segment(1).'/photos?about_event_id='.$about_event_id);

	}

	public function view_photo()
   {
   	 $id = $this->uri->segment(3);
   	 $data['row'] = $this->explore_model->get_rows(array('table'=>'tbl_about_events_photos as p',
												  'where'=>array('p.photo_id'=>$id),
												  'join'=>array('table'=>'tbl_about_events as e','on'=>'e.about_event_id=p.about_event_id'),
												  'fields'=>'e.title,p.image,p.uploader_name'
													)
											)->row();
   	 $this->load->view('about/events/view-photo',$data);

   }

 
}