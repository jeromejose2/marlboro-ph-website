<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Move_fwd extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		$this->load->model('module_model');
		$this->load->model('category_model');
		$this->_table = 'tbl_move_forward';
 	}
	
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		$challenge = '';
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			$challenge = $like['challenges'];
			unset($like['search']);
			unset($like['challenges']);
			unset($like['limit']);
		}
		$param['having'] = $like;
		$param['offset'] = $data['offset'];
		$param['limit'] = PER_PAGE;
		$param['table'] = $this->_table;
		$param['fields'] = '*, (SELECT GROUP_CONCAT(challenge SEPARATOR \'<br><br>\') FROM tbl_challenges WHERE tbl_challenges.move_forward_id = ' . $this->_table . '.move_forward_id) AS challenges';
		//$param['join'] = array('tbl_challenges', 'tbl_challenges.move_forward_id = tbl_move_forward.move_forward_id'); 
		$param['order_by'] = array('field'=>'date_added','order'=>'DESC');
		$param['having'] = array('challenges LIKE ' => '%' . $challenge . '%', 'is_deleted' => 0);
		$param['like'] = $like;
		$data['categories'] = $this->global_model->get_rows($param)->result_array();
		$records = $this->global_model->get_total_rows($param);
		$data['pagination'] = $this->global_model->pagination(count($data['categories']), $page , SITE_URL . '/move_fwd');
		$data['main_categories'] = $this->get_main_categories();
		$data['parent_categories'] = $this->category_model->get_parent_categories();
		$access = $this->module_model->check_access('move_fwd');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = count($data['categories']);

		return $this->load->view('move_fwd/index', $data, true);		
	}
	
	private function get_main_categories() {
		$where['parent'] = 0;
		$where['is_deleted'] = 0;
		$parent_categories = $this->category_model->get_category($where, null, null,$count, true);
		$parents = array();
		if($parent_categories) {
			foreach($parent_categories as $k => $v) {
				$parents[$v['category_id']] = $v;
			}
		}
		return $parents;
	}
	
	public function add() {
		$data['main_content'] = $this->add_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function add_content() {
		$error = '';
		if($this->input->post('submit')) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules($this->get_rules());
			$valid = $this->form_validation->run();
			if($valid) {
				$post = $this->input->post();
				//$post['original_slots'] = $post['slots'];
				$post['permalink'] = url_title(ucwords($post['move_forward_title']), '-');
				$user = $this->login_model->extract_user_details();
				$post['created_by'] = $user['cms_user_id'];
				$post['move_forward_image'] = '';
				$t_meta['x1'] = $this->input->post('x');
				$t_meta['y1'] = $this->input->post('y');
				$t_meta['x2'] = $this->input->post('x2');
				$t_meta['y2'] = $this->input->post('y2');
				$t_meta['width'] = $this->input->post('width');
				$t_meta['height'] = $this->input->post('height');
				$post['thumbnail_meta'] = serialize($t_meta);


				unset($post['type']);
				unset($post['points']);
				unset($post['task']);
				if($_FILES['photo']['name']) {
					$post['move_forward_image'] = $this->upload_image($error, $config);
					if(!$error) {
						$params = array_merge($config,array('file_path'=>$config['file_path'],'full_path'=>$config['full_path'],'file_name'=>$config['file_name']));	    
						$this->generate_thumb($params);		
					}
				}
					
				if(!$error) {
					$id = $this->global_model->insert($this->_table, $post);	
					
					$this->save_tasks($id);	

					$post = array();
					$post['url'] = SITE_URL . '/move_fwd/add';
					$post['description'] = 'added a new move fwd item';
					$post['table'] = 'tbl_move_forward';
					$post['record_id'] = $id;
					$post['type'] = 'add';
					$this->module_model->save_audit_trail($post);
					redirect('move_fwd');	
				}
			} else {
				$error = validation_errors();
			}
		}
		$data['error'] = $error;
		$data['record'] = $_POST;
		$param['table'] = 'tbl_categories';
		$param['where'] = array('origin_id'	=> MOVE_FWD, 'is_deleted' => 0, 'parent' => 0);
		$data['categories'] = $this->global_model->get_rows($param)->result_array();
		$param['table'] = 'tbl_prizes';
		$param['where'] = array('origin_id'	=> MOVE_FWD, 'is_deleted' => 0);
		$param['order_by'] = array('field'	=> 'prize_name', 'order'	=> 'ASC');
		$data['prizes'] = $this->global_model->get_rows($param)->result_array();
		return $this->load->view('move_fwd/add', $data, true);			
	}

	private function save_tasks($id, $is_update = false) {
		//$prizes = $this->input->post('prize');
		//$points = $this->input->post('points');
		$tasks = $this->input->post('task');
		$types = $this->input->post('type');
		$challenge_id = $this->input->post('challenge_id');

		//$this->global_model->delete('tbl_challenges', array('move_forward_id'	=> $id));
		for($i = 0; $i < 3; $i++) {
			if(isset($types[$i]) && !empty($tasks[$i])) {
			// if(isset($types[$i])) {
			// if(isset($types[$i])) {
				$param['move_forward_id'] = $id;
				$param['challenge'] = $tasks[$i];
				//$param['prize_id'] = $prizes[$i];
				//$param['points'] = $points[$i];
				$param['type'] = $types[$i];
				if($is_update) {
					$this->global_model->update_record('tbl_challenges', 'WHERE challenge_id = ' . $challenge_id[$i], $param);	
				} else {
					$this->global_model->insert('tbl_challenges', $param);
				}
				
			}
		}
	}
	
	public function edit() {
		$id = $this->uri->segment(3);
		$data['main_content'] = $this->edit_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function edit_content($id) {
		$error = '';
		$param['table'] = $this->_table;
		$param['where'] = array('move_forward_id'	=> $id);
		$record = (array)$this->global_model->get_row($param);

		$param['table'] = 'tbl_challenges';
		$param['where'] = array('move_forward_id'	=> $id);
		$param['order_by'] = array('field'	=> 'challenge_id', 'order'	=> 'ASC');
		$challenges = $this->global_model->get_rows($param)->result_array();
		$record['challenges'] = $challenges;
		
		$data['record'] = $_POST ? $_POST : $record;
		$old_content = $new_content = array();
		if($this->input->post('submit')) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules($this->get_rules());
			$valid = $this->form_validation->run();
			if($valid) {
				$post = $this->input->post();
				// $post['original_slots'] = $post['slots'];
				//$post['original_slots'] = 0;
				$post['permalink'] = url_title(ucwords($post['move_forward_title']), '-');
				$user = $this->login_model->extract_user_details();
				$post['created_by'] = $user['cms_user_id'];
				if($_FILES['photo']['name'])
					$post['move_forward_image'] = $this->upload_image($error);
				
				$config['full_path'] = 'uploads/move_fwd/' . $post['move_forward_image'];
				$config['file_name'] =  $post['move_forward_image'];
				$config['file_path'] = 'uploads/move_fwd/';
				$params = array_merge($config,array('file_path'=>$config['file_path'],'full_path'=>$config['full_path'],'file_name'=>$config['file_name']));	    
				$this->generate_thumb($params);
				
				if(!$error) {
					$t_meta['x1'] = $this->input->post('x');
					$t_meta['y1'] = $this->input->post('y');
					$t_meta['x2'] = $this->input->post('x2');
					$t_meta['y2'] = $this->input->post('y2');
					$t_meta['width'] = $this->input->post('width');
					$t_meta['height'] = $this->input->post('height');
					$post['thumbnail_meta'] = serialize($t_meta);
					$post['is_premium_offer'] = $this->input->post('is_premium_offer');
					$this->global_model->update($this->_table, $post, array('move_forward_id'	=> $id));	
					
					$this->save_tasks($id, true);

					$fields = array('move_forward_title', 'move_forward_description', 'move_forward_image', 'category_id', 'status');
					$status = array('Unpublished', 'Published');
					$categories = $this->get_category_details();
					foreach($record as $k => $v) {
						if(in_array($k, $fields)) {
							if($record[$k] != $this->input->post($k)) {
								if($k == 'status') {
									$new_content[$k] = $status[$this->input->post($k)];
									$old_content[$k] = $status[$record[$k]];
								} elseif($k == 'category_id') {
									$new_content[$k] = $categories[$this->input->post($k)]['category_name'];
									$old_content[$k] = $categories[$record[$k]]['category_name'];
								} else {
									if($k == 'move_forward_image' && $this->input->post($k) == '')
										continue;
									$new_content[$k] = $this->input->post($k);
									$old_content[$k] = $record[$k];
								}
							}
						}
					}
					$post = array();
					$post['url'] = SITE_URL . '/move_fwd/edit/' . $id;
					$post['description'] = 'updated a move_fwd item';
					$post['table'] = $this->_table;
					$post['record_id'] = $id;
					$post['type'] = 'edit';
					$post['field_changes'] = serialize(array('old'	=> $old_content,
													  		  'new'	=> $new_content));
					$this->module_model->save_audit_trail($post);				

					redirect('move_fwd');	
				}
				
			} else {
				$error = validation_errors();
				print_r($_POST);
				die();
			}

		}
		$data['error'] = $error;

		
		$param['table'] = 'tbl_categories';
		$param['where'] = array('origin_id'	=> MOVE_FWD, 'is_deleted' => 0, 'parent' => 0);
		$param['order_by'] = null;
		$data['categories'] = $this->global_model->get_rows($param)->result_array();
		$param['table'] = 'tbl_prizes';
		$param['where'] = array('origin_id'	=> MOVE_FWD, 'is_deleted' => 0);
		$param['order_by'] = array('field'	=> 'prize_name', 'order'	=> 'ASC');
		$data['prizes'] = $this->global_model->get_rows($param)->result_array();
		
		return $this->load->view('move_fwd/add', $data, true);					
	}

	private function get_category_details() {
		$param['where'] = array('is_deleted'	=> 0);
		$param['table'] = 'tbl_categories';
		$categories = $this->global_model->get_rows($param)->result_array();
		$category_arr = array();
		if($categories) {
			foreach($categories as $k => $v) {
				$category_arr[$v['category_id']] = $v;
			}
		}
		return $category_arr;
	}
	
	
	public function delete() {
		$table = $this->_table;
		$id = $this->uri->segment(3);
		$field = 'move_forward_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/move_fwd') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$where[$field] = $id;
			$this->global_model->delete_record($table, $where);
		}
		redirect('move_fwd');
	}
	
	private function get_rules() {
		$config = array(
		   array(
				 'field'   => 'move_forward_title',
				 'label'   => 'move fwd title',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'move_forward_description',
				 'label'   => 'move fwd description',
				 'rules'   => 'required'
			  )
		);
		return $config;
	}

	private function upload_image(&$error = false, &$uploaded = false) {
		$this->load->helper(array('upload_helper', 'resize'));
		$config['upload_path'] = 'uploads/move_fwd/';
		$config['allowed_types'] = 'gif|png|jpg';
		$config['do_upload'] = 'photo';
		$config['max_width'] = 2000;
		$config['max_height'] = 1600;
		$config['max_size'] =  8072;
		$config['file_name'] = uniqid() . '.' . end(explode('.', $_FILES['photo']['name']));
		$uploaded = upload($config);
		if(!is_array($uploaded)) {
			$error = $uploaded;
			return;
		} else {

			$params = array('new_filename' => 'main_' .$config['file_name'], 'width'=>319,'height'=>300,'source_image'=>$config['upload_path'].'/'.$uploaded['file_name'],'new_image_path'=>$config['upload_path'].'/','file_name'=>$config['file_name']);
	    	resize($params);
	    }
		return $uploaded['file_name'];
	}

	private function generate_thumb($uploaded) {
		list($width, $height) = getimagesize($uploaded['full_path']); 
		if($width < 300) {
			copy($uploaded['full_path'], $uploaded['file_path'].'thumb_'.$uploaded['file_name']);
			return;
		}

		$this->load->helper('crop');
		$config['x'] = $this->input->post('x');
		$config['y'] = $this->input->post('y');
		$config['x2'] = $this->input->post('x2');
		$config['y2'] = $this->input->post('y2');
		$config['width'] = $this->input->post('width');
		$config['height'] = $this->input->post('height');
		$params = array_merge($config, $uploaded);	    
		crop($params);	

	}

	// public function resize_all() {
		
	// 	$this->load->helper('resize');
	// 	$activities = $this->db->select('*')
	// 						   ->from('tbl_backstage_events_photos')
	// 						   ->get()
	// 						   ->result_array();
	// 						   print_r($activities);
	// 	if($activities) {
	// 		$config['upload_path'] = 'uploads/backstage/photos/';
	// 		$config['allowed_types'] = 'gif|png|jpg';
	// 		foreach ($activities as $key => $value) {
	// 			echo 'new_filename' => '100_100_' .$value['image'] .'<br>';
	// 			$params = array('new_filename' => '100_100_' .$value['image'], 'width'=>100,'height'=>100,'source_image'=>$config['upload_path'].'/'.$value['image'],'new_image_path'=>$config['upload_path'].'/thumbnails/','file_name'=>$value['image']);
	//     		resize($params);
	// 			//echo $config['new_image'] . '<br>';
	// 		}
	// 	}

	// }

	public function test() {
		echo 'test';
		$this->load->helper('resize');
		$activities = $this->db->select('*')
							   ->from('tbl_backstage_events_photos')
							   ->get()
							   ->result_array();
							   
		if($activities) {
			$config['upload_path'] = 'uploads/backstage/photos/';
			$config['allowed_types'] = 'gif|png|jpg';
		 	
		 	foreach ($activities as $key => $value) {
		 		echo 'test';
		 		//echo 'new_filename' => '100_100_' .$value['image'] .'<br>';
				$params = array('new_filename' => '331_176_' .$value['image'], 'width'=>331,'height'=>176,'source_image'=>$config['upload_path'].'/'.$value['image'],'new_image_path'=>$config['upload_path'].'/thumbnails/','file_name'=>$value['image']);
	    		resize($params);
				//echo $config['new_image'] . '<br>';
			}
		}
	}

	

	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		elseif($method == 'test')
			$this->test();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */