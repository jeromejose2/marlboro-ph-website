<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_offer_participants extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		$this->load->model('module_model');
		$this->_table = 'tbl_loggin_offers';
		$this->load->helper('file');
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$this->db->select('*, tbl_login_offers.title as offer_title, tbl_registrants.registrant_id as rid')->from('tbl_confirm_login_offers');

		$this->db->join('tbl_registrants', 'tbl_registrants.registrant_id = tbl_confirm_login_offers.registrant_id');
		$this->db->join('tbl_login_offers', 'tbl_login_offers.id = tbl_confirm_login_offers.offer_id');

		if(isset($_GET['person_id']) and $_GET['person_id'] != ""){
			$this->db->where('person_id LIKE', "%".$_GET['person_id']."%");
		}
		if(isset($_GET['first_name']) and $_GET['first_name'] != ""){
			$this->db->where('first_name LIKE', "%".$_GET['first_name']."%");
		}
		if(isset($_GET['last_name']) and $_GET['last_name'] != ""){
			$this->db->where('third_name LIKE', "%".$_GET['last_name']."%");
		}
		if(isset($_GET['previous_login_date']) and $_GET['previous_login_date'] != ""){
			$this->db->where('DATE(date_confirmed)', $_GET['previous_login_date']);
		}
		if(isset($_GET['last_login_date']) and $_GET['last_login_date'] != ""){
			$this->db->where('DATE(last_date_login)', $_GET['last_login_date']);
		}
		if(isset($_GET['promo']) and $_GET['promo'] != ""){
			$this->db->where('tbl_login_offers.title LIKE', "%".$_GET['promo']."%");
		}
		if(isset($_GET['points_awarded']) and $_GET['points_awarded'] != ""){
			$this->db->where('points', $_GET['points_awarded']);
		}


		$this->db->order_by('tbl_confirm_login_offers.id', 'desc');
		$content = $this->db->get()->result();


		$perks = $this->db->query('SELECT DISTINCT registrant_id from tbl_buys')->result();
		$reg_ids = [];
		foreach($perks as $perk){
			$reg_ids[] = $perk->registrant_id;
		}

		$data['reg_ids'] = $reg_ids;

		$data['content'] = $content;

		$data['total'] = count($content);

		return $this->load->view('login_offer_participants/index', $data, true);		
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		elseif($method == 'create')
			$this->create();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */