<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Comment extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		include_once('application/models/points_model.php');
		include_once('application/models/notification_model.php');
		$this->points_model = new Points_Model();
		$this->notification_model = new Notification_Model();
		$this->_table = 'vw_comments';
		//$this->output->enable_profiler(TRUE);
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		$where = 'is_deleted != 1';
		$param = array();
		if(isset($_GET['search'])) {
			//print_r($_GET);
			//die();
			$where .=  " AND CONCAT(first_name, ' ' , third_name) LIKE '%$_GET[first_name]%'";
			$where .= (isset($_GET['id']) && $_GET['id']) ? " AND comment_id LIKE '%$_GET[id]%'" : "";
			$where .= (isset($_GET['nick_name']) && $_GET['nick_name']) ? " AND nick_name LIKE '%$_GET[nick_name]%'" : "";
			$where .= $_GET['comment'] ? " AND comment LIKE '%$_GET[comment]%'" : "";
			$where .= $_GET['title'] ? " AND ((gtitle LIKE '%$_GET[title]%' AND origin_id = " . MOVE_FWD_GALLERY . ") OR (move_forward_title LIKE '%$_GET[title]%' AND origin_id = " . MOVE_FWD . ") OR (ntitle LIKE '%$_GET[title]%' AND origin_id = " . ABOUT_NEWS . ") OR (btitle LIKE '%$_GET[title]%' AND origin_id = " . BACKSTAGE_PHOTOS . ") OR (vtitle LIKE '%$_GET[title]%' AND origin_id = " . ABOUT_VIDEOS . "))" : "";
			$where .= $_GET['fromavail'] ? " AND DATE(comment_status_changed) >= '$_GET[fromavail]'" : "";
			$where .= $_GET['toavail'] ? " AND DATE(comment_status_changed) <= '$_GET[toavail]'" : "";
			$where .= $_GET['comment_status'] != '' ? " AND comment_status = '$_GET[comment_status]'" : "";
			$where .= $_GET['origin_id'] != '' ? " AND origin_id = '$_GET[origin_id]'" : "";
			
			

			// $like = $this->input->get();
			// if($like['fromavail'] && $like['toavail']) {
			// 	$from = $like['fromavail'] > $like['toavail'] ?  $like['toavail'] :  $like['fromavail'];
			// 	$to = $like['toavail'] < $like['fromavail'] ?  $like['fromavail'] :  $like['toavail'];
			// 	$where['DATE(date_created) <='] = $to;
			// 	$where['DATE(date_created) >='] = $from;
			// }
			// if(isset($like['id'])) {
			// 	$where['comment_id'] = $like['id'];
			// }

			// if($like['first_name']) {
			// 	$like["CONCAT(first_name, ' ' , third_name)"] = $like['first_name'];
			// }

			// if($like['title']) {
			// 	$or_where['gtitle LIKE'] = '%' .$like['title'] .'%';
			// 	$or_where['ntitle LIKE'] = '%' .$like['title'] .'%';
			// 	$or_where['btitle LIKE'] = '%' .$like['title'] .'%';
			// 	$or_where['vtitle LIKE'] = '%' .$like['title'] .'%';
			// }
			// unset($like['title']);
			// unset($like['first_name']);
			// //if($like['origin_id'] && $like['title']) {
			// 	//$param['join'] = $this->get_join($like['origin_id']);
			// //}
				
			// unset($like['search']);
			// unset($like['fromredeem']);
			// unset($like['fromavail']);
			// unset($like['toavail']);
			// unset($like['toredeem']);
			// unset($like['id']);

		}
		
		$param['limit'] = PER_PAGE;
		$query = "SELECT * FROM {$this->_table} WHERE {$where} ORDER BY comment_date_created DESC LIMIT " . $data['offset'] . ", " . PER_PAGE;


		$comments = $this->global_model->custom_query($query)->result_array();

		//die($this->db->last_query());
		$query = "SELECT * FROM {$this->_table} WHERE {$where}";
		$records = $this->global_model->custom_query($query)->num_rows();

		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/comment');
		$data['status'] = array('Pending', 'Approved', 'Rejected');
		$exclude = array(FLASH_OFFERS, BIRTHDAY_OFFERS);
		$data['origins'] = $this->module_model->get_origins($exclude);
	
		$comments_arr = array();
		if($comments) {
			foreach ($comments as $key => $value) {
				$value['item_title'] = $this->get_title($value['origin_id'], $value);
				$value['url'] = $this->get_url($value['origin_id'], $value['suborigin_id']);
				$comments_arr[] = $value;
			}
		}
		$data['categories'] = $comments_arr;
		$access = $this->module_model->check_access('comment');
		$data['total'] = $records;
		$data['edit'] = $access['edit'];
		return $this->load->view('comment/index', $data, true);		
	}

	private function get_title_field($origin_id) {
		$title = array(MOVE_FWD => 'move_forward_title');
		return $title[$origin_id];
	}

	private function get_title($origin_id, $value) {
		$title = '';
		switch($origin_id) {
			case MOVE_FWD:
				$title = $value['move_forward_title'];
				break;
			case ABOUT_NEWS:
				$title = $value['ntitle'];
				break;
			case ABOUT_VIDEOS:
				$title = $value['vtitle'];
				break;
			case MOVE_FWD_GALLERY:
				$title = $value['gtitle'];
				break;
			case BACKSTAGE_PHOTOS:
				$title = $value['btitle'];
				break;

		}
		return $title;

	}

	private function get_url($origin_id, $suborigin_id) {
		$url = BASE_URL;
		switch($origin_id) {
			case MOVE_FWD:
				$param['table'] = 'tbl_move_forward';
				$param['where'] = array('move_forward_id'	=> $suborigin_id);
				$move_forward = $this->global_model->get_row($param);
				$url .= 'move_forward/offer/' . $suborigin_id . '/' . $move_forward->permalink;
				break;
			// case ABOUT_NEWS:
			// 	$param['table'] = 'tbl_move_forward';
			// 	$param['where'] = array('move_forward_id'	=> $suborigin_id);
			// 	$move_forward = $this->global_model->get_row($param);
			// 	$url .= 'move_forwardoffer/' . $suborigin_id . '/' . $move_forward->permalink;
			// 	break;
			// case ABOUT_VIDEOS:
			// 	$param['table'] = 'tbl_move_forward';
			// 	$param['where'] = array('move_forward_id'	=> $suborigin_id);
			// 	$move_forward = $this->global_model->get_row($param);
			// 	$url .= 'move_forwardoffer/' . $suborigin_id . '/' . $move_forward->permalink;
			// 	break;
			// case MOVE_FWD_GALLERY:
			// 	$param['table'] = 'tbl_move_forward';
			// 	$param['where'] = array('move_forward_id'	=> $suborigin_id);
			// 	$move_forward = $this->global_model->get_row($param);
			// 	$url .= 'move_forwardoffer/' . $suborigin_id . '/' . $move_forward->permalink;
			// 	break;
			case BACKSTAGE_PHOTOS:
				$param['table'] = 'tbl_backstage_events_photos';
				$param['join'] = array('tbl_backstage_events' => 'tbl_backstage_events_photos.backstage_event_id = tbl_backstage_events.backstage_event_id');
				$param['where'] = array('tbl_backstage_events_photos.photo_id'	=> $suborigin_id);
				$backstage_pass = $this->global_model->get_row($param);
				$url .= 'backstage_pass/content/' . $backstage_pass->url_title . '/' . $backstage_pass->photo_id;
				break;
			case MOVE_FWD_COMMENTS_PHOTOS:
				$param['table'] = 'vw_comments';
				$param['where'] = array('comment_id'	=> $suborigin_id);
				$move_forward = $this->global_model->get_row($param);
				$url .= 'move_forward/offer/' . $move_forward->suborigin_id . '/' . url_title(ucwords($move_forward->move_forward_title), '-');
				break;
			case MOVE_FWD_COMMENT_REPLIES_PHOTOS:
				$param['table'] = 'vw_comments';
				$param['join'] = array('tbl_comment_replies' => 'tbl_comment_replies.comment_id = vw_comments.comment_id');
				$param['where'] = array('comment_reply_id'	=> $suborigin_id);
				$move_forward = $this->global_model->get_row($param);
				$url .= 'move_forward/offer/' . $move_forward->suborigin_id . '/' . url_title(ucwords($move_forward->move_forward_title), '-');
				break;
			case BACKSTAGE_VIDEOS:
				$param['table'] = 'vw_comments';
				$param['join'] = array('tbl_backstage_videos' => 'tbl_backstage_videos.about_video_id = vw_comments.suborigin_id');
				$param['where'] = array('about_video_id'	=> $suborigin_id);
				$move_forward = $this->global_model->get_row($param);
				$url .= 'events/videos/content/' . url_title(ucwords($move_forward->bvtitle), '-');
				break;
		}
		return $url;

	}

	private function get_join($origin_id) {
		$join = array();
		switch($origin_id) {
			case MOVE_FWD:
				$join = array('tbl_move_forward'	=> 'tbl_move_forward.move_forward_id = ' . $this->_table . '.suborigin_id');
				break;
		}
		return $join;

	}

	public function approve() {
		$id = $this->uri->segment(3);
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/comment') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->update_status($id, 1, 'approved a comment');
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function approve_all() {
		$ids = $this->input->post('ids');
		if($ids) {
			foreach ($ids as $key => $value) {
				$this->update_status($value, 1, 'approved a comment');	
			}
		}	
	}

	public function disapprove() {
		$id = $this->uri->segment(3);
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/comment') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->update_status($id, 2, 'rejected a comment',  $this->input->post('message'));
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function multiple_disapprove(){
		 
		$ids = $this->input->post('ids');
		if($ids) {
			foreach ($ids as $key => $value) {
				$this->update_status($value, 2, 'disapproved a comment',  $this->input->post('message'));	
 			}
		}	
		redirect($_SERVER['HTTP_REFERER']);
	}

	private function update_status($id, $new_status, $action_text = '', $message = '') {
		$where['comment_id'] = $id;
		$where2['comment_id'] = $id;
		$where2['is_deleted'] = 0;
		$param['where'] = $where2;
		$param['table'] = $this->_table;
		$param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id  = ' . $this->_table . '.registrant_id');
		$comment = (array)$this->global_model->get_row($param);
		if(!$comment) {
			$this->session->set_flashdata('error', 'Sorry, comment is already deleted.');
			redirect('comment');
		}

		if($comment['comment_status'] != 0) {
			$err_msg = $comment['comment_status'] == 1 ? "approved" : "rejected";
			$this->session->set_flashdata('error', 'Sorry, comment is already '.$err_msg);
			redirect('comment');
		}

		#update status
		$set['comment_status'] = $new_status;
		$set['comment_status_changed'] = date('Y-m-d H:i:s');
		$this->global_model->update_record('tbl_comments', $where, $set);
		
		#save audit trail
		$status = $this->global_model->get_status();

		$new_content['comment_status'] = $status[$new_status];
		$old_content['comment_status'] = $status[$comment['comment_status']];
		$post['url'] = @$_SERVER['HTTP_REFERER'];
		$post['description'] = $action_text;
		$post['table'] = $this->_table;
		$post['record_id'] = $id;
		$post['message'] = $message;
		$post['field_changes'] = serialize(array('old'	=> $old_content,
												  'new'	=> $new_content));
		$this->module_model->save_audit_trail($post);	
	
		# points
		if($new_status == 1) {
			if($comment['comment_status'] == 0) {
				if($comment['registrant_id'] != $comment['recipient_id']) {
					$points = $this->points_model->earn(COMMENT, array('suborigin'	=> $id,
														'registrant'	=> $comment['registrant_id'],
														'remarks'		=> $this->get_remarks($comment['origin_id'], $comment['suborigin_id'])));
					$this->notification_model->notify($comment['registrant_id'], COMMENT, array(
															'message' 	=> $this->get_notification_copy($comment['origin_id'], $comment['suborigin_id'], 1, $points),
															'suborigin' => $id
															));
					if($comment['recipient_id'] != 0) {
						$points = $this->points_model->earn(COMMENT_RECEIVE, array('suborigin'	=> $id,
																	'registrant'	=> $comment['recipient_id']));	
						$this->notification_model->notify($comment['recipient_id'], COMMENT_RECEIVE, array(
															'message' => $this->get_notification_copy($comment['origin_id'], $comment['suborigin_id'], 1, $points, true),
															'suborigin' => $id
															));
					}
						
				}
			} elseif($comment['comment_status'] == 2) {
				if($comment['registrant_id'] != $comment['recipient_id']) {
					

					$points = $this->points_model->activate($comment['registrant_id'], array(
						array(
							'origin' => COMMENT,
							'suborigin' => $id
						)
					));	

					if(!$points) {
						$points = $this->points_model->earn(COMMENT, array('suborigin'	=> $id,
														'registrant'	=> $comment['registrant_id'],
														'remarks'		=> $this->get_remarks($comment['origin_id'], $comment['suborigin_id'])));	
					}

					$point_str = is_array($points) ? $points[0] : $points;
					
					$this->notification_model->notify($comment['registrant_id'], COMMENT, array(
																'message' => $this->get_notification_copy($comment['origin_id'], $comment['suborigin_id'], 1, $point_str, $message),
																'suborigin' => $id
																));
					
					if($comment['recipient_id'] != 0) {
						$points = $this->points_model->activate($comment['recipient_id'], array(
															array(
																'origin' => COMMENT_RECEIVE,
																'suborigin' => $id
															)
														));	
						if(!$points) {
							$points = $this->points_model->earn(COMMENT_RECEIVE, array('suborigin'	=> $id,
															'registrant'	=> $comment['registrant_id'],
															'remarks'		=> $this->get_remarks($comment['origin_id'], $comment['suborigin_id'])));	
						}

						$point_str = is_array($points) ? $points[0] : $points;
						$this->notification_model->notify($comment['recipient_id'], COMMENT_RECEIVE, array(
															'message' => $this->get_notification_copy($comment['origin_id'], $comment['suborigin_id'], 1, $point_str, true, $message),
															'suborigin' => $id
															));
					}
						
				}
			}
		} else {
			if($comment['registrant_id'] != $comment['recipient_id']) {
				if($comment['comment_status'] == 1) {
					$points = $this->points_model->deactivate($comment['registrant_id'], array(
						array(
							'origin' => COMMENT,
							'suborigin' => $id
						)
					));	

					
					if($comment['recipient_id'] != 0) {
						$this->points_model->deactivate($comment['recipient_id'], array(
							array(
								'origin' => COMMENT_RECEIVE,
								'suborigin' => $id
							)
						));		
					}
						
				}
				// /die($this->db->last_query());
				//die($this->db->last_query());
				$this->notification_model->notify($comment['registrant_id'], COMMENT, array(
															'message' => $this->get_notification_copy($comment['origin_id'], $comment['suborigin_id'], 0, 0, $message),
															'suborigin' => $id
															));
				
			}	
		}
	}

	private function get_remarks($origin_id, $suborigin_id) {
		if($origin_id == MOVE_FWD) {
			$param['table'] = 'tbl_move_forward';
			$param['where'] = array('move_forward_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			return '{{ name }} commented on ' . $record['move_forward_title'] . ' on MoveFWD';
		// } elseif($origin_id == ABOUT_PHOTOS) {
		// 	$param['table'] = 'tbl_about_events_photos';
		// 	$param['join'] = array('tbl_about_events' => 'tbl_about_events_photos.about_event_id = tbl_about_events.about_event_id');
		// 	$param['where'] = array('photo_id' => $suborigin_id);
		// 	$record = (array)$this->global_model->get_row($param);
		// 	return '{{ name }} commented on ' . $record['title'] . ' on ' . $record['type'];
		// 
		} elseif($origin_id == ABOUT_VIDEOS) {
			$param['table'] = 'tbl_about_videos';
			$param['where'] = array('about_video_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			return '{{ name }} commented on ' . $record['title'] . ' on About';
		} elseif($origin_id == ABOUT_NEWS) {
			$param['table'] = 'tbl_about_news';
			$param['where'] = array('about_news_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			return '{{ name }} commented on ' . $record['title'] . ' on About';
		} elseif($origin_id == BACKSTAGE_PHOTOS) {
			$param['table'] = 'tbl_backstage_events_photos';
			$param['join'] = array('tbl_backstage_events' => 'tbl_backstage_events_photos.backstage_event_id = tbl_backstage_events.backstage_event_id');
			$param['where'] = array('photo_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			return '{{ name }} commented on ' . $record['title'] . ' on Backstage Pass';
		}
	}

	private function get_notification_copy($origin_id, $suborigin_id, $status = 1, $points, $message = '') {
		$prepend_copy = 'Your comment on ';
		$disapproved_append_copy = " Please review the Terms and Conditions for more information: <a href=\"javascript:void(0)\" onclick=\"popup.open({url:'".BASE_URL."popups/terms.html'})\">Terms and Conditions</a>.";
		if($origin_id == MOVE_FWD) {
			$param['table'] = 'tbl_move_forward';
			$param['where'] = array('move_forward_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			if($status == 1) {
				if($points > 0) {
					return $prepend_copy . $record['move_forward_title'] . ' (MoveFWD) has been approved. You gained ' . $points . ' points. Thank you!';
				} else {
					return $prepend_copy . $record['move_forward_title'] . ' (MoveFWD) has been approved. Thank you!';
				}
			} else {
				if($message) {
					return $prepend_copy . $record['move_forward_title'] . ' (MoveFWD) has been disapproved because ' .  $message . '.' . $disapproved_append_copy;
				} else {
					return $prepend_copy . $record['move_forward_title'] . ' (MoveFWD) has been disapproved.' . $disapproved_append_copy;
				}
			}

		} elseif($origin_id == MOVE_FWD_GALLERY || $origin_id == MOVE_FWD_COMMENTS_PHOTOS) {
			$param['table'] = 'tbl_move_forward_gallery';
			$param['where'] = array('move_forward_gallery_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			if($status == 1) {
				if($points > 0) {
					return $prepend_copy . ' a MoveFWD entry has been approved. You gained ' . $points . ' points. Thank you!';
				} else {
					return $prepend_copy . ' a MoveFWD entry has been approved. Thank you!';
				}
			} else {
				if($message) {
					return $prepend_copy . ' a MoveFWD entry has been disapproved because ' . $message . '.' . $disapproved_append_copy;
				} else {
					return $prepend_copy . ' a MoveFWD entry has been disapproved.' . $disapproved_append_copy;
				}
			}

		// } elseif($origin_id == ABOUT_PHOTOS) {
		// 	$param['table'] = 'tbl_about_events_photos';
		// 	$param['join'] = array('tbl_about_events' => 'tbl_about_events_photos.about_event_id = tbl_about_events.about_event_id');
		// 	$param['where'] = array('photo_id' => $suborigin_id);
		// 	$record = (array)$this->global_model->get_row($param);
		// 	if($status == 1) 
		// 		return $prepend_copy . $record['title'] . ' on ' . $record['type'] . ' has been approved. You gained ' . $points . ' points. Thank you!';
		// 	else 
		// 		return $prepend_copy . $record['title'] . ' on ' . $record['type'] . ' has been disapproved. Please try again!';
		// 
		} elseif($origin_id == ABOUT_VIDEOS) {
			$param['table'] = 'tbl_about_videos';
			$param['where'] = array('about_video_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			if($status == 1) {
				if($points > 0) {
					return $prepend_copy . $record['title'] . ' has been approved. You gained ' . $points . ' points. Thank you!';
				} else {
					return $prepend_copy . $record['title'] . ' has been approved. Thank you!';
				}
			} else {
				if($message) {
					return $prepend_copy . $record['title'] . ' has been disapproved because ' . $message . '.' . $disapproved_append_copy;
				} else {
					return $prepend_copy . $record['title'] . ' has been disapproved.' . $disapproved_append_copy;
				}
			}
		} elseif($origin_id == ABOUT_NEWS) {
			$param['table'] = 'tbl_about_news';
			$param['where'] = array('about_news_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			if($status == 1) {
				if($points > 0) {
					return $prepend_copy . $record['title'] . ' has been approved. You gained ' . $points . ' points. Thank you!';
				} else {
					return $prepend_copy . $record['title'] . ' has been approved. Thank you!';
				}
			} else {
				if($message) {
					return $prepend_copy . $record['title'] . ' has been disapproved because ' . $message . '.' . $disapproved_append_copy;
				} else {
					return $prepend_copy . $record['title'] . ' has been disapproved.' . $disapproved_append_copy;
				}
			}
		} elseif($origin_id == BACKSTAGE_PHOTOS) {
			$param['table'] = 'tbl_backstage_events_photos';
			$param['join'] = array('tbl_backstage_events' => 'tbl_backstage_events_photos.backstage_event_id = tbl_backstage_events.backstage_event_id');
			$param['where'] = array('photo_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			if($status == 1) {
				if($points > 0) {
					return $prepend_copy . $record['title'] . ' (Backstage Pass) has been approved. You gained ' . $points . ' points. Thank you!';
				} else {
					return $prepend_copy . $record['title'] . ' (Backstage Pass) has been approved. Thank you!';
				}
			} else {
				if($message) {
					return $prepend_copy . $record['title'] . ' (Backstage Pass) has been disapproved because ' . $message . '.' . $disapproved_append_copy;
				} else {
					return $prepend_copy . $record['title'] . ' (Backstage Pass) has been disapproved.' . $disapproved_append_copy;
				}
			}
		} elseif($origin_id == BACKSTAGE_VIDEOS) {
			$param['table'] = 'tbl_backstage_videos';
			$param['where'] = array('about_video_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			if($status == 1) {
				if($points > 0) {
					return $prepend_copy . $record['title'] . ' (Backstage Pass) has been approved. You gained ' . $points . ' points. Thank you!';
				} else {
					return $prepend_copy . $record['title'] . ' (Backstage Pass) has been approved. Thank you!';
				}
			} else {
				if($message) {
					return $prepend_copy . $record['title'] . ' (Backstage Pass) has been disapproved because ' . $message . '.' . $disapproved_append_copy;
				} else {
					return $prepend_copy . $record['title'] . ' (Backstage Pass) has been disapproved.' . $disapproved_append_copy;
				}
			}
		}
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		elseif($method == 'approve')
			$this->approve();
		elseif($method == 'approve_all')
			$this->approve_all();
		elseif($method == 'disapprove')
			$this->disapprove();
		elseif($method == 'multiple_disapprove')
			$this->multiple_disapprove();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */