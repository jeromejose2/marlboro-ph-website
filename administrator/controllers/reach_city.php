<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reach_city extends CI_Controller {
	
	private $_has_null;
	public function __construct() {
		parent::__construct();
		$this->_has_null = false;
	}
	
	public function index()
	{
		if(!$this->input->get('fromdate') || !$this->input->get('todate')) {
			redirect('reach_city?fromdate=' . date('Y-m-d', strtotime('-30 days')) .  '&todate=' . date('Y-m-d'));
		}
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	public function export()
	{
		$this->load->library('to_excel_array');
		$records = $this->get_demographics($from, $to, true);
		$row[] = $from && $to ? array('City Reach Demographics (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')') : array('City Reach Demographics');

		$row[] = array('City', 'Count');
		if($records) {
			foreach($records as $k => $v) {
				$row[] = array($v['city'] == '' ? 'Null Values' : $v['city'],
							  $v['count']);
			}
		}
		$this->to_excel_array->to_excel($row, 'reach_'.date("YmdHis"));
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$all = $this->input->get('all') == '1' ? true : false;
		$data['records'] = $this->get_demographics($from, $to, $all);
		$total = 0;
		if($data['records']) {
			foreach($data['records'] as $k => $v) {
				$total += $v['count'];
			}
		}
		$data['total'] = $total;
		$data['provinces'] = $this->global_model->get_provinces();
		$data['has_null'] = $this->_has_null;
		return $this->load->view('reach/city', $data, true);		
	}

	private function get_demographics(&$from = false, &$to = false, $all = false) {
		$where = '';
		if(($this->input->get('fromdate') && $this->input->get('todate')) || $this->input->get('province'))  {
			$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
			$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
			if($from && $to) {
				$where['DATE(date_created) <='] = $to;
				$where['DATE(date_created) >='] = $from;	
			}
			if($this->input->get('province'))
				$where['province_id'] = $this->input->get('province');
		}
		$field = $this->input->get('field') ? $this->input->get('field') : 'count';
		$sort = $this->input->get('sort') ? $this->input->get('sort') : 'DESC';
		$field = $field == 'percentage' ? 'count' : $field;
		$param['where'] = $where;
		$param['fields'] = 'COUNT(tbl_registrants.city) AS count, tbl_cities.city';
		$param['table'] = 'tbl_registrants';
		$param['order_by'] = array('field'	=> $field, 'order'	=> $sort);
		$param['group_by'] = 'tbl_registrants.city';
		$param['join'] = array('tbl_cities' => 'tbl_cities.city_id = tbl_registrants.city');
		if(!$all) {
			$param['limit'] = 10;
			$param['offset'] = 0;	
		}
		$records = $this->global_model->get_rows($param)->result_array();
		$records_arr = array();
		if($records) {
			foreach ($records as $key => $value) {
				if($value['city'] == '') {
					$value['city'] = 'Null Values';
					$this->_has_null = true;
				}
				$records_arr[] = $value;
			}
		}
		return $records_arr;
	}

	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */