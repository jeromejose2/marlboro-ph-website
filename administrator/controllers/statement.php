<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statement extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('statement_model');
		$this->load->model('user_model');
		
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$where = '';
		if(isset($_GET['search'])) {
			$where =  " AND statement LIKE '%$_GET[statement]%'";
			$where .= $_GET['from_publish'] ? " AND DATE(date_published) >= '$_GET[from_publish]'" : "";
			$where .= $_GET['to_publish'] ? " AND DATE(date_published) <= '$_GET[to_publish]'" : "";
			$where .= $_GET['from_unpublish'] ? " AND DATE(date_unpublished) >= '$_GET[from_publish]'" : "";
			$where .= $_GET['to_unpublish'] ? " AND DATE(date_unpublished) <= '$_GET[to_unpublish]'" : "";
			$where .= $_GET['status'] != '' ? " AND status = '$_GET[status]'" : "";
		}
		$page = $this->uri->segment(2, 1);
		$data['users'] = $this->statement_model->get_statement($where, $page, PER_PAGE, $records);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/statement');
		$data['offset'] = ($page - 1) * PER_PAGE;
		$access = $this->module_model->check_access('statement');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = $records;
		return $this->load->view('statement/index', $data, true);		
	}
	
	public function add() {
		$data['main_content'] = $this->add_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function add_content() {
		$error = '';
		if($this->input->post('submit')) {
			$id = $this->statement_model->save_statement();	
				
			$post['url'] = SITE_URL . '/statement/add';
			$post['description'] = 'added a new statement';
			$post['table'] = 'tbl_statements';
			$post['record_id'] = $id;
			$post['type'] = 'add';
			$this->module_model->save_audit_trail($post);
			redirect('statement');
		}
		$data['error'] = $error;
		$data['statement'] = $_POST;
		return $this->load->view('statement/add', $data, true);			
	}
	
	public function edit() {
		$id = $this->uri->segment(3);
		$data['main_content'] = $this->edit_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function edit_content($id) {
		$error = '';
		$statement = $this->statement_model->get_statement(array('statement_id'	=> $id))[0];
		if(!$statement)
			redirect('statement');
		$new_content = array();
		$old_content = array();
		if($this->input->post('submit')) {
			$this->statement_model->save_statement(true);
			
			$fields = array('statement', 'status');
			$status = array('', 'Published', 'Unpublished');
			foreach($statement as $k => $v) {
				if(in_array($k, $fields)) {
					if($statement[$k] != $this->input->post($k)) {
						if($k == 'status') {
							$new_content[$k] = $status[$this->input->post($k)];
							$old_content[$k] = $status[$statement[$k]];
						} else {
							$new_content[$k] = $this->input->post($k);
							$old_content[$k] = $statement[$k];
						}
					}
				}
			}
			
			$post['url'] = SITE_URL . '/statement/edit/' . $id;
			$post['description'] = 'updated a statement';
			$post['table'] = 'tbl_statements';
			$post['record_id'] = $id;
			$post['type'] = 'edit';
			$post['field_changes'] = serialize(array('old'	=> $old_content,
											  		  'new'	=> $new_content));
			$this->module_model->save_audit_trail($post);				
			redirect('statement');
		}
		$data['error'] = $error;
		$data['statement'] = $_POST ? $_POST : $statement;
		return $this->load->view('statement/add', $data, true);			
	}
	
	
	private function validate_fields() {
		$email = $this->input->post('username');
		$id = $this->input->post('id');
		$where['username'] = $email;
		$user = $this->user_model->get_user($where);
		if($user) {
			if($id && $user[0]['cms_user_id'] == $id)
				return true;
			return false;
		}
		
		return true;
	}
	
	public function delete() {
		$table = 'tbl_statements';
		$id = $this->uri->segment(3);
		$field = 'statement_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/statement') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$where[$field] = $id;
			$this->global_model->delete_record($table, $where);
			
			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'deleted a statement';
			$post['table'] = 'tbl_statements';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);
			
		}
		redirect('statement');
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete();
		elseif($method == 'add')
			$this->add();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */