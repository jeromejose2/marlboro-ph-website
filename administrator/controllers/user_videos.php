<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class User_videos extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
 	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
 	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
		$this->load->helper('file');
		$this->load->helper('paging');
		$offset = (int)$this->input->get('per_page');
	    $limit = PER_PAGE;
	    $filters = $this->get_filters();

		$access = $this->module_model->check_access('user_videos');
		$total_rows = $this->explore_model->get_total_rows(array('table'=>'tbl_about_videos',
																 'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
																 'like'=>$filters['like_filters']
																 )
														    );
		$data['header_text'] = 'About User Videos';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total_rows'] = $total_rows;

		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_about_videos',
															 'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
														     'like'=>$filters['like_filters'],
														     'fields'=>'about_video_id,title,video,user_type,date_added,status,uploader_id,uploader_name',
														     'limit'=>$limit,
														     'offset'=>$offset,
														     'order_by'=>array('field'=>'date_added','order'=>'DESC')
 												     		)
														);
 		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']);
 		return $this->load->view('about/videos/moderator-list',$data,TRUE);

	}


	public function approve()
	{
		$about_video_id = $this->input->get('about_video_id');
 		$token = $this->input->get('token');

		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($about_video_id . ' ' .	$this->config->item('encryption_key'))) {

			$entry = $this->explore_model->get_row(array('table'=>'tbl_about_videos','where'=>array('about_video_id'=>$about_video_id, 'user_type' => 'registrant','status !='=>1)));
			
			if ($entry) {

				require_once 'application/models/points_model.php';
				$points_model = new Points_Model();
				if (!$entry->status) {
					$points_model->earn(ABOUT_VIDEOS, array(
						'suborigin' => $entry->about_video_id,
						'registrant' => $entry->uploader_id,
						'remarks' => '{{ name }} uploaded a video on About section'
					));
				} elseif ($entry->status == 2) {
					$activated = $points_model->activate($entry->uploader_id, array(
						array(
							'suborigin' => $entry->about_video_id,
							'origin' => ABOUT_VIDEOS
						)
					));
					if (!$activated) {
						$points_model->earn(ABOUT_VIDEOS, array(
							'suborigin' => $entry->about_video_id,
							'registrant' => $entry->uploader_id,
							'remarks' => '{{ name }} uploaded a video on About section'
						));
					}
				}

				require 'application/models/notification_model.php';
				$notification = new Notification_Model();
				$param = array('message'=>'Your video uploaded on About has been approved. You gained '.ABOUT_VIDEOS_APPROVE_BASE_POINT.' points. Thank you!','suborigin'=>$entry->about_video_id);
				$notification->notify($entry->uploader_id,ABOUT_VIDEOS,$param);


				$this->explore_model->update('tbl_about_videos',array('status'=>1),array('about_video_id'=>$about_video_id));
	 			$post['url'] = @$_SERVER['HTTP_REFERER'];
				$post['description'] = 'approved a video on About';
				$post['table'] = 'tbl_about_videos';
				$post['record_id'] = $about_video_id;
				$this->module_model->save_audit_trail($post);
 
			}

 			
  		}

		redirect($this->uri->segment(1).'?per_page='.$this->input->get('per_page'));
	}

	public function disapprove()
	{
		$about_video_id = $this->input->get('about_video_id');
 		$token = $this->input->get('token');

		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($about_video_id . ' ' .	$this->config->item('encryption_key'))) {
 			
 			$entry = $this->explore_model->get_row(array('table'=>'tbl_about_videos','where'=>array('about_video_id'=>$about_video_id, 'user_type' => 'registrant','status !='=>2)));
			
			if ($entry) {

				require_once 'application/models/points_model.php';
				$points_model = new Points_Model();
				if ($entry->status == 1) {
					$points_model->deactivate($entry->uploader_id, array(
						array(
							'suborigin' => $entry->about_video_id,
							'origin' => ABOUT_VIDEOS
						)
					));
				}

				require 'application/models/notification_model.php';
				$notification = new Notification_Model();
				$param = array('message'=>'Your video uploaded on About has been disapproved. Please try again!','suborigin'=>$entry->about_video_id);
				$notification->notify($entry->uploader_id,ABOUT_VIDEOS,$param);

				$this->explore_model->update('tbl_about_videos',array('status'=>2),array('about_video_id'=>$about_video_id));
				$post['url'] = @$_SERVER['HTTP_REFERER'];
				$post['description'] = 'disapproved a video on About';
				$post['table'] = 'tbl_about_videos';
				$post['record_id'] = $about_video_id;
				$this->module_model->save_audit_trail($post);
 
			}

			

		}

	    redirect($this->uri->segment(1).'?per_page='.$this->input->get('per_page'));
	}



	public function add()
	{
		
		$this->load->helper('file');

		$error = '';
		$row = '';

		if($this->input->post()) {

			$post_data = $this->input->post();		

			if($this->validate_form()) {
 
				
 				$data = $post_data;
 				$user = $this->login_model->extract_user_details();
				$data['date_added'] = TRUE; // Set NOW() value to date_added column field
				$data['type'] = 'video';
				$data['submitted_by'] = $user['cms_user_id'];
				$data['user_type'] = 'administrator';


 
				$id = $this->explore_model->insert('tbl_about_videos',$data); 
				$video_filename =  $this->do_upload($id,'video');

				if($video_filename)
	   		    	$this->explore_model->update('tbl_about_videos',array('video'=>$video_filename),array('about_video_id'=>$id));	   		    

	   		    $post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/add';
				$post['description'] = 'added a video';
				$post['table'] = 'tbl_about_videos';
				$post['record_id'] = $id;
				$post['type'] = 'add';
				$this->module_model->save_audit_trail($post);
	   		    redirect($this->uri->segment(1));

			} else {

				$post_data['video'] = '';
				$error = validation_errors();
				$row = json_decode(json_encode($post_data),false);

			}
			 
		} 

		$data['error'] = $error;
		$data['row'] = $row;
 		$data['header_text'] = 'Video';
 		$data['main_content'] = $this->load->view('about/'.$this->uri->segment(1).'/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

	}


	public function edit()
	{

		$this->load->helper('file');

		$error = '';
		$id = $this->uri->segment(3);
		$row = $this->explore_model->get_row(array('table'=>'tbl_about_videos','where'=>array('about_video_id'=>$id,'is_deleted'=>0)));


		if($this->input->post())
		{
			$post_data = $this->input->post();

			if($this->validate_form()){


				$trend = json_decode(json_encode($row),TRUE);


				$fields = array('title', 'description','status');
				$status = array('', 'Published', 'Unpublished');

				$new_content = array();
				$old_content = array();

				foreach($trend as $k => $v) {
					if(in_array($k, $fields)) {
						if($trend[$k] != $this->input->post($k)) {

							if($k == 'status') {
								
								$new_content[$k] = $status[$this->input->post($k)];
								$old_content[$k] = $status[$trend[$k]];

							}else {
								$new_content[$k] = $this->input->post($k);
								$old_content[$k] = $trend[$k];
							}

						}
					}
				}

				$post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/edit/' . $id;
				$post['description'] = 'updated a video';
				$post['table'] = 'tbl_about_videos';
				$post['record_id'] = $id;
				$post['type'] = 'edit';
				$post['field_changes'] = serialize(array('old'	=> $old_content,
														 'new'	=> $new_content));
				$this->module_model->save_audit_trail($post);


				$video_filename =  $this->do_upload($id,'video');

				if($video_filename){
					$post_data['file'] = $video_filename;
				}

 				$post_data['date_updated']	= TRUE; // Set NOW() value to column date_updated  
				$this->explore_model->update('tbl_about_videos',$post_data,array('about_video_id'=>$id,'is_deleted'=>0));
				$this->do_upload($id,'video');
				redirect($this->uri->segment(1));

			}else{

				$error = validation_errors();
 				$row = json_decode(json_encode($post_data),false);

			}

		}

		$video_url = ($this->input->post('video_url')) ?  $this->input->post('video_url') : $row->video;
		$video_embed = ($this->validate_YoutubeURL($video_url)) ? $this->parse_youtube_url($video_url,'embed') : false;

		$data['video_url'] = $video_url;
		$data['video_embed'] = $video_embed;
 

		$data['row'] = $row;
 		$data['error'] = $error;
		$data['header_text'] = 'Video';
 		$data['main_content'] = $this->load->view('about/'.$this->uri->segment(1).'/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

 
	}


	public function delete()
	{

 		$id = $this->uri->segment(3);
 		$token = $this->uri->segment(4);
 		$path = 'uploads/about/'.$this->uri->segment(1).'/';

		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$row = $this->explore_model->get_rows(array('table'=>'tbl_about_videos','where'=>array('about_video_id'=>$id)));			 
 
 			$this->explore_model->update('tbl_about_videos',array('is_deleted'=>1),array('about_video_id'=>$id));

 			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'deleted a videos';
			$post['table'] = 'tbl_about_videos';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);

		}

		redirect($this->uri->segment(1));

	}

	public function validate_form()
	{

		//$this->load->library('form_validation');

		$rules = array(
		   array(
				 'field'   => 'title',
				 'label'   => 'Title',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'description',
				 'label'   => 'Description',
				 'rules'   => 'required'
			  )
		);

		//$this->form_validation->set_rules($rules);
		
		//return $this->form_validation->run();
		return true;
	}




	public function do_upload($new_filename,$upload_file)
	{
		$this->load->helper(array('upload'));
		$upload_path = 'uploads/about/'.$this->uri->segment(1);

		if(!is_dir($upload_path)){
			mkdir($upload_path,0777,true);
		}

		$params = array('upload_path'=>$upload_path,'allowed_types'=>'avi|mov|wmv|mp4','file_name'=>$new_filename,'max_size'=>'0','max_width'=>0,'max_height'=>0,'do_upload'=>$upload_file);
	    $file = upload($params);
	    return (is_array($file) && count($file) > 0) ? $file['file_name'] : '';
	}

	public function get_filters()
	   {
		   
		   $where_filters = array('status'=>'status','DATE(date_added) >='=>'from_date_added','DATE(date_added) <='=>'to_date_added');
		   $like_filters = array('title'=>'title','uploader_name'=>'uploader_name');
		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?default=0'.$query_strings);		   
		   
	   } 

	   public function export()
	   {

		   	$filters = $this->get_filters();
		   	$params = 
		   	$data['query'] = $this->explore_model->get_rows(array('fields'=>"title,description, date_added",
		   														'table'=>'tbl_about_videos',
											   					'where'=>$filters['where_filters'],
											   					'like'=>$filters['like_filters']
											   					)
		   													);
		   	$data['filename'] = $this->uri->segment(1).'_'.date('M-d-Y');
		    $this->load->view('about/export-to-excell',$data);
	   }


	   public function parse_youtube_url($url,$return='',$width='',$height='',$rel=0){

			$urls = parse_url($url);

		    

			//url is http://youtu.be/xxxx

			if(@$urls['host'] == 'youtu.be'){

				$id = ltrim(@$urls['path'],'/');

			}

			//url is http://www.youtube.com/embed/xxxx

			else if(strpos(@$urls['path'],'embed') == 1){

				$id = end(explode('/',@$urls['path']));

			}

			 //url is xxxx only

			else if(strpos($url,'/')===false){

				$id = $url;

			}

			//http://www.youtube.com/watch?feature=player_embedded&v=m-t4pcO99gI

			//url is http://www.youtube.com/watch?v=xxxx

			else{

				parse_str(@$urls['query']);

				$id = @$v;

				

				/*if(!empty($feature)){

					$id = end(explode('v=',$urls['query']));

				}

				*/

			}

			//return embed iframe

			if($return == 'embed'){

				return '<iframe width="'.($width?$width:560).'" height="'.($height?$height:349).'" src="http://www.youtube.com/embed/'.$id.'?rel='.$rel.'" frameborder="0" allowfullscreen></iframe>';

			}
			
			if($return == 'embed_url'){

				return 'http://www.youtube.com/embed/'.$id.'?rel='.$rel.'"';

			}

			//return normal thumb

			else if($return == 'thumb'){

				return 'http://i1.ytimg.com/vi/'.$id.'/default.jpg';

			}

			//return hqthumb

			else if($return == 'hqthumb'){

				return 'http://i1.ytimg.com/vi/'.$id.'/hqdefault.jpg';

			}

			// else return id

			else{

				return $id;

			}

	}


	function validate_YoutubeURL($url) {

	    // Let's check the host first
	    $parse = parse_url($url);
	    $host = @$parse['host'];
	    if (!in_array($host, array('youtube.com', 'www.youtube.com'))) {
	        return false;
	    }

	    $ch = curl_init();
	    $oembedURL = 'www.youtube.com/oembed?url=' . urlencode($url).'&format=json';
	    curl_setopt($ch, CURLOPT_URL, $oembedURL);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	    // Silent CURL execution
	    $output = curl_exec($ch);
	    unset($output);

	    $info = curl_getinfo($ch);
	    curl_close($ch);

	    if ($info['http_code'] !== 404)
	        return true;
	    else 
	        return false;
	}

 
}