<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Community_manager extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		$this->_table = 'tbl_registrants';
		require_once('application/helpers/site_helper.php');
 	}
	
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}

	public function export()
	{
		$this->load->library('to_excel_array');
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			if($like['name']) {
				$like['CONCAT(first_name, \' \', third_name)'] = $like['name'];
				
			}	

			if($like['from'] && $like['to']) {
				$from = $like['from'] > $like['to'] ?  $like['to'] :  $like['from'];
				$to = $like['to'] < $like['from'] ?  $like['from'] :  $like['to'];
				$where['DATE(date_created) <='] = $to;
				$where['DATE(date_created) >='] = $from;

			}
			unset($like['from']);
			unset($like['to']);
			unset($like['name']);
			unset($like['search']);
			$param['like'] = $like;
		}
		$param['table'] = $this->_table;
		$param['where'] = array('is_cm'	=> 1, 'status !='	=> DEACTIVATED);
		$param['order_by'] = array('field'=>'date_created','order'=>'DESC');
		
		$records = $this->global_model->get_rows($param)->result_array();

		$row[] = array('Name', 'Nickname', 'Email', 'Date Created');
		if($records) {
			foreach($records as $k => $v) {
				$row[] = array($v['first_name'] . ' ' . $v['third_name'],
							   $v['nick_name'],
							   $v['email_address'],
							   $v['date_created']);
			}
		}
		$this->to_excel_array->to_excel($row, 'community_manager_'.date("YmdHis"));
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			if($like['name']) {
				$like['CONCAT(first_name, \' \', third_name)'] = $like['name'];
				
			}	

			if($like['from'] && $like['to']) {
				$from = $like['from'] > $like['to'] ?  $like['to'] :  $like['from'];
				$to = $like['to'] < $like['from'] ?  $like['from'] :  $like['to'];
				$where['DATE(date_created) <='] = $to;
				$where['DATE(date_created) >='] = $from;

			}
			unset($like['from']);
			unset($like['to']);
			unset($like['name']);
			unset($like['search']);
		}
		$param['offset'] = $data['offset'];
		$param['limit'] = PER_PAGE;
		$param['table'] = $this->_table;
		$param['where'] = array('is_cm'	=> 1, 'status !='	=> DEACTIVATED);
		$param['order_by'] = array('field'=>'date_created','order'=>'DESC');
		$param['like'] = $like;
		$data['users'] = $this->global_model->get_rows($param)->result_array();
		$records = $this->global_model->get_total_rows($param);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/community_manager');
		$access = $this->module_model->check_access('community_manager');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = $records;
		return $this->load->view('registrant/cm/index', $data, true);		
	}
	
	private function get_main_categories() {
		$where['parent'] = 0;
		$where['is_deleted'] = 0;
		$parent_categories = $this->category_model->get_category($where, null, null,$count, true);
		$parents = array();
		if($parent_categories) {
			foreach($parent_categories as $k => $v) {
				$parents[$v['category_id']] = $v;
			}
		}
		return $parents;
	}
	
	public function add() {
		$data['main_content'] = $this->add_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function add_content() {
		$error = '';
		if($this->input->post('submit')) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules($this->get_rules());
			$valid = $this->form_validation->run();
			if($valid) {
				$post = $this->input->post();
				if(!$this->validate_fields())
					$error = 'Sorry, email address is already in use';
				if(!$error) {
					$post['is_cm'] = 1;	
					$post['date_created'] = date('Y-m-d H:i:s');
					$post['status'] = VERIFIED;
					$post['salt'] = generate_password(20);
					$post['password'] = $this->encrypt->encode($post['password'], $post['salt']);
					$id = $this->global_model->insert($this->_table, $post);	
					
					$post = array();
					$post['url'] = SITE_URL . '/community_manager/add';
					$post['description'] = 'added a new community manager';
					$post['table'] = 'tbl_registrants';
					$post['record_id'] = $id;
					$post['type'] = 'add';
					$this->module_model->save_audit_trail($post);
					redirect('community_manager');	
				}
			} else {
				$error = validation_errors();
			}
		}
		$data['error'] = $error;
		$data['record'] = $_POST;
		return $this->load->view('registrant/cm/add', $data, true);			
	}

	public function edit() {
		$id = $this->uri->segment(3);
		$data['main_content'] = $this->edit_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function edit_content($id) {
		$error = '';
		$param['table'] = $this->_table;
		$param['where'] = array('registrant_id'	=> $id);
		$record = (array)$this->global_model->get_row($param);
		if($record['is_cm'] != 1)
			redirect('community_manager');

		$data['record'] = $_POST ? $_POST : $record;
		$old_content = $new_content = array();
		if($this->input->post('submit')) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules($this->get_rules());
			$valid = $this->form_validation->run();
			if($valid) {
				$post = $this->input->post();
				
				if(!$this->validate_fields())
					$error = 'Sorry, email address is already in use';
				
				if(!$error) {
					if($this->input->post('password')) {
						$post['salt'] = generate_password(20);
						$post['password'] = $this->encrypt->encode($post['password'], $post['salt']);	
					} else {
						unset($post['password']);
					}
					
					
					$this->global_model->update($this->_table, $post, array('registrant_id'	=> $id));	
					
					$fields = array('first_name', 'third_name', 'email_address');
					foreach($record as $k => $v) {
						if(in_array($k, $fields)) {
							if($record[$k] != $this->input->post($k)) {
								$new_content[$k] = $this->input->post($k);
								$old_content[$k] = $record[$k];
							}
						}
					}
					$post = array();
					$post['url'] = SITE_URL . '/community_manager/edit/' . $id;
					$post['description'] = 'updated a community manager';
					$post['table'] = $this->_table;
					$post['record_id'] = $id;
					$post['type'] = 'edit';
					$post['field_changes'] = serialize(array('old'	=> $old_content,
													  		  'new'	=> $new_content));
					$this->module_model->save_audit_trail($post);				

					redirect('community_manager');	
				}
				
			} else {
				$error = validation_errors();
			}

		}
		$data['error'] = $error;
		$data['is_edit'] = 1;
		return $this->load->view('registrant/cm/add', $data, true);					
	}

	public function delete() {
		$id = $this->uri->segment(3);
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/community_manager') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			//$where[$field] = $id;
			$this->global_model->update($this->_table, array('status'	=> DEACTIVATED), array('registrant_id'	=> $id));
			//$this->global_model->delete_record($table, $where);
		}
		redirect('community_manager');
	}
	
	private function get_rules() {
		$config = array(
		   array(
				 'field'   => 'first_name',
				 'label'   => 'first name',
				 'rules'   => 'required'
			  ),
		  array(
				 'field'   => 'third_name',
				 'label'   => 'last name',
				 'rules'   => 'required'
			  ),
		  array(
				 'field'   => 'nick_name',
				 'label'   => 'nickname',
				 'rules'   => 'required'
			  ),
		  array(
				 'field'   => 'email_address',
				 'label'   => 'email address',
				 'rules'   => 'required'
			  )
		);
		return $config;
	}

	private function validate_fields() {
		$email = $this->input->post('email_address');
		$id = $this->input->post('id');
		$where['email_address'] = $email;
		$param['table'] = 'tbl_registrants';
		$param['where'] = $where;

		$registrant = (array)$this->global_model->get_row($param);
		if($registrant) {
			if($id && $registrant['registrant_id'] == $id)
				return true;
			return false;
		}
		
		return true;
	}

	private function upload_image(&$error = false, &$uploaded = false) {
		$this->load->helper(array('upload_helper', 'resize'));
		$config['upload_path'] = 'uploads/move_fwd/';
		$config['allowed_types'] = 'gif|png|jpg';
		$config['do_upload'] = 'photo';
		$config['max_width'] = 2000;
		$config['max_height'] = 1600;
		$config['max_size'] =  8072;
		$config['file_name'] = uniqid() . '.' . end(explode('.', $_FILES['photo']['name']));
		$uploaded = upload($config);
		if(!is_array($uploaded)) {
			$error = $uploaded;
			return;
		} else {

			$params = array('new_filename' => 'main_' .$config['file_name'], 'width'=>319,'height'=>300,'source_image'=>$config['upload_path'].'/'.$uploaded['file_name'],'new_image_path'=>$config['upload_path'].'/','file_name'=>$config['file_name']);
	    	resize($params);
	    }
		return $uploaded['file_name'];
	}

	private function generate_thumb($uploaded) {
		list($width, $height) = getimagesize($uploaded['full_path']); 
		if($width < 300) {
			copy($uploaded['full_path'], $uploaded['file_path'].'thumb_'.$uploaded['file_name']);
			return;
		}

		$this->load->helper('crop');
		$config['x'] = $this->input->post('x');
		$config['y'] = $this->input->post('y');
		$config['x2'] = $this->input->post('x2');
		$config['y2'] = $this->input->post('y2');
		$config['width'] = $this->input->post('width');
		$config['height'] = $this->input->post('height');
		$params = array_merge($config, $uploaded);	    
		crop($params);	

	}

	// public function resize_all() {
		
	// 	$this->load->helper('resize');
	// 	$activities = $this->db->select('*')
	// 						   ->from('tbl_backstage_events_photos')
	// 						   ->get()
	// 						   ->result_array();
	// 						   print_r($activities);
	// 	if($activities) {
	// 		$config['upload_path'] = 'uploads/backstage/photos/';
	// 		$config['allowed_types'] = 'gif|png|jpg';
	// 		foreach ($activities as $key => $value) {
	// 			echo 'new_filename' => '100_100_' .$value['image'] .'<br>';
	// 			$params = array('new_filename' => '100_100_' .$value['image'], 'width'=>100,'height'=>100,'source_image'=>$config['upload_path'].'/'.$value['image'],'new_image_path'=>$config['upload_path'].'/thumbnails/','file_name'=>$value['image']);
	//     		resize($params);
	// 			//echo $config['new_image'] . '<br>';
	// 		}
	// 	}

	// }

	public function test() {
		echo 'test';
		$this->load->helper('resize');
		$activities = $this->db->select('*')
							   ->from('tbl_backstage_events_photos')
							   ->get()
							   ->result_array();
							   
		if($activities) {
			$config['upload_path'] = 'uploads/backstage/photos/';
			$config['allowed_types'] = 'gif|png|jpg';
		 	
		 	foreach ($activities as $key => $value) {
		 		echo 'test';
		 		//echo 'new_filename' => '100_100_' .$value['image'] .'<br>';
				$params = array('new_filename' => '331_176_' .$value['image'], 'width'=>331,'height'=>176,'source_image'=>$config['upload_path'].'/'.$value['image'],'new_image_path'=>$config['upload_path'].'/thumbnails/','file_name'=>$value['image']);
	    		resize($params);
				//echo $config['new_image'] . '<br>';
			}
		}
	}

	

	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		elseif($method == 'test')
			$this->test();
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */