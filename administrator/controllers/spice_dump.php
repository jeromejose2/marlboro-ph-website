<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
set_time_limit(0);
ini_set('memory_limit', '512M');
class Spice_dump extends CI_Controller {
	
	public function __construct() {
		parent::__construct();

	}
	public function index() {
		$filename = 'uploads/spice/extract_with_personid_01052015.csv';
		if (($handle = fopen("$filename", "r")) !== FALSE) {
			 while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
			 	$registrant_dup = $this->db->select()
			 						->from('tbl_duplicate_individual_id')
			 						->where('individual_id', $data[1])
			 						->get()
			 						->result_array();
			 	if(count($registrant_dup)) {
			 		if(strtoupper($data[4]) != 'NULL') {
			 			$registrant = $this->db->select()
			 							  ->from('tlb_registrants_prod')
			 							  ->where('email_address', $data[4])
			 							  ->order_by('registrant_id', 'DESC')
			 							  ->get()	
			 							  ->result_array();
			 			if($registrant) {
			 				foreach ($registrant as $key => $value) {
			 					if($key == 0) {
			 						$this->db->where('registrant_id', $value['registrant_id']);
			 						$update_arr['person_id'] = $data[0];
			 						$update_arr['cleanup_remarks'] = 'SUCCESS';
			 						$this->db->update('tlb_registrants_prod', $update_arr);
			 					} else {
			 						$this->db->where('registrant_id', $value['registrant_id']);
			 						$update_arr['cleanup_remarks'] = 'UPDATE FAILED - DUPLICATE EMAIL ADDRESS';
			 						$this->db->update('tlb_registrants_prod', $update_arr);	
			 					}
			 				}
			 			} else {
			 				foreach ($registrant_dup as $key => $value) {
			 					if($value['last_login'] != '') {
			 						$this->db->where('registrant_id', $value['registrant_id']);
			 						$update_arr['cleanup_remarks'] = 'UPDATE FAILED - DUPLICATE INDIVIDUAL ID - WITH LOGIN';
			 						$this->db->update('tlb_registrants_prod', $update_arr);	
			 					} else {
			 						$this->db->where('registrant_id', $value['registrant_id']);
			 						$update_arr['cleanup_remarks'] = 'UPDATE FAILED - DUPLICATE INDIVIDUAL ID - WITHOUT LOGIN';
			 						$this->db->update('tlb_registrants_prod', $update_arr);		
			 					}
			 				}		
			 			}
			 		} else {
			 			$registrant = $this->db->select()
			 							  ->from('tlb_registrants_prod')
			 							  ->where('individual_id', $data[1])
			 							  ->order_by('registrant_id', 'DESC')
			 							  ->get()	
			 							  ->result_array();	
			 			if($registrant) {
			 				foreach ($registrant as $key => $value) {
			 					if($key == 0) {
			 						$this->db->where('registrant_id', $value['registrant_id']);
			 						$update_arr['person_id'] = $data[0];
			 						$update_arr['cleanup_remarks'] = 'SUCCESS';
			 						$this->db->update('tlb_registrants_prod', $update_arr);
			 					} else {
			 						$this->db->where('registrant_id', $value['registrant_id']);
			 						$update_arr['cleanup_remarks'] = 'UPDATE FAILED - DUPLICATE EMAIL ADDRESS';
			 						$this->db->update('tlb_registrants_prod', $update_arr);	
			 					}
			 				}
			 			} else {
			 				$this->db->where('individual_id', $data[1]);
	 						$update_arr['cleanup_remarks'] = 'UPDATE FAILED - DUPLICATE INDIVIDUAL ID - NO MATCHING EMAIL ADDRESS ';
	 						$this->db->update('tlb_registrants_prod', $update_arr);			
			 			}
			 		}
			 		
			 	} else {
			 		$this->db->where('individual_id', $data[1]);
					$update_arr['person_id'] = $data[0];
					$update_arr['cleanup_remarks'] = 'SUCCESS';
					$this->db->update('tlb_registrants_prod', $update_arr);
			 	}
			 }
		}
	}

	public function update_individ(){
		$registrants = $this->db->select()
								->from('tbl_registrants')
								->join('tbl_arclight_emails', 'tbl_arclight_emails.email = tbl_registrants.email_address')
								->where('email !=', 'NULL')
								->where('tbl_registrants.individual_id is null')
								->order_by('registrant_id')
								->get()
								->result_array();
		die($this->db->last_query());
		echo '<pre>';
		print_r($registrants);	
		die();
		if($registrants) {
			foreach ($registrants as $key => $value) {
				$this->db->where('registrant_id', $value['registrant_id']);
				$update_arr['person_id'] = $value['personid'];
				$update_arr['individual_id'] = $value['individualid'];
				$this->db->update('tbl_registrants', $update_arr);
			}
		}
	}

	public function update_personid() {
		$login = $this->db->select('*')
							  ->from('tbl_login')
							  ->join('tbl_registrants', 'tbl_registrants.registrant_id = tbl_login.registrant_id')
							  ->where('status', 1)
							  ->group_by('tbl_login.registrant_id')
							  ->get()
							  ->result_array();
		$success = array();
		$failed = array();
		if($login) {
			foreach ($login as $key => $value) {
				$registrant = $this->db->select()
										->from('tbl_duplicate_individual_id')
										->where('individual_id', $value['individual_id'])
										->get()
										->result_array();
				if($registrant) {
					foreach($registrant as $k => $v) {
						if($value['email_address'] == $v['arclight_email']) {
							$success[] = $value['registrant_id'];
						} else {
							$failed[] = $value['registrant_id'];
						}
					}
				} else {
					$success[] = $value['registrant_id'];
				}
			}
		}
		$this->db->query("UPDATE tbl_registrants SET cleanup_remarks = 'SUCCESS', person_id = (SELECT personid FROM tbl_arclight_emails WHERE tbl_registrants.individual_id = tbl_arclight_emails.individualid) WHERE registrant_id IN (" . implode(',', $success) .  ")");
		echo "UPDATE tbl_registrants SET cleanup_remarks = 'SUCCESS', person_id = (SELECT personid FROM tbl_arclight_emails WHERE tbl_registrants.individual_id = tbl_arclight_emails.individualid) WHERE registrant_id IN (" . implode(',', $success) .  ")";
		
		//print_r($row);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
