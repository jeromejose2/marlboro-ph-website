<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Day_logins extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index()
	{
		if(!$this->input->get('fromdate') || !$this->input->get('todate')) {
			redirect('day_logins?fromdate=' . date('Y-m-d', strtotime('-30 days')) .  '&todate=' . date('Y-m-d'));
		}
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	public function export()
	{
		$this->load->library('to_excel_array');
		$records = $this->get_demographics($from, $to);
		$days = array('', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
		$row[] = $from && $to ? array('Login Spread (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')') : array('Login Spread');

		$row[] = array('Day', 'Visits', 'Visitors');
		if($records) {
			foreach($records as $k => $v) {
				$visitors = $v['visitors'] ? $v['visitors'] : '0';
				$visits = $v['visits'] ? $v['visits'] : '0';
				$row[] = array(
							  $days[$v['dlog']],
							  $visits,
							  $visitors);
			}
		}
		$this->to_excel_array->to_excel($row, 'day_logins_'.date("YmdHis"));
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$data['records'] = $this->get_demographics();
		$data['days'] = array('', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
		return $this->load->view('visits/day_login', $data, true);		
	}

	private function get_demographics(&$from = false, &$to = false) {
		$like = array();
		$where = array();
		if($this->input->get('fromdate') && $this->input->get('todate')) {
			$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
			$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
			$where['DATE(date_login) <='] = $to;
			$where['DATE(date_login) >='] = $from;
		}
		$field = $this->input->get('field') ? $this->input->get('field') : 'dlog';
		$sort = $this->input->get('sort') ? $this->input->get('sort') : 'asc';
		$sort = strtolower($sort) == 'asc' ? SORT_ASC : SORT_DESC;
		
		for($i = 1; $i <= 7; $i++) {
			$param['having'] = array('dlog'	=> $i);
			$param['like'] = $like;
			$param['fields'] = 'DAYNAME(date_login) AS tlog, DAYOFWEEK(date_login) as dlog, COUNT(*) AS count';
			$param['where'] = $where;
			$param['table'] = 'tbl_login';
			$param['order_by'] = array('field'	=> 'dlog', 'order'	=> 'ASC');
			$param['group_by'] = 'tlog';
			$visits = $this->global_model->get_rows($param)->result_array();	
			
			$param['like'] = $like;
			$param['fields'] = 'DAYNAME(date_login) AS tlog, DAYOFWEEK(date_login) as dlog';
			$param['where'] = $where;
			$param['table'] = 'tbl_login';
			//$param['order_by'] = array('field'	=> $field, 'order'	=> $sort);
			$param['group_by'] = array('registrant_id', 'dlog');
			$visitors = $this->global_model->get_rows($param)->num_rows();

			$records[] = array('visits'		=> isset($visits[0]['count']) ? $visits[0]['count'] : 0,
								'visitors'	=> $visitors,
								'dlog'		=> $i);
		}
		
		$this->array_sort_by_column($records, $field, $sort);
		return $records;
	}
	
	private function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
		$sort_col = array();
	    foreach ($arr as $key=> $row) {
	        $sort_col[$key] = $row[$col];
	    }

	    array_multisort($sort_col, $dir, $arr);
	}

	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */