<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logins extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

	}

	public function export()
	{
		$this->load->library('to_excel_array');
		$records = $this->get_demographics($from, $to)['records'];
		$row[] = $from && $to ? array('Time of Day for Logins (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')') : array('Time of Day for Logins');

		$row[] = array('Hour', 'Visits');
		if($records) {
			foreach($records as $k => $v) {
				 if($v['tlog'] <= 11) {
                      $time =  $v['tlog'] == 0 ? '12AM' : $v['tlog'] . 'AM';
                    } else {
                      $time = ($v['tlog'] - 12) == 0 ? '12PM' : ($v['tlog'] - 12) . 'PM';
                    }
				$row[] = array($time,
							  $v['count']);
			}
		}
		$this->to_excel_array->to_excel($row, 'time_of_day_logins_'.date("YmdHis"));
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$data = $this->get_demographics();
		$data['to'] = $data['to'];
		$data['from'] = $data['from'];
		$data['records'] = $data['records'];
		$query_string = $this->input->get() ?  $this->input->get() : array('temp'=>0); 
		$query_string['fromdate'] = $data['from'];
		$query_string['todate'] = $data['to'];


		if(isset($query_string['sort_field']))
			unset($query_string['sort_field']);

		if(isset($query_string['sort_order']))
			unset($query_string['sort_order']);

	 	$data['query_string'] = $query_string;
		return $this->load->view('visits/login', $data, true);		
	}

	private function get_demographics(&$from = false, &$to = false) {
		$like = array();
		$where = array();
		$to = '';
		$from = '';
		if($this->input->get('fromdate') && $this->input->get('todate')) {
			$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
			$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
			$where['DATE(date_login) <='] = $to;
			$where['DATE(date_login) >='] = $from;
		}else if(!$this->input->get()){
			
			$to = $this->global_model->get_row(array('table'=>'tbl_login','fields'=>'DATE(MAX(date_login)) as to_date'))->to_date;
			$from = date('Y-m-d',strtotime($to.' -1 month'));
 			$where['DATE(date_login) <='] = $to;
			$where['DATE(date_login) >='] = $from; 
		}

		$param['like'] = $like;
		$param['fields'] = 'HOUR(date_login) AS tlog, COUNT(*) AS count';
		$param['where'] = $where;
		$param['table'] = 'tbl_login';
		$param['order_by'] = array('field'	=> $this->input->get('sort_by') ? $this->input->get('sort_by') : 'tlog', 'order'	=> $this->input->get('sort_order') ? $this->input->get('sort_order') : 'ASC');
		$param['group_by'] = 'tlog';
		$param['offset'] = 0;
		$param['limit'] = 30;
		$records = array('records'=>$this->global_model->get_rows($param)->result_array(),'to'=>$to,'from'=>$from);
		return $records;
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */