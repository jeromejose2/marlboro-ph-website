<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reply extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		include_once('application/models/points_model.php');
		include_once('application/models/notification_model.php');
		$this->points_model = new Points_Model();
		$this->notification_model = new Notification_Model();
		$this->_table = 'tbl_comment_replies';
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		$where = 'is_deleted != 1';
		$param = array();
		if(isset($_GET['search'])) {
			//print_r($_GET);
			//die();
			$where .=  " AND CONCAT(vw_comments.first_name, ' ' , vw_comments.third_name) LIKE '%$_GET[first_name]%'";
			$where .= isset($_GET['nick_name']) ? " AND nick_name LIKE '%$_GET[nick_name]%'" : "";
			$where .= isset($_GET['comment_id']) ? " AND tbl_comment_replies.comment_id LIKE '%$_GET[comment_id]%'" : "";
			$where .= isset($_GET['comment_reply']) ? " AND comment_reply LIKE '%$_GET[comment_reply]%'" : "";
			$where .= isset($_GET['title']) ? " AND ((gtitle LIKE '%$_GET[title]%' AND origin_id = " . MOVE_FWD_GALLERY . ") OR (move_forward_title LIKE '%$_GET[title]%' AND origin_id = " . MOVE_FWD . ") OR (ntitle LIKE '%$_GET[title]%' AND origin_id = " . ABOUT_NEWS . ") OR (btitle LIKE '%$_GET[title]%' AND origin_id = " . BACKSTAGE_PHOTOS . ") OR (vtitle LIKE '%$_GET[title]%' AND origin_id = " . ABOUT_VIDEOS . "))" : "";
			$where .= $_GET['fromavail'] ? " AND DATE(comment_status_changed) >= '$_GET[fromavail]'" : "";
			$where .= $_GET['toavail'] ? " AND DATE(comment_status_changed) <= '$_GET[toavail]'" : "";
			$where .= $_GET['comment_reply_status'] != '' ? " AND comment_reply_status = '$_GET[comment_reply_status]'" : "";
			$where .= isset($_GET['origin_id']) != '' ? " AND origin_id = '$_GET[origin_id]'" : "";
		
		}
		$param['limit'] = PER_PAGE;
		$query = "SELECT *, tbl_registrants.first_name as fname,tbl_registrants.third_name as lname  FROM {$this->_table} Inner JOIN vw_comments ON vw_comments.comment_id = {$this->_table}.comment_id inner join tbl_registrants on {$this->_table}.registrant_id=tbl_registrants.registrant_id WHERE {$where} ORDER BY comment_reply_date_created DESC LIMIT " . $data['offset'] . ", " . PER_PAGE;

		$comments = $this->global_model->custom_query($query)->result_array();
 		
		//die($this->db->last_query());
		$query = "SELECT * FROM {$this->_table} Inner JOIN vw_comments ON vw_comments.comment_id = {$this->_table}.comment_id WHERE {$where}";
		$records = $this->global_model->custom_query($query)->num_rows();

// $query = "SELECT * FROM {$this->_table}
// Left Join (select * from vw_comments order by vw_comments.comment_date_created LIMIT " . $data['offset'] . ", " . PER_PAGE." ) as comments on  comments.comment_id = {$this->_table}.comment_id
//  WHERE {$where} ORDER BY comment_date_created DESC LIMIT " . $data['offset'] . ", " . PER_PAGE;


		//$page = $this->uri->segment(2, 1);
		// $like = array();
		// $data['offset'] = ($page - 1) * PER_PAGE;
		// $where = '';
		// $param = array();
		// if(isset($_GET['search'])) {
		// 	$like = $this->input->get();
		// 	$like[$this->_table . '.comment_id'] = $like['comment_id'];

		// 	if($like['fromavail'] && $like['toavail']) {
		// 		$from = $like['fromavail'] > $like['toavail'] ?  $like['toavail'] :  $like['fromavail'];
		// 		$to = $like['toavail'] < $like['fromavail'] ?  $like['fromavail'] :  $like['toavail'];
		// 		$where['DATE(date_created) <='] = $to;
		// 		$where['DATE(date_created) >='] = $from;
		// 	}

		// 	if($like['first_name']) {
		// 		$like["CONCAT(first_name, ' ' , third_name)"] = $like['first_name'];
		// 	}

		// 	unset($like['first_name']);
		// 	unset($like['search']);
		// 	unset($like['fromredeem']);
		// 	unset($like['fromavail']);
		// 	unset($like['toavail']);
		// 	unset($like['toredeem']);
		// 	unset($like['comment_id']);

		// }
		// $param['like'] = $like;
		// $param['table'] = $this->_table;
		// $param['where'] = $where;
		// $param['offset'] = $data['offset'];
		// $param['limit'] = PER_PAGE;
		// $param['order_by'] = array('field'	=> 'comment_reply_date_created', 'order'	=> 'DESC');
		// //$param['join'] = @array_merge($param['join'], array('tbl_registrants' => 'tbl_registrants.registrant_id = ' . $this->_table . '.registrant_id'));
		// $param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id = ' . $this->_table . '.registrant_id', 
		// 					  'vw_comments'	=> 'vw_comments.comment_id = ' . $this->_table . '.comment_id');
		// $comments = $this->global_model->get_rows($param)->result_array();
		// $records = $this->global_model->get_total_rows($param);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/reply');
		$data['status'] = array('Pending', 'Approved', 'Rejected');
		//$exclude = array(FLASH_OFFERS, BIRTHDAY_OFFERS);
		//$data['origins'] = $this->module_model->get_origins($exclude);
	
		$comments_arr = array();
		if($comments) {
			foreach ($comments as $key => $value) {
				$value['item_title'] = $this->get_title($value['origin_id'], $value);
				$value['url'] = $this->get_url($value['origin_id'], $value['suborigin_id']);
				$comments_arr[] = $value;
			}
		}
		$data['categories'] = $comments_arr;
		$access = $this->module_model->check_access('reply');
		$data['total'] = $records;
		$data['edit'] = $access['edit'];
		return $this->load->view('reply/index', $data, true);		
	}

	private function get_url($origin_id, $suborigin_id) {
		$url = BASE_URL;
		switch($origin_id) {
			case MOVE_FWD:
				$param['table'] = 'tbl_move_forward';
				$param['where'] = array('move_forward_id'	=> $suborigin_id);
				$move_forward = $this->global_model->get_row($param)
;				$url .= 'move_forward/offer/' . $suborigin_id . '/' . @$move_forward->permalink;
				break;
			// case ABOUT_NEWS:
			// 	$param['table'] = 'tbl_move_forward';
			// 	$param['where'] = array('move_forward_id'	=> $suborigin_id);
			// 	$move_forward = $this->global_model->get_row($param);
			// 	$url .= 'move_forwardoffer/' . $suborigin_id . '/' . $move_forward->permalink;
			// 	break;
			// case ABOUT_VIDEOS:
			// 	$param['table'] = 'tbl_move_forward';
			// 	$param['where'] = array('move_forward_id'	=> $suborigin_id);
			// 	$move_forward = $this->global_model->get_row($param);
			// 	$url .= 'move_forwardoffer/' . $suborigin_id . '/' . $move_forward->permalink;
			// 	break;
			// case MOVE_FWD_GALLERY:
			// 	$param['table'] = 'tbl_move_forward';
			// 	$param['where'] = array('move_forward_id'	=> $suborigin_id);
			// 	$move_forward = $this->global_model->get_row($param);
			// 	$url .= 'move_forwardoffer/' . $suborigin_id . '/' . $move_forward->permalink;
			// 	break;
			case BACKSTAGE_PHOTOS:
				$param['table'] = 'tbl_backstage_events_photos';
				$param['join'] = array('tbl_backstage_events' => 'tbl_backstage_events_photos.backstage_event_id = tbl_backstage_events.backstage_event_id');
				$param['where'] = array('tbl_backstage_events_photos.photo_id'	=> $suborigin_id);
				$backstage_pass = $this->global_model->get_row($param);
				$url .= 'backstage_pass/content/' . $backstage_pass->url_title . '/' . $backstage_pass->photo_id;
				break;
			case MOVE_FWD_COMMENTS_PHOTOS:
				$param['table'] = 'vw_comments';
				$param['where'] = array('comment_id'	=> $suborigin_id);
				$move_forward = $this->global_model->get_row($param);
				$url .= 'move_forward/offer/' . $move_forward->suborigin_id . '/' . url_title(ucwords($move_forward->move_forward_title), '-');
				break;
			case MOVE_FWD_COMMENT_REPLIES_PHOTOS:
				$param['table'] = 'vw_comments';
				$param['join'] = array('tbl_comment_replies' => 'tbl_comment_replies.comment_id = vw_comments.comment_id');
				$param['where'] = array('comment_reply_id'	=> $suborigin_id);
				$move_forward = $this->global_model->get_row($param);
				$url .= 'move_forward/offer/' . $move_forward->suborigin_id . '/' . url_title(ucwords($move_forward->move_forward_title), '-');
				break;

		}
		return $url;

	}

	private function get_title_field($origin_id) {
		$title = array(MOVE_FWD => 'move_forward_title');
		return $title[$origin_id];
	}

	// private function get_title($origin_id, $suborigin_id) {
	// 	$title = '';
	// 	switch($origin_id) {
	// 		case MOVE_FWD:
	// 			$table = 'tbl_move_forward';
	// 			$param['table'] = $table;
	// 			$param['where'] = array('move_forward_id' => $suborigin_id);
	// 			$row = (array)$this->global_model->get_row($param);
	// 			$title = $row['move_forward_title'];
	// 			break;
	// 	}
	// 	return $title;

	// }

	private function get_join($origin_id) {
		$join = array();
		switch($origin_id) {
			case MOVE_FWD:
				$join = array('tbl_move_forward'	=> 'tbl_move_forward.move_forward_id = ' . $this->_table . '.suborigin_id');
				break;
		}
		return $join;

	}

	public function approve() {
		$id = $this->uri->segment(3);
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/reply') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->update_status($id, 1, 'approved a comment reply');
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function disapprove() {
		$id = $this->uri->segment(3);
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/reply') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->update_status($id, 2, 'rejected a comment reply',  $this->input->post('message'));
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	private function update_status($id, $new_status, $action_text = '', $message = '') {
		$where['comment_reply_id'] = $id;
		$where2['comment_reply_id'] = $id;
		$where2['is_deleted'] = 0;
		$param['where'] = $where2;
		$param['table'] = $this->_table;
		$param['fields'] = '*, tbl_comment_replies.registrant_id AS commenter';
		$param['join'] = array('tbl_comments'	 => 'tbl_comments.comment_id = ' . $this->_table . '.comment_id',
								'tbl_registrants' => 'tbl_registrants.registrant_id  = tbl_comments.registrant_id');
		$comment = (array)$this->global_model->get_row($param);
		if(!$comment) {
			$this->session->set_flashdata('error', 'Sorry, reply is already deleted.');
			redirect('reply');
		}

		if($comment['comment_reply_status'] != 0) {
			$err_msg = $comment['comment_reply_status'] == 1 ? "approved" : "rejected";
			$this->session->set_flashdata('error', 'Sorry, reply is already '.$err_msg);
			redirect('reply');
		}

		$mvwdid = $this->db->select()->from('tbl_comments')->where('comment_id', $comment['comment_id'])->get()->result_array();
		if($mvwdid) {
			$mv_forward_id = $mvwdid[0]['suborigin_id'];
			$move_title = (array) $this->db->select()->from('tbl_move_forward')->where('move_forward_id', $mv_forward_id)->get()->row();
			if($move_title) {
				$mvwdid = $move_title['move_forward_title'];
			}
			else {
				$mvwdid = false;
			}
		} else {
			$mvwdid = false;
		}
		#update status
		$set['comment_reply_status'] = $new_status;
		$set['comment_reply_status_changed'] = date('Y-m-d H:i:s');
		$this->global_model->update_record($this->_table, $where, $set);
		// echo $this->db->last_query();
		
		#save audit trail
		$status = $this->global_model->get_status();
		$new_content['comment_reply_status'] = $status[$new_status];
		$old_content['comment_reply_status'] = $status[$comment['comment_reply_status']];
		$post['url'] = @$_SERVER['HTTP_REFERER'];
		$post['description'] = $action_text;
		$post['table'] = $this->_table;
		$post['record_id'] = $id;
		$post['message'] = $message;
		$post['field_changes'] = serialize(array('old'	=> $old_content,
												  'new'	=> $new_content));
		$this->module_model->save_audit_trail($post);	
	
		# points
		$pronoun = $comment['commenter'] == $comment['registrant_id'] ? ' your own comment' : $comment['first_name'] .  ' ' . $comment['third_name'] . '\'s comment';
		/*$this->points_model->earn(COMMENT_REPLY,array('suborigin'	=> $id,
													'registrant'	=> $comment['registrant_id'],
													'comment'		=> $comment['comment_id'],
													'remarks' 		=> '{{ name }} replied to '. $pronoun));*/
		if($new_status == 1) {
			if($comment['comment_reply_status'] == 0 && $comment['commenter'] != $comment['registrant_id']) {
				$points_registrant = $this->points_model->earn(COMMENT_REPLY_RECEIVE, array('suborigin'		=> $id,
														       'registrant'	=> $comment['registrant_id']));

				$points_commenter = $this->points_model->earn(COMMENT_REPLY_RECEIVE, array('suborigin'		=> $id,
														       'registrant'	=> $comment['commenter']));

				$this->notification_model->notify($comment['registrant_id'], COMMENT_REPLY_RECEIVE, array(
															'message' => $this->get_notification_copy($comment['origin_id'], $comment['suborigin_id'], 1, $points_registrant, true, $mvwdid),
															'suborigin' => $id
															));
				$this->notification_model->notify($comment['commenter'], COMMENT_REPLY, array(
															'message' => $this->get_notification_copy($comment['origin_id'], $comment['suborigin_id'], 1, $points_commenter, false, $mvwdid),
															'suborigin' => $id
															));
			} elseif($comment['comment_reply_status'] == 2) {
				$this->points_model->activate($comment['registrant_id'], array(
					array(
						'origin' => COMMENT_REPLY_RECEIVE,
						'suborigin' => $id
					)
				));		
			}
		} else {
			if($comment['comment_reply_status'] == 1) {
				$this->points_model->deactivate($comment['registrant_id'], array(
					array(
						'origin' => COMMENT_REPLY_RECEIVE,
						'suborigin' => $id
					)
				));		
			}

			$this->notification_model->notify($comment['commenter'], COMMENT_REPLY, array(
																'message' => $this->get_notification_copy($comment['origin_id'], $comment['suborigin_id'], 0, 0, false, $mvwdid, $message),
																'suborigin' => $id
																));	
		}
		
	}

	private function get_notification_copy($origin_id, $suborigin_id, $status = 1, $points, $is_recipient = false, $mv_title = false, $message = false) {
		$prepend_copy = !$is_recipient ? 'Your reply on ' : 'The reply on your comment under ';
		if($origin_id == MOVE_FWD || $origin_id == MOVE_FWD_COMMENTS_PHOTOS || $origin_id == MOVE_FWD_COMMENT_REPLIES_PHOTOS) {
			$param['table'] = 'tbl_move_forward';
			$param['where'] = array('move_forward_id' => $suborigin_id);

			$record = (array)$this->global_model->get_row($param);
			if($mv_title === false) {
				if($points > 0) {
					if($status == 1) {
						return $prepend_copy . @$mv_title . ' (MoveFWD) has been approved. You gained ' . $points . ' points. Thank you!';
					} else {
						if($message) {
							return $prepend_copy . @$mv_title . ' (MoveFWD) has been disapproved because '. $message .'. Please try again!';	
						} else {
							return $prepend_copy . @$mv_title . ' (MoveFWD) has been disapproved. Please try again!';
						}
					}
				} else {
					if($status == 1) {
						return $prepend_copy . @$mv_title . ' (MoveFWD) has been approved. Thank you!';
					} else {
						if($message) {
							return $prepend_copy . @$mv_title . ' (MoveFWD) has been disapproved because '. $message .'. Please try again!';	
						} else {
							return $prepend_copy . @$mv_title . ' (MoveFWD) has been disapproved. Please try again!';
						}
					}
				}				
			} else {
				if($points > 0) {
					if($status == 1) {
						return $prepend_copy . @$mv_title . ' (MoveFWD) has been approved. You gained ' . $points . ' points. Thank you!';
					} else {
						if($message) {
							return $prepend_copy . @$mv_title . ' (MoveFWD) has been disapproved because '. $message .'. Please try again!';
						} else {
							return $prepend_copy . @$mv_title . ' (MoveFWD) has been disapproved. Please try again!';
						}							
					}
				} else {
					if($status == 1) {
						return $prepend_copy . @$mv_title . ' (MoveFWD) has been approved. Thank you!';
					} else {
						if($message) {
							return $prepend_copy . @$mv_title . ' (MoveFWD) has been disapproved because '. $message .'. Please try again!';
						} else {
							return $prepend_copy . @$mv_title . ' (MoveFWD) has been disapproved. Please try again!';
						}						
					}
				}				
			}
			

		} elseif($origin_id == ABOUT_PHOTOS && FALSE) {
			$param['table'] = 'tbl_about_events_photos';
			$param['join'] = array('tbl_about_events' => 'tbl_about_events_photos.about_event_id = tbl_about_events.about_event_id');
			$param['where'] = array('photo_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			if($status == 1) 
				return $prepend_copy . $record['title'] . ' on ' . $record['type'] . ' has been approved. You gained ' . $points . ' points. Thank you!';
			else 
				return $prepend_copy . $record['title'] . ' on ' . $record['type'] . ' has been disapproved. Please try again!';
		} elseif($origin_id == ABOUT_VIDEOS) {
			$param['table'] = 'tbl_about_videos';
			$param['where'] = array('about_video_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			if($status == 1) 
				return $prepend_copy . $record['title'] . ' on About has been approved. You gained ' . $points . ' points. Thank you!';
			else 
				return $prepend_copy . $record['title'] . ' on About has been disapproved. Please try again!';
		} elseif($origin_id == ABOUT_NEWS) {
			$param['table'] = 'tbl_about_news';
			$param['where'] = array('about_news_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			if($status == 1) 
				return $prepend_copy . $record['title'] . ' on About has been approved. You gained ' . $points . ' points. Thank you!';
			else 
				return $prepend_copy . $record['title'] . ' on About has been disapproved. Please try again!';
		} elseif($origin_id == BACKSTAGE_PHOTOS) {
			$param['table'] = 'tbl_backstage_events_photos';
			$param['join'] = array('tbl_backstage_events' => 'tbl_backstage_events_photos.backstage_event_id = tbl_backstage_events.backstage_event_id');
			$param['where'] = array('photo_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			if($points > 0) {
				if($status == 1) {
					return $prepend_copy . @$record['title'] . ' (Backstage Pass) has been approved. You gained ' . $points . ' points. Thank you!';
				} else {
					if($message) {
						return $prepend_copy . @$record['title'] . ' (Backstage Pass) has been disapproved because '. $message .'. Please try again!';
					} else{
						return $prepend_copy . @$record['title'] . ' (Backstage Pass) has been disapproved. Please try again!';
					}					
				}
			} else {
				if($status == 1) {
					return $prepend_copy . @$record['title'] . ' (Backstage Pass) has been approved. Thank you!';
				} else {
					if($message) {
						return $prepend_copy . @$record['title'] . ' (Backstage Pass) has been disapproved because '. $message .'. Please try again!';
					} else {
						return $prepend_copy . @$record['title'] . ' (Backstage Pass) has been disapproved. Please try again!';
					}					
				}
			}
		}
	}

	private function get_title($origin_id, $value) {
		$title = '';
		switch($origin_id) {
			case MOVE_FWD:
				$title = $value['move_forward_title'];
				break;
			case ABOUT_NEWS:
				$title = $value['ntitle'];
				break;
			case ABOUT_VIDEOS:
				$title = $value['vtitle'];
				break;
			case MOVE_FWD_GALLERY:
				$title = $value['gtitle'];
				break;
			case BACKSTAGE_PHOTOS:
				$title = $value['btitle'];
				break;

		}
		return $title;

	}

	public function approve_all() {
		$ids = $this->input->post('ids');
		if($ids) {
			foreach ($ids as $key => $value) {
				$this->update_status($value, 1, 'approved a comment');	
			}
		}	
	}

	public function multiple_disapprove() {
		$ids = $this->input->post('ids');
		if($ids) {
			foreach ($ids as $key => $value) {
				$this->update_status($value, 2, 'rejected a comment reply',  $this->input->post('message'));
			}
		}	
		
		redirect($_SERVER['HTTP_REFERER']);

	}
	
	public function export() {
		$where = '1';
		if(isset($_GET['search'])) {
			//print_r($_GET);
			//die();
			$where .=  " AND CONCAT(first_name, ' ' , third_name) LIKE '%$_GET[first_name]%'";
			$where .= isset($_GET['nick_name']) ? " AND nick_name LIKE '%$_GET[nick_name]%'" : "";
			$where .= isset($_GET['comment_id']) ? " AND tbl_comment_replies.comment_id LIKE '%$_GET[comment_id]%'" : "";
			$where .= isset($_GET['comment_reply']) ? " AND comment_reply LIKE '%$_GET[comment_reply]%'" : "";
			$where .= isset($_GET['title']) ? " AND ((gtitle LIKE '%$_GET[title]%' AND origin_id = " . MOVE_FWD_GALLERY . ") OR (move_forward_title LIKE '%$_GET[title]%' AND origin_id = " . MOVE_FWD . ") OR (ntitle LIKE '%$_GET[title]%' AND origin_id = " . ABOUT_NEWS . ") OR (btitle LIKE '%$_GET[title]%' AND origin_id = " . BACKSTAGE_PHOTOS . ") OR (vtitle LIKE '%$_GET[title]%' AND origin_id = " . ABOUT_VIDEOS . "))" : "";
			$where .= $_GET['fromavail'] ? " AND DATE(comment_status_changed) >= '$_GET[fromavail]'" : "";
			$where .= $_GET['toavail'] ? " AND DATE(comment_status_changed) <= '$_GET[toavail]'" : "";
			$where .= $_GET['comment_reply_status'] != '' ? " AND comment_reply_status = '$_GET[comment_reply_status]'" : "";
			$where .= isset($_GET['origin_id']) != '' ? " AND origin_id = '$_GET[origin_id]'" : "";
		
		}
		$param['limit'] = PER_PAGE;
		$query = "SELECT * FROM {$this->_table} inner JOIN vw_comments ON vw_comments.comment_id = {$this->_table}.comment_id WHERE {$where}";
		$comments = $this->global_model->custom_query($query)->result_array();

		$comments_arr = array();
		if($comments) {
			foreach ($comments as $key => $value) {
				$value['item_title'] = $this->get_title($value['origin_id'], $value);
				$value['url'] = $this->get_url($value['origin_id'], $value['suborigin_id']);
				$comments_arr[] = $value;
			}
		}
		
		$records = $comments_arr;
		$status = array('Pending', 'Approved', 'Disapproved');
		$row[] = array(	'Name',
						'Comment',
						 'Item Title',
						 'URL', 
						 'Status',
						 'Date Created');
		if($records) {
				foreach($records as $k => $v) {
					$row[] = array($v['first_name'] . ' ' . $v['third_name'],
								  $v['comment_reply'],
								  $v['item_title'],
								  $v['url'],
								  $status[$v['comment_reply_status']],
								  $v['comment_reply_date_created']);
				}
			}		
		$this->load->library('to_excel_array');
		$this->to_excel_array->to_excel($row, 'comment_replies_'.date("YmdHis"));
	}
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		elseif($method == 'approve')
			$this->approve();
		elseif($method == 'disapprove')
			$this->disapprove();
		elseif($method == 'approve_all')
			$this->approve_all();
		elseif($method == 'export')
			$this->export();
		elseif($method=='multiple_disapprove')
			$this->multiple_disapprove();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */