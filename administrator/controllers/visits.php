<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Visits extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$data['records'] = $this->get_demographics();
		return $this->load->view('visits/index', $data, true);		
	}

	private function get_demographics() {
		$like['section'] = 'games';
		$where = array();
		if($this->input->get('fromdate') && $this->input->get('todate')) {
			$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
			$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
			$where['DATE(date_visited) <='] = $to;
			$where['DATE(date_visited) >='] = $from;
		} 
		$param['like'] = $like;
		$param['fields'] = 'DATE(date_visited) AS dv, COUNT(*) AS count';
		$param['where'] = $where;
		$param['table'] = 'tbl_page_visits';
		$param['order_by'] = array('field'	=> 'date_visited', 'order'	=> 'ASC');
		$param['group_by'] = 'dv';
		$param['offset'] = 0;
		$param['limit'] = 30;
		$records = $this->global_model->get_rows($param)->result_array();
		return $records;
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */