<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notify extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		set_time_limit(0);
		ini_set('memory_limit', '3000M');
	}

	public function index() {
		include_once('application/models/notification_model.php');
		$this->notification_model = new Notification_Model();
		//this is not recommended
		$users = $this->db->select('registrant_id, email_address, first_name, third_name')
						  ->from('tbl_registrants')
						  ->where('status', 1)
						  ->get()
						  ->result_array();	

		echo '<pre>';
		print_r($users);

		if($users) {
			$message = "Tell us about yourself.<br><br>

						To improve your MARLBORO.PH experience and to get news that matters to you, you can fill out <a href='" . BASE_URL . "profile/interest'>this form</a> and let us know what you like.";
			foreach ($users as $key => $value) {
				$param = array('message'=>$message,'suborigin'=>0);
				$this->notification_model->notify($value['registrant_id'], REFERRAL ,$param);	
			}
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */