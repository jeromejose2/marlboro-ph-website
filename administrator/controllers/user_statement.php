<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_statement extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('statement_model');
		$this->load->model('user_model');
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	} 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$statement_search = isset($_GET['statement']) ? '\'%' . $_GET['statement'] . '%\'' : '\'%%\'';
		$statements = $this->statement_model->get_statement(' AND statement LIKE ' . $statement_search);
		$statement_like = '';
		if($statements) {
			foreach($statements as $k => $v) {
				if($k == 0)
					$statement_like .= "AND (statement_id LIKE '%" . $v['statement_id'] . ",%'";
				else 
					$statement_like .= " OR statement_id LIKE '%" . $v['statement_id'] . ",%'";
			}
			$statement_like .= ")";
		}
		$where = '';
		if(isset($_GET['search'])) {
			$where =  " " . $statement_like;
			$where .=  " AND CONCAT(r.first_name, ' ', r.third_name) LIKE '%$_GET[name]%'";
			$where .= $_GET['from_publish'] ? " AND DATE(us.date_created) >= '$_GET[from_publish]'" : "";
			$where .= $_GET['to_publish'] ? " AND DATE(us.date_created) <= '$_GET[to_publish]'" : "";
		}

		
		$page = $this->uri->segment(2, 1);
		$all_statements = $this->statement_model->get_statement('', $page, PER_PAGE, $records, true);
		$data['users'] = $this->statement_model->get_user_statement($where, $page, PER_PAGE, $records);
		if(isset($_GET['statement']) && !$statement_like) {
			$data['users'] = array();
			$records = 0;
		}

		$statement_arr = array();
		if($all_statements) {
			foreach($all_statements as $k => $v) {
				$statement_arr[$v['statement_id']] = $v['statement'];
			}
		}

		$data['statement_arr'] = $statement_arr;

		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/user_statement');
		$data['offset'] = ($page - 1) * PER_PAGE;
		$access = $this->module_model->check_access('statement');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = $records;

		return $this->load->view('user_statement/index', $data, true);		
	}

	public function export()
	{
		$this->load->library('to_excel_array');
		$statement_search = isset($_GET['statement']) ? '\'%' . $_GET['statement'] . '%\'' : '\'%%\'';
		$statements = $this->statement_model->get_statement(' AND statement LIKE ' . $statement_search);
		$statement_like = '';
		if($statements) {
			foreach($statements as $k => $v) {
				if($k == 0)
					$statement_like .= "AND (statement_id LIKE '%" . $v['statement_id'] . ",%'";
				else 
					$statement_like .= " OR statement_id LIKE '%" . $v['statement_id'] . ",%'";
			}
			$statement_like .= ")";
		}
		$where = '';
		if(isset($_GET['search'])) {
			$where =  " " . $statement_like;
			$where .=  " AND CONCAT(first_name, ' ', third_name) LIKE '%$_GET[name]%'";
			$where .= $_GET['from_publish'] ? " AND date_published >= '$_GET[from_publish]'" : "";
			$where .= $_GET['to_publish'] ? " AND date_published <= '$_GET[to_publish]'" : "";
		}
		$page = $this->uri->segment(2, 1);
		$all_statements = $this->statement_model->get_statement('', $page, PER_PAGE, $records, true);
		$data['users'] = $this->statement_model->get_user_statement($where, $page, PER_PAGE, $records, true);
		if(isset($_GET['statement']) && !$statement_like) {
			$data['users'] = array();
		}
		
		$statement_arr = array();
		if($all_statements) {
			foreach($all_statements as $k => $v) {
				$statement_arr[$v['statement_id']] = $v['statement'];
			}
		}

		$records = $data['users'];
		$row[] = array('Person ID','Name', 
					   'Statement',
					   'Date');
		if($records) {
			foreach($records as $k => $v) {

				$statements = '';
				$statement_ids = explode(',', $v['statement_id']);
				if($statement_ids) {
					foreach($statement_ids as $sk => $sv) {
						if($sv)
							$statements .= $statement_arr[$sv] . ', ';
					}
					$statements = substr($statements, 0, -2);
				}

				$row[] = array($v['person_id'], $v['first_name'] . ' ' . $v['third_name'],
							  $statements,
							  $v['date_created']);
			}
		}
		$this->to_excel_array->to_excel($row, 'user_statements_'.date("YmdHis"));
	}
	
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete();
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */