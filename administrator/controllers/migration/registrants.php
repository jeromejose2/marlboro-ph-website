<?php

class Registrants extends CI_Controller
{
	public function index()
	{
		require_once 'application/helpers/site_helper.php';
		set_time_limit(0);
		ini_set('memory_limit', '3000M');
		error_reporting(E_ALL);
		ini_set('display_errors', true);

		echo '<pre>';
		$config = array();
		$config['hostname'] = "10.66.16.202";
		$config['username'] = "marlboro.ph2";
		$config['password'] = "NDexb4t5235mLqn2crk3Oqw19ZIf85";
		$config['database'] = "marlboro.ph2";
		$config['dbdriver'] = "mysql";
		$config['dbprefix'] = "";
		$config['pconnect'] = TRUE;
		$config['db_debug'] = TRUE;
		$config['cache_on'] = FALSE;
		$config['cachedir'] = "";
		$config['char_set'] = "utf8";
		$config['dbcollat'] = "utf8_general_ci";
		$db = new PDO('mysql:dbname='.$config['database'].';host='.$config['hostname'], $config['username'], $config['password']);

		$stmt = $db->prepare("SELECT * FROM brands");
		$stmt->execute();

		$tmp = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$brands = array();
		foreach ($tmp as $brand) {
			$brands[$brand['id']] = $brand;
		}

		$stmt = $db->prepare("SELECT * FROM marlborohunt_city_reference");
		$stmt->execute();
		$tmp = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$cities = array();
		foreach ($tmp as $city) {
			$cities[$city['id']] = $city['provinceid'];
		}

		$questions = array();
		$questions[1] = 'OS9';
		$questions[2] = 'LV9';
		$questions[3] = 'WT9';
		$questions[4] = 'DB9';

		$stmt = $db->prepare("SELECT sum(inv.point) as total, inv.userid ".
				"FROM tbl_code_inventory inv ".
				"LEFT JOIN tbl_code_detail det ON inv.codeid = det.id ".
				"GROUP BY inv.userid");
		$stmt->execute();
		$tmp = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$points = array();
		foreach ($tmp as $point) {
			$points[$point['userid']] = $point['total'];
		}
		
		$count = 61190;
		$page = $this->input->get('page');
		if (!$page) {
			$page = 1;
		}
		$limit = 10000;
		$offset = ($page - 1) * $limit;
		$stmt = $db->prepare("SELECT sm.* , sbp.brand_primary, sbp.brand_secondary, sbp.question_mark, cityref.city AS cityname ".
					"FROM social_member sm ".
					"LEFT JOIN social_brand_preferences sbp ON sbp.userid = sm.id ".
					"LEFT JOIN marlborohunt_city_reference cityref ON sm.city = cityref.id");
					// "LEFT JOIN marlborohunt_city_reference cityref ON sm.city = cityref.id LIMIT {$limit} OFFSET {$offset}");
		$stmt->execute();
		$registrants = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$stmt = null;
		$db = null;

		$count = count($registrants);
		print_r("Count: ".$count."<br>");
		$i = 0;
		$inserted = 0;
		$sql = '';
		while ($i++ < $count) {

			if (!isset($registrants[$i]))
				continue;
			$reg = $registrants[$i];

			$newReg = array();
			$newReg['registrant_id'] = $reg['id'];
			$newReg['starting_points'] = isset($points[$reg['id']]) ? $points[$reg['id']] : 0;
			$newReg['total_points'] = $newReg['starting_points'];
			$newReg['picture_status'] = $reg['photo_moderation'];
			$newReg['individual_id'] = $reg['registerid'];
			$newReg['first_name'] = $reg['name'] ? $reg['name'] : '';
			$newReg['middle_initial'] = $reg['middle_name'] ? $reg['middle_name'] : '';
			$newReg['third_name'] = $reg['last_name'] ? $reg['last_name'] : '';
			$newReg['nick_name'] = $reg['nickname'];
			$newReg['email_address'] = $reg['email'];
			$newReg['date_created'] = date('Y-m-d H:i:s', strtotime($reg['register_date']));
			$newReg['street_name'] = $reg['StreetName'];
			$newReg['barangay'] = $reg['barangay'];
			$newReg['mobile_phone'] = str_replace('-', '', $reg['phone_number']);
			$newReg['government_id_number'] = $reg['giid_number'];
			$newReg['government_id_type'] = $reg['giid_type'];
			$newReg['date_of_birth'] = $reg['birthday'];
			$newReg['zip_code'] = $reg['zipcode'];
			$newReg['gender'] = $reg['sex'] == 'Male' ? 'M' : 'F';
			$newReg['city'] = $reg['city'];
			$newReg['giid_file'] = $reg['img'];
			$newReg['current_picture'] = $reg['image_profile'];
			$newReg['referral_code'] = unique_code();
			$newReg['tracking_code'] = $reg['email_token'] ? $reg['email_token'] : '';
			$newReg['salt'] = $reg['salt'];
			$newReg['password'] = $reg['password'];

			$status = (int) $reg['n_status'];
			$verified = (int) $reg['verified'];
			if ($status == 0 && $verified == 0 && !$reg['img']) {
				$newReg['status'] = PENDING_GIID;
			} elseif ($status == 0 && $verified == 0 && $reg['img']) {
				$newReg['status'] = PENDING_CSR;
			} elseif ($status == 3 && $verified == 0 && $reg['img']) {
				$newReg['status'] = REJECTED_GIID;
			} elseif ($status == 3 && $verified == 0 && !$reg['img']) {
				$newReg['status'] = REJECTED_NO_GIID;
			} elseif ($status == 1) {
				$newReg['status'] = CSR_APPROVED;
			} elseif ($status == 2) {
				$newReg['status'] = VERIFIED;
			} elseif ($status == 5) {
				$newReg['status'] = DEACTIVATED;
			} elseif ($status == 4) {
				$newReg['status'] = DELETED;
			}

			if (isset($cities[$reg['city']])) {
				$newReg['province'] = $cities[$reg['city']];
			}
			if (isset($brands[$reg['brand_primary']])) {
				$brand = $brands[$reg['brand_primary']];
				$newReg['current_brand'] = $reg['brand_primary'];
				$newReg['current_brand_code'] = $brand['code'];
				$newReg['current_brand_affinity'] = 0;
				$newReg['current_brand_flavor'] = $brand['flavor'];
				$newReg['current_brand_tar_level'] = $brand['tar'];
			}
			if (isset($brands[$reg['brand_secondary']])) {
				$brand = $brands[$reg['brand_secondary']];
				$newReg['first_alternate_brand'] = $reg['brand_secondary'];
				$newReg['first_alternate_brand_code'] = $brand['code'];
				$newReg['first_alternate_brand_affinity'] = 0;
				$newReg['first_brand_flavor'] = $brand['flavor'];
				$newReg['first_brand_tar_level'] = $brand['tar'];
			}
			if (isset($questions[$reg['question_mark']])) {
				$newReg['alternate_purchase_indicator'] = $questions[$reg['question_mark']];
			}

			$newReg['from_migration'] = 1;
			$newReg['encryption_changed'] = 0;
			foreach ($newReg as &$val) {
				$val = mysql_real_escape_string($val);
			}
			$sql .= "INSERT INTO tbl_registrants(".implode(",", array_keys($newReg)).") VALUES('".implode("','", $newReg)."');\n";
			if ($this->db->affected_rows()) {
				$inserted++;
			}
		}

		print_r("Inserted: ".$inserted);
		file_put_contents('./registrants.sql', $sql);
	}

	public function update()
	{
		require_once 'application/helpers/site_helper.php';
		set_time_limit(0);
		ini_set('memory_limit', '3000M');
		error_reporting(E_ALL);
		ini_set('display_errors', true);

		echo '<pre>';
		$config = array();
		$config['hostname'] = "10.66.16.202";
		$config['username'] = "marlboro.ph2";
		$config['password'] = "NDexb4t5235mLqn2crk3Oqw19ZIf85";
		$config['database'] = "marlboro.ph2";
		$config['dbdriver'] = "mysql";
		$config['dbprefix'] = "";
		$config['pconnect'] = TRUE;
		$config['db_debug'] = TRUE;
		$config['cache_on'] = FALSE;
		$config['cachedir'] = "";
		$config['char_set'] = "utf8";
		$config['dbcollat'] = "utf8_general_ci";
		$db = new PDO('mysql:dbname='.$config['database'].';host='.$config['hostname'], $config['username'], $config['password']);

		$stmt = $db->prepare("SELECT sm.* , sbp.brand_primary, sbp.brand_secondary, sbp.question_mark, cityref.city AS cityname ".
					"FROM social_member sm ".
					"LEFT JOIN social_brand_preferences sbp ON sbp.userid = sm.id ".
					"LEFT JOIN marlborohunt_city_reference cityref ON sm.city = cityref.id");
					// "LEFT JOIN marlborohunt_city_reference cityref ON sm.city = cityref.id LIMIT {$limit} OFFSET {$offset}");
		$stmt->execute();
		$registrants = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$stmt = null;
		$db = null;

		$count = count($registrants);
		print_r("Count: ".$count."<br>");
		$i = 0;
		$inserted = 0;
		$sql = '';
		while ($i++ < $count) {

			if (!isset($registrants[$i]))
				continue;
			$reg = $registrants[$i];

			$newReg = array();
			$newReg['registrant_id'] = $reg['id'];

			$status = (int) $reg['n_status'];
			$verified = (int) $reg['verified'];
			$newReg['status'] = 0;
			if ($status == 0 && $verified == 0 && !$reg['img']) {
				$newReg['status'] = PENDING_GIID;
			} elseif ($status == 0 && $verified == 0 && $reg['img']) {
				$newReg['status'] = PENDING_CSR;
			} elseif ($status == 3 && $verified == 0 && $reg['img']) {
				$newReg['status'] = REJECTED_GIID;
			} elseif ($status == 3 && $verified == 0 && !$reg['img']) {
				$newReg['status'] = REJECTED_NO_GIID;
			} elseif ($status == 1) {
				$newReg['status'] = CSR_APPROVED;
			} elseif ($status == 2) {
				$newReg['status'] = VERIFIED;
			} elseif ($status == 5) {
				$newReg['status'] = DEACTIVATED;
			} elseif ($status == 4) {
				$newReg['status'] = DELETED;
			}

			$sql .= "UPDATE tbl_registrants SET status = ".$newReg['status']." WHERE registrant_id = ".$newReg['registrant_id'].";\n";
			if ($this->db->affected_rows()) {
				$inserted++;
			}
		}
		$sql .= "UPDATE tbl_registrants SET status = ".VERIFIED." WHERE status = ".CSR_APPROVED." AND from_migration = 1 AND individual_id <> '' AND individual_id IS NOT NULL;";
		print_r("Updated: ".$inserted);
		file_put_contents('./registrants-update.sql', $sql);
	}


	public function passwords()
	{
		require_once 'application/helpers/site_helper.php';
		set_time_limit(0);
		ini_set('memory_limit', '3000M');
		error_reporting(E_ALL);
		ini_set('display_errors', true);

		echo '<pre>';
		$config = array();
		$config['hostname'] = "10.66.16.202";
		$config['username'] = "marlboro.ph2";
		$config['password'] = "NDexb4t5235mLqn2crk3Oqw19ZIf85";
		$config['database'] = "marlboro.ph2";
		$config['dbdriver'] = "mysql";
		$config['dbprefix'] = "";
		$config['pconnect'] = TRUE;
		$config['db_debug'] = TRUE;
		$config['cache_on'] = FALSE;
		$config['cachedir'] = "";
		$config['char_set'] = "utf8";
		$config['dbcollat'] = "utf8_general_ci";
		$db = new PDO('mysql:dbname='.$config['database'].';host='.$config['hostname'], $config['username'], $config['password']);

		$stmt = $db->prepare("SELECT sm.* , sbp.brand_primary, sbp.brand_secondary, sbp.question_mark, cityref.city AS cityname ".
					"FROM social_member sm ".
					"LEFT JOIN social_brand_preferences sbp ON sbp.userid = sm.id ".
					"LEFT JOIN marlborohunt_city_reference cityref ON sm.city = cityref.id");
					// "LEFT JOIN marlborohunt_city_reference cityref ON sm.city = cityref.id LIMIT {$limit} OFFSET {$offset}");
		$stmt->execute();
		$registrants = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$stmt = null;
		$db = null;

		$count = count($registrants);
		print_r("Count: ".$count."<br>");
		$i = 0;
		$inserted = 0;
		$sql = '';
		while ($i++ < $count) {
			if (!isset($registrants[$i]))
				continue;
			$reg = $registrants[$i];

			$sql .= "UPDATE tbl_registrants SET salt = '".mysql_real_escape_string($reg['salt'])."', password = '".mysql_real_escape_string($reg['password'])."' WHERE from_migration = 1 AND registrant_id = ".$reg['id'].";\n";
			if ($this->db->affected_rows()) {
				$inserted++;
			}
		}
		print_r("Updated: ".$inserted);
		file_put_contents('./registrants-passwords.sql', $sql);
	}

	public function reset()
	{
		unlink('./registrants.sql');
	}

	public function reset_update()
	{
		unlink('./registrants-update.sql');
	}
}