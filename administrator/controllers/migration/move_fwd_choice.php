<?php

class Move_Fwd_Choice extends CI_Controller
{
	public function index()
	{
		set_time_limit(0);
		ini_set('memory_limit', '3000M');
		error_reporting(E_ALL);
		ini_set('display_errors', true);

		$config = array();
		$config['hostname'] = "10.66.16.202";
		$config['username'] = "marlboro.ph2";
		$config['password'] = "NDexb4t5235mLqn2crk3Oqw19ZIf85";
		$config['database'] = "marlboro.ph2";
		$config['dbdriver'] = "mysql";
		$config['dbprefix'] = "";
		$config['pconnect'] = TRUE;
		$config['db_debug'] = TRUE;
		$config['cache_on'] = FALSE;
		$config['cachedir'] = "";
		$config['char_set'] = "utf8";
		$config['dbcollat'] = "utf8_general_ci";
		$db = $this->load->database($config, true);

		$result = $db->query(
			"SELECT COUNT(b.id) as total, b.datetime, b.dateverified, b.dochallenge, b.userid, b.bucketid, b.n_status ".
			"FROM my_bucket b JOIN social_member sm ON sm.id = b.userid ".
			"WHERE b.n_status = 1 ".
			"GROUP BY b.bucketid, b.userid ".
			"ORDER BY b.dateverified DESC"
		)->result_array();

		foreach ($result as $r) {
			$choice = array();
			$choice['move_forward_choice'] = !$r['dochallenge'] ? MOVE_FWD_PLAY : MOVE_FWD_PLEDGE;
			$choice['registrant_id'] = $r['userid'];
			$choice['move_forward_id'] = $r['bucketid'];
			$choice['move_forward_choice_started'] = 1;
			$choice['move_forward_choice_date'] = date('Y-m-d H:i:s', strtotime($r['datetime']));
			if ((!$r['dochallenge'] && $r['total'] == 3) || $r['dochallenge']) {
				$choice['move_forward_choice_status'] = 1;
				$choice['move_forward_choice_done'] = date('Y-m-d H:i:s', strtotime($r['dateverified']));
			}
			$this->db->insert('tbl_move_forward_choice', $choice);
		}
	}
}