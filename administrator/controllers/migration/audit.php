<?php

class Audit extends CI_Controller
{
	public function index()
	{
		$logs= $this->db->select('*')
						->from('tbl_audit_trail')
						->where('audit_trail_id >=', 287)
						->where('audit_trail_id <=', 307)
						->get()
						->result_array();
		if($logs) {
			foreach ($logs as $key => $value) {
				$table = $this->encrypt->decode($value['table']);
				$record_id = $this->encrypt->decode($value['record_id']);
				if($table && $record_id) {
					$this->db->where('audit_trail_id', $value['audit_trail_id']);
					$post['table'] = $table;
					$post['record_id'] = $record_id;
					$this->db->update('tbl_audit_trail', $post);
				}
			}
		}

	}

}