<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Activity_log extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('statement_model');
		$this->load->model('user_model');
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	public function export()
	{
		$this->load->library('to_excel_array');
		$where = '';
		if(isset($_GET['search'])) {
			$where =  $_GET['user'] != '' ? " AND token = '" . md5($_GET['user'] . $this->config->item('encryption_key')) . "'" : "";
			//$where .= $_GET['activity'] ? " AND activity = '" . md5($_GET['activity'] . $this->config->item('encryption_key')) . "'" : "";
			$where .= $_GET['from'] ? " AND DATE(timestamp) >= '$_GET[from]'" : "";
			$where .= $_GET['to'] ? " AND DATE(timestamp) <= '$_GET[to]'" : "";
		}
		
		$records = $this->module_model->get_audit_trail($where, null, null, $count, true);
		$row[] = array('Name', 
					   'Activity',
					   'URL',
					   'IP Address',
					   'Date');
		if($records) {
			foreach($records as $k => $v) {
				$row[] = array($v['cms_user_name'],
							  $v['description'],
							  $v['url'],
							  $v['ip'],
							  $v['timestamp']);
			}
		}
		$this->to_excel_array->to_excel($row, 'activity_'.date("YmdHis"));
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$where = '';
		if(isset($_GET['search'])) {
			$where =  $_GET['user'] != '' ? " AND token = '" . md5($_GET['user'] . $this->config->item('encryption_key')) . "'" : "";
			//$where .= $_GET['activity'] ? " AND activity = '" . md5($_GET['activity'] . $this->config->item('encryption_key')) . "'" : "";
			$where .= $_GET['from'] ? " AND timestamp >= '$_GET[from]'" : "";
			$where .= $_GET['to'] ? " AND timestamp <= '$_GET[to]'" : "";
		}
		
		$data['users'] = $this->module_model->get_audit_trail($where, $page, PER_PAGE, $records);
		$data['cms_users'] = $this->user_model->get_users(false);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/activity_log');
		$data['activities'] = $this->module_model->get_activities();
		$data['offset'] = ($page - 1) * PER_PAGE;
		$data['total'] = $records;
		return $this->load->view('activity_log/index', $data, true);		
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */