<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Demographics extends CI_Controller {
	
	private $_has_null;
	public function __construct() {
		parent::__construct();
		$this->_has_null = false;
 	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
 	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$this->load->helper('simplify_datetime_range');
		$type = $this->input->get('type') ? $this->input->get('type') :  'age';
		$contents = $this->get_demographics($type);
		$data['records'] = $contents['records'];
		$total = 0;
		$has_null = false;
		if($data['records']) {
			foreach($data['records'] as $k => $v) {
				$total += $v['count'];
			}
		}
		$data['total'] = $total;
		$data['query_string'] = $contents['query_string'];
		$data['has_null'] = $this->_has_null;
 		return $this->load->view('demographics/' . $type, $data, true);		
	}

	public function export() {
		$this->load->library('to_excel_array');
		$type = $this->input->get('type') ? $this->input->get('type') :  'age';
		$records = $this->get_demographics($type, $from, $to)['records'];
		$row[] = $from && $to ? array(ucfirst($type) . ' Demographics (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')') : array(ucfirst($type) . ' Demographics');
		
		if($type == 'age') {
			$row[] = array(	'Age',
						'Count',
						 'Percentage');
			$total = 0;
			if($records) {

				foreach($records as $k => $v) {
					$total += $v['count'];
				}

				foreach($records as $k => $v) {
					$row[] = array(date('Y')==$v['age'] ? 'Null Values' : $v['age'],
								  $v['count'],
								  number_format(($v['count'] / $total) * 100, 2) . '%');
				}
			}	
		} else {
			$row[] = array(	'Gender',
							'Count',
						 	'Percentage');
			$total = 0;
			if($records) {

				foreach($records as $k => $v) {
					$total += $v['count'];
				}

				foreach($records as $k => $v) {
					$row[] = array($v['gender'],
								  $v['count'],
								  number_format(($v['count'] / $total) * 100, 2) . '%');
				}
			}		
		}
		
	 	$this->to_excel_array->to_excel($row, 'age_summary_'.date("YmdHis"));
	}

	public function export_age_by_range()
	{
		$this->load->library('to_excel_array');
		$records = $this->get_demographics('age', $from, $to)['records'];
		$range = array('18-24'=>array('18','24'),'25-29'=>array('25','29'),'30-39'=>array('30','39'),'40-49+'=>array('40','200'));
        $age_ranges = array('18-24'=>0,'25-29'=>0,'30-39'=>0,'40-49+'=>0);
        $total = 0;

		foreach($records as $v){

			foreach($range as $key=>$r){
                            
		        if($v['age'] >=$r[0] && $v['age'] <= $r[1]){
		           $age_ranges[$key] += $v['count'] ;
		           break;
		        }

		    }
		    $total += $v['count'];

		}

		$row[] = array(	'Age','Count','Percentage');
        
		$percentage = 0;
		$total_percentage = 0;

		foreach($age_ranges as $key=>$r){
 			
 			$percentage = round(($r / $total) * 100, 2);
   			$row[] = array($key,$r,$percentage);

		}

		$this->to_excel_array->to_excel($row, 'age_range_summary'.date("YmdHis"));
		
	}

	private function get_demographics($type, &$from = false, &$to = false) {
		
		$query_string = $this->input->get();
		$records_arr = array();

		switch($type) {
			case 'age':
				$param['fields'] = 'COUNT(YEAR(date_of_birth)) AS count, YEAR(CURDATE()) - YEAR(date_of_birth) AS age, COUNT(YEAR(date_of_birth)) AS percentage';
				$param['table'] = 'tbl_registrants';
				$param['order_by'] = array('field'	=> $this->input->get('sort_by') ? $this->input->get('sort_by') : 'age' , 'order'	=> $this->input->get('sort_order') ? $this->input->get('sort_order') :'ASC');
				$param['group_by'] = 'age';
				$where = array();
				
				if($this->input->get('fromage') && $this->input->get('toage')) {
					$fromage = $this->input->get('fromage') > $this->input->get('toage') ?  $this->input->get('toage') :  $this->input->get('fromage');
					$toage = $this->input->get('to') < $this->input->get('fromage') ?  $this->input->get('fromage') :  $this->input->get('toage');
				 	$where['YEAR(date_of_birth) <='] = date('Y') - $fromage;
				 	$where['YEAR(date_of_birth) >='] = date('Y') - $toage;
				}

				if($this->input->get('fromdate') && $this->input->get('todate')) {
					$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
					$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
					$where['DATE(date_created) <='] = $to;
					$where['DATE(date_created) >='] = $from;
				}
				  
				if(!$this->input->get()){
					
 					$to = $this->global_model->get_row(array('table'=>'tbl_registrants','fields'=>"DATE(MAX(date_created)) as to_date "))->to_date;
 					$from = date('Y-m-d',strtotime($to.' -1 month'));
					$where['DATE(date_created) >='] = $from;
					$where['DATE(date_created) <='] = $to;

 				}

 				$query_string['to'] = $to;
 				$query_string['from'] = $from;

				$param['where'] = $where;
				$records = $this->global_model->get_rows($param)->result_array();
				$records_arr = array();
				if($records) {
					foreach ($records as $key => $value) {
						if(!$value['age'] || $value['age'] > 120) {
							$value['age'] = 'Null Values';
							$this->_has_null = true;
						}
						$records_arr[] = $value;
					}
				}

				break;
			case 'gender':

				$where = '';
				if($this->input->get('fromdate') && $this->input->get('todate')) {
					$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
					$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
					$where['DATE(date_created) <='] = $to;
					$where['DATE(date_created) >='] = $from;
				}

				if(count($this->input->get()) < 2){

					$to = $this->global_model->get_row(array('table'=>'tbl_registrants','fields'=>"DATE(MAX(date_created)) as to_date "))->to_date;
 					$from =  $this->global_model->get_row(array('table'=>'tbl_registrants','fields'=>"DATE(MIN(date_created)) as from_date "))->from_date;

 					$where['DATE(date_created) <='] = $to;
					$where['DATE(date_created) >='] = $from;

				}

				$query_string['to'] = $to;
 				$query_string['from'] = $from;

				$param['where'] = $where;
				$param['fields'] = 'COUNT(gender) AS count, gender';
				$param['table'] = 'tbl_registrants';
				$param['order_by'] = array('field'	=> $this->input->get('gender') ? $this->input->get('gender') : 'gender', 'order'=> $this->input->get('sort_order') ? $this->input->get('sort_order') : 'ASC');
				$param['group_by'] = 'gender';
				$records = $this->global_model->get_rows($param)->result_array();
				$records_arr = array();
				if($records) {
					foreach ($records as $key => $value) {
						if($value['gender'] == '') {
							$value['gender'] = 'Null Values';
							$this->_has_null = true;
						}
						$records_arr[] = $value;
					}
				}
				break;
		}
		return array('query_string'=>$query_string,'records'=>$records_arr);
	}

	 

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */