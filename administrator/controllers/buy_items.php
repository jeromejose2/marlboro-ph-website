<?php 

class Buy_items extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		$this->_table = 'tbl_buy_items';
 	}
	
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			unset($like['search']);
		}

		//$param['having'] = $like;
		$param['offset'] = $data['offset'];
		$param['limit'] = PER_PAGE;
		$param['table'] = $this->_table;
		// $param['join'] = array(
		// 	'tbl_buys as b' => 'b.buy_item_id = tbl_buy_items.buy_item_id'
		// );
		// $param['fields'] = '*,tbl_buy_items.buy_item_id as buy_item_id, tbl_buy_items.status as status, b.source, tbl_buy_items.platform_restriction as platform_restriction';
		//$param['join'] = array('tbl_challenges', 'tbl_challenges.move_forward_id = tbl_move_forward.move_forward_id'); 
		$param['order_by'] = array('field'=>'created_date','order'=>'DESC');
		$param['where'] = array('is_deleted' => 0);
		//$param['having'] = array('challenges LIKE ' => '%' . $challenge . '%', 'is_deleted' => 0);
		$param['like'] = $like;
		$data['categories'] = $this->global_model->get_rows($param)->result_array();
		$records = $this->global_model->get_total_rows($param);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/buy_items');
		$access = $this->module_model->check_access('buy_items');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = $records;

		return $this->load->view('perks/buy/index', $data, true);		
	}

	public function export() {
		$like = array();
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			unset($like['search']);
		}

		//$param['having'] = $like;
		$param['table'] = $this->_table;
		//$param['join'] = array('tbl_challenges', 'tbl_challenges.move_forward_id = tbl_move_forward.move_forward_id'); 
		$param['order_by'] = array('field'=>'created_date','order'=>'DESC');
		$param['where'] = array('is_deleted' => 0);
		//$param['having'] = array('challenges LIKE ' => '%' . $challenge . '%', 'is_deleted' => 0);
		$param['like'] = $like;
		$records = $this->global_model->get_rows($param)->result_array();
		$row[] = array(	'Buy Item Name',
						'Description',
						 'Value',
						 'Stock', 
						 'Status',
						 'Properties',
						 'Date Created');
		if($records) {
			$property_display = '';
			foreach($records as $k => $v) {
				$properties = $this->get_properties($v['buy_item_id']);
				if($properties) {
					$property_display = '';
					foreach($properties as $pk => $pv) {
						$property_display .= $pk > 1 ? "\n\n" . $pv[0]['property_name'] : $pv[0]['property_name'];
						foreach ($pv as $key => $value) {
							$property_display .= "\n" . ($key + 1) . ". " . $value['value'];
						}
					}
				}
				$row[] = array($v['buy_item_name'],
							  $v['description'],
							  $v['credit_value'],
							  $v['stock'],
							  $v['status'],
							  $property_display,
							  $v['created_date']);
			}
		}		
		$this->load->library('to_excel_array');
		$this->to_excel_array->to_excel($row, 'buy_items_'.date("YmdHis"));
	}
	
	public function add() {
		$data['main_content'] = $this->add_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function add_content() {
		$error = '';
		$value1 = $value2 = array();
		if($this->input->post('submit')) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules($this->get_rules());
			$valid = !(strtotime($this->input->post('start_date')) > strtotime($this->input->post('end_date')));
			if(!$valid) {
				$error = "Sorry, start date must come before the end date";
			} else {
				$valid = $this->form_validation->run();	
			}
			if($valid) {
				if($_POST['details']) {
					$_POST['details'] = strip_tags($_POST['details'], '<br><br/><br />');
				}
				$post = $this->input->post();
				//$post['original_slots'] = $post['slots'];
				$user = $this->login_model->extract_user_details();
				$post['created_by'] = $user['cms_user_id'];
				$post['buy_item_image'] = '';
				
				if($_FILES['photo']['name']) {
					$post['buy_item_image'] = $this->upload_image($error, $config);
					if(!$error) {
						$params = array_merge($config,array('file_path'=>$config['file_path'],'full_path'=>$config['full_path'],'file_name'=>$config['file_name']));	    
						//$this->generate_thumb($params);		
					}
				}

				if(!$error) {
					$id = $this->global_model->insert($this->_table, $post);

					if($this->input->post('media_upload')) {
						$media = $this->input->post('media_upload');
						$title = $this->input->post('media_title');
						for($i = 0; $i < count($media); $i++) {
							$param = array();
							$param['media_title'] = $title[$i];
							$param['media_content'] = $media[$i];
							$param['media_type'] = 1;
							$param['origin_id'] = PERKS_BUY;
							$param['suborigin_id'] = $id;
							$param['cms_user_id'] = $user['cms_user_id'];
							$param['media_date_created'] = date('Y-m-d H:i:s');
							$this->global_model->insert('tbl_media', $param);
						}
					}

					if($this->input->post('prop_check') == 1) {
						for($i = 1; $i <= 2; $i++) {
							if($this->input->post('property' . $i) && count($this->input->post('value' . $i)) > 0) {
								$param = array();
								$param['buy_item_id'] = $id;
								$param['property_name'] = $this->input->post('property' . $i);
								$property_id = $this->global_model->insert('tbl_properties', $param);

								foreach($this->input->post('value' . $i) as $prop_value) {
									$param = array();
									$param['property_id'] = $property_id;
									$param['value'] = $prop_value;
									$property_value_id = $this->global_model->insert('tbl_property_values', $param);

									if($i == 1) {
										$value1[] = $property_value_id;
									} else {
										$value2[] = $property_value_id;
									}
								} 	

							}
						}	
					}

					if($value1) {
						$index = 0;
						foreach ($value1 as $key => $v1) {
							if($value2) {
								foreach ($value2 as $key2 => $v2) {
									$param = array();
									$param['property_value1_id'] = $v1;
									$param['property_value2_id'] = $v2;
									$param['original_stocks'] = $param['stocks'] = $this->input->post('property_stocks')[$index];
									$param['buy_item_id'] = $id;
									$this->global_model->insert('tbl_property_stocks', $param);
									$index++;
								}
							} else {
								$param = array();
								$param['property_value1_id'] = $v1;
								$param['original_stocks'] = $param['stocks'] = $this->input->post('property_stocks')[$index];
								$param['buy_item_id'] = $id;
								$this->global_model->insert('tbl_property_stocks', $param);
								$index++;
							}
								
						}
							
					}	

					$post = array();
					$post['url'] = SITE_URL . '/buy_items/add';
					$post['description'] = 'added a new buy item';
					$post['table'] = $this->_table;
					$post['record_id'] = $id;
					$post['type'] = 'add';
					$this->module_model->save_audit_trail($post);
					redirect('buy_items');	
				}
			} else {
				$error = !$error ? validation_errors() : $error;
			}
		}
		$data['error'] = $error;
		$data['record'] = $_POST;
		$data['action'] = SITE_URL . '/buy_items/add';
		return $this->load->view('perks/buy/add', $data, true);			
	}

	public function edit() {
		$id = $this->uri->segment(3);
		$data['main_content'] = $this->edit_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function edit_content($id) {
		$error = '';
		$param['table'] = $this->_table;
		$param['where'] = array('buy_item_id'	=> $id);
		$record = (array)$this->global_model->get_row($param);
		$media = $this->get_media($id);
		$properties = $this->get_properties($id);
		$value1 = $value2 = array();
		

		$data['record'] = $_POST ? $_POST : $record;
		$data['media'] = $media;
		$data['properties'] = $properties;
		$old_content = $new_content = array();
		if($this->input->post('submit')) {
			if($media) {
				$to_remove = array();
				foreach($media as $k => $v) {
					if(!@in_array($v['media_id'], $this->input->post('media_ids'))) {
						$to_remove[] = $v['media_id'];
					}
				}
				
				# delete all removed media
				if($to_remove) {
					$where = 'media_id IN(' .implode(',', $to_remove) . ')';
					$this->global_model->delete('tbl_media', $where);	
				}
				
			
			}

			// if($properties) {
			// 	foreach ($properties as $pk => $pv) {
			// 		foreach($pv as $v) {
			// 			$where = array('property_value_id' => $v['property_value_id']);
			// 			$this->global_model->delete('tbl_property_values', $where);
			// 		}
			// 	}

			// 	$where = array('buy_item_id' => $id);
			// 	$this->global_model->delete('tbl_properties', $where);
			// }
			$this->load->library('form_validation');
			$this->form_validation->set_rules($this->get_rules());
			$valid = !(strtotime($this->input->post('start_date')) > strtotime($this->input->post('end_date')));
			if(!$valid) {
				$error = "Sorry, start date must come before the end date";
			} else {
				$valid = $this->form_validation->run();	
			}
			if($valid) {
				if($_POST['details']) {
					$_POST['details'] = strip_tags($_POST['details'], '<br><br/><br />');
				}
				$post = $this->input->post();
				$user = $this->login_model->extract_user_details();
				$post['created_by'] = $user['cms_user_id'];
				
				if($_FILES['photo']['name']) {
					$post['buy_item_image'] = $this->upload_image($error, $config);
					if(!$error) {
						$params = array_merge($config,array('file_path'=>$config['file_path'],'full_path'=>$config['full_path'],'file_name'=>$config['file_name']));	    
						//$this->generate_thumb($params);		
					}
				}
				
				if(!$error) {
					$this->global_model->update($this->_table, $post, array('buy_item_id'	=> $id));
					if($this->input->post('media_upload')) {
						$media = $this->input->post('media_upload');
						$title = $this->input->post('media_title');
						for($i = 0; $i < count($media); $i++) {
							$param = array();
							$param['media_title'] = $title[$i];
							$param['media_content'] = $media[$i];
							$param['media_type'] = 1;
							$param['origin_id'] = PERKS_BUY;
							$param['suborigin_id'] = $id;
							$param['cms_user_id'] = $user['cms_user_id'];
							$param['media_date_created'] = date('Y-m-d H:i:s');
							$this->global_model->insert('tbl_media', $param);
						}
					}

					if($this->input->post('prop_check') == 1) {
						for($i = 1; $i <= 2; $i++) {
							if($this->input->post('property' . $i) && $this->input->post('value' . $i) && count($this->input->post('value' . $i)) > 0) {
								
								if(isset($properties[$i][0]['property_id'])) {
									$property_id = $properties[$i][0]['property_id'];
									$param = array();
									$param['property_name'] = $this->input->post('property' . $i);
									$this->global_model->update_record('tbl_properties', array('property_id'	=> $property_id), $param);
								} else {
									$param = array();
									$param['buy_item_id'] = $id;
									$param['property_name'] = $this->input->post('property' . $i);
									$property_id = $this->global_model->insert('tbl_properties', $param);	
								}

								$this->remove_deleted_values($property_id, $i);
								

								foreach($this->input->post('value' . $i) as $prop_key => $prop_value) {
									if(isset($this->input->post('property_value' . $i . '_id')[$prop_key])) {
										$property_value_id = $this->input->post('property_value' . $i . '_id')[$prop_key];
										$param = array();
										$param['value'] = $prop_value;
										$this->global_model->update_record('tbl_property_values', array('property_value_id' => $property_value_id), $param);
									} else {
										$param = array();
										$param['property_id'] = $property_id;
										$param['value'] = $prop_value;
										$this->global_model->insert('tbl_property_values', $param);	
										$property_value_id = $this->db->insert_id();
									}
									
									if($i == 1) {
										$value1[] = $property_value_id;
									} else {
										$value2[] = $property_value_id;
									}
								} 	
							}
						}		
					} else {
						$this->remove_all_properties($id);
					}

					

					if($value1) {
						$index = 0;
						$this->global_model->delete('tbl_property_stocks', array('buy_item_id'=> $id));
						foreach ($value1 as $key => $v1) {
							//$existing_p_stocks = (array)$this->global_model->get_row(array('table'=>'tbl_property_stocks', 'where' => array('property_value1_id'=>$v1, 'buy_item_id'=>$id)));
							
							if($value2) {
								foreach ($value2 as $key2 => $v2) {
									//$existing_p_stocks = (array)$this->global_model->get_row(array('table'=>'tbl_property_stocks', 'where' => array('property_value1_id'=>$v1, 'property_value2_id'=>$v2, 'buy_item_id'=>$id)));
									//if(!$existing_p_stocks) {
										$param = array();
										$param['property_value1_id'] = $v1;
										$param['property_value2_id'] = $v2;
										$param['original_stocks'] = $param['stocks'] = $this->input->post('property_stocks')[$index];
										$param['buy_item_id'] = $id;
										$this->global_model->insert('tbl_property_stocks', $param);	
										$index++;
									// } else {
									// 	$param = array();
									// 	$param['property_value1_id'] = $v1;
									// 	$param['property_value2_id'] = $v2;
									// 	$param['original_stocks'] = $param['stocks'] = $this->input->post('property_stocks')[$index];
									// 	$this->global_model->update_record('tbl_property_stocks', array('property_stock_id'=>$existing_p_stocks['property_stock_id']), $param);
									// }
									
								}		
							}	
							else {
								//if(!$existing_p_stocks) {
										$param = array();
										$param['property_value1_id'] = $v1;
										$param['original_stocks'] = $param['stocks'] = $this->input->post('property_stocks')[$index];
										$param['buy_item_id'] = $id;
										$this->global_model->insert('tbl_property_stocks', $param);	
									// } else {
									// 	$param = array();
									// 	$param['property_value1_id'] = $v1;
									// 	$param['original_stocks'] = $param['stocks'] = $this->input->post('property_stocks')[$index];
									// 	$this->global_model->update_record('tbl_property_stocks', array('property_stock_id'=>$existing_p_stocks['property_stock_id']), $param);
									// }	
										$index++;
							}

							
						
							
						}
							
					}	

					$fields = array('buy_item_name', 'description', 'buy_item_image', 'start_date', 'end_date', 'stock', 'status');
					$status = array('Active', 'Inactive');
					foreach($record as $k => $v) {
						if(in_array($k, $fields)) {
							if($record[$k] != $this->input->post($k)) {
								if($k == 'status') {
									$new_content[$k] = $status[$this->input->post($k)];
									$old_content[$k] = $status[$record[$k]];
								} else {
									if($k == 'buy_item_image' && $this->input->post($k) == '')
										continue;
									$new_content[$k] = $this->input->post($k);
									$old_content[$k] = $record[$k];
								}
							}
						}
					}
					$post = array();
					$post['url'] = SITE_URL . '/buy_items/edit/' . $id;
					$post['description'] = 'updated a buy item';
					$post['table'] = $this->_table;
					$post['record_id'] = $id;
					$post['type'] = 'edit';
					$post['field_changes'] = serialize(array('old'	=> $old_content,
													  		  'new'	=> $new_content));
					$this->module_model->save_audit_trail($post);				

					redirect('buy_items');
				}
				
			} else {
				$error = !$error ? validation_errors() : $error;
			}

		}
		$data['error'] = $error;
		$data['action'] = SITE_URL . '/buy_items/edit/' . $id;
		$data['property_stocks'] = $this->get_property_stocks($id);
		return $this->load->view('perks/buy/add', $data, true);					
	}

	public function delete() {
		$table = $this->_table;
		$id = $this->uri->segment(3);
		$field = 'buy_item_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/buy_items') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$where[$field] = $id;
			$this->global_model->delete_record($table, $where);
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	private function remove_deleted_values($property_id, $i) {
		$post = $this->input->post('property_value' . $i . '_id');

		if($post) {
			$this->global_model->delete('tbl_property_values', 'property_id = ' . $property_id . ' AND property_value_id NOT IN(' .  implode(',', $post)  . ')');
		}
		
	}

	private function remove_all_properties($id) {
		$param['table'] = 'tbl_properties';
		$param['join'] = array('tbl_property_values'	=>	'tbl_property_values.property_id = tbl_properties.property_id');
		$param['where'] = array('buy_item_id'	=> $id);
		$properties = $this->global_model->get_rows($param)->result_array();

		$property_value_ids = array(0);
		if($properties) {
			foreach ($properties as $key => $value) {
				$property_value_ids[] = $value['property_value_id'];
			}
		}

		$this->global_model->delete('tbl_properties', array('buy_item_id'	=> $id));
		$this->global_model->delete('tbl_property_values', 'property_value_id IN(' . implode(',', $property_value_ids) . ')');
		$this->global_model->delete('tbl_property_stocks', array('buy_item_id'	=> $id));

	}
	
	private function get_rules() {
		$config = array(
		   array(
				 'field'   => 'buy_item_name',
				 'label'   => 'item name',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'description',
				 'label'   => 'description',
				 'rules'   => 'required'
			  )
		);
		return $config;
	}

	private function get_media($id) {
		$param['table'] = 'tbl_media';
		$param['where'] = array('suborigin_id'	=> $id, 'origin_id'	=> PERKS_BUY);
		$media = $this->global_model->get_rows($param);
		return $media->result_array();
	} 

	private function get_properties($id) {
		$param['table'] = 'tbl_properties';
		$param['join'] = array('tbl_property_values' => 'tbl_properties.property_id = tbl_property_values.property_id');
		$param['where'] = array('buy_item_id'	=> $id);
		$param['order_by'] = array('field'	=> 'tbl_property_values.property_value_id', 'order'	=> 'ASC');
		$properties = $this->global_model->get_rows($param)->result_array();
		$properties_new = array();
		if($properties) {
			$cur_property = 0;
			$property_index = 0;
			foreach ($properties as $key => $value) {
				if($cur_property != $value['property_id']) {
					$property_index++;
				}
				$properties_new[$property_index][] = $value;
				$cur_property = $value['property_id'];
			}
		}
		return $properties_new;
	} 

	private function upload_image(&$error = false, &$uploaded = false) {
		$this->load->helper(array('upload_helper', 'resize'));
		$config['upload_path'] = 'uploads/buy/';
		$config['allowed_types'] = 'gif|png|jpg';
		$config['do_upload'] = 'photo';
		$config['max_width'] = 2000;
		$config['max_height'] = 1600;
		$config['max_size'] =  8072;
		$config['file_name'] = uniqid() . '.' . end(explode('.', $_FILES['photo']['name']));
		$uploaded = upload($config);
		if(!is_array($uploaded)) {
			$error = $uploaded;
			return;
		} else {

			$params = array('new_filename' => 'main_' .$config['file_name'], 'width'=>319,'height'=>300,'source_image'=>$config['upload_path'].'/'.$uploaded['file_name'],'new_image_path'=>$config['upload_path'].'/','file_name'=>$config['file_name']);
	    	resize($params);
	    }
		return $uploaded['file_name'];
	}

	private function generate_thumb($uploaded) {
		list($width, $height) = getimagesize($uploaded['full_path']); 
		if($width < 300) {
			copy($uploaded['full_path'], $uploaded['file_path'].'thumb_'.$uploaded['file_name']);
			return;
		}

		$this->load->helper('resize');
		// $config['x'] = $this->input->post('x');
		// $config['y'] = $this->input->post('y');
		// $config['x2'] = $this->input->post('x2');
		// $config['y2'] = $this->input->post('y2');
		// $config['width'] = $this->input->post('width');
		// $config['height'] = $this->input->post('height');
		// $params = array_merge($config, $uploaded);	    
		resize($uploaded);	

	}

	public function upload_media()
	{
		$config = array();
		$path = './uploads/buy/';
		if (!is_dir($path)) {
			mkdir($path);
		}
		$path .= 'media/';
		if (!is_dir($path)) {
			mkdir($path);
		}

		$config['upload_path'] = $path;
		$config['allowed_types'] = PRODUCT_INFO_FILE_UPLOAD_TYPES;
		if (strpos($_FILES['myFile']['type'], 'image') !== false) {
			$config['max_size'] = FILE_UPLOAD_LIMIT;
		} else {
			$config['max_size']	= '20000';
		}
		$config['max_size'] = FILE_UPLOAD_LIMIT;
		$config['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['myFile']['name'], PATHINFO_EXTENSION);

		$this->load->helper('resize');
		$this->load->library('upload', $config);

		if ($this->upload->do_upload('myFile')) {
			$data = $this->upload->data();
			if ($data['is_image']) {
				resize(array(
					'width' => 150,
					'height'=> 150,
					'source_image' => $path.$config['file_name'],
					'new_image_path' => $path,
					'file_name'=> $config['file_name']
				));
				resize(array(
					'width' => 576,
					'height'=> 296,
					'source_image' => $path.$config['file_name'],
					'new_image_path' => $path,
					'file_name'=> $config['file_name']
				));
			} else {
				$realdir = realpath($path);
				if ($realdir) {
					$realdir = str_replace("\\", '/', rtrim($realdir, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR);
					$thumbfilename = 'thumb_'.$config['file_name'].'.jpg';
					exec('ffmpeg -i '.$realdir.$config['file_name'].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realdir.$thumbfilename);
					resize(array(
						'width' => 150,
						'height'=> 150,
						'source_image' => $path.$thumbfilename,
						'new_image_path' => $path,
						'file_name'=> $thumbfilename
					));
				}
			}
			//$this->output->set_output($config['file_name'].' - '.($data['is_image'] ? 1 : 2));
			$this->output->set_output($config['file_name']);
		}


	}

	private function get_property_stocks($id) {
		$param['table'] = 'tbl_property_stocks';
		$param['where'] = array('buy_item_id'	=> $id);
		$param['order_by'] = array('field' => 'property_stock_id', 'order'	=> 'asc');
		$stocks = $this->global_model->get_rows($param)->result_array();
		$stocks_arr = array();
		if($stocks) {
			foreach ($stocks as $key => $value) {
				$stocks_arr[$value['property_value1_id'] . '_' . $value['property_value2_id']] = $value['stocks'];
			}
		}
		return $stocks_arr;
	}
	
	public function _remap($method) {
		if(is_numeric($method)) 
			$this->index();
		else
			$this->{$method}();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */