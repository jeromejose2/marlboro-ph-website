<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_statements_summary extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	public function export()
	{
		$this->load->library('to_excel_array');
		$records = $this->get_demographics($from, $to);
		$row[] = $from && $to ? array('User Statements (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')') : array('Time of Day for Logins');

		$row[] = array('Statement', 'Count');
		if($records) {
			foreach($records as $k => $v) {
				$row[] = array($v['statement'],
							  $v['count']);
			}
		}
		$this->to_excel_array->to_excel($row, 'user_statements_'.date("YmdHis"));
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$data['records'] = $this->get_demographics();
		return $this->load->view('visits/user_statements', $data, true);		
	}

	private function get_demographics(&$from = false, &$to = false) {
		$where = '';
		if($this->input->get('fromdate') && $this->input->get('todate')) {
			$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
			$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
			$where .= ' AND status = 1';
			$where .= ' AND DATE(date_created) <= \'' . $to . '\'';
			$where .= ' AND DATE(date_created) >= \'' . $from . '\'';
		}
		$query = "SELECT *, (SELECT COUNT(*) FROM tbl_user_statements WHERE statement_id LIKE CONCAT('%', s.statement_id, ',%') {$where}) AS count FROM tbl_statements s WHERE s.is_deleted = 0 AND s.status = 1 ORDER BY statement_id";
		$records = $this->global_model->custom_query($query)->result_array();
		return $records;
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */