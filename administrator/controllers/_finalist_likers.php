<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Finalist_Likers extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		$this->load->model('module_model');
		$this->_table = 'tbl_finalist_likers';
		error_reporting(0);
	}
	
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		if(isset($_GET['search'])) 
		{
			$like = $this->input->get();
			unset($like['search']);
		}
		$param['like'] = $like;
		$param['offset'] = $data['offset'];
		$param['table'] = $this->_table;
		$param['order_by'] = array('field'=>'like_date_created', 'order'=>'desc');
		$param['fields'] = '*, tbl_finalist_likers.date_created as like_date_created';
		$param['join'] = array('tbl_registrants'	=>	'tbl_registrants.registrant_id = tbl_finalist_likers.registrant_id');
		$data['likers'] = $this->global_model->get_rows($param)->result_array();
		$records = $this->global_model->get_total_rows($param);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/finalist_likers');
		$access = $this->module_model->check_access('game');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = $records;
		return $this->load->view('finalists/likers/index', $data, true);		
	}
	
	public function edit() {
		$id = $this->uri->segment(3);
		$data['main_content'] = $this->edit_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function edit_content($id) {
		$error = '';
		$param['table'] = $this->_table;
		$param['where'] = array('game_id'=> $id);
		$record = (array)$this->global_model->get_row($param);
		$data['record'] = $_POST ? $_POST : $record;
		if($this->input->post('submit')) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules($this->get_rules());
			$valid = $this->form_validation->run();
			if($valid) {
				$post = $this->input->post();
				$user = $this->login_model->extract_user_details();
				if($_FILES['photo']['name']) {
					$post['image'] = $this->upload_image($error);
					$this->load->helper('resize_helper');
					$param['width'] = 400;
					$param['height'] = 400;
					$param['source_image'] = 'uploads/game/' . $post['image'];
					$param['file_name'] = $post['image'];
					$param['new_image_path'] = 'uploads/game/';
					$error = resize($param);	
				}
					
				if(!$error) {
					$this->global_model->update($this->_table, $post, array('game_id'	=> $id));	
					
					$fields = array('name', 'description', 'image', 'status', 'send_type', 'redemption_address', 'stock');
					$status = array('Unpublished', 'Published');
					$origins = $this->module_model->get_origins();
					foreach($record as $k => $v) {
						if(in_array($k, $fields)) {
							if($record[$k] != $this->input->post($k)) {
								if($k == 'status') {
									$new_content[$k] = $status[$this->input->post($k)];
									$old_content[$k] = $status[$record[$k]];
								} elseif($k == 'origin_id') {
									$new_content[$k] = $origins[$this->input->post($k)];
									$old_content[$k] = $origins[$record[$k]];
								} else {
									if($k == 'image' && $this->input->post($k) == '')
										continue;
									$new_content[$k] = $this->input->post($k);
									$old_content[$k] = $record[$k];
								}
							}
						}
					}
					$post = array();
					$post['url'] = SITE_URL . '/prize/edit/' . $id;
					$post['description'] = 'updated a prize';
					$post['table'] = $this->_table;
					$post['record_id'] = $id;
					$post['type'] = 'edit';
					$post['field_changes'] = serialize(array('old'	=> $old_content,
													  		  'new'	=> $new_content));
					$this->module_model->save_audit_trail($post);				

					redirect('game');	
				}
				
			} else {
				$error = validation_errors();
			}
		}
		$data['error'] = $error;
		$data['origins'] = $this->module_model->get_origins();
		return $this->load->view('game/add', $data, true);			
	}
	
	
	public function delete() {
		$table = $this->_table;
		$id = $this->uri->segment(3);
		$field = 'prize_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/prize') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$where[$field] = $id;
			$this->global_model->delete_record($table, $where);
		}
		redirect('prize');
	}
	
	private function get_rules() {
		$config = array(
		   array(
				 'field'   => 'name',
				 'label'   => 'game name',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'description',
				 'label'   => 'game description',
				 'rules'   => 'required'
			  ),
		    array(
				 'field'   => 'mechanics',
				 'label'   => 'game mechanics',
				 'rules'   => 'required'
			  )
		);
		return $config;
	}

	private function upload_image(&$error = false) {
		$this->load->helper('upload_helper');
		$config['upload_path'] = 'uploads/game/';
		$config['allowed_types'] = 'gif|png|jpg';
		$config['do_upload'] = 'photo';
		$config['max_width'] = 1000;
		$config['max_height'] = 800;
		$config['max_size'] =  3072;
		$config['file_name'] = uniqid() . '.' . end(explode('.', $_FILES['photo']['name']));
		$uploaded = upload($config);
		if(!is_array($uploaded))
			$error = $uploaded;
		else
			return $uploaded['file_name'];
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		else
			$this->index();
	}
}

/* End of file welcome.php */