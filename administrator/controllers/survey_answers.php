<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Survey_answers extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
 	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
 		$this->load->view('main-template', $data);
	}

	public function view($id)
	{
		$data['main_content'] = $this->view_content($id);
		$data['nav'] = $this->nav_items();
 		$this->load->view('main-template', $data);
	}

	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
		$this->load->helper(array('paging','text','file'));
 		$offset = (int)$this->input->get('per_page');
	    $limit = PER_PAGE;
	    $filters = $this->get_filters();
	    	// $this->output->enable_profiler(true);

		$access = $this->module_model->check_access('survey_answers');

		$total_rows = $this->explore_model->get_rows(array('table'=>'tbl_survey as s',
														     'fields'=>"s.*",
														     'join'=>array(
														     				array('table'=>'tbl_survey_answers as sa','on'=>'s.id=sa.survey_id')
														     		),
														     'group_by' => 'sa.survey_id'
														     )
														)->num_rows();

		$rows = $this->explore_model->get_rows(array('table'=>'tbl_survey',
												     'fields'=>"tbl_survey.*"
												     )
												);
		$data['rows'] = $rows->result();


		foreach ($data['rows'] as $key => $value) {
			$total= $this->explore_model->get_rows(array('table'=>'tbl_survey_answers',
												 		'where'=>array(
													 				'survey_id' => $value->id
													 				),
												 		'group_by' => 'tbl_survey_answers.registrant_id'
												    	)
											  		)->num_rows(); 
			$data['rows'][$key]->count = $total;
		}
		
		$data['total_rows'] = $total_rows;
		$data['header_text'] = 'Survey Answers';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['query_strings'] = $filters['query_strings'];
		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']); 

		return $this->load->view('survey/survey_answer_list',$data,TRUE);
	}


	public function get_filters()
    {
		   
		   $where_filters = array('sl.source'=>'source',
		   							'sl.points'=>'points',
		   							'DATE(sl.timestamp) >='=>'from',
		   							'DATE(sl.timestamp) <='=>'to',
		   							'p.type'=>'prize_type');
		   $like_filters = array(
		   							"CONCAT(r.first_name,' ',r.third_name) "=>'name',
		   							's.title'=>'title'
		   						);
		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
		   
	   }



	   public function export($id)
	   {

	   		$this->load->library('to_excel_array');
	   
		   	$rows = $this->explore_model->get_rows(array('table'=>'tbl_survey_answers as sa',
		   												 'where'=>array('sa.survey_id' => $id),
													     'join'=>array(
													     				array('table'=>'tbl_survey as s','on'=>'s.id=sa.survey_id'),
													     				array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=sa.registrant_id'),
													     				array('table'=>'tbl_survey_questions as sq','on'=>'sq.id=sa.question_id')
													     		),
													     'fields'=>"s.title, s.points, sa.id,r.registrant_id,r.first_name, r.middle_initial, r.third_name, r.person_id, sq.question, sa.answer, sa.timestamp",
													     'group_by' => 'sa.registrant_id'
													     )
													);

		   	$res[] = array('Person ID',
		   				'Name',
					   'Survey Form',
					   'Points Awarded',
					   'Date');

		   	if($rows->num_rows()){;
		   		
		   		foreach($rows->result() as $v){
		   		
		   			$res[] = array(
		   							$v->person_id,
		   							ucwords($v->first_name.' '.$v->third_name),
		   							$v->title,
		   							$v->points,
		   							$v->timestamp 
		   						);
		   		}

		   	} 

			$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));
	   }


	    public function view_content($id)
		{
			$this->load->helper(array('paging','text','file'));
	 		$offset = (int)$this->input->get('per_page');
		    $limit = PER_PAGE;
		    $filters = $this->get_answer_filters();
		    	// $this->output->enable_profiler(true);

			$access = $this->module_model->check_access('survey_answers');
			$total_rows = $this->explore_model->get_rows(array('table'=>'tbl_survey_answers as sa',
																 'where'=>array_merge($filters['where_filters'],array('sa.survey_id'=>$id)),
															     'like'=>$filters['like_filters'],
															     'join'=>array(
															     				array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=sa.registrant_id'),
															     				array('table'=>'tbl_survey_questions as sq','on'=>'sq.id=sa.question_id')
															     		),
															     'fields'=>"sa.id, GROUP_CONCAT(sa.answer)",
															     'group_by'=>"sa.question_id, r.registrant_id",
															     )
															    )->num_rows(); 
			$data['header_text'] = 'Survey Answers';
			$data['edit'] = $access['edit'];
			$data['delete'] = $access['delete'];
			$data['add'] = $access['add'];
			$data['total_rows'] = $total_rows;

			$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_survey_answers as sa',
																 'where'=>array_merge($filters['where_filters'],array('sa.survey_id'=>$id)),
															     'like'=>$filters['like_filters'],
															     'limit'=>$limit,
															     'offset'=>$offset,
															     'join'=>array(
															     				array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=sa.registrant_id'),
															     				array('table'=>'tbl_survey_questions as sq','on'=>'sq.id=sa.question_id')
															     		),
															     'fields'=>"sa.id,r.registrant_id,r.first_name, r.middle_initial, r.third_name, r.person_id, sq.question, sa.answer, sa.timestamp,sa.question_id, GROUP_CONCAT(sa.answer SEPARATOR ' - ') as answer, sq.options",
															     'group_by'=>"sa.question_id, r.registrant_id",
															     'order_by' => array('field'=>'sa.timestamp','order'=>'ASC'),
															     )
															);
			
			$data['query_strings'] = $filters['query_strings'];
			$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).$filters['query_strings']); 
			return $this->load->view('survey/user_answer_list',$data,TRUE);
		}

		public function get_answer_filters()
    {
		   
		   $where_filters = array('DATE(sa.timestamp) >='=>'from',
		   							'DATE(sa.timestamp) <='=>'to',);
		   $like_filters = array(
		   							"CONCAT(r.first_name,' ',r.third_name) "=>'name',
		   							'sa.answer'=>'answer',
		   							'sq.question'=>'question',
		   							'r.person_id'=>'person_id'
		   						);
		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
		   
	   }

	   public function export_answers($id)
	   {

	   		$this->load->library('to_excel_array');
	   		$filters = $this->get_answer_filters();
	   
		   	$rows = $this->explore_model->get_rows(array('table'=>'tbl_survey_answers as sa',
																 'where'=>array_merge($filters['where_filters'],array('sa.survey_id'=>$id)),
															     'like'=>$filters['like_filters'],
															     'join'=>array(
															     				array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=sa.registrant_id'),
															     				array('table'=>'tbl_survey as s','on'=>'s.id=sa.survey_id'),
															     				array('table'=>'tbl_survey_questions as sq','on'=>'sq.id=sa.question_id')
															     		),
															     'fields'=>"s.title,sa.id,r.registrant_id,r.first_name, r.middle_initial, r.third_name, r.person_id, sq.question, sa.answer, sa.timestamp,sa.question_id, GROUP_CONCAT(sa.answer SEPARATOR ' - ') as answer, sq.options",
															     'group_by'=>"sa.question_id, r.registrant_id",
															     'order_by' => array('field'=>'sa.timestamp','order'=>'ASC'),
															     )
															);

		   	$res[] = array('Person ID',
		   				'Name',
					   'Survey Form',
					   'Question',
					   'Response',
					   'Date');

		   	if($rows->num_rows()){;
		   		
		   		foreach($rows->result() as $v){
		   			
		   			if($v->options == 2){
	   					$answer = substr($v->answer, 4);;
	                    $unserialize = @unserialize($answer);

	                    if ($unserialize !== false) {
	                        $answers = unserialize($answer);
	                        $answer = array();
	                        foreach($answers as $k => $val){
	                        	$answer[] = $val;
	                        }

		   			}}else{
		   				$answer = $v->answer;
		   			}


		   			$res[] = array(
		   							$v->person_id,
		   							ucwords($v->first_name.' '.$v->third_name),
		   							$v->title,
		   							$v->question,
		   							!is_array($answer) ? $answer : implode(',', $answer),
		   							$v->timestamp 
		   						);
		   		}

		   	} 

			$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));
	   }
}