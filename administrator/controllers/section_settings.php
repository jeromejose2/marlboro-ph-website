<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Section_settings extends CI_Controller {
	
	var $error;
	var $post;

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
		//$this->output->enable_profiler(TRUE);
 	} 

 	 
 	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

 	}
 	 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
	  
		$access = $this->module_model->check_access('section_settings');
 		$data['header_text'] = 'Section Settings';
		$data['edit'] = $access['edit'];
		$data['message'] = $this->session->flashdata('message');
		$data['class'] = $this->session->flashdata('class');

 		$data['rows'] = $this->session->flashdata('post') ? json_decode(json_encode($this->session->flashdata('post')),false) : $this->explore_model->get_rows(array('table'=>'tbl_section_settings',
																			 														     'order_by' => array('field'=>'date_added','order'=>'DESC')
																			 														     )
																																	)->result();
   		return $this->load->view($this->uri->segment(1).'/form',$data,TRUE);


	}
  
	
	public function edit()
	{
   
		if($this->input->post() && $this->validate_form())
		{
			 
			$setting_section_ids = $this->input->post('setting_section_id');
			$setting_ids = $this->input->post('setting_id');
			foreach($setting_section_ids as $key=>$v){
 				$this->explore_model->update('tbl_section_settings',array('setting_section_id'=>$v),array('setting_id'=>$setting_ids[$key]));
			}

			$response = array('message'=>'Section settings has been successfully updated.','class'=>'success');
			

		}else{

 			$response = array('message'=>$this->error,'class'=>'danger');

		}

		$this->session->set_flashdata($response);
		redirect($this->router->class);

		

	}
 

	public function validate_form()
	{

		$names = $this->input->post('name');
		$setting_section_ids = $this->input->post('setting_section_id');
		$setting_ids = $this->input->post('setting_id');
		$proceed = true;
		$error = false;
		$temp = array();
		$post = array();

		foreach($setting_section_ids as $key=>$v){

			$error = false;

			if(!(int)$v){
				$proceed = false;
				$this->error = 'ID must be greater than zero.';
				$class = 'has-error';
			}

			$temp = $setting_section_ids;
			unset($temp[$key]);
			
			/*if(in_array($v, $temp)){
				$proceed = false;
				$this->error = 'Duplicate IDs found.';
				$class = 'has-error';
			}*/

			$post[$key] = array('name'=>$names[$key],'setting_id'=>$setting_ids[$key],'setting_section_id'=>$v,'class'=>$class);

		}

 		$this->session->set_flashdata(array('post'=>$post));
		return $proceed;
	}
  
 
}