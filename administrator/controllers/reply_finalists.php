<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reply_Finalists extends CI_Controller {
	
	var $_table;

	public function __construct() {
		parent::__construct();
		include_once('application/models/points_model.php');
		include_once('application/models/notification_model.php');
		$this->points_model = new Points_Model();
		$this->notification_model = new Notification_Model();
		$this->load->model('module_model');
		$this->_table = 'tbl_finalist_comment_replies';
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {

		function toSlug($string) {
			$slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
   			return strtoupper($slug);
		}

		$page = $this->uri->segment(2, 1);
		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		// if(isset($_GET['search'])) {
		// 	$like = $this->input->get();
		// 	unset($like['search']);
		// }
		// $param['like'] = $like;
		// $param['offset'] = $data['offset'];
		// $param['table'] = $this->_table;
		// // $param['where'] = array('is_deleted'=>0);
		// $param['order_by'] = array('field'=>'date_added', 'order'=>'DESC');
		// $data['records'] = $this->global_model->get_rows($param)->result_array();

		$this->db->select('tbl_finalists.primary_photo,tbl_finalists.name, tbl_finalist_comments.*, tbl_finalist_comment_replies.*, tbl_finalist_comments.comment as comment_origin, tbl_registrants.nick_name, tbl_registrants.first_name, tbl_registrants.third_name, tbl_registrants.person_id')->from($this->_table);
		$this->db->join('tbl_finalist_comments', 'tbl_finalist_comments.finalist_comment_id = tbl_finalist_comment_replies.comment_id');
		$this->db->join('tbl_registrants', 'tbl_registrants.registrant_id = tbl_finalist_comment_replies.registrant_id');
		$this->db->join('tbl_finalists', 'tbl_finalists.finalist_id = tbl_finalist_comments.finalist_id');

			if(isset($_GET['search'])) {
			
				if(isset($_GET['first_name']) && $_GET['first_name'] != '') {
				$this->db->like('concat(tbl_registrants.first_name," ",tbl_registrants.third_name)',$_GET['first_name']);
							}
				if(isset($_GET['status']) && $_GET['status'] != '') {
				$this->db->where('tbl_finalist_comment_replies.status',$_GET['status']);
							}
				if(isset($_GET['date_status_approved_to']) && $_GET['date_status_approved_to'] != '') {
				$this->db->where('DATE(tbl_finalist_comment_replies.date_added) <=',$_GET['date_status_approved_to']);
							}
				if(isset($_GET['date_status_approved_from']) && $_GET['date_status_approved_from'] != '') {
				$this->db->where('DATE(tbl_finalist_comment_replies.date_added) >=',$_GET['date_status_approved_from']);
							}
		}

		$this->db->limit(PER_PAGE, $data['offset']);

		$this->db->order_by('tbl_finalist_comment_replies.date_added', 'DESC');
		$data['records'] = $this->db->get()->result_array();

		/*for count*/
		$this->db->select('tbl_finalists.primary_photo,tbl_finalists.name, tbl_finalist_comments.*, tbl_finalist_comment_replies.*, tbl_finalist_comments.comment as comment_origin, tbl_registrants.nick_name, tbl_registrants.first_name, tbl_registrants.third_name, tbl_registrants.person_id')->from($this->_table);
		$this->db->join('tbl_finalist_comments', 'tbl_finalist_comments.finalist_comment_id = tbl_finalist_comment_replies.comment_id');
		$this->db->join('tbl_registrants', 'tbl_registrants.registrant_id = tbl_finalist_comment_replies.registrant_id');
		$this->db->join('tbl_finalists', 'tbl_finalists.finalist_id = tbl_finalist_comments.finalist_id');

			if(isset($_GET['search'])) {
			
				if(isset($_GET['first_name']) && $_GET['first_name'] != '') {
				$this->db->like('concat(tbl_registrants.first_name," ",tbl_registrants.third_name)',$_GET['first_name']);
							}
				if(isset($_GET['status']) && $_GET['status'] != '') {
				$this->db->where('tbl_finalist_comment_replies.status',$_GET['status']);
							}
				if(isset($_GET['date_status_approved_to']) && $_GET['date_status_approved_to'] != '') {
				$this->db->where('DATE(tbl_finalist_comment_replies.date_added) <=',$_GET['date_status_approved_to']);
							}
				if(isset($_GET['date_status_approved_from']) && $_GET['date_status_approved_from'] != '') {
				$this->db->where('DATE(tbl_finalist_comment_replies.date_added) >=',$_GET['date_status_approved_from']);
							}

		}
		$count = $this->db->get()->result_array();

		/*for count*/

		$records =count($count);
		$data['pagination'] = $this->global_model->pagination($records, $page, SITE_URL.'/reply_finalists');
		$access = $this->module_model->check_access('finalists');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] =$records; //count($data['records']);
		$data['origins'] = $this->module_model->get_origins();

		// echo '<Pre>';
		// print_r($data['records']);
		// exit;

		return $this->load->view('finalists_reply/index', $data, TRUE);		
	}
	
	public function add() {
		$data['main_content'] = $this->add_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function add_content() {
		$error = '';
		if($this->input->post('submit')) 
		{
			//upload all first
			$config['upload_path'] = 'uploads/finalists/photos/';
			$config['allowed_types'] = '*';
			$this->load->library('upload', $config);
			$image = 'photo';

			$photo_files = $_FILES;
			$photo_file_count = count($_FILES['photo']['name']);
			$photo_file_names = array();
			if($photo_files['photo']['name'][0]) {
				for($i = 0; $i < $photo_file_count; $i++) {
					$_FILES['photo']['name'] = $photo_files['photo']['name'][$i];
					$_FILES['photo']['type'] = $photo_files['photo']['type'][$i];
					$_FILES['photo']['tmp_name'] = $photo_files['photo']['tmp_name'][$i];
					$_FILES['photo']['error'] = $photo_files['photo']['error'][$i];
					$_FILES['photo']['size'] = $photo_files['photo']['size'][$i];

					if($this->upload->do_upload($image))
					{
						//$photo_file_names[] = base_url().'uploads/finalists/photos/' . $photo_files['photo']['name'][$i];
						$photo_file_names[] = 'uploads/finalists/photos/'.$photo_files['photo']['name'][$i];
					} else 
					{
						print_r($this->upload->display_errors());
						exit;
					}
				}
			}
						

						$photo_array = array();
						foreach ($photo_file_names as $key => $value)
						{
							if($key == $this->input->post('coverphoto')[0])
							{
								
								$photo_array['coverphoto'][] =   $value;//$this->input->post('coverphoto')[0];
								//$photo_array['url'][] =  $value;
							}else
							{
								
								$photo_array['url'][] =  $value;
								
							}
						}


			$photos = $photo_array ? serialize($photo_array) : serialize(array());

				
			$config = array();
			$config['upload_path'] = 'uploads/finalists/videos/';
			$config['allowed_types'] = '*';
			$this->upload->initialize($config);
			$realpath = realpath($config['upload_path']);
			$video = 'video';

			$video_files = $_FILES;
			$video_file_count = count($_FILES['video']['name']);
			$video_file_names = array();
			if($video_files['video']['name'][0]) {
				for ($j=0; $j < $video_file_count; $j++) { 
					$_FILES['video']['name'] = $photo_files['video']['name'][$j];
					$_FILES['video']['type'] = $photo_files['video']['type'][$j];
					$_FILES['video']['tmp_name'] = $photo_files['video']['tmp_name'][$j];
					$_FILES['video']['error'] = $photo_files['video']['error'][$j];
					$_FILES['video']['size'] = $photo_files['video']['size'][$j];

					if($this->upload->do_upload($video)) {

						$video_file_names[] =  'uploads/finalists/videos/'. $video_files['video']['name'][$j];
						$realpath = str_replace("\\", '/', rtrim($realpath, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR);
						$thumbfilename = pathinfo($video_files['video']['name'][$j], PATHINFO_FILENAME).'.jpg';
						exec('ffmpeg -i '.$realpath.$video_files['video']['name'][$j].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realpath.'thumbnail/'.$thumbfilename);
						//exec('C:\ffmpeg\ffmpeg -i '.$realpath.$video_files['video']['name'][$j].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realpath.'thumbnail/'.$thumbfilename);



					} else {
						print_r($this->upload->display_errors());
						exit;
					}
				}
			}
						

						$video_array = array();
						foreach ($video_file_names as $key => $value)
						{

								$video_array['thumbnail'][] =  'uploads/finalists/videos/thumbnail/'.$thumbfilename;
								$video_array['url'][] =  $value;
							
						}

					
			$videos = $video_array ? serialize($video_array) : serialize(array());

			$this->db->insert('tbl_finalists', array(
				'name' => $this->input->post('name'),
				'person_id' => $this->input->post('person_id'),
				'location' => $this->input->post('location'),
				'write_ups' => $this->input->post('write_ups'),
				'photos' => $photos,
				'videos' => $videos,
				'is_deleted' => 0,
				'date_created' => date('Y-m-d H:i:s')
			));

			redirect('finalists');

			// $valid = $this->validate_fields();
			// if($valid) {
			// 	$this->module_model->save_module();	
			// 	redirect('module');
			// } else {
			// 	$error = 'Sorry, module name or uri already exists';
			// }
		}
		$data['error'] = $error;
		$data['module'] = $_POST;
		return $this->load->view('finalists/add', $data, true);			
	}
	
	public function edit() {
		$id = $this->uri->segment(3);
		$data['main_content'] = $this->edit_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function edit_content($id) {
		$error = '';
		if($this->input->post('submit')) {

			// kapag wala nabago
			// coverphoto_id = ''
			
			$data  = $this->db->where('finalist_id', $id)->get('tbl_finalists')->result_array()[0];

			$config['upload_path'] = 'uploads/finalists/photos/';
			$config['allowed_types'] = '*';
			$this->load->library('upload', $config);
			$image = 'photo';

			$photo_files = $_FILES;
			$photo_file_count = count($_FILES['photo']['name']);
			$photo_file_names = array();
			if($photo_files['photo']['name'][0]) {
				for($i = 0; $i < $photo_file_count; $i++) {
					$_FILES['photo']['name'] = $photo_files['photo']['name'][$i];
					$_FILES['photo']['type'] = $photo_files['photo']['type'][$i];
					$_FILES['photo']['tmp_name'] = $photo_files['photo']['tmp_name'][$i];
					$_FILES['photo']['error'] = $photo_files['photo']['error'][$i];
					$_FILES['photo']['size'] = $photo_files['photo']['size'][$i];

					if($this->upload->do_upload($image))
					{
						//$photo_file_names[] = base_url().'uploads/finalists/photos/' . $photo_files['photo']['name'][$i];
						$photo_file_names[] = 'uploads/finalists/photos/'.$photo_files['photo']['name'][$i];
					} else 
					{
						print_r($this->upload->display_errors());
						exit;
					}
				}
			}
						

						$photo_array = array();
						$photo_array1 = array();
						if($photo_file_names)
						{
								$count = 0;
									foreach ($photo_file_names as $key => $value)
									{
										

										if($this->input->post('coverphoto_id') != '')
										{

											$remove_uploadstring = str_replace('uploads/finalists/photos/', '', $value);
													$remove_uploadstring_coverphoto = str_replace('uploads/finalists/photos/', '',$this->input->post('coverphoto_id'));

														 if($remove_uploadstring == $remove_uploadstring_coverphoto)
														 	{
														 		
														 		$photo_array['coverphoto'][] = $value;
														 	}else
														 	{
														 		$photo_array['url'][] =  $value;
														 	}


										}else
										{
											if($count == 0)
											{
												$photo_array['coverphoto'][] = $value;
											}else
											{
													$photo_array['url'][] =  $value;
											}
										}
													
										$count++;
									}

						}else
						{
							if($this->input->post('coverphoto_id') != '')
							{
											foreach (unserialize($data['photos']) as $key => $v)
											{

												
												
													foreach ($v as $k => $value) 
													{

														if($key == 'coverphoto')
														{
															$photo_array1['url'][] =  $value;
														}
														if($key == 'url')
														{
																	$remove_uploadstring = str_replace('uploads/finalists/photos/', '', $value);
																	$remove_uploadstring_coverphoto = str_replace('uploads/finalists/photos/', '',$this->input->post('coverphoto_id'));
																		 
																		 if($remove_uploadstring == $remove_uploadstring_coverphoto)
																		 	{
																		 		
																		 		$photo_array1['coverphoto'][] = $value;
																		 	}else
																		 	{
																		 		$photo_array1['url'][] =  $value;
																		 	}

														}
													}
												
											
											}
								}
						}

	if(!empty($photo_array))
	{
		$photos = serialize($photo_array);
	}elseif (!empty($photo_array1)) 
	{
		$photos = serialize($photo_array1);
	}else
	{
		$photos = $data['photos'];
	}	



			$config = array();
			$config['upload_path'] = 'uploads/finalists/videos/';
			$config['allowed_types'] = '*';
			$this->upload->initialize($config);
			$realpath = realpath($config['upload_path']);
			$video = 'video';

			$video_files = $_FILES;
			$video_file_count = count($_FILES['video']['name']);
			$video_file_names = array();
			if($video_files['video']['name'][0]) {
				for ($j=0; $j < $video_file_count; $j++) { 
					$_FILES['video']['name'] = $photo_files['video']['name'][$j];
					$_FILES['video']['type'] = $photo_files['video']['type'][$j];
					$_FILES['video']['tmp_name'] = $photo_files['video']['tmp_name'][$j];
					$_FILES['video']['error'] = $photo_files['video']['error'][$j];
					$_FILES['video']['size'] = $photo_files['video']['size'][$j];

					if($this->upload->do_upload($video)) {

						$video_file_names[] =  'uploads/finalists/videos/'. $video_files['video']['name'][$j];
						$realpath = str_replace("\\", '/', rtrim($realpath, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR);
						$thumbfilename = pathinfo($video_files['video']['name'][$j], PATHINFO_FILENAME).'.jpg';
						exec('ffmpeg -i '.$realpath.$video_files['video']['name'][$j].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realpath.'thumbnail/'.$thumbfilename);
						//exec('C:\ffmpeg\ffmpeg -i '.$realpath.$video_files['video']['name'][$j].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realpath.'thumbnail/'.$thumbfilename);



					} else {
						print_r($this->upload->display_errors());
						exit;
					}
				}
			}
						

						$video_array = array();
						if($video_file_names)
						{
								foreach ($video_file_names as $key => $value)
								{

										$video_array['thumbnail'][] =  'uploads/finalists/videos/thumbnail/'.$thumbfilename;
										$video_array['url'][] =  $value;
									
								}
						}

					
			$videos = $video_array ? serialize($video_array) : $data['videos'];

			$this->db->update('tbl_finalists', array(
				'name' => $this->input->post('name'),
				'person_id' => $this->input->post('person_id'),
				'location' => $this->input->post('location'),
				'write_ups' => $this->input->post('write_ups'),
				'photos' => $photos,
				'videos' => $videos
			), array('finalist_id' => $this->input->post('id')));
			redirect('finalists');
		}
		$data['error'] = $error;
		// $data['module'] = $_POST ? $_POST : $this->module_model->get_module(array('module_id'	=> $id))[0];
		
		$data['record'] = $this->db->where('finalist_id', $id)->get('tbl_finalists')->result_array()[0];
		return $this->load->view('finalists/edit', $data, true);
	}
	
	
	public function validate() {
		$valid = $this->validate_fields();
		$data['main_content'] = $valid ? '1' : '0';
		$this->load->view('blank-template', $data); 
	}
	
	private function validate_fields() {
		$name = $this->input->post('module_name');
		$uri = $this->input->post('uri');
		$id = $this->input->post('id');
		$where['module_name'] = $name;
		$where['uri'] = $uri;
		$module = $this->module_model->get_module($where);
		if($module) {
			if($id && $module[0]['module_id'] == $id)
				return true;
			return false;
		}
		
		return true;
	}
	
	public function delete() 
	{
		//$this->db->delete('tbl_finalists', array('finalist_id' => $this->uri->segment(3)));

		$this->db->update('tbl_finalists', array(
				'is_deleted' =>1
			), array('finalist_id' => $this->uri->segment(3)));

		redirect('finalists');
		// $table = 'tbl_modules';
		// $id = $this->uri->segment(3);
		// $field = 'module_id';
		// if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/module') >= 0) {
		// 	$where[$field] = $id;
		// 	$this->global_model->delete_record($table, $where);
		// }
		// redirect('module');
	}

	private function update_status($type, $table, $new_status, $where, $params = FALSE)
	{
		$return = false;
		switch ($type) {
			case 'comment':
				$comment_status = (array)$this->db->select('status')->from($table)->where('finalist_comment_reply_id', $where)->get()->row();
				if($comment_status) {
					if($comment_status['status'] == 0 || $comment_status['status'] == '0') {
						$this->db->update($table, array('status'=>$new_status, 'date_status_changed'=>date('Y-m-d H:i:s')), array('finalist_comment_reply_id'=>$where));		
						$return = true;
					}
				}				
				break;

			case 'reply':
				#code///
				break;
			
			default:
				return false;
				break;
		}
		return $return;
	}

	public function changeStatus()
	{
		if($_POST && $_POST['action']) {
			$id = isset($_POST['id']) ? $_POST['id'] : 0;
			if($_POST['action'] == 'approve' || $_POST['action'] == 'reject') {
				$validate_comment = (array) $this->db->select()->from('tbl_finalist_comment_replies')->where('finalist_comment_reply_id', $id)->get()->row();
				if( ! $validate_comment) {
					$this->session->set_userdata('error', 'Sorry, reply is already deleted.');
					if($_POST['action'] == 'reject' || $_POST['action'] == 'multiple-reject') {
						redirect('reply_finalists');
					} else {
						return false;	
					}
				}

				if($validate_comment['status'] != 0) {
					$err_msg = $validate_comment['status'] == 1 ? "approved" : "rejected";
					$this->session->set_userdata('error', 'Sorry, reply is already '.$err_msg);
					if($_POST['action'] == 'reject' || $_POST['action'] == 'multiple-reject') {
						redirect('reply_finalists');
					} else {
						return false;	
					}	
				}
			}

			$registrant = @$this->db->select()->from('tbl_finalist_comment_replies')->where('finalist_comment_reply_id', $_POST['id'])->get()->row();
			// $id = isset($_POST['id']) ? $_POST['id'] : NULL;
			if($registrant) {
				$registrant = $registrant->registrant_id;
			} else {
				if($_POST['action'] != 'multiple-approve' AND $_POST['action'] != 'multiple-reject') {
					echo "Registrant could not be found";
					return false;	
				}				
			}#the reply on your comment on Crossover Finalist has been approved.
			switch ($_POST['action']) {
				case 'approve':
					$validate = $this->update_status('comment', 'tbl_finalist_comment_replies', 1, $id);

					if($validate) {
						$commentor = 0;
						$reg = (array) $this->db->select('comment_id')->from('tbl_finalist_comment_replies')->where('finalist_comment_reply_id', $id)->get()->row();
						if($reg) {
							$reg = (array) $this->db->select('registrant_id')->from('tbl_finalist_comments')->where('finalist_comment_id', $reg['comment_id'])->get()->row();
							$commentor = $reg['registrant_id'];
						}

						if($commentor != $registrant) {
							$points = $this->points_model->earn(COMMENT_REPLY_RECEIVE, array(
								'suborigin'=>$id,
								'registrant'=>$registrant
								// 'remarks'=>$this->get_remarks(FINALIST, $id)
							));

							$this->notification_model->notify($registrant, COMMENT_REPLY_RECEIVE, array(
								'message'=>$this->get_notification_copy(FINALIST, $id, 1, $points),
								'suborigin'=>$id
							));

							if($reg) {
								$points = $this->points_model->earn(COMMENT_REPLY_RECEIVE, array(
									'suborigin'=>$id,
									'registrant'=>$reg['registrant_id']
									// 'remarks'=>$this->get_remarks(FINALIST, $id)
								));

								$this->notification_model->notify($reg['registrant_id'], COMMENT_REPLY, array(
									'message'=>$this->get_notification_copy(FINALIST, $id, 1, $points, 1),
									'suborigin'=>$id
								));
							}
						}						
					}
					break;

				case 'multiple-approve':
					if($_POST['ids']) {
						foreach($_POST['ids'] as $k => $v) {
							$id = $v;
							$registrant_d = $this->db->select()->from('tbl_finalist_comment_replies')->where('finalist_comment_reply_id', $id)->get()->row();
							$registrant = $registrant_d->registrant_id;
							if($registrant_d->status == 0) {
								$commentor = 0;
								$reg = (array) $this->db->select('comment_id')->from('tbl_finalist_comment_replies')->where('finalist_comment_reply_id', $id)->get()->row();
								if($reg) {
									$reg = (array) $this->db->select('registrant_id')->from('tbl_finalist_comments')->where('finalist_comment_id', $reg['comment_id'])->get()->row();
									$commentor = $reg['registrant_id'];
								}
								$validate = $this->update_status('comment', 'tbl_finalist_comment_replies', 1, $id);

								if($validate) {
									if($commentor != $registrant) {
										$points = $this->points_model->earn(COMMENT_REPLY_RECEIVE, array(
											'suborigin'=>$id,
											'registrant'=>$registrant,
											'remarks'=>$this->get_remarks(FINALIST, $id)
										));

										$this->notification_model->notify($registrant, COMMENT_REPLY_RECEIVE, array(
											'message'=>$this->get_notification_copy(FINALIST, $id, 1, $points),
											'suborigin'=>$id
										));

										if($reg) {
											$points = $this->points_model->earn(COMMENT_REPLY_RECEIVE, array(
												'suborigin'=>$id,
												'registrant'=>$reg['registrant_id']
												// 'remarks'=>$this->get_remarks(FINALIST, $id)
											));

											$this->notification_model->notify($reg['registrant_id'], COMMENT_REPLY, array(
												'message'=>$this->get_notification_copy(FINALIST, $id, 1, $points, 1),
												'suborigin'=>$id
											));
										}
									}
								}
							}							
						}
					}
					break;

				case 'multiple-reject':
					if($_POST['ids']) {
						foreach($_POST['ids'] as $k => $v) {
							$id = $v;
							$registrant_d = $this->db->select()->from('tbl_finalist_comment_replies')->where('finalist_comment_reply_id', $id)->get()->row();
							$registrant = $registrant_d->registrant_id;
							if($registrant_d->status == 0) {
								// $points = $this->points_model->deactivate($registrant, array(
								// 	array(
								// 		'origin'=>COMMENT,
								// 		'suborigin'=>$id
								// 	)
								// ));
								$validate = $this->update_status('comment', 'tbl_finalist_comment_replies', 2, $id);

								if($validate) {
									$this->notification_model->notify($registrant, COMMENT_REPLY_RECEIVE, array(
										'message'=>$this->get_notification_copy(FINALIST, $id, 0, 0, false, $message = isset($_POST['message']) ? $_POST['message'] : NULL),
										'suborigin'=>$id
									));
								}
							}
						}
						redirect($_SERVER['HTTP_REFERER']);
					}
					break;

				case 'reject':
					// $points = $this->points_model->deactivate($registrant, array(
					// 	array(
					// 		'origin'=>COMMENT,
					// 		'suborigin'=>$id
					// 	)
					// ));
					$validate = $this->update_status('comment', 'tbl_finalist_comment_replies', 2, $id);

					if($validate) {
						$this->notification_model->notify($registrant, COMMENT_REPLY_RECEIVE, array(
							'message'=>$this->get_notification_copy(FINALIST, $id, 0, 0, false, $message = isset($_POST['message']) ? $_POST['message'] : NULL),
							'suborigin'=>$id
						));
					}
					break;
				
				default:
					$this->db->update($this->_table, array('status'=>2), array('finalist_comment_reply_id'=>$_POST['id']));
					break;
			}
			if($_POST['action'] == 'reject' || $_POST['action'] == 'multiple-reject') {
				redirect($_SERVER['HTTP_REFERER']);
			} else {
				echo $_POST['action'];
			}
		}
	}

	private function get_remarks($origin_id, $suborigin_id) {
		if($origin_id == MOVE_FWD) {
			$param['table'] = 'tbl_move_forward';
			$param['where'] = array('move_forward_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			return '{{ name }} commented on ' . $record['move_forward_title'] . ' on MoveFWD';
		// } elseif($origin_id == ABOUT_PHOTOS) {
		// 	$param['table'] = 'tbl_about_events_photos';
		// 	$param['join'] = array('tbl_about_events' => 'tbl_about_events_photos.about_event_id = tbl_about_events.about_event_id');
		// 	$param['where'] = array('photo_id' => $suborigin_id);
		// 	$record = (array)$this->global_model->get_row($param);
		// 	return '{{ name }} commented on ' . $record['title'] . ' on ' . $record['type'];
		// 
		} elseif($origin_id == ABOUT_VIDEOS) {
			$param['table'] = 'tbl_about_videos';
			$param['where'] = array('about_video_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			return '{{ name }} commented on ' . $record['title'] . ' on About';
		} elseif($origin_id == ABOUT_NEWS) {
			$param['table'] = 'tbl_about_news';
			$param['where'] = array('about_news_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			return '{{ name }} commented on ' . $record['title'] . ' on About';
		} elseif($origin_id == BACKSTAGE_PHOTOS) {
			$param['table'] = 'tbl_backstage_events_photos';
			$param['join'] = array('tbl_backstage_events' => 'tbl_backstage_events_photos.backstage_event_id = tbl_backstage_events.backstage_event_id');
			$param['where'] = array('photo_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			return '{{ name }} commented on ' . $record['title'] . ' on Backstage Pass';
		} elseif($origin_id == FINALIST) {
			$param['table'] = 'tbl_finalist_comment_replies';
			$param['where'] = array('finalist_comment_reply_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			$record = (array)$this->db->select()->from('tbl_finalists')->where('finalist_id', $record['finalist_id'])->get()->row();
			return '{{ name }} commented on ' . $record['name'] . ' on Finalist';
		}
	}

	private function get_notification_copy($origin_id, $suborigin_id, $status = 1, $points, $is_recipient = false, $message = '', $extra = false) {
		$prepend_copy = !$is_recipient ? 'Your reply on ' : 'The reply on your comment under ';
		$disapproved_append_copy = " Please review the Terms and Conditions for more information: <a href=\"javascript:void(0)\" onclick=\"popup.open({url:'".BASE_URL."popups/terms.html'})\">Terms and Conditions</a>.";
		if($origin_id == MOVE_FWD) {
			$param['table'] = 'tbl_move_forward';
			$param['where'] = array('move_forward_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			if($status == 1) 
				return $prepend_copy . $record['move_forward_title'] . ' has been approved. You gained ' . $points . ' points. Thank you!';
			else 
				return $prepend_copy . $record['move_forward_title'] . ' has been disapproved because ' .  $message . '.' . $disapproved_append_copy;

		} elseif($origin_id == MOVE_FWD_GALLERY) {
			$param['table'] = 'tbl_move_forward_gallery';
			$param['where'] = array('move_forward_gallery_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			if($status == 1) 
				return $prepend_copy . ' a MoveFWD entry has been approved. You gained ' . $points . ' points. Thank you!';
			else 
				return $prepend_copy . ' a MoveFWD entry has been disapproved because ' . $message . '.' . $disapproved_append_copy;

		// } elseif($origin_id == ABOUT_PHOTOS) {
		// 	$param['table'] = 'tbl_about_events_photos';
		// 	$param['join'] = array('tbl_about_events' => 'tbl_about_events_photos.about_event_id = tbl_about_events.about_event_id');
		// 	$param['where'] = array('photo_id' => $suborigin_id);
		// 	$record = (array)$this->global_model->get_row($param);
		// 	if($status == 1) 
		// 		return $prepend_copy . $record['title'] . ' on ' . $record['type'] . ' has been approved. You gained ' . $points . ' points. Thank you!';
		// 	else 
		// 		return $prepend_copy . $record['title'] . ' on ' . $record['type'] . ' has been disapproved. Please try again!';
		// 
		} elseif($origin_id == ABOUT_VIDEOS) {
			$param['table'] = 'tbl_about_videos';
			$param['where'] = array('about_video_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			if($status == 1) 
				return $prepend_copy . $record['title'] . ' has been approved. You gained ' . $points . ' points. Thank you!';
			else 
				return $prepend_copy . $record['title'] . ' has been disapproved because ' . $message . '.' . $disapproved_append_copy;
		} elseif($origin_id == ABOUT_NEWS) {
			$param['table'] = 'tbl_about_news';
			$param['where'] = array('about_news_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			if($status == 1) 
				return $prepend_copy . $record['title'] . ' has been approved. You gained ' . $points . ' points. Thank you!';
			else 
				return $prepend_copy . $record['title'] . ' has been disapproved because ' . $message . '.' . $disapproved_append_copy;
		} elseif($origin_id == BACKSTAGE_PHOTOS) {
			$param['table'] = 'tbl_backstage_events_photos';
			$param['join'] = array('tbl_backstage_events' => 'tbl_backstage_events_photos.backstage_event_id = tbl_backstage_events.backstage_event_id');
			$param['where'] = array('photo_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			if($status == 1) 
				return $prepend_copy . $record['title'] . ' has been approved. You gained ' . $points . ' points. Thank you!';
			else 
				return $prepend_copy . $record['title'] . ' has been disapproved because ' . $message . '.' . $disapproved_append_copy;
		} elseif($origin_id == BACKSTAGE_VIDEOS) {
			$param['table'] = 'tbl_backstage_videos';
			$param['where'] = array('about_video_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			if($status == 1) 
				return $prepend_copy . $record['title'] . ' has been approved. You gained ' . $points . ' points. Thank you!';
			else 
				return $prepend_copy . $record['title'] . ' has been disapproved because ' . $message . '.' . $disapproved_append_copy;
		} elseif($origin_id == FINALIST) {
			$param['table'] = 'tbl_finalist_comment_replies';
			$param['where'] = array('finalist_comment_reply_id' => $suborigin_id);
			$record = (array)$this->global_model->get_row($param);
			$record = (array)$this->db->select()->from('tbl_finalists')->where('finalist_id', $record['finalist_id'])->get()->row();
			if($status == 1) {
				if($points > 0) {
					return $prepend_copy . $record['name'] . ' (Crossover Finalist) has been approved. You gained ' . $points . ' points. Thank you!';
				} else {
					return $prepend_copy . $record['name'] . ' (Crossover Finalist) has been approved. Thank you!';
				}
			} else {
				if($message) {
					return $prepend_copy . $record['name'] . ' (Crossover Finalist) has been disapproved because ' . $message . '.' . $disapproved_append_copy;
				} else {
					return $prepend_copy . $record['name'] . ' (Crossover Finalist) has been disapproved.' . $disapproved_append_copy;
				}
			}
		}
	}

	// public function changeStatus()
	// {
	// 	if($_POST && $_POST['action']) {
	// 		switch ($_POST['action']) {
	// 			case 'approve':
	// 				$this->db->update($this->_table, array('status'=>1), array('finalist_comment_reply_id'=>$_POST['id']));
	// 				break;
				
	// 			default:
	// 				$this->db->update($this->_table, array('status'=>2), array('finalist_comment_reply_id'=>$_POST['id']));
	// 				break;
	// 		}
	// 		echo $_POST['action'];
	// 	}
	// }

	public function export() {
		// $where = '1';
		// if(isset($_GET['search'])) {
		// 	//print_r($_GET);
		// 	//die();
		// 	$where .=  " AND CONCAT(first_name, ' ' , third_name) LIKE '%$_GET[first_name]%'";
		// 	$where .= isset($_GET['nick_name']) ? " AND nick_name LIKE '%$_GET[nick_name]%'" : "";
		// 	$where .= isset($_GET['comment_reply']) ? " AND comment_reply LIKE '%$_GET[comment_reply]%'" : "";
		// 	$where .= isset($_GET['title']) ? " AND ((gtitle LIKE '%$_GET[title]%' AND origin_id = " . MOVE_FWD_GALLERY . ") OR (move_forward_title LIKE '%$_GET[title]%' AND origin_id = " . MOVE_FWD . ") OR (ntitle LIKE '%$_GET[title]%' AND origin_id = " . ABOUT_NEWS . ") OR (btitle LIKE '%$_GET[title]%' AND origin_id = " . BACKSTAGE_PHOTOS . ") OR (vtitle LIKE '%$_GET[title]%' AND origin_id = " . ABOUT_VIDEOS . "))" : "";
		// 	$where .= $_GET['fromavail'] ? " AND DATE(comment_status_changed) >= '$_GET[fromavail]'" : "";
		// 	$where .= $_GET['toavail'] ? " AND DATE(comment_status_changed) <= '$_GET[toavail]'" : "";
		// 	$where .= $_GET['comment_reply_status'] != '' ? " AND comment_reply_status = '$_GET[comment_reply_status]'" : "";
		// 	$where .= isset($_GET['origin_id']) != '' ? " AND origin_id = '$_GET[origin_id]'" : "";
		
		// }
		// $param['limit'] = PER_PAGE;
		// $query = "SELECT * FROM {$this->_table} LEFT JOIN vw_comments ON vw_comments.comment_id = {$this->_table}.comment_id WHERE {$where}";
		// $comments = $this->global_model->custom_query($query)->result_array();

		// $comments_arr = array();
		// if($comments) {
		// 	foreach ($comments as $key => $value) {
		// 		$value['item_title'] = $this->get_title($value['origin_id'], $value);
		// 		$value['url'] = $this->get_url($value['origin_id'], $value['suborigin_id']);
		// 		$comments_arr[] = $value;
		// 	}
		// }


		$this->db->select('tbl_finalists.primary_photo,tbl_finalists.name, tbl_finalist_comments.*, tbl_finalist_comment_replies.*, tbl_finalist_comments.comment as comment_origin, tbl_registrants.nick_name, tbl_registrants.first_name, tbl_registrants.third_name, tbl_registrants.person_id')->from($this->_table);
		$this->db->join('tbl_finalist_comments', 'tbl_finalist_comments.finalist_comment_id = tbl_finalist_comment_replies.comment_id');
		$this->db->join('tbl_registrants', 'tbl_registrants.registrant_id = tbl_finalist_comment_replies.registrant_id');
		$this->db->join('tbl_finalists', 'tbl_finalists.finalist_id = tbl_finalist_comments.finalist_id');

			if(isset($_GET['search'])) {
			
				if(isset($_GET['first_name']) && $_GET['first_name'] != '') {
				$this->db->like('concat(tbl_registrants.first_name," ",tbl_registrants.third_name)',$_GET['first_name']);
							}
				if(isset($_GET['status']) && $_GET['status'] != '') {
				$this->db->where('tbl_finalist_comment_replies.status',$_GET['status']);
							}
				if(isset($_GET['date_status_approved_to']) && $_GET['date_status_approved_to'] != '') {
				$this->db->where('DATE(tbl_finalist_comment_replies.date_added) <=',$_GET['date_status_approved_to']);
							}
				if(isset($_GET['date_status_approved_from']) && $_GET['date_status_approved_from'] != '') {
				$this->db->where('DATE(tbl_finalist_comment_replies.date_added) >=',$_GET['date_status_approved_from']);
							}


			// $where['DATE(date_created) <='] = $to;
 			// $where['DATE(date_created) >='] = $from;


		}


		// $this->db->order_by('date_added', 'DESC');
		$data['records'] = $this->db->get()->result_array();
		
		$records = $data['records'];
		$status = array('Pending', 'Approved', 'Disapproved');
		$row[] = array(	'Name',
						'Comment',
						 'Finalist Name',
						 // 'URL', 
						 'Status',
						 'Date Created');
		if($records) {
				foreach($records as $k => $v) {

					switch ($v['status']) {
                              case 2:
                                $stats =  'Rejected';
                                break;

                              case 1:
                               $stats =  'Approved';
                                break;
                              
                              default:
                                $stats =  'Pending';
                                break;

								}
					$row[] = array($v['first_name'] . ' ' . $v['third_name'],
								  $v['comment'],
								  $v['name'],
								  // $v['url'],
								$stats,
								  $v['date_added']);
				}
			}		
		$this->load->library('to_excel_array');
		$this->to_excel_array->to_excel($row, 'finalist_reply_comment_'.date("YmdHis"));
	}

	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		elseif($method == 'changeStatus')
			$this->changeStatus();
		elseif($method == 'export')
		$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */