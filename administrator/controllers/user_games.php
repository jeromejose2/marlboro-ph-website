<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class User_games extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');

 	} 

 	 
 	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
		//$this->output->enable_profiler(TRUE);

 	}
 	 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
		$this->load->helper('paging');
		$offset = (int)$this->input->get('per_page');
	    $limit = PER_PAGE;
	    $filters = $this->get_filters(); 

		$access = $this->module_model->check_access('user_games');
		$total_rows = $this->global_model->get_rows(array('table'=>'tbl_page_visits as v',
															 'where'=>array_merge($filters['where_filters'],array()),
														     'like'=>$filters['like_filters'],
														     'join'=>array('tbl_login'=>'tbl_login.login_id=v.login_id', 
														     				'tbl_registrants' => 'tbl_login.registrant_id=tbl_registrants.registrant_id'),
 														     )
														)->num_rows();
 		$data['header_text'] = 'User Points';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = $total_rows;
		$data['query_strings'] = $filters['query_strings'];
		$field = $this->input->get('field') ? $this->input->get('field') : 'date_visited';
		$sort = $this->input->get('sort') ? $this->input->get('sort') : 'desc';
		$data['games'] = $this->global_model->get_rows(array('table'=>'tbl_page_visits as v',
															 'where'=>array_merge($filters['where_filters'],array()),
														     'like'=>$filters['like_filters'],
														     'join'=>array('tbl_login'=>'tbl_login.login_id=v.login_id', 
														     				'tbl_registrants' => 'tbl_login.registrant_id=tbl_registrants.registrant_id'),
 														     'offset'=>$offset,
														     'limit'=>$limit,
														     'order_by'=>array('field'=>$field,'order'=>$sort),
														     'fields'=>'v.*,tbl_registrants.first_name,tbl_registrants.third_name,tbl_registrants.registrant_id')
														)->result_array();
		$data['game_names'] = $this->global_model->get_rows(array('table'=>'tbl_games')
														)->result_array();
  		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']);
 		return $this->load->view('game/user',$data,TRUE);
	} 

	

	public function get_filters()
	   {
		   
		   $where_filters = array('DATE(date_visited) >='=>'fromdate','DATE(date_visited) <='=>'todate','registrant_id'=>'registrant_id');
		   $like_filters = array("CONCAT(first_name,' ',third_name)"=>'name', 'uri'	=> 'uri');
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   $valid_where_filters['uri !='] = 'games';
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 				}
									 
				  
 		   } 

		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));

 				}

 				if($column_field == 'uri' && !$this->input->get($filter)) {
 					$valid_like_filters[$column_field] = 'games';	
				}
					
				 
				   
		   } 

 		   $query_strings = $this->input->get() ? http_build_query($this->input->get()) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?' . $query_strings);		   
		   
	   } 


	   public function export()
	   {
	   		$this->load->library('to_excel_array');

		   	$filters = $this->get_filters();
		   	$field = $this->input->get('field') ? $this->input->get('field') : 'date_visited';
			$sort = $this->input->get('sort') ? $this->input->get('sort') : 'desc';
		   	$query = $this->global_model->get_rows(array('table'=>'tbl_page_visits as v',
															 'where'=>array_merge($filters['where_filters'],array()),
														     'like'=>$filters['like_filters'],
														     'join'=>array('tbl_login'=>'tbl_login.login_id=v.login_id', 
														     				'tbl_registrants' => 'tbl_login.registrant_id=tbl_registrants.registrant_id'),
 														     'order'=>array('field'=>$field,'order'=>$sort),
														     'fields'=>'v.*,tbl_registrants.first_name,tbl_registrants.third_name')
														);
		   	$res[] = array('Name','Game', 'Date');

		   	if($query->num_rows()){
		   		foreach($query->result() as $v){
 		   			$res[] = array($v->first_name.' '.$v->third_name,strtoupper(str_replace('games/', '',$v->uri)), $v->date_visited);
		   		}
		   	}
		   	$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));

	   }

	   private function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
			    $sort_col = array();
			    foreach ($arr as $key=> $row) {
			        $sort_col[$key] = $row[$col];
			    }

		    array_multisort($sort_col, $dir, $arr);
		}

		private function get_average($arr) {
			 $total = 0;
			 if($arr) {
			 	foreach ($arr as $key => $value) {
			 		$total += $value['points'];
			 	}
			 }
			 if($total == 0)
			 	return 0;
			 return number_format($total / count($arr), 2);
		}

 
}
