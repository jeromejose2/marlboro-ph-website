<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class User_flash_offers extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
 	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
 		$this->load->view('main-template', $data);
	}

	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
		$this->load->helper(array('paging','text','file'));
 		$offset = (int)$this->input->get('per_page');
 		//$this->output->enable_profiler(true);
	    $limit = PER_PAGE;
	    $filters = $this->get_filters();

		$access = $this->module_model->check_access('user_flash_offers');
		$total_rows = $this->explore_model->get_rows(array('table'=>'tbl_user_flash_prizes as u',
															 'where'=>array_merge($filters['where_filters'],array()),
														     'like'=>$filters['like_filters'],
														     'join'=>array(array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=u.registrant_id'),
														     			array('table'=>'tbl_flash_offers as p','on'=>'p.prize_id=u.prize_id')),
														     'fields'=>"u.prize_id"
														     )
														    )->num_rows(); 
		$data['header_text'] = 'User Flash Prizes';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total_rows'] = $total_rows;

		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_user_flash_prizes as u',
															 'where'=>array_merge($filters['where_filters'],array()),
														     'like'=>$filters['like_filters'],
														     'limit'=>$limit,
														     'offset'=>$offset,
														     'join'=>array(array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=u.registrant_id'),
														     			array('table'=>'tbl_flash_offers as p','on'=>'p.prize_id=u.prize_id')),
														     'fields'=>"u.user_flash_prize_id,r.registrant_id,r.first_name, r.middle_initial, r.third_name, p.prize_name,p.media,u.prize_status,p.send_type,p.type as prize_type,u.date_created,u.date_claimed, r.person_id"
														     )
														);
		$data['query_strings'] = $filters['query_strings'];
		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']); 
		return $this->load->view($this->uri->segment(1).'/list',$data,TRUE);
	}



	public function edit()
	{

		$id = $this->uri->segment(3);
 		$row = $this->explore_model->get_rows(array('table'=>'tbl_user_flash_prizes as u',
													'where'=>array('u.user_flash_prize_id'=>$id),
													'join'=>array(array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=u.registrant_id'),
																array('table'=>'tbl_flash_offers as o','on'=>'o.prize_id=u.prize_id')),
													'fields'=>'u.*,r.first_name, r.middle_initial, r.third_name, o.prize_name, o.description, o.send_type, o.redemption_address, o.redemption_instruction, o.type as media_type,o.media'
 												   )
											)->row();		
 
		if($this->input->post() && $row){

 			$statuses = array('0'=>'Pending','1'=>'Delivered','2'=>'Redeemed');
 			$prize_status = $this->input->post('prize_status');
			$new_content['prize_status'] = $this->input->post('prize_status');
			$old_content['prize_status'] = $row->prize_status;

			$new_status = $statuses[$prize_status];
			$current_status = $statuses[$row->prize_status];
 
			$post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/edit/' . $id;
			$post['description'] = 'updated the user\'s flash prize from '.$current_status.' to '.$new_status.' status.';
			$post['table'] = 'tbl_user_flash_prizes';
			$post['record_id'] = $id;
			$post['type'] = 'edit';
			$post['field_changes'] = serialize(array('old'	=> $old_content,'new'	=> $new_content));
			$this->module_model->save_audit_trail($post);
			$this->explore_model->update('tbl_user_flash_prizes',array('prize_status'=>$prize_status,'date_modified'=> array(array('field'=>'date_claimed','value'=>date('Y-m-d H:i:s')))),array('user_flash_prize_id'=>$id));
			redirect($this->router->class);	
  
		}else{
			$this->load->helper('file');

			$data['header_text'] = 'User Flash Prizes';
			$data['row'] = $row;
			$data['id'] = $id;
			$data['main_content'] = $this->load->view('user_flash_offers/form',$data,TRUE);
			$data['nav'] = $this->nav_items();
	 		$this->load->view('main-template',$data);
		}

 
					
 
	}


	  

	public function get_filters()
	   {
		   
		   $where_filters = array('u.prize_status'=>'prize_status',
		   							'p.send_type'=>'send_type',
		   							'DATE(u.date_created) >='=>'fromavail',
		   							'DATE(u.date_created) <='=>'toavail',
		   							'DATE(u.date_claimed) >='=>'fromredeem',
		   							'DATE(u.date_claimed) <='=>'toredeem',
		   							'p.type'=>'prize_type');
		   $like_filters = array("CONCAT(r.first_name,' ',r.third_name) "=>'name','p.prize_name'=>'prize_name');
		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
		   
	   }

	   public function export()
	   {

	   		$this->load->library('to_excel_array');
	   		$filters = $this->get_filters();
	   				   	$rows = $this->explore_model->get_rows(array('table'=>'tbl_user_flash_prizes as u',
																 'where'=>array_merge($filters['where_filters'],array()),
															     'like'=>$filters['like_filters'],
															     'join'=>array(array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=u.registrant_id'),
															     			array('table'=>'tbl_flash_offers as p','on'=>'p.prize_id=u.prize_id'),
															     			array('table'=>'tbl_provinces as pr','on'=>'pr.province_id=r.province','type'=>'left'),
															     			array('table'=>'tbl_cities as c','on'=>'c.city_id=r.city','type'=>'left')
															     			),
															     'fields'=>"r.person_id, r.first_name, r.third_name, r.street_name, r.barangay, r.mobile_phone, pr.province as provincename, c.city as cityname, p.send_type, p.prize_name,u.prize_status,p.description as prize_description,p.type as prize_type,u.date_created,u.date_claimed, u.date_created as date_availed"
															     )
															);
	   				   	
		   	// $rows = $this->explore_model->get_rows(array('table'=>'tbl_user_flash_prizes as u',
						// 										 'where'=>array_merge($filters['where_filters'],array()),
						// 									     'like'=>$filters['like_filters'],
						// 									     'join'=>array(array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=u.registrant_id'),
						// 									     			array('table'=>'tbl_flash_offers as p','on'=>'p.prize_id=u.prize_id'),
						// 									     			array('table'=>'tbl_provinces as pr','on'=>'pr.province_id=r.province','type'=>'left'),
						// 									     			array('table'=>'tbl_cities as c','on'=>'c.city_id=r.city','type'=>'left'),
						// 									     			array('table'=>'tbl_user_flash_prizes as fp','on'=>'fp.registrant_id = r.registrant_id','type'=>'left')),
						// 									     'fields'=>"r.person_id, r.first_name, r.third_name, r.street_name, r.barangay, r.mobile_phone, pr.province as provincename, c.city as cityname, p.send_type, p.prize_name,u.prize_status,p.description as prize_description,p.type as prize_type,u.date_created,u.date_claimed, fp.date_created as date_availed"
						// 									     )
						// 									);

		   	$res[] = array('Person ID',
		   				'Name',
		   				'Street',
					   'Barangay',
					   'City',
					   'Province',
					   'Mobile',
					   'Prize',
					   'Prize Type',
					   'Send Type',
					   'Status',
					   'Date Availed', 
					   'Date Claimed');

		   	if($rows->num_rows()){

		   		$prize_status = array('Pending','Delivered','Redeemed');
		   		
		   		foreach($rows->result() as $v){
		   			$date_claimed = $v->prize_status ? date('F d, Y H:i',strtotime($v->date_claimed)) : 'N/A';
		   			$res[] = array($v->person_id ,ucwords($v->first_name.' '.$v->third_name),$v->street_name,$v->barangay, $v->cityname, $v->provincename,$v->mobile_phone,$v->prize_name,$v->prize_type,$v->send_type,$prize_status[$v->prize_status],$v->date_availed,$date_claimed);
		   		}

		   	} 

			$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));

 
	   }

	   
 	public function update_status()
 	{

 		$id = $this->uri->segment(3);
 		$token = $this->uri->segment(4);
 		$status = $this->uri->segment(5);
 		$row = $this->explore_model->get_rows(array('table'=>'tbl_user_flash_prizes as u',
 													'where'=>array('u.user_flash_prize_id'=>$id,'u.prize_status'=>0),
 													'join'=>array(array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=u.registrant_id'),
														     			array('table'=>'tbl_flash_offers as p','on'=>'p.prize_id=u.prize_id')),
 													'fields'=>'u.*,r.registrant_id,r.first_name,r.third_name,p.send_type'
 													)
 											);

 		if($row && strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
 			$this->explore_model->update('tbl_user_flash_prizes',array('prize_status'=>$status,'date_modified'=>array(array('field'=>'date_claimed','value'=>'NOW()'))),array('user_flash_prize_id'=>$id));
 			$name = ($row->first_name || $row->third_name) ? ucwords($row->first_name.' '.$row->third_name) : '(No name)';
 			$action = ($row->send_type=='Delivery') ? 'delivered' : 'redeemed';
 			$post['url'] = $_SERVER['HTTP_REFERER'];
			$post['description'] = 'update '.$name.'\'s flash prize to '.$action.' status.';
			$post['table'] = 'tbl_user_flash_prizes';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);
		}

		redirect($this->router->class);

 	}
}
