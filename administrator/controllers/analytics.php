<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Analytics extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('statement_model');
		$this->load->model('user_model');
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$this->load->view('main-template', $data);
	}
	
	private function main_content() {
		$data['users'] = $this->module_model->get_audit_trail('', 1, PER_PAGE, $records);
		$page = $this->uri->segment(2, 1);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/activity_log');
		$data['offset'] = ($page - 1) * PER_PAGE;
		return $this->load->view('analytics/index', $data, true);		
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */