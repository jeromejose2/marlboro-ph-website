<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Profiling extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
		//$this->output->enable_profiler(TRUE);
 	} 

 	 
 	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

 	}
 	 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
		$this->load->helper('paging');
		$offset = (int)$this->input->get('per_page');
	    $limit = PER_PAGE;
	    $filters = $this->get_filters(); 

		$access = $this->module_model->check_access('regions');
		$total_rows = $this->explore_model->get_rows(array('table'=>'tbl_profiling as p',
															 'where'=>$filters['where_filters'],
														     'like'=>$filters['like_filters'],
														     'order_by' => array('field'=>'p.date_added','order'=>'DESC'),
														     'join'=>array(array('table'=>'tbl_categories as c','on'=>'c.category_id = p.interest'),
														     			   array('table'=>'tbl_categories as c2','on'=>'c2.category_id = p.sub_interest'),
														     			   array('table'=>'tbl_registrants as r','on'=>'r.registrant_id = p.registrant_id')
														     			   ), 
														     'fields'=>'p.profiling_id'
 														     )
														    )->num_rows();
 		$data['header_text'] = 'Profiling';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total_rows'] = $total_rows;
		$data['query_strings'] = $filters['query_strings'];
		$data['interest'] = $this->explore_model->get_rows(array('table'=>'tbl_categories','where'=>array('origin_id'=>PROFILING,'parent'=>0,'is_deleted'=>0)));

		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_profiling as p',
															 'where'=>$filters['where_filters'],
														     'like'=>$filters['like_filters'],
														     'order_by' => array('field'=>'p.date_added','order'=>'DESC'),
														     'join'=>array(array('table'=>'tbl_categories as c','on'=>'c.category_id = p.interest'),
														     			   array('table'=>'tbl_categories as c2','on'=>'c2.category_id = p.sub_interest'),
														     			   array('table'=>'tbl_registrants as r','on'=>'r.registrant_id = p.registrant_id')
														     			   ),
														     'offset'=>$offset,
														     'limit'=>$limit,
														     'fields'=>'p.profiling_id, c.category_name as interest, c2.category_name as sub_interest, r.first_name, r.third_name, p.date_added, r.person_id'
 														     )
														);
  
 		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']);
 		return $this->load->view('user_profiling/list',$data,TRUE);


	}

	public function sub_interest(){

		$interest = $this->input->get('interest');
		$data = array();
		if($interest)
			$data = $this->explore_model->get_rows(array('table'=>'tbl_categories','where'=>array('parent'=>$interest,'origin_id'=>PROFILING)))->result_array();
		echo json_encode($data);

	}
 

	public function delete()
	{

 		$id = $this->uri->segment(3);
 		$token = $this->uri->segment(4);

		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
 			
 			$this->explore_model->update('tbl_profiling',array('is_deleted'=>1),array('region_id'=>$id));

 			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'deleted a region for backstage pass events';
			$post['table'] = 'tbl_profiling';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);

		}
		redirect($this->uri->segment(1));

	}
	

	public function get_filters()
	   {
		   
		   $where_filters = array('DATE(p.date_added) >='=>'from_date_added','DATE(p.date_added) <='=>'to_date_added','p.interest'=>'interest','p.sub_interest'=>'sub_interest');
		   $like_filters = array("CONCAT(r.first_name,' ', r.third_name)"=>'registrant_name');
		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
		   
	   } 


	   public function export()
	   {
	   		$this->load->library('to_excel_array');

		   	$filters = $this->get_filters();
		   	$params = 
		   	$query = $this->explore_model->get_rows(array('table'=>'tbl_profiling as p',
															 'where'=>$filters['where_filters'],
														     'like'=>$filters['like_filters'],
														     'order_by' => array('field'=>'p.date_added','order'=>'DESC'),
														     'join'=>array(array('table'=>'tbl_categories as c','on'=>'c.category_id = p.interest'),
														     			   array('table'=>'tbl_categories as c2','on'=>'c2.category_id = p.sub_interest'),
														     			   array('table'=>'tbl_registrants as r','on'=>'r.registrant_id = p.registrant_id')
														     			   ),
														     'fields'=>'p.profiling_id, c.category_name as interest, c2.category_name as sub_interest, r.first_name, r.third_name, p.date_added, r.person_id'
 														     )
														);

		   	$res[] = array('Person ID','Registrant','Interest','Sub Date','Interest Added');

		   	if($query->num_rows()){
		   		foreach($query->result() as $v){
 		   			$res[] = array($v->person_id,$v->first_name.' '.$v->third_name,$v->interest,$v->sub_interest, $v->date_added);
		   		}
		   	}
		   	$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));

	   }


 
}