<?php

class Mym extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		set_time_limit(0);
		ini_set('memory_limit', '3000M');
		error_reporting(E_ALL);
		ini_set('display_errors', true);
	}

	public function edm()
	{
		$ids = $this->input->post('ids');
		if (!$ids) {
			@file_put_contents("./uploads/arclight/mym.log", date('Y-m-d H:i:s')." --- NO ids\n", FILE_APPEND);
			return;
		}

		$registrants = $this->db->select()
			->from('tbl_registrants')
			->where_in('registrant_id', $ids)
			->where('from_site', 0)
			->get()
			->result();

		require_once 'application/libraries/Edm.php';
		$edm = new Edm();
		$this->load->library('Encrypt');

		$success = 0;
		$count = count($registrants);
		foreach ($registrants as $registrant) {

			$email = $registrant->email_address;
			$username = $registrant->email_address;
			$firstname = $registrant->first_name;
			$lastname = $registrant->third_name;
			
			$password = $this->encrypt->decode($registrant->password, $registrant->salt);

			$edm->add_recipient(EDM_MARLBORO_WELCOME_LIST_ID, $email, $username, $firstname, $lastname, $password);
			$sendReturn = $edm->send(EDM_MARLBORO_WELCOME_MAIL_ID, $email, $username, $firstname, $password);
			if ($sendReturn !== true) {
				$this->db->set('edm_status', 2);
				$this->db->set('status', MYM_APPROVED);
				$this->db->set('edm_error_message', json_encode($sendReturn));
				$this->db->where('registrant_id', $registrant->registrant_id);
				$this->db->update('tbl_registrants');
			} else {
				$this->db->set('edm_status', 1);
				$this->db->where('registrant_id', $registrant->registrant_id);
				$this->db->update('tbl_registrants');
				if ($this->db->affected_rows()) {
					$success++;
				}
			}

		}

		@file_put_contents("./uploads/arclight/mym.log", date('Y-m-d H:i:s').' --- Registrants = '.$count.', Success = '.$success.', Failed = '.($count - $success)."\n", FILE_APPEND);
	}
}