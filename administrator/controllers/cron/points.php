<?php
class Points extends CI_Controller
{
    //var $edm;
    public function __construct()
    {
        parent::__construct();
        require_once 'application/models/points_model.php';
        $this->points_model = new Points_model();
    }

    public function update_points() {
        $response = $this->points_model->updatePoints();
        
        if($response){
            $this->load->library('email');

            $this->email->from('no-reply@marlboro-stage2.yellowhub.com', 'nuworks');
            $this->email->to('nuworks.client4@gmail.com');

            $this->email->subject('Email Test');
            $this->email->message('Updated points.');

            $this->email->send();
        }
    }   
}