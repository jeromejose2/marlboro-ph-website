<?php

class Arclight extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Arclight_Model');
		include_once('application/models/points_model.php');
		include_once('application/models/notification_model.php');
		$this->points_model = new Points_Model();
		$this->notification_model = new Notification_Model();
	}

	// 23	23	*	*	*
	public function push()
	{
		if ($filename = $this->Arclight_Model->push()) {
			$this->output->set_output($filename);
		}
	}

	// */15	*	*	*	*
	public function pull()
	{
		$result = $this->Arclight_Model->pull_with_inbound_failed();

		if($result){
			foreach ($result as $r) {
				$this->points_model->earn(REFERRAL, 
					array(
						'registrant' => $r['referrer'],
						'suborigin' => $r['suborigin']
					));

				$message = "Congratulations! Your friend, ". $r['name'] .", is now part of the MARLBORO community! ".
							"You get ".REFERER_BASE_POINT." points for the successful referral. Don’t forget to invite ". $r['name'] .
							" to log in and experience the exclusive PERKS in the website. ".
							"<br><br> This will unlock more points for you, and help you collect more items or win future bids!";

				$this->notification_model->notify($r['referrer'], REFERRAL, 
					array('message' => $message,'suborigin'=>0));
			}
		}
	}


	public function force_verify()
	{
		$result = $this->Arclight_Model->force_pull_with_inbound_failed();

		if($result){
			foreach ($result as $r) {
				$this->points_model->earn(REFERRAL, 
					array(
						'registrant' => $r['referrer'],
						'suborigin' => $r['suborigin']
					));

				$message = "Congratulations! Your friend, ". $r['name'] .", is now part of the MARLBORO community! ".
							"You get ".REFERER_BASE_POINT." points for the successful referral. Don’t forget to invite ". $r['name'] .
							" to log in and experience the exclusive PERKS in the website.".
							"<br> This will unlock more points for you, and help you collect more items or win future bids!";

				$this->notification_model->notify($r['referrer'], REFERRAL, 
					array('message' => $message,'suborigin'=>0));
			}
		}
	}
}
