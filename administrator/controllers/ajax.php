<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {
	
	function delete() {
		$table = $this->input->post('table');
		$id = $this->db->input->post('id');
		$field = $this->db->input->post('field');
		
		$where[$field] = $id;
		$this->global_model->delete_record($table, $where);
	}
	
	function load_cities() {
		$province = $_REQUEST['province'];
		$cities = $this->global_model->get_cities($province);
		echo json_encode($cities);
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
	