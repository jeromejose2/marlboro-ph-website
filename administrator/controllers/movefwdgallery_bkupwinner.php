    public function set_winner() {
        $id = $this->uri->segment(3);
        $token = $this->uri->segment(4);
        if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/move_fwd_gallery') !== false && $token == md5($id . ' ' .    $this->config->item('encryption_key'))) {
            $this->update_winner_status($id, 1, 'set a move forward gallery as winner');
        }
        redirect('move_fwd_gallery');
    }

    public function unset_winner() {
        $id = $this->uri->segment(3);
        $token = $this->uri->segment(4);
        if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/move_fwd_gallery') !== false && $token == md5($id . ' ' .    $this->config->item('encryption_key'))) {
            $this->update_winner_status($id, 0, 'unset a move forward gallery as winner');
        }
        redirect('move_fwd_gallery');
    }

    private function update_winner_status($id, $new_status, $action_text = '', $message = '') {
        $where['move_forward_gallery_id'] = $id;
        $param['where'] = $where;
        $param['table'] = $this->_table;
        $param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id  = ' . $this->_table . '.registrant_id',
                               'tbl_challenges' => 'tbl_challenges.challenge_id = ' . $this->_table . '.challenge_id');
        $gallery = (array)$this->global_model->get_row($param);
        if(!$gallery)
            redirect('move_fwd_gallery');

        $set['mfg_winner_status'] = $new_status;
        $this->global_model->update_record($this->_table, $where, $set);

        if($new_status == 1) {

            $mfwd = $this->global_model->get_row(array('table' => 'tbl_move_forward_gallery', 'where' => 'move_forward_gallery_id ='.$id));
            $mfwd_ch_id = $mfwd->challenge_id;
            $mfwd_ch = $this->global_model->get_row(array('table' => 'tbl_challenges', 'where' => 'challenge_id ='.$mfwd_ch_id));
            $mfwd_id = $mfwd_ch->move_forward_id;
            $mfwd_fwd = $this->global_model->get_row(array('table' => 'tbl_move_forward', 'where' => 'move_forward_id ='.$mfwd_id));
            $mfwd_prize_id = $mfwd_fwd->prize_id;
            $mfwd_prize = $this->global_model->get_row(array('table' => 'tbl_prizes', 'where' => 'prize_id ='.$mfwd_prize_id));
            $mfwd_prize_name = $mfwd_prize->prize_name;

            // $this->spice->track_action(1420, array(array('ActivityId' => 1122, 'ActionValue' => $id.'||'.$mfwd_prize_name)), $this->spice->get_person_id());
            // $this->spice->loginAdmin('CMSPHSysUser', 'Default#14');
            
            $param = array('CellId' => 1420, 'ActionList' =>array(array('ActivityId'   => 1122, 'ActionValue' => $id.'||'.$mfwd_prize_name)));
            $response = $this->spice->trackAction($param);

            $type = $gallery['type'] == 'Photo' ? 1 : 2;
            $this->points_model->earn(MOVE_FWD_WINNER, array('registrant'   => $gallery['registrant_id'], 'suborigin'   => $id, 'type'  => $type));         
        } else {
            $where = array();
            $set['total_points'] = $gallery['total_points'] - MOVE_FWD_WINNER_BASE_POINT;
            $where['registrant_id'] = $gallery['registrant_id'];
            $this->global_model->update('tbl_registrants', $set, $where);

            $where = array();
            $where['origin_id'] = MOVE_FWD_WINNER;
            $where['suborigin_id'] = $id;
            $this->global_model->delete('tbl_points', $where);

        }

        #save audit trail
        $new_content['mfg_winner_status'] = $new_status == 1 ? 'Yes' : 'No';
        $old_content['mfg_winner_status'] = $new_status == 1 ? 'No' : 'Yes';
        $post['url'] = @$_SERVER['HTTP_REFERER'];
        $post['description'] = $action_text;
        $post['table'] = $this->_table;
        $post['record_id'] = $id;
        $post['message'] = $message;
        $post['field_changes'] = serialize(array('old'  => $old_content,
                                                  'new' => $new_content));
        $this->module_model->save_audit_trail($post);   

    }