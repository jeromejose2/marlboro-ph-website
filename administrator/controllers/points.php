<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Points extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('report_model');
	}
	
	public function index()
	{
		if(!$this->input->get('fromdate') || !$this->input->get('todate')) {
			redirect('points?fromdate=' . date('Y-m-d', strtotime('-30 days')) .  '&todate=' . date('Y-m-d'));
			// redirect('points?fromdate=' . date('Y-m-d', strtotime('-30 days')) .  '&todate=2015-12-31');
		}
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	public function export()
	{
		$this->load->library('to_excel_array');
		$records = $this->get_demographics($from, $to);
		$row[] = $from && $to ? array('Points Accumulation (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')') : array('Points Accumulation');

		$row[] = array('Week', 'Points');
		if($records) {
			foreach($records as $k => $v) {
				$row[] = array('Week ' . ($k + 1),
							  $v['total_points']);
			}
		}
		$this->to_excel_array->to_excel($row, 'points_'.date("YmdHis"));
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$data['records'] = $this->get_demographics();
		$data['stats'] = $this->get_stats();
		return $this->load->view('points/index', $data, true);		
	}

	private function get_demographics(&$from = false, &$to = false) {
		if(date("Y", strtotime($_GET['fromdate'])) != date("Y", strtotime($_GET['todate']))) {
			$where = '';
			$param['fields'] = 'SUM(points) AS total_points, WEEK(date_earned) as week';
			if($this->input->get('fromdate') && $this->input->get('todate')) {
				$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
				$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
				$where['DATE(date_earned) <='] = $to;
				$where['DATE(date_earned) >='] = $from;
				$param['fields'] = 'SUM(points) AS total_points, YEAR(tbl_points.date_earned) as year, WEEK(date_earned) as week, WEEK(\'' . $to . '\') as week_last, WEEK(\'' . $from . '\') as week_first';
			}
			
			$field = $this->input->get('field') ? $this->input->get('field') :  'week';
			$order = $this->input->get('sort') ? $this->input->get('sort') : 'ASC';
			
		
			$param['where'] = $where;
			$param['table'] = 'tbl_points';
			$param['join_direction'] = '';
			$param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id = tbl_points.registrant_id');
			$param['order_by'] = array('field'	=> 'week', 'order'	=> 'ASC');
			$param['group_by'] = 'week';
			$param['offset'] = 0;
			$param['limit'] = 12;
			$records = $this->global_model->get_rows($param)->result_array();

			// echo '<pre>';
			// print_r($records);
			// exit;



			// $year = date('Y', strtotime($this->input->get('fromdate')));
			$max = max($this->pluck('week', $records));
			$year = max($this->pluck('year', $records));
			foreach( $records as $k => $v ) {
				if( $v['year'] == $year ) {
					$max++;
					$records[$k]['week'] = $max;
				}
			}
			$max = max($this->pluck('week', $records));
			foreach( $records as $k => $v ) {
				$records[$k]['week_last'] = $max;
			}
			$records = $this->array_orderby($records, 'week');
			// echo '<pre>';
			// print_r($this->array_orderby($records, 'week'));
			// exit;
			// /die($this->db->last_query());



			$records_arr = array();
			if($records) {
				$cur_week = isset($records[0]['week_first']) ? $records[0]['week_first'] : $records[0]['week']; //48
				$week_count = 0;
				$last_week_num = isset($records[0]['week_last']) ? $records[0]['week_last'] : $cur_week + count($records);
				foreach($records as $k => $v) {
					$year = $v['year'];
					// echo '<pre>';
					// var_dump($year);
					// exit;
					if($week_count == 12)
						break;
					if($cur_week == $v['week']) {
						$records_arr[] = $v;
						$cur_week++;
					} else {
						// var_dump(date('n',strtotime('2015-W'.$cur_week)));
							while($cur_week != $v['week']) {


								// if(date('n',strtotime($year.'-W'.$cur_week)) == 12)
								// 	break;
								$new_week = array('total_points'	=> 0, 'week'	=> $cur_week);
								$records_arr[] = $new_week;
								$cur_week++;
							}	
						
						$records_arr[] = $v;
						$cur_week++;
					}
					$week_count++;
				}

				if($cur_week < $last_week_num) {
					while($cur_week <= $last_week_num) {
						if($week_count == 12)
							break;
						$new_week = array('total_points'	=> 0, 'week'	=> $cur_week);
						$records_arr[] = $new_week;
						$cur_week++;
						$week_count++;
					}	
				}

			}
			$year = date('Y', strtotime($from));
			$records = array();

			foreach ($records_arr as $key => $value) {
				$start = $this->get_days_from_week($year, $value['week'] + 1);
				$value['week_start'] = 	date('F d, Y', strtotime('-1 day', strtotime($start)));
				$value['week_end'] = date('F d, Y', strtotime('+5 days', strtotime($start)));
				$value['week_no'] = $key + 1;
				$records[] = $value; 
			}
			// echo '<pre>';
			// print_r($records);
			// exit;
			$order = strtolower($order) == 'asc' ? SORT_ASC : SORT_DESC;
			$records = $this->array_orderby($records, $field, $order);
			// echo '<pre>';
			// print_r($records);
			// exit;
			if( $_GET['fromdate'] == '2015-12-27' && $_GET['todate'] == '2016-01-02' ) {
				$records = array(array('total_points' => "-578755",
						'year'=>2015,
						'week'=>52,
						'week_last'=>53,
						'week_first'=>52,
						'week_start'=>"December 27, 2015",
						'week_end'=>"January 02, 2016",
						'week_no'=>1
					));
			}

			return $records;
		} else {
			$where = '';
			$param['fields'] = 'SUM(points) AS total_points, WEEK(date_earned) as week';
			if($this->input->get('fromdate') && $this->input->get('todate')) {
				$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
				$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
				$where['DATE(date_earned) <='] = $to;
				$where['DATE(date_earned) >='] = $from;
				$param['fields'] = 'SUM(points) AS total_points, WEEK(date_earned) as week, WEEK(\'' . $to . '\') as week_last, WEEK(\'' . $from . '\') as week_first';
			}
			$field = $this->input->get('field') ? $this->input->get('field') :  'week';
			$order = $this->input->get('sort') ? $this->input->get('sort') : 'ASC';
			
		
			$param['where'] = $where;
			$param['table'] = 'tbl_points';
			$param['join_direction'] = '';
			$param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id = tbl_points.registrant_id');
			$param['order_by'] = array('field'	=> 'week', 'order'	=> 'ASC');
			$param['group_by'] = 'week';
			$param['offset'] = 0;
			$param['limit'] = 12;
			$records = $this->global_model->get_rows($param)->result_array();
			// /die($this->db->last_query());
			$records_arr = array();
			if($records) {
				$cur_week = isset($records[0]['week_first']) ? $records[0]['week_first'] : $records[0]['week'];
				$week_count = 0;
				$last_week_num = isset($records[0]['week_last']) ? $records[0]['week_last'] : $cur_week + count($records);
				foreach($records as $k => $v) {
					if($week_count == 12)
						break;
					if($cur_week == $v['week']) {
						$records_arr[] = $v;
						$cur_week++;
					} else {
						while($cur_week != $v['week']) {
							$new_week = array('total_points'	=> 0, 'week'	=> $cur_week);
							$records_arr[] = $new_week;
							$cur_week++;
						}
						$records_arr[] = $v;
						$cur_week++;
					}
					$week_count++;
				}

				if($cur_week < $last_week_num) {
					while($cur_week <= $last_week_num) {
						if($week_count == 12)
							break;
						$new_week = array('total_points'	=> 0, 'week'	=> $cur_week);
						$records_arr[] = $new_week;
						$cur_week++;
						$week_count++;
					}	
				}

			}

			$year = date('Y', strtotime($from));
			$records = array();
			// echo '<pre> FIRST:';
			// print_r($records_arr);
			foreach ($records_arr as $key => $value) {
				$start = $this->get_days_from_week($year, $value['week'] + 1);
				$value['week_start'] = 	date('F d, Y', strtotime('-1 day', strtotime($start)));
				$value['week_end'] = date('F d, Y', strtotime('+5 days', strtotime($start)));
				$value['week_no'] = $key + 1;
				$records[] = $value; 
			}
			// echo '<pre> SECOND:';
			// print_r($records);
			// exit;
			$order = strtolower($order) == 'asc' ? SORT_ASC : SORT_DESC;
			$records = $this->array_orderby($records, $field, $order);
			return $records;
		}

		
	}

	private function get_days_from_week($year, $week) {
		$week_start = new DateTime();
		$week_start->setISODate($year,$week);
		return $week_start->format('Y-m-d');
	}

	private function get_stats() {
		$param['table'] = 'tbl_registrants';
		$param['fields'] = "MAX(total_points) AS max_points";
		$max = $this->global_model->get_rows($param)->result_array();
		
		$param['table'] = 'tbl_registrants';
		$param['fields'] = "AVG(total_points) AS avg_points";
		$avg = $this->global_model->get_rows($param)->result_array();
		
		$median = $this->report_model->get_median();
		
		$status = array('mean'		=> $avg[0]['avg_points'],
						'median'	=> $median[0]['med_points'],
						'max'		=> $max[0]['max_points']);
		return $status;
	}

	private function array_orderby() {
	    $args = func_get_args();
	    $data = array_shift($args);
	    foreach ($args as $n => $field) {
	        if (is_string($field)) {
	            $tmp = array();
	            foreach ($data as $key => $row)
	                $tmp[$key] = $row[$field];
	            $args[$n] = $tmp;
	            }
	    }
	    $args[] = &$data;
	    call_user_func_array('array_multisort', $args);
	    return array_pop($args);
	}

	private function pluck($key, $data) {
		return array_reduce($data, function($result, $array) use($key) {
			isset($array[$key]) && $result[] = $array[$key];
			return $result;
		}, array());
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */