<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Finalists extends CI_Controller {

	var $_table;

	public function __construct() {
		parent::__construct();
		$this->load->model('module_model');
		$this->_table = 'tbl_finalists';
	}

	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);
	}

	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			unset($like['search']);
		}
		$param['like'] = $like;
		$param['offset'] = $data['offset'];
		$param['table'] = $this->_table;
		$param['where'] = array('is_deleted'=>0);
		$param['order_by'] = array('field'=>'date_created', 'order'=>'DESC');
		$data['records'] = $this->global_model->get_rows($param)->result_array();
		$records = $this->global_model->get_total_rows($param);
		$data['pagination'] = $this->global_model->pagination($records, $page, SITE_URL.'/finalists');
		$access = $this->module_model->check_access('finalists');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = count($data['records']);
		$data['origins'] = $this->module_model->get_origins();
		return $this->load->view('finalists/index', $data, TRUE);
	}

	public function add() {
		$data['main_content'] = $this->add_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	private function add_content() {
		$error = '';
		if($this->input->post('submit'))
		{
			//upload all first
			$config['upload_path'] = 'uploads/finalists/photos/';
			$config['allowed_types'] = '*';
			$this->load->library('upload', $config);
			$image = 'photo';
			$primary_photo = 'primaryphoto';
			$cover_photo = 'coverphotoreal';

			$photo_files = $_FILES;
			$photo_file_count = count($_FILES['photo']['name']);
			$photo_file_names = array();
			if($photo_files['photo']['name'][0]) {
				for($i = 0; $i < $photo_file_count; $i++) {
					$_FILES['photo']['name'] = $photo_files['photo']['name'][$i];
					$_FILES['photo']['type'] = $photo_files['photo']['type'][$i];
					$_FILES['photo']['tmp_name'] = $photo_files['photo']['tmp_name'][$i];
					$_FILES['photo']['error'] = $photo_files['photo']['error'][$i];
					$_FILES['photo']['size'] = $photo_files['photo']['size'][$i];

					if($this->upload->do_upload($image))
					{
						//$photo_file_names[] = base_url().'uploads/finalists/photos/' . $photo_files['photo']['name'][$i];
						$photo_file_names[] = 'uploads/finalists/photos/'.$photo_files['photo']['name'][$i];
					} else
					{
						print_r($this->upload->display_errors());
						exit;
					}
				}
			}

			if($this->upload->do_upload($primary_photo)) {
				$primary_photo = 'uploads/finalists/photos/'.$this->upload->data()['file_name'];
			}

			if($this->upload->do_upload($cover_photo)) {
				$cover_photo = 'uploads/finalists/photos/'.$this->upload->data()['file_name'];
				$test2 = $this->upload->data();
			}


						$photo_array = array();
						foreach ($photo_file_names as $key => $value)
						{
							if($key == $this->input->post('coverphoto')[0])
							{

								$photo_array['coverphoto'][] =   $value;//$this->input->post('coverphoto')[0];
								//$photo_array['url'][] =  $value;
							}else
							{

								$photo_array['url'][] =  $value;

							}
						}


			$photos = $photo_array ? serialize($photo_array) : serialize(array());


			$config = array();
			$config['upload_path'] = 'uploads/finalists/videos/';
			$config['allowed_types'] = '*';
			$this->upload->initialize($config);
			$realpath = realpath($config['upload_path']);
			$video = 'video';

			$video_files = $_FILES;
			$video_file_count = count($_FILES['video']['name']);
			$video_file_names = array();
			if($video_files['video']['name'][0]) {
				for ($j=0; $j < $video_file_count; $j++) {
					$_FILES['video']['name'] = $photo_files['video']['name'][$j];
					$_FILES['video']['type'] = $photo_files['video']['type'][$j];
					$_FILES['video']['tmp_name'] = $photo_files['video']['tmp_name'][$j];
					$_FILES['video']['error'] = $photo_files['video']['error'][$j];
					$_FILES['video']['size'] = $photo_files['video']['size'][$j];

					if($this->upload->do_upload($video)) {

						$video_file_names[] =  'uploads/finalists/videos/'. $video_files['video']['name'][$j];
						$realpath = str_replace("\\", '/', rtrim($realpath, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR);
						$thumbfilename = pathinfo($video_files['video']['name'][$j], PATHINFO_FILENAME).'.jpg';
						exec('ffmpeg -i '.$realpath.$video_files['video']['name'][$j].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realpath.'thumbnail/'.$thumbfilename);
						//exec('C:\ffmpeg\ffmpeg -i '.$realpath.$video_files['video']['name'][$j].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realpath.'thumbnail/'.$thumbfilename);



					} else {
						print_r($this->upload->display_errors());
						exit;
					}
				}
			}


						$video_array = array();
						foreach ($video_file_names as $key => $value)
						{

								$video_array['thumbnail'][] =  'uploads/finalists/videos/thumbnail/'.$thumbfilename;
								$video_array['url'][] =  $value;

						}


			$videos = $video_array ? serialize($video_array) : serialize(array());

			$this->db->insert('tbl_finalists', array(
				'name' => $this->input->post('name'),
				'person_id' => $this->input->post('person_id'),
				'location' => $this->input->post('location'),
				'interest' => $this->input->post('interest'),
				'write_ups' => $this->input->post('write_ups'),
				'short_description' => $this->input->post('short_description'),
				'primary_photo' => $primary_photo,
				'cover_photo' => $cover_photo,
				'photos' => $photos,
				'videos' => $videos,
				'is_deleted' => 0,
				'date_created' => date('Y-m-d H:i:s')
			));

			redirect('finalists');

			// $valid = $this->validate_fields();
			// if($valid) {
			// 	$this->module_model->save_module();
			// 	redirect('module');
			// } else {
			// 	$error = 'Sorry, module name or uri already exists';
			// }
		}
		$data['error'] = $error;
		$data['module'] = $_POST;
		return $this->load->view('finalists/add', $data, true);
	}

	public function edit() {
		$id = $this->uri->segment(3);
		$data['main_content'] = $this->edit_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	private function edit_content($id) {
		$error = '';
		$trigger = '';
		$trigger2 = '';
		if($this->input->post('submit')) {
			// echo '<pre>';
			// print_r($_POST);
			// exit;
			// kapag wala nabago
			// coverphoto_id = ''

			$data = $this->db->where('finalist_id', $id)->get('tbl_finalists')->result_array()[0];
			if($this->input->post('delete_photocover')) {
				$data_array = unserialize($data['photos']);
				$original_coverphoto = '';
				$original_images = $data_array['url'];
				$return_to_array = array('coverphoto'=>$original_coverphoto, 'url'=>$original_images);
				$serialize = serialize($return_to_array);
				$this->db->where('finalist_id', $id);
				$this->db->update('tbl_finalists', array('photos'=>$serialize));
			}

			$data = $this->db->where('finalist_id', $id)->get('tbl_finalists')->result_array()[0];
			// echo '<pre>';
			// print_r($data);
			// exit;
			if($this->input->post('remove_photos')) {
				$data_array = unserialize($data['photos']);
				$original_coverphoto = $this->input->post('delete_photocover') ? '' : $data_array['coverphoto'];
				$original_images = $data_array['url'];
				$strip = array_intersect($original_images, $this->input->post('remove_photos'));
				$result = array_diff($original_images, $strip);
				$return_to_array = array('coverphoto'=>$original_coverphoto, 'url'=>$result);
				$serialize = serialize($return_to_array);
				$this->db->where('finalist_id', $id);
				$this->db->update('tbl_finalists', array('photos'=>$serialize));
			}

			$data = $this->db->where('finalist_id', $id)->get('tbl_finalists')->result_array()[0];
			if($this->input->post('remove_videos')) {
				$original_videos = unserialize($data['videos']);
			}

			$config['upload_path'] = 'uploads/finalists/photos/';
			$config['allowed_types'] = '*';
			$config['encrypt_name'] = false;
			$this->load->library('upload', $config);
			$image = 'photo';
			$primary_photo = 'primaryphoto';
			$cover_photo = 'coverphotoreal';
			$cover_photo_id = 'coverphoto_id';

			$photo_files = $_FILES;
			$photo_file_count = count($_FILES['photo']['name']);
			// var_dump($photo_file_count);
			// exit;
			$photo_file_names = array();
			if($photo_files['photo']['name'][0]) {
				for($i = 0; $i < $photo_file_count; $i++) {
					$_FILES['photo']['name'] = $photo_files['photo']['name'][$i];
					$_FILES['photo']['type'] = $photo_files['photo']['type'][$i];
					$_FILES['photo']['tmp_name'] = $photo_files['photo']['tmp_name'][$i];
					$_FILES['photo']['error'] = $photo_files['photo']['error'][$i];
					$_FILES['photo']['size'] = $photo_files['photo']['size'][$i];

					if($upload_result = $this->upload->do_upload($image))
					{
						//$photo_file_names[] = base_url().'uploads/finalists/photos/' . $photo_files['photo']['name'][$i];
						//$photo_file_names[] = 'uploads/finalists/photos/'.$photo_files['photo']['name'][$i]; # original
						$photo_file_names[] = 'uploads/finalists/photos/'.$this->upload->data()['file_name'];
					} else
					{
						print_r($this->upload->display_errors());
						exit;
					}
				}
			}

			if($this->upload->do_upload($primary_photo)) {
				$primary_photo = 'uploads/finalists/photos/'.$this->upload->data()['file_name'];
			} else {
				$primary_photo = $data['primary_photo'];
			}

			if($this->upload->do_upload($cover_photo)) {
				$cover_photo = 'uploads/finalists/photos/'.$this->upload->data()['file_name'];
			} else {
				$cover_photo = $data['cover_photo'];
			}

			if($this->input->post('coverphoto_id')) {

			}


						$photo_array = array();
						$photo_array1 = array();
						if($photo_file_names)
						{
								$count = 0;
									foreach ($photo_file_names as $key => $value)
									{


										if($this->input->post('coverphoto_id') != '')
										{

											$remove_uploadstring = str_replace('uploads/finalists/photos/', '', $value);
													$remove_uploadstring_coverphoto = str_replace('uploads/finalists/photos/', '',$this->input->post('coverphoto_id'));

														 if($remove_uploadstring == $remove_uploadstring_coverphoto)
														 	{

														 		$photo_array['coverphoto'][] = $value;
														 	}else
														 	{
														 		$photo_array['url'][] =  $value;
														 	}


										}else
										{
											$photo_array['url'][] = $value;
											// if($count == 0)
											// {
											// 	$photo_array['coverphoto'][] = $value;
											// }else
											// {
											// 		$photo_array['url'][] =  $value;
											// }
										}

										$count++;
									}

						}else
						{
							if($this->input->post('coverphoto_id') != '')
							{
											foreach (unserialize($data['photos']) as $key => $v)
											{



													foreach ($v as $k => $value)
													{

														if($key == 'coverphoto')
														{
															$photo_array1['url'][] =  $value;
														}
														if($key == 'url')
														{
																	$remove_uploadstring = str_replace('uploads/finalists/photos/', '', $value);
																	$remove_uploadstring_coverphoto = str_replace('uploads/finalists/photos/', '',$this->input->post('coverphoto_id'));

																		 if($remove_uploadstring == $remove_uploadstring_coverphoto)
																		 	{

																		 		$photo_array1['coverphoto'][] = $value;
																		 	}else
																		 	{
																		 		$photo_array1['url'][] =  $value;
																		 	}

														}
													}


											}
								}
						}

	if(!empty($photo_array))
	{
		$photos = serialize($photo_array);
		$trigger = 1;
	}elseif (!empty($photo_array1))
	{
		$photos = serialize($photo_array1);
		$trigger2 = 1;
	}else
	{
		$photos = $data['photos'];
	}



			$config = array();
			$config['upload_path'] = 'uploads/finalists/videos/';
			$config['allowed_types'] = '*';
			$this->upload->initialize($config);
			$realpath = realpath($config['upload_path']);
			$video = 'video';

			$video_files = $_FILES;
			$video_file_count = count($_FILES['video']['name']);
			$video_file_names = array();
			if($video_files['video']['name'][0]) {
				for ($j=0; $j < $video_file_count; $j++) {
					$_FILES['video']['name'] = $photo_files['video']['name'][$j];
					$_FILES['video']['type'] = $photo_files['video']['type'][$j];
					$_FILES['video']['tmp_name'] = $photo_files['video']['tmp_name'][$j];
					$_FILES['video']['error'] = $photo_files['video']['error'][$j];
					$_FILES['video']['size'] = $photo_files['video']['size'][$j];

					if($this->upload->do_upload($video)) {

						$video_file_names[] =  'uploads/finalists/videos/'. $video_files['video']['name'][$j];
						$realpath = str_replace("\\", '/', rtrim($realpath, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR);
						$thumbfilename = pathinfo($video_files['video']['name'][$j], PATHINFO_FILENAME).'.jpg';
						exec('ffmpeg -i '.$realpath.$video_files['video']['name'][$j].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realpath.'thumbnail/'.$thumbfilename);
						//exec('C:\ffmpeg\ffmpeg -i '.$realpath.$video_files['video']['name'][$j].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realpath.'thumbnail/'.$thumbfilename);



					} else {
						print_r($this->upload->display_errors());
						exit;
					}
				}
			}


						$video_array = array();
						if($video_file_names)
						{
								foreach ($video_file_names as $key => $value)
								{

										$video_array['thumbnail'][] =  'uploads/finalists/videos/thumbnail/'.$thumbfilename;
										$video_array['url'][] =  $value;

								}
						}


			$videos = $video_array ? serialize($video_array) : $data['videos'];

			$original = $this->db->select()->from('tbl_finalists')->where('finalist_id', $id)->get()->result_array();

			if($photo_array) {
				if($original) {
					$original = unserialize($original[0]['photos']);
					$original['url'] = is_array($original['url']) ? $original['url'] : array();
					if($photos) {
						$p = unserialize($photos)['url'];
						foreach($p as $k => $v) {
							array_push($original['url'], $v);
						}
					}
					$photos = serialize($original);
				}
			}

			// if($this->input->post('coverphoto_id')) {
			// 	$change_cover = unserialize($photos);
			// 	echo '<pre>';
			// 	echo 'HI';
			// 	print_r($change_cover);
			// 	exit;
			// 	$change_cover['coverphoto'][0] = $this->input->post('coverphoto_id');
			// }

			if($this->input->post('coverphoto_id')) {
				$photos = unserialize($photos);
				// echo '<pre>';
				// print_r($photos);
				// exit;
				$needle = 'uploads';
				$haystack = $this->input->post('coverphoto_id');
				if(stripos($haystack, $needle) === false) {
					$v = 'uploads/finalists/photos/'.$haystack;
				} else {
					$v = $haystack;
				}
				$photos['coverphoto'][0] = $v;
				$photos = serialize($photos);
			}

			if($this->input->post('has_coverphoto')) {
				$validate = unserialize($photos);
				if(isset($validate['coverphoto'][0]) && $validate['coverphoto'][0] != $this->input->post('has_coverphoto')) {
					if( ! $this->input->post('delete_photocover')) {
						array_push($validate['url'], $this->input->post('has_coverphoto'));
					}
				}
				$photos = serialize($validate);
			}

			$this->db->update('tbl_finalists', array(
				'name' => $this->input->post('name'),
				'person_id' => $this->input->post('person_id'),
				'location' => $this->input->post('location'),
				'interest' => $this->input->post('interest'),
				'write_ups' => $this->input->post('write_ups'),
				'short_description' => $this->input->post('short_description'),
				'photos' => $photos,
				'videos' => $videos,
				'primary_photo' => $primary_photo,
				'cover_photo' => $cover_photo
			), array('finalist_id' => $this->input->post('id')));
			redirect('finalists');
		}
		$data['error'] = $error;
		// $data['module'] = $_POST ? $_POST : $this->module_model->get_module(array('module_id'	=> $id))[0];

		$data['record'] = $this->db->where('finalist_id', $id)->get('tbl_finalists')->result_array()[0];
		// $e = unserialize($data['record']['photos']);
		// echo '<pre>';
		// print_r($e);
		// exit;
		return $this->load->view('finalists/edit', $data, true);
	}


	public function validate() {
		$valid = $this->validate_fields();
		$data['main_content'] = $valid ? '1' : '0';
		$this->load->view('blank-template', $data);
	}

	private function validate_fields() {
		$name = $this->input->post('module_name');
		$uri = $this->input->post('uri');
		$id = $this->input->post('id');
		$where['module_name'] = $name;
		$where['uri'] = $uri;
		$module = $this->module_model->get_module($where);
		if($module) {
			if($id && $module[0]['module_id'] == $id)
				return true;
			return false;
		}

		return true;
	}

	public function delete()
	{
		//$this->db->delete('tbl_finalists', array('finalist_id' => $this->uri->segment(3)));

		$this->db->update('tbl_finalists', array(
				'is_deleted' =>1
			), array('finalist_id' => $this->uri->segment(3)));

		redirect('finalists');
		// $table = 'tbl_modules';
		// $id = $this->uri->segment(3);
		// $field = 'module_id';
		// if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/module') >= 0) {
		// 	$where[$field] = $id;
		// 	$this->global_model->delete_record($table, $where);
		// }
		// redirect('module');
	}

	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */