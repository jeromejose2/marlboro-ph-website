<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
 	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
		//$this->output->enable_profiler(TRUE);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
		$this->load->helper(array('paging','text'));
 		$offset = (int)$this->input->get('per_page');
	    $limit = PER_PAGE;
	    $filters = $this->get_filters();

		$access = $this->module_model->check_access('news');
		$total_rows = $this->explore_model->get_total_rows(array('table'=>'tbl_about_news',
																 'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
																 'like'=>$filters['like_filters']
																 )
														    ); 
		$data['header_text'] = 'News';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total_rows'] = $total_rows;

		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_about_news',
															 'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
														     'like'=>$filters['like_filters'],
														     'order_by'=>array('field'=>'date_added','order'=>'DESC'),
														     'limit'=>$limit,
														     'offset'=>$offset
														     )
														);
		$data['query_strings'] = $filters['query_strings'];
		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']); 

 		return $this->load->view('about/'.$this->uri->segment(1).'/list',$data,TRUE);
	}


	public function add()
	{

		$error = '';
		$row = '';

		if($this->input->post()) {

			$post_data = $this->input->post();		

			if($this->validate_form()) {
 
				
 				$data = $post_data;
 				$data['url_title'] = url_title(ucwords(strtolower(trim($post_data['title']))));
				$data['date_added'] = TRUE; // Set NOW() value to date_added column field
 				$data['image_crop_coordinates'] = serialize(array('x1'=>$this->input->post('x'),
 															 'y1'=>$this->input->post('y'),
 															 'x2'=>$this->input->post('x2'),
 															 'y2'=>$this->input->post('y2'),
 															 'width'=>$this->input->post('width'),
 															 'height'=>$this->input->post('height')
 															 ));
 
 
				$id = $this->explore_model->insert('tbl_about_news',$data);
				$image_filename = $this->do_upload($id,'image');

				if($image_filename) 
	   		    	$this->explore_model->update('tbl_about_news',array('image'=>$image_filename),array('about_news_id'=>$id));

	   		    $post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/add';
				$post['description'] = 'added a news';
				$post['table'] = 'tbl_about_news';
				$post['record_id'] = $id;
				$post['type'] = 'add';
				$this->module_model->save_audit_trail($post);

	   		    redirect($this->uri->segment(1));

			} else {

				$post_data['image'] = '';
				$error = validation_errors();
				$row = json_decode(json_encode($post_data),false);

			}


			
 
		}
 

		$data['error'] = $error;
		$data['row'] = $row;
 		$data['header_text'] = 'News';
 		$data['main_content'] = $this->load->view('about/'.$this->uri->segment(1).'/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
		//$this->output->enable_profiler(TRUE);

	}


	public function edit()
	{

		$error = '';
		$id = $this->uri->segment(3);
		$row = $this->explore_model->get_row(array('table'=>'tbl_about_news','where'=>array('about_news_id'=>$id,'is_deleted'=>0)));


		if($this->input->post())
		{
			$post_data = $this->input->post();

			if($this->validate_form()){


				$trend = json_decode(json_encode($row),TRUE);


				$fields = array('title', 'description','status');
				$status = array('', 'Published', 'Unpublished');

				$new_content = array();
				$old_content = array();

				foreach($trend as $k => $v) {
					if(in_array($k, $fields)) {
						if($trend[$k] != $this->input->post($k)) {

							if($k == 'status') {
								
								$new_content[$k] = $status[$this->input->post($k)];
								$old_content[$k] = $status[$trend[$k]];

							}else {
								$new_content[$k] = $this->input->post($k);
								$old_content[$k] = $trend[$k];
							}

						}
					}
				}

				$post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/edit/' . $id;
				$post['description'] = 'updated a news';
				$post['table'] = 'tbl_about_news';
				$post['record_id'] = $id;
				$post['type'] = 'edit';
				$post['field_changes'] = serialize(array('old'	=> $old_content,
														 'new'	=> $new_content));
				$this->module_model->save_audit_trail($post);

 				$post_data['date_updated']	= TRUE; // Set NOW() value to column date_updated
 				$post_data['url_title'] = url_title(ucwords(strtolower(trim($post_data['title']))));
 				$post_data['image_crop_coordinates'] = serialize(array('x1'=>$this->input->post('x'),
 															 'y1'=>$this->input->post('y'),
 															 'x2'=>$this->input->post('x2'),
 															 'y2'=>$this->input->post('y2'),
 															 'width'=>$this->input->post('width'),
 															 'height'=>$this->input->post('height')
 															 ));
				$this->explore_model->update('tbl_about_news',$post_data,array('about_news_id'=>$id,'is_deleted'=>0));
				$this->do_upload($id,'image');
				redirect($this->uri->segment(1));

			}else{

				$error = validation_errors();
				$post_data['image'] = $post_data['current_image'];
				$row = json_decode(json_encode($post_data),false);

			}

		}
 

		$data['row'] = $row;
		$data['error'] = $error;
		$data['header_text'] = 'News';
 		$data['main_content'] = $this->load->view('about/'.$this->uri->segment(1).'/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

	}


	public function delete()
	{

 		$id = $this->uri->segment(3);
 		$token = $this->uri->segment(4);
 		$path = 'uploads/about/'.$this->uri->segment(1).'/';

		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$row = $this->explore_model->get_rows(array('table'=>'tbl_about_news','where'=>array('about_news_id'=>$id)));


			 

 			$this->explore_model->update('tbl_about_news',array('is_deleted'=>1),array('about_news_id'=>$id));


 			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'deleted a news';
			$post['table'] = 'tbl_about_news';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);

		}
		redirect($this->uri->segment(1));

	}

	public function validate_form()
	{

		$this->load->library('form_validation');

		$rules = array(
		   array(
				 'field'   => 'title',
				 'label'   => 'Title',
				 'rules'   => 'required|callback_validate_title'
			  ),
		   array(
				 'field'   => 'description',
				 'label'   => 'Description',
				 'rules'   => 'required'
			  )
		);

		$this->form_validation->set_rules($rules);
		
		return $this->form_validation->run();
	}

	public function validate_title($title)
	{	

		$row = $this->explore_model->get_row(array('table'=>'tbl_about_news',
												   'where'=>array('about_news_id !='=>$this->uri->segment(3),'url_title'=>url_title(ucwords(strtolower(trim($title)))))
												   )
											);

		if($row){
			$this->form_validation->set_message('validate_title','Title already exists!');
			return false;
		}else{
			return true;
		}

	}




	public function do_upload($new_filename,$upload_file)
	{
		$this->load->helper(array('upload','resize','crop'));
		$upload_path = 'uploads/about/'.$this->uri->segment(1).'/';

		if(!is_dir($upload_path)){
			mkdir($upload_path,0777,true);
		}

		$params = array('upload_path'=>$upload_path,'allowed_types'=>'png|jpg|gif','file_name'=>$new_filename,'max_size'=>'2097152','max_width'=>0,'max_height'=>0,'do_upload'=>$upload_file);
	    $file = upload($params);

	    $params = array('x'=>$this->input->post('x'),
	    				'y'=>$this->input->post('y'),
	    				'x2'=>$this->input->post('x2'),
	    				'y2'=>$this->input->post('y2'),
						'width'=>$this->input->post('width'),
						'height'=>$this->input->post('height')
					);


	    if(is_array($file)){

	    	$params = array_merge($params,array('file_path'=>$file['file_path'],'full_path'=>$file['full_path'],'file_name'=>$file['file_name']));	    	

	    }else{

	    	$this->load->helper('file');
	    	$file = get_file_info('uploads/about/'.$this->uri->segment(1).'/'.$this->input->post('current_image'));
	    	$file['file_name'] = $this->input->post('current_image');
	    	$params = array_merge($params,array('file_path'=>'uploads/about/'.$this->uri->segment(1),'full_path'=>$file['server_path'],'file_name'=>$this->input->post('current_image')));
	    }

	    crop($params);	    

	    $params = array('width'=>50,'height'=>50,'source_image'=>'uploads/about/'.$this->uri->segment(1).'/thumb_'.$file['file_name'],'new_image_path'=>'uploads/about/'.$this->uri->segment(1).'/','file_name'=>$file['file_name']);
	    resize($params);

	    $params = array('width'=>331,'height'=>176,'source_image'=>'uploads/about/'.$this->uri->segment(1).'/thumb_'.$file['file_name'],'new_image_path'=>'uploads/about/'.$this->uri->segment(1).'/','file_name'=>$file['file_name']);
	    resize($params);


	    

	    return (is_array($file) && count($file) > 0) ? $file['file_name'] : '';

	}

	public function get_filters()
	   {
		   
		   $where_filters = array('status'=>'status','DATE(date_added) >='=>'from_date_added','DATE(date_added) <='=>'to_date_added');
		   $like_filters = array('title'=>'title','description'=>'description');
		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
		   
	   } 

	   public function export()
	   {
	   		$this->load->library('to_excel_array');
		   	$filters = $this->get_filters();
		   	$where_filters = array_merge(array('is_deleted'=>0),$filters['where_filters']);

		   	$rows = $this->explore_model->get_rows(array('table'=>'tbl_about_news',
															 'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
														     'like'=>$filters['like_filters'],
														     'order_by'=>array('field'=>'date_added','order'=>'DESC')
														     )
														);
			$res[] = array('Title','Description','Image','Date Added');
			if($rows->num_rows()){
				foreach($rows->result() as $v){
					$image = BASE_URL.'/about/news/'.$v->image;
					$status = ($v->status) ? 'Published' : 'Unpublished';
					$res[] = array($v->title,strip_tags($v->description),$image,$status,$v->date_added);
				}
			}

		   	$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));
		    
	   }

	   public function view_photo()
	   {
	   	 $id = $this->uri->segment(3);
	   	 $data['row'] = $this->explore_model->get_row(array('table'=>'tbl_about_news',
													  'where'=>array('about_news_id'=>$id)
 													)
												);
	   	 $this->load->view('about/news/view-photo',$data);

	   }

 
}