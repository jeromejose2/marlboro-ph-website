<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class User_points extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');

 	} 

 	 
 	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
		//$this->output->enable_profiler(TRUE);

 	}
 	 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
		$this->load->helper('paging');
		$offset = (int)$this->input->get('per_page');
	    $limit = PER_PAGE;
	    $filters = $this->get_filters(); 

		$access = $this->module_model->check_access('user_points');
		$total_rows = $this->explore_model->get_rows(array('table'=>'tbl_points as p',
															 'where'=>array_merge($filters['where_filters'],array()),
														     'like'=>$filters['like_filters'],
														     'join'=>array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=p.registrant_id'),
 														     'fields'=>"p.points_id",
														     'group_by'=>'p.registrant_id'
 														     )
														)->num_rows();
		
 		$data['header_text'] = 'User Points';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total_rows'] = $total_rows;
		$data['query_strings'] = $filters['query_strings'];
 		
		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_points as p',
															 'where'=>array_merge($filters['where_filters'],array()),
														     'like'=>$filters['like_filters'],
														     'join'=>array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=p.registrant_id'),
														     'order_by' => array('field'=>'r.first_name','order'=>'ASC'),
														     'offset'=>$offset,
														     'limit'=>$limit,
														     'fields'=>"r.first_name,r.third_name, SUM(p.points) as points, p.registrant_id, r.total_points, r.starting_points, r.person_id",
														     'group_by'=>'p.registrant_id'
 														     )
														);
  		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']);
 		return $this->load->view($this->uri->segment(1).'/list',$data,TRUE);



	} 

	

	public function get_filters()
	   {
		   
		   $where_filters = array('DATE(date_earned) >='=>'from_date_earned','DATE(date_earned) <='=>'to_date_earned','registrant_id'=>'registrant_id');
		   $like_filters = array("CONCAT(first_name,' ',third_name)"=>'name');
		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
		   
	   } 


	   public function export()
	   {
	   		ini_set('memory_limit', '1024M');
	   		$from = $this->input->get('from_date_earned') ? $this->input->get('from_date_earned') : date('Y-m-d', strtotime('-30 days'));
	   		$to = $this->input->get('to_date_earned') ? $this->input->get('to_date_earned') : date('Y-m-d');
	   		$this->load->library('to_excel_array');
	   		$res[] = array('User Points (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')');

		   	$filters = $this->get_filters();
		   	$params = 
		   	$query = $this->explore_model->get_rows(array('table'=>'tbl_points as p',
															 'where'=>array_merge($filters['where_filters'],array()),
														     'like'=>$filters['like_filters'],
														     'join'=>array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=p.registrant_id'),
														     'order_by' => array('field'=>'r.first_name','order'=>'ASC'),
														     'fields'=>"r.first_name,r.third_name, r.email_address, SUM(p.points) as points, p.registrant_id,DATE(p.date_earned) AS dearned, r.total_points, r.person_id",
														     'group_by'=>'p.registrant_id'
 														     )
														);
		   	if($query->num_rows()){
		   		$res[] = array('Person ID', 'Name', 'Email', 'Current Points');
		   		$cur_user = 0;
		   		foreach( $query->result() as $v ) {
		   			// if( $cur_user != $v->registrant_id ) {
		   				$res[] = array($v->person_id, $v->first_name.' '.$v->third_name, $v->email_address, $v->total_points);
		   				$cur_user = $v->registrant_id;
		   			// }
		   		}
		   	}
		   	$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));

	   }

	   public function export_backup()
	   {
	   	ini_set('memory_limit', '1024M');
   		$from = $this->input->get('from_date_earned') ? $this->input->get('from_date_earned') : date('Y-m-d', strtotime('-30 days'));
   		$to = $this->input->get('to_date_earned') ? $this->input->get('to_date_earned') : date('Y-m-d');
   		$this->load->library('to_excel_array');
   		$res[] = array('User Points (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')');

	   	$filters = $this->get_filters();
	   	$params = 
	   	$query = $this->explore_model->get_rows(array('table'=>'tbl_points as p',
														 'where'=>array_merge($filters['where_filters'],array()),
													     'like'=>$filters['like_filters'],
													     'join'=>array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=p.registrant_id'),
													     'order_by' => array('field'=>'r.first_name','order'=>'ASC'),
													     'fields'=>"r.first_name,r.third_name, r.email_address, SUM(p.points) as points, p.registrant_id,DATE(p.date_earned) AS dearned, r.total_points, r.person_id",
													     'group_by'=>'p.registrant_id, DATE(p.date_earned)'
														     )
													);
	   
	   	if($query->num_rows()){
	   		$start = strtotime($from);
	   		$end = strtotime($to);
	   		$fields = array('Person ID', 'Name', 'Email', 'Current Points');
	   		$dates = array();
	   		// while($start <= $end) {
	   		// 	$fields[] = date('m/d', $start);
	   		// 	$dates[] = date('Ymd', $start);
	   		// 	$start = strtotime('+1 day', $start);	
	   		// }
	   		$res[] = $fields;

	   		$cur_user = 0;
	   		$rows = array();
	   		foreach($query->result() as $v){
	   			if($cur_user != $v->registrant_id) {
		   				$res[] = $rows;
		   				$rows = array($v->person_id, $v->first_name . ' ' . $v->third_name, $v->email_address, $v->total_points);
		   			} 

		   		// 	foreach($dates as $k => $date) {
   				// 	if(date('Ymd', strtotime($v->dearned)) == $date) {
   				// 		$rows[$k + 2] = $v->points;
   				// 	} elseif(!isset($rows[$k + 2])) {
   				// 		$rows[$k + 2] = '0';
   				// 	}
   				// 	//echo date('Ymd', strtotime($v->dlogin))  . ' ' .  $date . ' ' . $v->registrant_id . ' = ' . $rows[$k + 2] . '<br>';
   				// }
		   			$cur_user = $v->registrant_id;
		   			//$res[] = array($v->first_name . ' ' . $v->third_name,$v->points, $v->date_earned);
	   		}
	   		$res[] = $rows;
	   	}
	   	$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));
	   }

	   public function export2()
	   {
	   	$from = $this->input->get('from_date_earned') ? $this->input->get('from_date_earned') : date('Y-m-d', strtotime('-30 days'));
   		$to = $this->input->get('to_date_earned') ? $this->input->get('to_date_earned') : date('Y-m-d');
   		$this->load->library('to_excel_array');
   		$res[] = array('User Points (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')');

	   	$filters = $this->get_filters();
	   	$params = 
	   	$query = $this->explore_model->get_rows(array('table'=>'tbl_points as p',
														 'where'=>array_merge($filters['where_filters'],array()),
													     'like'=>$filters['like_filters'],
													     'join'=>array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=p.registrant_id'),
													     'order_by' => array('field'=>'r.registrant_id','order'=>'ASC'),
													     'fields'=>"r.first_name,r.third_name, r.email_address, SUM(p.points) as points, p.registrant_id,DATE(p.date_earned) AS dearned",
													     'group_by'=>'p.registrant_id, DATE(p.date_earned)'
														     )
													);
	   
	   	if($query->num_rows()){
	   		$start = strtotime($from);
	   		$end = strtotime($to);
	   		$fields = array('Name', 'Email');
	   		$dates = array();
	   		while($start <= $end) {
	   			$fields[] = date('m/d', $start);
	   			$dates[] = date('Ymd', $start);
	   			$start = strtotime('+1 day', $start);	
	   		}
	   		$res[] = $fields;

	   		$cur_user = 0;
	   		$rows = array();
	   		foreach($query->result() as $v){
	   			if($cur_user != $v->registrant_id) {
		   				$res[] = $rows;
		   				$rows = array($v->first_name . ' ' . $v->third_name, $v->email_address, $v->total_points);
		   			} 

		   		// 	foreach($dates as $k => $date) {
   				// 	if(date('Ymd', strtotime($v->dearned)) == $date) {
   				// 		$rows[$k + 2] = $v->points;
   				// 	} elseif(!isset($rows[$k + 2])) {
   				// 		$rows[$k + 2] = '0';
   				// 	}
   				// 	//echo date('Ymd', strtotime($v->dlogin))  . ' ' .  $date . ' ' . $v->registrant_id . ' = ' . $rows[$k + 2] . '<br>';
   				// }
		   			$cur_user = $v->registrant_id;
		   			//$res[] = array($v->first_name . ' ' . $v->third_name,$v->points, $v->date_earned);
	   		}
	   		$res[] = $rows;
	   	}
	   	$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));
	   }

	   public function view_points()
	   {
	   		$registrant_id = $this->input->get('registrant_id');
	   		$filters = $this->get_filters();
	   		$data['registrant'] = $this->explore_model->get_row(array('table'=>'tbl_registrants',
	   																	'where'=>array('registrant_id'=>$registrant_id)
	   																)
 	   															);
	   		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_points',
		   														'where'=>$filters['where_filters'],
		   														'order_by'=>array('field'=>'date_earned','order'=>'ASC')
		   														)
	   														);
	   		$sorted = $data['rows']->result_array();
	   		$this->array_sort_by_column($sorted, 'points');
	   		$data['max'] = $sorted[count($sorted) - 1]['points'];
	   		$data['mean'] = $this->get_average($sorted);
	   		$count = count($sorted);
	   		if($count % 2 == 0) {
	   			$data['median'] = number_format(($sorted[$count/2]['points'] + $sorted[($count/2) - 1]['points'])/2, 2);
	   		} else {
	   			$data['median'] = $sorted[floor($count/2)]['points'];	
	   		}

	   		$this->load->view('user_points/breakdown-points',$data);
	   }

	   public function view_points_new()
	   {
	   	$registrant_id = $this->input->get('registrant_id');
	   	$filters = $this->get_filters();
	   	$data['registrant'] = $this->explore_model->get_row(array('table'=>'tbl_registrants', 'where'=>array('registrant_id'=>$registrant_id)));
	   	$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_points', 'where'=>$filters['where_filters'], 'order_by'=>array('field'=>'date_earned', 'order'=>'ASC')));

	   	$this->load->view('user_points/breakdown-points-new', $data);
	   }

	   private function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
			    $sort_col = array();
			    foreach ($arr as $key=> $row) {
			        $sort_col[$key] = $row[$col];
			    }

		    array_multisort($sort_col, $dir, $arr);
		}

		private function get_average($arr) {
			 $total = 0;
			 if($arr) {
			 	foreach ($arr as $key => $value) {
			 		$total += $value['points'];
			 	}
			 }
			 if($total == 0)
			 	return 0;
			 return number_format($total / count($arr), 2);
		}

 
}
