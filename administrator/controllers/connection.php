<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Connection extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
 	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

 	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
		$this->load->helper('paging');
		$offset = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
	    $limit = PER_PAGE;
	    $filters = $this->get_filters();

		$access = $this->module_model->check_access('registrant');
		$total_rows = $this->explore_model->get_total_rows(array('table'=>'tbl_explore',
																 'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
																 'like'=>$filters['like_filters']
																 )
														    );
 
		$data['header_text'] = 'Connections';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total_rows'] = $total_rows;

		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_explore',
															 'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
														     'like'=>$filters['like_filters']
														     )
														);
		$data['pagination'] = paging($total_rows,$limit,$offset,site_url('/product?default=0'.$filters['query_strings'])); 

 		return $this->load->view('explore/list',$data,TRUE);
	}


	public function add()
	{

		$error = '';
		$row = '';

		if($this->input->post()) {

			$post_data = $this->input->post();		

			if($this->validate_form()) {
 
				
 				$data = $post_data; 
				$data['date_added'] = TRUE; // Set NOW() value to date_added column field
				$data['image_crop_coordinates'] = serialize(array('x1'=>$this->input->post('x'),
 															 'y1'=>$this->input->post('y'),
 															 'x2'=>$this->input->post('x2'),
 															 'y2'=>$this->input->post('y2'),
 															 'width'=>$this->input->post('width'),
 															 'height'=>$this->input->post('height')
 															 ));
 
 
				$id = $this->explore_model->insert('tbl_explore',$data);
				$image_filename = $this->do_upload($id,'image');

				if($image_filename) 
	   		    	$this->explore_model->update('tbl_explore',array('image'=>$image_filename),array('explore_id'=>$id));

	   		    redirect($this->uri->segment(1));

			} else {

				$post_data['image'] = '';
				$error = validation_errors();
				$row = json_decode(json_encode($post_data),false);

			}


			
 
		}


		$cat = $this->explore_model->get_row(array('table'=>'tbl_categories','where'=>array('LCASE(category_name)'=>'Connections'),'fields'=>'category_id'));
		$parent_id = ($cat) ? $cat->category_id : null;
		

		$data['error'] = $error;
		$data['row'] = $row;
 		$data['header_text'] = 'Connections';
		$data['categories'] = $this->explore_model->get_rows(array('table'=>'tbl_categories','where'=>array('parent'=>$parent_id)));
		$data['main_content'] = $this->load->view('explore/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
		$this->output->enable_profiler(TRUE);

	}


	public function edit()
	{

		$error = '';
		$id = $this->uri->segment(3);

		if($this->input->post())
		{
			$post_data = $this->input->post();

			if($this->validate_form()){

 				$post_data['date_updated']	= TRUE; // Set NOW() value to column date_updated 
 				$post_data['image_crop_coordinates'] = serialize(array('x1'=>$this->input->post('x'),
 															 'y1'=>$this->input->post('y'),
 															 'x2'=>$this->input->post('x2'),
 															 'y2'=>$this->input->post('y2'),
 															 'width'=>$this->input->post('width'),
 															 'height'=>$this->input->post('height')
 															 ));
				$this->explore_model->update('tbl_explore',$post_data,array('explore_id'=>$id,'is_deleted'=>0));
				$this->do_upload($id,'image');
				redirect($this->uri->segment(1));

			}else{

				$error = validation_errors();
				$post_data['image'] = $post_data['current_image'];
				$row = json_decode(json_encode($post_data),false);

			}


		}else{

			$row = $this->explore_model->get_row(array('table'=>'tbl_explore','where'=>array('explore_id'=>$id,'is_deleted'=>0)));

		}

 		
		$cat = $this->explore_model->get_row(array('table'=>'tbl_categories','fields'=>'category_id','where'=>array('LCASE(category_name)'=>'passion')));
		$parent_id = ($cat) ? $cat->category_id : null;

		$data['row'] = $row;
		$data['error'] = $error;
		$data['header_text'] = 'Passion';
		$data['categories'] = $this->explore_model->get_rows(array('table'=>'tbl_categories','where'=>array('parent'=>$parent_id)));
		$data['main_content'] = $this->load->view('explore/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

		$this->output->enable_profiler(TRUE);

	}


	public function delete()
	{

 		$id = $this->uri->segment(3);
 		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
 			$this->explore_model->update('tbl_explore',array('is_deleted'=>1),array('explore_id'=>$id));
		}
		redirect($this->uri->segment(1));

	}

	public function validate_form()
	{

		$this->load->library('form_validation');

		$rules = array(
		   array(
				 'field'   => 'title',
				 'label'   => 'Title',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'description',
				 'label'   => 'Description',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'category_id',
				 'label'   => 'Category',
				 'rules'   => 'required'
			  )
		);

		$this->form_validation->set_rules($rules);
		
		return $this->form_validation->run();
	}




	public function do_upload($new_filename,$upload_file)
	{
		$this->load->helper(array('upload','resize','crop'));

		$params = array('upload_path'=>'uploads/explore/'.$this->uri->segment(1).'/','allowed_types'=>'png|jpg|gif','file_name'=>$new_filename,'max_size'=>'2097152','max_width'=>0,'max_height'=>0,'do_upload'=>$upload_file);
	    $file = upload($params);

	    $params = array('file_path'=>$file['file_path'],
	    				'full_path'=>$file['full_path'],
	    				'file_name'=>$file['file_name'],
	    				'x'=>$this->input->post('x'),
	    				'y'=>$this->input->post('y'),
	    				'x2'=>$this->input->post('x2'),
	    				'y2'=>$this->input->post('y2'),
						'width'=>$this->input->post('width'),
						'height'=>$this->input->post('height')
					);
	    crop($params);

	    $params = array('width'=>50,'height'=>50,'source_image'=>'uploads/explore/'.$this->uri->segment(1).'/thumb_'.$file['file_name'],'new_image_path'=>'uploads/explore/'.$this->uri->segment(1).'/','file_name'=>$file['file_name']);
	    resize($params);

	    return (is_array($file)) ? $file['file_name'] : '';

	}

	public function get_filters()
	   {
		   
		   $where_filters = array('status');
		   $like_filters = array('title','description');
		   $query_strings = $this->input->get();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $key=>$v){
			    
 				if($this->input->get($key)){
					$valid_where_filters[$v] = trim($this->input->get($key));
				}else if($this->input->get($v) || $this->input->get($v) === '0'){
					$valid_where_filters[$v] = trim($this->input->get($v));
				}
				  
 		   } 
		    
		   
		   foreach($like_filters as $key=>$v){
			   
 				if($this->input->get($key)){
					$valid_like_filters[$v] = trim($this->input->get($key));
				}else if($this->input->get($v)){
					$valid_like_filters[$v] = trim($this->input->get($v));
				}
				   
		   }

		   if(isset($query_strings['per_page']))
		   		unset($query_strings['per_page']);

		   	if(isset($query_strings['default']))
		   		unset($query_strings['default']);


 		   $query_strings = ($this->input->get()) ? http_build_query(array_filter($this->input->get())) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?default=0'.$query_strings);		   
		   
	   } 

	   public function export()
	   {

		   	$filters = $this->get_filters();
		   	$params = 
		   	$data['query'] = $this->explore_model->get_rows(array('fields'=>"title,description, date_added",
		   														'table'=>'tbl_explore_trends',
											   					'where'=>$filters['where_filters'],
											   					'like'=>$filters['like_filters']
											   					)
		   													);
		   	$data['filename'] = $this->uri->segment(1).'_'.date('M-d-Y');
		    $this->load->view('explore/export-to-excell',$data);
	   }

 
}