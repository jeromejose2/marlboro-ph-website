<?php

class Brand_history extends CI_Controller
{
	public function index()
	{
		$data = array();

		$this->db->start_cache();

		$search = array();
		$search['title'] = (string) $this->input->get('title');
		$search['status'] = (string) $this->input->get('status');
		$search['from_date'] = (string) $this->input->get('from_date');
		$search['to_date'] = (string) $this->input->get('to_date');

		$page = $this->uri->segment(2, 1);
		$this->db->select()
			->from('tbl_brand_history');

		if ($search['title']) {
			$this->db->like('brand_history_title', $search['title']);
		}

		if ($search['status'] != '') {
			$this->db->where('brand_history_status', $search['status']);
		}

		if ($search['from_date']) {
			$this->db->where('brand_history_date_created >=', $search['from_date']);
		}

		if ($search['to_date']) {
			$this->db->where('brand_history_date_created <=', $search['to_date']);
		}

		$data['offset'] = ($page - 1) * PER_PAGE;
		$data['total'] = $this->db->count_all_results();
		$this->db->limit(PER_PAGE, $data['offset']);
		$data['records'] = $this->db->get()->result();

		$this->db->stop_cache();
		$this->db->flush_cache();

		$data['pagination'] = $this->global_model->pagination($data['total'], $page, SITE_URL.'/brand_history');
		$data['access'] = $this->module_model->check_access('brand_history');

		$data['nav'] = $this->nav_items();
		$data['main_content'] = $this->load->view('brand_history/index', $data, true);
		$this->load->view('main-template', $data);
	}

	public function edit()
	{
		$data = array(
			'error' => '',
			'nav' => $this->nav_items()
		);

		$id = $this->input->get('id');
		if (!$id) {
			redirect('brand_history');
		}

		$data['history'] = $this->db->select()
			->from('tbl_brand_history')
			->where('brand_history_id', $id)
			->limit(1)
			->get()
			->row();

		if (!$data['history']) {
			show_404();
		}

		$post = $this->input->post();
		if ($post) {

			$this->load->helper('resize');
			$this->load->library('upload');
			$this->load->library('form_validation');
			if (isset($_POST['title'])) {
				$this->form_validation->set_rules('title', 'required|alpha_numeric_space');
			}
			$this->form_validation->set_rules('status',	'required|numeric');
			$this->form_validation->set_rules('year', 'required|numeric');

			$path = './uploads/brand_history/';
			if (!is_dir($path)) {
				mkdir($path);
			}
			if ($this->form_validation->run()) {

				$title = null;
				$title_is_image = 2;
				if (!isset($_POST['title'])) {

					if (!empty($_FILES['brand_history_title']['name'])) {
						$config = array();
						$config['upload_path'] = $path;
						$config['allowed_types'] = FILE_UPLOAD_ALLOWED_TYPES;
						$config['max_size'] = FILE_UPLOAD_LIMIT;
						$config['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['brand_history_title']['name'], PATHINFO_EXTENSION);

						$this->upload->initialize($config);
						if (!$this->upload->do_upload('brand_history_title')) {
							$data['error'] = $this->upload->display_errors();
						} else {
							resize(array(
								'width' => 150,
								'height'=> 150,
								'source_image' => $path.$config['file_name'],
								'new_image_path' => $path,
								'file_name'=> $config['file_name']
							));
							$title = $config['file_name'];
						}
					}

				} else {
					$title = $this->input->post('title');
					$title_is_image = 1;
				}

				if (!$data['error']) {

					$config = array();
					$config['upload_path'] = $path;
					$config['allowed_types'] = BRAND_HISTORY_FILE_UPLOAD_TYPES;
					$config['max_size'] = FILE_UPLOAD_LIMIT;
					$config['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['brand_history_upload']['name'], PATHINFO_EXTENSION);

					$this->upload->initialize($config);

					$updateData = array(
						'brand_history_title_type' => $title_is_image,
						'brand_history_description' => $post['description'],
						'brand_history_explore_copy' => $post['brand_history_explore_copy'],
						'brand_history_explore_link' => $post['brand_history_explore_link'],
						'brand_history_year' => $post['year'],
						'brand_history_year_desc' => $post['year_desc'],
						'brand_history_status' => $post['status'],
					);

					if ($title) {
						$updateData['brand_history_title'] = $title;
					}

					if (!empty($_FILES['brand_history_upload']['name'])) {
						if ($this->upload->do_upload('brand_history_upload')) {
							$updateData['brand_history_type'] = $this->upload->data()['is_image'] ? 1 : 2;
							$uploadDataSuccess = $this->upload->data();
							if ($uploadDataSuccess['is_image']) {
								resize(array(
									'width' => 483,
									'height'=> 262,
									'source_image' => $path.$config['file_name'],
									'new_image_path' => $path,
									'file_name'=> $config['file_name']
								));

								resize(array(
									'width' => 150,
									'height'=> 150,
									'source_image' => $path.$config['file_name'],
									'new_image_path' => $path,
									'file_name'=> $config['file_name']
								));
							}
							$updateData['brand_history_media'] = $config['file_name'];
						} else {
							$data['error'] = $this->upload->display_errors();
						}
					}

					if (!$data['error']) {

						$cms_user_id = $this->login_model->extract_user_details()['cms_user_id'];

						$this->db->where('brand_history_id', $id);
						$this->db->update('tbl_brand_history', $updateData);
						redirect('brand_history');
					
					}
				}
				
			} else {
				$data['error'] = validation_errors();
			}
			
		}

		
		$data['main_content'] = $this->load->view('brand_history/edit', $data, true);
		$this->load->view('main-template', $data);
	}

	public function add()
	{
		$data = array(
			'error' => ''
		);

		$post = $this->input->post();
		if ($post) {

			$this->load->helper('resize');
			$this->load->library('upload');
			$this->load->library('form_validation');
			if (isset($_POST['title'])) {
				$this->form_validation->set_rules('title', 'required|alpha_numeric_space');
			}
			$this->form_validation->set_rules('status',	'required|numeric');
			$this->form_validation->set_rules('year', 'required|numeric');

			if ($this->form_validation->run()) {

				$config = array();
				$path = './uploads/brand_history/';
				if (!is_dir($path)) {
					mkdir($path);
				}

				$title = null;
				$title_is_image = 2;
				if (!isset($_POST['title'])) {
					$config['upload_path'] = $path;
					$config['allowed_types'] = FILE_UPLOAD_ALLOWED_TYPES;
					$config['max_size'] = FILE_UPLOAD_LIMIT;
					$config['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['brand_history_title']['name'], PATHINFO_EXTENSION);

					$this->upload->initialize($config);
					if (!$this->upload->do_upload('brand_history_title')) {
						$data['error'] = $this->upload->display_errors();
					} else {
						resize(array(
							'width' => 150,
							'height'=> 150,
							'source_image' => $path.$config['file_name'],
							'new_image_path' => $path,
							'file_name'=> $config['file_name']
						));
						$title = $config['file_name'];
					}

				} else {
					$title = $this->input->post('title');
					$title_is_image = 1;
				}

				if (!$data['error']) {

					$config = array();
					if (!empty($_FILES['brand_history_upload']['name'])) {

						$config['upload_path'] = $path;
						$config['allowed_types'] = BRAND_HISTORY_FILE_UPLOAD_TYPES;
						$config['max_size'] = FILE_UPLOAD_LIMIT;
						$config['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['brand_history_upload']['name'], PATHINFO_EXTENSION);

						$this->upload->initialize($config);
						if (!$this->upload->do_upload('brand_history_upload')) {
							$data['error'] = $this->upload->display_errors();
						} else {
							$uploadSuccessData = $this->upload->data();
							if ($uploadSuccessData) {
								if ($uploadSuccessData['is_image']) {
									resize(array(
										'width' => 483,
										'height'=> 262,
										'source_image' => $path.$config['file_name'],
										'new_image_path' => $path,
										'file_name'=> $config['file_name']
									));

									resize(array(
										'width' => 150,
										'height'=> 150,
										'source_image' => $path.$config['file_name'],
										'new_image_path' => $path,
										'file_name'=> $config['file_name']
									));
								}
							}
						}
					}
					
					if (!$data['error']) {
						$cms_user_id = $this->login_model->extract_user_details()['cms_user_id'];
						$a = 0;
						if (isset($uploadSuccessData['is_image'])) {
							$a = $uploadSuccessData['is_image'] ? 1 : 2;
						}
						
						$this->db->insert('tbl_brand_history', array(
							'brand_history_media' => isset($config['file_name']) ? $config['file_name'] : '',
							'brand_history_title' => $title,
							'brand_history_title_type' => $title_is_image,
							'brand_history_description' => $post['description'],
							'brand_history_explore_copy' => $post['brand_history_explore_copy'],
							'brand_history_explore_link' => $post['brand_history_explore_link'],
							'brand_history_type' => $a,
							'brand_history_date_created' => date('Y-m-d H:i:s'),
							'brand_history_status' => $post['status'],
							'brand_history_year' => $post['year'],
							'brand_history_year_desc' => $post['year_desc'],
							'cms_user_id' => $cms_user_id
						));
						if ($this->db->affected_rows()) {
							redirect('brand_history');
						}
					}

				}
				
			} else {
				$data['error'] = validation_errors();
			}
			
		}

		$data['nav'] = $this->nav_items();
		$data['main_content'] = $this->load->view('brand_history/add', $data, true);
		$this->load->view('main-template', $data);
	}

	public function delete()
	{
		$id = $this->input->get('id');
		if ($id) {
			$this->db->where('brand_history_id', $id)
				->delete('tbl_brand_history');
		}
		redirect($this->input->server('HTTP_REFERER'));
	}

	public function change_status()
	{
		$id = $this->input->get('id');
		if ($id) {
			$this->db->where('brand_history_id', $id)
				->update('tbl_brand_history', array(
					'brand_history_status' => (int) $this->input->get('status')
				));
		}
		redirect($this->input->server('HTTP_REFERER'));
	}

	private function nav_items()
	{
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}

	public function export()
	{
		$this->load->library('to_excel_array');

		$search = array();
		$search['title'] = (string) $this->input->get('title');
		$search['status'] = (string) $this->input->get('status');
		$search['from_date'] = (string) $this->input->get('from_date');
		$search['to_date'] = (string) $this->input->get('to_date');

		$rows = array(array(
			'brand_history_media' => 'Media',
			'brand_history_title' => 'Title',
			'brand_history_year' => 'Year',
			'brand_history_status' => 'Status',
			'brand_history_date_created' => 'Date Created'
		));
		$this->db->select(implode(',', array_keys($rows[0])))
			->from('tbl_brand_history');

		if ($search['title']) {
			$this->db->like('brand_history_title', $search['title']);
		}

		if ($search['status'] != '') {
			$this->db->where('brand_history_status', $search['status']);
		}

		if ($search['from_date']) {
			$this->db->where('brand_history_date_created >=', $search['from_date']);
		}

		if ($search['to_date']) {
			$this->db->where('brand_history_date_created <=', $search['to_date']);
		}

		foreach ($this->db->get()->result_array() as $data) {
			if ($data['brand_history_status'] == 1) {
				$data['brand_history_status'] = 'Published';
			} else {
				$data['brand_history_status'] = 'Not Published';
			}
			if ($data['brand_history_title_type'] == 2) {
				$data['brand_history_title'] = BASE_URL.'uploads/brand_history/'.$data['brand_history_title'];
			}
			$data['brand_history_media'] = BASE_URL.'uploads/brand_history/'.$data['brand_history_media'];
			$rows[] = $data;
		}
		$this->to_excel_array->to_excel($rows, 'Brand_history_'.date('YmdHis'));
	}

}