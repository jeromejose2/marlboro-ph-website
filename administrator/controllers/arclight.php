<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Arclight extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		set_time_limit(0);
		ini_set('memory_limit', '3000M');
		error_reporting(E_ALL);
		ini_set('display_errors', "1");
	}

	public function inbound()
	{
		$data = array();
		$page = $this->uri->segment(3, 1);
		$data['offset'] = ($page - 1) * PER_PAGE;
		$data['search'] = array();
		$data['search']['filename'] = (string) $this->input->get('filename');
		if ($data['search']['filename'] && pathinfo($data['search']['filename'], PATHINFO_EXTENSION) == 'xml')
			$data['search']['filename'] = pathinfo($data['search']['filename'], PATHINFO_FILENAME);

		$data['search']['from_date'] = (string) $this->input->get('from_date');
		$data['search']['to_date'] = (string) $this->input->get('to_date');
		$this->db->start_cache();
		$this->db->select()
			->from('tbl_arclight_inbound');
		if ($data['search']['filename']) {
			$this->db->like('inbound_filename', $data['search']['filename']);
		}
		if ($data['search']['from_date']) {
			$this->db->where('date_of_processing >=', $data['search']['from_date']);
		}
		if ($data['search']['to_date']) {
			$this->db->where('date_of_processing <=', $data['search']['to_date']);
		}
		$data['total'] = $this->db->count_all_results();
		$this->db->order_by('date_of_processing', 'DESC');
		$this->db->limit(PER_PAGE, $data['offset']);
		$data['records'] = $this->db->get()->result();
		$this->db->stop_cache();
		$this->db->flush_cache();

		$data['pagination'] = $this->global_model->pagination($data['total'], $page , SITE_URL.'/arclight/inbound');

		$data['nav'] = $this->nav_items();
		$data['main_content'] = $this->load->view('arclight/inbound', $data, true);
		$this->load->view('main-template', $data);
	}

	public function outbound()
	{
		$data = array();
		$page = $this->uri->segment(3, 1);
		$data['offset'] = ($page - 1) * PER_PAGE;
		$data['search'] = array();
		$data['search']['filename'] = (string) $this->input->get('filename');
		if ($data['search']['filename'] && !pathinfo($data['search']['filename'], PATHINFO_EXTENSION))
			$data['search']['filename'] = $data['search']['filename'].'.xml';

		$data['search']['from_date'] = (string) $this->input->get('from_date');
		$data['search']['to_date'] = (string) $this->input->get('to_date');
		$this->db->start_cache();
		$this->db->select()
			->from('tbl_arclight_outbound');
		if ($data['search']['filename']) {
			$this->db->like('outbound_filename', $data['search']['filename']);
		}
		if ($data['search']['from_date']) {
			$this->db->where('date_fetched >=', $data['search']['from_date']);
		}
		if ($data['search']['to_date']) {
			$this->db->where('date_fetched <=', $data['search']['to_date']);
		}
		$data['total'] = $this->db->count_all_results();
		$this->db->order_by('date_fetched', 'DESC');
		$this->db->limit(PER_PAGE, $data['offset']);
		$data['records'] = $this->db->get()->result();
		$this->db->stop_cache();
		$this->db->flush_cache();

		$data['pagination'] = $this->global_model->pagination($data['total'], $page , SITE_URL.'/arclight/outbound');

		$data['nav'] = $this->nav_items();
		$data['main_content'] = $this->load->view('arclight/outbound', $data, true);
		$this->load->view('main-template', $data);
	}

	public function outbound_fetch()
	{
		$filename = $this->input->post('outbound_filename');
		if ($filename) {
			$withEmail = $this->input->post('outbound_with_email') ? true : false;
			$this->load->model('Arclight_Model');
			$this->Arclight_Model->pull($filename, $withEmail);
		}
		redirect('arclight/outbound');
	}

	public function outbound_match()
	{
		$data = array(
			'outbound_records_count' => 0,
			'registrants_inserted' => array(),
			'registrants_not_inserted' => array(),
			'duplicates_info' => array(),
			'not_found' => null,
			'success_inserted' => false,
			'duplicates' => array()
		);
		$data['nav'] = $this->nav_items();
		$filename = $this->input->get('outbound_filename');
		if ($filename) {
			$dbConn = null;
			if (!PROD_MODE && $this->input->get('prod')) {
				require 'administrator/config/database.php';
				$dbConn = $this->load->database($db['prod'], true);
			} else {
				$dbConn = $this->db;
			}

			$this->load->model('Arclight_Model');
			$registrants = $dbConn->select()
				->from('tbl_registrants')
				->get()
				->result_array();
			$records = array();
			if (!file_exists('./uploads/outbound/'.$filename) || $this->input->get('reload')) {
				$records = $this->Arclight_Model->get_outbound_records($filename);
				if (!is_dir('./uploads/outbound')) {
					mkdir('./uploads/outbound');
				}
				if ($records) {
					$records->asXml('./uploads/outbound/'.$filename);
				} else {
					$data['not_found'] = false;
				}
			} else {
				$records = new SimpleXMLElement(file_get_contents('./uploads/outbound/'.$filename));
			}
			
			$registrantsFilterField = 'email_address';
			$outboundFilterField = 'EmailAddress';
			if ($records) {
				$individuals = array();
				$individualIds = array();
				$outboundRecords = array();
				$records = $records->children();
				$data['outbound_records_count'] = count($records);
				foreach ($records as $rec) {
					$id = (string) $rec->IndividualID;
					$email = strtolower((string) $rec->EmailAddress);
					$rec = (array) $rec;
					foreach ($rec as &$r) {
						if (is_object($r)) {
							$r = (array) $r;
							if (!$r) {
								$r = '';
							}
						}
					}
					if ($email) {
						$individuals[$email] = 0;
						$outboundRecords[$email] = $rec;
					}
					if ($id) {
						$individualIds[$id] = $rec;
					}
				}

				if ($individuals) {

					foreach ($individualIds as $id => $val) {
						if (!isset($data['duplicates'][$val[$outboundFilterField]])) {
							$data['duplicates'][$val[$outboundFilterField]] = 1;
							$data['duplicates_info'][$val[$outboundFilterField]] = array($val);
						} else {
							$data['duplicates'][$val[$outboundFilterField]]++;
							$data['duplicates_info'][$val[$outboundFilterField]][] = $val;
						}
					}
					
					foreach ($data['duplicates'] as $email => $duplicate) {
						if ($duplicate === 1) {
							unset($data['duplicates'][$email], $data['duplicates_info'][$email]);
						}
					}

					foreach ($registrants as $registrant) {
						$registrantFilterId = strtolower($registrant[$registrantsFilterField]);
						if (isset($individuals[$registrantFilterId])) {
							$data['registrants_inserted'][] = $outboundRecords[$registrantFilterId];
							$individuals[$registrantFilterId]++;
						}
					}

					foreach ($individuals as $id => $val) {
						if (!$val && !isset($data['duplicates'][strtoupper($id)])) {
							$data['registrants_not_inserted'][] = $outboundRecords[$id];
						}
					}

					if ($this->input->get('insert') && $data['registrants_not_inserted']) {

						$govIds = $dbConn->select()
							->from('tbl_giid_types')
							->get()
							->result_array();
						foreach ($govIds as &$g) {
							$g = $g['giid_type_id'];
						}

						$brands = $dbConn->select()
							->from('tbl_brands')
							->get()
							->result_array();

						$this->load->library('Encrypt');
						require_once 'application/helpers/site_helper.php';

						$countNew = 0;
						$batchLimit = 500;
						$batch = 1;
						$forEdmSending = array();
						foreach ($data['registrants_not_inserted'] as $new_reg) {

							$salt = generate_password(20);
							$passwd = $this->encrypt->encode(generate_password(), $salt);
							$new_data = array(
								'from_site' => 0,
								'status' => ACCESS_GRANTED_STATUS,
								'password' => $passwd,
								'salt' => $salt,
								'street_name' => $new_reg['StreetName'],
								'date_created' => $new_reg['AudienceDropDate'],
								'date_of_capture' => $new_reg['AudienceDropDate'],
								'individual_id' => $new_reg['IndividualID'],
								'zip_code' => $new_reg['PostalCode'],
								'first_name' => $new_reg['FirstName'],
								'middle_initial' => $new_reg['SecondName'],
								'third_name' => $new_reg['ThirdName'],
								'nick_name' => $new_reg['Nickname'],
								'email_address' => $new_reg['EmailAddress'],
								'date_of_birth' => $new_reg['DateOfBirth'],
								'gender' => $new_reg['Gender'] == 'MALE' ? 'M' : 'F',
								'mobile_phone' => $new_reg['MobilePhoneNumber'],
								'government_id_number' => $new_reg['GovernmentIDNumber'],
								'government_id_type' => in_array($new_reg['GovernmentIDType'], $govIds) ? $new_reg['GovernmentIDType'] : $govIds[0],

								'current_brand_flavor' => $new_reg['CurrentBrandFlavor'] && in_array($new_reg['CurrentBrandFlavor'][0], array('M', 'N', 'Z')) ? $new_reg['CurrentBrandFlavor'][0] : null,
								'current_brand_tar_level' => $new_reg['CurrentBrandTarLevel'] && in_array($new_reg['CurrentBrandTarLevel'][0], array('F', 'L', 'Z')) ? $new_reg['CurrentBrandTarLevel'][0] : null,
								'current_brand_affinity' => $new_reg['CurrentBrandAffinity'] == 'NO ANSWER' || !$new_reg['CurrentBrandAffinity'] ? 0 : $new_reg['CurrentBrandAffinity'],

								'first_brand_flavor' => $new_reg['FirstBrandFlavor'] && in_array($new_reg['FirstBrandFlavor'][0], array('M', 'N', 'Z')) ? $new_reg['FirstBrandFlavor'][0] : null,
								'first_brand_tar_level' => $new_reg['FirstBrandTarLevel'] && in_array($new_reg['FirstBrandTarLevel'][0], array('F', 'L', 'Z')) ? $new_reg['FirstBrandTarLevel'][0] : null,
								'first_alternate_brand_affinity' => $new_reg['FirstAlternateBrandAffinity'] == 'NO ANSWER' || !$new_reg['FirstAlternateBrandAffinity'] ? 0 : $new_reg['FirstAlternateBrandAffinity']
							);
	
							if ($new_reg['CurrentBrand']) {
								foreach ($brands as $brand) {
									if (strpos($brand['brand_name'], $new_reg['CurrentBrand']) === 0) {
										$new_data['current_brand_code'] = $brand['code'];
										$new_data['current_brand'] = $brand['brand_id'];
										break;
									}
								}
							} else {
								$new_data['current_brand_code'] = '';
								$new_data['current_brand'] = '';
							}
							
							if ($new_reg['FirstAlternateBrand']) {
								foreach ($brands as $brand) {
									if (strpos($brand['brand_name'], $new_reg['FirstAlternateBrand']) === 0) {
										$new_data['first_alternate_brand_code'] = $brand['code'];
										$new_data['first_alternate_brand'] = $brand['brand_id'];
										break;
									}
								}
							} else {
								$new_data['first_alternate_brand_code'] = '';
								$new_data['first_alternate_brand'] = '';
							}
							
							$dbConn->insert('tbl_registrants', $new_data);
							if ($dbConn->affected_rows()) {
								$countNew++;
								if ($countNew > $batchLimit) {
									$countNew = 1;
									$batch++;
								}
								if (!isset($forEdmSending[$batch])) {
									$forEdmSending[$batch] = array();
								}
								$forEdmSending[$batch][] = $dbConn->insert_id();
							}
						}

						$multiCurl = curl_multi_init();
						$curls = array();

						foreach ($forEdmSending as $k => $v) {
							$curls[$k] = curl_init(BASE_URL.'admin/cron/mym/edm');
							curl_setopt($curls[$k], CURLOPT_POST, true);
							curl_setopt($curls[$k], CURLOPT_POSTFIELDS, http_build_query(array('ids' => $v)));
							curl_multi_add_handle($multiCurl, $curls[$k]);
						}
						
						do {
							curl_multi_exec($multiCurl, $running);
						} while($running > 0);

						$data['success_inserted'] = true;
						$data['date_inserted'] = date('Y-m-d H:i:s');
					}

				}
			}
		}

		$data['main_content'] = $this->load->view('arclight/outbound_match', $data, true);
		$this->load->view('main-template', $data);
	}
	
	private function nav_items()
	{
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}

	public function push_in_process()
	{
		$this->load->model('Arclight_Model');
		if ($filename = $this->Arclight_Model->push_in_process()) {
			$this->output->set_output($filename);
		}
	}

	public function push_modified()
	{
		$this->load->model('Arclight_Model');
		if ($filename = $this->Arclight_Model->push_modified()) {
			$this->output->set_output($filename);
		}
	}

	public function push_in_process_new_format()
	{
		$this->load->model('Arclight_Model');
		if ($filename = $this->Arclight_Model->push_in_process_new_format()) {
			$this->output->set_output($filename);
		}
	}	
}

/* End of file arclight.php */
/* Location: ./application/controllers/arclight.php */