<?php

class Lamps_Reservation extends CI_Controller
{
	public function index()
	{
		$data = array();

		$this->db->start_cache();

		$search = array();
		$search['lamp_name'] = (string) $this->input->get('lamp_name');
		$search['reserve_status'] = (string) $this->input->get('reserve_status');
		$search['from_booking_date'] = (string) $this->input->get('from_booking_date');
		$search['to_booking_date'] = (string) $this->input->get('to_booking_date');
		$search['to_date_created'] = (string) $this->input->get('to_date_created');
		$search['from_date_created'] = (string) $this->input->get('from_date_created');

		$page = $this->uri->segment(2, 1);
		$this->db->select()
			->from('tbl_lamp_reservations lr')
			->join('tbl_registrants r', 'r.registrant_id = lr.registrant_id')
			->join('tbl_lamps l', 'l.lamp_id = lr.lamp_id')
			->join('tbl_schedules s', 's.schedule_id = lr.schedule_id');

		if ($search['lamp_name']) {
			$this->db->like('l.lamp_name', $search['lamp_name']);
		}

		if ($search['reserve_status'] != '') {
			$this->db->where('lamp_reserve_status', $search['reserve_status']);
		}

		if ($search['from_date_created']) {
			$this->db->where('lamp_reserve_date_created >=', $search['from_date_created']);
		}

		if ($search['to_date_created']) {
			$this->db->where('lamp_reserve_date_created <=', $search['to_date_created']);
		}

		if ($search['from_booking_date']) {
			$this->db->where('s.date >=', $search['from_booking_date']);
		}

		if ($search['to_booking_date']) {
			$this->db->where('s.date <=', $search['to_booking_date']);
		}

		$data['offset'] = ($page - 1) * PER_PAGE;
		$data['total'] = $this->db->count_all_results();
		$this->db->limit(PER_PAGE, $data['offset']);
		$data['reservations'] = $this->db->get()->result();
		
		$this->db->stop_cache();
		$this->db->flush_cache();

		$data['pagination'] = $this->global_model->pagination($data['total'], $page, SITE_URL.'/lamps_reservation');
		$data['access'] = $this->module_model->check_access('lamps_reservation');

		$data['nav'] = $this->nav_items();
		$data['lamp_ids'] = $this->get_user_lamp_ids();
		$data['main_content'] = $this->load->view('perks/reserve/lamps/reservations', $data, true);
		
		$this->load->view('main-template', $data);
	}

	private function get_user_lamp_ids()
	{
		$user = $this->login_model->extract_user_details();
		$param['table'] = 'tbl_lamp_permissions';
		$param['where'] = array('cms_user_id'	=> $user['cms_user_id']);
		$lamps = $this->global_model->get_rows($param)->result_array();
		$lamp_ids = array();
		if ($lamps) {
			foreach ($lamps as $key => $value) {
				$lamp_ids[] = $value['lamp_id'];
			}
		}
		return $lamp_ids;	
	}

	private function nav_items()
	{
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}

	public function _remap($method)
	{
		if (is_numeric($method)) {
			$this->index();
		} else {
			$this->{$method}();
		}
	}

	public function change_status()
	{
		$id = $this->input->get('id');
		$status = $this->input->get('status');

		if ($id) {
			$reserve = (array) $this->db->select()
				->from('tbl_lamp_reservations lr')
				->join('tbl_schedules s', 's.schedule_id = lr.schedule_id')
				->where('lr.lamp_reserve_id', $id)
				->get()
				->row();

			if ($reserve) {

				$redirectUrl = $this->input->server('HTTP_REFERER');

				if ($reserve['original_slots']) {
					
					$numOfGuests = count(json_decode($reserve['lamp_reserve_guests'], true)) + 1;

					if ((!$reserve['lamp_reserve_status'] && $status == 1) || ($reserve['lamp_reserve_status'] == 2 && $status == 1)) {
						
						if ($reserve['slots'] >= $numOfGuests) {

							$this->db->set('slots', 'slots - '.$numOfGuests, false);
							$this->db->where('schedule_id', $reserve['schedule_id']);
							$this->db->update('tbl_schedules');
							
						} elseif ($reserve['slots'] < $numOfGuests) {
							if (strpos($redirectUrl, '?') === false) {
								redirect($redirectUrl.'?error=Not enought slots');
							} else {
								redirect($redirectUrl.'&error=Not enought slots');
							}
						} else {
							if (strpos($redirectUrl, '?') === false) {
								redirect($redirectUrl.'?error=No more slots');
							} else {
								redirect($redirectUrl.'&error=No more slots');
							}
						}

					} elseif ($reserve['lamp_reserve_status'] == 1 && $status == 2) {

						$this->db->set('slots', 'slots + '.$numOfGuests, false);
						$this->db->where('schedule_id', $reserve['schedule_id']);
						$this->db->update('tbl_schedules');

					}

					if ($this->db->affected_rows()) {
						$this->db->set('lamp_reserve_status', $status);
						$this->db->where('lamp_reserve_id', $id);
						$this->db->update('tbl_lamp_reservations');
					}

				}

				require_once 'application/models/points_model.php';
				$points_model = new Points_Model();
				if ($status == 2) {
					$points_model->deactivate($reserve['registrant_id'], [array('suborigin' => $reserve['lamp_reserve_id'], 'origin' => PERKS_RESERVE)]);
				} elseif ($reserve['lamp_reserve_status'] == 2 && $status == 1) {
					$points_model->activate($reserve['registrant_id'], [array('suborigin' => $reserve['lamp_reserve_id'], 'origin' => PERKS_RESERVE)]);
				}
				redirect($redirectUrl);
			}
		}
	}
}