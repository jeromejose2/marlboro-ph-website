<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Survey_log extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
 	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
 		$this->load->view('main-template', $data);
	}

	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
		$this->load->helper(array('paging','text','file'));
 		$offset = (int)$this->input->get('per_page');
	    $limit = PER_PAGE;
	    $filters = $this->get_filters();
	    	// $this->output->enable_profiler(true);

		$access = $this->module_model->check_access('survey_log');
		$total_rows = $this->explore_model->get_rows(array('table'=>'tbl_survey_log as sl',
															 'where'=>array_merge($filters['where_filters'],array()),
														     'like'=>$filters['like_filters'],
														     'join'=>array(
														     				array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=sl.registrant_id'),
														     				array('table'=>'tbl_survey as s','on'=>'s.id=sl.survey_id')
														     		),
														     'fields'=>"sl.id"
														     )
														    )->num_rows(); 
		$data['header_text'] = 'Survey Log';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total_rows'] = $total_rows;

		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_survey_log as sl',
															 'where'=>array_merge($filters['where_filters'],array()),
														     'like'=>$filters['like_filters'],
														     'limit'=>$limit,
														     'offset'=>$offset,
														     'join'=>array(
														     				array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=sl.registrant_id'),
														     				array('table'=>'tbl_survey as s','on'=>'s.id=sl.survey_id')
														     		),
														     'fields'=>"sl.id,r.registrant_id,r.first_name, r.middle_initial, r.third_name, r.person_id, s.title, sl.source, sl.points, sl.timestamp"
														     )
														);
		$data['query_strings'] = $filters['query_strings'];
		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']); 
		return $this->load->view('survey/survey_logs',$data,TRUE);
	}


	public function get_filters()
	   {
		   
		   $where_filters = array('sl.source'=>'source',
		   							'sl.points'=>'points',
		   							'DATE(sl.timestamp) >='=>'from',
		   							'DATE(sl.timestamp) <='=>'to'
		   						);
		   $like_filters = array(
		   							"CONCAT(r.first_name,' ',r.third_name) "=>'name',
		   							's.title'=>'title',
		   							'r.person_id'=>'person_id'
		   						);
		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
		   
	   }

	   public function export()
	   {

	   		$this->load->library('to_excel_array');
	   		$filters = $this->get_filters();
		   	$rows = $this->explore_model->get_rows(array('table'=>'tbl_survey_log as sl',
															 'where'=>array_merge($filters['where_filters'],array()),
														     'like'=>$filters['like_filters'],
														     'join'=>array(
														     				array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=sl.registrant_id'),
														     				array('table'=>'tbl_survey as s','on'=>'s.id=sl.survey_id')
														     		),
														     'fields'=>"sl.id,r.registrant_id,r.first_name, r.middle_initial, r.third_name, r.person_id, s.title, sl.source, sl.points, sl.timestamp"
														     )
														);

		   	$res[] = array('Person ID',
	   					'Name',
		   				'Title',
					   'Source',
					   'Points', 
					   'Date');

		   	if($rows->num_rows()){

		   		foreach($rows->result() as $v){
		   			$res[] = array(
		   						$v->person_id,
		   						ucwords($v->first_name.' '.$v->third_name),
		   						$v->title,
		   						$v->source,
		   						$v->points,
		   						$v->timestamp
	   						);
		   		}

		   	} 

			$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));

 
	   }

}