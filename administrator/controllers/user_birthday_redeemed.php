<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_birthday_redeemed extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$data['records'] = $this->get_demographics();
		return $this->load->view('visits/user_birthday_redeemed', $data, true);		
	}

	public function export()
	{
		$this->load->library('to_excel_array');
		$records = $this->get_demographics($from, $to);
		$row[] = $from && $to ? array('User Birthday Prizes Redeemed (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')') : array('Login Spread');

		$row[] = array('Prize', 'Count');
		if($records) {
			foreach($records as $k => $v) {
				$row[] = array(
							  $v['prize_name'],
							  $v['count']);
			}
		}
		$this->to_excel_array->to_excel($row, 'user_birthday_prizes_'.date("YmdHis"));
	}
	

	private function get_demographics(&$from = false, &$to = false) {
		$where = array('prize_status' => 1);
		if($this->input->get('fromdate') && $this->input->get('todate')) {
			$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
			$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
			$where['DATE(date_claimed) <='] = $to;
			$where['DATE(date_claimed) >='] = $from;
		}
		$param['fields'] = 'DATE(date_claimed) AS dv, COUNT(*) AS count, prize_name';
		$param['where'] = $where;
		$param['table'] = 'tbl_user_birthday_prizes';
		$param['order_by'] = array('field'	=> 'prize_month', 'order'	=> 'ASC');
		$param['group_by'] = 'prize_id';
		$param['offset'] = 0;
		$param['limit'] = 30;
		$records = $this->global_model->get_rows($param)->result_array();
		return $records;
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */