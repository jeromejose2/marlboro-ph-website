<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Move_fwd_prizes extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		$this->_table = 'tbl_move_forward_choice';
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		$where['move_forward_choice_status'] = 1;
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			if($like['fromredeem'] && $like['toredeem']) {
				$from = $like['fromredeem'] > $like['toredeem'] ?  $like['toredeem'] :  $like['fromredeem'];
				$to = $like['toredeem'] < $like['fromredeem'] ?  $like['fromredeem'] :  $like['toavail'];
				$where['DATE(prize_delivered_date) <='] = $to;
				$where['DATE(prize_delivered_date) >='] = $from;
			}

			if($like['first_name']) {
				$like['CONCAT(first_name, \' \', third_name)'] = $like['first_name'];
			}

			if($like['fromaccom'] && $like['toaccom']) {
				$where['DATE(move_forward_choice_done) <='] = $like['toaccom'];
				$where['DATE(move_forward_choice_done) >='] = $like['fromaccom'];
			}

			unset($like['search']);
			unset($like['fromredeem']);
			unset($like['toredeem']);
			unset($like['fromaccom']);
			unset($like['toaccom']);
			unset($like['prize_name']);
			unset($like['send_type']);
			unset($like['first_name']);

		}
		$param['like'] = $like;
		$param['table'] = $this->_table;
		$param['where'] = $where;
		$param['offset'] = $data['offset'];
		$param['limit'] = PER_PAGE;
		$param['fields'] = '*';
		$param['order_by'] = array('field' => 'move_forward_choice_done', 'order'	=> 'DESC');
		$param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id = ' . $this->_table . '.registrant_id',
							  'tbl_move_forward' => 'tbl_move_forward.move_forward_id = ' . $this->_table . '.move_forward_id',
							  'tbl_prizes' => 'tbl_move_forward.prize_id = tbl_prizes.prize_id');
		$data['categories'] = $this->global_model->get_rows($param)->result_array();
		$records = $this->global_model->get_total_rows($param);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/move_fwd_prizes');

		$access = $this->module_model->check_access('move_fwd_prizes');
		$data['total'] = $records;
		$data['edit'] = $access['edit'];
		return $this->load->view('move_fwd/prizes/index', $data, true);		
	}

	public function edit()
	{
		$id = $this->uri->segment(3);
		$data['main_content'] = $this->edit_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	private function edit_content($id) {
		$error = false;
		$param['table'] = $this->_table;
		$param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id = ' . $this->_table . '.registrant_id',
							  'tbl_move_forward' => 'tbl_move_forward.move_forward_id = ' . $this->_table . '.move_forward_id',
							  'tbl_prizes' => 'tbl_move_forward.prize_id = tbl_prizes.prize_id');
		$param['where'] = array($this->_table . '.move_forward_choice_id'	=> $id);
		$prize = (array)$this->global_model->get_row($param);
			
		if($this->input->post('submit')) {
			$param['prize_delivered'] = $this->input->post('prize_status');
			if($param['prize_delivered'] == 1) {
				$param['prize_delivered_date'] = date('Y-m-d H:i:s');
			}
			$where = array('move_forward_choice_id'	=> $prize['move_forward_choice_id']);
			$this->global_model->update($this->_table, $param, $where);
			redirect('move_fwd_prizes');
		}

		$data['record'] = $prize;
		$data['error'] = $error;
		return $this->load->view('move_fwd/prizes/add', $data, true);		
	}
	
	// public function _remap($method) {
	// 	if($method == 'edit')
	// 		$this->edit($this->uri->segment(3));
	// 	elseif($method == 'delete')
	// 		$this->delete($this->uri->segment(3));
	// 	elseif($method == 'add')
	// 		$this->add();
	// 	else
	// 		$this->index();
	// }

	public function set_winner() {
		$id = $this->uri->segment(3);
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/move_fwd_prizes') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->update_winner_status($id, 1, 'set a move forward accomplished activities as winner');
		}
		redirect('move_fwd_prizes');
	}

	public function unset_winner() {
		$id = $this->uri->segment(3);
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/move_fwd_prizes') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->update_winner_status($id, 0, 'unset a move forward accomplished activities as winner');
		}
		redirect('move_fwd_prizes');
	}

	private function update_winner_status($id, $new_status, $action_text = '', $message = '') {
		$where['move_forward_choice_id'] = $id;
		$param['where'] = $where;
		$param['table'] = $this->_table;
		$param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id  = ' . $this->_table . '.registrant_id');

		$accomplished = (array)$this->global_model->get_row($param);
		if(!$accomplished)
			redirect('move_fwd_prizes');

		$set['is_winner'] = $new_status;
		if($new_status == 1) {
			//SPICE
			$mfwd = $this->global_model->get_row(array('table' => 'tbl_move_forward_choice', 'where' => 'move_forward_choice_id ='.$id));
			$mfwd_id = $mfwd->move_forward_id;
			$mfwd_fwd = $this->global_model->get_row(array('table' => 'tbl_move_forward', 'where' => 'move_forward_id ='.$mfwd_id));
			$mfwd_prize_id = $mfwd_fwd->prize_id;
			$mfwd_attribute_id = $mfwd_fwd->attribute_id;
			$mfwd_fwd_title = $mfwd_fwd->move_forward_title;
			$mfwd_prize = $this->global_model->get_row(array('table' => 'tbl_prizes', 'where' => 'prize_id ='.$mfwd_prize_id));
			$mfwd_prize_attribute_id = $mfwd_prize->attribute_id;
			$mfwd_prize_title = $mfwd_prize->prize_name;
			$mfwd_choice = $mfwd->move_forward_choice;
			if($mfwd_choice == 1){
				$mfwd_choice = 'play';
			}else{
				$mfwd_choice = 'pledge'; 
			}

			$sec_settings = $this->global_model->get_row(array('table' => 'tbl_section_settings', 'where' => 'name = "MOVE_FWD"'));
			$cell_id = $sec_settings->setting_section_id;

			$act_settings = $this->global_model->get_row(array('table' => 'tbl_activity_settings', 'where' => 'name = "MOVE_FWD"'));
			$act_id = $act_settings->setting_activity_id;

			$params['table'] = 'tbl_registrants';
			$params['where'] = array('registrant_id' => $accomplished['registrant_id']);
			$reg = (array) $this->global_model->get_row($params);
			$data['reg'] = $reg;

			$param = array('CellId' => $cell_id, 'ActionList' =>array(array('ActivityId' => $act_id, 'ActionValue' => $mfwd_fwd_title.'|won '.$mfwd_choice.'|'.$mfwd_prize_title)), 'PersonId' => $data['reg']['person_id']);
			$response = $this->spice_library->trackActionAdmin($param);
			$response_json = $this->spice_library->parseJSON($response);

			if($response_json->MessageResponseHeader->TransactionStatus != 0){
					$data_error = array('origin_id'=>MOVE_FWD,
							'method'=>'trackAction',
							'transaction'=>$this->router->method,
							'input'=> $this->session->userdata('spice_input'),
							'response'=>$response,
							'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
						);
					$this->spice_library->saveError($data_error);

					$set['is_winner'] = 0;
			}else{
				$set['is_winner'] = 1;
			}

			//			
				
			//PENDING $this->points_model->earn(MOVE_FWD_ACCOMPLISHED_WINNER, array('registrant'	=> $accomplished['registrant_id'], 'suborigin'	=> $id));
		}
		// } else {
		// 	$where = array();
		// 	$set['total_points'] = $accomplished['total_points'] - MOVE_FWD_WINNER_BASE_POINT;
		// 	$where['registrant_id'] = $accomplished['registrant_id'];
		// 	$this->global_model->update('tbl_registrants', $set, $where);

		// 	$where = array();
		// 	$where['origin_id'] = MOVE_FWD_WINNER;
		// 	$where['suborigin_id'] = $id;
		// 	$this->global_model->delete('tbl_points', $where);

		// }
		
		$this->global_model->update_record($this->_table, $where, $set);

		#save audit trail
		$new_content['mfg_winner_status'] = $new_status == 1 ? 'Yes' : 'No';
		$old_content['mfg_winner_status'] = $new_status == 1 ? 'No' : 'Yes';
		$post['url'] = @$_SERVER['HTTP_REFERER'];
		$post['description'] = $action_text;
		$post['table'] = $this->_table;
		$post['record_id'] = $id;
		$post['message'] = $message;
		$post['field_changes'] = serialize(array('old'	=> $old_content,
												  'new'	=> $new_content));
		$this->module_model->save_audit_trail($post);	

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */