<?php 

class Lamp_calendar extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		$this->_table = 'tbl_schedules';
 	}
	
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		if( ! $this->check_permission($this->uri->segment(2)))
		{
			redirect('lamps');
		}
		$data = array();		
		return $this->load->view('perks/reserve/lamps/calendar', $data, true);		
	}

	private function check_permission($lamp_id) {
		$user = $this->login_model->extract_user_details();
		$param['table'] = 'tbl_lamp_permissions';
		$param['where'] = array('lamp_id'	=> $lamp_id, 'cms_user_id'	=> $user['cms_user_id']);
		$permission = (array) $this->global_model->get_row($param);
		return $permission;
	}

	public function export() {
		$like = array();
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			unset($like['search']);
		}

		//$param['having'] = $like;
		$param['table'] = $this->_table;
		//$param['join'] = array('tbl_challenges', 'tbl_challenges.move_forward_id = tbl_move_forward.move_forward_id'); 
		$param['join'] = array('tbl_cms_users' => 'tbl_lamp_permissions.cms_user_id = tbl_cms_users.cms_user_id',
								'tbl_lamps' => 'tbl_lamps.lamp_id = tbl_lamp_permissions.lamp_id'); 
		$param['order_by'] = array('field'=>'tbl_lamp_permissions.created_date','order'=>'DESC');
		//$param['having'] = array('challenges LIKE ' => '%' . $challenge . '%', 'is_deleted' => 0);
		$param['like'] = $like;
		$records = $this->global_model->get_rows($param)->result_array();
		$row[] = array(	'Name',
						'LAMP');
		if($records) {
				foreach($records as $k => $v) {
					$row[] = array($v['name'], $v['lamp_name']);
				}
			}		
		$this->load->library('to_excel_array');
		$this->to_excel_array->to_excel($row, 'lamps_'.date("YmdHis"));
	}

	public function date() {
		$date = strtotime('2014-05-22');
		echo $date . '<br>';
		echo date('Y-m-d', strtotime('+1 day', $date)) . '<br>';
		echo date('Y-m-d', strtotime('+1 Monday', $date)) . '<br>';
	}

	public function save_dates() {
		$start_date = $this->input->post('start');
		$end_date = $this->input->post('end');
		$start_epoch = strtotime($start_date);
		$end_epoch = strtotime($end_date);
		$days = array('', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

		$this->save_date($start_date, $this->input->post('note'), $this->input->post('slots'), $this->input->post('reservation_cost'));
		if($this->input->post('repeat')) {
			$end_lookup = 0;
			$i = 1;
			while($end_lookup <= $end_epoch) {
				for($j = 1; $j <= 7; $j++) {
					if(in_array($j, $this->input->post('day')) && $end_lookup <= $end_epoch) {
						$end_lookup = strtotime('+' . $i . ' ' . $days[$j], $start_epoch);	
						$this->save_date(date('Y-m-d', $end_lookup), $this->input->post('note'), $this->input->post('slots'), $this->input->post('reservation_cost'));	
					}
					
				}
				$i++;
			}
		}

	}

	public function remove_dates() {
		$with_schedules = array();
		$start_date = $this->input->post('start');
		$end_date = $this->input->post('end');
		$start_epoch = strtotime($start_date);
		$end_epoch = strtotime($end_date);
		$days = array('', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

		$reservation = $this->check_existing_reservation($start_date);
		if(!$reservation) {
			$this->global_model->delete($this->_table, array('date'	=> $start_date));	
		} else {
			$with_schedules[] = $start_date;
		}

			
		if($this->input->post('repeat')) {
			$end_lookup = 0;
			$i = 1;
			while($end_lookup < $end_epoch) {
				for($j = 1; $j <= 7; $j++) {
					if(in_array($j, $this->input->post('day'))) {
						$end_lookup = strtotime('+' . $i . ' ' . $days[$j], $start_epoch);
						$reservation = $this->check_existing_reservation(date('Y-m-d', $end_lookup));
						if(!$reservation) {
							$this->global_model->delete($this->_table, array('date'	=> date('Y-m-d', $end_lookup)));	
						} else {
							$with_schedules[] = date('Y-m-d', $end_lookup);
						}
						//$this->global_model->delete($this->_table, array('date'	=> date('Y-m-d', $end_lookup)));	
						//$this->save_date(date('Y-m-d', $end_lookup), $this->input->post('note'));	
					}
					
				}
				$i++;
			}
		}

		$this->output->set_content_type('application/json')
			 ->set_output(json_encode($with_schedules));

	}

	private function check_existing_reservation($date) {
		$reservation = $this->db->select('*')
								->from('tbl_lamp_reservations')
								->join('tbl_schedules', 'tbl_schedules.schedule_id = tbl_lamp_reservations.schedule_id')
								->where('date', $date)
								->get()
								->result_array();
		return $reservation;
	}
	
	private function save_date($date, $note, $slots, $reservation_cost) {
		$user = $this->login_model->extract_user_details();
		$post['created_by'] = $user['cms_user_id'];
		$post['date'] = $date;
		$post['lamp_id'] = $this->uri->segment(3);
		$post['note'] = $note;
		$post['slots'] = $slots;
		$post['original_slots'] = $slots;
		$post['reservation_cost'] = $reservation_cost;
		$this->global_model->insert($this->_table, $post);
	}

	public function add() {
		$data['main_content'] = $this->add_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function add_content() {
		$error = '';
		if($this->input->post('submit')) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules($this->get_rules());
			$valid = $this->form_validation->run();	
			if($valid) {
				$post = $this->input->post();
				if(!$error) {
					$id = $this->global_model->insert($this->_table, $post);
					$post = array();
					$post['url'] = SITE_URL . '/move_fwd/add';
					$post['description'] = 'added a new move fwd item';
					$post['table'] = 'tbl_move_forward';
					$post['record_id'] = $id;
					$post['type'] = 'add';
					$this->module_model->save_audit_trail($post);
					redirect('lamp_permissions');	
				}
			} else {
				$error = validation_errors();
			}
		}
		$data['error'] = $error;
		$data['record'] = $_POST;
		$data['lamps'] = $this->get_lamps();
		return $this->load->view('perks/reserve/lamps/permissions/add', $data, true);			
	}

	public function get_avail_users() {
		$lamp_id = $this->input->post('id');
		$query = "SELECT * FROM tbl_cms_users 
				  WHERE cms_user_id NOT IN (SELECT cms_user_id FROM tbl_lamp_permissions WHERE lamp_id = $lamp_id)
				  ORDER BY name";
		$users = $this->global_model->custom_query($query)->result_array();
		$data['users'] = $users;
		$this->load->view('perks/reserve/lamps/permissions/user', $data);	

	}

	public function get_lamps() {
		$param['table'] = 'tbl_lamps';
		$param['order_by'] = array('field'	=> 'lamp_name', 'order'	=> 'ASC');
		$lamps = $this->global_model->get_rows($param)->result_array();
		return $lamps;
	}

	public function get_events() {
		$start = $this->input->get('start');
		$end = $this->input->get('end');
		$lamp_id = $this->uri->segment(3);
		$param['table'] = $this->_table;
		$param['where'] = array('lamp_id' => $lamp_id, 'DATE(date) >=' => date('Y-m-d', $start), 'DATE(date) <=' => date('Y-m-d', $end));
		$events = $this->global_model->get_rows($param)->result_array();
		$event_arr = array();
		if($events) {
			foreach ($events as $key => $value) {
				$event_arr[] = array('start'	=> $value['date'], 'title' => $value['note']);
			}
		}
		echo json_encode($event_arr);
	}

	public function get_event() {
		$date = $this->input->post('date');
		$lamp_id = $this->input->post('id');
		$param['table'] = $this->_table;
		$param['where'] = array('lamp_id' => $lamp_id, 'DATE(date)'	=> $date);
		$events = $this->global_model->get_rows($param)->result_array();
		$event_arr = array();
		if($events) {
			foreach ($events as $key => $value) {
				$event_arr[] = array('start'	=> $value['date'], 'title' => $value['note'], 'original_slots'	=> $value['original_slots'], 'reservation_cost'	=> $value['reservation_cost']);
			}
		}
		$this->output->set_content_type('application/json')
			 ->set_output(json_encode($event_arr));
	}

	public function delete() {
		$table = $this->_table;
		$id = $this->uri->segment(3);
		$field = 'lamp_permission_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/lamp_permissions') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$where[$field] = $id;
			$this->global_model->delete($table, $where);
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	private function get_rules() {
		$config = array(
		   array(
				 'field'   => 'cms_user_id',
				 'label'   => 'user',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'lamp_id',
				 'label'   => 'LAMP',
				 'rules'   => 'required'
			  )
		);
		return $config;
	}

	
	
	public function _remap($method) {
		if(is_numeric($method))
			$this->index();
		else 
			$this->{$method}();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */