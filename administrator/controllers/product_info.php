<?php

class Product_info extends CI_Controller
{
	public function index()
	{
		$data = array();

		$this->db->start_cache();

		$search = array();
		$search['title'] = (string) $this->input->get('title');
		$search['status'] = (string) $this->input->get('status');
		$search['from_date'] = (string) $this->input->get('from_date');
		$search['to_date'] = (string) $this->input->get('to_date');

		$page = $this->uri->segment(2, 1);
		$this->db->select()
			->from('tbl_product_info');

		if ($search['title']) {
			$this->db->like('product_info_title', $search['title']);
		}

		if ($search['status'] != '') {
			$this->db->where('product_info_status', $search['status']);
		}

		if ($search['from_date']) {
			$this->db->where('product_info_date_created >=', $search['from_date']);
		}

		if ($search['to_date']) {
			$this->db->where('product_info_date_created <=', $search['to_date']);
		}

		$data['offset'] = ($page - 1) * PER_PAGE;
		$data['total'] = $this->db->count_all_results();
		$this->db->limit(PER_PAGE, $data['offset']);
		$data['records'] = $this->db->get()->result();

		$this->db->stop_cache();
		$this->db->flush_cache();

		$data['pagination'] = $this->global_model->pagination($data['total'], $page, SITE_URL.'/product_info');
		$data['access'] = $this->module_model->check_access('product_info');

		$data['nav'] = $this->nav_items();
		$data['main_content'] = $this->load->view('product_info/index', $data, true);
		$this->load->view('main-template', $data);
	}

	public function upload()
	{
		$config = array();
		$path = './uploads/product_info/';
		if (!is_dir($path)) {
			mkdir($path);
		}
		$path .= 'media/';
		if (!is_dir($path)) {
			mkdir($path);
		}

		$config['upload_path'] = $path;
		$config['allowed_types'] = PRODUCT_INFO_FILE_UPLOAD_TYPES;
		if (strpos($_FILES['myFile']['type'], 'image') !== false) {
			$config['max_size'] = FILE_UPLOAD_LIMIT;
		} else {
			$config['max_size']	= VIDEO_UPLOAD_LIMIT;
		}
		$config['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['myFile']['name'], PATHINFO_EXTENSION);

		$this->load->helper('resize');
		$this->load->library('upload', $config);

		if ($this->upload->do_upload('myFile')) {
			$data = $this->upload->data();
			if ($data['is_image']) {
				resize(array(
					'width' => 150,
					'height'=> 150,
					'source_image' => $path.$config['file_name'],
					'new_image_path' => $path,
					'file_name'=> $config['file_name']
				));
				resize(array(
					'width' => 576,
					'height'=> 296,
					'source_image' => $path.$config['file_name'],
					'new_image_path' => $path,
					'file_name'=> $config['file_name']
				));
			} else {
				$realdir = realpath($path);
				if ($realdir) {
					$realdir = str_replace("\\", '/', rtrim($realdir, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR);
					$thumbfilename = 'thumb_'.$config['file_name'].'.jpg';
					exec('ffmpeg -i '.$realdir.$config['file_name'].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realdir.$thumbfilename);
					resize(array(
						'width' => 150,
						'height'=> 150,
						'source_image' => $path.$thumbfilename,
						'new_image_path' => $path,
						'file_name'=> $thumbfilename
					));
				}
			}
			$this->output->set_output($config['file_name'].' - '.($data['is_image'] ? 1 : 2));
		}


	}

	private function nav_items()
	{
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}

	public function add()
	{
		$data = array( 'error' => '');
		$post = $this->input->post();
		if ($post) {

			$this->load->library('form_validation');
			$this->form_validation->set_rules('title', 'required|alpha_numeric_space');
			$this->form_validation->set_rules('status', 'required|numeric');
			$this->form_validation->set_rules('description', 'required');

			if ($this->form_validation->run()) {

				$config = array();
				$path = './uploads/product_info/';
				if (!is_dir($path)) {
					mkdir($path);
				}
				$config['upload_path'] = $path;
				$config['allowed_types'] = FILE_UPLOAD_ALLOWED_TYPES;
				$config['max_size'] = FILE_UPLOAD_LIMIT;
				$config['max_width'] = 193;
				$config['max_height'] = 225;
				$config['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['product_info_upload']['name'], PATHINFO_EXTENSION);

				$this->load->helper('resize');
				$this->load->library('upload', $config);
				if ($this->upload->do_upload('product_info_upload')) {

					$title_type = 1;
					if (!isset($post['title'])) {
						$titleConf = array();
						$titleConf['upload_path'] = $path;
						$titleConf['allowed_types'] = FILE_UPLOAD_ALLOWED_TYPES;
						$titleConf['max_size'] = FILE_UPLOAD_LIMIT;
						$titleConf['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['product_info_title']['name'], PATHINFO_EXTENSION);

						$this->upload->initialize($titleConf);
						if ($this->upload->do_upload('product_info_title')) {
							resize(array(
								'width' => 200,
								'height'=> 30,
								'source_image' => $path.$titleConf['file_name'],
								'new_image_path' => $path,
								'file_name'=> $titleConf['file_name']
							));
							$post['title'] = $titleConf['file_name'];
						} else {
							$data['error'] = $this->upload->display_errors();
						}
						$title_type = 2;
					}

					if (!$data['error']) {

						$contentImageFilename = null;
						if (!empty($_FILES['product_info_content_image_upload']['name'])) {

							$contentImageConf['upload_path'] = $path;
							$contentImageConf['allowed_types'] = FILE_UPLOAD_ALLOWED_TYPES;
							$contentImageConf['max_size'] = FILE_UPLOAD_LIMIT;
							$contentImageConf['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['product_info_content_image_upload']['name'], PATHINFO_EXTENSION);

							$this->upload->initialize($contentImageConf);
							if (!$this->upload->do_upload('product_info_content_image_upload')) {
								$data['error'] = $this->upload->display_errors();
							} else {
								$contentImageFilename = $contentImageConf['file_name'];
								resize(array(
									'width' => 150,
									'height'=> 150,
									'source_image' => $path.$contentImageConf['file_name'],
									'new_image_path' => $path,
									'file_name'=> $contentImageConf['file_name']
								));
							}

						}

						if (!$data['error']) {

							resize(array(
								'width' => 150,
								'height'=> 150,
								'source_image' => $path.$config['file_name'],
								'new_image_path' => $path,
								'file_name'=> $config['file_name']
							));

							resize(array(
								'width' => 193,
								'height'=> 225,
								'source_image' => $path.$config['file_name'],
								'new_image_path' => $path,
								'file_name'=> $config['file_name']
							));

							$today = date('Y-m-d H:i:s');
							$cms_user_id = $this->login_model->extract_user_details()['cms_user_id'];
							$this->db->insert('tbl_product_info', array(
								'product_info_title' => $post['title'],
								'product_info_title_type' => $title_type,
								'product_info_status' => $post['status'],
								'product_info_image' => $config['file_name'],
								'product_info_content_image' => $contentImageFilename,
								'product_info_description' => $this->input->post('description', false),
								'product_info_date_created' => $today,
								'cms_user_id' => $cms_user_id
							));

							if ($this->db->affected_rows()) {

								$info_id = $this->db->insert_id();
								foreach ($post['product_media_upload'] as $k => $m) {
									if (!$m)
										continue;
									$m = explode(' - ', $m);
									if (count($m) != 2) {
										continue;
									}
									$file = $m[0];
									$type = $m[1];
									$title = isset($post['product_media_title'][$k]) ? $post['product_media_title'][$k] : null;
									$this->db->insert('tbl_product_media', array(
										'product_info_id' => $info_id,
										'product_media_title' => $title,
										'product_media_type' => $type,
										'product_media_content' => $file,
										'product_media_date_created' => $today,
										'product_media_cms_user_id' => $cms_user_id
									));
								}

								redirect('product_info');

							}

						}

					}
					
				} else {
					$data['error'] = $this->upload->display_errors();
				}

			} else {
				$data['error'] = validation_errors();	
			}

		}

		$data['nav'] = $this->nav_items();
		$data['main_content'] = $this->load->view('product_info/add', $data, true);
		$this->load->view('main-template', $data);
	}

	public function change_status()
	{
		$id = $this->input->get('id');
		$status = (int) $this->input->get('status');
		if ($id) {
			$this->db->where('product_info_id', $id)
				->update('tbl_product_info', array(
					'product_info_status' => $status
				));
		}
		redirect($this->input->server('HTTP_REFERER'));
	}

	public function delete()
	{
		$id = $this->input->get('id');
		if ($id) {
			$this->db->from('tbl_product_info')
				->where('product_info_id', $id)
				->delete();
			if ($this->db->affected_rows()) {
				$this->db->from('tbl_product_media')
					->where('product_info_id', $id)
					->delete();
			}
		}
		redirect($this->input->server('HTTP_REFERER'));
	}

	public function edit()
	{
		$data = array('error' => '');

		$id = $this->input->get('id');
		if (!$id) {
			redirect('product_info');
		}

		$data['product'] = (array) $this->db->select()
			->from('tbl_product_info')
			->where('product_info_id', $id)
			->limit(1)
			->get()
			->row();

		if (!$data['product']) {
			show_404();
		}

		$data['medias'] = $this->db->select()
			->from('tbl_product_media')
			->where('product_info_id', $id)
			->get()
			->result_array();

		$post = $this->input->post();
		if ($post) {

			$this->load->helper('resize');
			$this->load->library('upload');
			$this->load->library('form_validation');
			$this->form_validation->set_rules('title', 'required|alpha_numeric_space');
			$this->form_validation->set_rules('status', 'required|numeric');
			$this->form_validation->set_rules('description', 'required');

			if ($this->form_validation->run()) {

				$updateData = array(
					'product_info_status' => $post['status'],
					'product_info_description' => isset($_POST['description']) ? $_POST['description'] : ''
				);
				$path = './uploads/product_info/';
				if (!is_dir($path)) {
					mkdir($path);
				}

				$hasUploadImage = false;
				if (!empty($_FILES['product_info_upload']['name'])) {

					$config = array();
					$config['upload_path'] = $path;
					$config['allowed_types'] = FILE_UPLOAD_ALLOWED_TYPES;
					$config['max_size'] = FILE_UPLOAD_LIMIT;
					$config['max_width'] = 193;
					$config['max_height'] = 225;
					$config['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['product_info_upload']['name'], PATHINFO_EXTENSION);
					$this->upload->initialize($config);
					if ($this->upload->do_upload('product_info_upload')) {
						$updateData['product_info_image'] = $config['file_name'];
						$hasUploadImage = true;
					} else {
						$data['error'] = $this->upload->display_errors();
					}

				}
				
				if (!$data['error']) {

					if (!empty($_FILES['product_info_content_image_upload']['name'])) {

						$contentImageConf['upload_path'] = $path;
						$contentImageConf['allowed_types'] = FILE_UPLOAD_ALLOWED_TYPES;
						$contentImageConf['max_size'] = FILE_UPLOAD_LIMIT;
						$contentImageConf['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['product_info_content_image_upload']['name'], PATHINFO_EXTENSION);

						$this->upload->initialize($contentImageConf);
						if (!$this->upload->do_upload('product_info_content_image_upload')) {
							$data['error'] = $this->upload->display_errors();
						} else {
							$updateData['product_info_content_image'] = $contentImageConf['file_name'];
							resize(array(
								'width' => 150,
								'height'=> 150,
								'source_image' => $path.$contentImageConf['file_name'],
								'new_image_path' => $path,
								'file_name'=> $contentImageConf['file_name']
							));
						}

					}

					if (!$data['error']) {

						$updateData['product_info_title_type'] = 1;
						if (!isset($post['title'])) {

							if (!empty($_FILES['product_info_title']['name'])) {
								$titleConf = array();
								$titleConf['upload_path'] = $path;
								$titleConf['allowed_types'] = FILE_UPLOAD_ALLOWED_TYPES;
								$titleConf['max_size'] = FILE_UPLOAD_LIMIT;
								$titleConf['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['product_info_title']['name'], PATHINFO_EXTENSION);

								$this->upload->initialize($titleConf);
								if ($this->upload->do_upload('product_info_title')) {
									resize(array(
										'width' => 200,
										'height'=> 30,
										'source_image' => $path.$titleConf['file_name'],
										'new_image_path' => $path,
										'file_name'=> $titleConf['file_name']
									));
									$updateData['product_info_title'] = $titleConf['file_name'];
								} else {
									$data['error'] = $this->upload->display_errors();
								}
							}
							
							$updateData['product_info_title_type'] = 2;

						} else {
							$updateData['product_info_title'] = $post['title'];
						}

						if (!$data['error']) {

							if ($hasUploadImage) {

								resize(array(
									'width' => 150,
									'height'=> 150,
									'source_image' => $path.$updateData['product_info_image'],
									'new_image_path' => $path,
									'file_name'=> $updateData['product_info_image']
								));

								resize(array(
									'width' => 193,
									'height'=> 225,
									'source_image' => $path.$updateData['product_info_image'],
									'new_image_path' => $path,
									'file_name'=> $updateData['product_info_image']
								));

							}

							$mediaIds = array();
							foreach ($data['medias'] as $media) {
								$mediaIds[$media['product_media_id']] = $media['product_media_id'];
							}

							if (!empty($post['product_media_title_edit'])) {
								foreach ($post['product_media_title_edit'] as $key => $dataEdit) {

									if (!isset($post['product_media_ids_edit'][$key])) {
										continue;
									}

									if (isset($mediaIds[$post['product_media_ids_edit'][$key]])) {
										unset($mediaIds[$post['product_media_ids_edit'][$key]]);
									}

									$this->db->where('product_media_id', $post['product_media_ids_edit'][$key]);
									$this->db->update('tbl_product_media', array(
										'product_media_title' => $dataEdit
									));

								}
							}
							
							if ($mediaIds) {
								$this->db->where_in('product_media_id', $mediaIds);
								$this->db->delete('tbl_product_media');
							}

							if (!empty($post['product_media_upload'])) {

								$today = date('Y-m-d H:i:s');
								$cms_user_id = $this->login_model->extract_user_details()['cms_user_id'];
								foreach ($post['product_media_upload'] as $k => $m) {
									if (!$m)
										continue;
									$m = explode(' - ', $m);
									if (count($m) != 2) {
										continue;
									}
									$file = $m[0];
									$type = $m[1];
									$title = isset($post['product_media_title'][$k]) ? $post['product_media_title'][$k] : null;
									$this->db->insert('tbl_product_media', array(
										'product_info_id' => $id,
										'product_media_title' => $title,
										'product_media_type' => $type,
										'product_media_content' => $file,
										'product_media_date_created' => $today,
										'product_media_cms_user_id' => $cms_user_id
									));
								}

							}

							$this->db->where('product_info_id', $id);
							$this->db->update('tbl_product_info', $updateData);
							redirect('product_info');

						}

					}
					
				}

			} else {
				$data['error'] = validation_errors();	
			}

		}

		$data['nav'] = $this->nav_items();
		$data['main_content'] = $this->load->view('product_info/edit', $data, true);
		$this->load->view('main-template', $data);
	}

	public function export()
	{
		$this->load->library('to_excel_array');

		$search = array();
		$search['title'] = (string) $this->input->get('title');
		$search['status'] = (string) $this->input->get('status');
		$search['from_date'] = (string) $this->input->get('from_date');
		$search['to_date'] = (string) $this->input->get('to_date');

		$rows = array(array(
			'product_info_image' => 'Image',
			'product_info_title' => 'Title',
			'product_info_description' => 'Description',
			'product_info_status' => 'Status',
			'product_info_date_created' => 'Date Created'
		));
		$this->db->select(implode(',', array_keys($rows[0])))
			->from('tbl_product_info');

		if ($search['title']) {
			$this->db->like('product_info_title', $search['title']);
		}

		if ($search['status'] != '') {
			$this->db->where('product_info_status', $search['status']);
		}

		if ($search['from_date']) {
			$this->db->where('product_info_date_created >=', $search['from_date']);
		}

		if ($search['to_date']) {
			$this->db->where('product_info_date_created <=', $search['to_date']);
		}

		foreach ($this->db->get()->result_array() as $data) {
			if ($data['product_info_status'] == 1) {
				$data['product_info_status'] = 'Published';
			} else {
				$data['product_info_status'] = 'Not Published';
			}
			$data['product_info_image'] = BASE_URL.'uploads/product_info/'.$data['product_info_image'];
			$rows[] = $data;
		}
		$this->to_excel_array->to_excel($rows, 'Product_info_'.date('YmdHis'));
	}
}