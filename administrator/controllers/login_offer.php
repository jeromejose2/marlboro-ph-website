<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_offer extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		$this->load->model('module_model');
		$this->_table = 'tbl_loggin_offers';
		$this->load->helper('file');
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	private function add_content()
	{	
		if($this->input->post('submit')){
			$title = $_POST['title'];
			$content = $_POST['content'];
			$points = $_POST['points'];
			$start_date = $_POST['start_date'];
			$end_date = $_POST['end_date'];
			$days_inactive = $_POST['days_inactive'];

			$errors = [];

			if($title==""){
				$errors[] = "Indicate offer's title";
			}else if(strlen($title)>255){
				$errors[] = "Title character length exceeded. It should be 255 characters only";
			}
			if($content==""){
				$errors[] = "Indicate offer's content";
			}else if(strlen($title)>1000){
				$errors[] = "Content character length exceeded. It should be 1000 characters only";
			}
			if($points==""){
				$errors[] = "Indicate points to be rewarded";
			}else if(!is_numeric($points)){
				$errors[] = "Invalid points. It should be a number!";
			}else if($points<=0){
				$errors[] = "Invalid points. It should be 1 point or greater!";
			}

			$date_err = 0;
			if($start_date==""){
				$errors[] = "Indicate offer's start date";
				$date_err = 1;
			}

			if($end_date==""){
				$errors[] = "Indicate offer's end date";
				$date_err = 1;
			}

			if($date_err==0){
				if(str_replace("-", "", $start_date)>str_replace("-", "", $end_date) or str_replace("-", "", $end_date)<str_replace("-", "", $start_date)){
					$errors[] = "Invalid offer's date range.";
				}
			}
			

			if($days_inactive==""){
				$errors[] = "Set days of user inactivity";
			}else if(!is_numeric($days_inactive)){
				$errors[] = "Invalid days. It should be a number!";
			}else if($days_inactive<=0){
				$errors[] = "Invalid days. It should be 1 day or greater!";
			}

			if(count($errors)>0){
				$data['error'] = $errors;

				return $this->load->view('login_offer/add', $data, true);	
			}else{
				$insert_data['title'] = $title;
				$insert_data['content'] = $content;
				$insert_data['points'] = $points;
				$insert_data['start_date'] = $start_date;
				$insert_data['end_date'] = $end_date;
				$insert_data['days_inactive'] = $days_inactive;
				$insert_data['is_active'] = 2;
				$insert_data['date_created'] = date('Y-m-d H:i:s');

				$data['success'] = "Successfully Added!";
				$content = $this->db->select('*')->from('tbl_login_offers')->order_by('date_created', 'desc')
									->where('is_active', 1)->get()->result();

				$data['content'] = $content;

				$data['total'] = count($content);

				$this->db->insert('tbl_login_offers', $insert_data);
				// return $this->load->viewlogin_offer, $data, true);	
				return redirect('login_offer', $data);
			}
		}else{
			$data['contenta'] = "";
			return $this->load->view('login_offer/add', $data, true);	
		}
	}
	
	public function add() {
		$data['main_content'] = $this->add_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}

	public function edit($id) {
		$data['main_content'] = $this->edit_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	public function edit_content ($id) {
		if($this->input->post('submit')){
			$title = $_POST['title'];
			$content = $_POST['content'];
			$points = $_POST['points'];
			$start_date = $_POST['start_date'];
			$end_date = $_POST['end_date'];
			$days_inactive = $_POST['days_inactive'];

			$errors = [];

			if($title==""){
				$errors[] = "Indicate offer's title";
			}else if(strlen($title)>255){
				$errors[] = "Title character length exceeded. It should be 255 characters only";
			}
			if($content==""){
				$errors[] = "Indicate offer's content";
			}else if(strlen($title)>1000){
				$errors[] = "Content character length exceeded. It should be 1000 characters only";
			}
			if($points==""){
				$errors[] = "Indicate points to be rewarded";
			}else if(!is_numeric($points)){
				$errors[] = "Invalid points. It should be a number";
			}
			$date_err = 0;
			if($start_date==""){
				$errors[] = "Indicate offer's start date";
				$date_err = 1;
			}

			if($end_date==""){
				$errors[] = "Indicate offer's end date";
				$date_err = 1;
			}

			if($date_err==0){
				if(str_replace("-", "", $start_date)>str_replace("-", "", $end_date) or str_replace("-", "", $end_date)<str_replace("-", "", $start_date)){
					$errors[] = "Invalid offer's date range.";
				}
			}
			

			if($days_inactive==""){
				$errors[] = "Set days of user inactivity";
			}else if(!is_numeric($days_inactive)){
				$errors[] = "Invalid days. It should be a number";
			}

			if(count($errors)>0){
				$data['error'] = $errors;
			}else{
				$update_data['title'] = $title;
				$update_data['content'] = $content;
				$update_data['points'] = $points;
				$update_data['start_date'] = $start_date;
				$update_data['end_date'] = $end_date;
				$update_data['days_inactive'] = $days_inactive;

		        $this->db->where('id', $id);
		        $this->db->update('tbl_login_offers', $update_data);

				$data['success'] = "Successfully Edited!";
			}

			$content = $this->db->select('*')->from('tbl_login_offers')
										->where('id', $id)->get()->row();

			$data['offer'] = $content;
			return $this->load->view('login_offer/edit', $data, true);

		}else{
			$content = $this->db->select('*')->from('tbl_login_offers')
										->where('id', $id)->get()->row();

			$data['offer'] = $content;

			return $this->load->view('login_offer/edit', $data, true);
		}
	}
	public function delete($id)
	{
		$data = array(
            'is_active' => 0,
        );
        $this->db->where('id', $id);
        $this->db->update('tbl_login_offers', $data);

     	return redirect('login_offer', $data);
	}

	public function activate($id)
	{
		$data = array(
            'is_active' => 2,
        );
        $this->db->where('is_active', 1);
        $this->db->update('tbl_login_offers', $data);

		$data = array(
            'is_active' => 1,
        );
        $this->db->where('id', $id);
        $this->db->update('tbl_login_offers', $data);

     	return redirect('login_offer', $data);
	}
	public function deactivate($id)
	{
		$data = array(
            'is_active' => 2,
        );
        $this->db->where('id', $id);
        $this->db->update('tbl_login_offers', $data);

     	return redirect('login_offer', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {

		$this->db->select('*')->from('tbl_login_offers')->order_by('date_created', 'desc');

		if(isset($_GET['title']) and $_GET['title']!=""){
			$this->db->where('title LIKE '.'"%'.$_GET['title'].'%"');
		}
		if(isset($_GET['start_date']) and $_GET['start_date']!=""){
			$this->db->where('start_date', $_GET['start_date']);
		}
		if(isset($_GET['end_date']) and $_GET['end_date']!=""){
			$this->db->where('end_date', $_GET['end_date']);
		}
		if(isset($_GET['status']) and $_GET['status']!=""){
			$this->db->where('is_active', $_GET['status']);
		}

		$this->db->where('(is_active = 1 or is_active=2)');

		$content = $this->db->get()->result();
		$data['content'] = $content;

		$data['total'] = count($content);

		return $this->load->view('login_offer/index', $data, true);		
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'deactivate')
			$this->deactivate($this->uri->segment(3));
		elseif($method == 'activate')
			$this->activate($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		elseif($method == 'create')
			$this->create();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */