<?php

class Buy_mechanics extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('settings_model');
	}
		
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	public function index()
	{
		$data['main_content'] = $this->buy_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	private function buy_content() {
		if($this->input->post('submit')) {
			$this->settings_model->save_settings('buy_mechanics');
		}
		$data['settings'] = $this->settings_model->get_settings('buy_mechanics');
		$data['error'] = '';
		$data['title'] = 'Buy Mechanics';
		return $this->load->view('settings/index', $data, true);		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */