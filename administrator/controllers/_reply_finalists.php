<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reply_Finalists extends CI_Controller {
	
	var $_table;

	public function __construct() {
		parent::__construct();
		$this->load->model('module_model');
		$this->_table = 'tbl_finalist_comment_replies';
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {

		function toSlug($string) {
			$slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
   			return strtoupper($slug);
		}

		$page = $this->uri->segment(2, 1);
		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			unset($like['search']);
		}
		$param['like'] = $like;
		$param['offset'] = $data['offset'];
		$param['table'] = $this->_table;
		// $param['where'] = array('is_deleted'=>0);
		$param['order_by'] = array('field'=>'date_added', 'order'=>'DESC');
		$data['records'] = $this->global_model->get_rows($param)->result_array();

		$this->db->select('tbl_finalists.name, tbl_finalist_comments.*, tbl_finalist_comment_replies.*, tbl_finalist_comments.comment as comment_origin, tbl_registrants.nick_name, tbl_registrants.first_name, tbl_registrants.third_name, tbl_registrants.person_id')->from($this->_table);
		$this->db->join('tbl_finalist_comments', 'tbl_finalist_comments.finalist_comment_id = tbl_finalist_comment_replies.comment_id');
		$this->db->join('tbl_registrants', 'tbl_registrants.registrant_id = tbl_finalist_comment_replies.registrant_id');
		$this->db->join('tbl_finalists', 'tbl_finalists.finalist_id = tbl_finalist_comments.finalist_id');
		$this->db->limit(PER_PAGE, $data['offset']);

		// $this->db->order_by('date_added', 'DESC');
		$data['records'] = $this->db->get()->result_array();

		$records = $this->global_model->get_total_rows($param);
		$data['pagination'] = $this->global_model->pagination($records, $page, SITE_URL.'/reply_finalists');
		$access = $this->module_model->check_access('finalists');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = count($data['records']);
		$data['origins'] = $this->module_model->get_origins();

		// echo '<Pre>';
		// print_r($data['records']);
		// exit;

		return $this->load->view('finalists_reply/index', $data, TRUE);		
	}
	
	public function add() {
		$data['main_content'] = $this->add_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function add_content() {
		$error = '';
		if($this->input->post('submit')) 
		{
			//upload all first
			$config['upload_path'] = 'uploads/finalists/photos/';
			$config['allowed_types'] = '*';
			$this->load->library('upload', $config);
			$image = 'photo';

			$photo_files = $_FILES;
			$photo_file_count = count($_FILES['photo']['name']);
			$photo_file_names = array();
			if($photo_files['photo']['name'][0]) {
				for($i = 0; $i < $photo_file_count; $i++) {
					$_FILES['photo']['name'] = $photo_files['photo']['name'][$i];
					$_FILES['photo']['type'] = $photo_files['photo']['type'][$i];
					$_FILES['photo']['tmp_name'] = $photo_files['photo']['tmp_name'][$i];
					$_FILES['photo']['error'] = $photo_files['photo']['error'][$i];
					$_FILES['photo']['size'] = $photo_files['photo']['size'][$i];

					if($this->upload->do_upload($image))
					{
						//$photo_file_names[] = base_url().'uploads/finalists/photos/' . $photo_files['photo']['name'][$i];
						$photo_file_names[] = 'uploads/finalists/photos/'.$photo_files['photo']['name'][$i];
					} else 
					{
						print_r($this->upload->display_errors());
						exit;
					}
				}
			}
						

						$photo_array = array();
						foreach ($photo_file_names as $key => $value)
						{
							if($key == $this->input->post('coverphoto')[0])
							{
								
								$photo_array['coverphoto'][] =   $value;//$this->input->post('coverphoto')[0];
								//$photo_array['url'][] =  $value;
							}else
							{
								
								$photo_array['url'][] =  $value;
								
							}
						}


			$photos = $photo_array ? serialize($photo_array) : serialize(array());

				
			$config = array();
			$config['upload_path'] = 'uploads/finalists/videos/';
			$config['allowed_types'] = '*';
			$this->upload->initialize($config);
			$realpath = realpath($config['upload_path']);
			$video = 'video';

			$video_files = $_FILES;
			$video_file_count = count($_FILES['video']['name']);
			$video_file_names = array();
			if($video_files['video']['name'][0]) {
				for ($j=0; $j < $video_file_count; $j++) { 
					$_FILES['video']['name'] = $photo_files['video']['name'][$j];
					$_FILES['video']['type'] = $photo_files['video']['type'][$j];
					$_FILES['video']['tmp_name'] = $photo_files['video']['tmp_name'][$j];
					$_FILES['video']['error'] = $photo_files['video']['error'][$j];
					$_FILES['video']['size'] = $photo_files['video']['size'][$j];

					if($this->upload->do_upload($video)) {

						$video_file_names[] =  'uploads/finalists/videos/'. $video_files['video']['name'][$j];
						$realpath = str_replace("\\", '/', rtrim($realpath, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR);
						$thumbfilename = pathinfo($video_files['video']['name'][$j], PATHINFO_FILENAME).'.jpg';
						exec('ffmpeg -i '.$realpath.$video_files['video']['name'][$j].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realpath.'thumbnail/'.$thumbfilename);
						//exec('C:\ffmpeg\ffmpeg -i '.$realpath.$video_files['video']['name'][$j].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realpath.'thumbnail/'.$thumbfilename);



					} else {
						print_r($this->upload->display_errors());
						exit;
					}
				}
			}
						

						$video_array = array();
						foreach ($video_file_names as $key => $value)
						{

								$video_array['thumbnail'][] =  'uploads/finalists/videos/thumbnail/'.$thumbfilename;
								$video_array['url'][] =  $value;
							
						}

					
			$videos = $video_array ? serialize($video_array) : serialize(array());

			$this->db->insert('tbl_finalists', array(
				'name' => $this->input->post('name'),
				'person_id' => $this->input->post('person_id'),
				'location' => $this->input->post('location'),
				'write_ups' => $this->input->post('write_ups'),
				'photos' => $photos,
				'videos' => $videos,
				'is_deleted' => 0,
				'date_created' => date('Y-m-d H:i:s')
			));

			redirect('finalists');

			// $valid = $this->validate_fields();
			// if($valid) {
			// 	$this->module_model->save_module();	
			// 	redirect('module');
			// } else {
			// 	$error = 'Sorry, module name or uri already exists';
			// }
		}
		$data['error'] = $error;
		$data['module'] = $_POST;
		return $this->load->view('finalists/add', $data, true);			
	}
	
	public function edit() {
		$id = $this->uri->segment(3);
		$data['main_content'] = $this->edit_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function edit_content($id) {
		$error = '';
		if($this->input->post('submit')) {

			// kapag wala nabago
			// coverphoto_id = ''
			
			$data  = $this->db->where('finalist_id', $id)->get('tbl_finalists')->result_array()[0];

			$config['upload_path'] = 'uploads/finalists/photos/';
			$config['allowed_types'] = '*';
			$this->load->library('upload', $config);
			$image = 'photo';

			$photo_files = $_FILES;
			$photo_file_count = count($_FILES['photo']['name']);
			$photo_file_names = array();
			if($photo_files['photo']['name'][0]) {
				for($i = 0; $i < $photo_file_count; $i++) {
					$_FILES['photo']['name'] = $photo_files['photo']['name'][$i];
					$_FILES['photo']['type'] = $photo_files['photo']['type'][$i];
					$_FILES['photo']['tmp_name'] = $photo_files['photo']['tmp_name'][$i];
					$_FILES['photo']['error'] = $photo_files['photo']['error'][$i];
					$_FILES['photo']['size'] = $photo_files['photo']['size'][$i];

					if($this->upload->do_upload($image))
					{
						//$photo_file_names[] = base_url().'uploads/finalists/photos/' . $photo_files['photo']['name'][$i];
						$photo_file_names[] = 'uploads/finalists/photos/'.$photo_files['photo']['name'][$i];
					} else 
					{
						print_r($this->upload->display_errors());
						exit;
					}
				}
			}
						

						$photo_array = array();
						$photo_array1 = array();
						if($photo_file_names)
						{
								$count = 0;
									foreach ($photo_file_names as $key => $value)
									{
										

										if($this->input->post('coverphoto_id') != '')
										{

											$remove_uploadstring = str_replace('uploads/finalists/photos/', '', $value);
													$remove_uploadstring_coverphoto = str_replace('uploads/finalists/photos/', '',$this->input->post('coverphoto_id'));

														 if($remove_uploadstring == $remove_uploadstring_coverphoto)
														 	{
														 		
														 		$photo_array['coverphoto'][] = $value;
														 	}else
														 	{
														 		$photo_array['url'][] =  $value;
														 	}


										}else
										{
											if($count == 0)
											{
												$photo_array['coverphoto'][] = $value;
											}else
											{
													$photo_array['url'][] =  $value;
											}
										}
													
										$count++;
									}

						}else
						{
							if($this->input->post('coverphoto_id') != '')
							{
											foreach (unserialize($data['photos']) as $key => $v)
											{

												
												
													foreach ($v as $k => $value) 
													{

														if($key == 'coverphoto')
														{
															$photo_array1['url'][] =  $value;
														}
														if($key == 'url')
														{
																	$remove_uploadstring = str_replace('uploads/finalists/photos/', '', $value);
																	$remove_uploadstring_coverphoto = str_replace('uploads/finalists/photos/', '',$this->input->post('coverphoto_id'));
																		 
																		 if($remove_uploadstring == $remove_uploadstring_coverphoto)
																		 	{
																		 		
																		 		$photo_array1['coverphoto'][] = $value;
																		 	}else
																		 	{
																		 		$photo_array1['url'][] =  $value;
																		 	}

														}
													}
												
											
											}
								}
						}

	if(!empty($photo_array))
	{
		$photos = serialize($photo_array);
	}elseif (!empty($photo_array1)) 
	{
		$photos = serialize($photo_array1);
	}else
	{
		$photos = $data['photos'];
	}	



			$config = array();
			$config['upload_path'] = 'uploads/finalists/videos/';
			$config['allowed_types'] = '*';
			$this->upload->initialize($config);
			$realpath = realpath($config['upload_path']);
			$video = 'video';

			$video_files = $_FILES;
			$video_file_count = count($_FILES['video']['name']);
			$video_file_names = array();
			if($video_files['video']['name'][0]) {
				for ($j=0; $j < $video_file_count; $j++) { 
					$_FILES['video']['name'] = $photo_files['video']['name'][$j];
					$_FILES['video']['type'] = $photo_files['video']['type'][$j];
					$_FILES['video']['tmp_name'] = $photo_files['video']['tmp_name'][$j];
					$_FILES['video']['error'] = $photo_files['video']['error'][$j];
					$_FILES['video']['size'] = $photo_files['video']['size'][$j];

					if($this->upload->do_upload($video)) {

						$video_file_names[] =  'uploads/finalists/videos/'. $video_files['video']['name'][$j];
						$realpath = str_replace("\\", '/', rtrim($realpath, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR);
						$thumbfilename = pathinfo($video_files['video']['name'][$j], PATHINFO_FILENAME).'.jpg';
						exec('ffmpeg -i '.$realpath.$video_files['video']['name'][$j].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realpath.'thumbnail/'.$thumbfilename);
						//exec('C:\ffmpeg\ffmpeg -i '.$realpath.$video_files['video']['name'][$j].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realpath.'thumbnail/'.$thumbfilename);



					} else {
						print_r($this->upload->display_errors());
						exit;
					}
				}
			}
						

						$video_array = array();
						if($video_file_names)
						{
								foreach ($video_file_names as $key => $value)
								{

										$video_array['thumbnail'][] =  'uploads/finalists/videos/thumbnail/'.$thumbfilename;
										$video_array['url'][] =  $value;
									
								}
						}

					
			$videos = $video_array ? serialize($video_array) : $data['videos'];

			$this->db->update('tbl_finalists', array(
				'name' => $this->input->post('name'),
				'person_id' => $this->input->post('person_id'),
				'location' => $this->input->post('location'),
				'write_ups' => $this->input->post('write_ups'),
				'photos' => $photos,
				'videos' => $videos
			), array('finalist_id' => $this->input->post('id')));
			redirect('finalists');
		}
		$data['error'] = $error;
		// $data['module'] = $_POST ? $_POST : $this->module_model->get_module(array('module_id'	=> $id))[0];
		
		$data['record'] = $this->db->where('finalist_id', $id)->get('tbl_finalists')->result_array()[0];
		return $this->load->view('finalists/edit', $data, true);
	}
	
	
	public function validate() {
		$valid = $this->validate_fields();
		$data['main_content'] = $valid ? '1' : '0';
		$this->load->view('blank-template', $data); 
	}
	
	private function validate_fields() {
		$name = $this->input->post('module_name');
		$uri = $this->input->post('uri');
		$id = $this->input->post('id');
		$where['module_name'] = $name;
		$where['uri'] = $uri;
		$module = $this->module_model->get_module($where);
		if($module) {
			if($id && $module[0]['module_id'] == $id)
				return true;
			return false;
		}
		
		return true;
	}
	
	public function delete() 
	{
		//$this->db->delete('tbl_finalists', array('finalist_id' => $this->uri->segment(3)));

		$this->db->update('tbl_finalists', array(
				'is_deleted' =>1
			), array('finalist_id' => $this->uri->segment(3)));

		redirect('finalists');
		// $table = 'tbl_modules';
		// $id = $this->uri->segment(3);
		// $field = 'module_id';
		// if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/module') >= 0) {
		// 	$where[$field] = $id;
		// 	$this->global_model->delete_record($table, $where);
		// }
		// redirect('module');
	}

	public function changeStatus()
	{
		if($_POST && $_POST['action']) {
			$registrant = @$this->db->select()->from('tbl_finalist_comments')->where('finalist_comment_id', $_POST['id'])->get()->row();
			$id = isset($_POST['id']) ? $_POST['id'] : NULL;
			if($registrant) {
				$registrant = $registrant->registrant_id;
			} else {
				if($_POST['action'] != 'multiple-approve' AND $_POST['action'] != 'multiple-reject') {
					echo "Registrant could not be found";
					return false;	
				}				
			}
			switch ($_POST['action']) {
				case 'approve':
					$points = $this->points_model->earn(COMMENT, array(
						'suborigin'=>$id,
						'registrant'=>$registrant,
						'remarks'=>$this->get_remarks(FINALIST, $id)
					));

					$this->notification_model->notify($registrant, COMMENT, array(
						'message'=>$this->get_notification_copy(FINALIST, $id, 1, $points),
						'suborigin'=>$id
					));

					$this->update_status('comment', 'tbl_finalist_comments', 1, $id);
					$this->db->set('total_points', 'total_points + '.$points, false);
					$this->db->where('registrant_id', $registrant);
					$this->db->update('tbl_registrants');
					break;

				case 'multiple-approve':
					if($_POST['ids']) {
						foreach($_POST['ids'] as $k => $v) {
							$id = $v;
							$registrant_d = $this->db->select()->from('tbl_finalist_comments')->where('finalist_comment_id', $id)->get()->row();
							$registrant = $registrant_d->registrant_id;
							if($registrant_d->status != 1) {
								$points = $this->points_model->earn(COMMENT, array(
									'suborigin'=>$id,
									'registrant'=>$registrant,
									'remarks'=>$this->get_remarks(FINALIST, $id)
								));

								$this->notification_model->notify($registrant, COMMENT, array(
									'message'=>$this->get_notification_copy(FINALIST, $id, 1, $points),
									'suborigin'=>$id
								));

								$this->update_status('comment', 'tbl_finalist_comments', 1, $id);
								$this->db->set('total_points', 'total_points + '.$points, false);
								$this->db->where('registrant_id', $registrant);
								$this->db->update('tbl_registrants');
							}							
						}
					}
					break;

				case 'multiple-reject':
					if($_POST['ids']) {
						foreach($_POST['ids'] as $k => $v) {
							$id = $v;
							$registrant_d = $this->db->select()->from('tbl_finalist_comments')->where('finalist_comment_id', $id)->get()->row();
							$registrant = $registrant_d->registrant_id;
							if($registrant_d->status != 2) {
								$points = $this->points_model->deactivate($registrant, array(
									array(
										'origin'=>COMMENT,
										'suborigin'=>$id
									)
								));

								$this->notification_model->notify($registrant, COMMENT, array(
									'message'=>$this->get_notification_copy(FINALIST, $id, 0, 0, $message = isset($_POST['message']) ? $_POST['message'] : NULL),
									'suborigin'=>$id
								));

								$this->update_status('comment', 'tbl_finalist_comments', 2, $id);
							}
						}
						redirect('finalists_comments');
					}
					break;

				case 'reject':
					$points = $this->points_model->deactivate($registrant, array(
						array(
							'origin'=>COMMENT,
							'suborigin'=>$id
						)
					));

					$this->notification_model->notify($registrant, COMMENT, array(
						'message'=>$this->get_notification_copy(FINALIST, $id, 0, 0, $message = isset($_POST['message']) ? $_POST['message'] : NULL),
						'suborigin'=>$id
					));

					$this->update_status('comment', 'tbl_finalist_comments', 2, $id);
					break;
				
				default:
					$this->db->update($this->_table, array('status'=>2), array('finalist_comment_id'=>$_POST['id']));
					break;
			}
			echo $_POST['action'];
		}
	}

	private function update_status($type, $table, $new_status, $where, $params = FALSE)
	{
		switch ($type) {
			case 'comment':
				$this->db->update($table, array('status'=>$new_status), array('finalist_comment_id'=>$where));
				break;

			case 'reply':
				
				break;
			
			default:
				return false;
				break;
		}
		if($this->db->affected_rows()) {
			return TRUE;
		}
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		elseif($method == 'changeStatus')
			$this->changeStatus();
		else
			$this->index();
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */