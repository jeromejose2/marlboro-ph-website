<?php

class Dbam extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$this->load->helper('video_helper');
	}
	public function index()
	{
		$data = array();

		$this->db->start_cache();

		$search = array();
		$search['title'] = (string) $this->input->get('title');
		$search['status'] = (string) $this->input->get('status');
		$search['from_date'] = (string) $this->input->get('from_date');
		$search['to_date'] = (string) $this->input->get('to_date');

		$page = $this->uri->segment(2, 1);
		$this->db->select()
			->from('tbl_dbam');

		if ($search['title']) {
			$this->db->like('dbam_title', $search['title']);
		}

		if ($search['status'] != '') {
			$this->db->where('dbam_status', $search['status']);
		}

		if ($search['from_date']) {
			$this->db->where('dbam_date_created >=', $search['from_date']);
		}

		if ($search['to_date']) {
			$this->db->where('dbam_date_created <=', $search['to_date']);
		}

		$data['offset'] = ($page - 1) * PER_PAGE;
		$data['total'] = $this->db->count_all_results();
		$this->db->limit(PER_PAGE, $data['offset']);
		$data['records'] = $this->db->get()->result();

		$this->db->stop_cache();
		$this->db->flush_cache();

		$data['pagination'] = $this->global_model->pagination($data['total'], $page, SITE_URL.'/dbam');
		$data['access'] = $this->module_model->check_access('dbam');

		$data['nav'] = $this->nav_items();
		$data['main_content'] = $this->load->view('dbam/index', $data, true);
		$this->load->view('main-template', $data);
	}

	public function edit()
	{
		$data = array(
			'error' => '',
			'nav' => $this->nav_items()
		);

		$id = $this->input->get('id');
		if (!$id) {
			redirect('dbam');
		}

		$data['record'] = $this->db->select()
			->from('tbl_dbam')
			->where('dbam_id', $id)
			->limit(1)
			->get()
			->row();

		if (!$data['record']) {
			show_404();
		}

		$post = $this->input->post();
		if ($post) {

			$this->load->helper('resize');
			$this->load->library('upload');
			$this->load->library('form_validation');
			$this->form_validation->set_rules('title', 'required|alpha_numeric_space');
			$this->form_validation->set_rules('status',	'required|numeric');

			$path = './uploads/dbam/';
			if (!is_dir($path)) {
				mkdir($path);
			}
			if ($this->form_validation->run()) {

				$updateData = array(
					'dbam_title' => $post['title'],
					'dbam_status' => $post['status'],
				);

				if (!empty($_FILES['dbam_upload']['name'])) {

					$config = array();
					$config['upload_path'] = $path;
					$config['allowed_types'] = DBAM_FILE_UPLOAD_TYPES;
					if (strpos($_FILES['dbam_upload']['type'], 'image') !== false) {
						$config['max_size'] = FILE_UPLOAD_LIMIT;
					} else {
						$config['max_size']	= VIDEO_UPLOAD_LIMIT;
					}
					$config['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['dbam_upload']['name'], PATHINFO_EXTENSION);

					$this->upload->initialize($config);

					if ($this->upload->do_upload('dbam_upload')) {

						$uploadSuccessData = $this->upload->data();
						if ($uploadSuccessData['is_image']) {
							$updateData['dbam_type'] = 1;
							resize(array(
								'width' => 576,
								'height'=> 296,
								'source_image' => $path.$config['file_name'],
								'new_image_path' => $path,
								'file_name'=> $config['file_name']
							));

							resize(array(
								'width' => 150,
								'height'=> 150,
								'source_image' => $path.$config['file_name'],
								'new_image_path' => $path,
								'file_name'=> $config['file_name']
							));
						} else {

							$updateData['dbam_type'] = 2;

							$realdir = realpath($path);
							if ($realdir) {
								$realdir = str_replace("\\", '/', rtrim($realdir, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR);

								$thumbfilename = 'thumb_'.$config['file_name'].'.jpg';
								exec('ffmpeg -i '.$realdir.$config['file_name'].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realdir.$thumbfilename);
								resize(array(
									'width' => 150,
									'height'=> 150,
									'source_image' => $path.$thumbfilename,
									'new_image_path' => $path,
									'file_name'=> $thumbfilename
								));

								$duration = get_video_duration($realdir.$config['file_name']);
								$updateData['duration'] = $duration;

								$thumbfilename = 'thumb_'.$config['file_name'].'.mp4';
								// exec('ffmpeg -i '.$realdir.$config['file_name'].' -ss 00:00:05 -t 00:00:10 -r 10 -y '.$realdir.$thumbfilename);
							}
							
						}
						$updateData['dbam_content'] = $config['file_name'];
					} else {
						$data['error'] = $this->upload->display_errors();
					}
				}

				if ($post['remove_thumb']) {
					$updateData['dbam_thumbnail'] = null;
				} elseif (!empty($_FILES['dbam_thumbnail']['name'])) {
					$tmpConfig = array();
					$tmpConfig['upload_path'] = $path;
					$tmpConfig['allowed_types'] = FILE_UPLOAD_ALLOWED_TYPES;
					$tmpConfig['max_size'] = FILE_UPLOAD_LIMIT;
					$tmpConfig['file_name'] = 'thumb_'.sha1(uniqid()).'.'.pathinfo($_FILES['dbam_thumbnail']['name'], PATHINFO_EXTENSION);
					$this->upload->initialize($tmpConfig);
					if ($this->upload->do_upload('dbam_thumbnail')) {
						resize(array(
							'width' => 150,
							'height'=> 150,
							'source_image' => $path.$tmpConfig['file_name'],
							'new_image_path' => $path,
							'file_name'=> $tmpConfig['file_name']
						));
						$updateData['dbam_thumbnail'] = $tmpConfig['file_name'];
					} else {
						$data['error'] = $this->upload->display_errors();
					}
				}

				if (!$data['error']) {

					$this->db->where('dbam_id', $id);
					$this->db->update('tbl_dbam', $updateData);
					redirect($post['back_url']);
				
				}
				
			} else {
				$data['error'] = validation_errors();
			}
			
		}

		
		$data['main_content'] = $this->load->view('dbam/edit', $data, true);
		$this->load->view('main-template', $data);
	}

	public function create_gif()
	{
		$path = './uploads/dbam/';
		$realdir = realpath($path);
		$realdir = str_replace("\\", '/', rtrim($realdir, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR);
		$result = $this->db->select()
			->from('tbl_dbam')
			->where('dbam_type', 2)
			->get()
			->result_array();
		foreach ($result as $r) {
			$thumbfilename = 'thumb_'.$r['dbam_content'].'.mp4';
			exec('ffmpeg -i '.$realdir.$r['dbam_content'].' -ss 00:00:05 -t 00:00:10 -r 10 -y '.$realdir.$thumbfilename);
		}
	}

	public function upload()
	{
		$config = array();
		$path = './uploads/dbam/';
		if (!is_dir($path)) {
			mkdir($path);
		}
		// $path .= 'media/';
		// if (!is_dir($path)) {
		// 	mkdir($path);
		// }

		$config['upload_path'] = $path;
		$config['allowed_types'] = DBAM_FILE_UPLOAD_TYPES;
		if (strpos($_FILES['myFile']['type'], 'image') !== false) {
			$config['max_size'] = FILE_UPLOAD_LIMIT;
		} else {
			$config['max_size']	= VIDEO_UPLOAD_LIMIT;
		}
		$config['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['myFile']['name'], PATHINFO_EXTENSION);

		$this->load->helper('resize');
		$this->load->library('upload', $config);

		if ($this->upload->do_upload('myFile')) {
			$data = $this->upload->data();
			if ($data['is_image']) {
				resize(array(
					'width' => 150,
					'height'=> 150,
					'source_image' => $path.$config['file_name'],
					'new_image_path' => $path,
					'file_name'=> $config['file_name']
				));
				resize(array(
					'width' => 576,
					'height'=> 296,
					'source_image' => $path.$config['file_name'],
					'new_image_path' => $path,
					'file_name'=> $config['file_name']
				));
			} else {
				$realdir = realpath($path);
				if ($realdir) {
					$realdir = str_replace("\\", '/', rtrim($realdir, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR);
					$thumbfilename = 'thumb_'.$config['file_name'].'.jpg';
					exec('ffmpeg -i '.$realdir.$config['file_name'].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realdir.$thumbfilename);
					resize(array(
						'width' => 150,
						'height'=> 150,
						'source_image' => $path.$thumbfilename,
						'new_image_path' => $path,
						'file_name'=> $thumbfilename
					));

					$thumbfilename = 'thumb_'.$config['file_name'].'.mp4';
					// exec('ffmpeg -i '.$realdir.$config['file_name'].' -ss 00:00:05 -t 00:00:10 -r 10 -y '.$realdir.$thumbfilename);
					
				}
			}
			$this->output->set_output($config['file_name'].' - '.($data['is_image'] ? 1 : 2));
		} else {
			$this->output->set_status_header(500);
			$this->output->set_output($this->upload->display_errors('', ''));
		}


	}

	// public function add()
	// {
	// 	$data = array(
	// 		'error' => ''
	// 	);

	// 	$post = $this->input->post();
	// 	if ($post) {

	// 		$this->load->helper('resize');
	// 		$this->load->library('upload');
	// 		$this->load->library('form_validation');
	// 		$this->form_validation->set_rules('title', 'required|alpha_numeric_space');
	// 		$this->form_validation->set_rules('status',	'required|numeric');

	// 		if ($this->form_validation->run()) {

	// 			$config = array();
	// 			$path = './uploads/dbam/';
	// 			if (!is_dir($path)) {
	// 				mkdir($path);
	// 			}

	// 			$config['upload_path'] = $path;
	// 			$config['allowed_types'] = DBAM_FILE_UPLOAD_TYPES;
	// 			if (strpos($_FILES['dbam_upload']['type'], 'image') !== false) {
	// 				$config['max_size'] = FILE_UPLOAD_LIMIT;
	// 			} else {
	// 				$config['max_size']	= '20000';
	// 			}
	// 			$config['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['dbam_upload']['name'], PATHINFO_EXTENSION);

	// 			$this->upload->initialize($config);

	// 			if ($this->upload->do_upload('dbam_upload')) {

	// 				$cms_user_id = $this->login_model->extract_user_details()['cms_user_id'];
	// 				$uploadSuccessData = $this->upload->data();

	// 				if ($uploadSuccessData['is_image']) {
	// 					resize(array(
	// 						'width' => 576,
	// 						'height'=> 296,
	// 						'source_image' => $path.$config['file_name'],
	// 						'new_image_path' => $path,
	// 						'file_name'=> $config['file_name']
	// 					));

	// 					resize(array(
	// 						'width' => 150,
	// 						'height'=> 150,
	// 						'source_image' => $path.$config['file_name'],
	// 						'new_image_path' => $path,
	// 						'file_name'=> $config['file_name']
	// 					));
	// 				} else {
	// 					$realdir = realpath($path);
	// 					if ($realdir) {
	// 						$realdir = str_replace("\\", '/', rtrim($realdir, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR);
	// 						$thumbfilename = 'thumb_'.$config['file_name'].'.jpg';
	// 						exec('ffmpeg -i '.$realdir.$config['file_name'].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realdir.$thumbfilename);
	// 						resize(array(
	// 							'width' => 150,
	// 							'height'=> 150,
	// 							'source_image' => $path.$thumbfilename,
	// 							'new_image_path' => $path,
	// 							'file_name'=> $thumbfilename
	// 						));
	// 					}
	// 				}
					
	// 				$this->db->insert('tbl_dbam', array(
	// 					'dbam_content' => $config['file_name'],
	// 					'dbam_title' => $post['title'],
	// 					'dbam_type' => $uploadSuccessData['is_image'] ? 1 : 2,
	// 					'dbam_date_created' => date('Y-m-d H:i:s'),
	// 					'dbam_status' => $post['status'],
	// 					'cms_user_id' => $cms_user_id
	// 				));
	// 				if ($this->db->affected_rows()) {
	// 					redirect('dbam');
	// 				}

	// 			} else {
	// 				$data['error'] = $this->upload->display_errors();
	// 			}

				
	// 		} else {
	// 			$data['error'] = validation_errors();
	// 		}
			
	// 	}

	// 	$data['nav'] = $this->nav_items();
	// 	$data['main_content'] = $this->load->view('dbam/add', $data, true);
	// 	$this->load->view('main-template', $data);
	// }

	public function add()
	{
		$data = array(
			'error' => ''
		);
		$post = $this->input->post();
		if ($post) {

			$today = date('Y-m-d H:i:s');
			$cms_user_id = $this->login_model->extract_user_details()['cms_user_id'];
			foreach ($post['dbam_upload'] as $k => $m) {
				if (!$m)
					continue;
				$m = explode(' - ', $m);
				if (count($m) != 2) {
					continue;
				}
				$file = $m[0];
				$type = $m[1];
				$title = isset($post['dbam_title'][$k]) ? $post['dbam_title'][$k] : null;
				$realdir = realpath('./uploads/dbam/');
				$realdir = str_replace("\\", '/', rtrim($realdir, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR);
				$this->db->insert('tbl_dbam', array(
					'dbam_title' => $title,
					'dbam_type' => $type,
					'dbam_content' => $file,
					'dbam_date_created' => $today,
					'dbam_status' => $post['status'],
					'cms_user_id' => $cms_user_id,
					'dbam_duration' => get_video_duration($realdir.$file)
				));
			}

			redirect('dbam');

		}
		
		$data['nav'] = $this->nav_items();
		$data['main_content'] = $this->load->view('dbam/add', $data, true);
		$this->load->view('main-template', $data);
	}

	public function delete()
	{
		$id = $this->input->get('id');
		if ($id) {
			$this->db->where('dbam_id', $id)
				->delete('tbl_dbam');
		}
		redirect($this->input->server('HTTP_REFERER'));
	}

	public function change_status()
	{
		$id = $this->input->get('id');
		if ($id) {
			$this->db->where('dbam_id', $id)
				->update('tbl_dbam', array(
					'dbam_status' => (int) $this->input->get('status')
				));
		}
		redirect($this->input->server('HTTP_REFERER'));
	}

	private function nav_items()
	{
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}

	public function export()
	{
		$this->load->library('to_excel_array');

		$search = array();
		$search['title'] = (string) $this->input->get('title');
		$search['status'] = (string) $this->input->get('status');
		$search['from_date'] = (string) $this->input->get('from_date');
		$search['to_date'] = (string) $this->input->get('to_date');

		$rows = array(array(
			'dbam_content' => 'Media',
			'dbam_title' => 'Title',
			'dbam_type' => 'Type',
			'dbam_status' => 'Status',
			'dbam_date_created' => 'Date Created'
		));
		$this->db->select(implode(',', array_keys($rows[0])))
			->from('tbl_dbam');

		if ($search['title']) {
			$this->db->like('dbam_title', $search['title']);
		}

		if ($search['status'] != '') {
			$this->db->where('dbam_status', $search['status']);
		}

		if ($search['from_date']) {
			$this->db->where('dbam_date_created >=', $search['from_date']);
		}

		if ($search['to_date']) {
			$this->db->where('dbam_date_created <=', $search['to_date']);
		}

		foreach ($this->db->get()->result_array() as $data) {
			if ($data['dbam_status'] == 1) {
				$data['dbam_status'] = 'Published';
			} else {
				$data['dbam_status'] = 'Not Published';
			}
			if ($data['dbam_type'] == 1) {
				$data['dbam_type'] = 'Image';
			} elseif ($data['dbam_type'] == 2) {
				$data['dbam_type'] = 'Video';
			}
			$data['dbam_content'] = BASE_URL.'uploads/dbam/'.$data['dbam_content'];
			$rows[] = $data;
		}
		$this->to_excel_array->to_excel($rows, 'Dont_be_a_maybe_'.date('YmdHis'));
	}

	public function _remap($method)
	{
		if (is_numeric($method)) {
			$this->index();
		} else {
			$this->{$method}();
		}
	}

}