<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class User_logins extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		redirect('user_logins_per_day');
		$this->load->model('explore_model');
	} 

 	 
 	public function index()
	{
		if(!$this->input->get('from_date_login') || !$this->input->get('to_date_login')) {
			redirect('user_logins?from_date_login=' . date('Y-m-d', strtotime('-30 days')) .  '&to_date_login=' . date('Y-m-d'));
		}
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
		//$this->output->enable_profiler(TRUE);

 	}
 	 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
		$this->load->helper('paging');
		$offset = (int)$this->input->get('per_page');
	    $limit = PER_PAGE;
	    $filters = $this->get_filters(); 

		$access = $this->module_model->check_access('user_logins');
		$total_rows = $this->explore_model->get_rows(array('table'=>'tbl_login as p',
															 'where'=>array_merge($filters['where_filters'],array()),
														     'like'=>$filters['like_filters'],
														     'join'=>array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=p.registrant_id'),
 														     'fields'=>"p.login_id",
														     'group_by'=>'p.registrant_id'
 														     )
														)->num_rows();
 		$data['header_text'] = 'User Logins';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total_rows'] = $total_rows;
		$data['query_strings'] = $filters['query_strings'];
 		$field = $this->input->get('field') ? $this->input->get('field') : 'first_name';
 		$sort = $this->input->get('sort') ? $this->input->get('sort') : 'asc';
		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_login as p',
															 'where'=>array_merge($filters['where_filters'],array()),
														     'like'=>$filters['like_filters'],
														     'join'=>array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=p.registrant_id'),
														     'order_by' => array('field'=>$field,'order'=>$sort),
														     'offset'=>$offset,
														     'limit'=>$limit,
														     'fields'=>"r.first_name,r.third_name, COUNT(*) as logins, p.registrant_id",
														     'group_by'=>'p.registrant_id'
 														     )
														);
  		$total = $this->get_total();
 		$data['total'] = $total;
 		$data['ave'] = number_format(($total / $total_rows), 2);
 		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']);
 		return $this->load->view($this->uri->segment(1).'/list',$data,TRUE);
	} 

	private function get_total() {
		$total = 0;
		$filters = $this->get_filters(); 
		$records = $this->explore_model->get_rows(array('table'=>'tbl_login as p',
															 'where'=>array_merge($filters['where_filters'],array()),
														     'like'=>$filters['like_filters'],
														     'join'=>array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=p.registrant_id'),
 														     'fields'=>"p.login_id, COUNT(*) as logins",
														     'group_by'=>'p.registrant_id'
 														     ))->result_array();
													
		if($records) {
			foreach ($records as $key => $value) {
				$total += $value['logins'];
			}
		}
		return $total;
	}

	

	public function get_filters()
	   {
		   
		   $where_filters = array('DATE(date_login) >='=>'from_date_login','DATE(date_login) <='=>'to_date_login','registrant_id'=>'registrant_id');
		   $like_filters = array("CONCAT(first_name,' ',third_name)"=>'name');
		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}									 
				  
 		   } 	    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
		   
	   } 


	   public function export()
	   {
	   		$from = $this->input->get('from_date_login') ? $this->input->get('from_date_login') : date('Y-m-d', strtotime('-30 days'));
	   		$to = $this->input->get('to_date_login') ? $this->input->get('to_date_login') : date('Y-m-d');
	   		$this->load->library('to_excel_array');
			$filters = $this->get_filters();
			$field = $this->input->get('field') ? $this->input->get('field') : 'first_name';
 			$sort = $this->input->get('sort') ? $this->input->get('sort') : 'asc';
		   	$query = $this->explore_model->get_rows(array('table'=>'tbl_login as p',
															 'where'=>array_merge($filters['where_filters'],array()),
														     'like'=>$filters['like_filters'],
														     'join'=>array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=p.registrant_id'),
														     'order_by' => array('field'=>'p.registrant_id','order'=>$sort),
														     'fields'=>"r.first_name,r.third_name, r.email_address, COUNT(*) as logins, p.registrant_id, DATE(p.date_login) AS dlogin",
														     'group_by'=>array('p.registrant_id','DATE(p.date_login)')
 														     )
														);

		   	$res[] = array('Logins (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')');
		   	
		   	if($query->num_rows()){
		   		$start = strtotime($from);
		   		$end = strtotime($to);
		   		$fields = array('Name', 'Email');
		   		$dates = array();
		   		while($start <= $end) {
		   			$fields[] = date('m/d', $start);
		   			$dates[] = date('Ymd', $start);
		   			$start = strtotime('+1 day', $start);	
		   		}
		   		$res[] = $fields;
		   		
		   		$cur_user = 0;
		   		$rows = array();
		   		foreach($query->result() as $v){
 		   			if($cur_user != $v->registrant_id) {
 		   				$res[] = $rows;
 		   				$rows = array($v->first_name . ' ' . $v->third_name, $v->email_address);
 		   			} 
 		   			foreach($dates as $k => $date) {
	   					if(date('Ymd', strtotime($v->dlogin)) == $date) {
	   						$rows[$k + 2] = $v->logins;
	   					} elseif(!isset($rows[$k + 2])) {
	   						$rows[$k + 2] = '0';
	   					}
	   					//echo date('Ymd', strtotime($v->dlogin))  . ' ' .  $date . ' ' . $v->registrant_id . ' = ' . $rows[$k + 2] . '<br>';
	   				}
 		   			$cur_user = $v->registrant_id;
 		   			//$res[] = array($v->first_name . ' ' . $v->third_name, $v->logins);
		   		}
		   		$res[] = $rows;
		   	}
		   	$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));

	   }

	   public function view_logins()
	   {

	   		$registrant_id = $this->input->get('registrant_id');
	   		$filters = $this->get_filters();
	   		$data['registrant'] = $this->explore_model->get_row(array('table'=>'tbl_registrants',
	   																	'where'=>array('registrant_id'=>$registrant_id)
	   																)
 	   															);

	   		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_login',
		   														'where'=>$filters['where_filters'],
		   														'order_by'=>array('field'=>'date_login','order'=>'ASC')
		   														)
	   														);

	   		$this->load->view('user_logins/breakdown-logins',$data);

	   }

	   

 
}
