<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Global_Model extends CI_Model {
	 
 	public function __construct()
	{
		parent::__construct(); 
	}
	
	
	# generate pagination links
	public function pagination($total_rows, $cur_page, $base_url, $limit = PER_PAGE) {
		$this->load->library('pagination');
		$settings = array("total_rows"			=> $total_rows,
						  "base_url"			=> $base_url,
						  "cur_page"			=> $cur_page,
						  "use_page_numbers"	=> true,
						  "num_links"			=> 4,
						  "uri_segment"			=> 2,
						  "per_page"			=> $limit,	
						  "suffix"				=> '?'.http_build_query($_GET, '', "&"),
						  "full_tag_open" => '',
						  "full_tag_close" => '',
						  "num_tag_open" => '<li>',
						  "num_tag_close" => '</li>',
						  "cur_tag_open" => '<li class="active"><a>',
						  "cur_tag_close" => '</a></li>',
						  "next_link" => 'Next',
						  "next_tag_open" => '<li>',
						  "next_tag_close" => '</li>',
						  "prev_link" => 'Prev',
						  "prev_tag_open" => '<li>',
						  "prev_tag_close" => '</li>',
						  "first_link" => 'First',
						  "first_tag_open" => '<li>',
						  "first_tag_close" => '</li>',
						  "last_link" => 'Last',
						  "last_tag_open" => '<li>',
						  "last_tag_close" => '</li>',
						  "first_url" => $base_url . '?'.http_build_query($_GET, '', "&"));


		$this->pagination->initialize($settings);
		$pagination_links = $this->pagination->create_links();
		$select25 = $this->input->get('limit') && $this->input->get('limit') == '25' ?  "<option selected>25</option>" : "<option>25</option>";
		$select50 = $this->input->get('limit') && $this->input->get('limit') == '50' ?  "<option selected>50</option>" : "<option>50</option>";
		$select100 = $this->input->get('limit') && $this->input->get('limit') == '100' ?  "<option selected>100</option>" : "<option>100</option>";
		$select500 = $this->input->get('limit') && $this->input->get('limit') == '500' ?  "<option selected>500</option>" : "<option>500</option>";
		return '<div>
                  Show: 
                  <select>
                  	<option>10</option>
                    '. $select25 . '
                    '. $select50 . '
                    '. $select100 . '
                    '. $select500 . '
                  </select>
               </div>' . $pagination_links;	 	
	} 
	
	public function delete_record($table, $where = '') {
		if(is_array($where)) {
			$this->db->where($where);
			$this->db->update($table, array('is_deleted'	=> 1));
		} else {
			$query = "UPDATE $table SET is_deleted = 1 $where";
			$this->db->query($query);	
		}
	}
	
	public function update_record($table, $where = '', $set) {
		if(is_array($where)) {
			$this->db->where($where);
			$this->db->update($table, $set);
		} else {
			$post = '';
			if($set) {
				foreach($set as $k => $v) {
					$post .= $k . " = '" . mysql_real_escape_string($v) . "',";
				}
			}
			$post = substr($post, 0, -1);
			$query = "UPDATE $table SET $post $where";
			$this->db->query($query);
		}	
	}
	
	public function get_status($type = 'account') {
		$status = array();
		switch($type) {
			case 'mod':
				$status[0] = 'Pending';
				$status[1] = 'Approved';
				$status[2] = 'Rejected';
				break;	
			case 'account':
			default:
				$status[VERIFIED] = 'Verified';
				$status[CSR_APPROVED] = 'CSR Approved';
				$status[PENDING_GIID] = 'Pending GIID Upload';
				$status[PENDING_CSR] = 'Pending CSR Approval';
				$status[REJECTED_NO_GIID] = 'Rejected - No GIID Upload';
				$status[REJECTED_GIID] = 'Rejected - GIID Invalid';
				$status[DEACTIVATED] = 'Deactivated';
				$status[ARCLIGHT_FAILED] = 'Arclight Failed';
				$status[MYM_APPROVED] = 'MYM Approved';
				$status[ARCLIGHT_IN_PROCESS] = 'Arclight In Process';
				break;
		}
		
		return $status;
	}
	
	public function get_provinces() {
		$provinces = $this->db->select('*')
							  ->from('tbl_provinces')
							  ->order_by('province')
							  ->get()
							  ->result_array();
		return $provinces;
	}
	
	public function get_cities($province = '') {
		$cities = $this->db->select('*')
							  ->from('vw_cities')
							  ->where('province_id', $province)
							  ->order_by('city')
							  ->group_by('city')
							  ->get()
							  ->result_array();
		return $cities;
	}
	
	public function send_email($data) {
		$this->load->library('email');
		$config['mailtype'] = 'html';
		$this->email->initialize($config);	
		$this->email->from($data['email'], $data['name']);
		$this->email->to($data['to']);
		$this->email->subject($data['subject']);
		$this->email->message($data['content']);
	
		if($this->email->send())
			return true;
		else 
			print_r($this->email->print_debugger());
	}

	function get_rows($param)
	{
  		array_filter($param);
	
		if(isset($param['fields'])){
			$this->db->select($param['fields']);
		}
		
		if(isset($param['order_by']) && $param['order_by']){
			$order_by = $param['order_by'];
 			$this->db->order_by($order_by['field'], $order_by['order']);
		}
		
		if(isset($param['having']) && $param['having']){
			
			$having = $param['having'];
			foreach($having as $key=>$v){
				$this->db->having($key, $v); 
				
			}
			
		}

		if(isset($param['like']) && $param['like']){
			
			$like = $param['like'];
			foreach($like as $key=>$v){
				
				$this->db->like($key, $v); 
				
			}
			
		}

		if(isset($param['or_like']) && $param['or_like']){
			
			$or_like = $param['or_like'];
			foreach($or_like as $key=>$v){				
				$this->db->or_like($key, $v);				
			}
			
		}

		 


		if(isset($param['group_by']) && $param['group_by']){
			$order_by = $param['group_by'];
 			$this->db->group_by($order_by);
		}

		if(isset($param['join']) && $param['join']){
			
			$like = $param['join'];
			foreach($like as $key=>$v){
				if(isset($param['join_direction']) && isset($param['join_direction']))
					$this->db->join($key, $v, $param['join_direction']); 
				else 
					$this->db->join($key, $v, 'left'); 
			}
			
		}


		if(isset($param['or_where']) && $param['or_where'] && ((isset($param['where']) && $param['where']) || (isset($param['like']) && $param['like']))) {
			$this->db->or_where($param['or_where']);
 		}


		if(isset($param['where']) && $param['where'])
		{
			$where = $param['where'];	
			if(isset($where['where_in'])){ 
		    
				$field = $where['where_in']['field'];
				$arr = $where['where_in']['arr'];
				
				if($field && count($arr) > 0)
					$this->db->where_in($field,$arr); 
					unset($where['where_in']);
 			}

 			if(isset($param['limit']) && $param['limit']) 
				$query = $this->db->get_where($param['table'], $where, $param['limit'], $param['offset']);
			else
				$query = $this->db->get_where($param['table'], $where);
		}else{
			if(isset($param['limit']) && $param['limit'])
				$query = $this->db->get($param['table'],$param['limit'],$param['offset']);
			else
				$query = $this->db->get($param['table']);
			
		}
			
 		return $query;
		
 	}
	
	
	function get_row($param)
	{
		$table = @$param['table'];
		$where = @$param['where'];
		
		if(isset($param['limit']))
			$this->db->limit($param['limit']);
		
		if(isset($param['join']) && $param['join']){
			$like = $param['join'];
			foreach($like as $key=>$v){
				$this->db->join($key, $v, 'left'); 
			}
		}	
 		if(isset($param['fields'])) {
 			$this->db->select($param['fields']);
 		}

		if($where)
 	   		$record = $this->db->get_where($table,$where);
		else
		    $record = $this->db->get($table);
			
	   return $record->row();
	   
 	}
	
	
	function get_total_rows($param)
	{
		
  		if(isset($param['where']) && $param['where']) {
  			$where = $param['where'];	
			if(isset($where['where_in'])){ 
		    
				$field = $where['where_in']['field'];
				$arr = $where['where_in']['arr'];
				
				if($field && count($arr) > 0)
					$this->db->where_in($field,$arr); 
					unset($where['where_in']);
 			}
 			$this->db->where($where);
  		}
	  		
			
		if(isset($param['like']) && $param['like']){
			$like = $param['like'];
			foreach($like as $key=>$v){
				
				$this->db->like($key, $v); 
				
			}
			
		}

		if(isset($param['join']) && $param['join']){
			
			$like = $param['join'];
			foreach($like as $key=>$v){
				
				$this->db->join($key, $v, 'left'); 
				
			}
			
		}
		$this->db->from($param['table']);
		return $this->db->count_all_results();
 	
	}
	
	function insert($table,$data)
	{
		if(isset($data['date_added']))
		{
			if(is_array($data['date_added'])){
				 
				$dates = $data['date_added'];
				foreach($dates as $v)
					if($v['value']=='NOW()')
						$this->db->set($v['field'],'NOW()',FALSE);
					else
						$this->db->set($v['field'],"'".$v['value']."'",FALSE);
				
			}else{
				
				$this->db->set('date_added','NOW()',FALSE);
			
			}
			
			unset($data['date_added']);
		} 
  		$this->db->insert($table,$this->filter_data_fields($table,$data));		
		return $this->db->insert_id();
		
	}

	function filter_data_fields($table,$data)
	{
		
		$fields =  $this->db->list_fields($table);
		$filtered_data = array();
  		foreach($data as $key=>$v){

			if(in_array($key, $fields))
				$filtered_data[$key] = $v;

		}
		return $filtered_data;
	}
	
	function update($table,$data,$where)
	{
	 
		if(isset($data['date_modified']))
		{
			
			if(is_array($data['date_modified'])){
				 
				$dates = $data['date_modified'];
				foreach($dates as $v)
					if($v['value']=='NOW()')
						$this->db->set($v['field'],'NOW()',FALSE);
					else
						$this->db->set($v['field'],"'".$v['value']."'",FALSE);
			   
 			}else{			

				$this->db->set('date_modified','NOW()',FALSE);
 
			}
			
			unset($data['date_modified']);
						
		} 
		
		$this->db->where($where);
		return $this->db->update($table,$this->filter_data_fields($table,$data));		
		
	}
	
	function delete($table,$where)
	{
		
		return $this->db->delete($table,$where);
			
	}
	
	function get_last_id($table,$id)
	{
		
		$this->db->order_by($id,'desc');
		$res = $this->db->get($table,1,0);
		$row = $res->row();
		if($row)
			return $row->$id;
		else
			return 0;
		
	}
	
	function get_sum($table,$field,$where)
	{
		
		$this->db->select_sum($field);
		if($where)
			$query = $this->db->get_where($table,$where);
 		else
			$query = $this->db->get($table);
		
		$row = $query->row();
		return $row->$field;
	
	}
	
	
	function custom_query($sql)
	{
		return $this->db->query($sql);
	}
	
}