<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Move_fwd_model extends CI_Model {
	
	public function is_complete_all_activity_cms($registrant_id, &$move_forward = 0, &$gallery = 0, $move_forward_id = false) {
		
		// $pledged = $this->db->select('*')
		// 					->from('tbl_move_forward_choice')
		// 					->where('move_forward_choice_status', 1)
		// 					->get()
		// 					->num_rows();
		
		if($move_forward_id)
			$this->db->where('tbl_move_forward_gallery.move_forward_gallery_id', $move_forward_id);
		$move_forward = $this->db->select('*')
								 ->from('tbl_move_forward')
								 ->join('tbl_challenges', 'tbl_challenges.move_forward_id = tbl_move_forward.move_forward_id')
								 ->join('tbl_move_forward_gallery', 'tbl_challenges.challenge_id = tbl_move_forward_gallery.challenge_id')
								 ->where('is_deleted', 0)
								 ->where('status', 1)
								 ->get()
								 ->num_rows();
		
		if($move_forward_id) {
			$this->db->join('tbl_challenges', 'tbl_challenges.challenge_id = tbl_move_forward_gallery.challenge_id')
					 ->join('tbl_move_forward', 'tbl_move_forward.move_forward_id = tbl_challenges.move_forward_id')
					 ->where('tbl_move_forward_gallery.move_forward_gallery_id', $move_forward_id);
		}
			
		$gallery = $this->db->select('*')
							->from('tbl_move_forward_gallery')
							->where('mfg_status', 1)
							->where('registrant_id', $registrant_id)
							->group_by('tbl_move_forward_gallery.challenge_id')
							->get()
							->num_rows();
		return $move_forward == $gallery && $gallery > 0;

	}

	public function is_complete_all_activity($registrant_id, &$move_forward = 0, &$gallery = 0, $move_forward_id = false) {
		
		// $pledged = $this->db->select('*')
		// 					->from('tbl_move_forward_choice')
		// 					->where('move_forward_choice_status', 1)
		// 					->get()
		// 					->num_rows();
		
		if($move_forward_id)
			$this->db->where('tbl_move_forward.move_forward_id', $move_forward_id);
		$move_forward = $this->db->select('*')
								 ->from('tbl_move_forward')
								 ->join('tbl_challenges', 'tbl_challenges.move_forward_id = tbl_move_forward.move_forward_id')
								 ->where('is_deleted', 0)
								 ->where('status', 1)
								 ->get()
								 ->num_rows();
		
		if($move_forward_id) {
			$this->db->join('tbl_challenges', 'tbl_challenges.challenge_id = tbl_move_forward_gallery.challenge_id')
					 ->join('tbl_move_forward', 'tbl_move_forward.move_forward_id = tbl_challenges.move_forward_id')
					 ->where('tbl_move_forward.move_forward_id', $move_forward_id);
		}
			
		$gallery = $this->db->select('*')
							->from('tbl_move_forward_gallery')
							->where('mfg_status', 1)
							->where('registrant_id', $registrant_id)
							->group_by('tbl_move_forward_gallery.challenge_id')
							->get()
							->num_rows();
		return $move_forward == $gallery && $gallery > 0;

	}


}