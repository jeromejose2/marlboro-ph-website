<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function get_users($page = 1, &$records = null) {
		$limit = PER_PAGE;
		$offset = ($page - 1) * $limit;
		$users = array();
		if(!$page) {
			$this->db->order_by('name');
			$users = $this->db->select('*')
							  ->from('tbl_cms_users')
							  ->where('is_deleted', 0)
							  ->get()
							  ->result_array();	
		} else {
			$users = $this->db->select('*')
							  ->from('tbl_cms_users')
							  ->limit($limit, $offset)
							  ->where('is_deleted', 0)
							  ->get()
							  ->result_array();	
		}
		return $users;	
	}
	
	public function save_user($is_update = false) {
		$name = $this->input->post('name');
		$email = $this->input->post('username');
		$password = $this->input->post('password');
		$password = md5($password . $this->config->item('encryption_key'));
		$position = $this->input->post('position');
		$company = $this->input->post('company');
		$id = $this->input->post('id');
		$post['name'] = $name;
		$post['username'] = $email;
		$post['position'] = $position;
		$post['company'] = $company;
		if(!$is_update) {
			$post['password'] = $password;
			$post['last_login'] = date('Y-m-d H:i:s');
			$post['date_password_created'] = date('Y-m-d H:i:s');
			$this->db->insert('tbl_cms_users', $post);
			$id = $this->db->insert_id();
		} else {
			if($this->input->post('password') != '')
				$post['password'] = $password;
			$this->db->where('cms_user_id', $id)
					 ->update('tbl_cms_users', $post);
		}
		$this->save_user_permissions($id, $is_update);	
		return $id;
	}
	
	public function get_user($where = '', $page = 1, $per_page = PER_PAGE, &$records = 0) {
		$user = array();
		$offset = ($page - 1) * $per_page;
		if(is_array($where)) {
			$where['is_deleted'] = 0;
			$this->db->limit($per_page, $offset);
			$user = $this->db->get_where('tbl_cms_users', $where);
			$this->db->get_where('tbl_cms_users', $where);
			$records = $this->db->count_all_results();
		} else {
			$query = "SELECT * FROM tbl_cms_users WHERE is_deleted = 0 " . $where . " LIMIT $offset, $per_page";
			$user = $this->db->query($query);
			$query = "SELECT COUNT(*) AS num_rows FROM tbl_cms_users WHERE is_deleted = 0 " . $where;
			$records =  $this->db->query($query)->row_array()['num_rows'];
		}
		return $user->result_array();
	}
	
	private function save_user_permissions($id, $is_update = false) {
		$modules = $this->db->get('tbl_modules')->result_array();
		if($modules) {
			foreach($modules as $k => $v) {
				$post['view'] = @in_array($v['module_id'], $this->input->post('view')) ? 1 : 0;
				$post['add'] = @in_array($v['module_id'], $this->input->post('add')) ? 1 : 0;
				$post['edit'] = @in_array($v['module_id'], $this->input->post('edit')) ? 1 : 0;
				$post['delete'] = @in_array($v['module_id'], $this->input->post('delete')) ? 1 : 0;
				$post['cms_user_id'] = $id;
				$post['module_id'] = $v['module_id'];
				if(!$is_update)
					$this->db->insert('tbl_permissions', $post);
				else {
					$where['module_id'] = $v['module_id'];
					$where['cms_user_id'] = $id;
					$valid = $this->db
							->select()
							->from('tbl_permissions')
							->where($where)
							->get()
							->row();
					if($valid) {
						$this->db->where($where);
						$this->db->update('tbl_permissions', $post);
					} else {
						$this->db->insert('tbl_permissions', $post);
					}
				}
			}
		}
	}
	
	public function get_user_permissions($id) {
		$permissions = $this->db->get_where('vw_module_permissions', array('cms_user_id'	=> $id, 'group !=' => 0))->result_array();
		$perms_arr = array();
		if($permissions) {
			foreach($permissions as $k => $v) {
				if($v['view'] == 1)
					$perms_arr['view'][] = $v['module_id'];
				if($v['add'] == 1)
					$perms_arr['add'][] = $v['module_id'];
				if($v['edit'] == 1)
					$perms_arr['edit'][] = $v['module_id'];
				if($v['delete'] == 1)
					$perms_arr['delete'][] = $v['module_id'];
				
			}
		}
		return $perms_arr;
	}
	
	
	
	
}