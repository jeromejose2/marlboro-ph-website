<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrant_Model extends CI_Model {
	
	private $_table;
	public function __construct() {
		parent::__construct();
		$this->_table = 'tbl_registrants';
	}
	
	public function save_statement($is_update = false) {
		$statement = $this->input->post('statement');
		$id = $this->input->post('id');
		$post['statement'] = $statement;
		if(!$is_update) {
			$this->db->insert($this->_table, $post);
			$id = $this->db->insert_id();
		} else {
			$this->db->where('statement_id', $id)
					 ->update($this->_table, $post);
		}
		return $id;	
	}
	
	public function get_registrant($where = '', $page = 1, $per_page = PER_PAGE, &$records = 0, $all = false) {
		$user = array();
		$offset = ($page - 1) * $per_page;
		if(is_array($where)) {
			if(!$all)
				$this->db->limit($per_page, $offset);
			$user = $this->db->get_where($this->_table, $where);
			$this->db->get_where($this->_table, $where);
			$records = $this->db->count_all_results();
		} else {
			$limit = $all ? "" : "LIMIT $offset, $per_page";
			$query = "SELECT * FROM $this->_table WHERE 1 " . $where . " ORDER BY date_created DESC " . $limit;
			$user = $this->db->query($query);
			$query = "SELECT COUNT(*) AS num_rows FROM $this->_table WHERE 1 " . $where;
			$records =  $this->db->query($query)->row_array()['num_rows'];
		}
		return $user->result_array();
	}
	
	public function get_all_module() {
		$modules = $this->db->select('*')
							->order_by('group')
							->from('tbl_modules')
							->where('is_deleted', 0)
							->get();
		return $modules->result_array();
	}
	
	public function get_registrant_stats() {
		$records = array();
		$query = "SELECT COUNT(*) AS num_rows FROM tbl_registrants";
		$records['all'] =  $this->db->query($query)->row_array()['num_rows'];
		
		//$status = $this->get_status();
		// foreach($status as $k => $stat) {
		// 	$query = "SELECT COUNT(*) AS num_rows FROM tbl_registrants WHERE status = $k";
		// 	$records[$k] =  $this->db->query($query)->row_array()['num_rows'];		
		// }
		$records_temp = $this->db->select('COUNT(*) AS count, status')
								->from('tbl_registrants')
								->group_by('status')
								->get()
								->result_array();
		if($records_temp) {
			foreach($records_temp as $k => $v) {
				$records[$v['status']] = $v['count'];
			}
			unset($records[ARCLIGHT_IN_PROCESS]);
		}
		return $records;
	}

	public function get_arclight_in_process() {
		$records = array();
		$tmp = $this->db->select('COUNT(registrant_id) as total, status')
			->from('tbl_registrants')
			->where('arclight_in_process', 1)
			->where('status !=', ACCESS_GRANTED_STATUS)
			->group_by('status')
			->get()
			->result_array();
		foreach ($tmp as $t) {
			$records[$t['status']] = $t['total'];
		}
		return $records;
	}

	public function get_profile_stats() {
		$query = "SELECT COUNT(*) AS count FROM tbl_registrants WHERE ((new_picture = '' AND picture_status = 1) OR (new_picture <> '' AND picture_status <> 1))";
		$res = $this->db->query($query)->row();
		return $res->count;

	}

	public function get_brands() {
		$param['table'] = 'tbl_brands';
		$brands = $this->global_model->get_rows($param)->result_array();
		$brands_arr = array(); 
		if($brands) {
			foreach ($brands as $key => $value) {
				$brands_arr[$value['brand_id']] = $value; 
			}
		}
		return $brands_arr;
	}

	public function get_alernate_purchases() {
		$param['table'] = 'tbl_alternate_purchase';
		$brands = $this->global_model->get_rows($param)->result_array();
		$brands_arr = array(); 
		if($brands) {
			foreach ($brands as $key => $value) {
				$brands_arr[$value['alternate_id']] = $value; 
			}
		}
		return $brands_arr;
	}

	public function get_giid_types() {
		$param['table'] = 'tbl_giid_types';
		$giid_temp = $this->global_model->get_rows($param)->result_array();
		$giid = array();
		
		if($giid_temp) {
			foreach ($giid_temp as $key => $value) {
				$giid[$value['giid_type_id']] = $value['giid_type'];
			}
		}
		return $giid;
	}
	
	public function get_status() {
		$status[VERIFIED] = 'Verified';
		$status[CSR_APPROVED] = 'CSR Approved';
		$status[PENDING_GIID] = 'Pending GIID Upload';
		$status[PENDING_CSR] = 'Pending CSR Approval';
		$status[REJECTED_NO_GIID] = 'Rejected - No GIID Upload';
		$status[REJECTED_GIID] = 'Rejected - GIID Invalid';
		$status[DEACTIVATED] = 'Deactivated';
		$status[ARCLIGHT_IN_PROCESS] = 'Arclight In Process';
		return $status;
	}

	// public function update_giid_birthday() {
	// 	$query = "SELECT * FROM tbl_registrants
	// 			  WHERE government_id_number = ''
	// 			  OR government_id_number IS NULL
	// 			  OR date_of_birth = '0000-00-00'
	// 			  OR date_of_birth IS NULL";
	// 	$users = $this->db->query($query)->result_array();
	// 	if($users) {
	// 		foreach ($users as $key => $value) {
	// 			$arclight_data = $this->db->select('*')
	// 									  ->from('tbl_arclight_delta')
	// 									  ->where('EmailAdress', $value['email_address'])
	// 									  ->order_by('IndividualID', 'DESC')
	// 									  ->get()
	// 									  ->row_array();

	// 			$giid_type = $this->db->select('*')
	// 								  ->from('tbl_giid')
	// 								  ->join('tbl_giid_types', 'tbl_giid.type_code = tbl_giid_types.giid_type_code')
	// 								  ->where('tbl_giid.type_id_num', $arclight_data['GovernmentIDType'])
	// 								  ->get()
	// 								  ->row_array();

	// 			$query = "UPDATE tbl_registrants SET ";
	// 			if(!$value['government_id_number'])
	// 				$query .= " government_id_number = '" . $arclight_data['GovernmentIDNumber'] "', government_id_type = '" . $giid_type['type_id_num'] . "'";
	// 			if($value['date_of_birth'] == '0000-00-00')
	// 				$query .= " government_id_number = '" . $arclight_data['DateOfBirth'] "',";
	// 			$query .= " WHERE registrant_id = " . $value['registrant_id'];
	// 			die($query);
	// 		}
	// 	}
	// }

}