<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings_Model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function get_settings($type = 'move_fwd_mechanics') {
		$settings = $this->db->select('*')
							 ->from('tbl_settings')
							 ->where('type', $type)
							 ->get()
							 ->result_array();
		return $settings[0];
	}
	
	public function save_settings($type = 'move_fwd_mechanics', $description = false) {
		$post['description'] = !$description ? $this->input->post('description') : $description;
		$this->db->where('type', $type);
		$this->db->update('tbl_settings', $post);
	}

}