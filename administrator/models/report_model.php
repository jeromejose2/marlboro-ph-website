<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_Model extends CI_Model {
	 
 	public function __construct()
	{
		parent::__construct(); 
	}
	
	public function get_page_views($section)  {
		$where['section'] = $section;
		if($this->input->get('fromdate') && $this->input->get('todate')) {
			$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
			$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
			$where['DATE(date_visited) <='] = $to;
			$where['DATE(date_visited) >='] = $from;
		}

		$views = $this->db->select('DATE(date_visited), COUNT(*)')
						  ->from('tbl_page_visists')
						  ->where($where)
						  ->group_by('DATE(date_visited)')
						  ->limit(31, 0)
						  ->order_by('date_visited') 
						  ->get();
	}

	public function get_login_spread() {
		$query = "SELECT AVG(a.visits), a.daystring FROM 
					  (SELECT COUNT(*) as visits, DAYNAME(l.date_login) AS daystring 
					   FROM tbl_login l
					   GROUP BY daystring) a";
		$average_visits = $this->db->query($query)->result_array();

		$query = "SELECT AVG(a.visits), a.daystring FROM 
					  (SELECT COUNT(*) as visits, DAYNAME(l.date_login) AS daystring 
					   FROM tbl_login l
					   GROUP BY daystring, registrant_id) a";
		$average_visitor = $this->db->query($query)->result_array();
		$averages = array();
		if($average_visits) {
			foreach($average_visits as $k => $v) {
				foreach($average_visitor as $vk => $vv) {
					if($v['daystring'] == $vv['daystring']) {
						$v['visitor'] = $vv['visits'];
						$averages[] = $v;
					}
				}
			}
		}
		return $averages;

	}

	public function get_page_demographics($section, &$from = false, &$to = false, $field = 'date_visited', $sort = 'asc') { 
		$like['section'] = $section;
		$where = array();
		if($this->input->get('fromdate') && $this->input->get('todate')) {
			$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
			$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
			$where['DATE(date_visited) <='] = $to;
			$where['DATE(date_visited) >='] = $from;
		}
		$field = $this->input->get('field') ? $this->input->get('field') : $field;
		$sort = $this->input->get('sort') ? $this->input->get('sort') : $field;
		$records = $this->db->select('DATE(date_visited) AS dv, COUNT(*) AS count, (SELECT COUNT(DISTINCT(registrant_id)) FROM tbl_page_visits LEFT JOIN tbl_login ON tbl_login.login_id = tbl_page_visits.login_id WHERE DATE(date_visited) = dv AND section LIKE \'%' . $section . '%\') AS visitors')
							->from('tbl_page_visits')
							->where($where)
							->order_by($field, $sort)
							->limit(31, 0)
							->like($like)
							->group_by('dv')
							->get(); 
		return $records;
	}

	public function get_move_fwd_stats() {
		$records = array();
		$query = "SELECT COUNT(*) AS num_rows FROM tbl_move_forward_gallery";
		$records['all'] =  $this->db->query($query)->row_array()['num_rows'];
		
		$records_temp = $this->db->select('COUNT(*) AS count, mfg_status')
								->from('tbl_move_forward_gallery')
								->group_by('mfg_status')
								->get()
								->result_array();
		if($records_temp) {
			foreach($records_temp as $k => $v) {
				$records[$v['mfg_status']] = $v['count'];
			}
		}
		return $records;
	}

	public function get_median() {
		$this->db->query("SET @rownum := 0;");
		$query = "SELECT
				   AVG(t.mark) AS med_points
				FROM
				(
				   SELECT
				      @rownum := @rownum + 1 AS rownum,
				      tbl_registrants.total_points AS mark
				   FROM
				      tbl_registrants
				   ORDER BY tbl_registrants.total_points
				) AS t
				WHERE
				   t.rownum IN (
				      CEIL(@rownum/2),
				      FLOOR(@rownum/2)
				   )
				";
		$res = $this->db->query($query);
		return $res->result_array();
	}

	public function get_user_activities() {
		echo '<pre>';
		$games = $this->get_games();
		$to_report = array(COMMENT, COMMENT_REPLY, MOVE_FWD, WEBGAMES);
		$users = $this->db->select('registrant_id, first_name, third_name')
						  ->from('tbl_registrants')
						  ->get()
						  ->result_array();
		foreach ($to_report as $key => $value) {
			switch($value) {
				case COMMENT:
					$comments = $this->db->select('registrant_id, COUNT(comment_id) AS count')
										 ->from('tbl_comments')
										 ->group_by('registrant_id')
										 ->get()
										 ->result_array();
					$comments = $this->format_arr($comments, COMMENT);
					break;
				case COMMENT_REPLY:
					$replies = $this->db->select('registrant_id, COUNT(comment_reply_id) AS count')
											 ->from('tbl_comment_replies')
											 ->group_by('registrant_id')
											 ->get()
											 ->result_array();
					$replies = $this->format_arr($comments, COMMENT_REPLY);
					break;
				case MOVE_FWD:
					$move_fwd = $this->db->select('registrant_id, COUNT(move_forward_gallery_id) AS count')
											 ->from('tbl_move_forward_gallery')
											 ->group_by('registrant_id')
											 ->get()
											 ->result_array();
					$move_fwd = $this->format_arr($comments, MOVE_FWD);
					break;
				case WEBGAMES:
					$temp = $this->db->select('registrant_id, COUNT(game_id) AS count, game_id')
											 ->from('tbl_game_scores')
											 ->group_by('registrant_id')
											 ->group_by('game_id')
											 ->get()
											 ->result_array();
					if($temp) {
						foreach ($temp as $key => $value) {
							$games_arr[$value['registrant_id']][$games[$value['game_id']]] = $value['count'];
						}
					}
					break;
				case BACKSTAGE_PHOTOS:
					$backstage = $this->db->select('uploader_id, COUNT(photo_id) AS count')
											 ->from('tbl_backstage_events_photos')
											 ->where('user_type !=', 'administrator')
											 ->group_by('uploader_id')
											 ->get()
											 ->result_array();
					$backstage = $this->format_arr($backstage, BACKSTAGE_PHOTOS, 'uploader_id');
					break;
				case ABOUT_VIDEOS:
					$about_videos = $this->db->select('uploader_id, COUNT(about_video_id) AS count')
											 ->from('tbl_about_videos')
											 ->where('user_type !=', 'administrator')
											 ->group_by('about_video_id')
											 ->get()
											 ->result_array();
					$about_videos = $this->format_arr($about_videos, ABOUT_VIDEOS, 'uploader_id');
					break;
			}
		}
		$users_arr = array();
		if($users) {
			foreach ($users as $key => $value) {
				$value['comments'] = isset($comments[$value['registrant_id']][COMMENT]) ?  $comments[$value['registrant_id']][COMMENT] :0;
				$value['replies'] = isset($replies[$value['registrant_id']][COMMENT_REPLY]) ?  $replies[$value['registrant_id']][COMMENT_REPLY] :0;
				$value['move_fwd'] = isset($move_fwd[$value['registrant_id']][MOVE_FWD]) ?  $move_fwd[$value['registrant_id']][MOVE_FWD] :0;
				foreach ($games as $gkey => $gvalue) {
					$value[$gvalue] = isset($games_arr[$value['registrant_id']][$gvalue]) ?  $games_arr[$value['registrant_id']][$gvalue] :0;	
				}
				$value['backstage'] = isset($backstage[$value['registrant_id']][BACKSTAGE_PHOTOS]) ?  $backstage[$value['registrant_id']][BACKSTAGE_PHOTOS] :0;
				$value['about_videos'] = isset($move_fwd[$value['registrant_id']][ABOUT_VIDEOS]) ?  $move_fwd[$value['registrant_id']][ABOUT_VIDEOS] :0;
				$users_arr[] = $value;		
			}
		}
		
		print_r($users_arr);
		die();
	}

	private function format_arr($array, $index, $id_field = 'registrant_id') {
		$new_arr = array();
		if($array) {
			foreach ($array as $key => $value) {
				if(isset($value[$id_field]) && isset($value['count']))
					$new_arr[$value[$id_field]][$index] = $value['count'];
			}
		}
		return $new_arr;
	}

	private function get_games() {
		$games = $this->db->select('*')
						->from('tbl_games')
						->get()
						->result_array();
		$games_arr = array();
		if($games) {
			foreach ($games as $key => $value) {
				$games_arr[$value['game_id']] = strtolower($value['name']);
			}
		}
		return $games_arr;
	}
}