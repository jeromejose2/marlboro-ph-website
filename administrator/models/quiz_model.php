<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quiz_Model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function get_user_quiz($page = 1, $all = false) {
		$user = array();
		$offset = ($page - 1) * PER_PAGE * 6;
		$where =  '';
		if(isset($_GET['search'])) {
			$where .= isset($_GET['name']) ? "WHERE CONCAT(first_name, ' ', third_name) LIKE '%$_GET[name]%'" : "";
			$where .= isset($_GET['result']) ? " AND result LIKE '%$_GET[result]%'" : "";
			$where .= isset($_GET['from_buy']) && $_GET['from_buy'] ? " AND DATE(date_created) >= '%$_GET[from_buy]%'" : "";
			$where .= isset($_GET['to_buy']) && $_GET['to_buy'] ? " AND DATE(date_created) <= '%$_GET[to_buy]%'" : "";
		}
		$limit = $all ? '' : " LIMIT $offset, " . PER_PAGE * 6;
		$query = "SELECT q.*, r.first_name, r.third_name, vq.question, vq.choice
				 FROM tbl_premium_user_quiz q
				 LEFT JOIN tbl_registrants r ON r.registrant_id = q.registrant_id
				 LEFT JOIN tbl_premium_user_answers a ON q.user_quiz_id = a.user_quiz_id
				 LEFT JOIN vw_questions vq ON vq.choice_id = a.choice_id
				 $where
				 ORDER BY q.date_created DESC
				 $limit";
		$buys = $this->db->query($query)->result_array();
		return $buys;
	}

	public function get_user_quiz_count() {
		$user = array();
		$where =  '';
		if(isset($_GET['search'])) {
			$where .= isset($_GET['name']) ? "WHERE CONCAT(first_name, ' ', third_name) LIKE '%$_GET[name]%'" : "";
			$where .= isset($_GET['result']) ? " AND result LIKE '%$_GET[result]%'" : "";
			$where .= isset($_GET['from_buy']) && $_GET['from_buy'] ? " AND DATE(date_created) >= '%$_GET[from_buy]%'" : "";
			$where .= isset($_GET['to_buy']) && $_GET['to_buy'] ? " AND DATE(date_created) <= '%$_GET[to_buy]%'" : "";
		}
		$query = "SELECT q.*, r.first_name, r.third_name, vq.question, vq.choice
				 FROM tbl_premium_user_quiz q
				 LEFT JOIN tbl_registrants r ON r.registrant_id = q.registrant_id
				 LEFT JOIN tbl_premium_user_answers a ON q.user_quiz_id = a.user_quiz_id
				 LEFT JOIN vw_questions vq ON vq.choice_id = a.choice_id
				 $where
				 GROUP BY q.user_quiz_id";
		$buys = $this->db->query($query)->num_rows();
		return $buys;
	}
}