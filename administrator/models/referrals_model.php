<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Referrals_Model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function get_referrals($page = 1, $all = false) {
		$offset = ($page - 1) * PER_PAGE * 6;
		$where =  '';
		if(isset($_GET['search'])) {
			 $where .= isset($_GET['name']) ? "WHERE CONCAT(b.first_name, ' ', b.third_name) LIKE '%$_GET[name]%'" : "";
		}
		$limit = $all ? '' : " LIMIT $offset, " . PER_PAGE * 6;
		
		$query = "SELECT a.*, count(a.referer) as referrals,
				 (SELECT count(c.referer) from tbl_referrals c where c.referer=b.registrant_id AND status=1) as verified,
				 b.first_name, b.third_name
				 FROM tbl_referrals a
				 LEFT JOIN tbl_registrants b ON a.referer = b.registrant_id
				 $where
				 GROUP BY b.registrant_id
				 ORDER BY a.timestamp DESC
				 $limit";

		return $this->db->query($query)->result_array();
	}

	public function get_user_referral_count() {
		$where =  '';

		$where =  '';
		if(isset($_GET['search'])) {
			 $where .= isset($_GET['name']) ? "WHERE CONCAT(b.first_name, ' ', b.third_name) LIKE '%$_GET[name]%'" : "";
		}
		
		$query = "SELECT count(a.referer) as referals,  b.first_name
				 FROM tbl_referrals a
				 LEFT JOIN tbl_registrants b ON a.referer = b.registrant_id
				 $where GROUP BY b.registrant_id";

		return $this->db->query($query)->num_rows();
	}

	public function get_referrals_list($referer, $status) {
		
		$status = $status ? 'AND status=1' : '';
		
		$query = "SELECT * FROM tbl_referrals a WHERE referer=$referer $status ORDER BY timestamp DESC";

		return $this->db->query($query)->result_array();
	}

	public function get_referral_export() {
		$where =  '';

		if(isset($_GET['search'])) {
			 $where .= isset($_GET['name']) ? "WHERE CONCAT(b.first_name, ' ', b.third_name) LIKE '%$_GET[name]%'" : "";
		}
		
		$query = "SELECT a.*, count(a.referer) as referrals,
				 (SELECT count(c.referer) from tbl_referrals c where c.referer=b.registrant_id AND status=1) as verified,
				 b.first_name, b.third_name
				 FROM tbl_referrals a
				 LEFT JOIN tbl_registrants b ON a.referer = b.registrant_id
				 $where
				 GROUP BY b.registrant_id
				 ORDER BY a.timestamp DESC";

		return $this->db->query($query)->result_array();
	}
}