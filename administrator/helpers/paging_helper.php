<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function paging($total_rows,$limit,$offset=0,$base_url)
{
	
	$CI =& get_instance();
	$CI->load->library('pagination');
 	$config['base_url'] = $base_url;
	$config['per_page'] = $limit;
	$config['total_rows'] =  $total_rows;
	$config['num_links'] = 8;
	$config['enable_query_strings'] = TRUE;
	$config['page_query_string'] = TRUE;
	$config['prev_link'] = 'Prev';
	$config['prev_tag_open'] ='<li>';
	$config['prev_tag_close'] = '</li>';
	$config['next_link'] = 'Next';
	$config['next_tag_open'] = '<li>';
	$config['next_tag_close'] = '</li>';
	$config['num_tag_open'] = '<li>';
	$config['num_tag_close'] = '</li>';
	$config['cur_tag_open'] = '<li class="active"><a href="">';
	$config['cur_tag_close'] = '</a></li>';
	$config['last_tag_open'] = '<li>';
	$config['last_tag_close'] = '</li>';
	$config['first_tag_open'] = '<li>';
	$config['first_tag_close'] = '</li>';
	$CI->pagination->initialize($config);
	$pagination = $CI->pagination->create_links();
	$select25 = $CI->input->get('limit') && $CI->input->get('limit') == '25' ?  "<option selected value='25'>25</option>" : "<option value='25'>25</option>";
	$select50 = $CI->input->get('limit') && $CI->input->get('limit') == '50' ?  "<option selected value='50'>50</option>" : "<option value='50'>50</option>";
	$select100 = $CI->input->get('limit') && $CI->input->get('limit') == '100' ?  "<option selected value='100'>100</option>" : "<option value='100'>100</option>";
	$select500 = $CI->input->get('limit') && $CI->input->get('limit') == '500' ?  "<option selected value='500'>500</option>" : "<option value='500'>500</option>";
	$selectAll = $CI->input->get('limit') && $CI->input->get('limit') == $total_rows ?  "<option selected value='".$total_rows."'>All</option>" : "<option  value='".$total_rows."' >All</option>";
	return '<div>
              Show: 
              <select>
              	<option>10</option>
                '. $select25 . '
                '. $select50 . '
                '. $select100 . '
                '. $select500 . '
                '. $selectAll . '
              </select>
           </div>' . $pagination;	 	
}