<?php

class Spice{
    
    const APP_ID = 1054;
    const API_URL = 'https://qa.mrm-pmi.com/Services/PMI.MRM.Services.RESTService.svc/';
    const MARKET_CODE = 'PH';
    const USER_KEY = '_spice_user';
    const SESSION_KEY = '_spice_session_key';

    const MEMBER_PERSON_ID = '_spice_member_person_id';
    const MEMBER_SESSION_KEY = '_spice_member_session_key';

    const ADMIN_PERSON_ID = '_spice_admin_person_id';
    const ADMIN_SESSION_KEY = '_spice_admin_session_key';

    private $CI;

    public function __construct(){

        $this->CI = &get_instance();

    }

    public function loginAdmin($username, $password){
        
       if(!$this->checkSession($this->CI->session->userdata(self::ADMIN_SESSION_KEY))){

            $response = $this->createSession($username,$password);
            $response = $this->parseJSON($response);          

            if ($response->ResponseHeader->TransactionStatus) {
                return false;
            }

            $this->CI->session->set_userdata(self::ADMIN_SESSION_KEY,$response->SessionKey);
            $this->CI->session->set_userdata(self::ADMIN_PERSON_ID,$response->PersonId);
            
       }

 
    }

    public function loginMember($username, $password){
        
       
       //if(!$this->checkSession($this->CI->session->userdata(self::MEMBER_SESSION_KEY))){

            $response = $this->createSession($username,$password);
            $response = $this->parseJSON($response);

            if ($response->ResponseHeader->TransactionStatus) {
                return false;
            }

            $this->CI->session->set_userdata(self::MEMBER_SESSION_KEY,$response->SessionKey);
            $this->CI->session->set_userdata(self::MEMBER_PERSON_ID,$response->PersonId);
            
            return $response;


       // }        
  
    }

    public function logOut($sessionKey,$personId){

        $response = $this->endSession($sessionKey);        

        $this->CI->session->unset_userdata($sessionKey,'');
        $this->CI->session->unset_userdata($personId,'');

    }

    private function setError($code, $message){
        echo  "<br/>TransactionStatus: {$code}.\nTransactionStatusMessage: {$message}\n<br/>";
    }

    public function endSession($sessionKey){

        $data = [
            'ApplicationId' => self::APP_ID,
            'DeviceId' => '',
            'LanguageCode' => '',
            'MarketCode' => self::MARKET_CODE,
            'SessionKey' => $sessionKey,
            'SubMarketCode' => ''
        ];
        return $this->api('EndSession', $data);

    }    

    public function createSession($loginName, $password)
    {

        // Requried Inputs: MessageRequestHeader, LoginName, EncryptedPassword, ApplicationId, MarketCode

        $data = array(
                        'EncryptedPassword' => $this->encryptPassword($loginName, $password),
                        'LoginName' => $loginName,
                        'MessageRequestHeader' => $this->createHeader()
                    );
        return $this->api('CreateSession', $data);
    }

    public function createSessionNoEncryption($loginName, $password)
    {

        // Requried Inputs: MessageRequestHeader, LoginName, EncryptedPassword, ApplicationId, MarketCode

        $data = array(
                        'EncryptedPassword' => $password,
                        'LoginName' => $loginName,
                        'MessageRequestHeader' => $this->createHeader()
                    );
        return $this->api('CreateSession', $data);
    }



    public function createHeader($sessionKey = ''){

        return array(
                    'ApplicationId' => self::APP_ID,
                    'DeviceId' => '',
                    'LanguageCode' => '',
                    'MarketCode' => self::MARKET_CODE,
                    'SessionKey' => $sessionKey
                    );

    }

    public function getMemberPersonId()
    {
        return $this->CI->session->userdata(self::MEMBER_PERSON_ID);
    }

    public function trackAction($param)
    {
        
        $data = array_merge(array('PersonId' => $this->getMemberPersonId(),'MessageRequestHeader' => $this->createHeader($this->CI->session->userdata(self::ADMIN_SESSION_KEY))),
                            $param);

        return $this->api('TrackAction', $data);
    }

    public function trackActionAdmin($param)
    {
        $this->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);

        $data = array_merge(array('MessageRequestHeader' => $this->createHeader($this->CI->session->userdata(self::ADMIN_SESSION_KEY))),
                            $param);
        return $this->api('TrackAction', $data);
    }
    
    public function checkAction($filters)
    {
        $data = array_merge(
            array('MessageRequestHeader' => $this->createHeader($this->CI->session->userdata(self::ADMIN_SESSION_KEY))),
            $filters
        );

        return $this->api('CheckAction', $data);
    }

    public function getBulk($table){

        $data = array(
                        'MessageRequestHeader' => $this->createHeader($this->CI->session->userdata(self::ADMIN_SESSION_KEY)),
                        'ReturnType'=>'xml',
                         'Table' => $table
                    );

         return $this->api('GetBulk', $data);

    }

    public function getBrand($filters = array())
    {

        $data = array_merge(
            ['MessageRequestHeader' => $this->createHeader($this->CI->session->userdata(self::ADMIN_SESSION_KEY))],
            $filters
        );
        $response = $this->api('GetBrand', $data);
        $response_json = $this->parseJSON($response);
       
        if(!$response_json->ResponseHeader->TransactionStatus){

            $brand = $response_json->Brand;
            $brands = $this->parseJSON($this->toJSONFormat($brand));
            $data = array();
            foreach($brands as $v){
                $data[] = array('brand_id'=>$v->BrandId,'brand_name'=>$v->BrandName);
            }


            return $data;

        }else{

            return false;

        }

    }

    public function setSessionKey($token){
        $this->_token = $token;
        return $this;
    }

    public function encryptPassword($loginName, $password){

        if (strlen($loginName) < 30) {
            $loginName = str_pad($loginName, 30, ' ', STR_PAD_RIGHT);
        }
        $loginName = substr($loginName, 0, 30);
        return base64_encode(sha1($password.$loginName, true).$loginName);           

    }

    private function api($method, array $data){


        $curl = curl_init(self::API_URL.trim($method, '/').'/');
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_CAINFO, __DIR__.'/spice/AddTrustExternalCARoot.crt');
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-type: application/json',
            'Accept: application/json'
        ]);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        $response = curl_exec($curl);
        
        if (!$response) {
            return null;
        }

        if (curl_errno($curl)) {
            throw new Exception('CURL Error: '.curl_error($curl));
        }
        curl_close($curl);
        return $response;
    }

    private function api2($method, array $data){


        $curl = curl_init(self::API_URL.trim($method, '/').'/');
       $data = stripslashes(json_encode($data));
       echo $data;
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_CAINFO, __DIR__.'/spice/AddTrustExternalCARoot.crt');
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-type: application/json',
            'Accept: application/json'
        ]);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        $response = curl_exec($curl);
        
        if (!$response) {
            return 'empty';
        }

        if (curl_errno($curl)) {
            throw new Exception('CURL Error: '.curl_error($curl));
        }
        curl_close($curl);
        return $response;
    }



    public function ManagePerson($data){

        $data = array_merge(array('MessageRequestHeader' => $this->createHeader($this->CI->session->userdata(self::ADMIN_SESSION_KEY))),
                           $data);

        $data = $this->api('ManagePerson', $data);
        return $data;
    } 

    public function GetPerson($filter){

        $data = array_merge(array('MessageRequestHeader'=>$this->createHeader($this->CI->session->userdata(self::ADMIN_SESSION_KEY)),'ProfileType'=>'C'),
                            $filter);


        $data = $this->api('GetPerson',$data);
        return $data;

    }

    public function SearchPerson($filter){

        $data = array_merge(array('MessageRequestHeader'=>$this->createHeader($this->CI->session->userdata(self::ADMIN_SESSION_KEY)),'ProfileType'=>'C'),
                            $filter);
                            
        $data = $this->api('SearchPerson',$data);
        return $data;

    }

    public function AgeVerificationRequest($data){

        $data = array_merge(array('MessageRequestHeader'=>$this->createHeader($this->CI->session->userdata(self::ADMIN_SESSION_KEY))),$data);
        $data = $this->api('AgeVerificationRequest',$data);
        return $data;

    }

    public function AgeVerificationRequest2($data){

        $data = array_merge(array('MessageRequestHeader'=>$this->createHeader($this->CI->session->userdata(self::ADMIN_SESSION_KEY))),$data);
        $data = $this->api2('AgeVerificationRequest',$data);
        return $data;

    }

    public function checkSession($sessionKey){

        $data = [
            'MessageRequestHeader' => $this->createHeader($sessionKey),
            'Source' => ''
        ];

        $response = $this->api('CheckSession', $data);
        $response = $this->parseJSON($response);

        return (int)$response->ResponseHeader->TransactionStatus === 0 ? true : false;
    }

    public function toArray($data){
        
        $response = json_decode($data,true);
        return $response;
    }

    public function getGIIDTypes($IdDocumentType){
        
        $json = $this->parseJSON($this->getBulk('ReferenceData'));
        $xml = $this->parseXML($json->MessageBody);
        $response = array();
 
        foreach($xml as $v){

            if(strtolower($v->GroupCode) == strtolower($IdDocumentType)){
               $response[] = array('ClassName'=>(string)$v->ClassName,'ClassCode'=>(string)$v->ClassCode);
            }

        }

        return $response;       


    }

    public function parseJSON($data){
        
        $response = json_decode($data);
        return $response;

    }

    public function toJSONFormat($data){
        
        $response = json_encode($data);
        return $response;

    }

    public function printArray($data){
        echo "<br/><br/><pre>";
        print_r($data);
        echo "</pre><br/><br/>";
    }

    public function fromXMLtoObject($data){
        $data = new SimpleXMLElement($data);
        $data = json_decode(json_encode((array)$data));
        return $data;

    }

    public function parseXML($data){
        return new SimpleXMLElement($data);
    }   

    public function createLoginToken($PersonID, $dlTemplateId, $cellId, $extParams = null)
    {
        $data = [
                'MessageRequestHeader' => $this->createHeader($this->CI->session->userdata(self::ADMIN_SESSION_KEY)),
                'DLTemplateId' => $dlTemplateId,
                'PersonID' => $PersonID,
                'CellID' => $cellId
            ];

        if ($extParams) {
            $data['ExtensionParameter'] = $extParams;
        }
        return $this->api('CreateLoginToken', $data);
    }

   


    public function sendMailing($params) 
    {

        $data = array_merge( array('MessageRequestHeader' => $this->createHeader($this->CI->session->userdata(self::ADMIN_SESSION_KEY))),$params);        

        return $this->api('SendMailing', $data);

    }
 
    public function validateLoginLoken($token)
    {
        $data = array(
            'MessageRequestHeader' => $this->createHeader($this->CI->session->userdata(self::ADMIN_SESSION_KEY)),
            'LoginToken' => $token
        );

        return $this->api('ValidateLoginToken', $data);
    }

    public function ChangePassword($params)
    {
        if (isset($params['NewPassword'], $params['LoginName'])) {
            $params['EncryptedChangedPassword'] = $this->encryptPassword($params['LoginName'], $params['NewPassword']);
        }
        if (isset($params['CurrentPassword'], $params['LoginName'])) {
            $params['EncryptedPassword'] = $this->encryptPassword($params['LoginName'], $params['CurrentPassword']);
        }
        
        unset($params['NewPassword'], $params['CurrentPassword']);

        $data = array_merge(array('MessageRequestHeader'=>$this->createHeader($this->CI->session->userdata(self::MEMBER_SESSION_KEY))),$params);
        return $this->api('ChangePassword', $data);
    }

    public function resetPassword($params)
    {
        if (isset($params['NewPassword'])) {
            $params['EncryptedChangedPassword'] = $this->encryptPassword($params['EmailAddress'], $params['NewPassword']);
            $params['LoginName'] = NULL;
            $params['EncryptedPassword'] = NULL;
        }
        unset($params['CurrentPassword'], $params['NewPassword'], $params['EmailAddress']);

        $data = array_merge(
            ['MessageRequestHeader' => $this->createHeader($params['SessionKey'])],
            $params
        );

        return $this->api('ChangePassword', $data);
    }

    public function autoLogin($token) 
    {
       $data = [
           'MessageRequestHeader' => $this->createHeader(),
           'LoginToken' => $token
       ];    

       return $this->api('AutoLogin', $data);
   }
}