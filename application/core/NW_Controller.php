<?php

class NW_Controller extends CI_Controller
{
	public function _remap()
	{
		$method = strtolower($this->input->server('REQUEST_METHOD'));
		if ($this->input->is_ajax_request() && !$this->uri->segment(3)
			&& method_exists($this, $method)
			&& $this->session->userdata('user_id'))
		{
			$this->output->set_content_type('application/json');
			$this->{$method}();
			return;
		}
		show_404();
	}
}