<?php

if (!defined('BASE_URL'))
	define('BASE_URL', base_url());

function send_email($data)
{
	$ci = & get_instance();
	$ci->load->library('email');
	$config['mailtype'] = 'html';
	$ci->email->initialize($config);	
	$ci->email->from($data['email'], $data['name']);
	$ci->email->to($data['to']);
	$ci->email->subject($data['subject']);
	$ci->email->message($data['content']);

	if($ci->email->send())
		return true;
	else 
		print_r($ci->email->print_debugger());
}

function stristr_reverse($haystack, $needle)
{ 
	$pos = stripos($haystack, $needle) + strlen($needle);
	return substr($haystack, 0, $pos);
}

function validate_youtube_url($url)
{
	// Let's check the host first
	$parse = parse_url($url);
	$host = @$parse['host'];
	if (!in_array($host, array('youtube.com', 'www.youtube.com'))) {
		return false;
	}

	$ch = curl_init();
	$oembedURL = 'www.youtube.com/oembed?url=' . urlencode($url).'&format=json';
	curl_setopt($ch, CURLOPT_URL, $oembedURL);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	// Silent CURL execution
	$output = curl_exec($ch);
	unset($output);

	$info = curl_getinfo($ch);
	curl_close($ch);

	if ($info['http_code'] !== 404) {
		return true;
	}
	return false;
}

function show_comments($origin, $suborigin, $data = array(), $analytics = array())
{
	$ci = & get_instance();
	// return '<br><br>';
	$data['origin'] = $origin;
	$data['suborigin'] = $suborigin;
	$data['user_name'] = isset($data['name']) ? $data['name'] : $ci->session->userdata('user')['nick_name'];
	$data['user_id'] = isset($data['user_id']) ? $data['user_id'] : $ci->session->userdata('user_id');
	$data['ga'] = $analytics;
	$data['user_pic'] = $ci->db->select()
		->from('tbl_registrants')
		->where('registrant_id', $data['user_id'])
		->get()
		->row()
		->current_picture;
	return $ci->load->view('comment/index', $data, true);
}

function checkImageIfExist($url)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_NOBODY, 1);
	curl_setopt($ch, CURLOPT_FAILONERROR, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	if(curl_exec($ch) !== FALSE) {
		return true;
	} else {
		return false;
	}
}

function encrypt($string)
{
	$ci = & get_instance();
	if (!isset($ci->encrypt)) {
		$ci->load->library('Encrypt');
	}
	return $ci->encrypt->encode($string);
}

function decrypt($string)
{
	$ci = & get_instance();
	if (!isset($ci->encrypt)) {
		$ci->load->library('Encrypt');
	}
	return $ci->encrypt->decode($string);
}

function page_visit()
{
	$ci = & get_instance();
	if ($ci->session->userdata('user_id') && $ci->session->userdata('login_id')) {
		require_once 'administrator/helpers/visit_helper.php';
		$sections = explode(',', PAGES);
		$uri = $ci->uri->uri_string();
		foreach ($sections as $section) {
			if (match_page_section($section, $uri) || $section == '') {
				if (!$ci->session->userdata('visit_'.$section)) {
					$ci->session->set_userdata('visit_'.$section, 1);
					$ci->db->insert('tbl_page_visits', array(
						'uri' => $uri,
						'section' => $section,
						'ip_address' => $ci->input->ip_address(),
						'login_id' => $ci->session->userdata('login_id')
					));
					if ($ci->db->affected_rows()) {
						return $ci->db->insert_id();
					}
				}
				break;
			}
		}
	}
	return false;
}

function generate_password($length = 8)
{
	$chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.
		'0123456789`~!@#$^';

	$str = '';
	$max = strlen($chars) - 1;

	for ($i=0; $i < $length; $i++)
	$str .= $chars[mt_rand(0, $max)];

	return $str;
}

function unique_code($length = 8)
{
	return substr(md5(uniqid(mt_rand(), true)) , 0, $length);
}

function generate_token()
{
	return md5(uniqid());
}