<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function simplify_datetime_range($start_date,$end_date){

    if($start_date !== '0000-00-00 00:00:00' && $end_date !== '0000-00-00 00:00:00'){

        $start_date = strtotime($start_date);
        $end_date = strtotime($end_date);

        $start_time =  date('h:i a',$start_date)=='12:00 am' ?  '' : date('h:i a',$start_date); 
        $end_time =  date('h:i a',$end_date)=='12:00 am' ?  '' : date('h:i a',$end_date);

        $time = ' '.$start_time; //' '.$start_time && $end_time ? $start_time.' to '.$end_time : ' '.$start_time;


        if(date('Y',$start_date)==date('Y',$end_date)){
            $start_year = '';
            $end_year = ', '.date('Y',$start_date);
        }else{
            $start_year = ', '.date('Y',$start_date);
            $end_year = ', '.date('Y',$end_date);
        }

         if(date('d',$start_date)==date('d',$end_date)){

            $start_day = date(' d',$start_date);
            $end_day = '';

         }else{

            $start_day = date(' d',$start_date).' to ';
            $end_day = date(' d',$end_date);

         }

        return (date('m',$start_date)==date('m',$end_date)) ? date('F',$start_date).$start_day.$start_year.$end_day.$end_year.$time : date('F',$start_date).$start_day.$start_year.date('F',$end_date).$end_day.$end_year.$time;

     }else{

        return null;
        
     }


}