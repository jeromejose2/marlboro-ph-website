<?php

class User extends CI_Controller
{
	public function index()
	{
		redirect('user/login');
	}

	public function login()
	{
		$responseUrl = $this->input->get('success');
		$responseTkn = $this->input->get('token');
		$responseForgot = $this->input->get('request');

		$data = array();

		if($responseUrl == '0'){
			$data['errorUrl'] = '';
		}else if($responseUrl == '1'){
			$data['successUrl'] = '';
		}else if($responseForgot == '1'){
			$data['successForgot'] = '';	
		}else if($responseTkn == '0'){
			$data['invalidToken'] = '';
		}
		
		$this->session->reset_id();
		$data['invalid_message'] = $this->session->flashdata('invalid_login_message');

		// $this->load->layout('user/login', $data, 'user/template');
		// <<< IP FILTER >>>
		if($_SERVER['REMOTE_ADDR'] == '180.232.124.116') {
			$this->load->view('login_firmfilter', $data);
		} else {
			$this->load->view('login_rubyburst', $data);
		}

		/*if(date('Ymd') >= 20150401) 
			$this->load->layout('crossover/login', $data, 'crossover/template');
		else
			$this->load->layout('user/login', $data, 'user/template');*/
	}

	public function swf()
	{
		$this->load->view('user/swf_xml');
	}

	private function validate_password($password, $user, &$validation_message)
	{
		$count = 0;
		$patterns =  array('lowercase' => 'lowercase', 'uppercase' => 'uppercase', 'digit' => 'digit', 'special character' => 'special character');
		
		if (preg_match('/[a-z]/', $password)) {
			$count++;
			unset($patterns['lowercase']);
		}

		if (preg_match('/[A-Z]/', $password)) {
			$count++;
			unset($patterns['uppercase']);
		}
		
		if (preg_match('/\d/', $password)) {
			$count++;
			unset($patterns['digit']);
		}

		if (preg_match('/\W/', $password)) {
			$count++;
			unset($patterns['special character']);
		}

		if ($count < 3) {
			$last = array_pop($patterns); 
			$validation_message = 'Your password must contained from the following: '.implode(',', $patterns).' or '.$last.'</br>';
			return false;
		}
		
		if (preg_match('/user|guest|admin|sys|test|pass|super|'.$user->first_name.'|'.$user->third_name.'/i', $password, $matches)) {
			$validation_message = 'Your new password must not contained from the following: user,guest,admin,sys,test,pass,super,'.strtolower($user->first_name).' or '.strtolower($user->third_name);
			return false;
		}

		return true;
	}

	public function reset_success()
	{
		if (!$this->session->flashdata('forgot_submit')) {
			redirect('user/login');
		}
		$this->load->layout('user/reset_success', array(), 'user/template');
	}

	public function forgot_submit()
	{
		$email_input = strtolower($this->input->post('email'));
		if ($email_input) {

			$response = $this->spice->GetPerson(array('LoginName' => $email_input));
			$response_json = $this->spice->parseJSON($response);
			
			if($response_json->MessageResponseHeader->TransactionStatus != '600'){
				
				if($response_json->ConsumerProfiles->AgeVerificationStatus == 1){

				    $PersonId = $response_json->ConsumerProfiles->PersonId;
					// $dlTemplateId = 1033;
					// $cellId = 1394;
					$dlTemplateId = 1010;
					$cellId = 944;
					$login = $this->spice->createLoginToken($PersonId,$dlTemplateId, $cellId);
					$login_json = $this->spice->parseJSON($login);
			 
			 		$code = 'M00000215';
			 		$base_url = str_replace(array('http://','https://'), '', BASE_URL);
					$base_url = $base_url.'user/validate_reset_password_token'.'?token=' .$login_json->LoginToken;

					$params = ['Email' => $email_input,
					            'MailingCode' => $code,
					            'PersonId' => $PersonId,
					            'IsSMTPMail' => false,
					            'Fields' => [   
					            				['Name' => 'ph_campaignurlpersonal', "ValueDataType"=>"String", 'ValueAsString' => $base_url],
					                            ['Name' => 'ph_firstname', "ValueDataType"=>"String", 'ValueAsString' => $response_json->ConsumerProfiles->PersonDetails->FirstName],
					                            ['Name' => 'ph_lastname', "ValueDataType"=>"String", 'ValueAsString' => $response_json->ConsumerProfiles->PersonDetails->LastName],
					                            ['Name' => 'ph_emailaddress', "ValueDataType"=>"String", 'ValueAsString' => $email_input]
					                        ]
					            ];


				   $response = $this->spice->sendMailing($params);
				   $response_json = $this->spice->parseJSON($response);

				    if($response_json->MessageResponseHeader->TransactionStatus != 0){
			 			$this->output->set_content_type('application/json')->set_output(json_encode(array('error'=>$response_json->MessageResponseHeader->TransactionStatusMessage,'success'=>'')));

			 			$data_error = array('origin_id'=>RESET_PASSWORD,
								'method'=>'sendMailing',
								'transaction'=>'Request new password(forgot password) mailing.',
								'input'=> $this->session->userdata('spice_input'),
								'response'=>$response,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
						$this->spice->saveError($data_error);

						redirect('user/forgot');
			        } 

			        redirect('user/login?request=1');

				}else if($response_json->ConsumerProfiles->AgeVerificationStatus == 0){
					$this->session->set_userdata('invalid_forgot_email',  'Account pending for approval!' );

					/*$data_error = array('origin_id'=>RESET_PASSWORD,
							'method'=>'GetPerson',
							'transaction'=> 'forgot password',
							'input'=> $this->session->userdata('spice_input'),
							'response'=>$response,
							'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage.' but Age verification is pending or not verified.'
						);
					$this->spice->saveError($data_error);*/

					redirect('user/forgot');
				}

			}else{
				
				$this->session->set_userdata('invalid_forgot_email',  ucfirst(strtolower($response_json->MessageResponseHeader->TransactionStatusMessage )));

				$data_error = array('origin_id'=>RESET_PASSWORD,
						'method'=>'GetPerson',
						'transaction'=> 'forgot password',
						'input'=> $this->session->userdata('spice_input'),
						'response'=>$response,
						'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
					);
				$this->spice->saveError($data_error);

				redirect('user/forgot');
			}
		}
	}

	public function auth()
	{
		$this->load->model('Notification_Model');
		$username = strtolower($this->input->post('username'));
		$password = $this->input->post('password');
		$message = 'The username/password you entered is incorrect. Please try again.';
		

		if (filter_var($username, FILTER_VALIDATE_EMAIL) && $password) {			

			
			$this->load->model('Registration_Model');
			$this->load->library('spice');
			
			$registrant = false;
			$accessGranted = $this->spice->loginMember($username,$password);
			$message = $accessGranted->ResponseHeader->TransactionStatusMessage;

			if(!$accessGranted->ResponseHeader->TransactionStatus && (int)$accessGranted->PersonId){

				session_regenerate_id();

				$response = $this->spice->GetPerson(array('LoginName'=>$username));
				$response = $this->spice->parseJSON($response);


				$RegistrationDate = $this->spice->extractSpiceDate($response->ConsumerProfiles->RegistrationDate);
				
				$AgeVerificationStatus = $response->ConsumerProfiles->AgeVerificationStatus;
 
				if(!$AgeVerificationStatus){

					$this->session->set_userdata('invalid_login_message', 'Your Age Verification status has not been verified.');
		    		redirect('user/login');
 
				}							

				$ConsumerProfiles = $response->ConsumerProfiles;
				$ConsumerProfiles = $this->spice->parseJSON($this->spice->toJSONFormat((array)$ConsumerProfiles));
				
				$total_derivations = count($ConsumerProfiles->Derivations);

				for($i = 0; $i<$total_derivations; $i++){
					if($ConsumerProfiles->Derivations[$i]->AttributeCode=='ca_referral_code'){
						$Derivations_referral = $ConsumerProfiles->Derivations[$i];
					}
					// changes
					// }else{
					// 	$Derivations_referral = '';
					// }
					// changes
				}

				$Derivations_referral = $Derivations_referral ? $Derivations_referral : '';

				// $Derivations_referral = $ConsumerProfiles->Derivations[REFERRAL_CODE];
				
				$is_registered =  $this->db->select()->from('tbl_registrants')->where('person_id',$ConsumerProfiles->PersonId)->get()->row();

				$province = strtolower($ConsumerProfiles->Addresses[0]->Locality);
				$city = strtolower($ConsumerProfiles->Addresses[0]->City);
				$street_name = $ConsumerProfiles->Addresses[0]->AddressLine1;
				$barangay = $ConsumerProfiles->Addresses[0]->Area;
				$zip_code = $ConsumerProfiles->Addresses[0]->PostalCode;

				$date_of_birth = $this->spice->extractSpiceDate($ConsumerProfiles->PersonDetails->DateOfBirth);
				
				$province = $this->db->select()->from('tbl_provinces')->where('LOWER(province)',$province)->get()->row();
				$province = $province ? $province->province_id : '';

				$city = $this->db->select()->from('tbl_cities')->where(array('LOWER(city) '=>$city,'province_id'=>$province))->get()->row();
				$city = $city ? $city->city_id : '';
				$nick_name = '';
				$outlet_code = '';
				// $referral_code = $this->Registration_Model->referral_code();
				$Derivations = array();

				if($ConsumerProfiles->Derivations){

					$total_derivations = count($ConsumerProfiles->Derivations);
						
					for($i = 0; $i<$total_derivations; $i++){
						if($ConsumerProfiles->Derivations[$i]->AttributeCode=='ca_nickname'){
							$nick_name = $ConsumerProfiles->Derivations[$i]->AttributeValue;
						}

						if($ConsumerProfiles->Derivations[$i]->AttributeCode=='ca_outletcode'){
							$outlet_code = $ConsumerProfiles->Derivations[$i]->AttributeValue;
						}

						if($ConsumerProfiles->Derivations[$i]->AttributeCode=='ca_personal_referral_code'){
							$referral_code = $ConsumerProfiles->Derivations[$i]->AttributeValue ? $ConsumerProfiles->Derivations[$i]->AttributeValue : $this->Registration_Model->referral_code();
							// changes
							if( ! $ConsumerProfiles->Derivations[$i]->AttributeValue || $ConsumerProfiles->Derivations[$i]->AttributeValue == '0') {
								$ConsumerProfiles->Derivations[$i]->AttributeValue = $referral_code;
							}
							// changes
						}else{
							//$ConsumerProfiles->Derivations[$i]->AttributeValue = '0';
						}
					}

					// $nick_name = $ConsumerProfiles->Derivations[NICKNAME]->AttributeValue;
					// $outlet_code = $ConsumerProfiles->Derivations[OUTLET_CODE]->AttributeValue;

					$Derivations = json_decode(json_encode($ConsumerProfiles->Derivations),true);
				}

				if($is_registered->referral_code == ''){
					$referral_code = $this->Registration_Model->referral_code();
				}

				if( ! $ConsumerProfiles->Derivations[$i]->AttributeValue || $ConsumerProfiles->Derivations[$i]->AttributeValue == '0') {
					$has_referral = $this->db->select('referral_code')->from('tbl_registrants')->where('person_id', (string)$ConsumerProfiles->PersonId)->get()->row();
					if($has_referral->referral_code) {
						$insert_new_user = array('person_id'=>(string)$ConsumerProfiles->PersonId,
									 'first_name'=>(string)$ConsumerProfiles->PersonDetails->FirstName,
									 'third_name'=>(string)$ConsumerProfiles->PersonDetails->LastName,
									 'middle_initial'=>(string)$ConsumerProfiles->PersonDetails->MiddleName,
									 'nick_name'=>$nick_name,
									 'outlet_code'=>$outlet_code,
									 'referred_by_code'=> ($Derivations_referral->AttributeValue && $Derivations_referral->AttributeCode==='ca_referral_code') ? $Derivations_referral->AttributeValue : ''
										);
					} else {
						$insert_new_user = array('person_id'=>(string)$ConsumerProfiles->PersonId,
									 'first_name'=>(string)$ConsumerProfiles->PersonDetails->FirstName,
									 'third_name'=>(string)$ConsumerProfiles->PersonDetails->LastName,
									 'middle_initial'=>(string)$ConsumerProfiles->PersonDetails->MiddleName,
									 'nick_name'=>$nick_name,
									 'outlet_code'=>$outlet_code,
									 'referral_code' => (string) $referral_code,
									 'referred_by_code'=> ($Derivations_referral->AttributeValue && $Derivations_referral->AttributeCode==='ca_referral_code') ? $Derivations_referral->AttributeValue : ''
										);
					}					
				} else {
					$insert_new_user = array('person_id'=>(string)$ConsumerProfiles->PersonId,
									 'first_name'=>(string)$ConsumerProfiles->PersonDetails->FirstName,
									 'third_name'=>(string)$ConsumerProfiles->PersonDetails->LastName,
									 'middle_initial'=>(string)$ConsumerProfiles->PersonDetails->MiddleName,
									 'nick_name'=>$nick_name,
									 'outlet_code'=>$outlet_code,
									 'referred_by_code'=> ($Derivations_referral->AttributeValue && $Derivations_referral->AttributeCode==='ca_referral_code') ? $Derivations_referral->AttributeValue : ''
										);
				}
			 	
			 	// print_r($referral_code); die();
				if($is_registered){

					$this->db->where('person_id', $ConsumerProfiles->PersonId);
					$this->db->update('tbl_registrants', $insert_new_user); 

				}else{
					$this->db->set('date_created',"'$RegistrationDate'",FALSE);
					$this->db->insert('tbl_registrants', $insert_new_user);

					
					$params = array('person_id'=> $ConsumerProfiles->PersonId,
		                    		'updates'=> array('Derivations' => $Derivations)
                    			);

	        		$response = $this->spice->updatePerson($params);
	        		$response_json = $this->spice->parseJSON($response);

				}

				$registrant = $this->db->select()->from('tbl_registrants')->where('person_id',$ConsumerProfiles->PersonId)->get()->row();		
 			 
				$user = array('login_attempts'=>$registrant->login_attempts,
							  'date_blocked'=>$registrant->date_blocked,
							  'person_id'=>$ConsumerProfiles->PersonId,
							  'registrant_id'=>$registrant->registrant_id,
							  'referred_by_code'=>$registrant->referred_by_code,
							  'registrant_id'=>$registrant->registrant_id,
							  'first_name'=>$ConsumerProfiles->PersonDetails->FirstName,
							  'third_name'=>$ConsumerProfiles->PersonDetails->LastName,
							  'middle_initial'=>$ConsumerProfiles->PersonDetails->MiddleName,
							  'date_of_birth'=>$ConsumerProfiles->PersonDetails->DateOfBirth,
							  'nick_name'=>$registrant->nick_name,
							  'mobile_phone'=>$ConsumerProfiles->PersonDetails->MobileTelephoneNumber,
							  'referral_code'=>$registrant->referral_code,
							  'email_address'=>$ConsumerProfiles->Login[0]->LoginName,
							  'total_points'=>$registrant->total_points,
							  'status'=>$ConsumerProfiles->AgeVerificationStatus,
							  'is_cm'=>$registrant->is_cm
						  );

				$this->load->library('Encrypt');

				$today = date('Y-m-d H:i:s');				 

				// check if from referral;
				$isFirstLogin = false;

				if($user['referred_by_code']){
 
					$referer = $this->Registration_Model->get_user_by_refCode($user['referred_by_code']);
					if($referer){
						$isFirstLogin = $this->Registration_Model->isFirstLogin($user['registrant_id']);
					}
					
				}
 				
 				//get last login

				$this->db->insert('tbl_login', array(
					'registrant_id' => $user['registrant_id'],
					'date_login' => $today
				));
				$ins_id = $this->db->insert_id();

				if ($this->db->affected_rows()) {

					$this->load->model(array('profile_model','Points_Model'));

					$city = $this->db->select()->from('tbl_cities')
												->where('city_id', $registrant->city)
												->get()
												->row();

					$response = $this->spice->GetPerson(array('PersonId' =>$this->spice->getMemberPersonId()));
					$spice_userinfo = $this->spice->parseJSON($response);
					$spice_userinfo = $spice_userinfo->ConsumerProfiles;

					$this->session->set_userdata('user', array(
						'first_name' => $user['first_name'],
						'last_name' => $user['third_name'],
						'middle_initial' => $user['middle_initial'],
						'birthdate' => $user['date_of_birth'],
						'nick_name' => $user['nick_name'],
						'mobile_num' => $user['mobile_phone'],
						'referral_code' => $user['referral_code'],
						'person_id'=> $user['person_id'],
						'email' => $user['email_address'],
						'city' => $spice_userinfo->Addresses[0]->City
					));

					$this->session->set_userdata('check_offers', 1);
					$this->session->set_userdata('user_points', $user['total_points']);
					$this->session->set_userdata('user_id', $user['registrant_id']);
					$this->session->set_userdata('login_id', $ins_id);
					$this->session->set_userdata('is_cm', $user['is_cm']);
					$this->session->set_userdata('flash_offer_sess', 1);
					$this->session->set_userdata('app_id', APP_ID);


					$this->db->where('registrant_id', $user['registrant_id']);
					$this->db->update('tbl_registrants', array(
						'login_attempts' => 0,
						'date_blocked' => null,
						'login_id' => $this->session->userdata('login_id'),
						'app_id' => $this->session->userdata('app_id')
					));
					

					if(!$isFirstLogin) {

						// if(date('Ymd') >= 20150401){
						// 	$res = $this->Points_Model->earn_login_km(5, array(
						// 		'date' => $today
						// 	));

						// 	$this->db->close();
						// }else{
						// 	$this->Points_Model->earn(LOGIN, array(
						// 		'date' => $today,
						// 		'suborigin' => $this->session->userdata('login_id')
						// 	));
						// }

						//get last login
						$registrant_data = $this->db->select('*')->from('tbl_registrants')
														->where('registrant_id', $user['registrant_id'])
														->get()->row();

						$login = $this->db->select('*')->from('tbl_login')
														->where('registrant_id', $user['registrant_id'])
														->order_by('date_login', 'DESC')
														->limit(2)
														->get()->result();
					

						$last_login = date_create($registrant_data->last_login);
						$last_login_formatted = date_format($last_login, "Y/m/d");
						$today = date('Y/m/d');

						$lastlogintimestamp = strtotime($last_login_formatted);
						$todaytimestamp = strtotime($today);

						$timeDiff = abs($todaytimestamp - $lastlogintimestamp);
						$numberDays = $timeDiff/86400;

						$numberDays = intval($numberDays);


						$login_promo = $this->db->select('*')->from('tbl_login_offers')->where('is_active', 1)->order_by('date_created', 'desc')->get()->row();
						if(!$this->validate_offer_confirmation($registrant_data->registrant_id, $login_promo->id)){
							if($numberDays>=$login_promo->days_inactive){
								$today = date('Y-m-d');
								if((str_replace("-", "", $today)>=str_replace("-", "", $login_promo->start_date)) and (str_replace("-", "", $today)<=str_replace("-", "", $login_promo->end_date))){
									// echo "granted";
									// echo $login_promo->days_inactive."<br />";
									// echo $numberDays;
									// exit;
									$this->session->set_userdata('login_promo', 1);
									$this->session->set_userdata('last_login', $login[1]->date_login);
									$this->session->set_userdata('stat', "grant");
								}else{
									// echo "nd pasok ung date range";
									// exit;
									$this->session->set_userdata('login_promo', 0);
									$this->session->set_userdata('last_login', $login[1]->date_login);
									$this->session->set_userdata('stat', "nd pasok ung date range");
								}
							}else{
								// echo "nd pasok ung required days of login mo";
								// exit;
								$this->session->set_userdata('login_promo', 0);
									$this->session->set_userdata('last_login', $login[1]->date_login);
								$this->session->set_userdata('stat', "nd pasok ung required days of login mo");
							}
						}else{
							$this->session->set_userdata('login_promo', 0);
									$this->session->set_userdata('last_login', $login[1]->date_login);
							$this->session->set_userdata('stat', "claimed na");
						}
						//END LOGIN PROMO

						$this->Points_Model->earn(LOGIN, array(
							'date' => $today,
							'suborigin' => $this->session->userdata('login_id')
						));
	 
						$this->db->where('registrant_id', $user['registrant_id']);
						$this->db->update('tbl_registrants', array(
							'last_login' => $today
						));

						if(!$user['referred_by_code']){
							$this->Points_Model->earn(REGISTRATION);							 
						} else {
							$valid_refer_code = $this->Registration_Model->get_user_by_refCode($user['referred_by_code']);
							if( ! $valid_refer_code) {
		                                $this->Points_Model->earn(REGISTRATION);                             
							}
						}

						//SPICE INSERT DERIVATIONS
						$response = $this->spice->GetPerson(array('PersonId' =>$this->spice->getMemberPersonId()));
						$spice_userinfo = $this->spice->parseJSON($response);
						$spice_userinfo = $spice_userinfo->ConsumerProfiles;

						$consumer_attributes = $this->profile_model->get_rows(array('table'=>'tbl_consumer_attr_spice'))->result_array();
						
						$total_derivations = count($spice_userinfo->Derivations);
						$total_address = count($spice_userinfo->Addresses);
						$total_cas = count($consumer_attributes);
						$derivations = array();
						$var = array();
						$country = 'PH';
						$total_km = $registrant->total_km_points;
						$credit_km = $registrant->credit_km_points;

					 	// print_r($spice_userinfo); die();
						for($i = 0; $i<$total_cas; $i++){       
						                            
						    $var = array('AttributeCode'=> $consumer_attributes[$i]['attribute_code'],'AttributeValue'=> '0');

						    for($j = 0; $j<$total_derivations; $j++){
						        if($consumer_attributes[$i]['attribute_code'] == $spice_userinfo->Derivations[$j]->AttributeCode){
						            if($consumer_attributes[$i]['attribute_code'] == 'ca_total_km'){
						            	$var = array('AttributeCode'=> $consumer_attributes[$i]['attribute_code'],'AttributeValue'=> $total_km);
						                break;
						            }else if($consumer_attributes[$i]['attribute_code'] == 'ca_credit_km'){
						            	$var = array('AttributeCode'=> $consumer_attributes[$i]['attribute_code'],'AttributeValue'=> $credit_km);
						                break;
						            }else if($spice_userinfo->Derivations[$j]->AttributeValue == ''){
						                $var = array('AttributeCode'=> $consumer_attributes[$i]['attribute_code'],'AttributeValue'=> '0');
						                break;
						            }else{
						                $var = array('AttributeCode'=> $spice_userinfo->Derivations[$j]->AttributeCode,'AttributeValue'=> $spice_userinfo->Derivations[$j]->AttributeValue);
						                break;
						            }
						        }else{
						            $var = array('AttributeCode'=> $consumer_attributes[$i]['attribute_code'],'AttributeValue'=> '0');
						            // break;
						        }
						    }
						    
						    $derivations[] = $var;

						}


						if($total_address != 0){
							if($spice_userinfo->Addresses[0]->Country == ''){
								$country = 'PH';
							}

							$params = array('person_id'=> $this->spice->getMemberPersonId(),
		                        			'updates'=> array(
		                        				'AddressType' => array(
		                        					array(
		                        						'AddressLine1' => $spice_userinfo->Addresses[0]->AddressLine1,
														'Area' => $spice_userinfo->Addresses[0]->Area,
														'Locality' => $spice_userinfo->Addresses[0]->Locality,
												        'City' => $spice_userinfo->Addresses[0]->City, 
												        'Country' => $country,
												        'Premise'=> $country,
												        'PostalCode' => $spice_userinfo->Addresses[0]->PostalCode
		                        					)
		                        				),
		                                        'Derivations'=> $derivations,
		                                )
							);
						}else{
								$params = array('person_id'=> $this->spice->getMemberPersonId(),
		                        			'updates'=> array(
		                                        'Derivations'=> $derivations,
		                                )
								);
						}

		        		$response = $this->spice->updatePerson($params);
		        		$response_json = $this->spice->parseJSON($response);


						if ($response_json->MessageResponseHeader->TransactionStatus != 5) {
							
							$data_error = array('origin_id'=>LOGIN,
									'method'=>'ManagePerson',
									'transaction'=> 'update person derivations',
									'input'=> $this->session->userdata('spice_input'),
									'response'=>$response,
									'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
								);
							$this->spice->saveError($data_error);

							$this->session->set_userdata('response_message_error', ucfirst(strtolower($response_json->MessageResponseHeader->TransactionStatusMessage)));
							redirect('user/login');
						}
					
					}else{
						$res = $this->Points_Model->earn_login_km(5, array(
							'date' => $today
						));
						$this->session->set_userdata('flash_referral_offer_sess',1);

						$this->db->where('registrant_id', $user['registrant_id']);
						$this->db->update('tbl_registrants', array(
							'last_login' => $today
						));

						/// Insert spice data to website db								
						// var_dump($user['registrant_id']);
						// echo ' | ';
						$referee_first_login = $this->Points_Model->earn(
							REFEREE_FIRSTLOGIN,
							array(
								'registrant' => $user['registrant_id'],
								'suborigin' => $this->session->userdata('login_id'),
								'remarks' => 'first login as LAS2'
							));
						// var_dump($referee_first_login);
						// echo ' | ';

						$referer_first_login = $this->Points_Model->earn(
							REFERER_FIRSTLOGIN,
							array(
								'registrant' => $referer['registrant_id'],
								'suborigin' => $this->session->userdata('login_id'),
								'remarks' => 'LAS2 ('. $user['registrant_id'] .') logged-in'
							));
						// var_dump($referer_first_login);
						// echo ' | ';

						$this->load->model('Crossover_Notification_Model');

						$notifyLas2 = "Welcome to the MARLBORO community! Here’s ".REFEREE_FIRST_LOGIN_POINT." points to get you started. ".
									"Discover the many exciting offers you can redeem on PERKS and continue visiting".
									" the website to get more points. And while you’re at it, why don’t you invite".
									" your adult smoker friends as well?";

							$notify_referee = $this->Notification_Model->notify($user['registrant_id'], REFEREE_FIRSTLOGIN, 
								array('message' => $notifyLas2,'suborigin'=>0));
							// var_dump($notify_referee);
							// echo ' | ';

						// $notifyLas1First = "Congratulations! Your friend, ".$user['first_name']." ".$user['third_name'].", ".
						// 	"is now part of the MARLBORO community! You get 100 points ".
						// 	"for the successful referral. Don't forget to invite ".$user['nick_name']." ".
						// 	"to log in and experience the exclusive PERKS in the website.";						

						$notifyLas1First = "Congratulations! You get 300 points for successfully referring ".
								$user['first_name']." ".$user['third_name']. " Your friend has also logged in and has started enjoying the".
								" perks of being a MARLBORO community member.";

							$notify_referer = $this->Notification_Model->notify($referer['registrant_id'], REFERER_FIRSTLOGIN,
								array('message' => $notifyLas1First,'suborigin'=>0));
							// var_dump($notify_referer);
							// exit;

						

						// $notifyLas1 = "Congratulations! You get 200 more points! Your friend, ".
						// 	$user['first_name']." ".$user['third_name'].", has just logged in to MARLBORO.PH.".
						// 	" Invite your friends to be active members of the community,".
						// 	" and get more points to win freebies.";

						// $notifyLas1 = "Congratulations! You get ".REFERER_FIRST_LOGIN_POINT." more points! Your friend, ".
						// 	$user['first_name']." ".$user['third_name'].", has just logged in to MARLBORO.PH.".
						// 	" Invite your friends to be active members of the community,".
						// 	" and get more points to win freebies.";
						
						// if( ! IS_CROSSOVER) {
						// 	$this->Notification_Model->notify($referer['registrant_id'], REFERER_FIRSTLOGIN, 
						// 		array('message' => $notifyLas1,'suborigin'=>0));
						// } else {
						// 	$this->Crossover_Notification_Model->notify($referer['registrant_id'], REFERER_FIRSTLOGIN, 
						// 		array('message' => $notifyLas1,'suborigin'=>0));
						// }

					}
					

					$user_id = $this->session->userdata('user_id');
					$hidden_packs = $this->profile_model->get_rows(array('table'=>'tbl_hidden_packs',
																'where'=>array("CURDATE() >= DATE(start_date) AND CURDATE() <= DATE(end_date) AND is_deleted = '0' AND hidden_pack_id NOT IN(SELECT hidden_pack_id FROM tbl_hidden_pack_logs WHERE registrant_id = '$user_id' AND DATE(date_added) = CURDATE())"=>null)));

					if($hidden_packs->num_rows()){

						$pages = '';
						$temp = array();

						foreach($hidden_packs->result() as $r){

							$pages = $r->page ? explode(',',$r->page) : false;

							if($pages){

								$key = rand(0,count($pages) - 1);
								$temp = array('id'=>$r->hidden_pack_id,'image'=>BASE_URL.'uploads/hidden_packs/'.$r->image);
								$this->session->set_userdata($pages[$key],$temp); 
							}

						} // End foreach

					}
					$_SESSION['filter_first_login'] = -1;
					if(date('Ymd') >= 20140725)  {
						if($this->session->userdata('edm_popup')) {
							redirect(BASE_URL . 'edm?' . $this->session->userdata('edm_popup'));
						} else {
							//redirect(BASE_URL . 'premium');	
							// redirect(BASE_URL . 'edm?' . $this->session->userdata('edm_popup'));
							redirect(BASE_URL);
						}
						
					}  
						
					else 
						redirect(BASE_URL);

				}

			}		
					
		}
		
		if( ! $this->session->userdata('profile_picture')) {
			$row = $this->db->select()->from('tbl_registrants')->where('registrant_id', $this->session->userdata('user_id'))->get()->row();
			if($row) {
				if(file_exists('uploads/profile/'.$row->registrant_id.'/'.$row->current_picture) && $row->current_picture) {
					$this->session->set_userdata('profile_picture', base_url().'uploads/profile/'.$row->registrant_id.'/'.$row->current_picture);
				} else {
					$this->session->set_userdata('profile_picture', 'http://marlboro-stage2.yellowhub.com/spice/images/default-profile-photo.png');
				}
			}
		}
		
		$this->session->set_userdata('invalid_login_message', ucfirst(strtolower($message)));
	    redirect('user/login');

	}

	 

	 
	public function validate_step_1($funcCall = false)
	{
		if (!isset($this->form_validation))
			$this->load->library('form_validation'); 

		$_POST['mobile_number'] = $this->input->post('mobile_prefix').$this->input->post('mobile_number');

		$this->form_validation->set_rules('smoker','Smoker','required|numeric|callback_smoker_check');
		$this->form_validation->set_rules('fname', 'First name', 'required|alpha_space');
		$this->form_validation->set_rules('lname', 'Last name', 'required|alpha_space');
		$this->form_validation->set_rules('mname', 'Middle name', 'alpha_space');
		$this->form_validation->set_rules('nname', 'Nick name', 'required|alpha_space');
		$this->form_validation->set_rules('birthday', 'Birthdate', 'required|callback_birthdate_check');
		$this->form_validation->set_rules('gender', 'Gender', 'required|callback_check_gender');
		$this->form_validation->set_rules('mobile_prefix', 'Mobile Prefix number', 'required|numeric|min_length[4]|max_length[4]|callback_check_mobile_prefix');
		$this->form_validation->set_rules('mobile_number', 'Mobile number', 'required|numeric|min_length[11]|max_length[11]');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('street', 'Street', 'required');
		$this->form_validation->set_rules('brgy', 'Barangay', 'required');
		$this->form_validation->set_rules('province', 'Province', 'required|numeric|callback_province_check');
		$this->form_validation->set_rules('city', 'City', 'required|numeric|callback_city_check');
		$this->form_validation->set_rules('zip_code', 'Zip Code', 'required|numeric|min_length[4]|max_length[4]');
		// $this->form_validation->set_rules('referred_code', 'Referral Code', 'callback_validate_referral_code');
		$this->form_validation->set_rules('outlet_code', 'Outlet Code', 'callback_outlet_code_check');
		//$this->form_validation->set_message('email_check', 'E-mail already exists in database');
		$this->form_validation->set_message('outlet_code_check','Outlet code is not existing');
		//$this->form_validation->set_message('birthdate_check', 'Sorry, You need to be a legal aged (18 years old and above) smoker in order to register.');

		if (!$funcCall) {
			$result = array(
				'success' => false,
				'error' => ''
			);
			$this->output->set_content_type('application/json');
			if (!$this->form_validation->run()) {
				$result['error'] = validation_errors();
				$this->output->set_output(json_encode($result));
			} else {
				$result['success'] = true;
				$this->output->set_output(json_encode($result));
			}
		}
	}

	public function validate_referral_code($referral_code){

		if($referral_code){

			$row = $this->db->get_where('tbl_referrals',array('email'=>$this->input->post('email'),'code'=>$referral_code))->row();

			if(!$row){

				$this->form_validation->set_message('validate_referral_code','Referral code not valid.');
				return false;

			}

		}

		return true;

	}

	public function check_mobile_prefix($val)
	{

		$row = $this->db->get_where('tbl_mobile_prefix',array('mobile_prefix'=>$val))->row();
		if(!$row){
			$this->form_validation->set_message('check_mobile_prefix','Mobile Prefix number does not exists.');
			return false;
		}else{
			return true;
		}

	}

	public function check_gender($val)
	{
		if($val !='F' && $val != 'M')
		{
			$this->form_validation->set_message('check_gender','Gender field may only contain "F" character for female or "M" character for male.');
			return false;
		}else{
			return true;
		}

	}

	public function smoker_check($val)
	{

		if($val != 1){
			$this->form_validation->set_message('smoker_check','Smoker field may only contain "Yes" character.');
			return false;
		}else{
			return true;
		}

	}

	public function token_check($token)
	{
		$success = false;
		if ($token && $this->session->userdata('reg_token') === $token) {
			$success = true;
		} else {
			//$this->form_validation->set_message('token_check', 'Invalid form request. correct:'.$this->session->userdata('reg_token'));
			$this->form_validation->set_message('token_check', 'Invalid form request.');
		}
		return $success;
	}

	public function outlet_code_check($code)
	{
		if (!$code) {
			return true;
		}

		$outlet = $this->db->select()
			->from('tbl_outlet_codes')
			->where('outlet_code', $code)
			->limit(1)
			->get()
			->row();

		return $outlet ? true : false;
	}

	public function email_check($email)
	{
		$exists = $this->db->select()
			->from('tbl_registrants')
			->where('email_address', $email)
			->limit(1)
			->get()
			->row();
		return $exists ? false : true;
	}

	public function birthdate_check($date)
	{
		$date_arr = explode('-',$date);

		$year = (isset($date_arr[0])) ? $date_arr[0] : 0;
		$month = (isset($date_arr[1])) ? $date_arr[1] : 0;
		$day = (isset($date_arr[2])) ? $date_arr[2] : 0;

		if(checkdate($month, $day, $year)){

			$date = new DateTime($date);
			$age = $date->diff(new DateTime);
			if (!$age || $age->y < MINIMUM_AGE) {
				$this->form_validation->set_message('birthdate_check',' Sorry, You need to be a legal aged ('.MINIMUM_AGE.' years old and above) smoker in order to register.');
				return false;
			}
			return true;

		}else{
			$this->form_validation->set_message('birthdate_check','Invalid Birthdate format.');
			return false;
		}

		/*
		$date = new DateTime($date);
		$age = $date->diff(new DateTime);
		if (!$age || $age->y < MINIMUM_AGE) {
			return false;
		}
		return true;
		*/
	}

	public function province_check($p)
	{
		$province = $this->db->select()
			->from('tbl_provinces')
			->where('province_id', $p)
			->limit(1)
			->get()
			->row();
		if (!$province) {
			$this->form_validation->set_message('province_check','Province does not exists.');
			return false;
		}
		return true;
	}

	public function city_check($id)
	{
		
		$where = array('city_id'=>$id,'province_id'=>$this->input->post('province'));
		 
		$city = $this->db->select()
			->from('tbl_cities')
			->where($where)
			->limit(1)
			->get()
			->row();
 
		if (!$city) {
			$this->form_validation->set_message('city_check','City does not exists.');
			return false;
		}
		return true;
	}

	public function validate_step_2($funcCall = false)
	{

 		$current_brand = $this->db->get_where('tbl_brands_spice',array('brand_id'=>$this->input->post('current_brand')))->row();
		$first_alternate_brand = $this->db->get_where('tbl_brands_spice',array('brand_id'=>$this->input->post('first_alternate_brand')))->row();
		$error = '';
		$count_error = 0;

		if(!$current_brand){
			$error = 'Your primary brand does not exists.';
			$count_error++;
		}

		if(!$first_alternate_brand){
			$error .= '<br/><br/>Your secondary brand does not exists.';
			$count_error++;
		}

		if (!$funcCall) {
			$result = array(
				'success' => false,
				'error' => ''
			);
			$this->output->set_content_type('application/json');
			if ($count_error) {
				$result['error'] = $error;
				$this->output->set_output(json_encode($result));
			} else {
				$result['success'] = true;
				$this->output->set_output(json_encode($result));
			}
		}
 

	}

	public function validate_step_3($funcCall = false, $withGiid = true)
	{


		if (!isset($this->form_validation))
			$this->load->library('form_validation');

		$this->form_validation->set_rules('__token', 'Token', 'callback_token_check');
		if ($withGiid) {
			$this->form_validation->set_rules('giid_number', 'GIID Number', 'required');
			$this->form_validation->set_rules('giid_type', 'GIID Type', 'required');
		}


		if (!$funcCall) {
			$result = array(
				'success' => false,
				'error' => ''
			);
			$this->output->set_content_type('application/json');
			if (!$this->form_validation->run()) {
				$result['error'] = validation_errors();
				$this->output->set_output(json_encode($result));
			} else {
				$imageString = $this->input->post('captured_image_ba');
				if (empty($_FILES['giid']['name']) && !$imageString) {
					$result['error'] = 'Please upload a file.';
					$this->output->set_output(json_encode($result));
					return;
				}
				$result['success'] = true;
				$this->output->set_output(json_encode($result));
			}
		}

	}

	

	public function confirm()
	{

		$this->load->library(array('spice','Encrypt'));
		$this->load->helper('upload');


 
		$imageString = $this->input->post('captured_image_ba');		 
		$salt = generate_password(20);
		$fileTypes = array('.jpeg'=>1,'.png'=>2,'.jpg'=>3,'.bmp'=>4,'.pdf'=>5);	

		$city = $this->db->select()
			->from('tbl_cities')
			->where(array('city_id'=>$this->input->post('city')))
			->limit(1)
			->get()
			->row();

		$province = $this->db->select()
			->from('tbl_provinces')
			->where(array('province_id'=>$this->input->post('province')))
			->limit(1)
			->get()
			->row();

		$city = $city ? $city->city : '';
		$province = $province ? $province->province : '';
		$path = './uploads/registration/';
		$f = '';


		if(!is_dir($path)){
			mkdir($path,0777,true);
		}
 

		if(!$imageString){

 			/*$file_tmp = $_FILES['giid']['tmp_name'];
		    $type = pathinfo($file_tmp, PATHINFO_EXTENSION);
			$imageString = base64_encode(file_get_contents($file_tmp));*/

			$filename = uniqid();
			$max_size = 2048;
			$params = array('upload_path'=>$path,'allowed_types'=>SPICE_FILE_UPLOAD_ALLOWED_TYPES,'file_name'=>$filename,'max_size'=>$max_size,'max_width'=>0,'max_height'=>0,'do_upload'=>'giid');
	    	$file = upload($params);

	    	if(is_array($file)){ 

	    		$image = BASE_URL.'uploads/registration/'.$file['file_name'];
	    		$imageString = base64_encode(file_get_contents($image));
				unlink($path.$file['file_name']);

	    	}else{

	    		$this->output->set_output('<br/>ERROR UPLOAD: '.$file);
	    		return;

	    	}

	    	$IdDocumentFileType = $fileTypes[$file['file_ext']];
			
		}else{

			

			if(!is_dir($path)){
				mkdir($path,0777,true);
			}
			$filename = uniqid().'.jpg';
			$IdDocumentFileType = $fileTypes['.jpg'];

			file_put_contents($path.$filename, gzuncompress(base64_decode($imageString)));
			$imageString = base64_encode(file_get_contents(BASE_URL.$path.$filename));
			unlink($path.$filename);

		}

			$marketBrands = $this->db->select()
                    ->from('tbl_brands_spice')
                    ->where(array('fieldmarketer_flag' => 'true'))
                    ->get()
                    ->result();

	        foreach($marketBrands as $v){
	            $marketingPreference[] = array('BrandId'=>$v->brand_id, 'OptValue' => '1');
	        }

            $marketChannels = $this->db->select()
					->from('tbl_channel_codes')
					->get()
					->result();
			
			foreach($marketChannels as $v){
                $marketingPreference[] = array('ChannelCode'=>$v->channel_code, 'OptValue' => '1');
            }

			$email_pre = '';
			$MultibrandSmoker = $this->input->post('current_brand') !== $this->input->post('first_alternate_brand') ? '1' : '0';

			$dateTime = DateTime::createFromFormat('Y-m-d H:i:s', $this->input->post('birthday').' 13:00:00');
			$dateTime->setTimeZone(new DateTimeZone('Asia/Manila'));
			$DateOfBirth = sprintf(
			   '/Date(%s%s)/',
			   $dateTime->format('U') * 1000,
			   $dateTime->format('O')
			);

			/*jpeg - 1
			png - 2
			jpg - 3
			bmp - 4
			pdf - 5*/

			
			$dateTime = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
	        $dateTime->setTimeZone(new DateTimeZone('Asia/Manila'));
	        $date = sprintf('/Date(%s%s)/',
	                               $dateTime->format('U') * 1000,
	                               $dateTime->format('O')
	                            );
			$data = array(  'ProfileType'=>'C',
							'ConsumerProfile'=> array( 'PersonalDetails'=>array(
																				'FirstName' => $this->input->post('fname'),
																			    'MiddleName'=> $this->input->post('mname'),
																			    'LastName' => $this->input->post('lname'),
																			    'Gender'=> $this->input->post('gender'),
	 																		    'EmailAddress'=> strtolower($this->input->post('email')),
																			    'MobileTelephoneNumber'=> ltrim($this->input->post('mobile_prefix'), '0').$this->input->post('mobile_number'),
																			    'DateOfBirth'=>$DateOfBirth
																			    ),
														'AddressType'=>array(array( 
																			        'AddressLine1' => $this->input->post('street'),
																					'Area' => $this->input->post('brgy'),
																					'Locality' => $province,
																			        'City' => $city, 
																			        'Country' => 'PH',
																			        'Premise'=>'PH',
																			        'PostalCode' => $this->input->post('zip_code')
																			    )
																    ),
														'Derivations'=>array(																		
																    	[
																    		'AttributeCode' => 'ca_nickname',
																    		'AttributeValue' =>  $this->input->post('nname')
																    	],
																    	[
																    		'AttributeCode' => 'ca_outletcode',
																    		'AttributeValue' => $this->input->post('outlet_code') ? $this->input->post('outlet_code') : '0' //$spice_userinfo->Derivations[1]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_reaction',
																    		'AttributeValue' =>  $this->input->post('alternate_purchase') //$spice_userinfo->Derivations[2]->AttributeValue
																    	],														    	
																    	[
																    		'AttributeCode' => 'ca_points',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[3]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_main_interest1',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[4]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_main_interest2',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[5]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_main_interest3',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[6]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi1_subinterest1',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[7]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi1_subinterest2',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[8]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi2_subinterest1',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[9]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi2_subinterest2',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[10]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi3_subinterest1',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[11]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi3_subinterest2',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[12]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_referral_code',
																    		'AttributeValue' => $this->input->post('referred_code') ? $this->input->post('referred_code') : '0' //$spice_userinfo->Derivations[12]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_personal_referral_code',
																    		'AttributeValue' => '0'
																    	],
																    	[
																    		'AttributeCode' => 'ca_total_km',
																    		'AttributeValue' => '0'
																    	]

															),
														'BrandPreferences'=>array(
																								    								


											    								'PrimaryBrand'=>array('BrandId'=>$this->input->post('current_brand')),
																				'SecondaryBrand'=>array('BrandId'=>$this->input->post('first_alternate_brand')),
																				'MultibrandSmoker'=>$MultibrandSmoker
																			),
														'LoginType'=>array(
										    						'EncryptedPassword' => $this->spice->encryptPassword(strtolower($this->input->post('email')),uniqid()),
																    'LoginName' => strtolower($this->input->post('email')),
																    'PasswordResetEmail' => strtolower($this->input->post('email'))
										    					),
														'TermsAndConditionsAcceptedList'=>array(
	 																					        'TermsAndConditionsCode' => 'TNC1',
																						        'TermsAndConditionsVersionNumber' => '2'
											    											),
														'MarketingPreference' => $marketingPreference,
														'IdDocumentNumber'=>$this->input->post('giid_number'),
				    									'IdDocumentTypeCode'=>$this->input->post('giid_type'),
				    									'VerificationImage'=>[
				    														'IdDocumentImage'=>"$imageString",
				    														'IdDocumentFileType'=>$IdDocumentFileType,
				    														'IdDocumentMarketCode'=>'PH',
				    														'IdDocumentTypeCode'=>$this->input->post('giid_type'),
				    														'IdDocumentCountryCode'=>'PH'
				    														],
				    									'VerificationInfo'=>[
				    														'VerificationStatus'=>'0',
				    														'VerificationTypeCode'=>'7',
				    														'VerificationSource'=>'0',
				    														'VerificationSubStatusCode'=>'0'
				    													],
				    									'RegistrationDate'	=> $date
												   			    									   
												)
							);
			

			$manageperson = $this->spice->ManagePerson($data);
			$response_json = $this->spice->parseJSON($manageperson);

			
			if(!$manageperson || $response_json->MessageResponseHeader->TransactionStatus != 14 ){

				$data = array('origin_id'=>REGISTRATION,
								'method'=>'ManagePerson',
								'transaction'=>'Upload now registration',
								'input'=>$this->session->userdata('spice_input'),
								'response'=>$manageperson,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
				$this->spice->saveError($data);

				$response = $manageperson ? $response_json->MessageResponseHeader->TransactionStatusMessage : 'Response is empty. Please try again.';
				$this->output->set_output('<br/>ERROR UPLOAD: '.ucfirst(strtolower($response)));
				return;

			}else{
				if(date('Ymd') >= 20150401){ 
					$this->load->model(array('profile_model'));

					$sec_settings = $this->profile_model->get_row(array('table' => 'tbl_section_settings', 'where' => 'name = "REGISTRATION"'));
					$cell_id = $sec_settings->setting_section_id;

					$act_settings = $this->profile_model->get_row(array('table' => 'tbl_activity_settings', 'where' => 'name = "REGISTRATION"'));
					$act_id = $act_settings->setting_activity_id;
					
					$name =  $this->input->post('fname').' '.$this->input->post('mname').' '.$this->input->post('lname');

					$param = array('PersonId' => $response_json->PersonId, 'CellId' => $cell_id, 'ActionList' =>array(array('ActivityId'   => $act_id, 'ActionValue' => $name )));

					// print_r($param);
					$response = $this->spice->trackActionAdmin($param);
					$response_json = $this->spice->parseJSON($response);

					if($response_json
						->MessageResponseHeader->TransactionStatus != 0){
							$data['message'] = '<h3>'.$response_json->MessageResponseHeader->TransactionStatusMessage.'</h3>';

							$data_error = array('origin_id'=> REGISTRATION,
									'method'=>'trackAction',
									'transaction'=>$this->router->method,
									'input'=> $this->session->userdata('spice_input'),
									'response'=>$response,
									'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
								);
							$this->spice->saveError($data_error);
					}
				}
			}
		 
	}


	public function upload_later(){

		$this->load->library(array('spice','encrypt'));

 		$city = $this->db->select()
			->from('tbl_cities')
			->where(array('city_id'=>$this->input->post('city')))
			->limit(1)
			->get()
			->row();

		$province = $this->db->select()
			->from('tbl_provinces')
			->where(array('province_id'=>$this->input->post('province')))
			->limit(1)
			->get()
			->row();

		$city = $city ? $city->city : '';
		$province = $province ? $province->province : '';

		$email_pre = '';
		$MultibrandSmoker = $this->input->post('current_brand') !== $this->input->post('first_alternate_brand') ? 'false' : 'true';

		$dateTime = DateTime::createFromFormat('Y-m-d H:i:s', $this->input->post('birthday').' 13:00:00');
		$dateTime->setTimeZone(new DateTimeZone('Asia/Manila'));
		$DateOfBirth = sprintf('/Date(%s%s)/',
							   $dateTime->format('U') * 1000,
							   $dateTime->format('O')
							);

		$data = array(  'ProfileType'=>'C',
						'ConsumerProfile'=> array( 'PersonalDetails'=>array(
																			'FirstName' => $this->input->post('fname'),
																		    'MiddleName'=> $this->input->post('mname'),
																		    'LastName' => $this->input->post('lname'),
																		    'Gender'=> $this->input->post('gender'),
 																		    'EmailAddress'=>strtolower($this->input->post('email')),
																		    'MobileTelephoneNumber'=>ltrim($this->input->post('mobile_prefix'), '0').$this->input->post('mobile_number'),
																		    'DateOfBirth'=>$DateOfBirth
																		    ),
													'AddressType'=>array(array( 
																    			'AddressLine1' => $this->input->post('street'),
																				'Area' => $this->input->post('brgy'),
																				'Locality' => $province,
																		        'City' => $city, 
																		        'Country' => 'PH',
																		        'Premise'=>'PH',
																		        'PostalCode' => $this->input->post('zip_code')
																		    )
															    ),
													'Derivations'=>array(
																		[
																    		'AttributeCode' => 'ca_nickname',
																    		'AttributeValue' =>  $this->input->post('nname')
																    	],
																    	[
																    		'AttributeCode' => 'ca_outletcode',
																    		'AttributeValue' =>  $this->input->post('outlet_code') ? $this->input->post('outlet_code') : '0' //$spice_userinfo->Derivations[1]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_reaction',
																    		'AttributeValue' =>  $this->input->post('alternate_purchase') //$spice_userinfo->Derivations[2]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_points',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[3]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_main_interest1',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[4]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_main_interest2',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[5]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_main_interest3',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[6]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi1_subinterest1',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[7]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi1_subinterest2',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[8]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi2_subinterest1',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[9]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi2_subinterest2',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[10]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi3_subinterest1',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[11]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi3_subinterest2',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[12]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_referral_code',
																    		'AttributeValue' => $this->input->post('referred_code') ? $this->input->post('referred_code') : '0'
																    	],
																    	[
																    		'AttributeCode' => 'ca_personal_referral_code',
																    		'AttributeValue' => '0'
																    	]

														),
													'BrandPreferences'=>array(
										    								'PrimaryBrand'=>array('BrandId'=>$this->input->post('current_brand')),
																			'SecondaryBrand'=>array('BrandId'=>$this->input->post('first_alternate_brand')),
																			'MultibrandSmoker'=>$MultibrandSmoker
																		),
													'LoginType'=>array(
									    						'EncryptedPassword' => $this->spice->encryptPassword(strtolower($this->input->post('email')),uniqid()),
															    'LoginName' => strtolower($this->input->post('email')),
															    'PasswordResetEmail' => strtolower($this->input->post('email'))
									    					),
													'TermsAndConditionsAcceptedList'=>array(
 																					        'TermsAndConditionsCode' => 'TNC1',
																					        'TermsAndConditionsVersionNumber' => '2'
										    											)
											   			    									
											)
						);
		$manageperson = $this->spice->ManagePerson($data);
		$response_json = $this->spice->parseJSON($manageperson);

		if(!$manageperson || $response_json->MessageResponseHeader->TransactionStatus != 14){

			$data = array('origin_id'=>REGISTRATION,
								'method'=>'ManagePerson',
								'transaction'=>'Upload later registration',
								'input'=>$this->session->userdata('spice_input'),
								'response'=>$manageperson,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
			$this->spice->saveError($data);

			$this->output->set_content_type('application/json')->set_output(json_encode(array('error'=>$response_json->MessageResponseHeader->TransactionStatusMessage,'success'=>'')));
			return;

		}

 
		$PersonId = $response_json->PersonId;
		// $dlTemplateId = 1028;
		// $cellId = 1394;
		$dlTemplateId = 1007;
		$cellId = 936;

		$login = $this->spice->createLoginToken($PersonId,$dlTemplateId, $cellId);
		$login_json = $this->spice->parseJSON($login);

		if($login_json->MessageResponseHeader->TransactionStatus!==0){

			$this->output->set_content_type('application/json')->set_output(json_encode(array('error'=>$login_json->MessageResponseHeader->TransactionStatusMessage,'success'=>'')));
			$data = array('origin_id'=>REGISTRATION,
								'method'=>'createLoginToken',
								'transaction'=>'Create login token failed for GIID upload later.',
								'input'=>$this->session->userdata('spice_input'),
								'response'=>$login,
								'response_message'=>$login_json->MessageResponseHeader->TransactionStatusMessage
							);
			$this->spice->saveError($data);
			return;

		}
 
 		$code = 'M00000213';
 		$base_url = str_replace(array('http://','https://'), '', BASE_URL);
		$base_url = $base_url.'user/validate_giid_upload_token'.'?token=' .$login_json->LoginToken;

		$params = ['Email' => strtolower($this->input->post('email')),
		            'MailingCode' => $code,
		            'IsSMTPMail' => false,
		            'Fields' => [
		            				['Name' => 'ph_campaignurlpersonal', "ValueDataType"=>"String", 'ValueAsString' => $base_url],
		                            ['Name' => 'ph_firstname', "ValueDataType"=>"String", 'ValueAsString' => $this->input->post('fname')],
		                            ['Name' => 'ph_lastname', "ValueDataType"=>"String", 'ValueAsString' => $this->input->post('lname')],
		                            ['Name' => 'ph_emailaddress', "ValueDataType"=>"String", 'ValueAsString' => $this->input->post('email')]
		                        ]
		            ];

	   $sendMailing = $this->spice->sendMailing($params);
	   $response_json = $this->spice->parseJSON($sendMailing);

	   if($response_json->MessageResponseHeader->TransactionStatus){ 	  

	   		$data = array('origin_id'=>REGISTRATION,
								'method'=>'sendMailing',
								'transaction'=>$this->router->method,
								'input'=>$this->session->userdata('spice_input'),
								'response'=>$sendMailing,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
			$this->spice->saveError($data);

 			$this->output->set_content_type('application/json')->set_output(json_encode(array('error'=>'Inputs:  <br/>'.$this->session->userdata('spice_input').'<br/><br/> Response: <br/>'.$sendMailing,'success'=>'')));

        } 

 
	}

	public function validate_refer_friend_token()
	{

		if($this->input->get('token'))
		{
			// print_r($this->input->get('token')); die();

			$tkn = $this->input->get('token');
			$email = $this->input->get('email');
			$referral_code = $this->input->get('referral_code');
			
			$token = $this->spice->validateLoginLoken($tkn);
			$token_json = $this->spice->parseJSON($token);
 
			if ($token_json->MessageResponseHeader->TransactionStatus == 0){				
				$successURL = $token_json->RedirectUrlParameters.'?token='.$tkn;

				$this->session->set_userdata('refer_friend', array('email_from_refer' => $email, 'referral_code_from_refer' => $referral_code));
				
				redirect($successURL);
			}else{

				$data_error = array('origin_id'=>REFERRAL,
						'method'=>'validateLoginToken',
						'transaction'=>$this->router->method,
						'input'=> $this->session->userdata('spice_input'),
						'response'=>$token,
						'response_message'=>$token_json->MessageResponseHeader->TransactionStatusMessage
					);

				$this->session->set_userdata('response_message_error', $token_json->MessageResponseHeader->TransactionStatusMessage);
				redirect('user/login');
			}
					
		}else{
			redirect('user/login?success=0');
		}


	}

	public function forgot()
	{
		$data = array();
		$data['invalid_forgot_email'] = $this->session->flashdata('invalid_forgot_email');

		// <<< IP FILTER >>>
		if($_SERVER['REMOTE_ADDR'] == '180.232.124.116') {
			$this->load->layout('user/forgot_firmfilter', $data, 'user/template_login_firmfilter');
		} else {
			$this->load->layout('user/forgot', $data, 'user/template');
		}
		// if(date('Ymd') >= 20150401) 
		// 	$this->load->layout('crossover/forgot', $data, 'crossover/template');
		// else
		// 	$this->load->layout('user/forgot', $data, 'user/template');
	}


	public function validate_reset_password_token()
	{

		if($this->input->get('token'))
		{
			$tkn = $this->input->get('token');
			
			$token = $this->spice->validateLoginLoken($tkn);
			$token_json = $this->spice->parseJSON($token);
		
 
			if ($token_json->MessageResponseHeader->TransactionStatus == 0){				
				$successURL = $token_json->RedirectUrlParameters.'&token='.$tkn;
				redirect($successURL);
				// echo $successURL." - 1";
			}else{

				$data_error = array('origin_id'=>RESET_PASSWORD,
						'method'=>'validateLoginToken',
						'transaction'=>$this->router->method,
						'input'=> $this->session->userdata('spice_input'),
						'response'=>$token,
						'response_message'=>$token_json->MessageResponseHeader->TransactionStatusMessage
					);

				$this->session->set_userdata('response_message_error', $token_json->MessageResponseHeader->TransactionStatusMessage);
				redirect('user/login');
			}
					
		}else{
			redirect('user/login?success=0');
		}


	}

	public function reset_password()
	{
		if($this->input->post() && $this->validate_resetpw()){

		
			$autologin = $this->spice->autoLogin($this->input->get('token'));
			$autologin_json = $this->spice->parseJSON($autologin);
			

			if($autologin_json->ResponseHeader->TransactionStatus !== 0){
				
				$this->session->set_userdata('response_message_error', $autologin_json->ResponseHeader->TransactionStatusMessage);
				redirect('user/login');
			}

			$personid = $autologin_json->PersonId;
			$sessionkey = $autologin_json->SessionKey;
			
			$spice_userinfo = $this->spice->GetPerson(array('PersonId' => $personid));
			$spice_userinfo = $this->spice->parseJSON($spice_userinfo);
			$spice_userinfo = ($spice_userinfo) ? $spice_userinfo->ConsumerProfiles : false;
			$email = strtolower($spice_userinfo->Login[0]->LoginName);
			
			$response = $this->spice->resetPassword(array('EmailAddress' => $email, 'NewPassword' => $this->input->post('new_password'), 'SessionKey' => $sessionkey ));
			$response_json = $this->spice->parseJSON($response);

			
			if($response_json->TransactionStatus != 0){

				$data_error = array('origin_id'=>RESET_PASSWORD,
						'method'=>'ChangePassword',
						'transaction'=>$this->router->method,
						'input'=> $this->session->userdata('spice_input'),
						'response'=>$response,
						'response_message'=>$response_json->TransactionStatusMessage
					);
				$this->spice->saveError($data_error);

				$this->session->set_userdata('response_message_error', ucfirst(strtolower($response_json->TransactionStatusMessage)));
				redirect('user/login');


			}else{

				redirect('user/login?success=1');

			} 


		}
		
 
		$autologin = $this->spice->autoLogin($this->input->get('token'));
		$autologin_json = $this->spice->parseJSON($autologin);

		if ($autologin_json->ResponseHeader->TransactionStatus == 0){

			$spice_userinfo = $this->spice->GetPerson(array('PersonId' =>  $autologin_json->PersonId ));
			$spice_userinfo = $this->spice->parseJSON($spice_userinfo);
		
			if ($spice_userinfo->MessageResponseHeader->TransactionStatus) {
				
				$this->session->set_userdata('response_message_error', $spice_userinfo->MessageResponseHeader->TransactionStatusMessage);
				redirect('user/login');
			}
			

		}else{

			$data_error = array('origin_id'=>RESET_PASSWORD,
					'method'=>'validateLoginToken',
					'transaction'=>$this->router->method,
					'input'=> $this->session->userdata('spice_input'),
					'response'=>$token,
					'response_message'=>$autologin_json->MessageResponseHeader->TransactionStatusMessage
				);
			$this->spice->saveError($data_error);
			
			$this->session->set_userdata('response_message_error', $autologin_json->MessageResponseHeader->TransactionStatusMessage);
			redirect('user/login');
		}		

		$data = array();
		$data['confirm_password'] = '';
		$data['user'] = $spice_userinfo->ConsumerProfiles;

		$this->load->layout('user/change_password', $data, 'user/template');
		// if(date('Ymd') >= 20150401) 
		// 	$this->load->layout('crossover/change_password', $data, 'crossover/template');
		// else
		// 	$this->load->layout('user/change_password', $data, 'user/template');
	}

	 

	public function validate_resetpw()
	{

		$this->load->library('form_validation');

		if($this->input->post('new_password') || $this->input->post('confirm_password')){
			$rules[] = array('field'=>'new_password','label'=>'New password','rules'=>'required|matches[confirm_password]|min_length[8]');
			$rules[] = array('field'=>'confirm_password','label'=>'Confirm New Password','rules'=>'required');

			$this->form_validation->set_rules($rules);
		}

		//$this->form_validation->set_error_delimiters('','');		
		return $this->form_validation->run();

	}

	public function tracking_code()
	{

		$data = array();
		$data['invalid'] = $this->session->userdata('invalid');
		$message =  $this->session->userdata('message');

		$this->session->set_userdata('invalid','');
		$this->session->set_userdata('message','');

		// <<< IP FILTER >>>
		if($_SERVER['REMOTE_ADDR'] == '180.232.124.116') {
			$layout = $message ? 'user/reset_success_firmfilter' : 'user/tracking_code_firmfilter';
		} else {
			$layout = $message ? 'user/reset_success' : 'user/tracking_code';
		}
		$data['success_message'] = $message;
		// <<< IP FILTER >>>
		if($_SERVER['REMOTE_ADDR'] == '180.232.124.116') {
			$this->load->layout($layout, $data, 'user/template_login_firmfilter');
		} else {
			$this->load->layout($layout, $data, 'user/template');
		}
		// if(date('Ymd') >= 20150401){ 
		// 	$layout = $message ? 'user/reset_success' : 'crossover/tracking_code';
		// 	$data['success_message'] = $message;
		// 	$this->load->layout($layout, $data, 'crossover/template');
		// }else{
		// 	$layout = $message ? 'user/reset_success' : 'user/tracking_code';
		// 	$data['success_message'] = $message;
		// 	$this->load->layout($layout, $data, 'user/template');
		// }
	}

	public function submit_track_code()
	{

		$email = $this->input->post('email');
		$submit_response = array('error'=>1,'message'=>'');


		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			
			$response =  $this->spice->GetPerson(array('LoginName'=>$email));
			$response_json = $this->spice->parseJSON($response);

			if (!$response_json->MessageResponseHeader->TransactionStatus) {

				if((int)$response_json->ConsumerProfiles->AgeVerificationStatus === 1){

 					echo json_encode(array('error'=>1,'message'=>'Your account has already been approved. You may now login to your account.'));
 					exit();
 
				}
				
				$ConsumerProfiles = $response_json->ConsumerProfiles;
				$ConsumerProfiles = $this->spice->parseJSON($this->spice->toJSONFormat((array)$ConsumerProfiles));	
 				$PersonId = $ConsumerProfiles->PersonId;

				// $dlTemplateId = 1028;
				// $cellId = 1394;
				$dlTemplateId = 1007;
				$cellId = 936;

				$login = $this->spice->createLoginToken($PersonId,$dlTemplateId, $cellId);
				$login_json = $this->spice->parseJSON($login);
			

				if($login_json->MessageResponseHeader->TransactionStatus===0){

					$code = 'M00000225';
 					$base_url = str_replace(array('http://','https://'), '', BASE_URL);
					$base_url = $base_url.'user/validate_giid_upload_token'.'?token=' .$login_json->LoginToken;

					$params = ['Email' => $email,
					            'MailingCode' => $code,
					            'IsSMTPMail' => false,
					            'Fields' => [   
					            				['Name' => 'ph_campaignurlpersonal', "ValueDataType"=>"String", 'ValueAsString' => $base_url],
					                            ['Name' => 'ph_firstname', "ValueDataType"=>"String", 'ValueAsString' => $ConsumerProfiles->PersonDetails->FirstName],
					                            ['Name' => 'ph_lastname', "ValueDataType"=>"String", 'ValueAsString' => $ConsumerProfiles->PersonDetails->LastName],
					                            ['Name' => 'ph_emailaddress', "ValueDataType"=>"String", 'ValueAsString' => $email]
					                        ]
					            ];



				    $sendMailing = $this->spice->sendMailing($params);
	 			    $response_json = $this->spice->parseJSON($sendMailing);

	 			   

	 			    if($response_json->MessageResponseHeader->TransactionStatus){

	 			    	$data = array('origin_id'=>REGISTRATION,
								'method'=>'sendMailing',
								'transaction'=>'Resend email of GIID upload',
								'input'=> $this->session->userdata('spice_input'),
								'response'=>$sendMailing,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
						$this->spice->saveError($data);

 	 			    	$submit_response['error'] = 1;
						$submit_response['message'] = $response_json->MessageResponseHeader->TransactionStatusMessage.'(sendMailing)';


	 			    }else{


	 			    	$submit_response['error'] = 0;
						$submit_response['message'] = 'Email has been sent for your GIID upload. Please don’t forget to submit a copy of a valid government-issued ID within seven (7) days. Your registration will only be processed upon submission of a valid government-issued ID.';


	 			    }			   

				}else{

					$data = array('origin_id'=>REGISTRATION,
								'method'=>'createLoginToken',
								'transaction'=>'Resend email for GIID upload',
								'input'=> $this->session->userdata('spice_input'),
								'response'=>$login,
								'response_message'=>$login_json->MessageResponseHeader->TransactionStatusMessage
							);
					$this->spice->saveError($data);

					$submit_response['error'] = 1;
					$submit_response['message'] =$login_json->MessageResponseHeader->TransactionStatusMessage.'(createLoginToken)';

				}
 				 

			} else {

 				$submit_response['error'] = 1;
				$submit_response['message'] = ucfirst(strtolower($response_json->MessageResponseHeader->TransactionStatusMessage));

				
			}

		}else{


			$submit_response['error'] = 1;
			$submit_response['message'] = ' The email address you entered does not exist. Please try again.';

		}

		$this->output->set_content_type('application/json')->set_output(json_encode($submit_response));

	 	//redirect('submit_giid');

	}

	public function validate_giid_upload_token(){

		$token = $this->input->get('token');

		$response = $this->spice->validateLoginLoken($token);
		$response_json = $this->spice->parseJSON($response);

	 
		if ($response_json->MessageResponseHeader->TransactionStatus) {

			$data = array('origin_id'=>REGISTRATION,
								'method'=>'validateLoginLoken',
								'transaction'=>$this->router->method,
								'input'=> $this->session->userdata('spice_input'),
								'response'=>$response,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
			$this->spice->saveError($data);

			$this->session->set_userdata('response_message_error', ucfirst(strtolower($response_json->MessageResponseHeader->TransactionStatusMessage)));
			redirect('user/login');

		
		}else{

			$redirect_url = $response_json->RedirectUrlParameters;
			redirect($redirect_url.$token);

		}

 	}

	public function giid_upload()
	{

		$token = $this->input->get('token');
		$data = array();
		$data['token'] = $token;
		$this->session->set_userdata('reg_token', $token);

		// <<< IP FILTER >>>
		if($_SERVER['REMOTE_ADDR'] == '180.232.124.116') {
			$this->load->layout('user/track_submit_firmfilter', $data, 'user/template_firmfilter');
		} else {
			$this->load->layout('user/track_submit', $data, 'user/template');
		}
		
		// if(date('Ymd') >= 20150401) 
		// 	$this->load->layout('crossover/track_submit', $data, 'crossover/template');
		// else
		// 	$this->load->layout('user/track_submit', $data, 'user/template');
		  		
	}

	
	public function submit_late_giid()
	{

		$this->load->helper('upload');

		if ($this->input->server('REQUEST_METHOD') != 'POST') {
			$this->output->set_output('Invalid request');
			return;
		}

		if($this->session->userdata('reg_token')!==$this->input->post('__token')){

			$this->output->set_output('Invalid request');
			return;

		}

		$token = $this->session->userdata('reg_token');
		$token = $this->spice->validateLoginLoken($token);
		$token_json = $this->spice->parseJSON($token);

 
		if ($token_json->MessageResponseHeader->TransactionStatus) {

			$data = array('origin_id'=>REGISTRATION,
								'method'=>'validateLoginLoken',
								'transaction'=>$this->router->method,
								'input'=> $this->session->userdata('spice_input'),
								'response'=>$token,
								'response_message'=>$token_json->MessageResponseHeader->TransactionStatusMessage
							);
			$this->spice->saveError($data);

			$this->session->set_userdata('response_message_error', $token_json->MessageResponseHeader->TransactionStatusMessage);
			redirect('user/login');
		}


		$imageString = $this->input->post('captured_image_ba');
		$path = './uploads/registration/';
		$fileTypes = array('.jpeg'=>1,'.png'=>2,'.jpg'=>3,'.bmp'=>4,'.pdf'=>5);	

		if(!$imageString){
 
			$filename = uniqid();
			$max_size = 2048;
			$params = array('upload_path'=>$path,'allowed_types'=>SPICE_FILE_UPLOAD_ALLOWED_TYPES,'file_name'=>$filename,'max_size'=>$max_size,'max_width'=>0,'max_height'=>0,'do_upload'=>'giid');
	    	$file = upload($params);

	    	if(is_array($file)){ 

	    		$imageString = base64_encode(file_get_contents($path.$file['file_name']));
				unlink($path.$file['file_name']);

	    	}else{

	    		$this->output->set_output('<br/>ERROR UPLOAD: '.$file);
	    		return;

	    	}

	    	$IdDocumentFileType = $fileTypes[$file['file_ext']];
			
		}else{

			
			if(!is_dir($path)){
				mkdir($path,0777,true);
			}
			$filename = uniqid().'.jpg';

			file_put_contents($path.$filename, gzuncompress(base64_decode($imageString)));
			$imageString = base64_encode(file_get_contents(BASE_URL.$path.$filename));
			unlink($path.$filename);

			$IdDocumentFileType = $fileTypes['.jpg'];

		}
		
		$updates = array('IdDocumentNumber'=>$this->input->post('giid_number'),
						'IdDocumentTypeCode'=>$this->input->post('giid_type'),
						'VerificationImage'=>[
												'IdDocumentImage'=>"$imageString",
												'IdDocumentFileType'=>$IdDocumentFileType,
												'IdDocumentMarketCode'=>'PH',
												'IdDocumentTypeCode'=>$this->input->post('giid_type'),
												'IdDocumentCountryCode'=>'PH'
											],
						'VerificationInfo'=>[
											'VerificationStatus'=>'0',
											'VerificationTypeCode'=>'7',
											'VerificationSource'=>'0',
											'VerificationSubStatusCode'=>'0'
										]
						);

		$response = $this->spice->updatePerson(array('person_id'=>$token_json->PersonId,'updates'=>$updates));
		$response_json = $this->spice->parseJSON($response);		 

 		
        if(!$response || $response_json->MessageResponseHeader->TransactionStatus != 5){

        	$data = array('origin_id'=>REGISTRATION,
								'method'=>'validateLoginLoken',
								'transaction'=>$this->router->method,
								'input'=> $this->session->userdata('spice_input'),
								'response'=>$response,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
			$this->spice->saveError($data);

 			$this->output->set_output('ERROR UPLOAD: '.$response);
 		}else{
			if(date('Ymd') >= 20150401){ 
				$this->load->model(array('profile_model'));

				$sec_settings = $this->profile_model->get_row(array('table' => 'tbl_section_settings', 'where' => 'name = "REGISTRATION"'));
				$cell_id = $sec_settings->setting_section_id;

				$act_settings = $this->profile_model->get_row(array('table' => 'tbl_activity_settings', 'where' => 'name = "REGISTRATION"'));
				$act_id = $act_settings->setting_activity_id;
				
				$resp =  $this->spice->GetPerson(array('PersonId'=> $token_json->PersonId));
				$resp_json = $this->spice->parseJSON($resp);
				$ConsumerProfiles = $resp_json->ConsumerProfiles;
				$ConsumerProfiles = $this->spice->parseJSON($this->spice->toJSONFormat((array)$ConsumerProfiles));	
		
				$name =  $ConsumerProfiles->PersonDetails->FirstName.' '.$ConsumerProfiles->PersonDetails->MiddleName.' '.$ConsumerProfiles->PersonDetails->LastName;

				// $param = array('CellId' => $cell_id, 'ActionList' =>array(array('ActivityId'   => $act_id, 'ActionValue' => $name, 'PersonId' => $ConsumerProfiles->PersonId )));


				$param = array('PersonId' => $ConsumerProfiles->PersonId, 'CellId' => $cell_id, 'ActionList' =>array(array('ActivityId'   => $act_id, 'ActionValue' => $name )));


				$response = $this->spice->trackActionAdmin($param);
				$response_json = $this->spice->parseJSON($response);

				if($response_json->MessageResponseHeader->TransactionStatus != 0){
						$data['message'] = '<h3>'.$response_json->MessageResponseHeader->TransactionStatusMessage.'</h3>';

						$data_error = array('origin_id'=> REGISTRATION,
								'method'=>'trackAction',
								'transaction'=>$this->router->method,
								'input'=> $this->session->userdata('spice_input'),
								'response'=>$response,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
						$this->spice->saveError($data_error);
				}
			}
		}
		 
	}

	public function register()
	{		

		if($this->input->get('token')){

			$response = $this->spice->validateLoginLoken($this->input->get('token'));
			$response_json = $this->spice->parseJSON($response);

			if ($response_json->MessageResponseHeader->TransactionStatus) {

				$this->session->set_userdata('response_message_error', ucfirst(strtolower($response_json->MessageResponseHeader->TransactionStatusMessage)));
				redirect('user/login');
				
			}

		}
		
		// generate preview code
		if(in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
			$db_hostname = 'localhost';
			$db_username = 'root';
			$db_password = '';
			$db_database = 'marlboro_preview';
			$db_active_record = '';
		}elseif(in_array($_SERVER['SERVER_ADDR'], array('216.185.103.30'))) {
			$db_hostname = '10.66.16.202';
			$db_username = 'crossover_prev';
			$db_password = 'A1fOSsAWhcpR8HeWy4jlWTTo7MBjvtad0kARkxMhiJAyoPX3mZ';
			$db_database = 'crossover_prev';
			$db_active_record = 'prod';
		}else{
			$db_hostname = 'localhost';
			$db_username = 'crossover_prev';
			$db_password = '4AK7iK9BLHe8K2UzQvbywCnTV3DHFKh5ItOLsR1w3WNHZB6S4y';
			$db_database = 'crossover_prev';
			$db_active_record = 'actual';
		}

		$config = array();
		$config['hostname'] = $db_hostname;
		$config['username'] = $db_username;
		$config['password'] = $db_password;
		$config['database'] = $db_database;
		$config['dbdriver'] = 'mysql';
		$config['dbprefix'] = '';
		$config['pconnect'] = TRUE;
		$config['db_debug'] = TRUE;
		$config['cache_on'] = FALSE;
		$config['cachedir'] = '';
		$config['char_set'] = 'utf8';
		$config['dbcollat'] = 'utf8_general_ci';
		$config['swap_pre'] = '';
		$config['autoinit'] = TRUE;
		$config['stricton'] = FALSE;

		$this->db = $this->load->database($config, TRUE);

		$login_code = $this->generate_login_code();

		$this->session->set_userdata('login_code', $login_code);

		$this->db->insert('tbl_code', array('code'=>$login_code, 'source'=>2));

		$this->db->close();
		$this->db = $this->load->database($db_active_record, TRUE);
		// generate preview code

 		$data = array();
		$data['token'] = generate_token();
		$this->load->model('Registration_Model');
		$this->session->set_userdata('reg_token', $data['token']);

		$data['mobile_prefixes'] = $this->Registration_Model->get_mobile_prefixes();
		$data['provinces'] = $this->Registration_Model->get_provinces();
		$data['alternate_purchase'] = $this->Registration_Model->get_alternate_purchase();
		$data['referral_code'] = $this->session->userdata('refer_friend')['referral_code_from_refer'];
		$data['email'] = $this->session->userdata('refer_friend')['email_from_refer'];

		// <<< IP FILTER >>>
		if($_SERVER['REMOTE_ADDR'] == '180.232.124.116') {
			$this->load->layout('user/steps', $data, 'user/template_firmfilter');
		} else {
			$this->load->layout('user/steps', $data, 'user/template');
		}

		/*if(date('Ymd') >= 20150401)
			$this->load->layout('crossover/steps', $data, 'crossover/template');
		else
			$this->load->layout('user/steps', $data, 'user/template');*/

	}

	private function generate_login_code()
	{
		if(in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
			$db_hostname = 'localhost';
			$db_username = 'root';
			$db_password = '';
			$db_database = 'marlboro_preview';
		}elseif(in_array($_SERVER['SERVER_ADDR'], array('216.185.103.30'))) {
			$db_hostname = '10.66.16.202';
			$db_username = 'crossover_prev';
			$db_password = 'A1fOSsAWhcpR8HeWy4jlWTTo7MBjvtad0kARkxMhiJAyoPX3mZ';
			$db_database = 'crossover_prev';
		}else{
			$db_hostname = 'localhost';
			$db_username = 'crossover_prev';
			$db_password = '4AK7iK9BLHe8K2UzQvbywCnTV3DHFKh5ItOLsR1w3WNHZB6S4y';
			$db_database = 'crossover_prev';
		}

		$config = array();
		$config['hostname'] = $db_hostname;
		$config['username'] = $db_username;
		$config['password'] = $db_password;
		$config['database'] = $db_database;
		$config['dbdriver'] = 'mysql';
		$config['dbprefix'] = '';
		$config['pconnect'] = TRUE;
		$config['db_debug'] = TRUE;
		$config['cache_on'] = FALSE;
		$config['cachedir'] = '';
		$config['char_set'] = 'utf8';
		$config['dbcollat'] = 'utf8_general_ci';
		$config['swap_pre'] = '';
		$config['autoinit'] = TRUE;
		$config['stricton'] = FALSE;

		$this->db = $this->load->database($config, TRUE);

		$chars = 'abcdefghijkmnopqrstuvwxyzABCEFGHJKLNPQRSTUVXYZ123456789';

		$str = '';
		$max = strlen($chars) - 1;

		for ($i=0; $i < 8 ; $i++) {
			$str .= $chars[mt_rand(0, $max)];
		}

		$is_existing = $this->db->select()->from('tbl_code')->where('code', $str)->get()->num_rows();

		if($is_existing) {
			return $this->generate_login_code();
		} else {
			return $str;
		}
	}

	public function get_brands(){

		$this->db->select('brand_name, brand_id');
		$this->db->order_by('brand_name');
		$this->db->where('consumer_flag', 'true');
 		$rows = $this->db->get('tbl_brands_spice');
 		$this->output->set_output(json_encode($rows->result_array()));
 		
	}

	public function get_giid_types(){

		$this->db->select('ClassName, ClassCode');
  		$this->db->order_by('ClassName');
 		$rows = $this->db->get('tbl_giid_types_spice');
 		$this->output->set_output(json_encode($rows->result_array()));

	}

	public function cities()
	{
		$this->load->model('Registration_Model');
		$provinceId = $this->input->get('province');
		if (!$provinceId) {
			return;
		}
		$cities = json_encode($this->Registration_Model->get_cities($provinceId));
		$this->output->set_content_type('application/json')->set_output($cities);
	}
	private function validate_offer_confirmation($registrant_id, $offer_id) {
		$offer = $this->db->select('*')->from('tbl_confirm_login_offers')
										->where('registrant_id', $registrant_id)
										->where('offer_id', $offer_id)->get()->row();
		if($offer){
			return true;
		}else{
			return false;
		}
	}
	public function autologin()
    {
        if($this->input->get('token'))
        {
            $tkn = $this->input->get('token');
                                 
            if($this->session->userdata('_spice_member_session_key')){

            	$token = $this->spice->validateLoginLoken($tkn);
				$token_json = $this->spice->parseJSON($token);
				
	 
				if ($token_json->MessageResponseHeader->TransactionStatus == 0){

					$id = $this->input->get('id');
					$code = $this->input->get('code');


					if($id){	                        		
                		redirect(STAGING . 'activities/'.$id);
                	}else if($code){	                        		
                		redirect(STAGING . '?pc='.$code);
                	}else{
						$successURL = $token_json->RedirectUrlParameters.'&token='.$tkn;
            			redirect($successURL);
            			// echo $successURL." - 2";
            		}
				}else{

					$data_error = array('origin_id'=>'50',
							'method'=>'validateLoginToken',
							'transaction'=>$this->router->method,
							'input'=> $this->session->userdata('spice_input'),
							'response'=>$token,
							'response_message'=>$token_json->MessageResponseHeader->TransactionStatusMessage
						);

					$this->session->set_userdata('response_message_error', $token_json->MessageResponseHeader->TransactionStatusMessage);
					redirect('user/login');
				}

            }else{
	  			$autologin = $this->spice->autoLogin($tkn);
	            $autologin_json = $this->spice->parseJSON($autologin);

	            if($autologin_json->ResponseHeader->TransactionStatus != 0){
	            	$data_error = array('origin_id'=> AUTOLOGIN,
	                    'method'=>'autoLogin',
	                    'transaction'=>$this->router->method,
	                    'input'=> $this->session->userdata('spice_input'),
	                    'response'=>$tkn,
	                    'response_message'=>$autologin_json->MessageResponseHeader->TransactionStatusMessage
	                );

	                $this->spice->saveError($data);

	                $this->session->set_userdata('response_message_error',$autologin_json->ResponseHeader->TransactionStatusMessage);

	                redirect('user/login');           
	            }else{
	            	$this->load->model('Notification_Model');
	            	$this->load->model('Registration_Model');
	            	$this->spice->setCIAutoLoginMember($autologin_json->SessionKey, $autologin_json->PersonId);

	            	if($autologin_json->SessionKey != '' && (int)$autologin_json->PersonId)
	            	{

	            		session_regenerate_id();

	                    $response = $this->spice->GetPerson(array('PersonId'=>$autologin_json->PersonId));
	                    $response = $this->spice->parseJSON($response);

	                    $RegistrationDate = $this->spice->extractSpiceDate($response->ConsumerProfiles->RegistrationDate);
	                    
	                    $AgeVerificationStatus = $response->ConsumerProfiles->AgeVerificationStatus;
	     
	                    if(!$AgeVerificationStatus)
	                    {
	                        $this->session->set_userdata('invalid_login_message', 'Your Age Verification status has not been verified.');
	                        redirect('user/login');
	                    }                           

	                    $ConsumerProfiles = $response->ConsumerProfiles;
	                    $ConsumerProfiles = $this->spice->parseJSON($this->spice->toJSONFormat((array)$ConsumerProfiles));
	                    
	                    $total_derivations = count($ConsumerProfiles->Derivations);

	                    for($i = 0; $i<$total_derivations; $i++){
	                        if($ConsumerProfiles->Derivations[$i]->AttributeCode=='ca_referral_code'){
	                            $Derivations_referral = $ConsumerProfiles->Derivations[$i];
	                        }
	                        // changes
	                        // }else{
	                        //  $Derivations_referral = '';
	                        // }
	                        // changes
	                    }

	                    $Derivations_referral = $Derivations_referral ? $Derivations_referral : '';

	                    // $Derivations_referral = $ConsumerProfiles->Derivations[REFERRAL_CODE];
	                    
	                    $is_registered =  $this->db->select()->from('tbl_registrants')->where('person_id',$ConsumerProfiles->PersonId)->get()->row();

	                    $province = strtolower($ConsumerProfiles->Addresses[0]->Locality);
	                    $city = strtolower($ConsumerProfiles->Addresses[0]->City);
	                    $street_name = $ConsumerProfiles->Addresses[0]->AddressLine1;
	                    $barangay = $ConsumerProfiles->Addresses[0]->Area;
	                    $zip_code = $ConsumerProfiles->Addresses[0]->PostalCode;

	                    $date_of_birth = $this->spice->extractSpiceDate($ConsumerProfiles->PersonDetails->DateOfBirth);
	                    
	                    $province = $this->db->select()->from('tbl_provinces')->where('LOWER(province)',$province)->get()->row();
	                    $province = $province ? $province->province_id : '';

	                    $city = $this->db->select()->from('tbl_cities')->where(array('LOWER(city) '=>$city,'province_id'=>$province))->get()->row();
	                    $city = $city ? $city->city_id : '';
	                    $nick_name = '';
	                    $outlet_code = '';
	                    // $referral_code = $this->Registration_Model->referral_code();
	                    $Derivations = array();

	                    if($ConsumerProfiles->Derivations){

	                        $total_derivations = count($ConsumerProfiles->Derivations);
	                            
	                        for($i = 0; $i<$total_derivations; $i++){
	                            if($ConsumerProfiles->Derivations[$i]->AttributeCode=='ca_nickname'){
	                                $nick_name = $ConsumerProfiles->Derivations[$i]->AttributeValue;
	                            }

	                            if($ConsumerProfiles->Derivations[$i]->AttributeCode=='ca_outletcode'){
	                                $outlet_code = $ConsumerProfiles->Derivations[$i]->AttributeValue;
	                            }

	                            if($ConsumerProfiles->Derivations[$i]->AttributeCode=='ca_personal_referral_code'){
	                                $referral_code = $ConsumerProfiles->Derivations[$i]->AttributeValue ? $ConsumerProfiles->Derivations[$i]->AttributeValue : $this->Registration_Model->referral_code();
	                                // changes
	                                if( ! $ConsumerProfiles->Derivations[$i]->AttributeValue || $ConsumerProfiles->Derivations[$i]->AttributeValue == '0') {
	                                    $ConsumerProfiles->Derivations[$i]->AttributeValue = $referral_code;
	                                }
	                                // changes
	                            }else{
	                                //$ConsumerProfiles->Derivations[$i]->AttributeValue = '0';
	                            }
	                        }

	                        // $nick_name = $ConsumerProfiles->Derivations[NICKNAME]->AttributeValue;
	                        // $outlet_code = $ConsumerProfiles->Derivations[OUTLET_CODE]->AttributeValue;

	                        $Derivations = json_decode(json_encode($ConsumerProfiles->Derivations),true);
	                    }

	                    
	                    if( ! $ConsumerProfiles->Derivations[$i]->AttributeValue || $ConsumerProfiles->Derivations[$i]->AttributeValue == '0') {
	                        $has_referral = $this->db->select('referral_code')->from('tbl_registrants')->where('person_id', (string)$ConsumerProfiles->PersonId)->get()->row();
	                        if($has_referral->referral_code) {
	                            $insert_new_user = array('person_id'=>(string)$ConsumerProfiles->PersonId,
	                                         'first_name'=>(string)$ConsumerProfiles->PersonDetails->FirstName,
	                                         'third_name'=>(string)$ConsumerProfiles->PersonDetails->LastName,
	                                         'middle_initial'=>(string)$ConsumerProfiles->PersonDetails->MiddleName,
	                                         'nick_name'=>$nick_name,
	                                         'outlet_code'=>$outlet_code,
	                                         'referred_by_code'=> ($Derivations_referral->AttributeValue && $Derivations_referral->AttributeCode==='ca_referral_code') ? $Derivations_referral->AttributeValue : ''
	                                            );
	                        } else {
	                            $insert_new_user = array('person_id'=>(string)$ConsumerProfiles->PersonId,
	                                         'first_name'=>(string)$ConsumerProfiles->PersonDetails->FirstName,
	                                         'third_name'=>(string)$ConsumerProfiles->PersonDetails->LastName,
	                                         'middle_initial'=>(string)$ConsumerProfiles->PersonDetails->MiddleName,
	                                         'nick_name'=>$nick_name,
	                                         'outlet_code'=>$outlet_code,
	                                         'referral_code' => (string) $referral_code,
	                                         'referred_by_code'=> ($Derivations_referral->AttributeValue && $Derivations_referral->AttributeCode==='ca_referral_code') ? $Derivations_referral->AttributeValue : ''
	                                            );
	                        }                   
	                    } else {
	                        $insert_new_user = array('person_id'=>(string)$ConsumerProfiles->PersonId,
	                                         'first_name'=>(string)$ConsumerProfiles->PersonDetails->FirstName,
	                                         'third_name'=>(string)$ConsumerProfiles->PersonDetails->LastName,
	                                         'middle_initial'=>(string)$ConsumerProfiles->PersonDetails->MiddleName,
	                                         'nick_name'=>$nick_name,
	                                         'outlet_code'=>$outlet_code,
	                                         'referred_by_code'=> ($Derivations_referral->AttributeValue && $Derivations_referral->AttributeCode==='ca_referral_code') ? $Derivations_referral->AttributeValue : ''
	                                            );
	                    }
	                 
	                    if($is_registered){

	                        $this->db->where('person_id', $ConsumerProfiles->PersonId);
	                        $this->db->update('tbl_registrants', $insert_new_user); 

	                    }else{
	                        $this->db->set('date_created',"'$RegistrationDate'",FALSE);
	                        $this->db->insert('tbl_registrants', $insert_new_user);

	                        
	                        $params = array('person_id'=> $ConsumerProfiles->PersonId,
	                                        'updates'=> array('Derivations' => $Derivations)
	                                    );

	                        $response = $this->spice->updatePerson($params);
	                        $response_json = $this->spice->parseJSON($response);

	                    }

	                    $registrant = $this->db->select()->from('tbl_registrants')->where('person_id',$ConsumerProfiles->PersonId)->get()->row();       
	                 
	                    $user = array('login_attempts'=>$registrant->login_attempts,
	                                  'date_blocked'=>$registrant->date_blocked,
	                                  'person_id'=>$ConsumerProfiles->PersonId,
	                                  'registrant_id'=>$registrant->registrant_id,
	                                  'referred_by_code'=>$registrant->referred_by_code,
	                                  'registrant_id'=>$registrant->registrant_id,
	                                  'first_name'=>$ConsumerProfiles->PersonDetails->FirstName,
	                                  'third_name'=>$ConsumerProfiles->PersonDetails->LastName,
	                                  'middle_initial'=>$ConsumerProfiles->PersonDetails->MiddleName,
	                                  'date_of_birth'=>$ConsumerProfiles->PersonDetails->DateOfBirth,
	                                  'nick_name'=>$registrant->nick_name,
	                                  'mobile_phone'=>$ConsumerProfiles->PersonDetails->MobileTelephoneNumber,
	                                  'referral_code'=>$registrant->referral_code,
	                                  'email_address'=>$ConsumerProfiles->Login[0]->LoginName,
	                                  'total_points'=>$registrant->total_points,
	                                  'status'=>$ConsumerProfiles->AgeVerificationStatus,
	                                  'is_cm'=>$registrant->is_cm
	                              );                    

	                    $this->load->library('Encrypt');

	                    $today = date('Y-m-d H:i:s');                

	                    // check if from referral;
	                    $isFirstLogin = false;

	                    if($user['referred_by_code']){
	     
	                        $referer = $this->Registration_Model->get_user_by_refCode($user['referred_by_code']);
	                        if($referer){
	                            $isFirstLogin = $this->Registration_Model->isFirstLogin($user['registrant_id']);
	                        }
	                        
	                    }
	     
	                    $this->db->insert('tbl_login', array(
	                        'registrant_id' => $user['registrant_id'],
	                        'date_login' => $today
	                    ));

	                    if ($this->db->affected_rows()) 
	                    {
	                        $this->load->model(array('profile_model','Points_Model'));

	                        $this->session->set_userdata('user', array(
	                            'first_name' => $user['first_name'],
	                            'last_name' => $user['third_name'],
	                            'middle_initial' => $user['middle_initial'],
	                            'birthdate' => $user['date_of_birth'],
	                            'nick_name' => $user['nick_name'],
	                            'mobile_num' => $user['mobile_phone'],
	                            'referral_code' => $user['referral_code'],
	                            'person_id'=> $user['person_id'],
	                            'email' => $user['email_address']
	                        ));

	                        $this->session->set_userdata('check_offers', 1);
	                        $this->session->set_userdata('user_points', $user['total_points']);
	                        $this->session->set_userdata('user_id', $user['registrant_id']);
	                        $this->session->set_userdata('login_id', $this->db->insert_id());
	                        $this->session->set_userdata('is_cm', $user['is_cm']);
	                        $this->session->set_userdata('flash_offer_sess', 1);
	                        $this->session->set_userdata('app_id', APP_ID);


	                        $this->db->where('registrant_id', $user['registrant_id']);
	                        $this->db->update('tbl_registrants', array(
	                            'login_attempts' => 0,
	                            'date_blocked' => null,
	                            'login_id' => $this->session->userdata('login_id'),
	                            'app_id' => $this->session->userdata('app_id')
	                        ));

	                        if(!$isFirstLogin)
	                        {

	                            // if(date('Ymd') >= 20150401){
	                            //     $res = $this->Points_Model->earn_login_km(5, array(
	                            //         'date' => $today
	                            //     ));
	                            // }else{
	                            //     $this->Points_Model->earn(LOGIN, array(
	                            //         'date' => $today,
	                            //         'suborigin' => $this->session->userdata('login_id')
	                            //     ));
	                            // }


	                           /*LOGIN PROMO AUTOLOGIN*/
	                           //get last login
								$registrant_data = $this->db->select('*')->from('tbl_registrants')
																->where('registrant_id', $user['registrant_id'])
																->get()->row();

								$login = $this->db->select('*')->from('tbl_login')
														->where('registrant_id', $user['registrant_id'])
														->order_by('date_login', 'DESC')
														->limit(2)
														->get()->result();
								
								$last_login = date_create($registrant_data->last_login);
								$last_login_formatted = date_format($last_login, "Y/m/d");
								$today = date('Y/m/d');

								$lastlogintimestamp = strtotime($last_login_formatted);
								$todaytimestamp = strtotime($today);

								$timeDiff = abs($todaytimestamp - $lastlogintimestamp);
								$numberDays = $timeDiff/86400;

								$numberDays = intval($numberDays);


								$login_promo = $this->db->select('*')->from('tbl_login_offers')->where('is_active', 1)->order_by('date_created', 'desc')->get()->row();
								if(!$this->validate_offer_confirmation($registrant_data->registrant_id, $login_promo->id)){
									if($numberDays>=$login_promo->days_inactive){
										$today = date('Y-m-d');
										if((str_replace("-", "", $today)>=str_replace("-", "", $login_promo->start_date)) and (str_replace("-", "", $today)<=str_replace("-", "", $login_promo->end_date))){
											// echo "granted";
											// echo $login_promo->days_inactive."<br />";
											// echo $numberDays;
											// exit;
											$this->session->set_userdata('login_promo', 1);
											$this->session->set_userdata('last_login', $login[1]->date_login);

											$this->session->set_userdata('stat', "granted");
										}else{
											// echo "nd pasok ung date range";
											// exit;
											$this->session->set_userdata('login_promo', 0);
											$this->session->set_userdata('last_login', $login[1]->date_login);

											$this->session->set_userdata('stat', "nd pasok ung date range");
										}
									}else{
										// echo "nd pasok ung required days of login mo";
										// exit;
										$this->session->set_userdata('login_promo', 0);
										$this->session->set_userdata('last_login', $login[1]->date_login);

										$this->session->set_userdata('stat', "nd pasok ung required days of login mo");
									}
								}else{
									$this->session->set_userdata('login_promo', 0);
									$this->session->set_userdata('last_login', $login[1]->date_login);
									
									$this->session->set_userdata('stat', "claimed ka na!");
								}

								//END LOGIN PROMO AUTOLOGIN

	                           $this->Points_Model->earn(LOGIN, array(
                                    'date' => $today,
                                    'suborigin' => $this->session->userdata('login_id')
                                ));
	         
	                            $this->db->close();
	                            $this->db->where('registrant_id', $user['registrant_id']);
	                            $this->db->update('tbl_registrants', array(
	                                'last_login' => $today
	                            ));

	                            if(!$user['referred_by_code']){
	                                $this->Points_Model->earn(REGISTRATION);                             
	                            } else {
						$valid_refer_code = $this->Registration_Model->get_user_by_refCode($user['referred_by_code']);
						if( ! $valid_refer_code) {
	                                $this->Points_Model->earn(REGISTRATION);                             
						}
	                            }
	                            
	                            //SPICE INSERT DERIVATIONS
	                            $response = $this->spice->GetPerson(array('PersonId' =>$this->spice->getMemberPersonId()));
	                            $spice_userinfo = $this->spice->parseJSON($response);
	                            $spice_userinfo = $spice_userinfo->ConsumerProfiles;

	                            $consumer_attributes = $this->profile_model->get_rows(array('table'=>'tbl_consumer_attr_spice'))->result_array();
	                            
	                            $total_derivations = count($spice_userinfo->Derivations);
	                            $total_address = count($spice_userinfo->Addresses);
	                            $total_cas = count($consumer_attributes);
	                            $derivations = array();
	                            $var = array();
	                            $country = 'PH';
	                            
	                            // print_r($spice_userinfo); die();
	                            for($i = 0; $i<$total_cas; $i++){       
	                                                        
	                                $var = array('AttributeCode'=> $consumer_attributes[$i]['attribute_code'],'AttributeValue'=> '0');

	                                for($j = 0; $j<$total_derivations; $j++){
	                                    if($consumer_attributes[$i]['attribute_code'] == $spice_userinfo->Derivations[$j]->AttributeCode){
	                                        if($spice_userinfo->Derivations[$j]->AttributeValue == ''){
	                                            $var = array('AttributeCode'=> $consumer_attributes[$i]['attribute_code'],'AttributeValue'=> '0');
	                                            break;
	                                        }else{
	                                            $var = array('AttributeCode'=> $spice_userinfo->Derivations[$j]->AttributeCode,'AttributeValue'=> $spice_userinfo->Derivations[$j]->AttributeValue);
	                                            break;
	                                        }
	                                    }else{
	                                        $var = array('AttributeCode'=> $consumer_attributes[$i]['attribute_code'],'AttributeValue'=> '0');
	                                        break;
	                                    }
	                                }
	                                
	                                $derivations[] = $var;

	                            }


	                            if($total_address != 0){
	                                if($spice_userinfo->Addresses[0]->Country == ''){
	                                    $country = 'PH';
	                                }

	                                $params = array('person_id'=> $this->spice->getMemberPersonId(),
	                                                'updates'=> array(
	                                                    'AddressType' => array(
	                                                        array(
	                                                            'AddressLine1' => $spice_userinfo->Addresses[0]->AddressLine1,
	                                                            'Area' => $spice_userinfo->Addresses[0]->Area,
	                                                            'Locality' => $spice_userinfo->Addresses[0]->Locality,
	                                                            'City' => $spice_userinfo->Addresses[0]->City, 
	                                                            'Country' => $country,
	                                                            'Premise'=> $country,
	                                                            'PostalCode' => $spice_userinfo->Addresses[0]->PostalCode
	                                                        )
	                                                    ),
	                                                    'Derivations'=> $derivations,
	                                            )
	                                );
	                            }else{
	                                    $params = array('person_id'=> $this->spice->getMemberPersonId(),
	                                                'updates'=> array(
	                                                    'Derivations'=> $derivations,
	                                            )
	                                    );
	                            }

	                            $response = $this->spice->updatePerson($params);
	                            $response_json = $this->spice->parseJSON($response);


	                            if ($response_json->MessageResponseHeader->TransactionStatus != 5) {
	                                
	                                $data_error = array('origin_id'=>LOGIN,
	                                        'method'=>'ManagePerson',
	                                        'transaction'=> 'update person derivations',
	                                        'input'=> $this->session->userdata('spice_input'),
	                                        'response'=>$response,
	                                        'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
	                                    );
	                                $this->spice->saveError($data_error);

	                                $this->session->set_userdata('response_message_error', ucfirst(strtolower($response_json->MessageResponseHeader->TransactionStatusMessage)));
	                                redirect('user/login');
	                            }
	                        
	                        }else{
	                            $this->session->set_userdata('flash_referral_offer_sess',1);

	                            $this->db->where('registrant_id', $user['registrant_id']);
	                            $this->db->update('tbl_registrants', array(
	                                'last_login' => $today
	                            ));

	                            /// Insert spice data to website db                             
	                            
	                            $this->Points_Model->earn(
	                                REFEREE_FIRSTLOGIN,
	                                array(
	                                    'registrant' => $user['registrant_id'],
	                                    'suborigin' => $this->session->userdata('login_id'),
	                                    'remarks' => 'first login as LAS2'
	                                ));

	                            $this->Points_Model->earn(
	                                REFERER_FIRSTLOGIN,
	                                array(
	                                    'registrant' => $referer['registrant_id'],
	                                    'suborigin' => $this->session->userdata('login_id'),
	                                    'remarks' => 'LAS2 ('. $user['registrant_id'] .') logged-in'
	                                ));

	                            $notifyLas2 = "Welcome to the MARLBORO community! Here’s ".REFEREE_FIRST_LOGIN_POINT." points to get you started. ".
	                                        "Discover the many exciting offers you can redeem on PERKS and continue visiting".
	                                        " the website to get more points. And while you’re at it, why don’t you invite".
	                                        " your adult smoker friends as well?";

	                            $this->Notification_Model->notify($user['registrant_id'], REFEREE_FIRSTLOGIN, 
	                                array('message' => $notifyLas2,'suborigin'=>0));

	                            $notifyLas1First = "Congratulations! Your friend, ".$user['first_name']." ".$user['third_name'].", ".
	                                "is now part of the MARLBORO community! You get 100 points ".
	                                "for the successful referral. Don't forget to invite ".$user['nick_name']." ".
	                                "to log in and experience the exclusive PERKS in the website.";

	                            $this->Notification_Model->notify($referer['registrant_id'], REFERER_FIRSTLOGIN,
	                                array('message' => $notifyLas1First,'suborigin'=>0));

	                            $notifyLas1 = "Congratulations! You get 200 more points! Your friend, ".
	                                $user['first_name']." ".$user['third_name'].", has just logged in to MARLBORO.PH.".
	                                " Invite your friends to be active members of the community,".
	                                " and get more points to win freebies.";

	                            // $notifyLas1 = "Congratulations! You get ".REFERER_FIRST_LOGIN_POINT." more points! Your friend, ".
	                            //  $user['first_name']." ".$user['third_name'].", has just logged in to MARLBORO.PH.".
	                            //  " Invite your friends to be active members of the community,".
	                            //  " and get more points to win freebies.";
	                            
	                            $this->Notification_Model->notify($referer['registrant_id'], REFERER_FIRSTLOGIN, 
	                                array('message' => $notifyLas1,'suborigin'=>0));

	                        }
	                        

	                        $user_id = $this->session->userdata('user_id');
	                        $hidden_packs = $this->profile_model->get_rows(array('table'=>'tbl_hidden_packs',
	                                                                    'where'=>array("CURDATE() >= DATE(start_date) AND CURDATE() <= DATE(end_date) AND is_deleted = '0' AND hidden_pack_id NOT IN(SELECT hidden_pack_id FROM tbl_hidden_pack_logs WHERE registrant_id = '$user_id' AND DATE(date_added) = CURDATE())"=>null)));

	                        if($hidden_packs->num_rows()){

	                            $pages = '';
	                            $temp = array();

	                            foreach($hidden_packs->result() as $r){

	                                $pages = $r->page ? explode(',',$r->page) : false;

	                                if($pages){

	                                    $key = rand(0,count($pages) - 1);
	                                    $temp = array('id'=>$r->hidden_pack_id,'image'=>BASE_URL.'uploads/hidden_packs/'.$r->image);
	                                    $this->session->set_userdata($pages[$key],$temp); 
	                                }

	                            } // End foreach

	                        }
	                        $id = $this->input->get('id');
	                        $code = $this->input->get('code');

	                        if(date('Ymd') >= 20140725)  {

	                           /* if($this->session->userdata('edm_popup')) {
	                                redirect(BASE_URL . 'edm?' . $this->session->userdata('edm_popup'));
	                            } else {
	                                //redirect(BASE_URL . 'premium');   
	                                redirect(BASE_URL . 'edm?' . $this->session->userdata('edm_popup'));
	                            }
	                             redirect(BASE_URL);*/ //DBAM 
	                        	
	                        	if($id){
	                        		redirect(STAGING . 'activities/'.$id);
	                        	}else if($code){	                        		
			                		redirect(STAGING . '?pc='.$code);
			                	}else{
									$successURL = $autologin_json->RedirectUrlParameters.'&token='.$tkn;
		                			redirect($successURL);
		                			// echo $successURL." - 4";
		                		}
	                        }else{
	                            //redirect(BASE_URL); //DBAM
	                            
	                            if($id){
	                        		redirect(STAGING . 'activities/'.$id);
	                        	}else if($code){	                        		
			                		redirect(STAGING . '?pc='.$code);
			                	}else{
									$successURL = $autologin_json->RedirectUrlParameters.'&token='.$tkn;
		                			redirect($successURL);
		                			// echo $successURL." - 3";

		                		}
	                        }

	                    }
					}	               
	            } 
	        }          
        }else{
            redirect('user/login?success=0');
        }
    }
}


