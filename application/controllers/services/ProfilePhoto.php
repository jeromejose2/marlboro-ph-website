<?php

class ProfilePhoto extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('profile_model');
    }

    public function submit_photo($jpg,$data)
    {  

    	 

    	$this->load->library('encrypt');
    	$id = $this->encrypt->decode($data->token); // $this->session->userdata('user_id');
    	$row = $this->profile_model->get_row(array('table'=>'tbl_registrants',
    												'where'=>array('registrant_id'=>$id)
    												)
    										);
 		
		if($row){
			
			$jpg = $jpg->data;
			$jpg = gzuncompress($jpg);
			$filename = md5($id.uniqid()).'_new.jpg';
 			$put_jpg = file_put_contents('uploads/profile/'.$id.'/'.$filename , $jpg) or trigger_error("Unable to create jpeg file.");
 
 	   		$this->profile_model->update('tbl_registrants',
 	   									array('new_picture'=>$filename,'picture_status'=>0,'date_modified'=>1),
 	   									array('registrant_id'=>$id)
 	   									);
			$response = 'Success';

		}else{
			$response = 'failed to upload';
		}
		
		return $response;	 			

    }

    public function test($jpg, $data)
    {
    	return $jpg;
    }

	
}