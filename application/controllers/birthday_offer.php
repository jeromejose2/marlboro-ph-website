<?php if ( !defined( "BASEPATH" ) ) { exit( "No direct script access allowed!" ); }

class Birthday_offer extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model( 'profile_model' );
	}

	public function get_birthday_offer() {
		$prize_id = $this->input->get( 'prize_id' );

		$user = $this->session->userdata( 'user' );
		$data['flash_offer'] = $this->input->get( 'flash_offer' );

		$spice_userinfo = $this->spice->GetPerson( array( 'PersonId' =>$this->spice->getMemberPersonId() ) );
		$spice_userinfo = $this->spice->parseJSON( $spice_userinfo );

		$date_time = $spice_userinfo && (int)$spice_userinfo->MessageResponseHeader->TransactionStatus == 0 ?  $spice_userinfo->ConsumerProfiles->PersonDetails->DateOfBirth : '';

		$match = preg_match( '/\/Date\((\d+)([-+])(\d+)\)\//', $date_time, $date );

		$timestamp = $date[1]/1000;
		$operator = $date[2];
		$hours = $date[3]*36; // Get the seconds

		$datetime = new DateTime();

		$datetime->setTimestamp( $timestamp );
		$datetime->modify( $operator . $hours . ' seconds' );

		$data['birthday_offer'] = $this->profile_model->get_row( array( 'table'=>'tbl_birthday_offers',
			 'where'=>array("month = '".date('m',strtotime($datetime->format('Y-m-d')))."' AND month ='".date('m')."'" =>null,
					'stock >'=>0,
					'status'=>1,
					'is_deleted'=>0,
					'prize_id' =>$prize_id
				)
			)
		);


		// print_r($data['birthday_offer']); die();

		$this->load->view( 'birthday_offer/birthday-offer', $data );
	}
	//http://marlboro-stage2.yellowhub.com/spice/birthday_offer/confirm_offer?flash_offer=
	//confirm_redemption  =   1
	public function confirm_offer() {

		if ( $this->input->post() ) {
			$validate_bday = false;
			if ( $this->validate_offer_confirmation() ) {
				$validate_bday = true;
				$user_id = $this->session->userdata( 'user_id' );
				$user = $this->session->userdata( 'user' );



				$spice_userinfo = $this->spice->GetPerson( array( 'PersonId' =>$this->spice->getMemberPersonId() ) );

				if ( !$spice_userinfo ) {
					//error message
					$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
					$data['msg'] = '<h3>Session expired. Please try again.</h3>';
					$this->load->view( 'profile/submit-response', $data );
					return;
				}

				$spice_userinfo = $this->spice->parseJSON( $spice_userinfo );
				$spice_userinfo = $spice_userinfo->ConsumerProfiles;

				$date_time = $spice_userinfo->PersonDetails->DateOfBirth;


				$match = preg_match( '/\/Date\((\d+)([-+])(\d+)\)\//', $date_time, $date );

				$timestamp = $date[1]/1000;
				$operator = $date[2];
				$hours = $date[3]*36; // Get the seconds

				$datetime = new DateTime();

				$datetime->setTimestamp( $timestamp );
				$datetime->modify( $operator . $hours . ' seconds' );


				if ( !(bool)strtotime( $datetime->format( 'Y-m-d' ) ) ) {
					//error message
					$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
					$data['msg'] = '<h3>Session expired. Please try again.</h3>';
					$this->load->view( 'profile/submit-response', $data );
					return;
				}

				$birthday_offer_stock = $this->profile_model->get_row( array( 'table'=>'tbl_birthday_offers',
						'where'=>array( 'month' =>date( 'm', strtotime( $datetime->format( 'Y-m-d' ) ) ),
							'month'=>date( 'm' ),
							'is_deleted'=>0
						),
						'fields'=>'prize_id'
					)
				);

				if($birthday_offer_stock) {
					if($birthday_offer_stock->stock <= 0 ){
						//error message
						$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
						$data['msg'] = '<h3>Sorry, this offer is out of stock.</h3>';
						$this->load->view( 'profile/submit-response', $data );
						return;
					}
				}

				$birthday_offer = $this->profile_model->get_row( array( 'table'=>'tbl_birthday_offers',
						'where'=>array( 'month' =>date( 'm', strtotime( $datetime->format( 'Y-m-d' ) ) ),
							'month'=>date( 'm' ),
							'stock >'=>0,
							'is_deleted'=>0
						),
						'fields'=>'prize_id'
					)
				);

				if ( (!$birthday_offer && !(int)$this->session->userdata( 'user_id' )) || !$birthday_offer ) {
					//error message
					$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
					$data['msg'] = '<h3>No available birthday offer at this time.</h3>';
					$this->load->view( 'profile/submit-response', $data );
					return;
				}else {
					$user_birthday_prize = $this->profile_model->get_row( array( 'table'=>'tbl_user_birthday_prizes',
							'where'=>array( 'prize_id' =>$birthday_offer->prize_id, 'registrant_id' =>$this->session->userdata( 'user_id' ) ),
							'fields'=>'user_birthday_prize_id'
						)
					);
					if ( $user_birthday_prize ) {
						//error message
						$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
						$data['msg'] = '<h3>Sorry, you can only redeem this birthday offer once.</h3>';
						$this->load->view( 'profile/submit-response', $data );
						return;
					}
				}



				/* *check status 10 times for same time clicking jeromejose 08-19-2015 */
				$update_status = false;
				$a = 1;
				while ( $a < 10 ) {


					$birthday_offer = $this->profile_model->get_row( array( 'table'=>'tbl_birthday_offers',
							'where'=>array( 'month' =>date( 'm', strtotime( $datetime->format( 'Y-m-d' ) ) ),
								'month'=>date( 'm' ),
								'stock >'=>0,
								'is_deleted'=>0,
								'status'=>1
							),
							'fields'=>'prize_id'
						)
					);

					if ( !$birthday_offer ) {
						//error message
						$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
						$data['msg'] = '<h3>No available birthday offer at this time.</h3>';
						$this->load->view( 'profile/submit-response', $data );
						return;
					}else {
						$user_birthday_prize = $this->profile_model->get_row( array( 'table'=>'tbl_user_birthday_prizes',
								'where'=>array( 'prize_id' =>$birthday_offer->prize_id, 'registrant_id' =>$this->session->userdata( 'user_id' ) ),
								'fields'=>'user_birthday_prize_id'
							)
						);
						if ( $user_birthday_prize ) {
							//error message
							$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
							$data['msg'] = '<h3>Sorry, you can only redeem this birthday offer once.</h3>';
							$this->load->view( 'profile/submit-response', $data );
							return;
						}
					}



					if ( $a == 9 )//update the stock
						{

						$birthday_offer_stock = $this->profile_model->get_row( array( 'table'=>'tbl_birthday_offers',
								'where'=>array( 'month' =>date( 'm', strtotime( $datetime->format( 'Y-m-d' ) ) ),
									'month'=>date( 'm' ),
									'is_deleted'=>0,
									'status'=>1
								)
							)
						);
						
						if ( $birthday_offer_stock ) {
							if($birthday_offer_stock->stock <= 0) {
							//error message
							$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
							$data['msg'] = '<h3>Sorry, this offer is out of stock.</h3>';
							$this->load->view( 'profile/submit-response', $data );
							return;
							}
						}
						
						$birthday_offer = $this->profile_model->get_row( array( 'table'=>'tbl_birthday_offers',
								'where'=>array( 'month' =>date( 'm', strtotime( $datetime->format( 'Y-m-d' ) ) ),
									'month'=>date( 'm' ),
									'stock >'=>0,
									'is_deleted'=>0,
									'status'=>1
								)
							)
						);

						if ( !$birthday_offer ) {
							//error message
							$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
							$data['msg'] = '<h3>No available birthday offer at this time.</h3>';
							$this->load->view( 'profile/submit-response', $data );
							return;
						}else {

								$user_birthday_prize = $this->profile_model->get_row( array( 'table'=>'tbl_user_birthday_prizes',
								'where'=>array( 'prize_id' =>$birthday_offer->prize_id, 'registrant_id' =>$this->session->userdata( 'user_id' ) ),
								'fields'=>'user_birthday_prize_id'
										)
									);
									if ( $user_birthday_prize ) {
										//error message
										$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
										$data['msg'] = '<h3>Sorry, you can only redeem this birthday offer once.</h3>';
										$this->load->view( 'profile/submit-response', $data );
										return;
									}


									$data = array( 'registrant_id'=>$user_id,
									'registrant_name'=>$user['first_name'].' '.$user['last_name'],
									'registrant_birthdate'=>$user['birthdate'],
									'prize_id'=>$birthday_offer->prize_id,
									'prize_name'=>$birthday_offer->prize_name,
									'prize_description'=>$birthday_offer->description,
									'prize_month'=>$birthday_offer->month,
									'prize_image'=>$birthday_offer->prize_image,
									'send_type'=>$birthday_offer->send_type,
									'redemption_address'=>$birthday_offer->redemption_address,
									'redemption_instruction'=>$birthday_offer->redemption_instruction
								);

								$id = $this->profile_model->insert( 'tbl_user_birthday_prizes', $data );





							$new_stock = (int)$birthday_offer->stock - 1;
							$this->profile_model->update( 'tbl_birthday_offers', array( 'stock'=>$new_stock ), array( 'prize_id'=>$birthday_offer->prize_id ) );
							$update_status = true;
							break;
						}


					}
					$a++;
				}
				/**check status 10 times for same time clicking jfj 03-20-2015 */

			}else {

				$data = array( 'msg'=>trim( validation_errors() ),
					'error_holder'=>'birthday-offer-error',
					'error'=>true,
					'btn'=>'birthday-offer-btn',
					'btn_text'=>'<i>Submit Confirmation</i>' );
			}

			if ( $validate_bday ) {
				if ( $update_status ) {


					// SPICE
					$response = $this->spice->GetPerson( array( 'PersonId' => $this->spice->getMemberPersonId() ) );
					$spice_userinfo = $this->spice->parseJSON( $response );

					if ( $spice_userinfo->MessageResponseHeader->TransactionStatus == 0 ) {
						$spice_userinfo = $spice_userinfo->ConsumerProfiles;

						$city = $this->db->select()
						->from( 'tbl_cities' )
						->where( array( 'city_id'=>$this->input->post( 'city' ) ) )
						->limit( 1 )
						->get()
						->row();

						$province = $this->db->select()
						->from( 'tbl_provinces' )
						->where( array( 'province_id'=>$this->input->post( 'province' ) ) )
						->limit( 1 )
						->get()
						->row();

						$city = $city ? $city->city : '';
						$province = $province ? $province->province : '';

						$sec_settings = $this->profile_model->get_row( array( 'table' => 'tbl_section_settings', 'where' => 'name = "BIRTHDAY_OFFERS"' ) );
						$cell_id = $sec_settings->setting_section_id;

						$act_settings = $this->profile_model->get_row( array( 'table' => 'tbl_activity_settings', 'where' => 'name = "BIRTHDAY_OFFERS"' ) );
						$act_id = $act_settings->setting_activity_id;

						$param = array( 'CellId' => $cell_id, 'ActionList' =>array( array( 'ActivityId'   => $act_id, 'ActionValue' => $birthday_offer->prize_name ) ) );
						$response = $this->spice->trackAction( $param );
						$response_json = $this->spice->parseJSON( $response );

						if ( $response_json->MessageResponseHeader->TransactionStatus != 0 ) {
							$data = array( 'msg'=> '<h3>'.$response_json->MessageResponseHeader->TransactionStatusMessage.'</h3>',
								'error_holder'=>'',
								'error'=>false,
								'btn'=>'',
								'btn_text'=>'' );
							$data_error = array( 'origin_id'=>BIRTHDAY_OFFERS,
								'method'=>'trackAction',
								'transaction'=>$this->router->method,
								'input'=> $this->session->userdata( 'spice_input' ),
								'response'=>$response,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
							$this->spice->saveError( $data_error );
						}else {
							if ( !$this->input->post( 'confirm_redemption' ) ) {
								$params = array( 'person_id'=> $this->spice->getMemberPersonId(),
									'updates'=> array(
										'AddressType' => array(
											array(
												'AddressLine1' => $this->input->post( 'street_name' ),
												'Area' => $this->input->post( 'barangay' ),
												'Locality' => $province,
												'City' => $city,
												'Country' => 'PH',
												'Premise'=>'PH',
												'PostalCode' => $this->input->post( 'zip_code' )
											)
										)
									)
								);

								$response = $this->spice->updatePerson( $params );
								$response_json = $this->spice->parseJSON( $response );

								if ( $response_json->MessageResponseHeader->TransactionStatus != 5 ) {
									$data = array( 'msg'=> '<h3>'.$response_json->MessageResponseHeader->TransactionStatusMessage.'</h3>',
										'error_holder'=>'',
										'error'=>false,
										'btn'=>'',
										'btn_text'=>'' );
									$data_error = array( 'origin_id'=>BIRTHDAY_OFFERS,
										'method'=>'ManagePerson',
										'transaction'=>$this->router->method,
										'input'=> $this->session->userdata( 'spice_input' ),
										'response'=>$response,
										'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
									);
									$this->spice->saveError( $data_error );
								}else {
									// $data = array( 'registrant_id'=>$user_id,
									// 	'registrant_name'=>$user['first_name'].' '.$user['last_name'],
									// 	'registrant_birthdate'=>$user['birthdate'],
									// 	'prize_id'=>$birthday_offer->prize_id,
									// 	'prize_name'=>$birthday_offer->prize_name,
									// 	'prize_description'=>$birthday_offer->description,
									// 	'prize_month'=>$birthday_offer->month,
									// 	'prize_image'=>$birthday_offer->prize_image,
									// 	'send_type'=>$birthday_offer->send_type,
									// 	'redemption_address'=>$birthday_offer->redemption_address,
									// 	'redemption_instruction'=>$birthday_offer->redemption_instruction
									// );

									// $id = $this->profile_model->insert( 'tbl_user_birthday_prizes', $data );
									//$this->profile_model->update('tbl_birthday_offers',array('stock'=>$new_stock),array('prize_id'=>$birthday_offer->prize_id));

									$this->load->model( 'notification_model' );
									if ( $birthday_offer->send_type=='Delivery' ) {
										$message = 'Your birthday gift item from Marlboro will be delivered to your mailing address within the next month.';
										$message_for_popup = 'You have successfully claimed the birthday offer!<br><small>(Expect to receive your gift within the next month.)</small>';
									}else {
										$message = 'Please follow the instruction below to redeem your birthday gift item from Marlboro:<br/>'.$birthday_offer->redemption_instruction;
										$message .= ' <br/>You can redeem your flash offer gift item from Marlboro at: <br/>'.$birthday_offer->redemption_address;
										$message_for_popup = '<h3>You have successfully claimed the birthday offer!<br><small>(Please check your notification to view the instruction and address of item redemption.)</h3>';
									}

									$param = array( 'message'=>$message, 'suborigin'=>$id );
									$this->notification_model->notify( $user_id, BIRTHDAY_OFFERS, $param );

									$data = array( 'msg'=>$message_for_popup,
										'error_holder'=>'',
										'error'=>false,
										'btn'=>'',
										'btn_text'=>'' );
								}
							}else {
								// $data = array( 'registrant_id'=>$user_id,
								// 	'registrant_name'=>$user['first_name'].' '.$user['last_name'],
								// 	'registrant_birthdate'=>$user['birthdate'],
								// 	'prize_id'=>$birthday_offer->prize_id,
								// 	'prize_name'=>$birthday_offer->prize_name,
								// 	'prize_description'=>$birthday_offer->description,
								// 	'prize_month'=>$birthday_offer->month,
								// 	'prize_image'=>$birthday_offer->prize_image,
								// 	'send_type'=>$birthday_offer->send_type,
								// 	'redemption_address'=>$birthday_offer->redemption_address,
								// 	'redemption_instruction'=>$birthday_offer->redemption_instruction
								// );

								// $id = $this->profile_model->insert( 'tbl_user_birthday_prizes', $data );
								//$this->profile_model->update('tbl_birthday_offers',array('stock'=>$new_stock),array('prize_id'=>$birthday_offer->prize_id));

								$this->load->model( 'notification_model' );
								if ( $birthday_offer->send_type=='Delivery' ) {
									$message = 'Your birthday gift item from Marlboro will be delivered to your mailing address within the next month.';
									$message_for_popup = 'You have successfully claimed the birthday offer!<br><small>(Expect to receive your gift within the next month.)</small>';
								}else {
									$message = 'Please follow the instruction below to redeem your birthday gift item from Marlboro:<br/>'.$birthday_offer->redemption_instruction;
									$message .= ' <br/>You can redeem your flash offer gift item from Marlboro at: <br/>'.$birthday_offer->redemption_address;
									$message_for_popup = '<h3>You have successfully claimed the birthday offer!<br><small>(Please check your notification to view the instruction and address of item redemption.)</h3>';
								}

								$param = array( 'message'=>$message, 'suborigin'=>$id );
								$this->notification_model->notify( $user_id, BIRTHDAY_OFFERS, $param );

								$data = array( 'msg'=>$message_for_popup,
									'error_holder'=>'',
									'error'=>false,
									'btn'=>'',
									'btn_text'=>'' );
							}
						}
					}else {
						$data = array( 'msg'=> '<h3>'.$spice_userinfo->MessageResponseHeader->TransactionStatusMessage.'</h3>',
							'error_holder'=>'',
							'error'=>false,
							'btn'=>'',
							'btn_text'=>'' );
						$data_error = array( 'origin_id'=>BIRTHDAY_OFFERS,
							'method'=>'GetPerson',
							'transaction'=>$this->router->method,
							'input'=> $this->session->userdata( 'spice_input' ),
							'response'=>$response,
							'response_message'=>$spice_userinfo->MessageResponseHeader->TransactionStatusMessage
						);
						$this->spice->saveError( $data_error );
					}
				}  else {

					//error message
					$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
					$data['msg'] = '<h3>Sorry, offer is no longer available.</h3>';
					$this->load->view( 'profile/submit-response', $data );
					return;

				}
			}else {

				$data = array( 'msg'=>trim( validation_errors() ),
					'error_holder'=>'birthday-offer-error',
					'error'=>true,
					'btn'=>'birthday-offer-btn',
					'btn_text'=>'<i>Submit Confirmation</i>' );
			}


			$this->load->view( 'profile/submit-response', $data );

		}else {

			$spice_userinfo = $this->spice->GetPerson( array( 'PersonId' => $this->spice->getMemberPersonId() ) );
			$spice_userinfo = $this->spice->parseJSON( $spice_userinfo );
			$spice_userinfo = $spice_userinfo->ConsumerProfiles;

			$data['spice_userinfos'] = $spice_userinfo;
			$data['cities'] =  $this->profile_model->get_rows( array( 'table'=>'tbl_cities', 'LOWER(province)'=>strtolower( $spice_userinfo->Addresses[0]->Locality ) ) );
			$data['provinces'] = $this->profile_model->get_rows( array( 'table'=>'tbl_provinces' ) );
			$this->load->view( 'birthday_offer/confirm-birthday-offer', $data );

		}

	}

	private function validate_offer_confirmation() {

		$this->load->library( 'form_validation' );
		$rules = array( array(
				'field'   => 'prize_id',
				'label'   => 'Birthday Offer',
				'rules'   => ''//'callback_check_birthday_offer'
			)
		);

		if ( !$this->input->post( 'confirm_redemption' ) ) {

			$rules[] = array( array(
					'field'   => 'street_name',
					'label'   => 'Street Name',
					'rules'   => 'trim|required'
				),
				array(
					'field'   => 'barangay',
					'label'   => 'Barangay',
					'rules'   => 'trim|required'
				),
				array(
					'field'   => 'zip_code',
					'label'   => 'Zip Code',
					'rules'   => 'trim|required'
				),
				array(
					'field'   => 'city',
					'label'   => 'City',
					'rules'   => 'trim|required'
				),
				array(
					'field'   => 'province',
					'label'   => 'Province',
					'rules'   => 'trim|required'
				),
				array(
					'field'   => 'prize_id',
					'label'   => 'Birthday Offer',
					'rules'   => ''//'callback_check_birthday_offer'
				)
			);

		}

		$this->form_validation->set_rules( $rules );
		return $this->form_validation->run();
	}

	public function check_birthday_offer() {

		$user = $this->session->userdata( 'user' );

		$spice_userinfo = $this->spice->GetPerson( array( 'PersonId' =>$this->spice->getMemberPersonId() ) );
		$spice_userinfo = $this->spice->parseJSON( $spice_userinfo );
		$spice_userinfo = $spice_userinfo->ConsumerProfiles;

		$date_time = $spice_userinfo->PersonDetails->DateOfBirth;


		$match = preg_match( '/\/Date\((\d+)([-+])(\d+)\)\//', $date_time, $date );

		$timestamp = $date[1]/1000;
		$operator = $date[2];
		$hours = $date[3]*36; // Get the seconds

		$datetime = new DateTime();

		$datetime->setTimestamp( $timestamp );
		$datetime->modify( $operator . $hours . ' seconds' );

		$birthday_offer = $this->profile_model->get_row( array( 'table'=>'tbl_birthday_offers',
				'where'=>array( 'month' =>date( 'm', strtotime( $datetime->format( 'Y-m-d' ) ) ),
					'month'=>date( 'm' ),
					'stock >'=>0
				),
				'fields'=>'prize_id'
			)
		);

		if ( $birthday_offer && (int)$this->session->userdata( 'user_id' ) ) {

			$user_birthday_prize = $this->profile_model->get_row( array( 'table'=>'tbl_user_birthday_prizes',
					'where'=>array( 'prize_id' =>$birthday_offer->prize_id, 'registrant_id' =>$this->session->userdata( 'user_id' ) ),
					'fields'=>'user_birthday_prize_id'
				)
			);
			if ( $user_birthday_prize ) {
				$this->form_validation->set_message( 'check_birthday_offer', 'Sorry, you can only redeem this birthday offer once.' );
				return false;
			}else {
				return true;
			}


		}else {
			$this->form_validation->set_message( 'check_birthday_offer', 'No available birthday offer at this time.' );
			return false;
		}

		return true;
	}

	public function get_birthday_offer_edm() {
		$user = $this->session->userdata( 'user' );

		$spice_userinfo = $this->spice->GetPerson( array( 'PersonId' =>$this->spice->getMemberPersonId() ) );
		$spice_userinfo = $this->spice->parseJSON( $spice_userinfo );
		$spice_userinfo = $spice_userinfo->ConsumerProfiles;

		$date_time = $spice_userinfo->PersonDetails->DateOfBirth;


		$match = preg_match( '/\/Date\((\d+)([-+])(\d+)\)\//', $date_time, $date );

		$timestamp = $date[1]/1000;
		$operator = $date[2];
		$hours = $date[3]*36; // Get the seconds

		$datetime = new DateTime();

		$datetime->setTimestamp( $timestamp );
		$datetime->modify( $operator . $hours . ' seconds' );

		$birthday_offer = $this->profile_model->get_row( array( 'table'=>'tbl_birthday_offers',
				'where'=>array( "month = '".date( 'm', strtotime( $datetime->format( 'Y-m-d' ) ) )."' AND month ='".date( 'm' )."'" =>null,
					'stock >'=>0,
					'status'=>1,
					'is_deleted'=>0
				),
				'fields'=>'prize_id'
			)
		);
		if ( $birthday_offer ) {
			$user_birthday_prize = $this->profile_model->get_row( array( 'table'=>'tbl_user_birthday_prizes',
					'where'=>array( 'prize_id' =>$birthday_offer->prize_id, 'registrant_id' =>$this->session->userdata( 'user_id' ) ),
					'fields'=>'user_birthday_prize_id'
				)
			);
			$birthday_offer = $user_birthday_prize ? '' : $birthday_offer->prize_id;
		}

		$prize_id = $birthday_offer;
		$user = $this->session->userdata( 'user' );
		$data['flash_offer'] = $this->input->get( 'flash_offer' );
		$data['birthday_offer'] = $this->profile_model->get_row( array( 'table'=>'tbl_birthday_offers', 'where'=>array( 'prize_id' =>$prize_id ) ) );
		$this->load->view( 'birthday_offer/birthday-offer', $data );
	}

	public function get_cities() {
		$rows = $this->profile_model->get_rows( array( 'table'=>'tbl_cities',
				'where'=>array( 'province_id'=>$this->input->get( 'province' ) ),
				'order_by'=>array( 'field'=>'city', 'order'=>'ASC' ),
				'group_by'=>array( 'city' )
			)
		);

		$data['data'] = $rows->result_array();
		$this->load->view( 'profile/json_format', $data );

	}

}
