<?php

class Notifications extends CI_Controller
{
	public function index()
	{
		$this->load->helper('text');
		$data = array();
		$data['user'] = $this->session->userdata('user_id');
		$data['notifications'] = $this->Notification_Model->get_by_user($data['user']);
		$this->load->layout('notifications/index', $data);
	}

	public function message()
	{
		$data = array();
		$data['notification_id'] = $this->uri->segment(3);
		if (!$data['notification_id']) {
			show_404();
		}
		$data['user'] = $this->session->userdata('user_id');
		$data['notification'] = $this->Notification_Model->get_notification($data['user'], $data['notification_id']);
		if (!$data['notification']) {
			show_404();
		} elseif (!$data['notification']['notification_status']) {
			$this->Notification_Model->read_notification($data['user'], $data['notification_id']);
		}
		$this->load->layout('notifications/message', $data);
	}

	public function delete()
	{
		if (!$this->session->userdata('user_id') || 
			$this->input->server('REQUEST_METHOD') != 'POST' || 
			!$this->input->is_ajax_request())
		{
			show_404();
		}

		$result['success'] = false;
		$ids = $this->input->post('ids');
		if ($ids && is_array($ids) && 
			$this->Notification_Model->delete_notifications($this->session->userdata('user_id'), $ids))
		{
			$result['success'] = true;
		}
		$this->output->set_content_type('application/json')
			->set_output(json_encode($result));
	}
}