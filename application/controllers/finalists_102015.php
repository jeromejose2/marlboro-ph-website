<?php if(!defined("BASEPATH")){ exit("No direct script access allowed!"); }

class Finalists extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// echo '<pre>';
		// print_r($this->session->all_userdata());
		// exit;
	}

	public function index($finalist_id = FALSE)
	{
		function toSlug($string) {
			$slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
   			return strtoupper($slug);
		}

		if($finalist_id) {
			$this->db->select()->from('tbl_finalists');
			$this->db->where('finalist_id', $finalist_id);
			$this->db->where('is_deleted', 0);
			$valid = $this->db->get()->result_array();			

			if($valid) {
				$this->load->model('finalist_comment_model');
				$this->db->select()->from('tbl_finalist_likers');
				$this->db->where('registrant_id', $this->session->userdata('user_id'));
				$this->db->where('finalist_id', $finalist_id);
				$exist = $this->db->get()->num_rows();
				$photo_lists = unserialize($valid[0]['photos']);
				$video_lists = unserialize($valid[0]['videos'])['url'];

				$final_photo_lists = array();
				$final_video_lists = array();

				$i = 0;
				foreach($photo_lists as $k => $v) {
					foreach($v as $key => $val) {
						$final_photo_lists[$i]['file'] = str_replace('uploads/', BASE_URL.'uploads/', $val);
						$final_photo_lists[$i]['type'] = 'image';
						$i++;
					}
				}

				if($video_lists) {
					foreach($video_lists as $vk => $vv) {
						$final_video_lists[$i]['file'] = str_replace('uploads/', BASE_URL.'uploads/', $vv);
						$final_video_lists[$i]['type'] = '';
						$final_video_lists[$i]['default'] = BASE_URL.'assets_finalist/img/play-red.png';
						$i++;
					}

					$final_photo_lists = array_merge($final_photo_lists, $final_video_lists);
				}

				// echo '<pre>';
				// print_r(json_encode(array_chunk($final_photo_lists, 4)));
				// exit;
				$data['liked'] = $exist ? TRUE : FALSE;
				$data['gallery'] = json_encode($final_photo_lists);
				$data['image_count'] = count($final_photo_lists);
				$data['count'] = ceil(count($final_photo_lists) / 4);
				$data['finalist'] = $valid[0];
				$data['comments'] = $this->finalist_comment_model->get_comments($finalist_id);
				$data['content'] = $this->load->view('finalists/finalists-details', $data, TRUE);
				$this->load->view('finalists/main-template', $data);
			} else {
				redirect('finalists');
			}
			return false;
		}
		$this->load->helper('text');

		$data['finalists'] = $this->db->select()->from('tbl_finalists')->where('is_deleted', 0)->get()->result_array();
		$data['user_points'] = $this->session->userdata('user_points');

		$data['content'] = $this->load->view('finalists/finalists-list', $data, TRUE);
		$this->load->view('finalists/main-template', $data);
	}

	public function details()
	{
		$data = array();
		$data['content'] = $this->load->view('finalists/finalists-details', $data, TRUE);
		$this->load->view('finalists/main-template', $data);
	}

	public function addComment()
	{
		$this->load->model('finalist_comment_model');

		$post = $this->input->post();

		$registrant_id = $this->session->userdata('user_id');
		$finalist_id = $post['fdid'];
		$comment = $post['comment'];
		$type = $post['type'];
		$comment_id = isset($post['comment_id']) ? $post['comment_id'] : FALSE;

		$this->finalist_comment_model->save($registrant_id, $finalist_id, $comment, $type, $comment_id);
	}

	public function checkUser()
	{
		if($_SERVER['REMOTE_ADDR'] == '180.232.124.116') {
			echo '<pre>';
			print_r($this->session->all_userdata());
			exit;	
		}
		show_error('Permission denied', 403);
	}

	public function _remap($method)
	{
		if(is_numeric($method)) {
			$this->index($method);
		} else {
			$this->$method();
		}
	}

}
