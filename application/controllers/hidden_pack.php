<?php if(!defined("BASEPATH")){ exit('No direct script access allowed!'); }

class Hidden_pack extends CI_Controller {

 
	public function index()
	{
		$this->load->model(array('Points_Model','profile_model','notification_model'));
		
		$user_id = $this->session->userdata('user_id');
		$hidden_pack_id = $this->input->get('pack_id');
		$source = $this->input->get('source');
		$row = $this->profile_model->get_row(array('table'=>'tbl_hidden_packs',
													'where'=>array("CURDATE() >= DATE(start_date) AND CURDATE() <= DATE(end_date) AND is_deleted = '0' AND hidden_pack_id NOT IN(SELECT hidden_pack_id FROM tbl_hidden_pack_logs WHERE registrant_id = '$user_id' AND DATE(date_added) = CURDATE()) "=>null,'hidden_pack_id'=>$hidden_pack_id)));
		$data['row'] = false;
		
		$popup = 'found-marlboro';
 
		if($row){

			if(strpos($row->page,$source)!== false){ 

				$id = $this->profile_model->insert('tbl_hidden_pack_logs',
													array('registrant_id'=>$user_id,
															'hidden_pack_id'=>$row->hidden_pack_id,
															'page_availed'=>$source,
															'date_added'=>true)
													);

	 			$this->session->unset_userdata($source);

				if($row->popup_layout=='Default'){ 
					$this->Points_Model->earn(HIDDEN_MARLBORO, array('suborigin' => $id)); 


					$param = array('message'=>'You have found the hidden Marlboro pack. You gained '.HIDDEN_MARLBORO_BASE_POINT.' points.','suborigin'=>$id);
					$this->notification_model->notify($user_id,HIDDEN_MARLBORO,$param);

					$data['message'] = "You found the hidden pack and earned ".HIDDEN_MARLBORO_BASE_POINT." points!";
					$data['row'] = $row;

				}else{

 					$this->Points_Model->earn(PREMIUM_HIDDEN_MARLBORO, array('suborigin' => $id));

					$param = array('message'=>'You have found the hidden Marlboro Premium Black pack. You gained '.PREMIUM_HIDDEN_MARLBORO_BASE_POINT.' points.','suborigin'=>$id);
					$this->notification_model->notify($user_id,PREMIUM_HIDDEN_MARLBORO,$param); 
					$popup = 'found-premium-black';

				}

			}


		}else{

			$data['message'] = 'Hidden marlboro pack not found.';

		}

  		$this->load->view('hidden_pack/'.$popup,$data);
	}

	public function premium()
	{
		$this->load->view('hidden_pack/found-premium-black');
	}

}