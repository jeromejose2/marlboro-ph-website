<?php

class Move_Forward extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Movefwd_Model');
		$this->load->model('profile_model');
	}

	public function index()
	{
		$data = array();
		$data['active'] = (int) $this->uri->segment(3);
		// $data['categories'] = $this->nav_movefwd_categories;
		//$data['visited'] = true;
		$data['offer_status'] = array();
		if (!$data['active']) {
			if ($this->uri->segment(2)) {
				show_404();
			}
			$data['offers'] = $this->Movefwd_Model->get_active_offers();
			// if (!$this->session->userdata('move_fwd')) {
			// 	//$data['visited'] = false;
			// 	$this->session->set_userdata('move_fwd', 1);
			// }
		} else {
			$found = false;
			foreach ($data['categories'] as $offer) {
				if ($offer['category_id'] == $data['active']) {
					$found = true;
					break;
				}
			}
			if (!$found) {
				show_404();
			}
			$data['offers'] = $this->Movefwd_Model->get_offers_by_category($data['active']);
		}

		$data['visited'] = $this->Movefwd_Model->has_viewed_splash();

		if($data['offers']) {
			foreach ($data['offers'] as $key => $value) {
				$complete = $this->Movefwd_Model->is_complete_all_activity($this->session->userdata('user_id'), $move_forward, $gallery, $value['move_forward_id']);
				$offer_status = 0;
				if($complete) {
					$offer_status = 2;
				} else {
					if($gallery >= 1 && $gallery < $move_forward) {
						$offer_status = 1;
					}

					if($this->Movefwd_Model->has_pledge_done($this->session->userdata('user_id'), $value['move_forward_id'])) {
						$offer_status = 3;
					}
				}
				$data['offer_status'][$value['move_forward_id']] = $offer_status;	
			}
		}

		if($data['offers']) {
			foreach($data['offers'] as $k => $v) {
				$data['offers'][$k]['has_image'] = $this->checkImageIfExist(BASE_URL.'uploads/move_fwd/thumb_'.$v['move_forward_image']) ? $v['move_forward_image'] : '';
			}
		}

		$this->load->layout('move_fwd/index', $data);
	}

	public function past()
	{
		$data = array();
		$data['active'] = (int) $this->uri->segment(3);
		// $data['categories'] = $this->nav_movefwd_categories;
		$categories = $this->nav_movefwd_categories;
		//$data['visited'] = true;
		$data['offer_status'] = array();
		if (!$data['active']) {
			// if ($this->uri->segment(2)) {
			// 	show_404();
			// }
			// $data['offers'] = $this->Movefwd_Model->get_active_offers();
			$data['offers2'] = array();
			$data['offers'] = array();
			foreach($categories as $k => $v) {
				$data['offers2'][] = $this->Movefwd_Model->get_offers_by_category($v['category_id']);
			}
			foreach ($data['offers2'] as $key => $value) {
				foreach ($value as $kb => $ab) {
					$data['offers'][] = $ab;
				}
			}
			// if (!$this->session->userdata('move_fwd')) {
			// 	//$data['visited'] = false;
			// 	$this->session->set_userdata('move_fwd', 1);
			// }
		} else {
			$found = false;
			foreach ($data['categories'] as $offer) {
				if ($offer['category_id'] == $data['active']) {
					$found = true;
					break;
				}
			}
			if (!$found) {
				show_404();
			}
			$data['offers'] = $this->Movefwd_Model->get_offers_by_category($data['active']);
		}

		$data['visited'] = $this->Movefwd_Model->has_viewed_splash_past();

		if($data['offers']) {
			foreach ($data['offers'] as $key => $value) {
				$complete = $this->Movefwd_Model->is_complete_all_activity($this->session->userdata('user_id'), $move_forward, $gallery, $value['move_forward_id']);
				$offer_status = 0;
				if($complete) {
					$offer_status = 2;
				} else {
					if($gallery >= 1 && $gallery < $move_forward) {
						$offer_status = 1;
					}

					if($this->Movefwd_Model->has_pledge_done($this->session->userdata('user_id'), $value['move_forward_id'])) {
						$offer_status = 3;
					}
				}
				$data['offer_status'][$value['move_forward_id']] = $offer_status;	
			}
		}

		if($data['offers']) {
			foreach($data['offers'] as $k => $v) {
				$data['offers'][$k]['has_image'] = $this->checkImageIfExist(BASE_URL.'uploads/move_fwd/thumb_'.$v['move_forward_image']) ? $v['move_forward_image'] : '';
			}
		}
		
		$this->load->layout('move_fwd/index-past-offer', $data);
	}

	public function offer()
	{
		$data = array();
		$user = $this->session->userdata('user_id');
		$offerId = (int) $this->uri->segment(3);
		$permalink = (string) $this->uri->segment(4);
		if (!$permalink) {
			show_404();
		}
		
		// $data['offer'] = $this->Movefwd_Model->get_offer_by_permalink($permalink, $offerId);
		$data['offer'] = $this->Movefwd_Model->get_offer($offerId, $permalink);
		if (!$data['offer']) {
			show_404();
		}

		// $data['categories'] = $this->nav_movefwd_categories;

		if ($data['offer']['status']) {

			$data['has_pledge'] = null;
			$data['entries'] = null;
			$data['entries_count'] = 0;
			$data['current'] = null;
			$data['pending_entry'] = null;
			$data['chosen_action'] = $this->Movefwd_Model->get_chosen_action($user, $data['offer']['move_forward_id']);
			$data['challenges'] = $this->Movefwd_Model->get_challenges_by_offer($data['offer']['move_forward_id']);
			$data['numOfTasks'] = count($data['challenges']);
			$data['current'] = isset($data['challenges'][0]) ? $data['challenges'][0] : null;
			if ($data['chosen_action'] == 1) {
				$data['entries'] = $this->Movefwd_Model->get_entries($user, $data['offer']['move_forward_id']);
				$data['entries_count'] = count($data['entries']);
				$data['current'] = isset($data['challenges'][$data['entries_count']]) ? $data['challenges'][$data['entries_count']] : null;
				if ($data['current']) {
					$data['pending_entry'] = $this->Movefwd_Model->get_pending_entry($user, $data['current']['challenge_id']);
				}
			} elseif ($data['chosen_action'] == 2) {
				$data['has_pledge'] = $this->Movefwd_Model->has_pledge_done($user, $data['offer']['move_forward_id']);
			}

			$data['entries_ids'] = json_encode(array(23,24));

			$m_fwd = $data['offer']['move_forward_id'];
			$e = $this->db->select()->from('tbl_challenges')->where('move_forward_id', $m_fwd)->get()->result_array();

			$data['has_pending'] = $this->check_for_pending($user, $e);
			
			$this->load->layout('move_fwd/offer', $data);




		} else {

			$this->load->layout('move_fwd/inactive_offer', $data);

		}
		
	}

	private function checkImageIfExist($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_NOBODY, 1);
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if(curl_exec($ch) !== FALSE) {
			return true;
		} else {
			return false;
		}
	}

	private function check_for_pending($user, $challenges) {
		if($challenges && is_array($challenges)) {
			$pending = false;
			foreach($challenges as $k => $v) {
				$result = $this->db->select()->from('tbl_move_forward_gallery')->where('challenge_id', $v['challenge_id'])->where('registrant_id', $user)->where('mfg_status', 0)->get()->result_array();
				if($result) {
					$pending = true;
				}
			}
			if($pending) {
				return true;
			} else {
				return false;
			}
		}
	}

	

	public function participants()
	{
		$id = $this->input->get('mfid');
		if (!$id) {
			show_404();
		}
		$limit = (int) $this->input->get('limit');
		$page = (int) $this->input->get('page');
		if (!$page) {
			$page = 1;
		}
		$result = array();
		$result['participants'] = $this->Movefwd_Model->get_participants($id, $page, $limit);
		$result['total'] = $this->Movefwd_Model->get_participants_count($id);
		$pages = ceil($result['total'] / $limit);
		$result['prev'] = 0;
		if ($page >= 2) {
			$result['prev'] = $page - 1;
		}
		if ($page < $pages) {
			$result['next'] = ++$page;
		} else {
			$result['next'] = false;
		}
		$this->output->set_content_type('application/json')
			->set_output(json_encode($result));
	}

	public function slots()
	{
		$id = $this->input->get('mfid');
		if ($id) {
			$offer = $this->Movefwd_Model->get_offer($id);
			if ($offer && $offer['slots']) {
				$this->output->set_output($offer['slots'].' SLOTS LEFT');
			}
		}
	}

	public function pledge()
	{
		$result = array(
			'success' => false,
			'error' => '',
			'points' => 0
		);
		$id = $this->input->post('mfid');
		$choice = $this->input->post('choice');
		$user = $this->session->userdata('user_id');
		$has_pledged = $this->Movefwd_Model->has_pledge_done($user, $id);

		if($has_pledged) {
			$result = array(
				'success' => false,
				'error' => 'You already pledged on this offer.',
				'points' => 0
			);
			$this->output->set_content_type('application/json')
			->set_output(json_encode($result));
			return;
		}

		if ($user && $id) {
			$this->load->model('Points_Model');
			$this->load->model('Perks_Model');
			$offer = $this->Movefwd_Model->get_active_offer($id);
			if ($offer) {
				$params = array(
					'registrant' => $user,
					'suborigin' => $id,
					'remarks' => '{{ name }} pledged for MoveFWD',
					'points' => $offer['pledge_points']
				);
				$bidStatus = $this->Perks_Model->check_bid_points($user);
				if (!$params['points']) {
					$result['error'] = 'Invalid pledge';
				} elseif (!empty($bidStatus['items']) && isset($bidStatus['available_points']) && $bidStatus['available_points'] < $offer['pledge_points']) {
					$result['error'] = "You only have {$bidStatus['available_points']} points available for you to use. You placed a bid for ".implode(',', $bidStatus['items']).", and {$bidStatus['reserved_points']} points are blocked off while you are still the highest bidder.";
				} elseif (!($pledged_points = $this->Points_Model->spend(MOVE_FWD_PLEDGE_ENTRY, $params))) {
					$result['error'] = "Earn more points to be able to pledge for this activity!";
				// } elseif ($this->Movefwd_Model->decrement_slot($id)) {
				} else {
					$has_pledged = $this->Movefwd_Model->has_pledge_done($user, $id);

					if($has_pledged) {
						$result = array(
							'success' => false,
							'error' => 'You already pledged on this offer.',
							'points' => 0
						);
						$this->output->set_content_type('application/json')
						->set_output(json_encode($result));
						return;
					}

					# choice
					$choice = $this->Movefwd_Model->get_chosen_action($user, $id);
					if (!$choice) {
						if($action === MOVE_FWD_PLEDGE) {
							if($has_pledged) {
								$result = array(
									'success' => false,
									'error' => 'You already pledged on this offer',
									'points' => 0
								);
								$this->output->set_content_type('application/json')
								->set_output(json_encode($result));
								return;
							}
						}
						if ($this->Movefwd_Model->choose_action($user, $id, (int) $this->input->post('choice'))) {
						}
					} else {
						$result = array(
							'success' => false,
							'error' => 'You canot pledge on this offer',
							'points' => 0
						);
						$this->output->set_content_type('application/json')
						->set_output(json_encode($result));
						return;
					}
					# /choice			

					$this->Notification_Model->notify($user, MOVE_FWD_PLEDGE_ENTRY, array(
							'message' => "You have successfully pledged {$pledged_points} points for {$offer['move_forward_title']}.",
							'suborigin' => $id
						));
					$result['success'] = true;
					$result['points'] = $pledged_points;

					$sec_settings = $this->profile_model->get_row(array('table' => 'tbl_section_settings', 'where' => 'name = "MOVE_FWD"'));
					$cell_id = $sec_settings->setting_section_id;

					$act_settings = $this->profile_model->get_row(array('table' => 'tbl_activity_settings', 'where' => 'name = "MOVE_FWD"'));
					$act_id = $act_settings->setting_activity_id;

					$param = array('CellId' => $cell_id, 'ActionList' =>array(array('ActivityId'   => $act_id , 'ActionValue' => $offer['move_forward_title'].'|pledged|')));
					$response = $this->spice->trackAction($param);
					$response_json = $this->spice->parseJSON($response);

					if($response_json->MessageResponseHeader->TransactionStatus != 0){
							$result['error'] = '<h3>'.$response_json->MessageResponseHeader->TransactionStatusMessage.'</h3>';

							$data_error = array('origin_id'=>MOVE_FWD,
									'method'=>'trackAction',
									'transaction'=>$this->router->method,
									'input'=> $this->session->userdata('spice_input'),
									'response'=>$response,
									'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
								);
							$this->spice->saveError($data_error);
					}

					// if($response_json->MessageResponseHeader->TransactionStatus != 0){
					// 		$result['error'] = '<h3>'.$response_json->MessageResponseHeader->TransactionStatusMessage.'</h3>';

					// 		$data_error = array('origin_id'=>MOVE_FWD,
					// 				'method'=>'trackAction',
					// 				'transaction'=>$this->router->method,
					// 				'input'=> $this->session->userdata('spice_input'),
					// 				'response'=>$response,
					// 				'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
					// 			);
					// 		$this->spice->saveError($data_error);
					// }else{
					// 	$this->Notification_Model->notify($user, MOVE_FWD_PLEDGE_ENTRY, array(
					// 			'message' => "You have successfully pledged {$pledged_points} points for {$offer['move_forward_title']}.",
					// 			'suborigin' => $id
					// 		));
					// 	$result['success'] = true;
					// 	$result['points'] = $pledged_points;
					// }
				}
			} else {
				$result['error'] = 'Offer not existing';
			}
		}

		$this->output->set_content_type('application/json')
			->set_output(json_encode($result));
	}

	private function check_if_already_played($user, $challenge_id) {
		$return = $this->db->select()->from('tbl_move_forward_gallery')->where('registrant_id', $user)->where('challenge_id', $challenge_id)->where('mfg_status', 1)->get()->result_array();

		if($return) {
			return true;
		} else {
			return false;
		}
	}

	public function upload()
	{
		$user = $this->session->userdata('user_id');
		$challenge_id = $this->input->post('challenge_id');
		$fwd_id = $this->db->select()->from('tbl_challenges')->where('challenge_id', $challenge_id)->get()->result_array();

		$challenges = $this->db->select()->from('tbl_challenges')->where('move_forward_id', $fwd_id[0]['move_forward_id'])->get()->result_array();

		$has_pending_entry = $this->Movefwd_Model->get_pending_entry($user, $challenge_id);
		$has_pending_entries = $this->check_for_pending($user, $challenges);
		$has_played_entry = $this->check_if_already_played($user, $challenge_id);

		if($has_pending_entry) {
			$this->output->set_output('Please give us time to process and verify your last submission.');
			return;
		}

		if($has_pending_entries) {
			$this->output->set_output('Please give us time to process and verify your last submission.');
			return;
		}

		if($has_played_entry) {
			$this->output->set_output('You already played this challenge. Please choose another one.');
			return;
		}

		// if($has_entry) {
		// 	if($fwd_id) {
		// 		$move_fwd_id = $fwd_id[0]['move_forward_id'];
		// 		echo '<pre>';
		// 		print_r($move_fwd_id);
		// 		exit;
		// 	} else {
		// 		$this->output->set_output('Test');
		// 		return;
		// 	}			
		// }

		$path = './uploads/profile/';
		if (!is_dir($path)) {
			mkdir($path);
		}
		if (!is_dir($path.$user)) {
			mkdir($path.$user);
		}
		$path = $path.$user.'/move_fwd/';
		if (!is_dir($path)) {
			mkdir($path);
		}

		$content = null;
		$type = (int) $this->input->post('type');
		if ($type == 1) {
			$config = array();
			$config['upload_path'] = $path;
			$config['allowed_types'] = MOVE_FWD_ALLOWED_TYPES;
			$config['max_size'] = FILE_UPLOAD_LIMIT;
			$config['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['move_fwd_upload']['name'], PATHINFO_EXTENSION);

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('move_fwd_upload')) {
				$this->output->set_output($this->upload->display_errors());
				return;
			}
			$this->load->helper('resize');
			resize(array(
				'width' => 115, 
				'height'=> 71, 
				'source_image' => $path.$config['file_name'],
				'new_image_path' => $path,
				'file_name' => $config['file_name']
			));
			$content = $config['file_name'];
		} elseif ($type == 2) {
			$content = (string) $this->input->post('video');
			if (!$content || !validate_youtube_url($content)) {
				$this->output->set_output('Invalid youtube url');
				return;
			}
		} else {
			$content = (string) $this->input->post('text');
			if (!$content) {
				$this->output->set_output('Please input your entry');
				return;
			}	
		}
		
		$this->db->insert('tbl_move_forward_gallery', array(
			'challenge_id' => $this->input->post('challenge_id'),
			'mfg_content' => $content,
			'mfg_status' => $this->session->userdata('is_cm') ? 1 : 0,
			'mfg_is_public' => 1,
			'registrant_id' => $user,
			'description' => $this->input->post('description')
		));

	}

	public function splash()
	{
		$this->session->set_userdata('move_fwd', 1);
	}

	public function gallery()
	{
		$data = array();
		$this->load->model('Comment_Model');
		$data['offer_id'] = (int) $this->uri->segment(3);
		if (!$data['offer_id']) {
			show_404();
		}
		$data['offer'] = $this->Movefwd_Model->get_offer($data['offer_id']);
		if (!$data['offer']) {
			show_404();
		}

		$this->load->model('View_Model');
		$data['visits'] = $this->View_Model->get_visits_count(MOVE_FWD_COMMENTS_PHOTOS, $data['offer_id']);
		$data['entries'] = $this->Movefwd_Model->get_gallery($data['offer_id']);
		// $data['visits'] = 0;

		$suborigins = array();
		foreach ($data['entries'] as $entry) {
			$suborigins[] = $entry['comment_id'];
		}

		$data['comments_count'] = 0;
		if ($suborigins) {
			$data['comments_count'] = $this->Comment_Model->get_comments_replies_count(MOVE_FWD_COMMENTS_PHOTOS, $suborigins);
		}
		
		$this->load->view('move_fwd/gallery', $data);
	}

	public function mechanics()
	{
		$data = array();
		$data['content'] = (array) $this->db->select()
			->from('tbl_settings')
			->where('type', 'move_fwd_mechanics')
			->get()
			->row();
		$this->load->view('move_fwd/move-fwd-mechanics', $data);
	}

	public function challenge() {
		$offer_id = $this->uri->segment(3);
		$challenges = $this->Movefwd_Model->get_challenges_by_offer($offer_id);
		if(!$challenges)
			redirect('move_forward');
		$data['challenges'] = $challenges;
		$this->load->view('move_fwd/move-fwd-completed', $data);
	}

	public function _remap()
	{
		// redirect('/');
		//die('test');
		$action = $this->uri->segment(2);
		if (!$action || $action == 'category') {
			$this->index();
		} elseif ($action == 'mechanics') {
			$this->mechanics();
		} elseif ($action == 'inactive_offer') {
			$this->inactive_offer();
		} elseif ($action == 'offer') {
			$this->offer();
		} elseif ($action == 'gallery') {
			$this->gallery();
		} elseif($action == 'challenge') {
			$this->challenge();
		} elseif($action == 'past') {
			$this->past();
		} elseif ($this->input->is_ajax_request()) {
			if ($action == 'slots') {
				$this->slots();
			} elseif ($action == 'participants') {
				$this->participants();
			} elseif ($this->input->server('REQUEST_METHOD') == 'POST' && $action == 'splash') {
				$this->splash();
			} elseif ($this->input->server('REQUEST_METHOD') == 'POST' && $action == 'pledge') {
				$this->pledge();
			} else {
				show_404();
			}
		} elseif ($this->input->server('REQUEST_METHOD') == 'POST' && $action == 'upload') {
			$this->upload();
		} elseif ($this->uri->segment(4)) {
			show_404();
		} 
	}
}