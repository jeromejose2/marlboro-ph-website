<?php if(!defined("BASEPATH")){ exit('No direct script access allowed!'); }

class Bid extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('perks_model', 'profile_model', 'points_model'));
	}
	
	public function index() {

		$data['bid_items'] = $this->perks_model->get_bid_items();
		$data['viewed'] = $this->perks_model->has_viewed_splash();
		$this->load->layout('perks/bid', $data); 
	}

	public function item($id = '') {
		if(!$id || !is_numeric($id)) {
			redirect('perks/bid');
		}
		$data = array();
		$item = (array) $this->perks_model->get_bid_item($id);
		$data['item'] = $item;
		$data['media'] = $this->perks_model->get_bid_item_media($id);
		$data['bidder'] = (array) $this->perks_model->get_bidder($id);
		if($item['has_ended']) {
			$this->load->view('perks/bid-item-ended', $data); 	
		} else {
			$this->load->view('perks/bid-item', $data); 
		}
		
	}

	public function confirm($id = '', $bid = '') {
		if(!$id || !$bid || !is_numeric($id) || !is_numeric($bid)) {
			redirect('perks/bid');
		}
		$data = array();
		$item = (array) $this->perks_model->get_bid_item($id);
		$data['item'] = $item;
		$data['media'] = $this->perks_model->get_bid_item_media($id);
		$data['bidder'] = (array) $this->perks_model->get_bidder($id);
		$this->load->view('perks/bid-confirm', $data); 	
	}

	public function bid_item() {
		if(!$this->input->post('id') || !is_numeric($this->input->post('id'))) {
			redirect('perks/bid');
		}
		$this->load->model('Registration_Model');
		$id = $this->input->post('id');
		$bidder = (array) $this->perks_model->get_bidder($id); 
		$item = (array) $this->perks_model->get_bid_item($id);
		$bid = $this->input->post('bid');
		$user = $this->Registration_Model->get_user_by_id($this->session->userdata('user_id'));
		$required_bid = $item['starting_bid'] >= @$bidder['bid'] ? $item['starting_bid'] : $bidder['bid'];
		$bid_data = $this->perks_model->check_bid_points($this->session->userdata('user_id'));
		$winner = $this->perks_model->get_user_won_bid();
		
		if(!is_numeric($bid)) {
			$data['success'] = 0;
			$data['error'] = 'Sorry, your bid is invalid.';	
		} elseif(!$item) {
			$data['success'] = 0;
			$data['error'] = 'Sorry, the item you are trying to bid for does not exist.';	
		} elseif($user['total_points'] < $bid) {
			$data['success'] = 0;
			$data['error'] = 'You don’t have enough points to bid for ' . $item['bid_item_name'] . '.';	
		} elseif($user['total_points'] >= $bid && $bid_data['available_points'] < $bid) {
			$data['success'] = 0;
			$data['error'] = 'You only have ' . $bid_data['available_points'] . ' points available for you to use. You placed a bid for ' . implode(', ', $bid_data['items']) . ', and ' . $bid_data['reserved_points'] . ' points are blocked off while you are still the highest bidder.';	
		} elseif(isset($bidder['registrant_id']) && $bidder['registrant_id'] == $this->session->userdata('user_id')) {
			$data['success'] = 0;
			$data['error'] = 'You are currently the highest bidder.';	
		} elseif(strtotime(date('Y-m-d H:i:s')) > strtotime($item['end_date']) || $item['status'] == 0) {
			$data['success'] = 0;
			$data['error'] = 'Sorry, this item is no longer active for bidding.';	
		} elseif(strtotime(date('Y-m-d H:i:s')) < strtotime($item['start_date'])) {
			$data['success'] = 0;
			$data['error'] = 'Sorry, this item is not yet active for bidding.';	
		} elseif($bid < $required_bid) {
			$data['success'] = 0;
			$data['error'] = 'Please place a bid higher than  ' . $required_bid . ' points.';	
		} elseif(@$bidder['bid'] >= $bid) {
			$data['success'] = 0;
			$data['error'] = 'Please place a bid higher than  ' . $bidder['bid'] . ' points.';
		} else {

			if(isset($bidder['registrant_id'])) {
				$this->load->model('notification_model');
				$message = 'You are no longer the highest bidder of ' . $item['bid_item_name'] . '. Should you wish to bid again, please do so here: <a href="' . BASE_URL . 'perks/bid/' . $item['bid_item_id'] . '">' . BASE_URL . 'perks/bid/' . $item['bid_item_id'] . '</a>';
				$param = array('message'=>$message,'suborigin'=>$id);
				$this->notification_model->notify($bidder['registrant_id'],PERKS_BID,$param);	
	
			}

			$sec_settings = $this->profile_model->get_row(array('table' => 'tbl_section_settings', 'where' => 'name = "PERKS_BID"'));
			$cell_id = $sec_settings->setting_section_id;

			$act_settings = $this->profile_model->get_row(array('table' => 'tbl_activity_settings', 'where' => 'name = "PERKS_BID"'));
			$act_id = $act_settings->setting_activity_id;
			
			$param = array('CellId' => $cell_id, 'ActionList' =>array(array('ActivityId'   => $act_id, 'ActionValue' => $item['bid_item_name'].'||'.$bid)));
			$response = $this->spice->trackAction($param);
			$response_json = $this->spice->parseJSON($response);

			if($response_json->MessageResponseHeader->TransactionStatus != 0){
					$data['success'] = 0;
					$data['error'] = '<h3>'.$response_json->MessageResponseHeader->TransactionStatusMessage.'</h3>';

					$data_error = array('origin_id'=>PERKS_BID,
							'method'=>'trackAction',
							'transaction'=>$this->router->method,
							'input'=> $this->session->userdata('spice_input'),
							'response'=>$response,
							'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
						);
					$this->spice->saveError($data_error);
			}else{
				$data['success'] = 1;
				$this->perks_model->save_bid();
			}
		}

		$this->output->set_content_type('application/json')
			 ->set_output(json_encode($data));
	} 

	public function get_bidder($id = '') {
		if(!$id || !is_numeric($id)) {
			redirect('perks/bid');
		}
		$bidder = (array) $this->perks_model->get_bidder($id);
		$data = $bidder;
		$this->output->set_content_type('application/json')
			 ->set_output(json_encode($data));
	}

	public function mechanics() {
		$mechanics = $this->perks_model->get_mechanics();
		$data['mechanics'] = $mechanics['description'];
		$data['what'] = 'Bid';
		$this->load->view('perks/mechanics-popup', $data); 
	}

	public function confirm_address() {

		if($this->input->post()){
			if($this->validate_offer_confirmation()){

				$user_id = $this->session->userdata('user_id');
				$user = $this->session->userdata('user');
				$this->load->model('notification_model');
				$bid = $this->get_won_bid_details($this->perks_model->get_user_won_bid());
				
 				if(isset($bid['bid_id'])) {
 					foreach ($bid['bid_id'] as $key => $value) {
 						$message = 'Congratulations, you won the bid for ' . $bid['name'][$key] . '. ' . $bid['bid'][$key] . '  points have been deducted from your account.<br><br>
									You will receive an official notification telegram to inform you on how to redeem your prize. Please ensure that all your contact details are up-to-date.<br><br>
									Continue logging on to MARLBORO.PH to earn more points!';
 						$param = array('message'=>$message,'suborigin'=>$value['bid_id']);
						$this->notification_model->notify($user_id,PERKS_BID,$param);	
 						
 						// $this->points_model->spend(PERKS_BID, array(
							// 						'points' => $bid['bid'][$key],
							// 						'suborigin' => $value,
							// 						'remarks' => '{{ name }} won the bid for ' . $bid['name'][$key] . ' on Perks',
							// 						'registrant'	=> $user_id
							// 						));	

 					}
 				}
 				
 				//SPICE

				$response = $this->spice->GetPerson(array('PersonId' => $this->spice->getMemberPersonId()));
				$spice_userinfo = $this->spice->parseJSON($response);
					
				if($spice_userinfo->MessageResponseHeader->TransactionStatus == 0)
				{
					$spice_userinfo = $spice_userinfo->ConsumerProfiles;

					$city = $this->db->select()
						->from('tbl_cities')
						->where(array('city_id'=>$this->input->post('city')))
						->limit(1)
						->get()
						->row();

					$province = $this->db->select()
						->from('tbl_provinces')
						->where(array('province_id'=>$this->input->post('province')))
						->limit(1)
						->get()
						->row();

					$city = $city ? $city->city : '';
					$province = $province ? $province->province : '';

					if(!$this->input->post('confirm_redemption')){
						//$this->profile_model->update('tbl_registrants',$this->input->post(),array('registrant_id'=>$user_id));
						$params = array('person_id'=> $this->spice->getMemberPersonId(),
	                    		'updates'=> array(
	                    				'AddressType' => array(
	                    					array(
	                    						'AddressLine1' => $this->input->post('street_name'),
												'Area' => $this->input->post('barangay'),
												'Locality' => $province,
										        'City' => $city, 
										        'Country' => 'PH',
										        'Premise'=>'PH',
										        'PostalCode' => $this->input->post('zip_code')
	                    					)
	                    				)
	                            )
	                    );

		        		$response = $this->spice->updatePerson($params);
		        		$response_json = $this->spice->parseJSON($response);
						//

		        		if($response_json->MessageResponseHeader->TransactionStatus != 0){
							$data = array('msg'=> '<h3>'.$response_json->MessageResponseHeader->TransactionStatusMessage.'</h3>',
										  	'error_holder'=>'',
											'error'=>false,
				 							'btn'=>'',
											'btn_text'=>'');
							$data_error = array('origin_id'=>PERKS_BID,
									'method'=>'ManagePerson',
									'transaction'=>$this->router->method,
									'input'=> $this->session->userdata('spice_input'),
									'response'=>$response,
									'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
								);
							$this->spice->saveError($data_error);
						}else{
							$data = array('msg'=>'You have successfully confirmed your mailing address!',
								  'error_holder'=>'',
								  'error'=>false,
	 							  'btn'=>'',
								  'btn_text'=>'');
							$this->perks_model->confirm_bid();
						}
					}
				}else{
					$data = array('msg'=> '<h3>'.$spice_userinfo->MessageResponseHeader->TransactionStatusMessage.'</h3>',
							  	'error_holder'=>'',
								'error'=>false,
	 							'btn'=>'',
								'btn_text'=>'');
					$data_error = array('origin_id'=>PERKS_BID,
							'method'=>'GetPerson',
							'transaction'=>$this->router->method,
							'input'=> $this->session->userdata('spice_input'),
							'response'=>$response,
							'response_message'=>$spice_userinfo->MessageResponseHeader->TransactionStatusMessage
						);
					$this->spice->saveError($data_error);
				}
			}else{

				$data = array('msg'=>trim(validation_errors()),
							  'error_holder'=>'bid-offer-error',
							  'error'=>true,
 							  'btn'=>'bid-offer-btn',
							  'btn_text'=>'<i>Submit Confirmation</i>');
			}
  			
  			$this->load->view('profile/submit-response',$data);
		}else{

			// $row = $this->profile_model->get_row(array('table'=>'tbl_registrants',
			// 												'where'=>array('registrant_id'=>$this->session->userdata('user_id'))
			// 												)
			// 										);
			// $data['row'] = $row;
			$spice_userinfo = $this->spice->GetPerson(array('PersonId' => $this->spice->getMemberPersonId()));
			$spice_userinfo = $this->spice->parseJSON($spice_userinfo);
			$spice_userinfo = $spice_userinfo->ConsumerProfiles;
			
			$data['spice_userinfos'] = $spice_userinfo;
			$data['cities'] =  $this->profile_model->get_rows(array('table'=>'tbl_cities','LOWER(province)'=>strtolower($spice_userinfo->Addresses[0]->Locality)));			
			$data['provinces'] = $this->profile_model->get_rows(array('table'=>'tbl_provinces'));
			$data['id'] = $this->input->get('id');
			$this->load->view('perks/confirm-bid-address',$data);

		}
		
	}

	private function validate_offer_confirmation()
	{

		$this->load->library('form_validation');
		$rules = array(array(
							 'field'   => 'prize_id',
							 'label'   => 'Birthday Offer',
							 'rules'   => 'callback_check_birthday_offer'
						  )
		   			);

		if(!$this->input->post('confirm_redemption')){

			$rules[] = array(array(
							 'field'   => 'street_name',
							 'label'   => 'Street Name',
							 'rules'   => 'trim|required'
						  ),
					   array(
							 'field'   => 'barangay',
							 'label'   => 'Barangay',
							 'rules'   => 'trim|required'
						  ),
					   array(
							 'field'   => 'zip_code',
							 'label'   => 'Zip Code',
							 'rules'   => 'trim|required'
						  ),
					   array(
							 'field'   => 'city',
							 'label'   => 'City',
							 'rules'   => 'trim|required'
						  ),
					   array(
							 'field'   => 'province',
							 'label'   => 'Province',
							 'rules'   => 'trim|required'
						  )
		   			);

		}

		$this->form_validation->set_rules($rules);		
		return $this->form_validation->run();
	}

	public function bid_notification() {
		$bids = $this->perks_model->get_user_won_bid();
		$bids_arr = $this->get_won_bid_details($bids);
		$data['bid'] = $bids_arr;
		$this->load->view('perks/bid-notification',$data);
	}

	private function get_won_bid_details($bids) {
		$total = 0;
		$bids_arr;
		if($bids) {
			foreach ($bids as $key => $value) {
				$bids_arr['name'][] = $value['bid_item_name'];
				$bids_arr['bid'][] = $value['bid'];
				$bids_arr['bid_id'][] = $value['bid_id'];
				$total += $value['bid'];
			}
			$bids_arr['total'] = $total;
		}
		return $bids_arr;
	}

	public function get_cities()
	{
 		$rows = $this->profile_model->get_rows(array('table'=>'tbl_cities',
 													'where'=>array('province_id'=>$this->input->get('province')),
 													'order_by'=>array('field'=>'city','order'=>'ASC'),
 													'group_by'=>array('city')
 													)
 												);

		$data['data'] = $rows->result_array();
		$this->load->view('profile/json_format',$data);

	}
}
