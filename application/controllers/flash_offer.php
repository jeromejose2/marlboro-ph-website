<?php if ( !defined( "BASEPATH" ) ) { exit( "No direct script access allowed!" ); }

class Flash_offer extends CI_Controller {

	var $prize_id = null;
	var $prize_stock = null;

	public function __construct() {
		parent::__construct();
		$this->load->model( 'profile_model' );
	}


	public function get_flash_offer() {
		$this->load->helper( 'file' );
		$prize_id = $this->input->get( 'prize_id' );
		$user = $this->session->userdata( 'user' );
		$data['type'] = $this->input->get( 'type' );
		$data['row'] = $this->profile_model->get_row( array( 'table'=> 'tbl_flash_offers', 'where'=>array( 'prize_id' =>$prize_id ) ) );
		$this->load->view( 'flash_offer/flash-offer', $data );
	}

	public function confirm_offer() {

		/**if ( $_SERVER['REMOTE_ADDR'] != "180.232.124.116" ) {
			//error message
			$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
			$data['msg'] = '<h3>Currently not available. Please try again...</h3>';
			$this->session->unset_userdata( 'flash_referral_offer_sess' );
			$this->load->view( 'profile/submit-response', $data );
			return;

			die();
		}**/

		if ( $this->input->post() ) {

			$validate_offer_ = false;
			if ( $this->validate_offer_confirmation() ) {
				$validate_offer_ = true;
				$type = $this->input->post( 'type' );
				$tbl_prizes = 'tbl_user_'.$type.'_prizes';


				$flash_offer = $this->profile_model->get_row( array( 'table'=> 'tbl_flash_offers',
						'where'=>array( 'CURDATE() >= DATE(start_date)'=>null,
							'CURDATE() <= DATE(end_date) '=>null,
							'stock >'=>0,
							'is_deleted'=>0,
							'category'=> $type=='flash' ? 1 : 2,
							'status'=>1
						)

					)
				);


				$user_id = $this->session->userdata( 'user_id' );

				if ( !$flash_offer && !(int)$user_id ) {
					//error message
					$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
					$data['msg'] = '<h3>No flash offer at this period of time.</h3>';
					$this->session->unset_userdata( 'flash_referral_offer_sess' );
					$this->load->view( 'profile/submit-response', $data );
					return;
				}

				if($flash_offer) {
					$flash_offer_provinces = $flash_offer->display_province ? explode(',', $flash_offer->display_province) : array();
					if( ! in_array($this->session->userdata('user_province_id'), $flash_offer_provinces)) {
						$data = array(
							'error_holder'=>'',
							'error'=>false,
							'btn'=>'',
							'btn_text'=>'',
							'msg'=>'<h3>Sorry, offer is not available.</h3>'
						);
						$this->session->unset_userdata('flash_referral_offer_sess');
						$this->load->view('profile/submit-response', $data);
						return;
					}				
				}
				

				/* *check status 10 times for same time clicking jeromejose 03-20-2015 */
				$update_status = false;
				$a = 1;
				while ( $a < 10 ) {


					$flash_offer = $this->profile_model->get_row( array( 'table'=> 'tbl_flash_offers',
							'where'=>array( 'CURDATE() >= DATE(start_date)'=>null,
								'CURDATE() <= DATE(end_date) '=>null,
								'stock >'=>0,
								'is_deleted'=>0,
								'category'=> $type=='flash' ? 1 : 2,
								'status'=>1
							)

						)
					);

					if ( !$flash_offer && !(int)$user_id ) {
						//error message
						$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
						$data['msg'] = '<h3>No flash offer at this period of time.</h3>';
						$this->session->unset_userdata( 'flash_referral_offer_sess' );
						$this->load->view( 'profile/submit-response', $data );
						return;
					}



					if ( !$flash_offer ) {
						//error message
						$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
						$data['msg'] = '<h3>Sorry, offer is no longer available.</h3>';
						$this->session->unset_userdata( 'flash_referral_offer_sess' );
						$this->load->view( 'profile/submit-response', $data );
						return;
					}else {
						$user_flash_prize = $this->profile_model->get_row( array( 'table'=> $tbl_prizes,
								'where'=>array( 'prize_id' =>$flash_offer->prize_id, 'registrant_id' =>$this->session->userdata( 'user_id' ) ),
								'fields'=>'user_flash_prize_id'
							)
						);
						if ( $user_flash_prize ) {
							//$this->form_validation->set_message('check_flash_offer','You have already confirmed the flash offer to your account.');
							//error message
							$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
							$data['msg'] = '<h3>Sorry, you can only redeem this flash offer once.</h3>';
							$this->session->unset_userdata( 'flash_referral_offer_sess' );
							$this->load->view( 'profile/submit-response', $data );
							return;
							//return false;
						}
					}

					if($flash_offer) {
						$flash_offer_provinces = $flash_offer->display_province ? explode(',', $flash_offer->display_province) : array();
						if( ! in_array($this->session->userdata('user_province_id'), $flash_offer_provinces)) {
							$data = array(
								'error_holder'=>'',
								'error'=>false,
								'btn'=>'',
								'btn_text'=>'',
								'msg'=>'<h3>Sorry, offer is not available.</h3>'
							);
							$this->session->unset_userdata('flash_referral_offer_sess');
							$this->load->view('profile/submit-response', $data);
							return;
						}				
					}

					if ( $a == 9 )//update the stock
						{

						$flash_offer = $this->profile_model->get_row( array( 'table'=> 'tbl_flash_offers',
								'where'=>array( 'CURDATE() >= DATE(start_date)'=>null,
									'CURDATE() <= DATE(end_date) '=>null,
									'stock >'=>0,
									'is_deleted'=>0,
									'category'=> $type=='flash' ? 1 : 2,
									'status'=>1
								)

							)
						);

						

						if ( !$flash_offer ) {
							//error message
							$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
							$data['msg'] = '<h3>Sorry, offer is no longer available.</h3>';
							$this->session->unset_userdata( 'flash_referral_offer_sess' );
							$this->load->view( 'profile/submit-response', $data );
							return;
						}


						if ( $flash_offer->stock == 0 ) {
							//error message
							$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
							$data['msg'] = '<h3>Sorry, offer is no longer available.</h3>';
							$this->session->unset_userdata( 'flash_referral_offer_sess' );
							$this->load->view( 'profile/submit-response', $data );
							return;
						}else {

							$user_flash_prize = $this->profile_model->get_row( array( 'table'=> $tbl_prizes,
									'where'=>array( 'prize_id' =>$flash_offer->prize_id, 'registrant_id' =>$this->session->userdata( 'user_id' ) ),
									'fields'=>'user_flash_prize_id'
								)
							);
							if ( $user_flash_prize ) {
								//$this->form_validation->set_message('check_flash_offer','You have already confirmed the flash offer to your account.');
								//error message
								$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
								$data['msg'] = '<h3>Sorry, you can only redeem this flash offer once.</h3>';
								$this->session->unset_userdata( 'flash_referral_offer_sess' );
								$this->load->view( 'profile/submit-response', $data );
								return;
								//return false;
							}

							$id = $this->profile_model->insert( $tbl_prizes, array( 'registrant_id'=>$this->session->userdata( 'user_id' ), 'prize_id'=>$flash_offer->prize_id ) );


							$new_stock = (int)$flash_offer->stock - 1;
							$this->profile_model->update( 'tbl_flash_offers', array( 'stock'=>$new_stock ), array( 'prize_id'=>$flash_offer->prize_id ) );
							$update_status = true;
							break;
						}

						if($flash_offer) {
						$flash_offer_provinces = $flash_offer->display_province ? explode(',', $flash_offer->display_province) : array();
							if( ! in_array($this->session->userdata('user_province_id'), $flash_offer_provinces)) {
								$data = array(
									'error_holder'=>'',
									'error'=>false,
									'btn'=>'',
									'btn_text'=>'',
									'msg'=>'<h3>Sorry, offer is not available.</h3>'
								);
								$this->session->unset_userdata('flash_referral_offer_sess');
								$this->load->view('profile/submit-response', $data);
								return;
							}				
						}						
					}
					$a++;
				}
				/**check status 10 times for same time clicking jfj 03-20-2015 */

			}
			if ( $update_status ) {
				if ( $validate_offer_ ) {
					//SPICE
					$response = $this->spice->GetPerson( array( 'PersonId' => $this->spice->getMemberPersonId() ) );
					$spice_userinfo = $this->spice->parseJSON( $response );

					if ( $spice_userinfo->MessageResponseHeader->TransactionStatus == 0 ) {
						$spice_userinfo = $spice_userinfo->ConsumerProfiles;

						$city = $this->db->select()
						->from( 'tbl_cities' )
						->where( array( 'city_id'=>$this->input->post( 'city' ) ) )
						->limit( 1 )
						->get()
						->row();

						$province = $this->db->select()
						->from( 'tbl_provinces' )
						->where( array( 'province_id'=>$this->input->post( 'province' ) ) )
						->limit( 1 )
						->get()
						->row();

						$city = $city ? $city->city : '';
						$province = $province ? $province->province : '';

						if ( date( 'Ymd' ) >= 20150401 ) {
							if ( $type=='flash' ) {
								$sec_settings = $this->profile_model->get_row( array( 'table' => 'tbl_section_settings', 'where' => 'name = "FLASH_OFFERS"' ) );

								$act_settings = $this->profile_model->get_row( array( 'table' => 'tbl_activity_settings', 'where' => 'name = "FLASH_OFFERS"' ) );
							}else {
								$sec_settings = $this->profile_model->get_row( array( 'table' => 'tbl_section_settings', 'where' => 'name = "REFERRAL_OFFERS"' ) );

								$act_settings = $this->profile_model->get_row( array( 'table' => 'tbl_activity_settings', 'where' => 'name = "REFERRAL_OFFERS"' ) );
							}
						}else {
							$sec_settings = $this->profile_model->get_row( array( 'table' => 'tbl_section_settings', 'where' => 'name = "FLASH_OFFERS"' ) );

							$act_settings = $this->profile_model->get_row( array( 'table' => 'tbl_activity_settings', 'where' => 'name = "FLASH_OFFERS"' ) );
						}

						if ( !$sec_settings || !$act_settings ) {

							if ( $type=='flash' ) {
								$data = array( 'msg'=>'Section/Activity setting is not configured.',
									'error_holder'=>'flash-offer-error',
									'error'=>true,
									'btn'=>'flash-offer-btn',
									'btn_text'=>'<i>Submit</i>' );
							}else {
								$data = array( 'msg'=>'Section/Activity setting is not configured.',
									'error_holder'=>'referral-offer-error',
									'error'=>true,
									'btn'=>'flash-offer-btn',
									'btn_text'=>'<i>Submit</i>' );
							}

							$this->load->view( 'profile/submit-response', $data );

							return;

						}else {

							$cell_id = $sec_settings->setting_section_id;
							$act_id = $act_settings->setting_activity_id;

						}

						$param = array( 'CellId' => $cell_id, 'ActionList' =>array( array( 'ActivityId'   => $act_id, 'ActionValue' => $flash_offer->prize_name ) ) );
						$response = $this->spice->trackAction( $param );
						$response_json = $this->spice->parseJSON( $response );

						// if ( $response_json->MessageResponseHeader->TransactionStatus != 0 ) {

					$track_action_status = FALSE;

					// $param = array('CellId' => $cell_id, 'ActionList' =>array(array('ActivityId'   => $act_id, 'ActionValue' => $flash_offer->prize_name)));
					// $response = $this->spice->trackAction($param);
					// $response_json = $this->spice->parseJSON($response);

					// if($response_json->MessageResponseHeader->TransactionStatus != 0){
					if($track_action_status === TRUE) {


					
							$data = array( 'msg'=> '<h3>'.$response_json->MessageResponseHeader->TransactionStatusMessage.'</h3>',
								'error_holder'=>'',
								'error'=>false,
								'btn'=>'',
								'btn_text'=>'' );

							if ( $type=='flash' ) {
								$data_error = array( 'origin_id'=>FLASH_OFFERS,
									'method'=>'trackAction',
									'transaction'=>$this->router->method,
									'input'=> $this->session->userdata( 'spice_input' ),
									'response'=>$response,
									'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
								);
							}else {
								if ( date( 'Ymd' ) >= 20150401 ) {
									$data_error = array( 'origin_id'=>REFERRAL_OFFERS,
										'method'=>'trackAction',
										'transaction'=>$this->router->method,
										'input'=> $this->session->userdata( 'spice_input' ),
										'response'=>$response,
										'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
									);
								}
							}

							$this->spice->saveError( $data_error );

						}else {

							if ( !$this->input->post( 'confirm_redemption' ) ) {

								$params = array( 'person_id'=> $this->spice->getMemberPersonId(),
									'updates'=> array(
										'AddressType' => array(
											array(
												'AddressLine1' => $this->input->post( 'street_name' ),
												'Area' => $this->input->post( 'barangay' ),
												'Locality' => $province,
												'City' => $city,
												'Country' => 'PH',
												'Premise'=>'PH',
												'PostalCode' => $this->input->post( 'zip_code' )
											)
										)
									)
								);

								$response = $this->spice->updatePerson( $params );
								$response_json = $this->spice->parseJSON( $response );


								if ( $response_json->MessageResponseHeader->TransactionStatus != 5 ) {
									$data = array( 'msg'=> '<h3>'.$response_json->MessageResponseHeader->TransactionStatusMessage.'</h3>',
										'error_holder'=>'',
										'error'=>false,
										'btn'=>'',
										'btn_text'=>'' );
									$data_error = array( 'origin_id'=>FLASH_OFFERS,
										'method'=>'ManagePerson',
										'transaction'=>$this->router->method,
										'input'=> $this->session->userdata( 'spice_input' ),
										'response'=>$response,
										'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
									);
									$this->spice->saveError( $data_error );

								}else {

									// $id = $this->profile_model->insert( $tbl_prizes, array( 'registrant_id'=>$this->session->userdata( 'user_id' ), 'prize_id'=>$flash_offer->prize_id ) );

									//$this->profile_model->update( 'tbl_flash_offers',array('stock'=>$new_stock),array('prize_id'=>$flash_offer->prize_id));

									$this->load->model( 'notification_model' );
									if ( $flash_offer->send_type=='Delivery' ) {
										if ( $type=='flash' ) {
											$message = 'Flash offer gift item from Marlboro will be delivered to your mailing address within 3-5 days.';
											$message_for_popup = 'You have successfully claimed the flash offer!<br><small>(Expect to receive your gift within the next month.)</small>';
										}else {
											/*referral*/
											$message = "CONGRATULATIONS!".
												"<p>You have successfully claimed your welcome gift from MARLBORO!".
												" We'll deliver it to your confirmed mailing address within a month.</p>".
												"<p>Continue earning points so you can redeem other cool prizes!</p>";
											$message_for_popup = "<h2>CONGRATULATIONS!</h2>".
											"<p>You have successfully claimed your welcome gift from MARLBORO!".
											" We'll deliver it to your confirmed mailing address within a month. </p>".
											"<p>Continue earning points so you can redeem other cool prizes!</p>";
										}
									}else {
										$message = 'Please follow the instruction below to redeem your flash offer gift item from Marlboro: <br/>'.$flash_offer->redemption_instruction;
										$message .= ' <br/>You can redeem your flash offer gift item from Marlboro at: <br/>'.$flash_offer->redemption_address;
										$message_for_popup = '<h3>You have successfully claimed the flash offer!<br><small>(Please check your notification to view the instruction and address of item redemption.)</h3>';
									}

									$origin = $type=='flash' ? FLASH_OFFERS : REFERRAL;

									$param = array( 'message'=>$message, 'suborigin'=>$id );
									$this->notification_model->notify( $user_id, $origin, $param );

									$data = array( 'error_holder'=>'',
										'error'=>false,
										'btn'=>'',
										'btn_text'=>'' );

									if ( $type=='flash' ) {
										// $data['msg'] = '<h3>You have successfully claimed the flash offer!</h3>';
										$data['msg'] = $message_for_popup;
									}else {
										/*referral*/
										// $data['msg'] = "<h2>CONGRATULATIONS!</h2>".
										// 	"<p>You have successfully claimed your welcome gift from MARLBORO!".
										// 	" We'll deliver it to your confirmed mailing address within a month. </p>".
										// 	"<p>Continue earning points so you can redeem other cool prizes!</p>";
										$data['msg'] = $message_for_popup;
									}
								}
							}else {
								//$id = $this->profile_model->insert( $tbl_prizes, array( 'registrant_id'=>$this->session->userdata( 'user_id' ), 'prize_id'=>$flash_offer->prize_id ) );
								//$this->profile_model->update( 'tbl_flash_offers',array('stock'=>$new_stock),array('prize_id'=>$flash_offer->prize_id));

								$this->load->model( 'notification_model' );
								if ( $flash_offer->send_type=='Delivery' ) {
									if ( $type=='flash' ) {
										$message = 'Flash offer gift item from Marlboro will be delivered to your mailing address within 3-5 days.';
									}else {
										/*referral*/
										$message = "CONGRATULATIONS!".
											"<p>You have successfully claimed your welcome gift from MARLBORO!".
											" We'll deliver it to your confirmed mailing address within a month. </p>".
											"<p>Continue earning points so you can redeem other cool prizes!</p>";
									}
								}else {
									$message = 'Please follow the instruction below to redeem your flash offer gift item from Marlboro: <br/>'.$flash_offer->redemption_instruction;
									$message .= ' <br/>You can redeem your flash offer gift item from Marlboro at: <br/>'.$flash_offer->redemption_address;
									$message .= '<br><small>(Please see your notification to see the instruction and address of item redemption.)</small>';
								}

								$origin = $type=='flash' ? FLASH_OFFERS : REFERRAL;

								$param = array( 'message'=>$message, 'suborigin'=>$id );
								$this->notification_model->notify( $user_id, $origin, $param );

								$data = array( 'error_holder'=>'',
									'error'=>false,
									'btn'=>'',
									'btn_text'=>'' );

								if ( $type=='flash' ) {
									$data['msg'] = '<h3>You have successfully claimed the flash offer!</h3>';
								}else {
									/*referral*/
									$data['msg'] = "<h2>CONGRATULATIONS!</h2>".
										"<p>You have successfully claimed your welcome gift from MARLBORO!".
										" We'll deliver it to your confirmed mailing address within a month. </p>".
										"<p>Continue earning points so you can redeem other cool prizes!</p>";
								}
							}
						}
					}else {
						$data = array( 'msg'=> '<h3>'.$spice_userinfo->MessageResponseHeader->TransactionStatusMessage.'</h3>',
							'error_holder'=>'',
							'error'=>false,
							'btn'=>'',
							'btn_text'=>'' );

						$data_error = array( 'origin_id'=>FLASH_OFFERS,
							'method'=>'GetPerson',
							'transaction'=>$this->router->method,
							'input'=> $this->session->userdata( 'spice_input' ),
							'response'=>$response,
							'response_message'=>$spice_userinfo->MessageResponseHeader->TransactionStatusMessage
						);
						$this->spice->saveError( $data_error );
					}
				}else {

					$data = array( 'msg'=>trim( validation_errors() ),
						'error_holder'=>'flash-offer-error',
						'error'=>true,
						'btn'=>'flash-offer-btn',
						'btn_text'=>'<i>Submit Confirmation</i>' );
				}

				$this->session->unset_userdata( 'flash_referral_offer_sess' );

				$this->load->view( 'profile/submit-response', $data );
			}else {
				//error message
				$data = array( 'error_holder'=>'', 'error'=>false, 'btn'=>'', 'btn_text'=>'' );
				$data['msg'] = '<h3>Sorry, offer is no longer available.</h3>';
				$this->session->unset_userdata( 'flash_referral_offer_sess' );
				$this->load->view( 'profile/submit-response', $data );
				return;
			}
		}else {
			$spice_userinfo = $this->spice->GetPerson( array( 'PersonId' => $this->spice->getMemberPersonId() ) );
			$spice_userinfo = $this->spice->parseJSON( $spice_userinfo );
			$spice_userinfo = $spice_userinfo->ConsumerProfiles;

			$data['spice_userinfos'] = $spice_userinfo;
			$data['cities'] =  $this->profile_model->get_rows( array( 'table'=>'tbl_cities', 'LOWER(province)'=>strtolower( $spice_userinfo->Addresses[0]->Locality ) ) );
			$data['provinces'] = $this->profile_model->get_rows( array( 'table'=>'tbl_provinces' ) );
			$this->load->view( 'flash_offer/confirm-birthday-offer', $data );
		}

	}

	private function validate_offer_confirmation() {

		$this->load->library( 'form_validation' );
		$rules = array( array(
				'field'   => 'prize',
				'label'   => 'Flash Offer',
				'rules'   => ''//'callback_check_flash_offer'
			)
		);

		if ( !$this->input->post( 'confirm_redemption' ) ) {

			$rules[] = array( array(
					'field'   => 'street_name',
					'label'   => 'Street Name',
					'rules'   => 'trim|required'
				),
				array(
					'field'   => 'barangay',
					'label'   => 'Barangay',
					'rules'   => 'trim|required'
				),
				array(
					'field'   => 'zip_code',
					'label'   => 'Zip Code',
					'rules'   => 'trim|required'
				),
				array(
					'field'   => 'city',
					'label'   => 'City',
					'rules'   => 'trim|required'
				),
				array(
					'field'   => 'province',
					'label'   => 'Province',
					'rules'   => 'trim|required'
				),
				array(
					'field'   => 'prize',
					'label'   => 'Flash Offer',
					'rules'   => ''//'callback_check_flash_offer'
				)
			);

		}

		$this->form_validation->set_rules( $rules );
		return $this->form_validation->run();
	}

	public function check_flash_offer() {
		$this->load->library( 'form_validation' );
		$type = $this->input->post( 'type' );
		$tbl_prizes = 'tbl_user_'.$type.'_prizes';

		$flash_offer = $this->profile_model->get_row( array( 'table'=> 'tbl_flash_offers',
				'where'=>array( 'status'=>1, 'CURDATE() >= DATE(start_date)'=>null, 'CURDATE() <= DATE(end_date) '=>null, 'stock >'=>0, 'is_deleted'=>0 ),
				'fields'=>'prize_id,stock'

			)
		);

		if ( $flash_offer && (int)$this->session->userdata( 'user_id' ) ) {
			$user_flash_prize = $this->profile_model->get_row( array( 'table'=> $tbl_prizes,
					'where'=>array( 'prize_id' =>$flash_offer->prize_id, 'registrant_id' =>$this->session->userdata( 'user_id' ) ),
					'fields'=>'user_flash_prize_id'
				)
			);
			if ( $user_flash_prize ) {
				$this->form_validation->set_message( 'check_flash_offer', 'Sorry, you can only redeem this flash offer once.' );
				return false;
			}else {
				return true;
			}


		}else {

			$this->form_validation->set_message( 'check_flash_offer', 'No flash offer at this period of time.' );
			return false;

		}
	}

	public function get_cities() {
		$rows = $this->profile_model->get_rows( array( 'table'=>'tbl_cities',
				'where'=>array( 'province_id'=>$this->input->get( 'province' ) ),
				'order_by'=>array( 'field'=>'city', 'order'=>'ASC' ),
				'group_by'=>array( 'city' )
			)
		);

		$data['data'] = $rows->result_array();
		$this->load->view( 'profile/json_format', $data );

	}
}
