<?php if(!defined("BASEPATH")){ exit('No direct script access allowed!'); }

class Messages extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('profile_model');
	}

	public function compose()
	{

		if($this->input->post()){

			if($this->validate_message_form()){

				$data = array_merge($this->input->post(),array('sender_id'=>$this->session->userdata('user_id'),'message_status'=>0));
				$this->profile_model->insert('tbl_messages',$data);

				$data = array('msg'=>'<h3>Your message has been successfully submitted.</h3>',
							  'error_holder'=>'',
							  'error'=>false,
							  'btn'=>'',
							  'btn_text'=>'');

			}else{

				$data = array('msg'=>validation_errors(),
							  'error_holder'=>'send-message-form-error-holder',
							  'error'=>true,
 							  'btn'=>'send-message-form-button',
							  'btn_text'=>'<i>SEND</i>');

			}

			$this->load->view('profile/submit-response',$data);

		}else{

			$id = $this->input->get('participant');
			$data['registrant'] = $this->profile_model->get_row(array('table'=>'tbl_registrants',
																		'where'=>array('registrant_id'=>$id)
																	)
																);
			$data['move_forward_id'] = $this->input->get('move_forward');
			$this->load->view('messages/compose-message',$data);


		}
		
	}

	public function validate_message_form()
	{

		$this->load->library('form_validation');
		$rules = array(array('field'=>'recipient_id','label'=>'Recipient','rules'=>'required|callback_validate_participant'),
						array('field'=>'subject','label'=>'Subject','rules'=>'required'),
						array('field'=>'message','label'=>'Message','rules'=>'required')
					);
		$this->form_validation->set_rules($rules);
		return $this->form_validation->run();

	}

	public function validate_participant($participant_id)
	{

		$move_forward_id = $this->input->post('move_forward_id');
		$row = $this->profile_model->get_row(array('table'=>'tbl_move_forward_choice',
													'where'=>array('registrant_id'=>$participant_id,'move_forward_id'=>$move_forward_id)
												)
											);
		if(!$row){
			$this->form_validation->set_message('validate_participant','Invalid participant.');
			return false;
		}else{
			return true;
		}

	}

	public function thank_you()
	{
		$data = array('message'=>'Your message has been successfully submitted.','redirect'=>'');
		$this->load->view('about/thank-you-popup',$data);
	}


	public function inbox()
	{

 		$user_id = $this->session->userdata('user_id');
 		$message_id = $this->uri->segment(3);
 

 		if($message_id){

 			$this->profile_model->update('tbl_messages',array('message_status'=>1),array('recipient_id'=>$user_id));
 			$data['message_id'] =  $message_id;
 			$this->load->layout('messages/message-content',$data);

 		}else{
 			
 			
			$rows = $this->profile_model->get_rows(array('table'=>'tbl_messages',
														 'where'=>array('message_reply_to_id'=>0,'is_deleted_by_sender !='=>$user_id,'is_deleted_by_recipient !='=>$user_id,"(recipient_id = '$user_id' OR sender_id = '$user_id') "=>null),
 														 'fields'=>'subject,message_id'
														)
													);
			$messages = array();

			if($rows){

				foreach($rows->result() as $v){

					$message_id = $v->message_id;
					$row = $this->profile_model->get_rows(array('table'=>'tbl_messages as m',
																 'where'=>array("(m.message_reply_to_id = '".$message_id."' OR m.message_id = '".$message_id."') "=>null),
																 'order_by'=>array('field'=>'m.message_date_sent','order'=>'DESC'),
																 'join'=>array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=m.sender_id'),
																 'fields'=>'r.first_name,r.middle_initial,r.third_name,m.message_status,m.message,m.message_date_sent,m.recipient_id'
														)
													)->row();

					if($row){

						$messages[] = array('message_id'=>$message_id,
										'subject'=>$v->subject,
										'sender_name'=>$row->first_name.' '.$row->middle_initial.' '.$row->third_name,
										'message'=>$row->message,
										'status'=>($row->recipient_id==$user_id) ? $row->message_status : 1,
										'date_sent'=>$row->message_date_sent
										);

					}

					
				}

			}

			$data['rows'] = json_decode(json_encode($messages),false);			
			$this->load->layout('messages/inbox',$data);

 		} 		 
		 
	}

	public function delete_messages()
	{

		$message_ids = $this->input->post('messages');
		$user_id = $this->session->userdata('user_id');

		if($message_ids){
			foreach($message_ids as $v){
				$row = $this->profile_model->get_row(array('table'=>'tbl_messages',
															'where'=>array('message_id'=>$v,"(recipient_id = '$user_id' OR sender_id = '$user_id') "=>null)
															)
													);

				if($row){
					$update = ($user_id==$row->sender_id) ? array('is_deleted_by_sender'=>$user_id) : array('is_deleted_by_recipient'=>$user_id);
					$this->profile_model->update('tbl_messages',$update,array('message_id'=>$v));
				}
				
			}
		}
		redirect('messages/inbox');

	}


	public function getTemplate()
	{
		$this->load->view('messages/messages-template-hb');
	}

	public function totalUnreadMessages()
	{
		$user_id = $this->session->userdata('user_id');
		$rows = $this->profile_model->get_rows(array('table'=>'tbl_messages',
													 'where'=>array('is_deleted_by_sender !='=>$user_id,
													 				'is_deleted_by_recipient !='=>$user_id,
													 				'message_status'=>0,
													 				'recipient_id'=>$user_id,
													 			),
													 'fields'=>'subject,message_id',
													 'group_by'=>'subject'
														)
													);
		$data['total_rows'] = $rows->num_rows();
		$data['user_id'] = $user_id;
 		$this->load->view('profile/json_format',array('data'=>$data));

	}
 

}