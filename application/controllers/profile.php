<?php if(!defined("BASEPATH")){ exit("No direct script access allowed!"); }

class Profile extends CI_Controller {

	
	public function __construct()
	{
		parent:: __construct();
 
		$this->load->model('profile_model');
		$this->load->model('registration_model');
	}

	public function index()
	{	
		$spice_userinfo = $this->spice->GetPerson(array('PersonId' =>$this->spice->getMemberPersonId()));
		$spice_userinfo = $this->spice->parseJSON($spice_userinfo);
		$spice_userinfo = ($spice_userinfo) ? $spice_userinfo->ConsumerProfiles : false;

		//$spice_brands = $this->spice->getBrand();
		
 		$user_id = $this->session->userdata('user_id');
     
		$row = $this->profile_model->get_rows(array('fields'=>'a.*, c.city as city_name, b.province as province_name',
													'table'=>'tbl_registrants as a',
													'where'=>array('a.registrant_id'=>$user_id),
													'join'=>array(array('table'=>'tbl_cities as c','on'=>'c.city_id=a.city','type'=>'left'),
																  array('table'=>'tbl_provinces as b','on'=>'b.province_id=a.province', 'type'=>'left')
															),
													)
											)->row(); 

		//echo @$spice_userinfo->Addresses[0]->AddressLine3.'----';

		$prov = ($spice_userinfo->Addresses[0]->Locality) ? $spice_userinfo->Addresses[0]->Locality : '';

		$total_derivations = count($spice_userinfo->Derivations);
						
		for($i = 0; $i<$total_derivations; $i++){
				if($spice_userinfo->Derivations[$i]->AttributeCode=='ca_nickname'){
					$nick_name = $spice_userinfo->Derivations[$i]->AttributeValue ? $spice_userinfo->Derivations[$i]->AttributeValue : $this->session->userdata('user')['first_name'];
				}

				if($spice_userinfo->Derivations[$i]->AttributeCode=='ca_reaction'){
					$reaction = $spice_userinfo->Derivations[$i]->AttributeValue;
				}

				if($spice_userinfo->Derivations[$i]->AttributeCode=='ca_referral_code'){
					$referral_code = $spice_userinfo->Derivations[$i]->AttributeValue;
				}
		}

		$data['user_id'] = $user_id;
 		$data['row'] = $row;
 		$data['user_info'] = $spice_userinfo;
 		$data['nick_name'] = $nick_name;
 		$data['reaction'] = $reaction;
 		$data['referral_code'] = $referral_code;
  		$data['spice_brands'] = $this->profile_model->get_rows(array('table'=>'tbl_brands_spice', 'where'=>array('consumer_flag'=> 'true')))->result(); //($spice_brands) ? $spice_brands : false;
 		$data['cities'] =  $this->profile_model->get_rows(array('table'=>'tbl_cities','LOWER(province)'=>strtolower($prov)));		
		$data['provinces'] = $this->profile_model->get_rows(array('table'=>'tbl_provinces'));
 		$data['brands'] = $this->profile_model->get_rows(array('table'=>'tbl_brands'));
 		$data['alternate_purchase'] = $this->profile_model->get_rows(array('table'=>'tbl_alternate_purchase'));
 		$data['statements'] = $this->profile_model->get_rows(array('table'=>'tbl_statements','where'=>array('is_deleted'=>0,'status'=>1)));
		$user_statements = $this->profile_model->get_rows(array('table'=>'tbl_user_statements',
		 																'where'=>array('registrant_id'=>$user_id),
		 																'order_by'=>array('field'=>'date_created','order'=>'DESC'),
		 																'limit'=>1
		 																)
																)->row();
 		$user_statement_ids = array(0);

 		if($user_statements){

  			$user_statement_ids = explode(',',$user_statements->statement_id);
 		}

 		$data['user_statements'] = $this->profile_model->get_rows(array('fields'=>'statement_id,statement',
  																	'table'=>'tbl_statements',
  																	'where'=>array('where_in'=>array('field'=>'statement_id','arr'=>$user_statement_ids)
  																				  )
  																	)
  															);

 		$user_statements = $this->profile_model->get_rows(array('table'=>'tbl_user_statements',
		 																'where'=>array('registrant_id'=>$user_id),
		 																'order_by'=>array('field'=>'date_created','order'=>'DESC'),
		 																'limit'=>1
		 																)
																)->row();
 		$user_statement_ids = array(0);

 		if($user_statements){

  			$user_statement_ids = explode(',',$user_statements->statement_id);
 		}

 		$data['user_statements'] = $this->profile_model->get_rows(array('fields'=>'statement_id,statement',
  																	'table'=>'tbl_statements',
  																	'where'=>array('where_in'=>array('field'=>'statement_id','arr'=>$user_statement_ids)
  																				  )
  																	)
  															);
 		$this->load->layout('profile/profile',$data);
 		//$this->output->enable_profiler(TRUE);
		
	}
 

	public function update_profile()
	{		
		if($this->input->post()){

			$post_data = $this->input->post();

			if($this->validate_profile()){
				
				$response = $this->spice->GetPerson(array('PersonId' =>$this->spice->getMemberPersonId()));
				$spice_userinfo = $this->spice->parseJSON($response);

				if($spice_userinfo->MessageResponseHeader->TransactionStatus == 0)
				{
					$spice_userinfo = $spice_userinfo->ConsumerProfiles;
					
					if(isset($post_data['password']) && $post_data['old_password']){

						//CHANGE PASSWORD
						$params = array(
								'LoginName' => strtolower($spice_userinfo->Login[0]->LoginName),
								'NewPassword' => $post_data['password'],
								'CurrentPassword' => $post_data['old_password']
								);

						$response = $this->spice->ChangePassword($params);
						$response_json = $this->spice->parseJSON($response);
						
						if ($response_json->MessageResponseHeader->TransactionStatus==0) {
							$data = array('msg'=>'<h3>'.$response_json->MessageResponseHeader->TransactionStatusMessage.'</h3>', 'btn' => 'update-profile', 'btn_text' => '<i>SAVE</i>', 'error_holder'=>'','error'=>false);
						}else{
							$data = array('msg'=>'<h3>'.$response_json->MessageResponseHeader->TransactionStatusMessage.'</h3>', 'btn' => 'update-profile', 'btn_text' => '<i>SAVE</i>', 'error_holder'=>'','error'=>true);
							
							$data_error = array('origin_id'=>MY_PROFILE,
									'method'=>'ChangePassword',
									'transaction'=>$this->router->method,
									'input'=> $this->session->userdata('spice_input'),
									'response'=>$response,
									'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
								);
							$this->spice->saveError($data_error);
						}
					}else{
						unset($post_data['password']);
					}

					$city = $this->db->select()
						->from('tbl_cities')
						->where(array('city_id'=>$this->input->post('city')))
						->limit(1)
						->get()
						->row();

					$province = $this->db->select()
						->from('tbl_provinces')
						->where(array('province_id'=>$this->input->post('province')))
						->limit(1)
						->get()
						->row();

					$city = $city ? $city->city : '';
					$province = $province ? $province->province : '';
					$referral_code = $this->registration_model->referral_code();

					$total_derivations = count($spice_userinfo->Derivations);
						
					for($i = 0; $i<$total_derivations; $i++){
							if($spice_userinfo->Derivations[$i]->AttributeCode=='ca_nickname'){
								$spice_userinfo->Derivations[$i]->AttributeValue = $post_data['nick_name'];
							}

							if($spice_userinfo->Derivations[$i]->AttributeCode=='ca_reaction'){
								if($post_data['alternate_purchase_indicator'])
									$spice_userinfo->Derivations[$i]->AttributeValue = $post_data['alternate_purchase_indicator'];
								else
									$spice_userinfo->Derivations[$i]->AttributeValue = '0';	
							}

							if($spice_userinfo->Derivations[$i]->AttributeCode=='ca_referral_code'){
								//if($spice_userinfo->Derivations[$i]->AttributeValue == '0'){
									$spice_userinfo->Derivations[$i]->AttributeValue = $spice_userinfo->Derivations[$i]->AttributeValue;//$referral_code ;
								//}
							}
					}
					
					$Derivations = json_decode(json_encode($spice_userinfo->Derivations),true);

					$MultibrandSmoker = $post_data['current_brand'] !== $post_data['first_alternate_brand'] ? '1' : '0';

					$params = array('person_id'=> $this->spice->getMemberPersonId(),
	                        		'updates'=> array(
	                        				'PersonalDetails' => array(
	                        					'FirstName' => $spice_userinfo->PersonDetails->FirstName,
						                        'MiddleName'=> $spice_userinfo->PersonDetails->MiddleName,
						                        'LastName' => $spice_userinfo->PersonDetails->LastName,
						                        'Gender' =>  $post_data['gender'],
										        'MobileTelephoneNumber' => ltrim($post_data['mobile_phone'], '0'),
											    'EmailAddress'=> $spice_userinfo->PersonDetails->EmailAddress,
						                        'DateOfBirth' => $spice_userinfo->PersonDetails->DateOfBirth
	                        				),
	                        				'AddressType' => array(
	                        					array(
	                        						'AddressLine1' => $this->input->post('street_name'),
													'Area' => $this->input->post('barangay'),
													'Locality' => $province,
											        'City' => $city, 
											        'Country' => 'PH',
											        'Premise'=>'PH',
											        'PostalCode' => $this->input->post('zip_code')
	                        					)
	                        				),
	                                        'Derivations'=>$Derivations,
	                                        'BrandPreferences' => array(
												'PrimaryBrand' => array(
													'BrandId' => $post_data['current_brand']
												),
												'SecondaryBrand' => array(
													'BrandId' => $post_data['first_alternate_brand']
												),
												'MultibrandSmoker'=>$MultibrandSmoker
											)
	                                )
						);

	        		$response = $this->spice->updatePerson($params);
	        		$response_json = $this->spice->parseJSON($response);

					if ($response_json->MessageResponseHeader->TransactionStatus==5) {
						$data = array('msg'=>'<h3>Successfully updated your profile.</h3>', 'btn' => 'update-profile', 'btn_text' => '<i>SAVE</i>', 'error_holder'=>'','error'=>false);

						/*for($i = 0; $i<$total_derivations; $i++){
								if($spice_userinfo->Derivations[$i]->AttributeCode=='ca_referral_code'){
									if($spice_userinfo->Derivations[$i]->AttributeValue == '0'){
										// $insert_new_user = array('referral_code' => (string)$referral_code);
			 			
							 		// 	$this->db->where('person_id', $this->spice->getMemberPersonId());
										// $this->db->update('tbl_registrants', $insert_new_user); 
									}
								}
						}*/
												
					}else{
						$data = array('msg'=>'<h3>Failed to update your profile.</h3>', 'btn' => 'update-profile', 'btn_text' => '<i>SAVE</i>', 'error_holder'=>'','error'=>true);

						$data_error = array('origin_id'=>MY_PROFILE,
								'method'=>'ManagePerson',
								'transaction'=>$this->router->method,
								'input'=> $this->session->userdata('spice_input'),
								'response'=>$response,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
						$this->spice->saveError($data_error);
					}
				}else{
					$data = array('msg'=>'<h3>'.$spice_userinfo->MessageResponseHeader->TransactionStatusMessage.'</h3>', 'btn' => 'update-profile', 'btn_text' => '<i>SAVE</i>', 'error_holder'=>'','error'=>true);

					$data_error = array('origin_id'=>MY_PROFILE,
							'method'=>'GetPerson',
							'transaction'=>$this->router->method,
							'input'=> $this->session->userdata('spice_input'),
							'response'=>$response,
							'response_message'=>$spice_userinfo->MessageResponseHeader->TransactionStatusMessage
						);
					$this->spice->saveError($data_error);
				}
			}else{
				$data = array('msg'=>trim(validation_errors()), 'btn' => 'update-profile', 'btn_text' => '<i>SAVE</i>', 'error_holder'=>'profile-error','error'=>true);
			}

		}

		$this->load->view('profile/submit-response',$data);
 
	}

	public function update_statements()
	{

		if($this->input->post()){


			if(count($this->input->post('user_statements')) >= 1 && count($this->input->post('user_statements')) <= 3){

				$this->load->model('Points_Model');
				$data = array('statement_id'=>implode(',',$this->input->post('user_statements')).',',
							  'registrant_id'=>$this->session->userdata('user_id'),
							  'date_modified'=>array('field'=>'date_created','value'=>'NOW()')
							  );

 				$user_statement_id = $this->profile_model->insert('tbl_user_statements',$data);
 				$this->Points_Model->earn(USER_STATEMENT, array('suborigin' => $user_statement_id));
 				$data = array('msg'=>'Your Maybe statement/s has been successfully updated.',
 								'error_holder'=>'',
 								'error'=>false,
 								'user_statement_id'=>$user_statement_id
 								);

			}else{
				$data = array('msg'=>'You must select at least 1 or up to 3 maybe statements.',
							  'error_holder'=>'maybe-statements-error',
							  'error'=>true,
							  'user_statement_id'=>'',
							  'btn'=>'maybe-btn',
							  'btn_text'=>'<i>SUBMIT</i>');
			}

			$this->load->view('profile/submit-response',$data);

		}
	}

	public function get_cities()
	{
 		$rows = $this->profile_model->get_rows(array('table'=>'tbl_cities',
 													'where'=>array('province_id'=>$this->input->get('province')),
 													'order_by'=>array('field'=>'city','order'=>'ASC'),
 													'group_by'=>array('city')
 													)
 												);

		$data['data'] = $rows->result_array();
		$this->load->view('profile/json_format',$data);

	}

	public function validate_profile()
	{

		$this->load->library('form_validation');

		$rules = array(
		   array(
				 'field'   => 'nick_name',
				 'label'   => 'Nick name',
				 'rules'   => 'trim|required|alpha_space'
			  ),
		   array(
				 'field'   => 'mobile_phone',
				 'label'   => 'Mobile number',
				 'rules'   => 'trim|required|numeric|min_length[11]|max_length[11]|callback_check_mobile_prefix'
			  ),
		   array(
				 'field'   => 'street_name',
				 'label'   => 'Street Name',
				 'rules'   => 'trim|required'
			  ),
		   array(
				 'field'   => 'barangay',
				 'label'   => 'Barangay',
				 'rules'   => 'trim|required'
			  ),
		   array(
				 'field'   => 'province',
				 'label'   => 'Province',
				 'rules'   => 'trim|required'
			  ),
		   array(
				 'field'   => 'city',
				 'label'   => 'City',
				 'rules'   => 'trim|required'
			  ),
		   array(
				 'field'   => 'zip_code',
				 'label'   => 'Zip Code',
				 'rules'   => 'trim|required'
			  ),
		   array(
				 'field'   => 'current_brand',
				 'label'   => 'Current brand',
				 'rules'   => 'trim|required'
			  ),
		   array(
				 'field'   => 'first_alternate_brand',
				 'label'   => 'First alternate brand',
				 'rules'   => 'trim|required'
			  ) 
		   ,
		   array(
				 'field'   => 'alternate_purchase_indicator',
				 'label'   => 'Out of stock reaction',
				 'rules'   => 'trim|required'
			  ) 
		);



		if($this->input->post('password') || $this->input->post('cpassword')){
			$rules[] = array('field'=>'old_password','label'=>'Current Password','rules'=>'callback_password_check');
			$rules[] = array('field'=>'password','label'=>'New password','rules'=>'required|matches[cpassword]|min_length[8]|callback_password_strength');
			$rules[] = array('field'=>'cpassword','label'=>'Confirm New Password','rules'=>'required');
		}

		//$this->form_validation->set_error_delimiters('','');
		$this->form_validation->set_rules($rules);
		
		return $this->form_validation->run();

	}

	public function check_mobile_prefix($mobile_prefix)
	{
		$row = $this->profile_model->get_row(array('table'=>'tbl_mobile_prefix','where'=>array('mobile_prefix'=>substr($mobile_prefix,0,4))));
		if(!$row){
			$this->form_validation->set_message('check_mobile_prefix','Invalid mobile prefix.');
			return false;
		}else{
			return true;
		}
	}

	public function password_check($str)
	{
		$spice_userinfos = $this->spice->SearchPerson(array('PersonId' =>$this->spice->getMemberPersonId()));
		$spice_userinfos = $this->spice->parseJSON($spice_userinfos);
		$spice_user = $spice_userinfos->MessageResponseHeader->TransactionStatusMessage;

		$spice_userinfo = $spice_userinfos->ConsumerProfiles[0];
		$email = $spice_userinfo->Login[0]->LoginName;

		// print_r($email); print_r($str); die();

		if ($spice_user != 'No record found.')
		{
 			if(!$this->spice->loginMember($email, $str))
			{
				$this->form_validation->set_message('password_check','Invalid current password.');
				return false;
			}else if($str == $this->input->post('password')){
				$this->form_validation->set_message('password_check','New password cannot be the same as old.');
				return false;
			}

		}else{

			$this->form_validation->set_message('password_check','Invalid user.');
			return false;

		}

		return true;

	}

	public function password_strength($password)
	{

 
 		$count = 0;
  		$patterns =  array('lowercase'=>'lowercase','uppercase'=>'uppercase','digit'=>'digit','special character'=>'special character');
 		$user = $this->session->userdata('user');		
		

		if(preg_match('/[a-z]/',$password)){
			$count++;
			unset($patterns['lowercase']);
 		}

		if(preg_match('/[A-Z]/',$password)){
			$count++;
			unset($patterns['uppercase']);
 		}
		

		if(preg_match('/\d/',$password)){
			$count++;
			unset($patterns['digit']);
 		}

		if(preg_match('/\W/',$password)){
			$count++;
			unset($patterns['special character']);
 		}
 		  

		if($count < 3){

			$last = array_pop($patterns); 
			$validation_message = 'Your password must contain from the following: '.implode(',',$patterns).' or '.$last.'</br>';
 			$this->form_validation->set_message('password_strength',$validation_message);
			return false;
		}
		  

		if(preg_match('/user|guest|admin|sys|test|pass|super|'.$user['first_name'].'|'.$user['last_name'].'/i',$password,$matches)){
 			$validation_message = 'Your new password must not contained from the following: user,guest,admin,sys,test,pass,super,'.strtolower($user['first_name']).' or '.strtolower($user['last_name']);
 			$this->form_validation->set_message('password_strength',$validation_message);
 			return false;
   		}

   		return true;
   		 
	}

	public function change_photo()
	{

		$this->load->helper(array('upload','resize','file'));

		$id = $this->session->userdata('user_id');

		$filename = md5($id.uniqid());
		$path = 'uploads/profile/'.$id;		
		$file_ext = array('png','jpg','gif');
		$length = count($file_ext);
		$count = 0;
		$temp = true;

		while($temp){

 			foreach($file_ext as $v){

				if(!file_exists($path.'/'.$filename.'.'.$v)){
					$count++;
				}
				
			}

			if($count==$length){				
				$temp = false;
			}else{
				$filename = md5($id.uniqid());
				$count = 0;
			}

		}


		if(!is_dir($path)){
			mkdir($path,0777,true);
		}


		$max_size = 3072;		 

		$params = array('upload_path'=>$path,'allowed_types'=>FILE_UPLOAD_ALLOWED_TYPES,'file_name'=>$filename,'max_size'=>$max_size,'max_width'=>0,'max_height'=>0,'do_upload'=>'user_photo');
	    $file = upload($params);

	    if(is_array($file)){

	    	$row = $this->profile_model->get_row(array('table'=>'tbl_registrants','where'=>array('registrant_id'=>$id)));
	    	if(file_exists($path.'/'.$row->new_picture) && $row->new_picture){
	    		unlink($path.'/'.$row->new_picture);
	    	}

	    	if(file_exists($path.'/46_46_'.$row->new_picture) && $row->new_picture){
	    		unlink($path.'/'.$row->new_picture);
	    	}
 
	    	$params = array('width'=>46,'height'=>46,'source_image'=>$path.'/'.$file['file_name'],'new_image_path'=>$path.'/','file_name'=>$file['file_name']);
  	    	resize($params);

	    	$this->profile_model->update('tbl_registrants',
	    								array('new_picture'=>$file['file_name'],'picture_status'=>0,'date_modified'=>1),
	    								array('registrant_id'=>$id)
	    								);
	    	$error = false;
	    	$msg = 'Your photo submission was successfully sent and is now subject to moderation. Please allow 24-48 hours for approval.'; 
	    }else{
	    	$msg = $this->upload->display_errors();
	    	$error = true;
	    }	    	

	    $data = array('msg'=>$msg,
	    			  'error_holder'=>'user_photo_error',
	    			  'error'=>$error,
	    			  'btn'=>'change-photo-btn',
					  'btn_text'=>'<i>SAVE</i>');
	    $this->load->view('profile/submit-response',$data);

	}

	public function change_photo_form()
	{

		$this->load->view('profile/upload-profile');

	}

	public function change_photo_webcam()
	{	
		$this->load->library('encrypt');
		$user_id = $this->session->userdata('user_id');
		$data['token'] = $this->encrypt->encode($user_id);		
		$this->load->view('profile/upload-webcam-profile',$data);

	}
 

	public function select_statements()
	{
		$user_id = $this->session->userdata('user_id');
		$data['statements'] = $this->profile_model->get_rows(array('table'=>'tbl_statements','where'=>array('is_deleted'=>0,'status'=>1)));
		$user_statements = $this->profile_model->get_rows(array('table'=>'tbl_user_statements',
		 																'where'=>array('registrant_id'=>$user_id),
		 																'order_by'=>array('field'=>'date_created','order'=>'DESC'),
		 																'limit'=>1
		 																)
																)->row();
 		$user_statement_ids = array(0);

 		if($user_statements){

  			$user_statement_ids = explode(',',$user_statements->statement_id);
 		}


 		$data['user_statements'] = $user_statement_ids; /* $this->profile_model->get_rows(array('fields'=>'statement_id,statement',
  																	'table'=>'tbl_statements',
  																	'where'=>array('where_in'=>array('field'=>'statement_id','arr'=>$user_statement_ids)
  																				  )
  																	)
  															); */

		$this->load->view('profile/select-statements',$data);

	}

	public function swf()
	{
		if($this->input->post()){


			if($this->input->post('imageString')){

				$imageString = $this->input->post('imageString');
				$filename = sha1(uniqid()).'.jpg';
				$user_id = $this->session->userdata('user_id');
				$path = 'uploads/profile/'.$user_id.'/';
				file_put_contents($path.$filename, gzuncompress(base64_decode($imageString)));

				$data = array('msg'=>'Your photo submission was successfully sent and is now subject to moderation. Please allow 24-48 hours for approval.',
	    			  'error_holder'=>'',
	    			  'error'=>false,
	    			  'btn'=>'change-photo-btn',
					  'btn_text'=>'<i>Submit</i>');

				$this->profile_model->update('tbl_registrants',
 	   									array('new_picture'=>$filename,'picture_status'=>0,'date_modified'=>1),
 	   									array('registrant_id'=>$user_id)
 	   									);


			}else{

				$data = array('msg'=>'Please capture photo via webcam.',
	    			  'error_holder'=>'error-via-webcam',
	    			  'error'=>true,
	    			  'btn'=>'take-photo-btn',
					  'btn_text'=>'<i>Submit</i>');

			}

			
	   		$this->load->view('profile/submit-response',$data);

		}else{
			$this->load->view('profile/swf_xml');
		}
		
	}


	public function interest(){

		$data = array();
		$this->load->layout('profile/interest',$data);
	}

	public function get_interests(){
		
		$rows = $this->profile_model->get_rows(array('table'=>'tbl_categories','where'=>array('origin_id'=>PROFILING,'parent'=>0)));
		$data = array();
		if($rows->num_rows()){
			
			foreach($rows->result() as $v)  {
				$sub_rows = $this->profile_model->get_rows(array('table'=>'tbl_categories',
																  'where'=>array('origin_id'=>PROFILING,'parent'=>$v->category_id),
																  'fields'=>'category_id,category_name'));

				$interest = $this->profile_model->get_rows(array('table'=>'tbl_profiling',
																  'where'=>array('registrant_id'=>$this->session->userdata('user_id'),
																  				'interest'=>$v->category_id),
																  'fields'=>'profiling_id')
															);
				$data_sub_rows = array();
				if($sub_rows->num_rows()){

					foreach($sub_rows->result() as $s){

						$sub_interest = $this->profile_model->get_rows(array('table'=>'tbl_profiling',
																			  'where'=>array('registrant_id'=>$this->session->userdata('user_id'),
																			  				'interest'=>$v->category_id,
																			  				'sub_interest'=>$s->category_id),
																			  'fields'=>'profiling_id'
																			  )
																		);
						$data_sub_rows[] = array('id'=>$s->category_id,
												'name'=>$s->category_name,
												'is_selected'=>$sub_interest->num_rows() ? 'active' : '');
					}

				}

				$data[] = array('interest_name'=>$v->category_name,
								'interest_id'=>$v->category_id,
								'sub_interests'=>$data_sub_rows,
								'image'=>BASE_URL.'uploads/category/'.$v->category_image,
								'is_selected'=>$interest->num_rows() ? 'active' : '');
			}

		}

		$this->output->set_output(json_encode($data));

	}

	public function save_interests(){

		$_POST = json_decode(file_get_contents('php://input'), true);
		$data = $_POST;
		$order = $_POST['order'];
		$items = $_POST['selected'];
		  

		$error = false;
		$message = '';
		$count = 0;

		$arrangement = array();

		foreach($order as $v){
			$arrangement[] = $v['id'];
		}


		if( (count($order) > 0 && count($order) <= 3) && (count($items) <= 6) ){


			$this->profile_model->delete('tbl_profiling',array('registrant_id'=>$this->session->userdata('user_id')));
			$inserted = array();
			$main_int = array();
			$sub_int = array();

			$categories = $this->profile_model->get_rows(array('table'=>'tbl_categories','where'=>array('origin_id'=>PROFILING)))->result_array();

			if($categories){
				foreach ($categories as $k => $v) {
					$cat[$v['category_id']] = $v['category_name'];
				}
				//print_r($cat); die();
			}

			foreach($items as $v){

				$interest = $this->profile_model->get_row(array('table'=>'tbl_categories',
																	'where'=>array('origin_id'=>PROFILING,'category_id'=>$v['sub_interest_id']),
																	'fields'=>'parent')
																);

				
				if($interest){
					$row = array('registrant_id'=>$this->session->userdata('user_id'),
								'interest'=>$interest->parent,
								'sub_interest'=>$v['sub_interest_id'],
								'prefered_order'=>@array_search($interest->parent, $arrangement),
								'date_added'=>true
								); 
					$inserted[] = $row;
					$this->profile_model->insert('tbl_profiling',$row);

					$count++;
				}


				if(!in_array($cat[$v['interest_id']], $main_int)){
					$main_int[] = $cat[$v['interest_id']];
				}

				$sub_int[] = $cat[$v['interest_id']].'_'.$cat[$v['sub_interest_id']];
			}			
			
			$response = $this->spice->GetPerson(array('PersonId' =>$this->spice->getMemberPersonId()));
			$spice_userinfo = $this->spice->parseJSON($response);
			$spice_userinfo = $spice_userinfo->ConsumerProfiles;

			$total_derivations = count($spice_userinfo->Derivations);
						
			for($i = 0; $i<$total_derivations; $i++){
					if($spice_userinfo->Derivations[$i]->AttributeCode=='ca_main_interest1'){
						$spice_userinfo->Derivations[$i]->AttributeValue = isset($main_int[0]) ? $main_int[0] : "0";
					}
					if($spice_userinfo->Derivations[$i]->AttributeCode=='ca_main_interest2'){
						$spice_userinfo->Derivations[$i]->AttributeValue = isset($main_int[1]) ? $main_int[1] : "0";
					}
					if($spice_userinfo->Derivations[$i]->AttributeCode=='ca_main_interest3'){
						$spice_userinfo->Derivations[$i]->AttributeValue = isset($main_int[2]) ? $main_int[2] : "0";
					}
					if($spice_userinfo->Derivations[$i]->AttributeCode=='ca_mi1_subinterest1'){
						$spice_userinfo->Derivations[$i]->AttributeValue = isset($sub_int[0]) ? $sub_int[0] : "0";
					}
					if($spice_userinfo->Derivations[$i]->AttributeCode=='ca_mi1_subinterest2'){
						$spice_userinfo->Derivations[$i]->AttributeValue = isset($sub_int[1]) ? $sub_int[1] : "0";
					}
					if($spice_userinfo->Derivations[$i]->AttributeCode=='ca_mi2_subinterest1'){
						$spice_userinfo->Derivations[$i]->AttributeValue = isset($sub_int[2]) ? $sub_int[2] : "0";
					}
					if($spice_userinfo->Derivations[$i]->AttributeCode=='ca_mi2_subinterest2'){
						$spice_userinfo->Derivations[$i]->AttributeValue = isset($sub_int[3]) ? $sub_int[3] : "0";
					}
					if($spice_userinfo->Derivations[$i]->AttributeCode=='ca_mi3_subinterest1'){
						$spice_userinfo->Derivations[$i]->AttributeValue = isset($sub_int[4]) ? $sub_int[4] : "0";
					}
					if($spice_userinfo->Derivations[$i]->AttributeCode=='ca_mi3_subinterest2'){
						$spice_userinfo->Derivations[$i]->AttributeValue = isset($sub_int[5]) ? $sub_int[5] : "0";
					}
			}
			
			$Derivations = json_decode(json_encode($spice_userinfo->Derivations),true);

			if($spice_userinfo->MessageResponseHeader->TransactionStatus == 0)
			{
				$spice_userinfo = $spice_userinfo->ConsumerProfiles;

				$params = array('person_id'=> $this->spice->getMemberPersonId(),
	                    		'updates'=> array(
	                                    'Derivations'=> $Derivations
	                            )
							); 
					
	    		$response = $this->spice->updatePerson($params);
	    		$response_json = $this->spice->parseJSON($response);

	    		if($response_json->MessageResponseHeader->TransactionStatus != 5){
					$message = array('message'=>'<h4>'.$response_json->MessageResponseHeader->TransactionStatusMessage.'</h4>','error'=>0,'message_class'=>'success','saving'=>0);
					$this->output->set_output(json_encode($message));

					$data_error = array('origin_id'=>MY_PROFILE,
							'method'=>'ManagePerson',
							'transaction'=>$this->router->method,
							'input'=> $this->session->userdata('spice_input'),
							'response'=>$response,
							'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
						);
					$this->spice->saveError($data_error);
				}else{
					$string = $count > 1 ? 'interests' : 'interest';
					$message = array('message'=>'<h4>Your '.$string.' have been successfully submitted.</h4>','error'=>0,'message_class'=>'success','saving'=>0);
					$this->output->set_output(json_encode($message));
				}
			}else{
				$message = array('message'=>'<h4>'.$spice_userinfo->MessageResponseHeader->TransactionStatusMessage.'</h4>','error'=>0,'message_class'=>'success','saving'=>0);
				$this->output->set_output(json_encode($message));

				$data_error = array('origin_id'=>MY_PROFILE,
						'method'=>'GetPerson',
						'transaction'=>$this->router->method,
						'input'=> $this->session->userdata('spice_input'),
						'response'=>$response,
						'response_message'=>$spice_userinfo->MessageResponseHeader->TransactionStatusMessage
					);
				$this->spice->saveError($data_error);
			}

		}else{

			$message = array('message'=>'<h4>Please select up to three interests only.</h4>','error'=>1,'message_class'=>'danger','saving'=>0);
			$this->output->set_output(json_encode($message));

		}
	}

	public function get_myinterest(){

		$limit = $this->input->get('limit') ? $this->input->get('limit') : 6;
		$rows = $this->profile_model->get_rows(array('table'=>'tbl_profiling as p',
															 'where'=>array('p.registrant_id'=>$this->session->userdata('user_id')), 
														     'order_by' => array('field'=>'p.prefered_order','order'=>'ASC'),
														     'limit'=>$limit,
														     'join'=>array(array('table'=>'tbl_categories as c','on'=>'c.category_id = p.interest'),
														     			   array('table'=>'tbl_categories as c2','on'=>'c2.category_id = p.sub_interest'),
														     			   array('table'=>'tbl_registrants as r','on'=>'r.registrant_id = p.registrant_id')
														     			   ),
														     'fields'=>'p.profiling_id, c.category_name as interest, c.category_image, c.category_id as interest_id, c2.category_id as sub_interest_id, r.first_name, r.third_name, p.date_added'
 														     )
														);
		$selectedInterests = array();
		$categories = array();

		if($rows->num_rows()){

			foreach($rows->result() as $key=>$v){
 				$selectedInterests[] = array('interest_name'=>$v->interest,'interest_id'=>$v->interest_id,'sub_interest_id'=>$v->sub_interest_id,'image'=>BASE_URL.'uploads/category/'.$v->category_image);
  			}

		}

		$rows = $this->profile_model->get_rows(array('table'=>'tbl_profiling',
													 'where'=>array('registrant_id'=>$this->session->userdata('user_id')), 
												     'order_by' => array('field'=>'prefered_order','order'=>'ASC'),
												     'group_by'=>'interest'													     			   
												     )
												);
		$categories = array();
		
		if($rows->num_rows()){
			foreach($rows->result() as $v){
				$categories[] = $v->interest;
			}
		}

		$rows = $this->profile_model->get_rows(array('table'=>'tbl_profiling as p',
													 'where'=>array('p.registrant_id'=>$this->session->userdata('user_id')), 
												     'order_by' => array('field'=>'p.prefered_order','order'=>'ASC'),
												     'group_by'=>'p.interest',
												     'join'=>array('table'=>'tbl_categories as c','on'=>'c.category_id = p.interest'),
												     'fields'=>'p.interest,c.category_name'	
												     )
												);
		$categoryOrder = array();
		
		if($rows->num_rows()){
			foreach($rows->result() as $v){
				$categoryOrder[] = array('id'=>$v->interest,'name'=>$v->category_name);
			}
		}

		$data = array('selectedInterests'=>$selectedInterests,'categories'=>$categories,'categoryOrder'=>$categoryOrder);

		$this->output->set_output(json_encode($data));

	}


}
