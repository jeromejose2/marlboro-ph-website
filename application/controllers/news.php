<?php if(!defined("BASEPATH")){ exit('No direct script access allowed!'); }

class News extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('profile_model');
	}

	public function index()
	{
		$this->load->helper('text');

		$data['rows'] = $this->profile_model->get_rows(array('table'=>'tbl_about_news',
															 'where'=>array('is_deleted'=>0,
 																			'status'=>1
 																			),
															  'order_by'=>array('field'=>'date_added',
																				'order'=>'DESC'
																				)
															)
														);
		$this->load->layout('about/news-list',$data);
		//$this->output->enable_profiler(TRUE);

	}

	public function content()
	{
		$this->load->model(array('Comment_Model','View_Model'));

		$url_title = strtolower(trim($this->uri->segment(4)));
		
		$row = $this->profile_model->get_row(array('table'=>'tbl_about_news',
 																'where'=>array('LOWER(url_title)'=>$url_title,
  																				'is_deleted'=>0,
 																				'status'=>1
 																			)
															)
														);
		if($row){
			$id = $row->about_news_id;
			$this->View_Model->visit(ABOUT_NEWS, $id);
  			$views = $this->View_Model->get_visits_count(ABOUT_NEWS, $id);
  			$this->profile_model->update('tbl_about_news',array('views'=>$views),array('about_news_id'=>$id));
  			$url_title = $row->url_title;

  			$prev = $this->profile_model->get_rows(array('table'=>'tbl_about_news',
														'where'=>array('about_news_id > '=>$row->about_news_id,'status'=>1,'is_deleted'=>0),
														'order_by'=>array('field'=>'about_news_id','order'=>'ASC'),
														'limit'=>'1',
														'fields'=>'url_title'
														)
												)->row();

			$next = $this->profile_model->get_rows(array('table'=>'tbl_about_news',
														'where'=>array('about_news_id <'=>$row->about_news_id,'status'=>1,'is_deleted'=>0),
														'order_by'=>array('field'=>'about_news_id','order'=>'DESC'),
														'limit'=>'1',
														'fields'=>'url_title'
														)
												)->row();

 		}

 		$data['prev'] = $prev;
 		$data['next'] = $next;
 		$data['url_title'] = $url_title;
		$data['row'] = $this->profile_model->get_row(array('table'=>'tbl_about_news',
 																'where'=>array('about_news_id'=>$id,
  																				'is_deleted'=>0,
 																				'status'=>1
 																			)
															)
														);

		$data['total_comments'] = $this->Comment_Model->get_comments_replies_count(ABOUT_NEWS, $id);
		$this->load->layout('about/news-content',$data);
		//$this->output->enable_profiler(TRUE);
	}


	public function get_comment_template()
	{
		return $this->load->view('about/comments','',TRUE);
	}

	public function print_array($data)
	{
		echo "<pre>";
		print_r($data);
		echo "</pre>";
	}
	
}