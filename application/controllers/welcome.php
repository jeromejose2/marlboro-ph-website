<?php

class Welcome extends CI_Controller
{
 
	public function index()
	{
		$this->load->model(array('profile_model','Movefwd_Model', 'Perks_model'));

        $data = array();    

        $user = $this->session->userdata('user');
        $birthday_offer = '';
        $flash_offer = '';
        $referral_offer = '';
        $bids = '';
        $user_flash_prize = '';

            $spice_userinfo = $this->spice->GetPerson(array('PersonId' =>$this->spice->getMemberPersonId()));
            $spice_userinfo = $this->spice->parseJSON($spice_userinfo);
            $spice_userinfo = $spice_userinfo->ConsumerProfiles;

            $date_time = $spice_userinfo->PersonDetails->DateOfBirth;
 
            $match = preg_match('/\/Date\((\d+)([-+])(\d+)\)\//', $date_time, $date);

            $timestamp = $date[1]/1000;
            $operator = $date[2];
            $hours = $date[3]*36; // Get the seconds

            $datetime = new DateTime();

            $datetime->setTimestamp($timestamp);
            $datetime->modify($operator . $hours . ' seconds');

            $birthday_offer = $this->profile_model->get_row(array('table'=>'tbl_birthday_offers',
                                                                   'where'=>array("month = '".date('m',strtotime($datetime->format('Y-m-d')))."' AND month ='".date('m')."'" =>null,
                                                                                  'stock >'=>0,
                                                                                  'status'=>1,
                                                                                  'is_deleted'=>0
                                                                                  ),
                                                                   'fields'=>'prize_id'
                                                            )
                                                        );
    
        if($birthday_offer){
            $user_birthday_prize = $this->profile_model->get_row(array('table'=>'tbl_user_birthday_prizes',
                                                                        'where'=>array('prize_id' =>$birthday_offer->prize_id,'registrant_id' =>$this->session->userdata('user_id')),
                                                                        'fields'=>'user_birthday_prize_id'
                                                                        )
                                                                );
            $birthday_offer = $user_birthday_prize ? '' : $birthday_offer->prize_id;
        }else{

            $birthday_offer = '';
        }
        //////

        if(!$birthday_offer){
            $flash_offer = $this->profile_model->get_row(array('table'=>'tbl_flash_offers',
                                                               'where'=>array('status'=>1,
                                                                              'CURDATE() >= DATE(start_date)'=>null,
                                                                              'CURDATE() <= DATE(end_date) '=>null,
                                                                              'stock >'=>0,
                                                                              'is_deleted'=>0,
                                                                              'category' => 1
                                                                              ),
                                                                'fields'=>'prize_id,display_number,display_province,start_date,end_date'
                                                            )
                                                        );
            $flash_offer_prize_id = ($flash_offer) ? $flash_offer->prize_id :'';

            // $user_flash_prize = $this->profile_model->get_row(array('table'=> 'tbl_user_flash_prizes',
            //                                                             'where'=>array('prize_id' => $flash_offer_prize_id,'registrant_id' =>$this->session->userdata('user_id')),
            //                                                             'fields'=>'user_flash_prize_id'
            //                                                             )
            //                                                     );

  
            $flash_offer_provinces = $flash_offer->display_province ? explode(',', $flash_offer->display_province) : array();
            $spice_userinfo = $this->spice->GetPerson(array('PersonId' =>$this->spice->getMemberPersonId()));
            $spice_userinfo = $this->spice->parseJSON($spice_userinfo);
            $spice_userinfo = $spice_userinfo->ConsumerProfiles;
            
            $province_name = ($spice_userinfo->Addresses[0]->Locality) ? $spice_userinfo->Addresses[0]->Locality : '';
            $province = $this->profile_model->get_row(array('table'=>'tbl_provinces','where'=>array('province'=> $province_name)));
            $registrant_province_id = ($province) ? $province->province_id : '';

            $this->session->set_userdata('user_province_id', $registrant_province_id);

            if(in_array($registrant_province_id, $flash_offer_provinces))
                $reg = $this->profile_model->get_row(array('table'=>'tbl_registrants','where'=>array('registrant_id'=>$this->session->userdata('user_id'))));
            else 
                $reg = false;
                // $reg = $this->profile_model->get_row(array('table'=>'tbl_registrants','where'=>array('registrant_id'=>$this->session->userdata('user_id'))));
            $display_number = $flash_offer->display_number;
            //$user_flash_prize = 1;

            $login_count = $this->profile_model->get_rows(array('table'=>'tbl_login',
                                                           'where'=>array('DATE(date_login) >= "' . $flash_offer->start_date . '"' => null,
                                                                          'DATE(date_login) <= "' . $flash_offer->end_date . '"' => null,
                                                                          'registrant_id'=>$this->session->userdata('user_id')),
                                                            'fields'=>'login_id'
                                                        )
                                                    )->num_rows();
            
            // var_dump($login_count);
            // echo ' | ';
            // var_dump($display_number);
            // echo ' | ';
            // var_dump($this->session->userdata('flash_offer_sess'));
            // echo ' | ';
            // var_dump($reg);
            // exit;
            if($reg){
                // $n = $this->session->userdata('flash_offer_count') ? (int)$this->session->userdata('flash_offer_count') - 1 : (int)$display_number - 1;
                // $this->session->set_userdata('flash_offer_count',$n);
                if($login_count <= $display_number && $this->session->userdata('flash_offer_sess')) {
                    $user_flash_prize = $this->profile_model->get_row(array('table'=>'tbl_user_flash_prizes',
                                                                    'where'=>array('prize_id' =>$flash_offer->prize_id,'registrant_id' =>$this->session->userdata('user_id')),
                                                                    'fields'=>'user_flash_prize_id'
                                                                    )
                                                            );
                    if($user_flash_prize) {
                        $this->session->unset_userdata('flash_offer_sess');
                    }  
                } 
            }
            // var_dump($display_number);
            // echo ' | ';
            // var_dump($login_count);
            // echo ' | ';
            // var_dump($user_flash_prize);
            // exit;
            // $flash_offer = $this->session->userdata('flash_offer_sess') ? '' : $flash_offer->prize_id;
            // $flash_offer = !$user_flash_prize ? '' : $flash_offer->prize_id;
            // $flash_offer = !isset($user_flash_prize) && $user_flash_prize ? '' : $flash_offer->prize_id;
            $flash_offer = $user_flash_prize ? '' : ($reg ? $flash_offer->prize_id : '');

        
        }else{

            $flash_offer = '';
        
        }       

        /*start referral */
        if($this->session->userdata('flash_referral_offer_sess')){

            

            $referral_offer = $this->profile_model->get_row(array('table'=>'tbl_flash_offers',
                                                           'where'=>array('status'=>1,
                                                                          'CURDATE() >= DATE(start_date)'=>null,
                                                                          'CURDATE() <= DATE(end_date) '=>null,
                                                                          'stock >'=>0,
                                                                          'is_deleted'=>0,
                                                                          'category' => 2
                                                                          ),
                                                            'fields'=>'prize_id,display_number,display_province,start_date,end_date'
                                                        )
                                                    );

            if($referral_offer){
                
                $referral_offer_provinces = $referral_offer->display_province ? explode(',', $referral_offer->display_province) : array();
                $spice_userinfo = $this->spice->GetPerson(array('PersonId' =>$this->spice->getMemberPersonId()));
                $spice_userinfo = $this->spice->parseJSON($spice_userinfo);
                $spice_userinfo = $spice_userinfo->ConsumerProfiles;
                
                $province_name = ($spice_userinfo->Addresses[0]->Locality) ? $spice_userinfo->Addresses[0]->Locality : '';
                $province = $this->profile_model->get_row(array('table'=>'tbl_provinces','where'=>array('province'=> $province_name)));
                $registrant_province_id = ($province) ? $province->province_id : '';

                if(in_array($registrant_province_id, $referral_offer_provinces))
                    $ref = $this->profile_model->get_row(array('table'=>'tbl_registrants','where'=>array('registrant_id'=>$this->session->userdata('user_id'))));
                else 
                    $ref = $this->profile_model->get_row(array('table'=>'tbl_registrants','where'=>array('registrant_id'=>$this->session->userdata('user_id'))));
                    
                    $display_number = $referral_offer->display_number;
                        
                if($ref){
                    $login_count = $this->profile_model->get_rows(array('table'=>'tbl_login',
                                                            'where'=>array('registrant_id'=>$this->session->userdata('user_id')),
                                                            'fields'=>'login_id'
                                                        )
                                                    )->num_rows();

                    if($login_count==1) {

                        $user_flash_prize = $this->profile_model->get_row(array('table'=>'tbl_user_flash_prizes',
                                                                        'where'=>array('prize_id' =>$referral_offer->prize_id,'registrant_id' =>$this->session->userdata('user_id')),
                                                                        'fields'=>'user_flash_prize_id'
                                                                        )
                                                                );  
                    } 
                }               
                
                $referral_offer = !isset($user_flash_prize) ? '' : $referral_offer->prize_id;

            }else{

                $referral_offer = '';
                
            }   
        }
        /*end referral*/

        $bids = $this->Perks_model->get_user_confirmed_bid();
        $data['birthday_offer'] = $birthday_offer;
        $data['flash_offer'] = $flash_offer;
        $data['referral_offer'] = $referral_offer;
        $data['bids'] = $bids;
        $data['flash_offer_confirmed'] = $user_flash_prize;
        $data['movefwd_categories'] = $this->Movefwd_Model->get_categories();
        $data['session_key'] = $this->session->userdata('_spice_member_session_key');
        $data['fixedContent'] = true;
       // $this->load->layout('home', $data); 
        // <<< IP FILTER >>>
        if($_SERVER['REMOTE_ADDR'] == '180.232.124.116') {
            $_SESSION['filter_first_login'] = $_SESSION['filter_first_login'] + 1;
            $data['is_first_login'] = $_SESSION['filter_first_login'];
            $this->load->layout('home_firmfilter', $data, 'template_firmfilter');
        } else {
            $this->load->layout('home_rubyburst', $data, 'template_rubyburst');
        }
        

         // $data['fixedContent'] = false;
        // $this->load->layout('home-crossover/teaser',$data);
       
 	}

	public function logout()
	{
		$this->load->library(array('spice'));
		$session_key = $this->session->userdata('_spice_member_session_key');
		$person_id = $this->session->userdata('_spice_member_person_id');
		
		$this->spice->logOut($session_key,$person_id);
		$this->session->sess_destroy();
		
		redirect('user/login');
	}

    public function marlboro_crossover(){
        redirect(STAGING);
    }
}
 
