<?php if(!defined("BASEPATH")){ exit('No direct script access allowed!'); }

class Backstage_pass extends CI_Controller {
	
	public function __construct()
	{
 		parent::__construct();
		$this->load->model('profile_model');
	}

	public function index()
	{	
		$this->load->helper('simplify_datetime_range');
 		$where = array('p.is_deleted'=>0,'p.status'=>1,'( ( CURDATE() BETWEEN DATE(p.start_date) AND DATE(p.end_date) OR DATE(p.end_date) >= CURDATE()  ) OR ( p.end_date = \'0000-00-00 00:00:00\' AND p.start_date = \'0000-00-00 00:00:00\' ) ) '=>null,'is_featured'=>1, 'where_in'=>array('field'=>'p.platform_restriction', 'arr'=>array(0, WEB_ONLY, BOTH_PLATFORM)));
 		$rows = $this->profile_model->get_rows(array('table'=>'tbl_backstage_events as p',
													 'where'=>$where,
												     'join'=>array('table'=>'tbl_regions as r','on'=>'r.region_id=p.region_id','type'=>'left'),
												     'fields'=>'p.*, r.name as region_name',
												     'order_by'=>array('field'=>'p.date_added','order'=>'DESC')												    
													)
											);
		$events = array();

		if($rows->num_rows()){

			foreach($rows->result() as $v){

				$date_address = '';

				if($v->venue)
					$date_address = '<h5>'.$v->venue;

 				$date_address .= ($v->venue && $v->region_name)  ? ', '.$v->region_name.'</h5>' : '</h5>';
			 	$schedule = simplify_datetime_range($v->start_date,$v->end_date);
				$date_address .= $schedule ? '<h5>'.$schedule.'</h5>' : '';
				$image = file_exists('uploads/backstage/events/331_176_'.$v->image) ? BASE_URL.'uploads/backstage/events/331_176_'.$v->image : BASE_URL.'images/default-no-image.jpg';
				$attendance = $this->profile_model->get_row(array('table'=>'tbl_backstage_attendance','where'=>array('event_id'=>$v->backstage_event_id,'registrant_id'=>$this->session->userdata('user_id'))));
				$events[] = array('backstage_event_id'=>$v->backstage_event_id,
								  'title'=>$v->title,
								  'date_address'=>$date_address,
								  'schedule'=>$schedule,
								  'venue'=>$v->venue,
 								  'image'=>$image,
								  'description'=>$v->description,
								  'attendance'=>$attendance ? '1' : '',
								  'url'=>BASE_URL.'backstage_pass/thumbnails/'.$v->url_title);

			} #End Foreach

		}

		$data['rows'] = $events;
		$this->load->layout('backstage/hot-list',$data);
	}

	public function attend_event()
 	{
 		$this->load->helper('simplify_datetime_range');
 		$event = $this->profile_model->get_row(array('table'=>'tbl_backstage_events','where'=>array('is_deleted'=>0,'status'=>1,'DATE(end_date) >= CURDATE()'=>null,'backstage_event_id'=>$this->input->get('event'))));
 		
 		$data['message'] = '';
 
 		if($event){

 			$attendance = $this->profile_model->get_row(array('table'=>'tbl_backstage_attendance','where'=>array('event_id'=>$event->backstage_event_id,'registrant_id'=>$this->session->userdata('user_id'))));
 			
 			if(!$attendance){
 				$url = BASE_URL.'google-oauth2callback/examples/calendar/simple.php?authURL=1';
   				$google_calendar_auth_url = $this->get_google_aut_url($url);
   				
   				//SPICE
 				$sec_settings = $this->profile_model->get_row(array('table' => 'tbl_section_settings', 'where' => 'name = "BACKSTAGE_PASS"'));
				$cell_id = $sec_settings->setting_section_id;

				$act_settings = $this->profile_model->get_row(array('table' => 'tbl_activity_settings', 'where' => 'name = "BACKSTAGE_PASS"'));
				$act_id = $act_settings->setting_activity_id;

				$dateTime = DateTime::createFromFormat('Y-m-d H:i:s', date("Y-m-d").' 00:00:01');
				$DateOfBirth = sprintf(
				   '/Date(%s%s)/',
				   $dateTime->format('U') * 1000,
				   $dateTime->format('O')
				);
				
				$param = array('CellId' => $cell_id, 'ActionList' =>array(array('ActivityId'   => $act_id, 'ActionValue' => $event->title.'|'.date("Y-m-d H:i:s"))));
				$response = $this->spice->trackAction($param);
				$response_json = $this->spice->parseJSON($response);

				if($response_json->MessageResponseHeader->TransactionStatus != 0){
						$data['message'] = '<h3>'.$response_json->MessageResponseHeader->TransactionStatusMessage.'</h3>';

						$data_error = array('origin_id'=>BACKSTAGE_PHOTOS,
								'method'=>'trackAction',
								'transaction'=>$this->router->method,
								'input'=> $this->session->userdata('spice_input'),
								'response'=>$response,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
						$this->spice->saveError($data_error);
				}
				
				$this->profile_model->insert('tbl_backstage_attendance',array('event_id'=>$event->backstage_event_id,'registrant_id'=>$this->session->userdata('user_id'),'date_added'=>true));
 				// $data['message'] = '<br/><h3>SUCCESS</h3>You have confirmed your attendace to '.$event->title.'.<br/> See you at '.$event->venue.' on '.simplify_datetime_range($event->start_date,$event->end_date).'<br/><br/><a class="button-small mark-my-calendar" href="'.$google_calendar_auth_url.'"><i>MARK MY CALENDAR</i></a>';
 				$data['message'] = '<br/><h3>SUCCESS</h3>You have confirmed your attendace to '.$event->title.'.<br/> See you at '.$event->venue.' on '.simplify_datetime_range($event->start_date,$event->end_date).'<br/><br/><a onclick="popup.close(function(){location.reload();})" class="button-small" href="javascript:void(0)"><i>OK</i></a>';

 			}else{

 				$data['message'] = '<h3>You have already confirmed your attendance of this event.</h3>';

 			}
 			 			
  		}else{

  			$data['message'] = '<h3>Event not exists/active.</h3>';

  		}
  		$data['event_id'] = $this->input->get('event');

  		$this->load->view('backstage/attend-event',$data);
 		
 	}
 	
 	private function get_google_aut_url($url){

 		return file_get_contents($url);

 	}

 	public function mark_google_calendar()
 	{
 		$row = $this->profile_model->get_row(array('table'=>'tbl_backstage_events as e',
													'where'=>array('e.backstage_event_id'=>$this->input->post('event_id'),
																	'e.is_deleted'=>0,
																	'e.status'=>1,
																	'DATE(e.end_date) > CURDATE()'=>null
																	),
													'join'=>array('table'=>'tbl_regions as r','on'=>'r.region_id=e.region_id','type'=>'left'),
													'fields'=>'e.title,e.description,e.venue,e.start_date,e.end_date,r.name as region_name'
													)
											);
 		$token = $_POST['token'];
		$response = array();
		
		if($row){ 
			
			error_reporting(E_ALL);
			ini_set('display_errors', '1');

			require_once 'google-oauth2callback/src/Google_Client.php';
			require_once 'google-oauth2callback/src/contrib/Google_CalendarService.php';

			$client = new Google_Client();
			$cal = new Google_CalendarService($client);

			$client->setAccessToken($token);

 			$location = $row->venue; //($row->venue) ? $row->venue.','.$row->region_name : $row->venue;
 			$start_date = date('Y-m-d',strtotime($row->start_date)).'T'.date('H:i:s',strtotime($row->start_date));
 			$end_date = date('Y-m-d',strtotime($row->end_date)).'T'.date('H:i:s',strtotime($row->end_date));

			$event = new Google_Event();
			$event->setSummary($row->title);
			$event->setLocation($location);
			$event->setDescription($row->description);
			$start = new Google_EventDateTime();
			$start->setDateTime($start_date);
			$start->setTimeZone('Asia/Manila');
			$event->setStart($start);

			$end = new Google_EventDateTime();	
			$end->setDateTime($end_date);
			$end->setTimeZone('Asia/Manila');
			$event->setEnd($end);
			$createdEvent = $cal->events->insert('primary', $event);
			$response = "You have successfully marked your google calendar for the event.";
		 

		}else{

			$response = "Event not found.";

		}

		$this->output->set_output(json_encode(array('response'=>$response)));
		
 	}

 	public function upcoming()
	{
		$limit = 8;
 		$total_rows = $this->profile_model->get_total_rows(array('table'=>'tbl_backstage_events',
											   'where'=>array(
											   	'is_deleted'=>0,
											      'status'=>1,
											      '( (CURDATE() BETWEEN DATE(start_date) AND DATE(end_date) OR DATE(end_date) >= CURDATE() ) OR 
											      ( end_date = \'0000-00-00 00:00:00\' AND start_date = \'0000-00-00 00:00:00\' ) )'=>null,
											   	'where_in'=>array('field'=>'platform_restriction', 'arr'=>array(0, WEB_ONLY, BOTH_PLATFORM))
											      )
 																)
 															);
 		$data['total_group'] = ($total_rows % $limit == 0) ? $total_rows / $limit : (int)($total_rows / $limit) + 1;
		$data['regions'] = $this->profile_model->get_rows(array('table'=>'tbl_regions',
															     'order_by'=>array('field'=>'name','order'=>'ASC'),
															     'where'=>array('is_deleted'=>0)
 														     )
														);
		$data['venues'] = $this->profile_model->get_rows(array('table'=>'tbl_venues',
															     'order_by'=>array('field'=>'name','order'=>'ASC'),
															     'where'=>array('is_deleted'=>0)
	 														     )
															);
		$this->load->layout('backstage/upcoming-list',$data);

	}

	public function photos()
	{ 
  		$limit = 8;
 		$total_rows = $this->profile_model->get_total_rows(array('table'=>'tbl_backstage_events',
																	'where'=>array('is_deleted'=>0,
																					'status'=>1,
 																					'DATE(end_date) < CURDATE()'=>null,
 																					'end_date !='=>'0000-00-00 00:00:00',
 																					'start_date !='=>'0000-00-00 00:00:00',
 																					'where_in'=>array('field'=>'platform_restriction', 'arr'=>array(0, WEB_ONLY, BOTH_PLATFORM))
																					)
 																)
 															); 		

 		$data['total_group'] = ($total_rows % $limit == 0) ? $total_rows / $limit : (int)($total_rows / $limit) + 1;
		$data['regions'] = $this->profile_model->get_rows(array('table'=>'tbl_regions',
														     'order_by'=>array('field'=>'name','order'=>'ASC'),
														     'where'=>array('is_deleted'=>0)
 														     )
														);
		$data['venues'] = $this->profile_model->get_rows(array('table'=>'tbl_venues',
															     'order_by'=>array('field'=>'name','order'=>'ASC'),
															     'where'=>array('is_deleted'=>0)
	 														     )
															);
		$data['event_types'] = $this->profile_model->get_rows(array('table'=>'tbl_backstage_event_types',
															     'order_by'=>array('field'=>'name','order'=>'ASC'),
															     'where'=>array('is_deleted'=>0)
	 														     )
															);
		$this->load->layout('backstage/photos-list',$data); 		
	}

	public function thumbnails()
	{
		$this->load->helper('simplify_datetime_range');
		$backstage_event_id = 0;
 		$url_title = strtolower(trim($this->uri->segment(3)));

		$row = $this->profile_model->get_rows(array('table'=>'tbl_backstage_events as p',
 															'where'=>array('LOWER(p.url_title)'=>$url_title,
																			'p.is_deleted'=>0,
																			'p.status'=>1
  																			),
 															 'join'=>array('table'=>'tbl_regions as r','on'=>'r.region_id=p.region_id','type'=>'left'),
 															 'fields'=>'p.*, r.name as region_name'
															)
														)->row();
		
		if($row){

			$backstage_event_id = $row->backstage_event_id;
			$url_title = $row->url_title;
			$region_name = $row->region_name;
		}		 

		$data['backstage_event_id'] = $backstage_event_id;
		$data['url_title'] = $url_title;
		$data['row'] = $row;
		$data['region_name'] = $region_name;
		$data['row_photos'] = $this->profile_model->get_rows(array('table'=>'tbl_backstage_events_photos',
		 															'where'=>array('backstage_event_id'=>$backstage_event_id,
		  																			'is_deleted'=>0,
		  																			'status'=>1
		 																			),
		 															'order_by'=>array('field'=>'photo_id',
																				  'order'=>'DESC'
																				 ),
		 															'fields'=>'photo_id,media_image_filename,media_type,media_duration'
																)
															);
		
 		$this->load->layout('backstage/photo-thumbnails',$data);
  	}



	public function content()
	{
		$this->load->helper(array('file','simplify_datetime_range'));
		$this->load->model(array('Comment_Model','View_Model'));

		$url_title = strtolower(trim($this->uri->segment(3)));
		$photo_id = $this->uri->segment(4);
		$backstage_event_id = 0;
		$row = $this->profile_model->get_rows(array('table'=>'tbl_backstage_events as p',
 															'where'=>array('LOWER(p.url_title)'=>$url_title,
																			'p.is_deleted'=>0,
																			'p.status'=>1
  																			),
 															 'join'=>array('table'=>'tbl_regions as r','on'=>'r.region_id=p.region_id','type'=>'left'),
 															 'fields'=>'p.*, r.name as region_name'
															)
														)->row();
		if($row){
			$backstage_event_id =  $row->backstage_event_id;
			$url_title = $row->url_title;
		}else{
			show_404();
		}

		$data['url_title'] = $url_title;
		$data['row'] = $row;

 		$row_photo = $this->profile_model->get_row(array('table'=>'tbl_backstage_events_photos',
 														 'where'=>array('backstage_event_id'=>$backstage_event_id,
																	    'photo_id'=>$photo_id,
																		'is_deleted'=>0,
																		'status'=>1
 																		)
														)
													);
		$prev = array();
		$next = array();		 

		if($row_photo){

			$prev = $this->profile_model->get_rows(array('table'=>'tbl_backstage_events_photos',
														'where'=>array('photo_id > '=>$row_photo->photo_id,'status'=>1,'is_deleted'=>0,'backstage_event_id'=>$backstage_event_id),
														'order_by'=>array('field'=>'photo_id','order'=>'ASC'),
														'limit'=>'1',
														'fields'=>'photo_id,backstage_event_id'
														)
												)->row();

			$next = $this->profile_model->get_rows(array('table'=>'tbl_backstage_events_photos',
														'where'=>array('photo_id <'=>$row_photo->photo_id,'status'=>1,'is_deleted'=>0,'backstage_event_id'=>$backstage_event_id),
														'order_by'=>array('field'=>'photo_id','order'=>'DESC'),
														'limit'=>'1',
														'fields'=>'photo_id,backstage_event_id'
														)
												)->row();

			$this->View_Model->visit(BACKSTAGE_PHOTOS, $photo_id);
  			$views = $this->View_Model->get_visits_count(BACKSTAGE_PHOTOS, $photo_id);
  			$this->profile_model->update('tbl_backstage_events_photos',array('views'=>$views),array('photo_id'=>$photo_id));
  			$data['prev'] = $prev;
			$data['next'] = $next;
			$data['row_photo'] = $row_photo;
			$data['total_comments'] = $row_photo ? $this->Comment_Model->get_comments_replies_count(BACKSTAGE_PHOTOS, $photo_id) : 0;

 	    	$this->load->layout('backstage/photo-content',$data);

		}else{
			show_404();
		}
 		
		
  	}




 	public function upload_form()
 	{
 		$id = $this->uri->segment(3);
		$data['row'] = $this->profile_model->get_row(array('table'=>'tbl_backstage_events',
 															'where'=>array('backstage_event_id'=>$id,
  																				'is_deleted'=>0,
 																				'status'=>1
 																			),
 															'fields'=>'title'
															)
														);
 		$this->load->view('backstage/photo-upload',$data);
 	}

 	public function upload_photo()
 	{
 		$this->load->helper(array('upload','resize'));

 		$backstage_event_id = (int)$this->uri->segment('3');
 		$registrant_id = $this->session->userdata('user_id');

 		$events = $this->profile_model->get_row(array('table'=>'tbl_backstage_events','where'=>array('backstage_event_id'=>$backstage_event_id,'is_deleted'=>0,'status'=>1)));
 		$registrant = $this->profile_model->get_row(array('table'=>'tbl_registrants','where'=>array('registrant_id'=>$registrant_id)));

 		if($events && $registrant){

	 		$path = 'uploads/backstage/photos/';

	 		if(!is_dir($path)){
				mkdir($path,0777,true);
			}
		
	 		$filename = md5(time().uniqid());
	 		$params = array('upload_path'=>$path,'allowed_types'=>FILE_UPLOAD_ALLOWED_TYPES,'file_name'=>$filename,'max_size'=>'3072','max_width'=>0,'max_height'=>0,'do_upload'=>'photo');
		    $file = upload($params);

		    if(is_array($file)){

		    	$params = array('width'=>331,'height'=>176,'source_image'=>$path.'/'.$file['file_name'],'new_image_path'=>$path.'/thumbnails/','file_name'=>$file['file_name']);
	  	    	resize($params);

	  	    	$params = array('width'=>100,'height'=>100,'source_image'=>$path.'/'.$file['file_name'],'new_image_path'=>$path.'/thumbnails/','file_name'=>$file['file_name']);
	  	    	resize($params);

	  	    	$this->profile_model->insert('tbl_backstage_events_photos',array('backstage_event_id'=>$backstage_event_id,
	  	    																'uploader_id'=>$this->session->userdata('user_id'),
	  	    																'uploader_name'=>$registrant->first_name.' '.$registrant->middle_initial.' '.$registrant->third_name,
	  	    																'user_type'=>'registrant',
	  	    																'media'=>$file['file_name'],
	  	    																'media_image_filename'=>$file['file_name'],
	  	    																'media_type'=>'photo',
	  	    																'status'=>$this->session->userdata('is_cm') ? 1 : '0',
	  	    																'date_added'=>true)
	  	    								);

		    	$data = array('msg'=>"<h2>THANK YOU FOR YOUR ENTRY</h2>Your photo submission was successfully sent and is now subject to moderation. Please allow 24-48 hours for approval.",
			    			  'error_holder'=>'error-photo-upload',
			    			  'error'=>false,
			    			  'btn'=>'btn-photo-upload-save',
							  'btn_text'=>'<i>SAVE</i>');
		    

		    }else{

		    	$data = array('msg'=>'<h3>'.str_replace('in your PHP configuration file', '3MB', $file).'</h3>',
			    			  'error_holder'=>'error-photo-upload',
			    			  'error'=>true,
			    			  'btn'=>'btn-photo-upload-save',
							  'btn_text'=>'<i>SAVE</i>');

		    }

		}else{

			$data = array('msg'=>'<h3>Invalid event/registrant</h3>',
		    			  'error_holder'=>'error-photo-upload',
		    			  'error'=>true,
		    			  'btn'=>'btn-photo-upload-save',
						  'btn_text'=>'<i>SAVE</i>');

		}

	    $this->load->view('profile/submit-response',$data);

 	} 

 	public function get_google_map()
 	{
 		$event_id = $this->input->get('event');
 		$data['row'] = $this->profile_model->get_row(array('table'=>'tbl_backstage_events','where'=>array('backstage_event_id'=>$event_id,'is_deleted'=>0,'status'=>1)));
 		$this->load->view('backstage/google-map',$data);
 	}

 	public function event_photo()
 	{
 		$event_id = $this->input->get('event');
 		$row = $this->profile_model->get_row(array('table'=>'tbl_backstage_events','where'=>array('backstage_event_id'=>$event_id,'is_deleted'=>0,'status'=>1,'DATE(end_date) > CURDATE()'=>null)));
 		$data['photo'] = ($row && file_exists('uploads/backstage/events/'.$row->image)) && $row->image ? BASE_URL.'uploads/backstage/events/'.$row->image : BASE_URL.'images/default-no-image.jpg';
 		$this->load->view('backstage/event-photo',$data);
 	} 
 
}