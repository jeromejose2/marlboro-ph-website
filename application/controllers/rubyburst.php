<?php if(!defined("BASEPATH")){ exit('No direct script access allowed!'); }

class Rubyburst extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('About_Model');
	}

	public function index()
	{
		$this->load->view('rubyburst/index');
	}

	public function findOutMore()
	{
		$this->load->view('rubyburst/more');
	}

	public function findStore()
	{
		$query = $this->db->query("SELECT * FROM tbl_stores group by region order by region ASC");

		$stores = array();

		$actual = array();

		foreach ($query->result() as $row) {
			$region = $row->region;
			
			$region_query = $this->db->query("SELECT * FROM tbl_stores where region='".$region."' order by store_name ASC");
			
			$regions[] = $region;

			foreach($region_query->result() as $region) {
				$stores[] = array("id"=>$region->store_id, "store_name"=>$region->store_name, "address" => $region->address);
			}

			$actual[] = $stores;
		}

		$this->load->view('rubyburst/burst', compact("regions", "actual"));
	}

	public function events()
	{
		$query = $this->db->query("SELECT * FROM tbl_events group by region order by region ASC");

		$stores = array();

		$actual = array();

		foreach ($query->result() as $row) {
			$region = $row->region;
			
			$region_query = $this->db->query("SELECT * FROM tbl_events where region='".$region."' order by event_name ASC");
			
			$regions[] = $region;

			foreach($region_query->result() as $region) {
				$stores[] = array("id"=>$region->store_id, "store_name"=>$region->event_name, "address" => $region->address, "date" => $region->date);
			}

			$actual[] = $stores;
		}

		$this->load->view('rubyburst/burst_events', compact("regions", "actual"));
	}

}
