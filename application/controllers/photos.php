<?php if(!defined("BASEPATH")){ exit('No direct script access allowed!'); }

class Photos extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('profile_model');
	}

	public function index()
	{
		$this->load->helper('text');
		$data = array();

		/*

		$rows = $this->profile_model->get_rows(array('table'=>'tbl_about_events',
													 'where'=>array('is_deleted'=>0,
																	'status'=>1,
													 'order_by'=>array('field'=>'date_added',
																		'order'=>'DESC'
																		),
													 'fields'=>'about_event_id,title,url_title'
													)
												);
		$albums = array();

		if($rows->num_rows()){
			
			foreach($rows->result() as $v){
				$photos = $this->profile_model->get_rows(array('table'=>'tbl_about_events_photos',
																'where'=>array('about_event_id'=>$v->about_event_id,'is_deleted'=>0,'status'=>1),
																'limit'=>3,
																'order_by'=>array('field'=>'photo_id',
																				  'order'=>'desc'
																				 )
																)
														);
				$albums[] = array('about_event_id'=>$v->about_event_id,'album_name'=>$v->title,'url_title'=>$v->url_title,'photos'=>$photos);
			}

		}


		$data['rows'] = $albums;
		*/
		$this->load->layout('about/photos-list',$data);
	}

	public function thumbnails()
	{
		$about_event_id = 0;
		$url_title = strtolower(trim($this->uri->segment(4)));
		$row = $this->profile_model->get_row(array('table'=>'tbl_about_events',
 															'where'=>array('LOWER(url_title)'=>$url_title,
  																				'is_deleted'=>0,
 																				'status'=>1,
 																				'type'=>'about'
 																			),
 															'fields'=>'about_event_id,title,url_title'
															)
														);

		
		if($row){
			$about_event_id = $row->about_event_id;
			$url_title = $row->url_title;
		}	

		$data['url_title'] = $url_title;
 		$data['about_event_id'] = $about_event_id;
		$data['row'] = $row;
		$data['row_photos'] = $this->profile_model->get_rows(array('table'=>'tbl_about_events_photos',
		 															'where'=>array('about_event_id'=>$about_event_id,
		  																			'is_deleted'=>0,
		  																			'status'=>1
		 																			),
		 															'order_by'=>array('field'=>'photo_id',
																				  'order'=>'DESC'
																				 ),
		 															'fields'=>'photo_id,image'
																)
															);
  		$this->load->layout('about/photo-thumbnails',$data);
 	}



	public function content()
	{
		$this->load->model(array('Comment_Model','View_Model'));

		$url_title = strtolower(trim($this->uri->segment(4)));
		$photo_id = $this->uri->segment(5);
		$about_event_id = 0;


		$row = $this->profile_model->get_row(array('table'=>'tbl_about_events',
													'where'=>array('LOWER(url_title)'=>$url_title,
																	'is_deleted'=>0,
																	'status'=>1,
																	'type'=>'about'
																),
													'fields'=>'about_event_id,title,url_title'
													)
											);
		if($row){
			$about_event_id =  $row->about_event_id;
			$url_title = $row->url_title;
		}

		$data['url_title'] = $url_title;
		$data['row'] = $row;
		$row_photo = $this->profile_model->get_row(array('table'=>'tbl_about_events_photos',
 														 'where'=>array('about_event_id'=>$about_event_id,
																		    'photo_id'=>$photo_id,
																			'is_deleted'=>0,
																			'status'=>1
 																		),
 														 'fields'=>'about_event_id,title,url_title'
														)
													);
		$prev = array();
		$next = array();
		 

		if($row_photo){

			$prev = $this->profile_model->get_rows(array('table'=>'tbl_about_events_photos',
														'where'=>array('photo_id > '=>$row_photo->photo_id,'status'=>1,'is_deleted'=>0,'about_event_id'=>$about_event_id),
														'order_by'=>array('field'=>'photo_id','order'=>'ASC'),
														'limit'=>'1',
														'fields'=>'photo_id,about_event_id'
														)
												)->row();

			$next = $this->profile_model->get_rows(array('table'=>'tbl_about_events_photos',
														'where'=>array('photo_id <'=>$row_photo->photo_id,'status'=>1,'is_deleted'=>0,'about_event_id'=>$about_event_id),
														'order_by'=>array('field'=>'photo_id','order'=>'DESC'),
														'limit'=>'1',
														'fields'=>'photo_id,about_event_id'
														)
												)->row();

			$this->View_Model->visit(ABOUT_PHOTOS, $photo_id);
  			$views = $this->View_Model->get_visits_count(ABOUT_PHOTOS, $photo_id);
  			$this->profile_model->update('tbl_about_events_photos',array('views'=>$views),array('photo_id'=>$photo_id));

		} 
 		
		$data['prev'] = $prev;
		$data['next'] = $next;
		$data['row_photo'] = $row_photo;
		$data['total_comments'] = $this->Comment_Model->get_comments_replies_count(ABOUT_PHOTOS, $photo_id);

 	    $this->load->layout('about/photo-content',$data);
  	}


 	public function upload_form()
 	{
 		$id = $this->uri->segment(3);
		$data['row'] = $this->profile_model->get_row(array('table'=>'tbl_about_events',
 															'where'=>array('about_event_id'=>$id,
  																				'is_deleted'=>0,
 																				'status'=>1
 																			),
 															'fields'=>'title'
															)
														);
 		$this->load->view('about/photo-upload',$data);
 	}

 	public function upload_photo()
 	{
 		$this->load->helper(array('upload','resize'));

 		$about_event_id = (int)$this->uri->segment('3');
 		$registrant_id = $this->session->userdata('user_id');

 		$events = $this->profile_model->get_row(array('table'=>'tbl_about_events','where'=>array('about_event_id'=>$about_event_id,'is_deleted'=>0,'status'=>1)));
 		$registrant = $this->profile_model->get_row(array('table'=>'tbl_registrants','where'=>array('registrant_id'=>$registrant_id)));

 		if($events && $registrant){

	 		$path = 'uploads/about/photos/';
	 		$filename = md5(time().uniqid());
	 		$params = array('upload_path'=>$path,'allowed_types'=>FILE_UPLOAD_ALLOWED_TYPES,'file_name'=>$filename,'max_size'=>'3072','max_width'=>0,'max_height'=>0,'do_upload'=>'photo');
		    $file = upload($params);

		    if(is_array($file)){

		    	$params = array('width'=>331,'height'=>176,'source_image'=>$path.'/'.$file['file_name'],'new_image_path'=>$path.'/thumbnails/','file_name'=>$file['file_name']);
	  	    	resize($params);

	  	    	$params = array('width'=>100,'height'=>100,'source_image'=>$path.'/'.$file['file_name'],'new_image_path'=>$path.'/thumbnails/','file_name'=>$file['file_name']);
	  	    	resize($params);

	  	    	$this->profile_model->insert('tbl_about_events_photos',array('about_event_id'=>$about_event_id,
	  	    																'uploader_id'=>$this->session->userdata('user_id'),
	  	    																'uploader_name'=>$registrant->first_name.' '.$registrant->middle_initial.' '.$registrant->third_name,
	  	    																'user_type'=>'registrant',
	  	    																'image'=>$file['file_name'],
	  	    																'status'=>'0',
	  	    																'date_added'=>true)
	  	    								);

		    	$data = array('msg'=>"<h2>THANK YOU FOR YOUR ENTRY</h2>Please give us time to process and verify your submission. We\'ll send you an e-mail within 24 hours.",
			    			  'error_holder'=>'error-photo-upload',
			    			  'error'=>false,
			    			  'btn'=>'btn-photo-upload-save',
							  'btn_text'=>'<i>SAVE</i>');
		    

		    }else{

		    	$data = array('msg'=>'<h3>'.str_replace('in your PHP configuration file', '2MB', $file).'</h3>',
			    			  'error_holder'=>'error-photo-upload',
			    			  'error'=>true,
			    			  'btn'=>'btn-photo-upload-save',
							  'btn_text'=>'<i>SAVE</i>');

		    }

		}else{

			$data = array('msg'=>'<h3>Invalid event/registrant</h3>',
			    			  'error_holder'=>'error-photo-upload',
			    			  'error'=>true,
			    			  'btn'=>'btn-photo-upload-save',
							  'btn_text'=>'<i>SAVE</i>');

		}

	    $this->load->view('profile/submit-response',$data);

 	}
 	

	public function get_comment_template()
	{
		return $this->load->view('about/comments','',TRUE);
	}

	

	
}