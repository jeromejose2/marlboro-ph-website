<?php 

class Upcoming_events extends NW_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('profile_model');
	}

	public function get()
	{
		$this->load->helper('simplify_datetime_range');
		$limit = 8;
		//$where = array('p.is_deleted'=>0,'p.status'=>1,'( ( CURDATE() NOT BETWEEN DATE(p.start_date) AND DATE(p.end_date) AND DATE(p.end_date) > CURDATE() ) OR ( p.end_date = \'0000-00-00 00:00:00\' AND p.start_date = \'0000-00-00 00:00:00\' ) ) '=>null);
		$where = array('p.is_deleted'=>0,'p.status'=>1,'( ( CURDATE() BETWEEN DATE(p.start_date) AND DATE(p.end_date) OR DATE(p.end_date) >= CURDATE() ) OR ( p.end_date = \'0000-00-00 00:00:00\' AND p.start_date = \'0000-00-00 00:00:00\' ) ) '=>null, 'where_in'=>array('field'=>'p.platform_restriction', 'arr'=>array(0, WEB_ONLY, BOTH_PLATFORM)));
		$start_date = $this->input->get('start_date');
		$end_date = $this->input->get('end_date');

		if((int)$this->input->get('venue')){
			$venue = $this->profile_model->get_row(array('table'=>'tbl_venues','where'=>array('venue_id'=>$this->input->get('venue'))));
			if($venue)
				$where['LOWER(p.venue)'] = strtolower($venue->name);
		}			

		if((int)$this->input->get('region'))
			$where['p.region_id'] = (int)$this->input->get('region');

		if((int)$this->input->get('status'))
			$where['p.is_highlighted'] = (int)$this->input->get('status');

		if($this->validate_date($start_date) && $this->validate_date($end_date)){

			$where['DATE(p.start_date) >='] = $start_date;
			$where['DATE(p.end_date) <='] = $end_date;

		}else{

			if($this->validate_date($start_date)){
				$where['DATE(p.start_date) '] = $start_date;
			}

			if($this->validate_date($end_date)){
				$where['DATE(p.end_date) '] = $end_date;
			}

		}

		if($this->input->get('total_rows')==1){

  			$total_rows = $this->profile_model->get_rows(array('table'=>'tbl_backstage_events as p',
																 'where'=>$where
	 														     )
													)->num_rows();
			$total_rows = ($total_rows % $limit == 0) ? $total_rows / $limit : (int)($total_rows / $limit) + 1;
			$this->output->set_output(json_encode(array('total_group'=>$total_rows)));

		}else{	
		
			$offset = ((int)$this->input->get('track_group')) ? (int)$this->input->get('track_group') * $limit : 0;
			$rows = $this->profile_model->get_rows(array('table'=>'tbl_backstage_events as p',
														 'where'=>$where,
													     'join'=>array('table'=>'tbl_regions as r','on'=>'r.region_id=p.region_id','type'=>'left'),
													     'fields'=>'p.*, r.name as region_name',
													     'order_by'=>array('field'=>'p.date_added','order'=>'DESC'),
													     'offset'=>$offset,
													     'limit'=>$limit
														)
												);

			$albums = array();

			if($rows->num_rows()){

				$count = 0;

				foreach($rows->result() as $v){ 

					$count++;
					$event = $v->venue;

					if($v->venue && $v->region_name) 
						$event .= ', ';

					if($v->region_name) 
						$event .= $v->region_name;		

						$schedule = simplify_datetime_range($v->start_date,$v->end_date);
						$event .= $schedule ? ' - '.$schedule : '';
						$image = file_exists('uploads/backstage/events/331_176_'.$v->image) ? BASE_URL.'uploads/backstage/events/331_176_'.$v->image : BASE_URL.'images/default-no-image.jpg';
						$attendance = $this->profile_model->get_row(array('table'=>'tbl_backstage_attendance','where'=>array('event_id'=>$v->backstage_event_id,'registrant_id'=>$this->session->userdata('user_id'))));


						$albums[] = array('backstage_event_id'=>$v->backstage_event_id,
	 								  'album_name'=>$v->title,
									  'event'=>($event) ? $event : '',
 									  'image'=>$image,
									  'description'=>$v->description,
									  'attendance'=>$attendance ? '1' : '',
									  'new_line'=>($count % 2 == 0) ? '<div class="clearfix"></div>' : '',
	 								  'url'=>BASE_URL.'backstage_pass/thumbnails/'.$v->url_title);
 
				} #End Foreach
			}

			$this->output->set_output(json_encode($albums));

		}

	}

	private function validate_date($date)
	{
		$date = explode('-',$date);

		if(count($date) == 3){
 			if(checkdate($date[1], $date[2],$date[0])){
				return true;
			} 
		}
		return false;

	}


}