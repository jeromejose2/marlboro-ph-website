<?php

class Newsfeed extends NW_Controller
{
	public function get()
	{
		$this->load->model('Newsfeed_Model');
		$this->output->set_output(json_encode($this->Newsfeed_Model->get()));
	}
}