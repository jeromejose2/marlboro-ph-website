<?php

class Finalists extends NW_Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function post()
	{
		if( ! $this->session->userdata('user_id')) {
			return FALSE;
		}

		# Production
		define('FINALIST_CELL_ID', 5718);
		define('FINALIST_ACTIVITY_ID', 1204);

		# Staging
		// define('FINALIST_CELL_ID', 1847);
		// define('FINALIST_ACTIVITY_ID', 1122);

		$this->db->select()->from('tbl_finalist_likers');
		$this->db->where('registrant_id', $this->session->userdata('user_id'));
		$this->db->where('finalist_id', $this->input->post('fdid'));
		$exist = $this->db->get()->result();

		$this->db->select()->from('tbl_finalists');
		$this->db->where('finalist_id', $this->input->post('fdid'));
		$result = $this->db->get()->row();

		if($exist) {
			$this->db->delete('tbl_finalist_likers', array('finalist_liker_id'=>$exist[0]->finalist_liker_id));

			$this->db->select()->from('tbl_finalists');
			$this->db->where('finalist_id', $this->input->post('fdid'));
			$this->db->set('likes', 'likes-1', FALSE);
			$this->db->update('tbl_finalists');

			$likes = $result->likes - 1;
			$act = 'down';

			// $actionValue = $this->session->userdata('user_id')."|unliked|".$exist[0]->finalist_liker_id;
			$actionValue = $result->name."|UnLiked";			
		} else {
			$data = array(
				'registrant_id'=>$this->session->userdata('user_id'),
				'finalist_id'=>$this->input->post('fdid')
			);

			$this->db->insert('tbl_finalist_likers', $data);

			$this->db->select()->from('tbl_finalists');
			$this->db->where('finalist_id', $this->input->post('fdid'));
			$this->db->set('likes', 'likes+1', FALSE);
			$this->db->update('tbl_finalists');

			$likes = $result->likes + 1;
			$act = 'up';

			// $actionValue = $this->session->userdata('user_id')."|liked|".$this->input->post('fdid');
			$actionValue = $result->name."|Liked";
		}
		$param = array('CellId'=>FINALIST_CELL_ID, 'ActionList'=>array(array('ActivityId'=>FINALIST_ACTIVITY_ID, 'ActionValue'=>$actionValue)));
		$response = $this->spice->trackAction($param);
		$response_json = $this->spice->parseJSON($response);
		// $response = $this->spice->trackAction($param);
		// $response_json = $this->spice->parseJSON($response);

		// if($response_json->MessageResponseHeader->TransactionStatus != 0) {
		// 	$result['error'] = '<h3>'.$response_json->MessageResponseHeader->TransactionStatusMessage.'</h3>';

		// 	$data_error = array('origin_id'=>FINALIST,
		// 			'method'=>'trackAction',
		// 			'transaction'=>$this->router->method,
		// 			'input'=>$this->session->userdata('spice_input'),
		// 			'response'=>$response,
		// 			'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage);
		// 	$this->spice->saveError($data_error);
		// }

		$return['likes'] = (int) $likes;
		$return['resp'] = (string) $act;
		$this->output->set_output(json_encode($return));
	}

}