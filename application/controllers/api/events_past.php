<?php

class Events_past extends NW_Controller {

	public function __construct()
	{
		// hey your the one
		parent::__construct();
		$this->load->model('profile_model');
	}

	public function get()
	{ 
		$this->load->helper('simplify_datetime_range');
		$limit = 8;
		$where = array('ep.status'=>1,'p.is_deleted'=>0,'p.status'=>1,'DATE(p.end_date) < CURDATE()'=>null,'p.end_date !='=>'0000-00-00 00:00:00','p.start_date !='=>'0000-00-00 00:00:00', 'where_in'=>array('field'=>'p.platform_restriction', 'arr'=>array(0, WEB_ONLY, BOTH_PLATFORM)));

		if((int)$this->input->get('venue')){
			$venue = $this->profile_model->get_row(array('table'=>'tbl_venues','where'=>array('venue_id'=>$this->input->get('venue'))));
			if($venue)
				$where['LOWER(p.venue)'] = strtolower($venue->name);
		}			

		if((int)$this->input->get('region'))
			$where['p.region_id'] = (int)$this->input->get('region');

		if($this->input->get('event_type'))
			$where['LOWER(p.event_type)'] = strtolower($this->input->get('event_type'));

		
		if((string)$this->input->get('total_rows')==1){

  			$total_rows = $this->profile_model->get_rows(array('table'=>'tbl_backstage_events as p',
  																'join'=>array('table'=>'tbl_backstage_events_photos as ep','on'=>'p.backstage_event_id=ep.backstage_event_id'),
  																'group_by'=>'ep.backstage_event_id',
																'where'=>$where
	 														     )
													)->num_rows();
			$total_rows = ($total_rows % $limit == 0) ? $total_rows / $limit : (int)($total_rows / $limit) + 1;
			$this->output->set_output(json_encode(array('total_group'=>$total_rows)));

		}else{			
 					
			$offset = ((int)$this->input->get('track_group')) ? (int)$this->input->get('track_group') * $limit : 0;
			
			$rows = $this->profile_model->get_rows(array('table'=>'tbl_backstage_events as p',
																 'where'=>$where,
	  														     'join'=>array(array('table'=>'tbl_regions as r','on'=>'r.region_id=p.region_id','type'=>'left'),
	  														     				array('table'=>'tbl_backstage_events_photos as ep','on'=>'p.backstage_event_id=ep.backstage_event_id')),
	 														     'fields'=>'p.*, r.name as region_name, ep.media_duration',
	 														     'order_by'=>array('field'=>'p.date_added','order'=>'DESC'),
	 														     'group_by'=>'ep.backstage_event_id',
	 														     'offset'=>$offset,
	 														     'limit'=>$limit
	 														     )
													);
			//echo $this->db->last_query();
			
			$albums = array();



			if($rows->num_rows()){
				
				foreach($rows->result() as $v){

					$photos = array();
					$rows = $this->profile_model->get_rows(array('table'=>'tbl_backstage_events_photos',
																	'where'=>array('backstage_event_id'=>$v->backstage_event_id,'is_deleted'=>0,'status'=>1),
																	'limit'=>3,
																	'order_by'=>array('field'=>'photo_id',
																					  'order'=>'desc'
																					 )
																	)
															);

					if($rows){
						
						foreach($rows->result() as $p){
							if(file_exists("uploads/backstage/photos/thumbnails/331_176_".$p->media_image_filename)){
								$playbutton = $p->media_type=='video' ? '<div class="btn-play"> <i class="glyphicon glyphicon-play"></i> </div>' : '';
 								$photos[] = array('thumbnail'=>BASE_URL."uploads/backstage/photos/thumbnails/331_176_".$p->media_image_filename,'playButton'=>$playbutton);
							}
								
						}

						if($photos){

							$event = $v->venue;

							if($v->venue && $v->region_name) 
								$event .= ', ';

							if($v->region_name) 
								$event .= $v->region_name;		

		 					$schedule = simplify_datetime_range($v->start_date,$v->end_date);
		 					$event .= $schedule ? ' - '.$schedule : '';
		 					$albums[] = array('backstage_event_id'=>$v->backstage_event_id,
			 								  'album_name'=>$v->title,
											  'event'=>($event) ? $event : '',
											  'photos'=>$photos,
			 								  'url'=>BASE_URL.'backstage_pass/thumbnails/'.$v->url_title,
			 								  'duration'=> $v->media_duration == '00:00:00' ? null : $v->media_duration);

						}

					}
	 				  	 								   
 				}  # End foreach

			}  

			$this->output->set_output(json_encode($albums));

		}

	}

	
}