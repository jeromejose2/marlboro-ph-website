<?php

class Notifications extends NW_Controller
{
	public function get()
	{
		$result = array(
			'points' => 0, 
			'error' => '', 
			'notifications' => 0
		);
		$user = $this->session->userdata('user_id');
		if (!$user) {
			$result['error'] = 'Invalid user session';
		} else {
			$user = $this->db->select('total_notifications, total_points, login_id, device_id, app_id')
				->from('tbl_registrants')
				->where('registrant_id', $user)
				->limit(1)
				->get()
				->row();
			if (!$user) {
				$result['error'] = 'Unknown user';
			} elseif ( $user->app_id == $this->session->userdata('app_id') && $user->login_id != $this->session->userdata('login_id') ) {
                $result['force_logout'] = true;
                $this->session->clear();
            } 

			$result['points'] = (int) $user->total_points;
			$result['notifications'] = (int) $user->total_notifications;
			$this->session->set_userdata('user_points', $result['points']);
		}
		$this->output->set_output(json_encode($result));
	}
}