<?php

class Comments extends NW_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Comment_Model');
	}

	public function get()
	{
		$origin = $this->input->get('origin');
		$suborigin = $this->input->get('suborigin');
		if (!$origin || !$suborigin) {
			show_404();
		}

		$limit = 5;
		$page = (int) $this->input->get('page');
		if (!$page) {
			$page = 1;
		}
		$result = array();
		$result['total'] = $this->Comment_Model->get_comments_count($origin, $suborigin);
		$result['comments'] = $this->Comment_Model->get($origin, $suborigin, $page, $limit);
		$pages = ceil($result['total'] / $limit);
		if ($page < $pages) {
			$result['next'] = ++$page;
		} else {
			$result['next'] = false;
		}
		$this->output->set_output(json_encode($result));
	}

	public function post()
	{
		$origin = (int) $this->input->post('origin');
		$suborigin = (int) $this->input->post('suborigin');
		
		if (!$origin || !$suborigin) {
			show_404();
		}

		$result = array(
			'success' => true,
			'error' => ''
		);

		$this->load->library('form_validation');
		$this->form_validation->set_rules('text','Comment','required');

		if(!$this->form_validation->run()){
 			$result = array(
				'success' => false,
				'error' => validation_errors()
			);
			$this->output->set_output(json_encode($result));
			return;
		}
		 

		$media = $this->input->post('media');
		if (!$this->input->post('is_reply')) {
			$recipient = (int) $this->input->post('recipient');
			if ($recipient && $origin === MOVE_FWD) {
				$result['error'] = 'Invalid recipient';
				$result['success'] = false;
				$this->output->set_output(json_encode($result));
				return;
			}
			$comment = $this->input->post('text');
			if (!$this->Comment_Model->save($origin, $suborigin, $comment, $media, $recipient)) {
				$result['success'] = false;
				$result['error'] = 'Failed inserting comment';
			}
			$this->output->set_output(json_encode($result));
		} else {
			$commentId = $this->input->post('comment_id');
			if (!$commentId) {
				show_404();
			}
			$this->Comment_Model->reply_to($commentId, $this->input->post('text'), $media);
			$this->output->set_output(json_encode($result));
		}
	}
}