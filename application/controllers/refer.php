<?php if(!defined("BASEPATH")){ exit("No direct script access allowed!"); }

set_time_limit(0);
ini_set('memory_limit', '512M');
class Refer extends CI_Controller {

	var $edm;
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('referral_model');
		require_once 'application/libraries/Edm.php';
		$this->edm = new Edm();

		if($_SERVER['REMOTE_ADDR'] != '180.232.124.116' ) {
			redirect('/');
		}
	}

	public function reset_refcode(){
		exit();
		$this->db->update('tbl_registrants',  array('referral_code' => '' ));
	}
	public function insert_refcode(){
		exit();
		$chrA = 97;
		$chrZ = 122;
		$nwChr= $chrA;
		$nwChr2 = $chrA;
		$max = 20;
		$count = 1;
		$i = 1000;

		$registrants = $this->db
			->get('tbl_registrants')
			->result();

		if($registrants){
			$code = 'aa1000';
			foreach ($registrants as $r) {

				$this->db
				->where('registrant_id', $r->registrant_id)
				->update('tbl_registrants',  array('referral_code' => $this->generate_referral_code($code) ));
				$code = $this->generate_referral_code($code);
			}
		}
	}
	private function generate_referral_code($code){

		$digit = substr($code, 2);

		$digit++;
		$chr1 = ord($code[0]);
		$chr2 = ord($code[1]);
		$chrStart = 97;
		$chrEnd   = 122;


		if($digit>9999){
			$digit = 1000;
			$chr2++;
		}

		if($chr2>$chrEnd){
			$chr1++;
			$chr2=$chrStart;
		}

		$chr1 = $chr1 > $chrEnd ? $chrStart : $chr1;
		return chr($chr1) . chr($chr2) . $digit; 
	}

	public function ref(){
		$chrA = 97;
		$chrZ = 122;
		$nwChr= $chrA;
		$nwChr2 = $chrA;
		$max = 20;
		$count = 1;
		$i = 1000;
		while(true){

			$count++;
			$i++;
			if($count>=$max){
				break;
			}
			if($i>9999){
				$nwChr++;
				$i=1000;
			}

			if($nwChr>$chrZ){
				$nwChr2++;
				$nwChr=$chrA;
			}
			
			$nwChr = $nwChr > $chrZ ? $chrA : $nwChr; 
			$nwChr2 = $nwChr2 > $chrZ ? $chrA : $nwChr2; 

			echo chr($nwChr2).chr($nwChr).$i;
			echo "<br>";
		}
		
	}

	
	public function index(){


		$this->session->unset_userdata('referral_data');
		$this->session->unset_userdata('referral_post');
		$data = array('errors'=>null,'post'=>null);


		if($this->input->post()){

			$emails = $this->input->post('email');
			$data['post'] = $this->input->post();
			$errors = array();
			$post = array();

			foreach($emails as $email){

				$response = $this->spice->GetPerson(array('LoginName'=>$email));
				$response = $this->spice->parseJSON($response);

				$row = $this->db->get_where('tbl_referrals',array('email'=>$email))->row();
				
				if($row || $response->MessageResponseHeader->TransactionStatus === 0){

					$errors[] = $email;

				}

			}

			if(count($errors) > 1){
				
				$last = array_pop($errors);
				$errors = '<span>'.implode(',', $errors).' and '.$last.' are already exist.</span>';

			}else if(count($errors) > 0){

				$errors = '<span>'.implode(',', $errors).' is already exist.</span>';

			}else{

				$this->session->set_userdata('referral_post',$this->input->post());
				redirect('refer/confirm');

			}

			$data['errors'] = $errors;	

			
		}

		$this->load->layout('referral/index', $data);
	}

	public function confirm(){
		$post = $this->session->userdata('referral_post');

		if(!$post){
			redirect( BASE_URL .'refer');
		}
		
		$data['post'] = $this->session->userdata('referral_post');
		$data['sender'] = $this->session->userdata('user')['first_name'] .' '.$this->session->userdata('user')['last_name'];
		$data['senderEmail'] = $this->session->userdata('user')['email'] ;
		$this->load->layout('referral/confirm', $data);
	}
	public function done(){

		$post = $this->session->userdata('referral_post');


		//
		$errors = array();

		foreach($post['email'] as $email){

			$response = $this->spice->GetPerson(array('LoginName'=>$email));
			$response = $this->spice->parseJSON($response);

			$row = $this->db->get_where('tbl_referrals',array('email'=>$email))->row();
			
			if($row || $response->MessageResponseHeader->TransactionStatus === 0){

				$errors[] = $email;

			}

		}
		
		 

		if(count($errors) > 1){
			
			$last = array_pop($errors);
			$errors = '<span>'.implode(',', $errors).' and '.$last.' are already exist.</span>';
			$this->session->set_userdata('errors', $errors);
			redirect('refer');
			return;

		}else if(count($errors) > 0){

			$errors = '<span>'.implode(',', $errors).' is already exist.</span>';
			$this->session->set_userdata('errors', $errors);
			redirect('refer');
			return;
		}
		//

		if(!$post){
			redirect( BASE_URL .'refer');
		}
		
		
		$user = $this->session->userdata('user');
		for ($i=0; $i < count($post['email']); $i++) { 


			$response = $this->spice_send_mailing($post['name'][$i],$post['email'][$i],$user);
			$response_json = $this->spice->parseJSON($response);
 
			if(!$response_json->MessageResponseHeader->TransactionStatus){
					    
			    $result = $this->referral_model->save(
					$post['email'][$i],
					$this->session->userdata('user_id'),
					$this->session->userdata('user')['referral_code'],
					$post['name'][$i]
				);

				if($result=='success'){				 
 
					$data['result']['success'][] = array(
						'email' => $post['email'][$i],
						'name' => $post['name'][$i],
						'message' => @$result['message']);

				}

				if($result=='registered'){

					$data['result']['registered'][] = array(
						'email' => $post['email'][$i],
						'name' => $post['name'][$i],
						'message' => @$result['message']);
				}


				if($result=='pending'){

					$data['result']['pending'][] = array(
						'email' => $post['email'][$i],
						'name' => $post['name'][$i],
						'message' => @$result['message']);
				}

			}else{


				$AdvisoryMessageTypes = $response_json->AdvisoryMessageTypes;
				$message = $response_json->MessageResponseHeader->TransactionStatusMessage.': '.$AdvisoryMessageTypes[0]->Message;				
 

				$data['result']['mailing_failed'][] = array(
						'email' => $post['email'][$i],
						'name' => $post['name'][$i],
						'message' => $message);

				$sendMailing = array('origin_id'=>REFERRAL,
								'method'=>'SendMailing',
								'transaction'=>'Refer a friend by sending email to '.$post['email'][$i],
								'input'=>$this->session->userdata('spice_input'),
								'response'=>$response,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
				$this->spice->saveError($sendMailing);


			}

			
		}
        
        $this->session->unset_userdata('referral_post');

		$this->load->layout('referral/msg', $data);
	}

	public function spice_send_mailing($name, $email_address,$user){

		$PersonId = $user['person_id'];
		// $dlTemplateId = 1034; # OLD
		$dlTemplateId = 1011;
		// $cellId = 1813 # OLD
		$cellId = 1144;
		$login = $this->spice->createLoginToken($PersonId,$dlTemplateId, $cellId);
		$login_json = $this->spice->parseJSON($login);

		// $code = 'M00000434'; # OLD
		$code = 'M00000434';
 		$base_url = str_replace(array('http://','https://'), '', BASE_URL);
		$base_url = $base_url.'user/validate_refer_friend_token?token='.$login_json->LoginToken.'&email='.$email_address.'&referral_code='.$this->session->userdata('user')['referral_code'];
 
		$params = ['Email' => $email_address,
		            'MailingCode' => $code,
		            'IsSMTPMail' => false,
		            'Fields' => [   
		            				['Name' => 'ph_campaignurlpersonal', "ValueDataType"=>"String", 'ValueAsString' => $base_url],
		            				['Name' =>'ph_referredname',"ValueDataType"=>"String",'ValueAsString'=>@$name],
		                            ['Name' => 'ph_firstname', "ValueDataType"=>"String", 'ValueAsString' => $user['first_name'] ],
									['Name' =>'ph_lastname',"ValueDataType"=>"String",'ValueAsString'=> $user['last_name'] ],
		                            ['Name' => 'ph_emailaddress', "ValueDataType"=>"String", 'ValueAsString' => $email_address]
		                        ]
		            ];

	   return $this->spice->sendMailing($params);  

	}

	private function send_edm($las1_email, $las1_name, $las2_name, $code, $las2_email) {

		$recipient_added = $this->edm->add_recipient_referral(EDM_MARLBORO_REFER_LIST_ID, $las2_email, $las2_name);
		$sent = $this->edm->send_recipient_referral(EDM_MARLBORO_REFER_MAIL_ID, $las1_email, $las1_name, $las2_name, $code, $las2_email);

		if($recipient_added !== true)
			$r_error = $recipient_added;

		if($sent !== true)
			$s_error = $sent;
	}

	private function send_referral($v){
		$template = file_get_contents('application/views/referral/edm-template.html');
		$search = array('{{LAS2_FIRSTNAME}}','{{LAS1_FIRSTNAME}}','{{REFERRAL_CODE}}','{{url}}');
		$replace = array($v['las2_name'],$v['las1_name'],$v['ref_code'],BASE_URL);
		$message = str_replace($search, $replace, $template );
		
		/*
		$this->load->library('email');

		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['mailtype'] = 'html';

		$this->email->initialize($config);
		$this->email->from($v['las1_email'],$v['las1_name']);
		$this->email->to($v['las2_email']); 
		$this->email->subject('Join and start collecting points to get instant prizes');

		$this->email->message($message);	
		$this->email->send();
		echo $this->email->print_debugger();
		*/
		$to = $v['las2_email'];
		$subject  = 'Join and start collecting points to get instant prizes';
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: '.$v['las1_name'].' <'.$v['las1_email'].'>' . "\r\n";

		mail($to, $subject, $message, $headers);

	}
}
