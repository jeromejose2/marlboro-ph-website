<?php if(!defined("BASEPATH")){ exit('No direct script access allowed!'); }

class Convert extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('notification_model');
	}

	public function points(){
		//crossover registrants
		// $this->db = $this->load->database('crossover_local', true);
		$this->db = $this->load->database('crossover_production', true);
		$registrants = $this->db->get('tbl_registrants')->result();

		foreach($registrants as $registrant){
			$user_id = $registrant->user_id;
			$credit_km_points = $registrant->credit_km_points;
			
			//dbam registrants
			//$this->db = $this->load->database('default', true);
			// $this->db = $this->load->database('actual', true);
			$this->db = $this->load->database('prod', true);
			
			$dbam_registrant = $this->db->get_where('tbl_registrants', array('registrant_id' => $user_id))->row();

			if($dbam_registrant){
				$dbam_registrant_id = $dbam_registrant->registrant_id;
				$dbam_total_points = $dbam_registrant->total_points;
				$dbam_added_total_points = $dbam_total_points+$credit_km_points;

				$recorded =  $this->db->get_where('tbl_recorded_registrants', array('registrant_id' => $user_id))->row();
				$this->notification_model->notify($user_id, POINTS_UPDATE_FROM_CROSSOVER, ["message" => "We converted your ".$credit_km_points." remaining KMs to Points! ".$credit_km_points." points have been credited to your account.", "suborigin" => 0]);
				// if(!$recorded){
				// 	$data = array(
				// 	   'registrant_id' => $dbam_registrant_id,
				// 	   'total_points' => $dbam_total_points,
				// 	   'crossover_credit_km_points' => $credit_km_points,
				// 	);

				// 	// $this->db->insert('tbl_recorded_registrants', $data); 
				// 	// echo $dbam_registrant_id . " : " . $dbam_total_points . " + " . $credit_km_points . " =  ";
				// 	// echo $dbam_added_total_points."<br />";
				// 	$data_points = array(
				// 	   'total_points' => $dbam_added_total_points
				// 	);
				// 	// $this->db->where('registrant_id', $user_id);
				// 	// $this->db->update('tbl_registrants', $data_points);

				// 	$this->notification_model->notify($user_id, POINTS_UPDATE_FROM_CROSSOVER, ["message" => "We converted your ".$credit_km_points." remaining KMs to Points! ".$credit_km_points." points have been credited to your account.", "suborigin" => 0]);
				// }
			}
		}
		echo "Registrant Points Update Successful!";
	}

}