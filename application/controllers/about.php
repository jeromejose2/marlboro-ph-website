<?php if(!defined("BASEPATH")){ exit('No direct script access allowed!'); }

class About extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('About_Model');
	}

	public function brand_history()
	{
		$data = array();
		$data['history'] = $this->About_Model->get_brand_history();
		$this->load->layout('about/brand-history', $data); 
	}

	public function product_info()
	{
		$data = array();
		$data['products'] = $this->About_Model->get_product_info();
		$this->load->layout('about/product-info',$data); 
	}

	public function dontbeamaybe()
	{
		$data = array();
		$data['content'] = $this->About_Model->get_dbam_content();
		$this->load->layout('about/dontbeamaybe', $data); 
	}


	/*--------------*/
	public function gallery()
	{
		$data = array();
		$id = $this->input->get('id');
		if ($id) {
			$data['gallery'] = $this->About_Model->get_product_info_gallery($id);
			$this->load->view('about/product-info-gallery', $data);
		}
		
	}

}