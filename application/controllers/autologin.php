<?php

class Autologin extends CI_Controller
{
   public function index()
    {
        if($this->input->get('token'))
        {
            $tkn = $this->input->get('token');
                                  
            // print_r($autologin_json); die();
            if($this->session->userdata('_spice_member_session_key')){

                $token = $this->spice->validateLoginLoken($tkn);
                $token_json = $this->spice->parseJSON($token);
     
                if ($token_json->MessageResponseHeader->TransactionStatus == 0){                
                    if($id){                                    
                        redirect('http://marlboro-stage2.yellowhub.com/spice/marlboro-crossover/backend/activities/'.$id);
                    }else{
                        $successURL = $autologin_json->RedirectUrlParameters.'token='.$tkn;
                        redirect($successURL);
                    }
                }else{

                    $data_error = array('origin_id'=>'50',
                            'method'=>'validateLoginToken',
                            'transaction'=>$this->router->method,
                            'input'=> $this->session->userdata('spice_input'),
                            'response'=>$token,
                            'response_message'=>$token_json->MessageResponseHeader->TransactionStatusMessage
                        );

                    $this->session->set_userdata('response_message_error', $token_json->MessageResponseHeader->TransactionStatusMessage);
                    redirect('user/login');
                }

            }else{
                $autologin = $this->spice->autoLogin($tkn);
                $autologin_json = $this->spice->parseJSON($autologin);

                if($autologin_json->ResponseHeader->TransactionStatus != 0){
                    $data_error = array('origin_id'=> AUTOLOGIN,
                        'method'=>'autoLogin',
                        'transaction'=>$this->router->method,
                        'input'=> $this->session->userdata('spice_input'),
                        'response'=>$tkn,
                        'response_message'=>$autologin_json->MessageResponseHeader->TransactionStatusMessage
                    );

                    $this->spice->saveError($data);

                    $this->session->set_userdata('response_message_error',$autologin_json->ResponseHeader->TransactionStatusMessage);

                    redirect('user/login');           
                }else{
                    $this->load->model('Notification_Model');
                    $this->load->model('Registration_Model');
                    $this->spice->setCIAutoLoginMember($autologin_json->SessionKey, $autologin_json->PersonId);

                    if($autologin_json->SessionKey != '' && (int)$autologin_json->PersonId)
                    {

                        session_regenerate_id();

                        $response = $this->spice->GetPerson(array('PersonId'=>$autologin_json->PersonId));
                        $response = $this->spice->parseJSON($response);

                        $RegistrationDate = $this->spice->extractSpiceDate($response->ConsumerProfiles->RegistrationDate);
                        
                        $AgeVerificationStatus = $response->ConsumerProfiles->AgeVerificationStatus;
         
                        if(!$AgeVerificationStatus)
                        {
                            $this->session->set_userdata('invalid_login_message', 'Your Age Verification status has not been verified.');
                            redirect('user/login');
                        }                           

                        $ConsumerProfiles = $response->ConsumerProfiles;
                        $ConsumerProfiles = $this->spice->parseJSON($this->spice->toJSONFormat((array)$ConsumerProfiles));
                        
                        $total_derivations = count($ConsumerProfiles->Derivations);

                        for($i = 0; $i<$total_derivations; $i++){
                            if($ConsumerProfiles->Derivations[$i]->AttributeCode=='ca_referral_code'){
                                $Derivations_referral = $ConsumerProfiles->Derivations[$i];
                            }
                            // changes
                            // }else{
                            //  $Derivations_referral = '';
                            // }
                            // changes
                        }

                        $Derivations_referral = $Derivations_referral ? $Derivations_referral : '';

                        // $Derivations_referral = $ConsumerProfiles->Derivations[REFERRAL_CODE];
                        
                        $is_registered =  $this->db->select()->from('tbl_registrants')->where('person_id',$ConsumerProfiles->PersonId)->get()->row();

                        $province = strtolower($ConsumerProfiles->Addresses[0]->Locality);
                        $city = strtolower($ConsumerProfiles->Addresses[0]->City);
                        $street_name = $ConsumerProfiles->Addresses[0]->AddressLine1;
                        $barangay = $ConsumerProfiles->Addresses[0]->Area;
                        $zip_code = $ConsumerProfiles->Addresses[0]->PostalCode;

                        $date_of_birth = $this->spice->extractSpiceDate($ConsumerProfiles->PersonDetails->DateOfBirth);
                        
                        $province = $this->db->select()->from('tbl_provinces')->where('LOWER(province)',$province)->get()->row();
                        $province = $province ? $province->province_id : '';

                        $city = $this->db->select()->from('tbl_cities')->where(array('LOWER(city) '=>$city,'province_id'=>$province))->get()->row();
                        $city = $city ? $city->city_id : '';
                        $nick_name = '';
                        $outlet_code = '';
                        // $referral_code = $this->Registration_Model->referral_code();
                        $Derivations = array();

                        if($ConsumerProfiles->Derivations){

                            $total_derivations = count($ConsumerProfiles->Derivations);
                                
                            for($i = 0; $i<$total_derivations; $i++){
                                if($ConsumerProfiles->Derivations[$i]->AttributeCode=='ca_nickname'){
                                    $nick_name = $ConsumerProfiles->Derivations[$i]->AttributeValue;
                                }

                                if($ConsumerProfiles->Derivations[$i]->AttributeCode=='ca_outletcode'){
                                    $outlet_code = $ConsumerProfiles->Derivations[$i]->AttributeValue;
                                }

                                if($ConsumerProfiles->Derivations[$i]->AttributeCode=='ca_personal_referral_code'){
                                    $referral_code = $ConsumerProfiles->Derivations[$i]->AttributeValue ? $ConsumerProfiles->Derivations[$i]->AttributeValue : $this->Registration_Model->referral_code();
                                    // changes
                                    if( ! $ConsumerProfiles->Derivations[$i]->AttributeValue || $ConsumerProfiles->Derivations[$i]->AttributeValue == '0') {
                                        $ConsumerProfiles->Derivations[$i]->AttributeValue = $referral_code;
                                    }
                                    // changes
                                }else{
                                    //$ConsumerProfiles->Derivations[$i]->AttributeValue = '0';
                                }
                            }

                            // $nick_name = $ConsumerProfiles->Derivations[NICKNAME]->AttributeValue;
                            // $outlet_code = $ConsumerProfiles->Derivations[OUTLET_CODE]->AttributeValue;

                            $Derivations = json_decode(json_encode($ConsumerProfiles->Derivations),true);
                        }

                        
                        if( ! $ConsumerProfiles->Derivations[$i]->AttributeValue || $ConsumerProfiles->Derivations[$i]->AttributeValue == '0') {
                            $has_referral = $this->db->select('referral_code')->from('tbl_registrants')->where('person_id', (string)$ConsumerProfiles->PersonId)->get()->row();
                            if($has_referral->referral_code) {
                                $insert_new_user = array('person_id'=>(string)$ConsumerProfiles->PersonId,
                                             'first_name'=>(string)$ConsumerProfiles->PersonDetails->FirstName,
                                             'third_name'=>(string)$ConsumerProfiles->PersonDetails->LastName,
                                             'middle_initial'=>(string)$ConsumerProfiles->PersonDetails->MiddleName,
                                             'nick_name'=>$nick_name,
                                             'outlet_code'=>$outlet_code,
                                             'referred_by_code'=> ($Derivations_referral->AttributeValue && $Derivations_referral->AttributeCode==='ca_referral_code') ? $Derivations_referral->AttributeValue : ''
                                                );
                            } else {
                                $insert_new_user = array('person_id'=>(string)$ConsumerProfiles->PersonId,
                                             'first_name'=>(string)$ConsumerProfiles->PersonDetails->FirstName,
                                             'third_name'=>(string)$ConsumerProfiles->PersonDetails->LastName,
                                             'middle_initial'=>(string)$ConsumerProfiles->PersonDetails->MiddleName,
                                             'nick_name'=>$nick_name,
                                             'outlet_code'=>$outlet_code,
                                             'referral_code' => (string) $referral_code,
                                             'referred_by_code'=> ($Derivations_referral->AttributeValue && $Derivations_referral->AttributeCode==='ca_referral_code') ? $Derivations_referral->AttributeValue : ''
                                                );
                            }                   
                        } else {
                            $insert_new_user = array('person_id'=>(string)$ConsumerProfiles->PersonId,
                                             'first_name'=>(string)$ConsumerProfiles->PersonDetails->FirstName,
                                             'third_name'=>(string)$ConsumerProfiles->PersonDetails->LastName,
                                             'middle_initial'=>(string)$ConsumerProfiles->PersonDetails->MiddleName,
                                             'nick_name'=>$nick_name,
                                             'outlet_code'=>$outlet_code,
                                             'referred_by_code'=> ($Derivations_referral->AttributeValue && $Derivations_referral->AttributeCode==='ca_referral_code') ? $Derivations_referral->AttributeValue : ''
                                                );
                        }
                     
                        if($is_registered){

                            $this->db->where('person_id', $ConsumerProfiles->PersonId);
                            $this->db->update('tbl_registrants', $insert_new_user); 

                        }else{
                            $this->db->set('date_created',"'$RegistrationDate'",FALSE);
                            $this->db->insert('tbl_registrants', $insert_new_user);

                            
                            $params = array('person_id'=> $ConsumerProfiles->PersonId,
                                            'updates'=> array('Derivations' => $Derivations)
                                        );

                            $response = $this->spice->updatePerson($params);
                            $response_json = $this->spice->parseJSON($response);

                        }

                        $registrant = $this->db->select()->from('tbl_registrants')->where('person_id',$ConsumerProfiles->PersonId)->get()->row();       
                     
                        $user = array('login_attempts'=>$registrant->login_attempts,
                                      'date_blocked'=>$registrant->date_blocked,
                                      'person_id'=>$ConsumerProfiles->PersonId,
                                      'registrant_id'=>$registrant->registrant_id,
                                      'referred_by_code'=>$registrant->referred_by_code,
                                      'registrant_id'=>$registrant->registrant_id,
                                      'first_name'=>$ConsumerProfiles->PersonDetails->FirstName,
                                      'third_name'=>$ConsumerProfiles->PersonDetails->LastName,
                                      'middle_initial'=>$ConsumerProfiles->PersonDetails->MiddleName,
                                      'date_of_birth'=>$ConsumerProfiles->PersonDetails->DateOfBirth,
                                      'nick_name'=>$registrant->nick_name,
                                      'mobile_phone'=>$ConsumerProfiles->PersonDetails->MobileTelephoneNumber,
                                      'referral_code'=>$registrant->referral_code,
                                      'email_address'=>$ConsumerProfiles->Login[0]->LoginName,
                                      'total_points'=>$registrant->total_points,
                                      'status'=>$ConsumerProfiles->AgeVerificationStatus,
                                      'is_cm'=>$registrant->is_cm
                                  );                    

                        $this->load->library('Encrypt');

                        $today = date('Y-m-d H:i:s');                

                        // check if from referral;
                        $isFirstLogin = false;

                        if($user['referred_by_code']){
         
                            $referer = $this->Registration_Model->get_user_by_refCode($user['referred_by_code']);
                            if($referer){
                                $isFirstLogin = $this->Registration_Model->isFirstLogin($user['registrant_id']);
                            }
                            
                        }
         
                        $this->db->insert('tbl_login', array(
                            'registrant_id' => $user['registrant_id'],
                            'date_login' => $today
                        ));

                        if ($this->db->affected_rows()) 
                        {
                            $this->load->model(array('profile_model','Points_Model'));

                            $this->session->set_userdata('user', array(
                                'first_name' => $user['first_name'],
                                'last_name' => $user['third_name'],
                                'middle_initial' => $user['middle_initial'],
                                'birthdate' => $user['date_of_birth'],
                                'nick_name' => $user['nick_name'],
                                'mobile_num' => $user['mobile_phone'],
                                'referral_code' => $user['referral_code'],
                                'person_id'=> $user['person_id'],
                                'email' => $user['email_address']
                            ));

                            $this->session->set_userdata('check_offers', 1);
                            $this->session->set_userdata('user_points', $user['total_points']);
                            $this->session->set_userdata('user_id', $user['registrant_id']);
                            $this->session->set_userdata('login_id', $this->db->insert_id());
                            $this->session->set_userdata('is_cm', $user['is_cm']);
                            $this->session->set_userdata('flash_offer_sess', 1);


                            $this->db->where('registrant_id', $user['registrant_id']);
                            $this->db->update('tbl_registrants', array(
                                'login_attempts' => 0,
                                'date_blocked' => null,
                                'login_id' => $this->session->userdata('login_id')
                            ));

                            if(!$isFirstLogin)
                            {

                                if(date('Ymd') >= 20150401){
                                    $res = $this->Points_Model->earn_login_km(5, array(
                                        'date' => $today
                                    ));
                                }else{
                                    $this->Points_Model->earn(LOGIN, array(
                                        'date' => $today,
                                        'suborigin' => $this->session->userdata('login_id')
                                    ));
                                }
             
                                $this->db->close();
                                $this->db->where('registrant_id', $user['registrant_id']);
                                $this->db->update('tbl_registrants', array(
                                    'last_login' => $today
                                ));

                                if(!$user['referred_by_code']){
                                    $this->Points_Model->earn(REGISTRATION);                             
                                }
                                
                                //SPICE INSERT DERIVATIONS
                                $response = $this->spice->GetPerson(array('PersonId' =>$this->spice->getMemberPersonId()));
                                $spice_userinfo = $this->spice->parseJSON($response);
                                $spice_userinfo = $spice_userinfo->ConsumerProfiles;

                                $consumer_attributes = $this->profile_model->get_rows(array('table'=>'tbl_consumer_attr_spice'))->result_array();
                                
                                $total_derivations = count($spice_userinfo->Derivations);
                                $total_address = count($spice_userinfo->Addresses);
                                $total_cas = count($consumer_attributes);
                                $derivations = array();
                                $var = array();
                                $country = 'PH';
                                
                                // print_r($spice_userinfo); die();
                                for($i = 0; $i<$total_cas; $i++){       
                                                            
                                    $var = array('AttributeCode'=> $consumer_attributes[$i]['attribute_code'],'AttributeValue'=> '0');

                                    for($j = 0; $j<$total_derivations; $j++){
                                        if($consumer_attributes[$i]['attribute_code'] == $spice_userinfo->Derivations[$j]->AttributeCode){
                                            if($spice_userinfo->Derivations[$j]->AttributeValue == ''){
                                                $var = array('AttributeCode'=> $consumer_attributes[$i]['attribute_code'],'AttributeValue'=> '0');
                                                break;
                                            }else{
                                                $var = array('AttributeCode'=> $spice_userinfo->Derivations[$j]->AttributeCode,'AttributeValue'=> $spice_userinfo->Derivations[$j]->AttributeValue);
                                                break;
                                            }
                                        }else{
                                            $var = array('AttributeCode'=> $consumer_attributes[$i]['attribute_code'],'AttributeValue'=> '0');
                                            break;
                                        }
                                    }
                                    
                                    $derivations[] = $var;

                                }


                                if($total_address != 0){
                                    if($spice_userinfo->Addresses[0]->Country == ''){
                                        $country = 'PH';
                                    }

                                    $params = array('person_id'=> $this->spice->getMemberPersonId(),
                                                    'updates'=> array(
                                                        'AddressType' => array(
                                                            array(
                                                                'AddressLine1' => $spice_userinfo->Addresses[0]->AddressLine1,
                                                                'Area' => $spice_userinfo->Addresses[0]->Area,
                                                                'Locality' => $spice_userinfo->Addresses[0]->Locality,
                                                                'City' => $spice_userinfo->Addresses[0]->City, 
                                                                'Country' => $country,
                                                                'Premise'=> $country,
                                                                'PostalCode' => $spice_userinfo->Addresses[0]->PostalCode
                                                            )
                                                        ),
                                                        'Derivations'=> $derivations,
                                                )
                                    );
                                }else{
                                        $params = array('person_id'=> $this->spice->getMemberPersonId(),
                                                    'updates'=> array(
                                                        'Derivations'=> $derivations,
                                                )
                                        );
                                }

                                $response = $this->spice->updatePerson($params);
                                $response_json = $this->spice->parseJSON($response);


                                if ($response_json->MessageResponseHeader->TransactionStatus != 5) {
                                    
                                    $data_error = array('origin_id'=>LOGIN,
                                            'method'=>'ManagePerson',
                                            'transaction'=> 'update person derivations',
                                            'input'=> $this->session->userdata('spice_input'),
                                            'response'=>$response,
                                            'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
                                        );
                                    $this->spice->saveError($data_error);

                                    $this->session->set_userdata('response_message_error', $response_json->MessageResponseHeader->TransactionStatusMessage);
                                    redirect('user/login');
                                }
                            
                            }else{
                                $this->session->set_userdata('flash_referral_offer_sess',1);

                                $this->db->where('registrant_id', $user['registrant_id']);
                                $this->db->update('tbl_registrants', array(
                                    'last_login' => $today
                                ));

                                /// Insert spice data to website db                             
                                
                                $this->Points_Model->earn(
                                    REFEREE_FIRSTLOGIN,
                                    array(
                                        'registrant' => $user['registrant_id'],
                                        'suborigin' => $this->session->userdata('login_id'),
                                        'remarks' => 'first login as LAS2'
                                    ));

                                $this->Points_Model->earn(
                                    REFERER_FIRSTLOGIN,
                                    array(
                                        'registrant' => $referer['registrant_id'],
                                        'suborigin' => $this->session->userdata('login_id'),
                                        'remarks' => 'LAS2 ('. $user['registrant_id'] .') logged-in'
                                    ));

                                $notifyLas2 = "Welcome to the MARLBORO community! Here’s ".REFEREE_FIRST_LOGIN_POINT." points to get you started. ".
                                            "Discover the many exciting offers you can redeem on PERKS and continue visiting".
                                            " the website to get more points. And while you’re at it, why don’t you invite".
                                            " your adult smoker friends as well?";

                                $this->Notification_Model->notify($user['registrant_id'], REFEREE_FIRSTLOGIN, 
                                    array('message' => $notifyLas2,'suborigin'=>0));

                                $notifyLas1First = "Congratulations! Your friend, ".$user['first_name']." ".$user['third_name'].", ".
                                    "is now part of the MARLBORO community! You get 100 points ".
                                    "for the successful referral. Don't forget to invite ".$user['nick_name']." ".
                                    "to log in and experience the exclusive PERKS in the website.";

                                $this->Notification_Model->notify($referer['registrant_id'], REFERER_FIRSTLOGIN,
                                    array('message' => $notifyLas1First,'suborigin'=>0));

                                $notifyLas1 = "Congratulations! You get 200 more points! Your friend, ".
                                    $user['first_name']." ".$user['third_name'].", has just logged in to MARLBORO.PH.".
                                    " Invite your friends to be active members of the community,".
                                    " and get more points to win freebies.";

                                // $notifyLas1 = "Congratulations! You get ".REFERER_FIRST_LOGIN_POINT." more points! Your friend, ".
                                //  $user['first_name']." ".$user['third_name'].", has just logged in to MARLBORO.PH.".
                                //  " Invite your friends to be active members of the community,".
                                //  " and get more points to win freebies.";
                                
                                $this->Notification_Model->notify($referer['registrant_id'], REFERER_FIRSTLOGIN, 
                                    array('message' => $notifyLas1,'suborigin'=>0));

                            }
                            

                            $user_id = $this->session->userdata('user_id');
                            $hidden_packs = $this->profile_model->get_rows(array('table'=>'tbl_hidden_packs',
                                                                        'where'=>array("CURDATE() >= DATE(start_date) AND CURDATE() <= DATE(end_date) AND is_deleted = '0' AND hidden_pack_id NOT IN(SELECT hidden_pack_id FROM tbl_hidden_pack_logs WHERE registrant_id = '$user_id' AND DATE(date_added) = CURDATE())"=>null)));

                            if($hidden_packs->num_rows()){

                                $pages = '';
                                $temp = array();

                                foreach($hidden_packs->result() as $r){

                                    $pages = $r->page ? explode(',',$r->page) : false;

                                    if($pages){

                                        $key = rand(0,count($pages) - 1);
                                        $temp = array('id'=>$r->hidden_pack_id,'image'=>BASE_URL.'uploads/hidden_packs/'.$r->image);
                                        $this->session->set_userdata($pages[$key],$temp); 
                                    }

                                } // End foreach

                            }
                            $id = $this->input->get('id');

                            // print_r($id); die();
                            if(date('Ymd') >= 20140725)  {
                                // if($this->session->userdata('edm_popup')) {
                                //     redirect(BASE_URL . 'edm?' . $this->session->userdata('edm_popup'));
                                // } else {
                                //     //redirect(BASE_URL . 'premium');   
                                //     redirect(BASE_URL . 'edm?' . $this->session->userdata('edm_popup'));
                                // }
                                 //redirect(BASE_URL);
                                if($id){
                                    
                                    redirect('http://marlboro-stage2.yellowhub.com/spice/marlboro-crossover/backend/activities/'.$id);
                                }else{
                                    $successURL = $autologin_json->RedirectUrlParameters.'token='.$tkn;
                                    redirect($successURL);
                                }
                            }else{
                                //redirect(BASE_URL);
                                if($id){
                                    
                                    redirect('http://marlboro-stage2.yellowhub.com/spice/marlboro-crossover/backend/activities/'.$id);
                                }else{
                                    $successURL = $autologin_json->RedirectUrlParameters.'token='.$tkn;
                                    redirect($successURL);
                                }
                            }

                        }
                    }                  
                } 
            }          
        }else{
            redirect('user/login?success=0');
        }
    }
}