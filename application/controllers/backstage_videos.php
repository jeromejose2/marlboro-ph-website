<?php if(!defined("BASEPATH")){ exit('No direct script access allowed!'); }

class Backstage_videos extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('profile_model');
	}

	public function index()
	{
		$this->load->helper('text');

		$rows = $this->profile_model->get_rows(array('table'=>'tbl_backstage_videos',
													 'where'=>array('is_deleted'=>0,'status'=>1),
													  'order_by'=>array('field'=>'date_added',
																		'order'=>'DESC'
																		)
													)
												);
		$videos = array();
		 

		if($rows->num_rows()){
			foreach($rows->result() as $v){
 
				if ($v->user_type=='administrator'){
					$video = file_exists('uploads/backstage/videos/'.$v->thumbnail) ? BASE_URL.'uploads/backstage/videos/'.$v->thumbnail : '';
				}else{
					$video = $this->parse_youtube_url($v->video,'hqthumb');
				}
 				$videos[] = array('about_video_id'=>$v->about_video_id,'video_thumbnail'=>$video,'title'=>$v->title,'url_title'=>$v->url_title, 'video_duration' => $v->video_duration);
			}
		}

		$data['rows'] = json_decode(json_encode($videos),false);
		$this->load->layout('backstage/videos-list',$data); 
	}

	public function content()
	{
		$this->load->helper('file');
		$this->load->model(array('Comment_Model','View_Model'));

		$url_title = strtolower(trim($this->uri->segment(4)));
		$row = $this->profile_model->get_row(array('table'=>'tbl_backstage_videos',
													'where'=>array('is_deleted'=>0,
																	'status'=>1,
																	'LOWER(url_title)'=>$url_title
																)
													)
											);
		$video = array();

		if($row){
			
			$video_mime_type = '';
			$video_embed = '';
			$url_title = $row->url_title;
			$id = $row->about_video_id;
			$video_resolution = '';

			$data['prev'] = $this->profile_model->get_rows(array('table'=>'tbl_backstage_videos',
														'where'=>array('about_video_id > '=>$row->about_video_id,'status'=>1,'is_deleted'=>0),
														'order_by'=>array('field'=>'about_video_id','order'=>'ASC'),
														'limit'=>'1',
														'fields'=>'url_title'
														)
												)->row();

			$data['next'] = $this->profile_model->get_rows(array('table'=>'tbl_backstage_videos',
														'where'=>array('about_video_id <'=>$row->about_video_id,'status'=>1,'is_deleted'=>0),
														'order_by'=>array('field'=>'about_video_id','order'=>'DESC'),
														'limit'=>'1',
														'fields'=>'url_title'
														)
												)->row();


			if ($row->user_type=='administrator'){
				$video_mime_type = get_mime_by_extension('uploads/backstage/videos/'.$row->video);
  			}else{
				$video_embed = $this->parse_youtube_url($row->video,'embed','100%','315');
			}
 			$data['total_comments'] = $this->Comment_Model->get_comments_replies_count(BACKSTAGE_VIDEOS, $id);
			$this->View_Model->visit(BACKSTAGE_VIDEOS, $id);
  			$views = $this->View_Model->get_visits_count(BACKSTAGE_VIDEOS, $id);
  			$this->profile_model->update('tbl_backstage_videos',array('views'=>$views),array('about_video_id'=>$id));
 			$video = array('video_mime_type'=>$video_mime_type,'video'=>BASE_URL.'uploads/backstage/videos/'.$row->video,'uploader_id'=>$row->uploader_id,'user_type'=>$row->user_type,'about_video_id'=>$row->about_video_id,'title'=>$row->title,'description'=>$row->description,'views'=>$views,'video_embed'=>$video_embed);
 			
		}

		$data['url_title'] = $url_title;
		$data['row'] = json_decode(json_encode($video),false);
 	    $this->load->layout('backstage/video-content',$data);

 	}


 	public function get_upload_form()
 	{
 		$this->load->view('about/video-upload');
 	}


	public function submit_yvideo()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('title', 'Video Title', 'trim|required');
		$this->form_validation->set_rules('url', 'Video URL', 'trim|required|callback_validate_YoutubeURL');
		$this->form_validation->set_error_delimiters('','');
 		
 
		if($this->form_validation->run()){
			$user = $this->session->userdata('user');
			$uploader_id = $this->session->userdata('user_id');

			$id = $this->profile_model->insert('tbl_backstage_videos',array('uploader_id'=>$uploader_id,
			 													   'uploader_name'=>$user['first_name'].' '.$user['middle_initial'].' '.$user['last_name'],
																    'user_type'=>'registrant',
																    'title'=>$this->input->post('title'),
																	'video'=>$this->input->post('url'),
																	'status'=>$this->session->userdata('is_cm') ? 1 : 0,
																	'date_added'=>TRUE)
											);
			$this->profile_model->update('tbl_backstage_videos',array('url_title'=>url_title($id.'-'.ucwords($this->input->post('title')))),array('about_video_id'=>$id));
			
 			$data = array('msg'=>"<h2>THANK YOU FOR YOUR ENTRY</h2>Your video submission was successfully sent and is now subject to moderation. Please allow 24-48 hours for approval.",
							  'error_holder'=>'',
							  'error'=>false,
 							  'btn'=>'',
 							  'about_video_id' => $id,
							  'btn_text'=>'');

		}else{
 
			$data = array('msg'=>trim(validation_errors()),
							  'error_holder'=>'error-video-upload',
							  'error'=>true,
 							  'btn'=>'btn-submit-video',
							  'btn_text'=>'<i>SAVE</i>');
		}

		$this->load->view('profile/submit-response',$data);

	}

	function validate_YoutubeURL($url) {

	    // Let's check the host first
	    $parse = parse_url($url);
	    $host = @$parse['host'];
	    if (!in_array($host, array('youtube.com', 'www.youtube.com'))) {
	    	$this->form_validation->set_message('validate_YoutubeURL','Please submit YouTube video URL.');
	        return false;
	    }

	    $ch = curl_init();
	    $oembedURL = 'www.youtube.com/oembed?url=' . urlencode($url).'&format=json';
	    curl_setopt($ch, CURLOPT_URL, $oembedURL);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	    // Silent CURL execution
	    $output = curl_exec($ch);
	    unset($output);

	    $info = curl_getinfo($ch);
	    curl_close($ch);

	    if ($info['http_code'] !== 404)
	        return true;
	    else {
	    	$this->form_validation->set_message('validate_YoutubeURL','Please submit YouTube video URL.');
	    	return false;

	    }
	        
	}


	public function parse_youtube_url($url,$return='',$width='',$height='',$rel=0){

			$urls = parse_url($url);

		    

			//url is http://youtu.be/xxxx

			if(@$urls['host'] == 'youtu.be'){

				$id = ltrim(@$urls['path'],'/');

			}

			//url is http://www.youtube.com/embed/xxxx

			else if(strpos(@$urls['path'],'embed') == 1){

				$id = end(explode('/',@$urls['path']));

			}

			 //url is xxxx only

			else if(strpos($url,'/')===false){

				$id = $url;

			}

			//http://www.youtube.com/watch?feature=player_embedded&v=m-t4pcO99gI

			//url is http://www.youtube.com/watch?v=xxxx

			else{

				parse_str(@$urls['query']);

				$id = @$v;

				

				/*if(!empty($feature)){

					$id = end(explode('v=',$urls['query']));

				}

				*/

			}

			//return embed iframe

			if($return == 'embed'){

				return '<iframe width="'.($width?$width:560).'" height="'.($height?$height:349).'" src="http://www.youtube.com/embed/'.$id.'?&wmode=opaque&autoplay=1&rel='.$rel.'&fs=0&modestbranding=1&controls=1&cc_load_policy=1" frameborder="0"></iframe>';

			}
			
			if($return == 'embed_url'){

				return 'http://www.youtube.com/embed/'.$id.'?rel='.$rel.'"';

			}

			//return normal thumb

			else if($return == 'thumb'){

				return 'http://i1.ytimg.com/vi/'.$id.'/default.jpg';

			}

			//return hqthumb

			else if($return == 'hqthumb'){

				return 'http://i1.ytimg.com/vi/'.$id.'/hqdefault.jpg';

			}

			// else return id

			else{

				return $id;

			}

	}


}