<?php if ( !defined( "BASEPATH" ) ) { exit( "No direct script access allowed!" ); }

class Login_offer extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model( 'profile_model' );
		$this->load->model( 'Points_Model' );
	}

	public function get_offer() {
		// $this->load->helper( 'file' );
		// $prize_id = $this->input->get( 'prize_id' );
		// $user = $this->session->userdata( 'user' );
		// $data['type'] = $this->input->get( 'type' );
		// $data['row'] = $this->profile_model->get_row( array( 'table'=> 'tbl_flash_offers', 'where'=>array( 'prize_id' =>$prize_id ) ) );
		// $this->load->view( 'flash_offer/flash-offer', $data );
		$offer = $this->db->select('*')->from('tbl_login_offers')->where('is_active', 1)->order_by('date_created', 'desc')->get()->row();
		$registrant_id = $this->session->userdata('user_id');

		if(!$this->validate_offer_confirmation($registrant_id, $offer->id)){
			$insert_data['registrant_id'] = $registrant_id;
			$insert_data['offer_id'] = $offer->id;
			$insert_data['last_date_login'] = $this->session->userdata('last_login');
			$insert_data['date_confirmed'] = date('Y-m-d H:i:s');

			$insert = $this->db->insert('tbl_confirm_login_offers', $insert_data);

			// //GIVE POINTS
			$insert = $this->db->insert('tbl_points', array(
				'registrant_id' => $registrant_id,
				'origin_id' => LOGIN_PROMO,
				'suborigin_id' => $offer->id,
				'points' => $offer->points ,
				'point_status' => 1,
				'date_earned' => $insert_data['date_confirmed'],
				'remarks' => "Registrant ID: ".$registrant_id." claimed ".$offer->points." points from Login Promo: ".$offer->id
			));

			// PANG SAMANTALA
			$registrant = $this->db->select('*')->from('tbl_registrants')->where('registrant_id', $registrant_id)->get()->row();
			$data_update = array(
               'total_points' => $registrant->total_points+$offer->points
            );

			$this->db->where('registrant_id', $registrant_id);
			$this->db->update('tbl_registrants', $data_update);

			$this->load->model( 'notification_model' );
			$message = 'Welcome back! We credited '.$offer->points.' bonus points to your account. Check out all the new stuff we\'re giving away at PERKS, and use your points to BUY the item you want.';
			$param = array( 'message'=>$message, 'suborigin'=>$offer->id);
			$this->notification_model->notify( $registrant_id, LOGIN_PROMO, $param );

			$data['offer'] = $offer;

			$this->session->set_userdata('login_promo', 0);
		}else{
			$data['offer'] = "";
			$this->session->set_userdata('login_promo', 0);
		}

		

		$this->load->view( 'login_offer/login-offer', $data);
	}

	public function confirm_offer() {
		echo $_POST['offer_id']."<-aus to!";
	}

	private function validate_offer_confirmation($registrant_id, $offer_id) {
		$offer = $this->db->select('*')->from('tbl_confirm_login_offers')
										->where('registrant_id', $registrant_id)
										->where('offer_id', $offer_id)->get()->row();
		if($offer){
			return true;
		}else{
			return false;
		}
	}

	private function validate_offer_confirmation_by_date(){
		$registrant_id = $this->session->userdata('user_id');

		$registrant_data = $this->db->select('*')->from('tbl_registrants')
										->where('registrant_id', $registrant_id)
										->get()->row();
		
		$last_login = date_create($registrant_data->last_login);
		$last_login_formatted = date_format($last_login, "Y/m/d");
		$today = date('Y/m/d');

		$lastlogintimestamp = strtotime($last_login_formatted);
		$todaytimestamp = strtotime($today);

		$timeDiff = abs($todaytimestamp - $lastlogintimestamp);
		$numberDays = $timeDiff/86400;

		$numberDays = intval($numberDays);


		$login_promo = $this->db->select('*')->from('tbl_login_offers')
									->where('is_active', 1)
									->order_by('date_created', 'desc')->get()->row();

		if($numberDays>=$login_promo->days_inactive){
			$today = date('Y-m-d');
			if((str_replace("-", "", $today)>=str_replace("-", "", $login_promo->start_date)) and (str_replace("-", "", $today)<=str_replace("-", "", $login_promo->end_date))){
				// echo "granted";
				// echo $login_promo->days_inactive."<br />";
				// echo $numberDays;
				// exit;
				return false;
			}else{
				// echo "nd pasok ung date range";
				// exit;
				return true;
			}
		}else{
			// echo "nd pasok ung required days of login mo";
			// exit;
			return true;
		}
	}
}
