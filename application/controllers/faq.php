<?php
class Faq extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$this->load->model('settings_model');
	}
		
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$this->load->view('template', $data);
	}
	
	private function main_content() {
		$data['faq'] = $this->settings_model->get_settings('faq');
		$data['error'] = '';
		$data['title'] = 'Frequently Asked Questions';
		return $this->load->view('faq', $data, true);		
	}
}