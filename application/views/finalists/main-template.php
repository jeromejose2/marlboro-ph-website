<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, height=device-height" />
    <title>Marlboro</title>
    <link rel="stylesheet" href="<?php echo BASE_URL ?>assets_finalist/css/main.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- For third-generation iPad with high-resolution Retina display: -->
    <link rel="icon" type="image/png" sizes="144x144" href="<?php echo BASE_URL ?>assets_finalist/img/favicon/apple-touch-icon-144x144.png">
    <!-- For iPhone with high-resolution Retina display: -->
    <link rel="icon" type="image/png" sizes="114x114" href="<?php echo BASE_URL ?>assets_finalist/img/favicon/apple-touch-icon-114x114.png">
    <!-- For first- and second-generation iPad: -->
    <link rel="icon" type="image/png"  sizes="72x72" href="<?php echo BASE_URL ?>assets_finalist/img/favicon/apple-touch-icon-72x72.png">
    <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
    <link rel="icon" type="image/png" href="<?php echo BASE_URL ?>assets_finalist/img/favicon/apple-touch-icon.png">
</head>

<body>
    <!--[if lt IE 10]>
  <p class="browsehappy">
    You are using an <strong>outdated</strong> browser. Please
    <a href="http://browsehappy.com/">upgrade your browser</a> to improve
    your experience.
  </p>
<![endif]-->
<div class="scrollbar-inner">
     <header>
        <!-- subheader -->
        <div class="sub-header clearfix">
            <a class="sub-logo" href="../index.php">
                <img src="<?php echo BASE_URL ?>assets_finalist/img/logo.png" alt="Malboro - Cross|Over">
            </a>
        </div>

        <!-- burger -->
        <div class="burger">
            <span></span>
        </div>


        <div class="container">

            <div class="brand">
                <a href="<?php echo BASE_URL ?>">
                    <img src="<?php echo BASE_URL ?>assets_finalist/img/logo.png" alt="Malboro - Cross|Over" />
                </a>
            </div>
            
            <nav class="main-nav clearfix">
                    <a class="nav-brand" href="<?php echo BASE_URL ?>">
                        <img src="<?php echo BASE_URL ?>assets_finalist/img/logo.png" alt="Malboro - Cross|Over" />
                    </a>
                    <div class="clearfix"></div>

                    <span>
                        <!-- <a href="index.html">ABOUT  <i class="fa fa-chevron-down"></i>          <i class="fa fa-caret-right"></i></a> -->
                        <a href="javascript:;" data-nav="t-about">ABOUT  <i class="fa fa-chevron-down"></i>          <i class="fa fa-caret-right"></i></a>
                        <ul class="inner t-about">
                            <li><a href="<?= BASE_URL ?>about/dont-be-a-maybe">Don't be a maybe</a></li>
                            <li><a href="<?= BASE_URL ?>about/brand_history">Brand History</a></li>
                            <li><a href="<?= BASE_URL ?>about/product_info">Product Info</a></li>
                        </ul>
                    </span>

                    <span class="backstagepass">
                        <!-- <a href="index.html">BACKSTAGE PASS <i class="fa fa-chevron-down"></i>  <i class="fa fa-caret-right"></i></a> -->
                        <a href="javascript:;" data-nav="t-backstage">BACKSTAGE PASS <i class="fa fa-chevron-down"></i>  <i class="fa fa-caret-right"></i></a>
                        <ul class="inner t-backstage">
                            <li><a href="<?= BASE_URL ?>events/hot">WHAT'S HOT</a></li>
                            <li><a href="<?= BASE_URL ?>events/upcoming">UPCOMING EVENTS</a></li>
                            <li><a href="<?= BASE_URL ?>events/past">GALLERY</a></li>
                            <li><a href="<?= BASE_URL ?>events/videos">VIDEOS</a></li>
                        </ul>
                    </span>

                    <span>
                        <!-- <a href="perks.php">PERKS <i class="fa fa-chevron-down"></i>            <i class="fa fa-caret-right"></i></a> -->
                        <a href="javascript:;" data-nav="t-crossover">CROSSOVER <i class="fa fa-chevron-down"></i>            <i class="fa fa-caret-right"></i></a>
                        <ul class="inner t-crossover">
                            <li><a href="<?= BASE_URL ?>finalists">FINALISTS</a></li>
                            <li><a href="<?= BASE_URL ?>events/past">GALLERY</a></li>
                        </ul>
                    </span>

                    <span class="perks">
                        <!-- <a href="index.html">BACKSTAGE PASS <i class="fa fa-chevron-down"></i>  <i class="fa fa-caret-right"></i></a> -->
                        <a href="javascript:;" data-nav="t-perks">PERKS <i class="fa fa-chevron-down"></i>  <i class="fa fa-caret-right"></i></a>
                        <ul class="inner t-perks">
                            <li><a href="<?= BASE_URL ?>perks/buy">BUY</a></li>
                        </ul>
                    </span>

                    <span class="movefwd">
                        <!-- <a href="index.html">BACKSTAGE PASS <i class="fa fa-chevron-down"></i>  <i class="fa fa-caret-right"></i></a> -->
                        <a href="javascript:;" data-nav="t-movefwd">MOVEFWD <i class="fa fa-chevron-down"></i>  <i class="fa fa-caret-right"></i></a>
                        <ul class="inner t-movefwd">
                            <li><a href="<?= BASE_URL ?>move_forward">ACTIVE OFFERS</a></li>
                            <li><a href="<?= BASE_URL ?>move_forward/past">PAST OFFERS</a></li>
                        </ul>
                    </span>

                    <span class="games">
                        <!-- <a href="events.html">GAMES <i class="fa fa-chevron-down"></i>          <i class="fa fa-caret-right"></i></a> -->
                        <a href="<?php echo BASE_URL ?>games" data-nav="t-games">GAMES <?php /* <i class="fa fa-chevron-down"></i>          <i class="fa fa-caret-right"> */ ?></i></a>
                        <?php /* <ul class="inner t-games">
                            <li><a href="javascript:;">GAMES#1</a></li>
                            <li><a href="javascript:;">GAMES#2</a></li>
                            <li><a href="javascript:;">GAMES#3</a></li>
                        </ul> */ ?>
                    </span>

                    <span>
                        <a href="<?php echo BASE_URL ?>profile">MY ACCOUNT <i class="fa fa-user"></i>  <!-- <i class="fa fa-caret-right"></i> --> </a>

                        <!-- <a href="javascript:;" data-nav="t-myaccount">MY ACCOUNT <i class="fa fa-user"></i>              <i class="fa fa-caret-right"></i></a> -->
                    </span>

                    <span>
                        <!-- <a href="movefwd.php">MOVE FWD <i class="fa fa-chevron-down"></i>        <i class="fa fa-caret-right"></i></a> -->
                        <a href="<?php echo BASE_URL ?>welcome/logout" data-nav="t-movefwd">LOGOUT <?php /* <i class="fa fa-chevron-down"></i>         <i class="fa fa-caret-right"></i> */ ?></a>
                        <?php /* <ul class="inner t-movefwd">
                            <li><a href="<?= BASE_URL ?>move_forward">ACTIVE OFFERS</a></li>
                            <?php foreach ($this->nav_movefwd_categories as $category): ?>
                            <li><a href="<?= BASE_URL ?>move_forward/category/<?= $category['category_id'] ?>"><?= $category['category_name'] ?></a></li>
                            <?php endforeach ?>
                        </ul> */ ?>
                    </span>     

                    

            <!-- nav end -->
            </nav>

            <ul class="sub-nav clearfix">
                <li>
                    <a href="<?php echo BASE_URL ?>points">
                        <span class="tablet-icon"><i class="fa fa-stop"></i></span>
                        <span>POINTS:</span><span class="points" id="points"><?php echo (int) $this->session->userdata('points') ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL ?>notifications">
                        <span class="tablet-icon"><i class="fa fa-exclamation-circle"></i></i></span>
                        <span>NOTIFICATIONS</span><span class="notification-icon"><i class="fa fa-exclamation-circle" style="color : #<?php echo $this->unread_notifications ? 'F00' : '000' ?>" id="notifs"></i></span>
                    </a>
                </li>
                <?php /* <li>
                    <a href="<?php echo BASE_URL ?>faq">
                        <span class="tablet-icon"><i class="fa fa-info-circle"></i></span>
                        <span>FAQ</span><span  class="faq-icon"><i class="fa fa-info-circle"></i></span>
                    </a>
                </li> */ ?>
                <?php /* <li>
                    <a href="javascript:;" onclick="alert('LOGOUT');">
                        <span class="tablet-icon"><i class="fa fa-sign-out"></i></i></span>
                        <span>LOGOUT</span><span class="none"></span>
                    </a>
                </li> */ ?>
            </ul>

        <!-- container end -->
        </div>
    </header>

    <!-- CONTENT -->
        <?php echo $content ?>
    <!-- CONTENT -->

    <div class="spacer"></div>
    <!-- FOOTER -->
    <footer>

       <div class="ft-dw">
           <div class="container">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <ul class="nav-ft">
                    <?php /* <li><a href="">terms and conditions</a></li> */ ?>
                    <?php /* <li><a href="">crossover finalist</a></li> */ ?>
                    <?php /* <li><a href="#">crossover</a></li> */ ?>
                    <?php /* <li><a href="#">network</a></li> */ ?>
                </ul>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <ul class="nav-ft text-right">
                    <li><a data-toggle="modal" data-target="#terms">terms and conditions</a></li>
                    <li><a data-toggle="modal" data-target="#prvcy">privacy policy</a></li>
                    <li><a data-toggle="modal" data-target="#contact">contact us</a></li>
                    <li><a href="http://www.pmi.com/eng/our_products/health_effects_of_smoking/pages/health_effects_of_smoking.aspx" target="_blank">smoking and health</a></li>
                </ul>
            </div>
            </div>
        </div>
        <div class="ft-up clearfix">
            <div class="container">
                <div class="col-md-9 col-sm-6 col-xs-12">
                    <p>This website is for adult smokers 18 years or older residing in the Philippines. Access to this website is subject to age verification.</p>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <p>copyright © 2013 pmftc inc. all rights reserved</p>
                </div>
            </div>
        </div>

        <div class="container warn">
            <p>government warning: cigarette smoking is dangerous to your health</p>
        </div>

    </footer>


    <?php /* OLD TERMS <div class="modal fade" id="terms" tabindex="-1" role="dialog">
         <div class="vertical-alignment-helper">
          <div class="modal-dialog vertical-align-center" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
                <div class="prkspp-cntnt">
                    <h2>Terms and Conditions</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem voluptatibus sunt accusamus adipisci ab beatae accusantium blanditiis impedit, odit ducimus repellendus reprehenderit alias totam quaerat ex quod possimus amet, voluptas.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem voluptatibus sunt accusamus adipisci ab beatae accusantium blanditiis impedit, odit ducimus repellendus reprehenderit alias totam quaerat ex quod possimus amet, voluptas.</p>gulp
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem voluptatibus sunt accusamus adipisci ab beatae accusantium blanditiis impedit, odit ducimus repellendus reprehenderit alias totam quaerat ex quod possimus amet, voluptas.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div> */ ?>

    <div class="modal fade" id="terms" tabindex="-1" role="dialog">
         <div class="vertical-alignment-helper">
          <div class="modal-dialog vertical-align-center" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
               <h2>Terms and Conditions</h2>
                <div class="prkspp-cntnt scrollbar-inner">            
                    <p>This website (the "Website") is owned and operated by or on behalf of PMFTC Inc. ("PM") based in the Philippines for verified adult smokers who have been provided with a personal username and password of access. In the course of logging onto the Website, you will have been asked to read and agree to these Terms and Conditions. Therefore, by logging onto the Website, you will be assumed to have read and agreed to these Terms and Conditions.</p>
                    <h4>WEBSITE USERS</h4>
                    <p>The Website is intended for verified adult smokers who have been provided with a username and password for access. Your username and password are strictly personal and must not be shared with anyone else.</p>
                    <h4>USE OF THE WEBSITE AND MATERIALS</h4>
                    <p>The Website and all its contents (including but not limited to messages posted on the forum, if any, software, files, graphics, data, images and other material) ("Material") are the exclusive property of PM, its affiliates, and/or its licensors and are protected by the intellectual property laws of the Philippines as well as in other countries. The Website and the Material are provided solely for your personal use. Any other use of the Website or the Material, including but not limited to:</p>
                        <ul>
                            <li>using the Website or Materials for business purposes;</li>
                            <li>distributing, sharing, copying, reproducing or publishing the Material; or</li>
                            <li>modifying, distributing, transmitting, performing, broadcasting, reproducing, publishing licensing, or reverse engineering parts or all of the Website, without the prior written permission of PM, is expressly prohibited.</li>
                        </ul>
                    
                    <h4>FORUM</h4>
                    <p>To the extent the Website includes a forum that allows interaction between users, you are hereby made aware (i) that PM, its affiliates and/or representatives acting on their behalf regularly monitors the information posted on the forum by users, and (ii) that PM reserves the right (which may be exercised at their sole discretion without notice) to delete, remove, move or edit comments or messages you have posted. PM and its affiliates also reserve themselves the right to terminate, or have terminated, your access to and use of the Website and/or forum at their sole discretion. You hereby waive any rights including any rights that you may have in regard to messages you have posted on this forum.</p>
                    <p>Without limiting the right of PM to reject or remove any comment or message posted in the Website, any comment or message you post or any submission you make must not:</p>
                        <ul>
                            <li>contain contact information (i.e., phone numbers, etc.);</li>
                            <li>contain swear words or other language likely to offend;</li>
                            <li>be racist, sexist, sexually explicit, abusive, or otherwise objectionable;</li>
                            <li>be derogatory to other users or other brands;</li>
                            <li>break the law, or condones or encourages unlawful activity;</li>
                            <li>refer to or contain a cigarette or the use of a cigarette;</li>
                            <li>refer to or contain alcoholic products;</li>
                            <li>refer to or contain a minor (i.e., a person less than eighteen (18) years old);</li>
                            <li>refer to or contain a cartoon; or</li>
                            <li>be otherwise unsuitable in any manner.</li>
                        </ul>
                    
                    <p>The comments made on the forum (or blog) shall focus on aspects unrelated to smoking and health. Smoking is dangerous and addictive. More information about this is available at: <a href="http://www.pmi.com/eng/our_products/health_effects_of_smoking/pages/ health_effects_of_smoking.aspx">http://www.pmi.com/eng/our_products/health_effects_of_smoking/pages/ health_effects_of_smoking.aspx</a></p>
                    <p>Any comments made about smoking and health will be removed from the site.</p>  
                    <p>You acknowledge that you are solely responsible for any information, data, personal views or beliefs, or any material whatsoever that you submit for display on the forum, including the accuracy, legality, reliability and appropriateness of all such information, data, personal views or material. In posting messages on the forum, you must not:</p>
                        <ul>
                            <li>post, link or otherwise publish any messages containing any form of advertising or promotion for products or services, chain messages or “spam”;</li>
                            <li>post, link to or otherwise publish any messages with recommendations to buy or refrain from buying a particular security or which contain confidential information of another party or which otherwise have the purpose of affecting the price or value of any traded security;</li>
                            <li>post, link to or otherwise publish any messages that are unlawful, threatening, discriminatory, abusive, libelous, indecent, infringe copyright or other rights of third parties or which contain any form of illegal content;</li>
                            <li>disguise the origin of any message;</li>
                            <LI>impersonate any person or entity (including forum users or hosts) or misrepresent any affiliation with any person or entity;</LI>
                            <li>post or otherwise publish any messages unrelated to the forum;</li>
                            <li>post or transmit any messages that contain software viruses, files or code designed to interrupt, destroy or limit the functionality of the Website or any computer software or equipment;</li>
                            <li>collect or store other users' personal data; or</li>
                            <li>restrict or inhibit any other user from using the forum.</li>
                        </ul>
                   
                    <p>PM may contribute material to the forum in order to stimulate discussions and ensure the smooth running of the forum.</p> 
                    <p>The forum may also contain messages from special guests invited by PM. The opinions of such guests are personal and do not necessarily represent the views of PM. Additionally, messages will be submitted by users over whom PM has no control. It is possible that some users will breach the above-listed rules, but PM has no responsibility in that regard. You acknowledge that PM cannot guarantee the accuracy, integrity or quality of the messages posted on the forum and that PM cannot be held liable for their contents. You also acknowledge that you bear all risk associated with your use of the forum and should not rely on messages in making (or refraining from making) any specific investment or other decisions.</p>
                    
                    <h4>POSTING DEVELOPED MATERIALS</h4>
                    <p>You acknowledge, represent and warrant that all tangible and intangible materials, including texts, drawings, designs, photographs, videos, sketches, and all other materials and ideas, which you have posted on the Website ("Developed Materials") are your original work for which you have the related intellectual property rights or work to which you have obtained exhaustive intellectual property rights. You also acknowledge, represent and warrant that such Developed Materials do not and will not infringe the intellectual property rights of any third party.</p>
                    <p>You acknowledge and agree that by posting the Developed Materials on the Website you relinquish all rights to such Developed Materials and that such Developed Materials shall belong to PM (unless PM notifies you otherwise) and shall be subject to PM’s and its designees’ unrestricted use.</p>
                    <p>You acknowledge and agree that by posting Developed Materials on the Website, you automatically and irrevocably assign to PM all intellectual property rights therein, free and clear of any liens, claims or other encumbrances, to the fullest extent permitted by law, as from the date of creation of such Developed Materials and you waive any moral rights therein to the fullest extent permitted by law.</p>
                    <p>All Developed Materials may be used, altered, duplicated and/or reproduced in all forms and for all media, whether known or unknown, including all electronic media, worldwide, by PM and its designees. You hereby agree to any alteration, modification or combination of any Developed Materials. PM may combine any Developed Materials with any other elements, information, materials, works or designs to which PM has or may obtain rights from any source. You acknowledge and agree that you shall not receive any form of compensation for use of the Developed Materials in any manner and waive any right to be named as the author of such Developed Materials.</p>
                    
                    <h4>DISCLAIMER</h4>
                    <p>Your use of the Website is at your sole risk. The Website is provided on an "as is" and "as available" basis. PM and its affiliates expressly disclaim all warranties of any kind, whether express or implied, including but not limited to the implied warranties that the Website and Materials are non-infringing, accurate and appropriate; that access to the Website will be uninterrupted and error-free; that the Website will be secure; that the Website or the servers that make the Website available will be virus-free; or that information on the Website will be complete, accurate, appropriate and timely. If you download any Materials from the Website, you do so at your own discretion and risk. You will be solely responsible for any damage to your computer system or loss of data that results from the download of any such Materials. In addition to the above, you assume the entire cost of all necessary servicing, repair or correction.</p>
                    
                    <h4>LIMITATION OF LIABILITY</h4>
                    <p>To the fullest extent permitted under applicable law, you understand and agree that neither PM nor any of its respective affiliates, subsidiaries or third party content providers shall be liable for any direct, indirect, incidental, special, exemplary, consequential, punitive or any other damages relating to or resulting from your use or inability to use the Website or from any actions we take or fail to take as a result of your communication with us. These include damages for errors, omissions, inaccuracies, interruptions, defects, delays, computer viruses, your loss of profits, loss of data, unauthorized access to and alteration of your transmissions and data, and other tangible and intangible losses.</p>
                    
                    <h4>IDEMNIFICATION</h4>
                    <p>You agree to indemnify, defend and hold harmless PM and any of its affiliates or subsidiaries, and its officers, directors, employees, agents, licensors and suppliers from and against all losses, liabilities, expenses, damages and costs, including reasonable attorneys' fees and court costs, arising out of or resulting from any violation of these Terms and Conditions (including but not limited to negligent or wrongful conduct) by you or any other person accessing the Website using your username and password. If you cause a technical disruption of the Website or the systems transmitting the Website to you or others, you agree to be responsible for any and all losses, liabilities, expenses, damages and costs, including reasonable attorneys' fees and court costs, arising or resulting from that disruption.</p>
                    
                    <h4>JURISDICTION AND GOVERNING LAW</h4>
                    <p>The laws of the Philippines govern these Terms and Conditions and your use of the Website, and you irrevocably consent to the exclusive jurisdiction of the courts located in Tanauan City, Batangas, for any action to enforce these Terms and Conditions. The Website has been designed to comply with the laws of the Philippines.</p>
                    
                    <h4>HOW TO CONTACT US</h4>
                    <p>For any questions you may have about these Terms and Conditions you can contact our adult- restricted hotline at 895-9999 (Metro Manila) or 1-800-10-895-9999 (Provincial Toll-Free).</p>
                    
                    <p><br /></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>

    <?php /* OLD PRIVACY <div class="modal fade" id="prvcy" tabindex="-1" role="dialog">
         <div class="vertical-alignment-helper">
              <div class="modal-dialog vertical-align-center" role="document">
                    <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          </div>
                          <div class="modal-body">
                            <div class="prkspp-cntnt">
                                <h2>Terms and Conditions</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem voluptatibus sunt accusamus adipisci ab beatae accusantium blanditiis impedit, odit ducimus repellendus reprehenderit alias totam quaerat ex quod possimus amet, voluptas.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem voluptatibus sunt accusamus adipisci ab beatae accusantium blanditiis impedit, odit ducimus repellendus reprehenderit alias totam quaerat ex quod possimus amet, voluptas.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem voluptatibus sunt accusamus adipisci ab beatae accusantium blanditiis impedit, odit ducimus repellendus reprehenderit alias totam quaerat ex quod possimus amet, voluptas.</p>
                            </div>
                          </div>
                    </div>
              </div>
        </div>
    </div> */ ?>

    <div class="modal fade" id="prvcy" tabindex="-1" role="dialog">
         <div class="vertical-alignment-helper">
          <div class="modal-dialog vertical-align-center" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
               <h2>Privacy Statement and Consent</h2>
                <div class="prkspp-cntnt scrollbar-inner">            
                    <p>We at PMFTC Inc. ("PM") respect the privacy of our consumers. We are committed to appropriately safeguarding personal information that you provide to us when you visit and use this Website (the “Website”). This section includes a description of the types of personal information we collect via the Website and your consent on how we can use it. It also describes the measures we take to protect the security of the information and how you can contact us.</p>
                    
                    <h4>INFORMATION WE COLLECT AND HOW WE USE IT</h4>
                    <p>In order to be able to sign up to the Website you must be at least 18 years of age and you must provide us with personal information, such as smoking status, name, age, country of residence and e-mail address/telephone number, to enable us to verify your eligibility to be granted access to the Website. When you visit the Website, your browser automatically sends us an IP (Internet Protocol) address and certain other information (such as the type of browser you use). In addition, we may monitor and/or register your activities on the Website, including your submissions to the forum, if any. However, we do not use client-side cookies to store your personal and access information.</p>
                    <h4>CONSENT OF USE</h4>
                    <p>You acknowledge and give consent to PM to:</p>
                    <ul>
                        <li>store and use your personal information mentioned under the previous section to allow PM to verify your age and compliance with PMI internal policies, and/or contact you;</li>
                        <li>provide you with, using any means (mail, email, SMS, etc.), communications related to tobacco products, including but not limited to information and material on brand launches, packaging changes, events, marketing activities and/or the regulation of tobacco products, to the extent permitted under applicable laws;</li>
                        <li>to obtain and analyze publicly available personal information about you from online third party social media sites, in order to communicate with you in a more personal and effective manner;</li>
                        <li>process your personal information in countries other than the country in which you live (for example, locating the database in one country and accessing the database remotely from another); and</li>
                        <li>disclose your personal information to PM’s service providers for the above purposes, to a PM affiliate to do the above for its own purposes and (if required by law) to authorities. PM may not disclose your personal information to third parties for other purposes.</li>
                    </ul>
                    
                    <h4>HOW WE PROTECT PERSONAL INFORMATION</h4>
                    <p>The security of personal information is a high priority for us. We maintain appropriate administrative, technical and physical safeguards designed to protect against unauthorized disclosure, use, alteration and destruction of the personal information that is provided to us when you visit and use the Website. We use secure socket layer (SSL) and digital certificate encryptions and technologies to keep the transmission of your personal information secure.</p>
                    
                    <h4>HOW TO CONTACT US</h4>
                    <p>For any questions you may have about this Privacy Statement you can contact our adult- restricted hotline at 895-9999 (Metro Manila) or 1-800-10-895-9999 (Provincial Toll-Free).</p>
                    
                    <h4>HOW TO UNSUBSCRIBE FROM OUR DATABASE</h4>
                    <p>PM is committed to respecting and protecting your privacy. Unless otherwise provided, none of the information you provide will be sold or transferred to non-affiliated parties of PM without your consent. If you wish to withdraw your name from our database, please call our adult-restricted hotline 895-9999 (Metro Manila) or 1-800-10-895-9999 (Provincial Toll-Free) or text MLIST <FULL NAME>, <DATE OF BIRTH IN MM/DD/YYY>, <MESSAGE> to 0908-895-9999 (For example: MLIST JUAN DELA CRUZ, 08/12/1982, HOW DO I GET OFF THE LIST?) or email us at info@marlboro.ph</p>

                    <p><br /></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>

    <div class="modal fade" id="play-video" tabindex="-1" role="dialog">
         <div class="vertical-alignment-helper">
              <div class="modal-dialog vertical-align-center" role="document">
                <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      </div>
                      <div class="modal-body">
                            <div class="prkspp-cntnt scrollbar-inner">
                                <video controls id="teaser-video" style="width: 100%">
                                    <source src="<?=BASE_URL?>assets_finalist/videos/trailer-new.mp4"  type="video/mp4">
                                </video>
                            </div>
                      </div>
                </div>
              </div>
        </div>
     </div>

     <div class="modal fade" id="contact" tabindex="-1" role="dialog">
         <div class="vertical-alignment-helper">
          <div class="modal-dialog vertical-align-center" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
                <div class="prkspp-cntnt scrollbar-inner">
                    <h2>YOU CAN CONTACT US AT OUR ADULT-RESTRICTED HOTLINE:</h2>
                    <p>895-9999 (Metro Manila) or 1-800-10-895-9999 (Provincial Toll-Free), or text MLIST <full name>, <date of birth in mm/dd/yyyy>, <message> to 0908-895-9999.</p>
                    <p>Standard SMS rates apply.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>

    </div>

        <script type="text/javascript">
            window.BASE_URL = "<?php echo BASE_URL ?>";
            window.PROD_MODE = <?php echo (int) PROD_MODE ?>;
            window.FD = {
                FDID : <?php echo isset($finalist['finalist_id']) ? (int) $finalist['finalist_id'] : 0 ?>,
                UDID : <?php echo (int) $this->session->userdata('user_id') ?>,
                GALLERY : <?php echo isset($gallery) ? $gallery : 0 ?>,
                OFFSET : 0,
                PROGRESS_COUNT : 0
            }
            var COUNT = FD.GALLERY.length;
            var MAX = <?php echo isset($count) ? (int) $count : (int) 0 ?>

            <?php if(PROD_MODE): ?>
                $(document).bind("contextmenu", function(){ return false; });
            <?php endif; ?>
        </script>


        <script type="text/javascript" src="<?php echo BASE_URL ?>assets_finalist/vendors/jquery/jquery.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL ?>assets_finalist/vendors/handlebars-v3.0.3.js"></script>

        <script src="<?php echo BASE_URL ?>assets_finalist/js/main.min.js"></script>
        <script src="<?php echo BASE_URL ?>assets_finalist/js/backend.js?bmw=<?php echo rand() ?>"></script>

        <script type="text/javascript" src="<?php echo BASE_URL ?>assets_finalist/vendors/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL ?>assets_finalist/vendors/jscrollbar/jquery.scrollbar.min.js"></script>

        <script type="text/javascript" src="<?php echo BASE_URL ?>assets_finalist/vendors/owl-carousel/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL ?>scripts/lytebox.2.3.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL ?>scripts/jquery.jscrollpane.min.js"></script>

        <script>

        </script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.scrollbar-inner').scrollbar();
                getNotifications();

                $('#play-v').click(function(){
                    var video = document.getElementById('teaser-video');
                    $('#play-video').modal('show');
                    $('#play-video').on('shown.bs.modal', function() {
                        $('#teaser-video')[0].play();
                    });
                    $('#play-video').on('hidden.bs.modal', function() {
                        $('#teaser-video')[0].pause();
                    });
                })
          });
        </script>

        <script type="text/javascript">
            setTimeout(function() { window.location.href = "<?=site_url()?>welcome/logout"; }, 6000 * 200);
        </script>
     </div>
    </body>

    </html>