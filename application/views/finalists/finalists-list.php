	<div class="upperBanner clearfix btm" style="background-image: url(<?php echo BASE_URL ?>assets_finalist/img/bg/finalist_bg.jpg)">
        <div class="cntnt">
            <h1 class="h1f">FINALISTS</h1><span class="logo-marl"></span>
            <?php /*<div class="clearfix"></div>
             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p> */ ?>
        </div>
         <div class="cntnt-right">
            <a href="#" id="play-v" class="ply-btn"></a>
        </div>
	</div>

	<div class="container section-finalist clearfix">

		<ul class="finalist-profile clearfix">

			<?php if($finalists): $i = 0; ?>
				<?php foreach($finalists as $k => $v): ?>
					<li class="item">
						<?php /* <div class="content" style="background-image:url(<?php echo BASE_URL . unserialize($v['photos'])['coverphoto'][0] ?>)"> */ ?>
						<div class="content" style="background-image:url(<?php echo BASE_URL . $v['primary_photo'] ?>)">
					<?php /* $v = 'assets_finalist/img/bg/samplelist2.jpg'; if($i % 2 === 0 ) { ?>
						<div class="content" style="background-image:url(assets_finalist/img/bg/samplelist.jpg)">
					<?php } else { ?>
						<div class="content" style="background-image:url(<?=$v?>)">
					<?php } */ ?>

					<div class="overlay">
						<span class="name"><?php echo $v['name'] ?></span>
						<span class="location"><?php echo strtoupper($v['location']) ?></span>
						<span class="description"><?php echo character_limiter($v['short_description'], 70) ?></span>
						<a href="<?php echo BASE_URL .'finalists/'. $v['finalist_id'] .'/'. toSlug($v['name']) ?>" class="btn-follow">FOLLOW THEIR JOURNEY</a>
					</div>

					<!-- content -->
					</div>

				<div class="bottom-panel">
					<?php /* <span class="points-wrap">
						<span class="val">1300</span>&nbsp;
						<span class="pt">POINTS</span>
					</span> */ ?>

					<span class="like-wrap">
						<span class="val"><?php echo $v['likes'] > 0 ? $v['likes'] : 0 ?></span>&nbsp;
						<span class="pt like">LIKES</span>
					</span>
				</div>

				</li>
				<?php $i++; endforeach; ?>
			<?php endif; ?>

			<?php /* for ($item = 0; $item < 6; $item++) {?>
				<li class="item">
					<?php $v = 'assets_finalist/img/bg/samplelist2.jpg'; if($item % 2 === 0 ) { ?>
						<div class="content" style="background-image:url(assets_finalist/img/bg/samplelist.jpg)">
					<?php } else { ?>
						<div class="content" style="background-image:url(<?=$v?>)">
					<?php } ?>

					<div class="overlay">
						<span class="name">TEAM KIM GUZMAN</span>
						<span class="location">QUEZON CITY</span>
						<span class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</span>
						<a href="<?php echo BASE_URL ?>finalists/details" class="btn-follow">FOLLOW THEIR JOURNEY</a>
					</div>

					<!-- content -->
					</div>

				<div class="bottom-panel">
					<span class="points-wrap">
						<span class="val">1300</span>&nbsp;
						<span class="pt">POINTS</span>
					</span>

					<span class="like-wrap">
						<span class="val">80</span>&nbsp;
						<span class="pt like">LIKES</span>
					</span>
				</div>

				</li>
			<?php } */?>

		</ul>
	</div>