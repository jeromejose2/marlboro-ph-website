	<div class="upperBanner clearfix btm" style="background-image: url('<?php echo BASE_URL ?>assets_finalist/img/bg/finalist_bg.jpg')">
	    <div class="cntnt">
	        <h1 class="h1f">FINALISTS</h1><span class="logo-marl"></span>
	        <div class="clearfix"></div>
	        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> -->
	    </div>

	    <div class="cntnt-right">
        	<?php /* <a href="#" data-toggle="modal" data-target="#play-video" class="ply-btn"></a> */ ?>
        	<a href="#" id="play-v" class="ply-btn"></a>
        </div>
	</div>

	<div class="container section-finalist-inner clearfix">

		<div class="inside-banner" style="background-image: url(<?php echo BASE_URL . $finalist['cover_photo'] ?>)">

			<a href="<?php echo BASE_URL ?>finalists" class="txtbtn"><i class="fa fa-chevron-left"></i><span>BACK TO FINALISTS</span></a>
			<div class="overlay">
				<div class="profile">
					<span class="name"><?php echo $finalist['name'] ?></span>
					<span class="location"><?php echo strtoupper($finalist['location']) ?></span>
					
					<?php /* <span class="points-wrap">
						<span class="value">9999</span><span class="pt">POINTS</span>
					</span> */ ?>

					<span class="likes-wrap">
						<span class="value" id="likes-value"><?php echo (int) $finalist['likes'] ?></span><span class="like">LIKES</span>
					</span>

				</div>
			</div>
		</div>
		
		<div class="content clearfix">

			<div class="top-sub-nav">
				<?php /* <a href="javascript:;" class="rbtn plege-pt">PLEDGE POINT</a> */ ?>
				<?php /* <a data-toggle="modal" data-target="#pledge-point" class="rbtn plege-pt">PLEDGE POINT</a> */ ?>
				<a data-toggle="modal" data-target="#like-this-team" class="rbtn like-team like-this-team like-this-team-btn" style="<?php echo isset($liked) && $liked ? 'background-color: #AAAAAA' : '' ?>"><?php echo isset($liked) && $liked ? 'UNLIKE THIS TEAM' : 'LIKE THIS TEAM' ?></a>
			</div>

			<div class="pane1 description">
				<?php echo $finalist['write_ups'] ?>
				<?php /* <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
				<br/><br/>
				Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
				<br/><br/>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
				<br/><br/>
				Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
				<br/><br/>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
				<br/><br/>
				Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p> */ ?>
			</div>

			<div class="pane2 image-with-nav">
				<div class="img-wrap">
					<div class="imgholder" style="background-image: url(<?php echo BASE_URL . unserialize($finalist['photos'])['coverphoto'][0] ?>)">
					</div>
				</div>

				<div class="nav-wrap">
					<a data-toggle="modal" data-target="#view-gallery" class="rbtn view-gal">VIEW GALLERY</a>
					
					<?php /* <a data-toggle="modal" data-target="#like-this-team" class="rbtn like-team">LIKE THIS TEAM</a> */ ?>
					<a class="rbtn like-team like-this-team like-this-team-btn" style="<?php echo isset($liked) && $liked ? 'background-color: #AAAAAA' : '' ?>"><?php echo isset($liked) && $liked ? 'UNLIKE THIS TEAM' : 'LIKE THIS TEAM' ?></a>

					<?php /* <a data-toggle="modal" data-target="#pledge-point" class="rbtn plege-pt">PLEDGE POINT</a> */ ?>

					<!-- <a href="javascript:;" class="rbtn plege-pt">PLEDGE POINT</a> -->
					<!-- <a href="javascript:;" class="rbtn like-team">LIKE THIS TEAM</a> -->
				</div>
			</div>	

		<!--content end-->
		</div>

	</div>

<!--modal View Gallery-->
<div class="modal fade" id="view-gallery" tabindex="-1" role="dialog">
    
    <div class="vertical-alignment-helper">
	    <div class="modal-dialog vertical-align-center" role="document">

	        <div class="modal-content">
	            <div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      		</div>

	            <div class="modal-body clearfix">

                
                    <div class="row">
                        <div class="col-md-8">
                            <div class="gallery-info clearfix">
                                <span class="title">GALLERY</span>
                                <?php /* <div class="description">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit am voptatem accusantium doloremque laudantium, totam.</p>
                                </div> */ ?>
                            </div>
                            <div class="prev_gllry">
                                <img name="preview" src="<?php echo BASE_URL . unserialize($finalist['photos'])['url'][0] ?>" alt="" style="opacity: 0;">
                            </div>

                            <!-- NEW -->
                            <div class="thmb_gllry">
                                <ul class="thumb-listing clearfix">
                                	<?php if(unserialize($finalist['photos'])): ?>
                                		<?php foreach(unserialize($finalist['photos']) as $k => $v): ?>
                                			<?php foreach($v as $key => $val): ?>
                                				<li class="thumb" style="background-image:url(<?php echo BASE_URL . $val ?>); background-repeat: no-repeat; background-size: cover; background-position: center">
                                					<a href="javascript:;"><img name="img1" src="<?php echo BASE_URL . $val ?>"></a>
                                				</li>
                                			<?php endforeach; ?>
                                		<?php endforeach; ?>
                                	<?php endif; ?>
                                </ul>
                                <div class="thumb-nav clearfix">
                                    <a href="javascript:;" class="prev-cont" onclick="paginate('prev')">&lt; PREV</a>
                                    <a href="javascript:;" class="next-cont <?php echo $image_count > 4 ? 'active' : '' ?>" onclick="paginate('next')">NEXT &gt;</a>
                                </div>
                            </div>
                            <!-- NEW -->                           
                            
                        </div>
                        <div class="col-md-4 glry_cmmt">
                            <div class="row">
                                <div class="cmt_pic">
                                    <img src="<?php echo BASE_URL ?>assets_finalist/img/profile-pics/prof-place_03.jpg" alt="" />
                                </div>
                                <div class="cmt_dsc">
                                    <h3><?php echo $this->session->userdata('user')['nick_name'] ?: $this->session->userdata('user')['first_name']; ?></h3>
                                    <input type="text" class="cmmntbx comment-box-main" placeholder="Comment"/>                                    
                                    <button class="encm comment-main-btn" onclick="enterComment()">Enter Comment</button>
                                </div>
                                    
                            </div>
				<div class="scl scrollbar-inner clearfix">

				<?php if($comments): ?>
					<?php foreach($comments as $ck => $cv): ?>
					<div class="row pstd">
                                <div class="cmt_pic">
                                    <img src="<?php echo BASE_URL ?>assets_finalist/img/profile-pics/prof-place_03.jpg" alt="" />
                                </div>
                                <div class="cmt_dsc">
                                    <h3><?php echo $cv['nick_name'] ?></h3>
                                    <p><?php echo $cv['comment'] ?></p>
                                    <button class="rply rply-btn-<?php echo $ck ?>" onclick="$('.reply-<?php echo $ck ?>').show(); $(this).hide()">reply</button>
                                    <div class="rplyCnt reply-<?php echo $ck ?>" style="display: none">
                                        <input type="text" class="cmmntbx comment-box-<?php echo $ck ?>" placeholder="Comment"/>
                                        <button class="encm reply-btn-<?php echo $ck ?>" onclick="enterReplyComment(<?php echo $ck ?>, <?php echo $cv['finalist_comment_id'] ?>)">enter comment</button>
                                    </div>
                                </div>
					<?php if($cv['replies']): foreach($cv['replies'] as $rk => $rv): ?>
						<div class="pstdd">
	                                    <div class="cmt_pic">
	                                        <img src="<?php echo BASE_URL ?>assets_finalist/img/profile-pics/prof-place_03.jpg" alt="" />
	                                    </div>
	                                    <div class="cmt_dsc">
	                                        <h3><?php echo $rv['nick_name'] ?></h3>
	                                        <p><?php echo $rv['comment'] ?></p>
	                                    </div>
	                                </div>
                          	<?php endforeach; endif; ?>
                            </div>
				<?php endforeach; ?>
                        <!-- </div> -->
                  	<?php endif; ?>
                        </div>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="row ft-dsclmr">
                        <small>Comments are subject to moderation. PM reserves the right to reject or remove any comment as per the <a data-toggle="modal" data-target="#terms">Terms and Conditions</a>.</small>
                    </div>
              </div>
                    
	            </div>
	        </div>
		
		<!--alignment-->
	    </div>
	</div>
<!--modal View Gallery end-->

<!--modal Thanks-4-like -->
<div class="modal fade" id="like-this-team" tabindex="-1" role="dialog">
	 <div class="vertical-alignment-helper">
	    <div class="modal-dialog vertical-align-center" role ="document">
	    	
			 <div class="modal-content">

	            <div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      		</div>
				
				<div class="modal-body clearfix">
					<div id="like-preloader" style="text-align: center">
						<img src="<?php echo BASE_URL ?>images/preloader.gif">
					</div>
					<div class="content" id="like-content">
						<div class="picture-wrap">
							<div class="circle" style="background-image:url(<?php echo BASE_URL . $finalist['primary_photo'] ?>);">
								<div class="check"><i class="fa fa-check"></i></div>
							</div>
						</div>
						<span class="thanks-greet">THANK YOU FOR LIKING!</span>
						<?php /* <p class="info">Lorem ipsum dolor sit amet duis, aute irure dolor velit es.</p> */ ?>
					</div>
				</div>
	      	</div>
	    </div>
	 </div>
</div>
<!--modal Thanks-4-like -->

<!-- Comment thankyou -->
<div class="modal fade" id="pledge-point-thankyou" tabindex="-1" role="dialog">
	<div class="vertical-alignment-helper">
		<div class="modal-dialog vertical-align-center" role ="document">
			
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      		</div>
			
				<div class="modal-body clearfix">
					<div class="content">
						<span class="title">THANK YOU FOR YOUR COMMENT</span>
						<div class="point-shared"></div>

						<div class="info">
							<span class="line1">Comments are subject to moderation.</span>
							<span class="line2">PM reserves the right to reject or remove any comment as per the Terms and Conditions.</span>
						</div>

						<div class="btn-wrap">
	      					<a href="javascript:;" class="btn-black expanded" data-dismiss="modal">CLOSE WINDOW</a>
	      				</div>
					</div>
				</div>
	      	</div>
		</div>
	</div>
</div>

<script id="gallery" type="text/x-handlebars-template">
	{{#if items}}
		{{#each items}}
			{{#if type}}
				<li class="thumb" style="background-image:url({{url}}{{file}}); background-repeat: no-repeat; background-size: cover; background-position: center; cursor: pointer" onclick="qwert('{{file}}', 'image')">
					<a href="javascript:;"><img name="img1" src="{{url}}{{file}}" alt=""/></a>
				</li>
			{{else}}
				<li class="thumb" style="background-image:url({{default}}); background-repeat: no-repeat; background-size: 20%; cursor: pointer; background-color: black; background-position: center" onclick="qwert('{{file}}', 'video')">
					<a href="javascript:;"><img name="img1" src="{{default}}" alt=""/></a>
				</li>
			{{/if}}
		{{/each}}
	{{/if}}
</script>



<style>
	.input-error-outline {
		outline-color: #de001f !important;
	}
	.gallery-vid-style {
		border-radius: 100% !important;
		opacity: 1 !important;
		padding: 6px 25px 10px 16px !important;
		box-shadow: 1px 3px 4px rgba(0, 0, 0, 0.81) !important;
		background-color: #BE131B !important;
		margin: 17px 38px !important;
	}
</style>