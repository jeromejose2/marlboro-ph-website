<!doctype html>
<html class="no-js" lang="">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="The first with 2 capsules. Crush the purple for a burst of flavor. Crush the creen capsule for a boost in menthol taste. Crush both capsules for a fusion of flavor and menthol.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Twitter Card data -->
    <meta name="twitter:card" value="The first with 2 capsules. Crush the purple for a burst of flavor. Crush the creen capsule for a boost in menthol taste. Crush both capsules for a fusion of flavor and menthol.">

    <!-- Open Graph data -->
    <meta property="og:title" content="Marlboro Ruby Burst" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="http://www.marlboro.ph/rubyburst" />
    <meta property="og:image" content="/img/fb_thumbnail.png" />
    <meta property="og:description" content="The first with 2 capsules. Crush the purple for a burst of flavor. Crush the creen capsule for a boost in menthol taste. Crush both capsules for a fusion of flavor and menthol." />

    <title>Marlboro</title><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

    

    <!--  OLD MARLBORO  -->
      <noscript>
      <meta http-equiv="refresh" content="0;http://marlboro-stage2.yellowhub.com/spice/noscript">
    </noscript>
    <link rel="stylesheet" href="<?= base_url() ?>/styles/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/plugins/jquery-ui/css/smoothness/jquery-ui-1.10.0.custom.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/styles/lytebox.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/styles/mainstyle.css"> -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>/images/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>/images/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>/images/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>/images/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>/images/favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>/images/favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>/images/favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="icon" type="image/png" href="<?= base_url() ?>/images/favicon/favicon-196x196.png" sizes="196x196">
    <link rel="icon" type="image/png" href="<?= base_url() ?>/images/favicon/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="<?= base_url() ?>/images/favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="<?= base_url() ?>/images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?= base_url() ?>/images/favicon/favicon-16x16.png" sizes="16x16">
    <meta name="msapplication-TileColor" content="#ab0702">
    <meta name="msapplication-TileImage" content="<?= base_url() ?>/images/favicon/mstile-144x144.png">
    <script type="text/javascript" src="<?= base_url() ?>/scripts/jquery.min.js"></script>
    <script>
      var MINIMUM_AGE = "18";
      var BASE_URL = "http://marlboro.ph/";
      var PROD_MODE = 0;
          $(document).bind("contextmenu", function() {
        return false;
      });
    </script>
    <!-- EOF OLD MARLBORO -->
    <link rel="stylesheet" href="<?= base_url()."/firm-filter/css/main_firmfilter.css" ?>">
    <link rel="stylesheet" href="<?= base_url().'/firm-filter/marl-includes/vendor.css' ?>">
    <link rel="stylesheet" href="<?= base_url() ?>firm-filter/css/overrides_firmfilter.css" />
  </head>

  <body>
    <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <header id="header" class="text-uppercase login-mode">
      <div class="container">
        <nav id="main-nav" class="clearfix">
          <ul class="list-unstyled list-inline text-center">
            <li class="active"><a href="#">Login</a></li>
            <li><a href="<?= base_url().'user/register' ?>">Register</a></li>
          </ul>
        </nav>
      </div>
    </header>


<section class="login-layout main">
  <div class="wrapper registration ">
    <div class="container">
      <div class="login-holder-outer">
        <div class="login-holder">
          <form autocomplete="off" class="content" onsubmit="return validate(this)" method="POST" action="<?= base_url() ?>user/auth">
            <h2>ALREADY REGISTERED?</h2>
            <h3>PLEASE PROVIDE YOUR LOGIN DETAILS BELOW</h3>
            <div id="error"><?= $invalid_message ? '<span>'.$invalid_message.'</span>' : '' ?></div>
            <div class="field-container">
              <label for="username">Username</label>
              <input type="text" name="username"class="req">
            </div>
            <div class="field-container">
              <label for="username">Password</label>
              <input type="password" name="password" class="req">
            </div>
            <br>
            <div class="copy">
              <p><a href="<?= base_url() ?>reset_password">Forgot your password?</a></p>

              Registered but haven't submitted your documents yet? <a href="<?= base_url() ?>submit_giid">Click here</a><p></p>
            </div>
            <div class="submit-container">
              <button type="submit" class="button btn btn-std-transparent">LOGIN</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

  <footer id="footer" class="text-uppercase login-mode">
    <section class="container-footer-actions">
      <div class="container-age-warning">
        <div class="container text-center">
          <p>This website is for adult smokers 18 years or older residing in the Philippines. Access to this website is subject to age verification.</p>
        </div>
      </div>
      <div class="container-links-copyright">
        <div class="container">
          <div class="container-footer-links col-md-6 col-sm-7 col-xs-12">
            <nav>
              <ul class="list-unstyled list-inline">
                <li><a id="trigger-terms" href="#">Terms and Conditions</a></li>
                <li><a id="trigger-privacy" href="#">Privacy Policy</a></li>
                <li><a id="trigger-contact" href="#">Contact Us</a></li>
                <li><a target="_blank" href="http://www.pmi.com/eng/our_products/health_effects_of_smoking/pages/health_effects_of_smoking.aspx">Smoking and Health</a></li>
              </ul>
            </nav>
          </div>
          <div class="container-copyright col-md-4 col-sm-5 col-xs-12">
            <p>Copyright © 2016 pmftc inc. all rights reserved</p>
          </div>
        </div>
      </div>
    </section>
    <section class="container-govt-warning text-center">
      <p>Government warning: Cigarette smoking <span>is dangerous to your health</span></p>
    </section>
  </footer>

  <div id="popup-terms" class="std-popup mfp-hide">
    <div class="pop-container">
      <div class="pop-header">
        <h3>Terms &amp; Conditions</h3>
      </div>
      <div class="pop-contents">
      <p>This website (the "<b>Website</b>") is owned and operated by or on behalf of PMFTC Inc. ("<b>PM</b>") based
        in the Philippines for verified adult smokers who have been provided with a personal username
        and password of access. In the course of logging onto the Website, you will have been asked
        to read and agree to these Terms and Conditions. Therefore, by logging onto the Website, you
        will be assumed to have read and agreed to these Terms and Conditions. </p><h4>Website Users</h4><p>The Website is intended for verified adult smokers who have been provided with a username
        and password for access. Your username and password are strictly personal and must not be
        shared with anyone else. </p><h4>Use of the Website and Materials</h4><p>The Website and all its contents (including but not limited to messages posted on the forum,
        if any, software, files, graphics, data, images and other material) ("<b>Material</b>") are the exclusive
        property of PM, its affiliates, and/or its licensors and are protected by the intellectual property
        laws of the Philippines as well as in other countries. The Website and the Material are provided
        solely for your personal use. Any other use of the Website or the Material, including but not
        limited to:</p><ul>
          <li>using the Website or Materials for business purposes; </li>
          <li>distributing, sharing, copying, reproducing or publishing the Material; or</li>
          <li> modifying, distributing, transmitting, performing, broadcasting, reproducing, publishing
            licensing, or reverse engineering parts or all of the Website,
            without the prior written permission of PM, is expressly prohibited.</li>
        </ul><h4>Forum</h4><p>To the extent the Website includes a forum that allows interaction between users, you are
        hereby made aware (i) that PM, its affiliates and/or representatives acting on their behalf
        regularly monitors the information posted on the forum by users, and (ii) that PM reserves the
        right (which may be exercised at their sole discretion without notice) to delete, remove, move
        or edit comments or messages you have posted. PM and its affiliates also reserve themselves
        the right to terminate, or have terminated, your access to and use of the Website and/or forum
        at their sole discretion. You hereby waive any rights including any rights that you may have in
        regard to messages you have posted on this forum.</p><p>Without limiting the right of PM to reject or remove any comment or message posted in the
        Website, any comment or message you post or any submission you make must not: </p><ul>
          <li>contain contact information (i.e., phone numbers, etc.);</li>
          <li>contain swear words or other language likely to offend;</li>
          <li>be racist, sexist, sexually explicit, abusive, or otherwise objectionable;</li>
          <li>be derogatory to other users or other brands;</li>
          <li>break the law, or condones or encourages unlawful activity; </li>
          <li>refer to or contain a cigarette or the use of a cigarette;</li>
          <li>refer to or contain alcoholic products;</li>
          <li>refer to or contain a minor (i.e., a person less than eighteen (18) years old);</li>
          <li>refer to or contain a cartoon; or</li>
          <li>be otherwise unsuitable in any manner.</li>
        </ul><p>The comments made on the forum (or blog) shall focus on aspects unrelated to smoking
        and health. Smoking is dangerous and addictive. More information about this is
        available at: <a href="http://www.pmi.com/eng/our_products/health_effects_of_smoking/pages/&#10;&#9;&#9;&#9;health_effects_of_smoking.aspx" target="_blank">http://www.pmi.com/eng/our_products/health_effects_of_smoking/pages/
        health_effects_of_smoking.aspx</a> </p><p>Any comments made about smoking and health will be removed from the site.</p><p>You acknowledge that you are solely responsible for any information, data, personal views
        or beliefs, or any material whatsoever that you submit for display on the forum, including the
        accuracy, legality, reliability and appropriateness of all such information, data, personal views or
        material. In posting messages on the forum, you must not:</p><ul>
          <li>post, link or otherwise publish any messages containing any form of advertising or
            promotion for products or services, chain messages or “spam”;</li>

        <li>post, link to or otherwise publish any messages with recommendations to buy or refrain
          from buying a particular security or which contain confidential information of another
          party or which otherwise have the purpose of affecting the price or value of any traded security;</li>

        <li>post, link to or otherwise publish any messages that are unlawful, threatening,
          discriminatory, abusive, libelous, indecent, infringe copyright or other rights of third
          parties or which contain any form of illegal content;</li>

        <li>disguise the origin of any message;</li>

        <li>impersonate any person or entity (including forum users or hosts) or misrepresent any
          affiliation with any person or entity;</li>

        <li>post or otherwise publish any messages unrelated to the forum;</li>

        <li>post or transmit any messages that contain software viruses, files or code designed to
        interrupt, destroy or limit the functionality of the Website or any computer software or equipment;</li>

        <li>collect or store other users' personal data; or</li>

        <li>restrict or inhibit any other user from using the forum.</li>
        </ul><p>PM may contribute material to the forum in order to stimulate discussions and ensure the smooth running of the forum. </p><p>The forum may also contain messages from special guests invited by PM. The opinions of such
        guests are personal and do not necessarily represent the views of PM. Additionally, messages
        will be submitted by users over whom PM has no control. It is possible that some users will
        breach the above-listed rules, but PM has no responsibility in that regard. You acknowledge
        that PM cannot guarantee the accuracy, integrity or quality of the messages posted on the
        forum and that PM cannot be held liable for their contents. You also acknowledge that you bear
        all risk associated with your use of the forum and should not rely on messages in making (or
        refraining from making) any specific investment or other decisions. </p><h4>Posting Developed Materials</h4><p>You acknowledge, represent and warrant that all tangible and intangible materials, including
        texts, drawings, designs, photographs, videos, sketches, and all other materials and ideas,
        which you have posted on the Website ("<b>Developed Materials</b>") are your original work for which
        you have the related intellectual property rights or work to which you have obtained exhaustive
        intellectual property rights. You also acknowledge, represent and warrant that such Developed
        Materials do not and will not infringe the intellectual property rights of any third party.</p><p>  You acknowledge and agree that by posting the Developed Materials on the Website you
        relinquish all rights to such Developed Materials and that such Developed Materials shall
        belong to PM (unless PM notifies you otherwise) and shall be subject to PM’s and its designees’
        unrestricted use.</p><p>You acknowledge and agree that by posting Developed Materials on the Website, you
        automatically and irrevocably assign to PM all intellectual property rights therein, free and clear
        of any liens, claims or other encumbrances, to the fullest extent permitted by law, as from the
        date of creation of such Developed Materials and you waive any moral rights therein to the
        fullest extent permitted by law. </p><p>All Developed Materials may be used, altered, duplicated and/or reproduced in all forms and
        for all media, whether known or unknown, including all electronic media, worldwide, by PM
        and its designees. You hereby agree to any alteration, modification or combination of any
        Developed Materials. PM may combine any Developed Materials with any other elements,
        information, materials, works or designs to which PM has or may obtain rights from any source.
        You acknowledge and agree that you shall not receive any form of compensation for use of
        the Developed Materials in any manner and waive any right to be named as the author of such
        Developed Materials.</p><h4>Disclaimer</h4><p>Your use of the Website is at your sole risk. The Website is provided on an "as is" and "as
        available" basis. PM and its affiliates expressly disclaim all warranties of any kind, whether
        express or implied, including but not limited to the implied warranties that the Website and
        Materials are non-infringing, accurate and appropriate; that access to the Website will be
        uninterrupted and error-free; that the Website will be secure; that the Website or the servers
        that make the Website available will be virus-free; or that information on the Website will be
        complete, accurate, appropriate and timely. If you download any Materials from the Website,
        you do so at your own discretion and risk. You will be solely responsible for any damage to
        your computer system or loss of data that results from the download of any such Materials. In
        addition to the above, you assume the entire cost of all necessary servicing, repair or correction.</p><h4>Limitation of Liability</h4><p>To the fullest extent permitted under applicable law, you understand and agree that neither PM
        nor any of its respective affiliates, subsidiaries or third party content providers shall be liable for
        any direct, indirect, incidental, special, exemplary, consequential, punitive or any other damages
        relating to or resulting from your use or inability to use the Website or from any actions we take
        or fail to take as a result of your communication with us. These include damages for errors,
        omissions, inaccuracies, interruptions, defects, delays, computer viruses, your loss of profits,
        loss of data, unauthorized access to and alteration of your transmissions and data, and other
        tangible and intangible losses. </p><h4>Indemnification</h4><p>You agree to indemnify, defend and hold harmless PM and any of its affiliates or subsidiaries,
        and its officers, directors, employees, agents, licensors and suppliers from and against all
        losses, liabilities, expenses, damages and costs, including reasonable attorneys' fees and court
        costs, arising out of or resulting from any violation of these Terms and Conditions (including but
        not limited to negligent or wrongful conduct) by you or any other person accessing the Website
        using your username and password. If you cause a technical disruption of the Website or the
        systems transmitting the Website to you or others, you agree to be responsible for any and all
        losses, liabilities, expenses, damages and costs, including reasonable attorneys' fees and court
        costs, arising or resulting from that disruption.</p><h4>Jurisdiction and Governing Law</h4><p>The laws of the Philippines govern these Terms and Conditions and your use of the Website,
        and you irrevocably consent to the exclusive jurisdiction of the courts located in Tanauan
        City, Batangas, for any action to enforce these Terms and Conditions. The Website has been
        designed to comply with the laws of the Philippines. </p><h4>How to Contact Us </h4><p>For any questions you may have about these Terms and Conditions you can contact our adult-
        restricted hotline at 895-9999 (Metro Manila) or 1-800-10-895-9999 (Provincial Toll-Free).</p>
      </div>
    </div>
  </div>
  <div id="popup-privacy" class="std-popup mfp-hide">
    <div class="pop-container">
      <div class="pop-header">
        <h3>Privacy Statement &amp; Consent</h3>
      </div>
      <div class="pop-contents">
        <p>We at PMFTC Inc. ("<b>PM</b>") respect the privacy of our consumers. We are committed to
        appropriately safeguarding personal information that you provide to us when you visit and
        use this Website (the “Website”). This section includes a description of the types of personal
        information we collect via the Website and your consent on how we can use it. It also describes
        the measures we take to protect the security of the information and how you can contact us. </p><h4>Information We Collect and How We Use It</h4><p>In order to be able to sign up to the Website you must be at least 18 years of age and you
        must provide us with personal information, such as smoking status, name, age, country of
        residence and e-mail address/telephone number, to enable us to verify your eligibility to be
        granted access to the Website. When you visit the Website, your browser automatically sends
        us an IP (Internet Protocol) address and certain other information (such as the type of browser
        you use). In addition, we may monitor and/or register your activities on the Website, including
        your submissions to the forum, if any. However, we do not use client-side cookies to store your
        personal and access information.</p><h4>Consent of use </h4><p>You acknowledge and give consent to PM to:</p><ul>
          <li>store and use your personal information mentioned under the previous section to allow
          PM to verify your age and compliance with PMI internal policies, and/or contact you;</li>

          <li>provide you with, using any means (mail, email, SMS, etc.), communications related to
          tobacco products, including but not limited to information and material on brand
          launches, packaging changes, events, marketing activities and/or the regulation of
          tobacco products, to the extent permitted under applicable laws;</li>

          <li>to obtain and analyze publicly available personal information about you from online third
          party social media sites, in order to communicate with you in a more personal and
          effective manner;</li>

          <li>process your personal information in countries other than the country in which you live
          (for example, locating the database in one country and accessing the database remotely
          from another); and</li>

          <li>disclose your personal information to PM’s service providers for the above purposes, to
          a PM affiliate to do the above for its own purposes and (if required by law) to authorities.
          PM may not disclose your personal information to third parties for other purposes.</li>
        </ul><h4>How We Protect Personal Information</h4><p>The security of personal information is a high priority for us. We maintain appropriate
        administrative, technical and physical safeguards designed to protect against unauthorized
        disclosure, use, alteration and destruction of the personal information that is provided to us
        when you visit and use the Website. We use secure socket layer (SSL) and digital certificate
        encryptions and technologies to keep the transmission of your personal information secure.</p><h4>How to Contact Us </h4><p>For any questions you may have about this Privacy Statement you can contact our adult-
        restricted hotline at 895-9999 (Metro Manila) or 1-800-10-895-9999 (Provincial Toll-Free).</p><h4>How to Unsubscribe from our Database</h4><p>PM is committed to respecting and protecting your privacy. Unless otherwise provided, none of
        the information you provide will be sold or transferred to non-affiliated parties of PM without your
        consent. If you wish to withdraw your name from our database, please call our adult-restricted
        hotline 895-9999 (Metro Manila) or 1-800-10-895-9999 (Provincial Toll-Free) or text MLIST
        &lt;FULL NAME&gt;, &lt;DATE OF BIRTH IN MM/DD/YYY&gt;, &lt;MESSAGE&gt; to 0908-895-9999 (For
        example: MLIST JUAN DELA CRUZ, 08/12/1982, HOW DO I GET OFF THE LIST?) or email us at info@marlboro.ph</p>
      </div>
    </div>
  </div>
  <div id="popup-contact"  class="std-popup mfp-hide">
    <div class="pop-container">
      <div class="pop-header">
        <h3>You can contact us at our adult-restricted hotline:</h3>
      </div>
      <div class="pop-contents">

        <p>895-9999 (Metro Manila) or 1-800-10-895-9999 (Provincial Toll-Free), or text MLIST &lt;full name&gt;, &lt;date of birth in mm/dd/yyyy&gt;, &lt;message&gt; to 0908-895-9999.</p>
        <p>Standard SMS rates apply.</p>
      </div>
    </div>
  </div>

  <script type="text/javascript">
  function validate(form){
    var error = false;
    $('.req',form).each(function(){
      var el = $(this);
      var val = $.trim(el.val());
      var typ = el.attr('type');
      var name = el.attr('name');
      if( val == '' ){
        error = 'Please input your '+name;
        return false;
      }

      if( typ == 'checkbox'){
        if( !el.is(':checked') ){
          error = 'You need to tick both statements in order to log in';
          return false;
        }
      }
    });
    if( error ){
      $('#error').html('<span>'+error+'</span>');
      $('html, body').animate({scrollTop:0})
      return false;
    }
  }

  function validate_cp(form){
    var error = false;

    $('.req',form).each(function(){
      var el = $(this);
      var val = $.trim(el.val());
      var np = $('input[name=new_password]').val();
      var cp = $('input[name=confirm_password]').val();
      var firstname = '';
      var lastname = '';
      
      if( (np && cp) == '' ){
        if(np == '')
          error = 'Please input your new password';
        else if(cp == '')
          error = 'Please input the confirm password field';
        return false;
      }else if(np != cp){
        error = 'Password did not match';
        return false;
      }else if(np.length <= 7){
        error = 'Password should be at least 8 characters';
        return false;
      }

      if(!np.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/)){
        error = ' Your password must have any of the following: Uppercase, Numerical, or Special characters.';
        return false;
      }

      if(np.match(new RegExp(firstname+'|'+lastname+'|user|guest|admin|sys|test|pass|super', 'gi'))){
        error = 'Your password must not contain the following: "user", "guest", "admin", "sys", "test", "pass", "super", your first or last name.';
        return false;
      }
    });

    if( error ){
      $('#error').html('<span>'+error+'</span>');
      $('html, body').animate({scrollTop:0})
      return false;
    }
  }



</script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?= BASE_URL ?>plugins/jquery-ui/js/jquery-ui-1.10.0.custom.min.js"></script>
<script src="<?= BASE_URL ?>scripts/bootstrap.min.js"></script>
<script src="<?= BASE_URL ?>scripts/exec.js"></script>
<script src="<?= BASE_URL ?>scripts/owl.carousel.min.js"></script>
<script src="<?= BASE_URL ?>scripts/swfobject.js"></script>
<script src="<?= BASE_URL ?>swf/registration-webcam.js"></script>
<link type="text/css" href="<?= BASE_URL ?>styles/jquery.jscrollpane.css" rel="stylesheet" media="all" />
<script src="<?= BASE_URL ?>scripts/jquery.mousewheel.js"></script>
<script src="<?= BASE_URL ?>scripts/jquery.jscrollpane.min.js"></script>

  <script src="<?= base_url().'/scripts/lytebox.2.3.js' ?>"></script>
  <script src="<?= base_url().'/firm-filter/marl-includes/jquery.magnific-popup.min.js' ?>"></script>
  <script src="<?= base_url().'/firm-filter/marl-includes/jquery.mCustomScrollbar.js' ?>"></script>
  <script src="<?= base_url().'/firm-filter/marl-includes/jquery.mousewheel.min.js' ?>"></script>

  <script src="<?= base_url().'/firm-filter/js/main_firmfilter.js' ?>"></script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-42684485-7', 'marlboro.ph');
  ga('send', 'pageview');
</script>

<script type="text/javascript">
  $(function(){
    <?php if($this->session->userdata('response_message_error')){ ?>
      popup.open({title: "<span style='color:red'><center>ERROR:</center></span>", message:"<span style='font-family: 'Maybebold'; 25px;'><center><?php echo $this->session->userdata('response_message_error'); ?></center></span>"});

    <?php $this->session->set_userdata('response_message_error', ''); } ?>

    <?php if(isset($errorUrl)){ ?>
        popup.open({title: "<span style='color:red'><center>ERROR:</center></span>", message:"<span style='font-family: 'Maybebold'; 25px;'><center>AN ERROR OCCURED. PLEASE TRY AGAIN!</center></span>"});
    <?php }else if(isset($successUrl)){ ?> 
      popup.open({title: "<center>CONGRATULATIONS!</center>", message:"<span style='font-family: 'Maybebold'; 25px;'><center>You have successfully changed your password. You may now login.</center></span>"});
    <?php }else if(isset($invalidToken)){ ?> 
        popup.open({title: "<span style='color:red'><center>ERROR:</center></span>", message:"<span style='font-family: 'Maybebold'; 25px;'><center>INVALID TOKEN!</center></span>"});
    <?php }else if(isset($successForgot)){ ?> 
        popup.open({title: "<center>FORGOT PASSWORD</center>", message:"<span style='font-family: 'Maybebold'; 25px;'><center>We have sent you an email.</center></span>"});
    <?php } ?>
    });
</script>
</body>