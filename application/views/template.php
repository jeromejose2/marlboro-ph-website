<!DOCTYPE html>
<html lang="en">
<head>
	<title>Marlboro</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<base href="<?=BASE_URL?>">
	<noscript>
		<meta http-equiv="refresh" content="0;noscript">
	</noscript>
	<link rel="stylesheet" href="styles/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="styles/lytebox.css">
	<link type="text/css" href="styles/jquery.jscrollpane.css" rel="stylesheet" media="all" />
	<link rel="stylesheet" type="text/css" href="styles/mainstyle.css">
	<link rel="stylesheet" href="styles/owl.carousel.css">
	<link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-touch-icon-152x152.png">
	<link rel="icon" type="image/png" href="images/favicon/favicon-196x196.png" sizes="196x196">
	<link rel="icon" type="image/png" href="images/favicon/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="images/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="images/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="images/favicon/favicon-16x16.png" sizes="16x16">
	<meta name="msapplication-TileColor" content="#ab0702">
	<meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
	<script src="scripts/jquery.min.js"></script>
	<script src="scripts/jFormslider.js"></script>


	<script>
		window.BASE_URL = "<?= BASE_URL ?>";
		window.DEFAULT_IMAGE = "<?= DEFAULT_IMAGE ?>";
		window.PROD_MODE = <?= (int) PROD_MODE ?>;
		window.Origin = {
			MOVE_FWD : <?= MOVE_FWD ?>,
			MOVE_FWD_GALLERY : <?= MOVE_FWD_GALLERY ?>,
			MOVE_FWD_COMMENTS_PHOTOS : <?= MOVE_FWD_COMMENTS_PHOTOS ?>,
			MOVE_FWD_COMMENT_REPLIES_PHOTOS : <?= MOVE_FWD_COMMENT_REPLIES_PHOTOS ?>
		};
		<?php if (!DEV_MODE): ?>
		$(document).bind("contextmenu", function() {
			return false;
		});
		<?php endif ?>

	</script>
	<body>
	<header class="main">
		<div class="container">
			<?php /*<a class="logo"  href="<?= BASE_URL ?>"><span class="sr-only">MARLBORO LOGO</span></a>*/ ?>
			<a class="logo"  href="<?php echo BASE_URL ?>"><span class="sr-only">MARLBORO LOGO</span></a>
			<button class="navbar-toggle"  data-toggle="collapse" data-target="#nav-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="collapse navbar-collapse" id="nav-collapse">
				<ul class="main-nav nav navbar-nav navbar-right">
					<?php /* <li <?= $this->uri->segment(1) == '' ? 'class="active"' : '' ?>><a href="<?= BASE_URL ?>">HOME</a></li> */ ?>
					<li <?= $this->uri->segment(1) == 'about' ? 'class="active"' : '' ?>>
						<a href="<?= BASE_URL ?>about/product_info" class="dropdown-toggle" data-toggle="dropdown">ABOUT <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="<?= BASE_URL ?>about/dont-be-a-maybe">Don't be a maybe</a></li>
							<li><a href="<?= BASE_URL ?>about/brand_history">Brand History</a></li>
							<li><a href="<?= BASE_URL ?>about/product_info">Product Info</a></li>
						</ul>
					</li>
					<?php /*if(date('Ymd') >= 20140725) { ?>
					<li><a href="<?= BASE_URL ?>premium">PREMIUM BLACK</a></li>
					<?php } */?>
					<li <?= $this->uri->segment(1) == 'backstage_pass' ? 'class="active"' : $this->uri->segment(1) == 'events' ? 'class="active"' : $this->uri->segment(1) == 'backstage_videos' ? 'class="active"' : '' ?>>
						<a href="<?= BASE_URL ?>events" class="dropdown-toggle" data-toggle="dropdown">BACKSTAGE PASS <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="<?= BASE_URL ?>events/hot">WHAT'S HOT</a></li>
							<li><a href="<?= BASE_URL ?>events/upcoming">UPCOMING EVENTS</a></li>
							<li><a href="<?= BASE_URL ?>events/past">PAST EVENTS</a></li>
							<li><a href="<?= BASE_URL ?>events/videos">VIDEOS</a></li>
						</ul>
					</li>
					<li <?= $this->uri->segment(1) == 'crossover' ? 'class="active"' : $this->uri->segment(1) == 'crossover' ? 'class="active"' : $this->uri->segment(1) == 'crossover' ? 'class="active"' : '' ?>>
						<a href="<?= BASE_URL ?>events" class="dropdown-toggle" data-toggle="dropdown">CROSS|OVER <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="<?= BASE_URL ?>finalists">FINALISTS</a></li>
							<li><a href="<?= BASE_URL ?>events/past">GALLERY</a></li>
						</ul>
					</li>
					<li <?= $this->uri->segment(1) == 'perks' ? 'class="active"' : '' ?>>
						<a href="<?= BASE_URL ?>perks" class="dropdown-toggle" data-toggle="dropdown">PERKS <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<?php /* <li><a href="<?= BASE_URL ?>perks/bid">BID</a></li> */ ?>
							<li><a href="<?= BASE_URL ?>perks/buy">BUY</a></li>
							<?php /* <li><a href="<?= BASE_URL ?>perks/reserve">RESERVE</a></li> */ ?>
						</ul>
					</li>
					<li <?= $this->uri->segment(1) == 'move_forward' ? 'class="active"' : '' ?>>
						<a href="<?= BASE_URL ?>move_forward" class="dropdown-toggle" data-toggle="dropdown">MOVEFWD <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="<?= BASE_URL ?>move_forward">ACTIVE OFFERS</a></li>
							<li><a href="<?= BASE_URL ?>move_forward/past">PAST OFFERS</a></li>
							<?php /* foreach ($this->nav_movefwd_categories as $category): ?>
							<li><a href="<?= BASE_URL ?>move_forward/category/<?= $category['category_id'] ?>"><?= $category['category_name'] ?></a></li>
							<?php endforeach */ ?>
						</ul>
					</li>
					<li class="<?= $this->uri->segment(1) == 'games' ? 'active' : '' ?> hidden-xs"><a href="<?= BASE_URL ?>games">GAMES</a></li>
					<!--<li class="<?= $this->uri->segment(1) == 'rubyburst' ? 'active' : '' ?> hidden-xs"><a href="<?= BASE_URL ?>rubyburst">RUBYBURST</a></li>-->
					<?php /* <li <?= $this->uri->segment(1) == 'perks' ? 'class="active"' : '' ?>><a href="#">PERKS</a></li> */ ?>
					<?php /* <li class="hidden-xs"><a href="<?= BASE_URL ?>crossover">CROSS|OVER</a></li> */ ?>
					<li <?= $this->uri->segment(1) == 'profile' ? 'class="active"' : '' ?>><a href="<?= BASE_URL ?>profile">MY ACCOUNT</a></li>
					<li id="profiling_nav"></li>
				</ul>
				<ul class="sub-nav nav navbar-nav navbar-right">
					<li><a href="<?= BASE_URL ?>points"> <span class="glyphicon glyphicon-cog"></span> POINTS  :  <span id="points"><?= $this->session->userdata('user_points') ?></span></a></li>
					<li>
						<a id="notifications-link" href="<?= BASE_URL ?>notifications" <?= $this->unread_notifications ? 'class="notify"' : '' ?>>
							<span class="glyphicon glyphicon-exclamation-sign"></span> NOTIFICATIONS <span class="glyphicon glyphicon-exclamation-sign"></span>
						</a>
					</li>
					<li><a href="<?= BASE_URL ?>faq" > <span class="glyphicon glyphicon-list-alt"></span> FAQ <span class="glyphicon glyphicon-question-sign"></span></a></li>
					<!-- <li><a href="javascript:void(0)" id="newsfeed-toggle" data-toggle="collapse" data-target="#newsfeed"> <span class="glyphicon glyphicon-list-alt"></span> NEWSFEED <span class="glyphicon glyphicon-list-alt"></span></a></li> -->
					<li><a href="<?= BASE_URL ?>welcome/logout"> <span class="glyphicon glyphicon-log-out"></span> LOGOUT <span class="glyphicon glyphicon-log-out"></span></a> </li>
				</ul>
			</div>

		</div>
		<div class="grunge-foreground"></div>
		<div class="grunge-background"></div>
		<div class="container newsfeed-holder">
			<div class="newsfeed collapse" id="newsfeed">
				<h3>NEWSFEED</h3>
				<div id="newsfeed-content">
					<div class="newsfeed-box">
						<ul id="newsfeed-list">
							<li>Loading...</li>
						</ul>
					</div>
				</div>
				<div class="close"><i>CLOSE</i> &times;</div>
			</div>
		</div>
	</header>

	<footer class="main">
		<div class="r18">
			This website is for adult smokers 18 years or older residing in the Philippines. Access to this website is subject to age verification
		</div>

		<div class="row">
			<nav class="col-md-6 col-sm-8">
				<a onclick="popup.open({url:'<?= BASE_URL ?>popups/terms.html'})">terms and conditions</a>
			 	<a onclick="popup.open({url:'<?= BASE_URL ?>popups/privacy-content.html'})">Privacy Statement and Consent</a>
			 	<a onclick="popup.open({url:'<?= BASE_URL ?>popups/contactus.html'})">contact us</a>
			 	<a target="_blank" href="http://www.pmi.com/eng/our_products/health_effects_of_smoking/pages/health_effects_of_smoking.aspx">smoking and health</a>
			</nav>

			<div class="col-md-6 col-sm-4 copyright">copyright &copy; 2013 PMFTC INC. All rights reserved</div>
		</div>
		<div class="government-warning">
			<span>GOVERNMENT WARNING: CIGARETTE SMOKING IS DANGEROUS TO YOUR HEALTH</span>
		</div>
	</footer>

	<section class="main <?=@$fixedContent ? 'fixed' : ''?>">
		<?php
			$source = $this->uri->segment(1);

			if($this->uri->segment(1)=='events'){
				$source = 'backstage_pass';
			}else if(!$this->uri->segment(1)){
				$source = "welcome";
			}

			$hidden_pack = $this->session->userdata($source);

			if($hidden_pack){?>
					<div onclick="popup.open({url:'<?=BASE_URL?>hidden_pack?source=<?=$source?>&pack_id=<?=$hidden_pack['id']?>',blurClose : false})" class="find-marlboro">
						<img src="<?=$hidden_pack['image']?>">
					</div>
		 <?php

			}

			echo $main_content; ?>
	</section>



<!-- Latest compiled and minified JavaScript -->
<script type="text/javascript"> var is_cm = <?php echo @$this->session->userdata("is_cm") ?>; </script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/lytebox.2.3.js"></script>
<script src="scripts/exec.js"></script>
<script src="scripts/owl.carousel.min.js"></script>
<script src="scripts/lib.js"></script>
<script src="scripts/app.js"></script>
<script src="scripts/jquery.mousewheel.js"></script>
<script src="scripts/jquery.jscrollpane.min.js"></script>
<script src="scripts/jquery.touchSwipe.min.js"></script>

<?php if (PROD_MODE): ?>
<script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  	<?php
	  	$userId = $this->session->userdata('user_id');
		// New Google Analytics code to set User ID.
		// $userId is a unique, persistent, and non-personally identifiable string ID.
		if (!$userId) {
		 	$gacode = "ga('create', 'UA-42684485-7', { 'userId': '%s' });";
		  	echo sprintf($gacode, $userId);
		}else{
			echo "ga('create', 'UA-42684485-7', 'marlboro.ph');";
		}
	?>

  // Set the user ID using signed-in user_id.
  ga('send', 'pageview');
</script>
<?php endif ?>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="<?= BASE_URL ?>scripts/shiv.js"></script>
  <script src="<?= BASE_URL ?>scripts/respond.min.js"></script>
<![endif]-->
<script>
<?php if ($this->session->userdata('edm_popup')): ?>
	popup.open({ url : "<?= BASE_URL.$this->session->flashdata('edm_popup') ?>" });
<?php endif ?>

User.getTopInterest();
</script>

<script type="text/javascript">
    setTimeout(function() { window.location.href = "<?=site_url()?>welcome/logout"; }, 1000 * 60 * 60 * 24);
</script>
<!-- visit <?= page_visit() ?> -->
</body>
</html>
