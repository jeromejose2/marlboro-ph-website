<div class="wrapper ">
		
			<div class="row container main messages">
				<?php $this->load->view('messages/nav') ?>
				<div class="col-md-9 col-md-push-3">
					<form action="<?=BASE_URL?>messages/delete_messages" method="POST" id="inbox-form">
						<ul class="inbox">
							
							<?php if($rows){ ?>
							<li>
 								<div class="clearfix"></div>
							</li>
							<?php foreach($rows as $v){?>
									<li <?=!$v->status ? 'class="unread"' : '' ?>>
 										<a href="<?=BASE_URL.'trash/index/'.$v->message_id.'/'.url_title($v->subject)?>">											
											<span class="name"><?=$v->sender_name?></span>
											<span class="date"><?=date('F d, Y',strtotime($v->date_sent)).' at '.date('h:i a',strtotime($v->date_sent))?></span>
											<span class="subject"><?=$v->subject?></span>
											<span class="msg"><?=$v->message?></span>
										</a>
									</li>
							<?php }
	 						 }else{ ?>
	 						 <li style="border-bottom: 0;"><h3>Trash is empty.</h3></li>
	 						 <?php } ?>
						</ul>
					</form>
				</div>

			</div>
	</div>




<script>

jQuery(document).ready(function(){

 
	jQuery('#delete-message-btn').on('click',function(){
  
		if(jQuery('input:checked').length > 0){		
			popup.open({type: 'confirm',message:'<h3>Are you sure you want to delete?</h3>',onConfirm:function(){ jQuery('#inbox-form').trigger('submit'); } });			
		}else{
			popup.open({type: 'alert',message:'<h3>Please select message to delete.</h3>'});
		}		
 
	});


});


</script>