
<div class="wrapper movefwd">	
	<div class="container main">
		<div class="row move-fwd-categories">
			<?php foreach ($movefwd_categories as $category): ?>
			<div class="col-sm-6 col-md-3 item">
				<div class="thumb-holder col-md-12">
					<div class="thumbnail" style="background-image:url('<?= BASE_URL ?>images/sample-category-<?= strtolower($category['category_name']) ?>.jpg')"></div>
				</div>
				<div class="col-md-12">
					<h2><?= $category['category_name'] ?></h2>
					<a href="<?= BASE_URL ?>move_forward/category/<?= $category['category_id'] ?>" class="button"><i>VIEW ALL</i></a>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php endforeach ?>
		</div>
	</div>
	<?php $this->load->view('move_fwd/splash') ?>
</div>

<div class="wrapper home fixed">
	
	<div class="container">
		<div class="holder-home">
			<h1 class="page-black">DECIDE NOW</h1>
			<!--<a class="button" href="<?= BASE_URL ?>finalists"><i>Cross|Over</i></a>-->
			<a class="button" href="<?= BASE_URL ?>points"><i>EARN POINTS</i></a>
			<a class="button" href="<?= BASE_URL ?>rubyburst"><i>RUBY BURST</i></a>
		</div>

	</div>

</div>
 
<script type="text/javascript">

	$(function(){

		console.log('User Session Key: <?=$session_key?>');
		console.log("<?php echo @$flash_offer ?>");
		console.log("<?php echo $this->session->userdata('stat') ?>");
		<?php /* console.log('Referral Offer: <?php echo var_dump($referral_offer) ?>'); */ ?>
		<?php if($this->session->userdata('login_promo')==1) { ?>
			popup.open({url:"login_offer/get_offer"});
		<?php } ?>
		<?php if($referral_offer){  ?>
		 		popup.open({url:"flash_offer/get_flash_offer?prize_id=<?=$referral_offer?>&rand=<?=uniqid()?>&type=referral"});
		 <?php } ?> 

		 <?php if(!$referral_offer && $birthday_offer && $birthdate != '0000-00-00'){ ?>
		 		popup.open({url:"birthday_offer/get_birthday_offer?prize_id=<?=$birthday_offer?>&flash_offer=<?=$flash_offer?>&rand=<?=uniqid()?>",
		 					onClose:function(){		
								<?php if($flash_offer && !$flash_offer_confirmed) { ?>
									 	 popup.open({url:"flash_offer/get_flash_offer?prize_id=<?=$flash_offer?>&rand=<?=uniqid()?>&type=flash"});
								 <?php } ?> 		
		 					}
		 				});
		 <?php } ?>

		 <?php if(!$referral_offer && !$birthday_offer && $flash_offer && !$flash_offer_confirmed) { ?>
		 			popup.open({url:"flash_offer/get_flash_offer?prize_id=<?=$flash_offer?>&rand=<?=uniqid()?>&type=flash"});
		 <?php } ?> 

		 <?php if(!$referral_offer && !$birthday_offer && !$flash_offer && $bids){ ?>
		 		popup.open({url:"perks/bid/confirm_address"});
		 <?php } ?> 
	});

	

</script>