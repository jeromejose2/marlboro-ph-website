<?php $rand = rand(1,3); ?>

<div class="popup popup-premium">
	<div class="content txtcenter">
		<button class="popup-close">× <span>CLOSE</span></button>
		
		<?php switch ($rand) { case 1: ?>

			<h2>Congratulations!</h2>
			<div class="line-separator"></div><br>
			<img src="<?=BASE_URL?>images/premium-bg-found-popup.jpg" class="img-responsive visible-xs m-auto">
			<h4>designed to stand out</h4>
			<h6>a collaboration with Pininfarina, the designers of Ferrari</h6>
		
		<?php break; case 2 : ?>

			<h2>Congratulations!</h2>
			<div class="line-separator"></div><br>
			<div class="row found-premium">
				<div class="col-sm-4 col-sm-push-8">
					<img src="<?=BASE_URL?>images/premium-bg-found-popup-02.jpg">
				</div>
				<div class="col-sm-8  col-sm-pull-4">
					<br>
					<h4>stays fresh always</h4>
					<h6>discover the difference Pro-Fresh technology makes</h6>
				</div>
			</div>

		<?php break; case 3 : ?>

			<h2>Congratulations!</h2>
			<div class="line-separator"></div><br>
			<div class="row found-premium">
				<div class="col-sm-4 col-sm-push-8">
					<img src="<?=BASE_URL?>images/premium-bg-found-popup-03.jpg">
				</div>
				<div class="col-sm-8  col-sm-pull-4">
					<br><br>
					<h4>crafted for discerning taste</h4>
					<h6>masterfully blended with the world’s finest tobaccos</h6>
				</div>
			</div>

		<?php break;}?>

	</div>

	<?php if($rand==1) { ?> <img src="<?=BASE_URL?>images/premium-bg-found-popup.jpg" class="img-responsive hidden-xs"> <?php } ?>

</div>
<script type="text/javascript">
$('.find-marlboro').remove();
</script>