	<div class="popup perks">
		<div class="content">
		<button class="popup-close">&times;<span>CLOSE</span></button>
		

		<div class="row perks-details">
			<div class="col-sm-5">
				
				<div class="carousel">
					<div id="holder">
						<?php if($media) {
							foreach ($media as $k => $v) { ?>
								<img src="<?php echo BASE_URL ?>uploads/buy/media/150_150_<?php echo $v['media_content'] ?>">	
							<?php }
						} ?>
					</div>
				</div>

			</div>

			<div class="col-sm-7">
				<h2 style="width: 90%;"><?php echo $item['buy_item_name'] ?></h2>
				<?php echo $item['description'] ?>

				<div class="transaction">
					<h2>CREDIT VALUE: <span class="red"><?php echo number_format($item['credit_value']) ?> POINTS</span></h2>
					<?php if($item['total_points'] - $item['credit_value'] >= 0) { ?>
					<h4 id="remaining-h4">REMAINING POINTS AFTER PURCHASE: <span class="red"><span id="remaining-points"><?php echo number_format($item['total_points'] - $item['credit_value']) ?></span> PTS</span></h4>
					<?php } else { ?>
					<h4 id="remaining-h4">Insufficient points</h4>
					<?php }?>
					<form id="buy-form">
					
					<div class="fieldset">
						<div class="txtcenter"><h2>OUT OF STOCK</h2></div>
					</div>
					
					<input name="id" type="hidden" value="<?php echo $item['buy_item_id'] ?>">
					</form>

				</div>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$("#holder").owlCarousel({
		items : 1,
		itemsDesktop : [3000,1],
		itemsMobile : [767,1],
		itemsTablet : [991,1],
		pagination : true
	});

	$.getJSON('<?php echo BASE_URL ?>api/notifications', function(data) {
		remainingPoints =  data.points - <?php echo $item['credit_value'] ?>;
		if(remainingPoints < 0) {
			$('#confirm-button').addClass('disabled');
			$('#remaining-h4').html('Insufficient points');
		}
		$('#remaining-points').html(remainingPoints);
	})

	$('#confirm-button').click(function() {
		if($(this).hasClass('disabled')) return;
		$.post('<?php echo BASE_URL ?>perks/buy/buy_item', $('#buy-form').serialize(), function(data) {
			if(data.success == 1) {
				popup.dialog({message:"You have successfully bought <?php echo $item['buy_item_name'] ?>" , title : "Buy Success", align: "center"});
			} else {
				popup.dialog({message: data.error , title : "Buy Unsuccessful", align: "center", onClose: function(){location.reload();}});
			}
			
		})
	});
});
</script>