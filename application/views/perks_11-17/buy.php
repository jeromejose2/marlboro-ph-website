<!-- main content starts here  -->
<section class="main">
	<div class="wrapper">	
		<div class="container main">
			
			<div class="col-md-3 col-lg-2  col-sm-12 side-rail">
				<?php $this->load->view('perks/nav') ?>
			</div>
			<div class="col-md-9 col-md-push-3  col-lg-push-2  main-rail">
				<div class="upload-from-event">
					<button type="button" class="button-small" onclick="popup.open({url:'<?=BASE_URL?>perks/buy/mechanics'})"><i>Read Terms &amp; Conditions</i></button>
				</div>
				<div class="clearfix"></div>
				<br>

				<div class="row perks-items">
					<!-- items -->
					<?php if($items) {
						$c = 0;
						foreach($items as $k => $v) {$c++; ?>
						<?php if($v['status'] == 2) { ?>
						<div class="col-md-6 item">
							<div class="thumb-holder col-sm-6">
								<div class="thumbnail" style="background-image:url('<?php echo BASE_URL ?>uploads/buy/main_<?php echo $v['buy_item_image'] ?>')"></div>
							</div>
							<div class="col-sm-6 copy">
								<h2><?php echo $v['buy_item_name'] ?></h2>
								<p><?php echo strlen(strip_tags($v['description'])) > 200 ? substr(strip_tags($v['description']), 0, 197) . '...' : strip_tags($v['description']) ?></p>
								<button type="button" class="button disabled" ><i>COMING SOON</i></button>
							</div>
							<div class="clearfix"></div>
						</div>
						<?php } else { ?>
						<?php
							$btn_style = '';
							if($v['stock'] <= 0) {
								$btn_style = 'inactive';
							} elseif($v['has_bought']) {
								$btn_style = 'inactive';
							} elseif(isset($available_points) && $available_points < $v['credit_value']) {
								$btn_style = 'inactive';
							}
						?>
						<?php
							$url = BASE_URL . 'perks/buy/item/' . $v['buy_item_id'];
							if($v['has_bought']) {
								$url = BASE_URL . 'perks/buy/item/' . $v['buy_item_id'] . '/has_bought';
							} elseif(isset($bid_data['reserved_points']) && $bid_data['available_points'] < $v['credit_value']) {
								$url = BASE_URL . 'perks/buy/item/' . $v['buy_item_id'] . '/no_points';
							}
						?>
						<div class="col-md-6 item <?php echo $btn_style ?>">
							<div class="thumb-holder col-sm-6">
								<?php /* <a href="#" onclick="popup.open({url:'<?php echo BASE_URL ?>perks/buy/item/<?php echo $v['buy_item_id'] ?>'}); return false"> */ ?>
								<a href="#" onclick="popup.open({url:'<?php echo $url ?>'}); return false">
									<div class="thumbnail" style="background-image:url('<?php echo BASE_URL ?>uploads/buy/main_<?php echo $v['buy_item_image'] ?>')"></div>
								</a>
							</div>
							<div class="col-sm-6 copy">
								<?php /* <h2><a href="#" onclick="popup.open({url:'<?php echo BASE_URL ?>perks/buy/item/<?php echo $v['buy_item_id'] ?>'}); return false"><?php echo $v['buy_item_name'] ?></a></h2> */ ?>
								<h2><a href="#" onclick="popup.open({url:'<?php echo $url ?>'}); return false"><?php echo $v['buy_item_name'] ?></a></h2>
								<p><?php echo strlen(strip_tags($v['description'])) > 200 ? substr(strip_tags($v['description']), 0, 197) . '...' : strip_tags($v['description']) ?></p>
								<?php /* <button type="button" class="button" onclick="popup.open({url:'<?php echo BASE_URL ?>perks/buy/item/<?php echo $v['buy_item_id'] ?>'})"><i><?php echo $v['stock'] == 0 ? 'DETAILS' : 'BUY NOW!' ?></i></button> */ ?>
								<button type="button" class="button" onclick="popup.open({url:'<?php echo $url ?>'})"><i>
									<?php if($v['has_bought']): ?>
										PURCHASED
									<?php elseif($v['stock'] <= 0): ?>
										DETAILS
									<?php elseif(isset($available_points) && $available_points < $v['credit_value']): ?>
										DETAILS
									<?php else: ?>
										BUY NOW!
									<?php endif; ?>
									<?php # echo $v['stock'] == 0 ? 'DETAILS' : 'BUY NOW!' ?>
								</i></button>
							</div>
							<div class="clearfix"></div>
						</div>
						<?php } ?>
						
						<?=($c%2==0) ? '<div class="clearfix"></div>' : ''?>
						<?php } 
					} ?>
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view('perks/splash-popup') ?>
</section>
<?php if(!$viewed): ?>
<script type="text/javascript">
$(function(){
	popup.open({html:'#popup1'});
});	
</script>
<?php endif; ?>