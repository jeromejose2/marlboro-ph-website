	<div class="popup perks">
		<div class="content">
		<button class="popup-close">&times;<span>CLOSE</span></button>
		

		<div class="row perks-details">
			<div class="col-sm-5">
				
				<div class="carousel">
					<div id="holder">
					  <?php if($media) {
						foreach($media as $k => $v) { ?>
							<img src="<?php echo BASE_URL ?>uploads/bid/media/150_150_<?php echo $v['media_content'] ?>"> 
						<?php }
					 } ?>
					</div>
				</div>

			</div>

			<div class="col-sm-7">
				<h2><?php echo $item['bid_item_name'] ?></h2>
				<?php echo $item['details'] ?>

				<div class="transaction" <?php echo !$bidder ? 'style="display:none"' : '' ?>>
					<!-- <h2>CURRENT BID: <span class="red"><?php echo number_format($bidder['bid']); ?> POINTS</span></h2>
					<h4>HIGHEST BIDDER: <span class="red"><?php echo $bidder['nick_name']; ?></span></h4> -->
					<?php if(!isset($bidder['nick_name'])) { ?>
					<h4>MINIMUM BID: <span id="high-bidder" class="red"><?php echo $item['starting_bid'] ?> POINTS</span></h4>
					<?php } else {?>
					<h4>CURRENT HIGHEST BIDDER: <span class="red" ><span id="high-bidder" class="red"><?php echo @$bidder['nick_name']; ?></span>, <span id="high-bid" ><?php echo number_format(@$bidder['bid']); ?></span> POINTS</span></h4>
					<?php } ?>
				</div>


				
				<hr>
				
				<div class="transaction">
					<h2>PLACE YOUR BID: <span class="red"><?php echo number_format($this->uri->segment(5)) ?> POINTS</span></h2>
				</div>
				<small><?php echo number_format($this->uri->segment(5)) ?> points will be blocked off for you to use on other features while you are the highest bidder of this item. These points will only be deducted from your account when you win this bid.</small>
				<br><br>		
				<div class="txtcenter">	
					<button type="button" class="button confirm-button" onclick="bid()"><i>CONFIRM</i></button>
					<button type="button" class="button" onclick="popup.open({url:'<?php echo BASE_URL ?>perks/bid/item/<?php echo $item['bid_item_id'] ?>'});"><i>BACK</i></button>
				</div>
				
				
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$("#holder").owlCarousel({
		items : 1,
		itemsDesktop : [3000,1],
		itemsMobile : [767,1],
		itemsTablet : [991,1],
		pagination : true
	});
});

function bid() {
	$.ajax({
		beforeSend: function(){
	        $('.confirm-button').attr('disabled','disabled').html('<i><img src="<?=BASE_URL?>images/spinner.gif" /> CONFIRMING...</i>');
	    },
	    success: function(){
			$.post('<?php echo BASE_URL ?>perks/bid/bid_item', {id: '<?php echo $item["bid_item_id"] ?>', 'bid' : '<?php echo $this->uri->segment(5) ?>'}, function(data) {
				if(data.success == 1) {
					popup.dialog({message:"You have successfully placed a bid of <?php echo $this->uri->segment(5) ?> points for <?php echo $item['bid_item_name'] ?>." , title : "BID SUCCESSFUL", align: 'center'});
				} else {
					popup.dialog({message: data.error , title : "BID UNSUCCESSFUL", align: 'center'});
				}
			})
		}
	});
}
</script>