<?php if($row && $row->user_type=='administrator'){ ?>
<style type="text/css">
video::-webkit-media-controls-fullscreen-button {
    display: none;
}
</style>
<script type="text/javascript" src="<?=BASE_URL?>scripts/jwplayer/jwplayer.js"></script>
<?php } ?>

		
<!-- main content starts here  -->
 	<div class="wrapper">
		
		<div class="container main">
			<div class="row">
				<div class="col-md-3 col-lg-2  col-sm-12 side-rail sticky">
					<nav>
						<a href="<?=BASE_URL?>about/photos"> <i class="glyphicon glyphicon-camera"></i>PHOTOS</a>
						<a href="<?=BASE_URL?>about/videos" class="active"> <i class="glyphicon glyphicon-film"></i>VIDEOS</a>
						<a href="<?=BASE_URL?>about/news"> <i class="glyphicon glyphicon-list"></i>NEWS</a>
					</nav>
				</div>

				<div class="col-md-9 col-lg-8  col-md-push-3 col-lg-push-2  main-rail">
					<h2><?=($row) ? $row->title : 'Content not found.'?><h2>
					 
					<div class="article-stat">
						<span>COMMENTS: <?=(int)@$total_comments?></span>
						<span>VIEWS: <?=($row) ? (int)$row->views : '0'?></span>
					</div>
					<figure class="theater">

							<?php if($prev){ ?>
									<div class="arrow left" onclick="redirect('<?=BASE_URL?>about/videos/content/<?=$prev->url_title?>')"></div> 
							<?php } ?>

							<?php if($next){ ?>
									<div class="arrow right" onclick="redirect('<?=BASE_URL?>about/videos/content/<?=$next->url_title?>')"></div>
							<?php } ?>						
							
	  						<?php if($row && $row->user_type=='administrator'){ ?>
				  						<video id="video" width="100%" height="100%"  controls="controls" autoplay>
					  						<source src="<?=$row->video?>" type="<?=$row->video_mime_type?>" />
											<script type="text/javascript">
											function tryFlash(){
												// flash fallback
												jwplayer("video").setup({ flashplayer: "<?=BASE_URL?>scripts/jwplayer/player.swf",
																		modes: [{ type: "html5" }, { type: "flash", src: "<?=BASE_URL?>scripts/jwplayer/player.swf", autostart:true}]});	
	 										}
											function initVideo(){
												var video = document.getElementById("video");
												// check if html5 video tag is supported if not fallback to flash
												 if(!video.play ? true : false)
													tryFlash();
											}
											initVideo();
											</script>
										</video>
 
			  				<?php }else if($row){  echo $row->video_embed;   } ?>

	  						<?php // if($row){ echo $row->description;  } ?>


	 				</figure>
				</div>

				<div class="col-md-8 col-md-push-3 col-lg-push-2">
					<?= show_comments(ABOUT_VIDEOS, $row->about_video_id, $row->user_type == 'registrant' ? array('recipient_id' => $row->uploader_id) : array(), array('category' => 'Comment', 'action' => 'submit', 'label' => 'Commented on About Videos #'.$row->about_video_id.': '.$row->title, 'value' => 1)) ?>
				</div>
			</div>
		</div>

	</div>
	<script type="text/javascript">
	$(function(){
		swipePrevNext();
	});
	function redirect(url){
		top.location.href=url;
	}
	</script>



	

 
	
