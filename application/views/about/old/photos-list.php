<!-- main content starts here  -->
 	<div class="wrapper">
		
		<div class="container main">
			<div class="row">
				<div class="col-md-3 col-lg-2  col-sm-12 side-rail sticky">
					<nav>
						<a href="<?=BASE_URL?>about/photos" class="active"> <i class="glyphicon glyphicon-camera"></i>PHOTOS</a>
						<a href="<?=BASE_URL?>about/videos"> <i class="glyphicon glyphicon-film"></i>VIDEOS</a>
						<a href="<?=BASE_URL?>about/news"> <i class="glyphicon glyphicon-list"></i>NEWS</a>
					</nav>
				</div>

				<div class="col-md-9 col-lg-10 col-md-push-3 col-lg-push-2  main-rail">
					
					<div class="txtcenter">

						<a href="<?=BASE_URL?>events/past" ><img src="<?=BASE_URL?>/images/photo-gallery.png"></a>
						<h3>GO TO <a href="<?=BASE_URL?>events/past" >BACKSTAGE PASS </a> TO SEE PHOTOS FROM OUR EVENTS</h3>

					</div>
				</div>
			</div>
		</div>

	</div>

	

 <!-- main content ends here  --> 
</html>