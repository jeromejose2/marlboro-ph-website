<div class="comments">

	<div class="add-comment">
		<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>

		<span>Jay Ramirez</span>
		<button type="button" onclick="popup.open({url:'popups/comment-attach.html'})">ATTACH PHOTO</button>

		<textarea class="comment-text"></textarea>
		<i> GO </i>
	</div>

	<ul class="level-1">
		<li>
			<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
			<span class="name">Jay Ramirez</span>
			<span class="message"><!-- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at augue massa. 
			Donec sed nulla a sem fringilla volutpat ut in purus. Aliquam egestas tellus id hendrerit facilisis. --> </span>
			<span class="timestamp">02/07/2014 - 4:32 pm</span>

			<button class="reply">Reply</button>
			<div class="reply">
				<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
				<textarea class="comment-text"></textarea>
				<i> GO </i>
			</div>

			<!-- second level comment -->
			<div class="comments">
				<ul>
					<li>
						<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
						<span class="name">Jay Ramirez</span>
						<span class="message"><!-- Lorem ipsum dolor sit ,   elit. Sed at augue massa. 
						Donec sed nulla a sem fringilla volutpat ut in purus.  egestas tellus id hendrerit facilisis. -->
						</span>
						<span class="timestamp">02/07/2014 - 4:32 pm</span>

						<button class="reply">Reply</button>
						<div class="reply">
							<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
							<textarea class="comment-text"></textarea>
							<i> GO </i>
						</div>
					</li>
					<li>
						<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
						<span class="name">Jay Ramirez</span>
						<span class="message">  <!-- dolor sit ,   elit. Sed at augue massa. 
						Donec sed nulla a sem fringilla volutpat ut in . Aliquam egestas tellus id hendrerit facilisis. --> </span>
						<span class="timestamp">02/07/2014 - 4:32 pm</span>

						<button class="reply">Reply</button>
						<div class="reply">
							<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
							<textarea class="comment-text"></textarea>
							<i> GO </i>
						</div>
					</li>
					<li>
						<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
						<span class="name">Jay Ramirez</span>
						<span class="message">  <!-- dipiscing elit. Sed at  massa. 
						Donec sed nulla a sem fringilla volutpat ut in purus. Aliquam egestas tellus id hendrerit facilisis. --> </span>
						<span class="timestamp">02/07/2014 - 4:32 pm</span>

						<button class="reply">Reply</button>
						<div class="reply">
							<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
							<textarea class="comment-text"></textarea>
							<i> GO </i>
						</div>
					</li>
					<li>
						<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
						<span class="name">Jay Ramirez</span>
						<span class="message"><!-- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at augue massa. 
						Donec sed nulla a sem fringilla volutpat ut in purus. Aliquam egestas tellus id hendrerit facilisis. --> </span>
						<span class="timestamp">02/07/2014 - 4:32 pm</span>

						<button class="reply">Reply</button>
						<div class="reply">
							<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
							<textarea class="comment-text"></textarea>
							<i> GO </i>
						</div>
					</li>
				</ul>
			</div>
			<!-- second level comment -->

		</li>

		<li>
			<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
			<span class="name">Jay Ramirez</span>
			<span class="message"><!-- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at augue 
			sed nulla a sem fringilla volutpat ut in purus. Aliquam egestas tellus id hendrerit facilisis. --> </span>
			<span class="timestamp">02/07/2014 - 4:32 pm</span>

			<button class="reply">Reply</button>
			<div class="reply">
				<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
				<textarea class="comment-text"></textarea>
				<i> GO </i>
			</div>

		</li>
		<li>
			<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
			<span class="name">Jay Ramirez</span>
			<span class="message"><!-- Lorem ipsum dolor sit amet, consectetur 
			Donec sed nulla a sem fringilla volutpat ut in purus. Aliquam egestas tellus id hendrerit facilisis. --> </span>
			<span class="timestamp">02/07/2014 - 4:32 pm</span>

			<button class="reply">Reply</button>
			<div class="reply">
				<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
				<textarea class="comment-text"></textarea>
				<i> GO </i>
			</div>
		</li>

		<li>
			<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
			<span class="name">Jay Ramirez</span>
			<span class="message"><!-- Lorem ipsum dolor sit amet,   elit. Sed at augue massa. 
			Donec sed nulla a sem fringilla volutpat ut in purus. Aliquam egestas tellus id  facilisis. --> </span>
			<span class="timestamp">02/07/2014 - 4:32 pm</span>

			<button class="reply">Reply</button>
			<div class="reply">
				<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
				<textarea class="comment-text"></textarea>
				<i> GO </i>
			</div>
		</li>
		<li>
			<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
			<span class="name">Jay Ramirez</span>
			<span class="message"><!-- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at augue massa. 
			Donec sed nulla a sem fringilla volutpat ut in purus. Aliquam egestas tellus id hendrerit facilisis. --> </span>
			<span class="timestamp">02/07/2014 - 4:32 pm</span>
			<button class="reply">Reply</button>
			<div class="reply">
				<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
				<textarea class="comment-text"></textarea>
				<i> GO </i>
			</div>
		</li>

		<li>
			<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
			<span class="name">Jay Ramirez</span>
			<span class="message"><!-- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at augue massa. 
			Donec sed nulla a sem fringilla volutpat ut in purus. Aliquam egestas tellus id hendrerit facilisis. --> </span>
			<span class="timestamp">02/07/2014 - 4:32 pm</span>

			<button class="reply">Reply</button>
			<div class="reply">
				<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
				<textarea class="comment-text"></textarea>
				<i> GO </i>
			</div>
		</li>
		<li>
			<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
			<span class="name">Jay Ramirez</span>
			<span class="message"><!-- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at augue massa. 
			Donec sed nulla a sem fringilla volutpat ut in purus. Aliquam egestas tellus id hendrerit facilisis. --> </span>
			<span class="timestamp">02/07/2014 - 4:32 pm</span>

			<button class="reply">Reply</button>
			<div class="reply">
				<figure><img src="<?=BASE_URL?>images/sample-comment-pic.jpg"></figure>
				<textarea class="comment-text"></textarea>
				<i> GO </i>
			</div>
		</li>
	</ul>
	<button class="comments-show-more">SHOW MORE</button>
</div>


<script type="text/javascript">
$(document).ready(function(){

	textresize($('.comment-text'));
	
	$('.comment-text').bind('keyup',function(){

		textresize($(this));

	});

	function textresize(el){
		var scroll = el.prop('scrollHeight');
		var height = el.outerHeight();
		var add = 12;

		if( scroll > height){
			el.css('height',scroll + add);
		}

		if( scroll < height){
			el.css('height',scroll + add);
		}

		if($.trim(el.val())==''){
			el.css('height',26);
		}
	}

	$('button.reply').click(function(){
		
		$('button.reply').show(0);
		$(this).hide(0);
		$('div.reply').hide();
		$(this).siblings('div.reply').fadeIn();
	});

});
</script>