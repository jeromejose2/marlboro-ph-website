<div class="popup-view">
	<div class="content ">
		<button class="popup-close">&times; <span>CLOSE</span></button>

				<h2 class="dinline" id="product-media-title">NO TITLE</h2>
				<figure class="theater" id="gallery-current-preview">
					<div class="no-image"></div>
				</figure>

				<div class="carousel">
					<div class="arrow left" id="product-media-gallery-prev"></div>
					<div class="arrow right" id="product-media-gallery-next"></div>

					<div id="product-media-gallery" class="thumbs">
						<?php if ($gallery): ?>
							<?php foreach ($gallery as $gal): ?>
							<div class="thumbnail" data-title="<?= $gal['product_media_title'] ?>" data-id="<?= $gal['product_media_id'] ?>" data-type="<?= $gal['product_media_type'] == 1 ? 'photo' : 'video' ?>" data-src="<?= $gal['product_media_content'] ?>">
								<?php if ($gal['product_media_type'] == 1): ?>
								<div class="no-image" style="background-image:url(<?= BASE_URL ?>uploads/product_info/media/150_150_<?= $gal['product_media_content'] ?>)"></div>
								<?php elseif ($gal['product_media_type'] == 2): ?>
								<div class="no-image" style="background-image:url(<?= BASE_URL ?>uploads/product_info/media/150_150_thumb_<?= $gal['product_media_content'] ?>.jpg)"></div>
								<?php endif ?>
							</div>
							<?php endforeach ?>
						<?php else: ?>
						<div class="thumbnail"><div class="no-image" style="background-image:url(<?= BASE_URL.DEFAULT_IMAGE ?>)"></div></div>
						<div class="thumbnail"><div class="no-image" style="background-image:url(<?= BASE_URL.DEFAULT_IMAGE ?>)"></div></div>
						<div class="thumbnail"><div class="no-image" style="background-image:url(<?= BASE_URL.DEFAULT_IMAGE ?>)"></div></div>
						<?php endif ?>
					</div>

				</div>

		</div>
	</div>
</div>

<script type="text/javascript">

var itemsPerSlide = 3;
var currentGalleryIndex = 0;
var galleryThumbsCount = <?= count($gallery) ?>;
var galleryThumbsList = $("#product-media-gallery");
galleryThumbsList.owlCarousel({
	items : itemsPerSlide,
	itemsDesktop : [3000,3],
	itemsMobile : [767,3],
	itemsTablet : [991,4],
	pagination : true,
	scrollPerPage : true
});

var galleryThumbs = galleryThumbsList.data("owlCarousel");
var galleryThumbnails = galleryThumbsList.find(".thumbnail");

$("#product-media-gallery-prev").click( function() {
	galleryThumbs.prev();
	var index = --currentGalleryIndex * itemsPerSlide;
	if (index < 0) {
		currentGalleryIndex = Math.floor((galleryThumbsCount - 1) / itemsPerSlide);
		index = currentGalleryIndex * itemsPerSlide;
	}
	galleryThumbnails.eq(index).trigger("click");
});

$("#product-media-gallery-next").click( function() {
	galleryThumbs.next();
	var index = ++currentGalleryIndex * itemsPerSlide;
	if (index >= galleryThumbsCount) {
		index = 0;
		currentGalleryIndex = index;
	}
	galleryThumbnails.eq(index).trigger("click");
});

var productMediaTitle = $("#product-media-title");
var galleryCurrentPreview = $("#gallery-current-preview");
var currentGalleryItemId = null;
galleryThumbnails.click( function() {
	
	var self = $(this);
	var id = self.data("id");
	if (!id) {
		return;
	}
	var title = self.data("title");
	if (title) {
		productMediaTitle.html(title);
	} else {
		productMediaTitle.html("NO TITLE");
	}
	
	var type = self.data("type");
	var src = self.data("src");
	if (type == "photo") {
		galleryCurrentPreview.html("<div class='no-image' style='background-image: url(\"" + BASE_URL + "uploads/product_info/media/576_296_" + src + "\")'></div>");
	} else if (type == "video") {
		var obj = "<video width='576' height='296' controls><source src=\"" + BASE_URL + "uploads/product_info/media/" + src + "\" type='video/mp4'>Your browser does not support the video tag.</video>";
		galleryCurrentPreview.html(obj);
	}
	if (id != currentGalleryItemId) {
		currentGalleryItemId = id;
	}

}).first().trigger("click");

</script>
