
<!-- main content starts here  -->
 <div class="wrapper dbam">
		
	<div class="container main">
		<div class="row">
			<?php $this->load->view('about/nav') ?>

			<div class="clearfix"></div>

			<div class="col-md-9 col-lg-10  col-md-push-3 col-lg-push-2 main-rail">
				<!-- content -->
				<div class="row gallery" id="dbam-gallery">
					<?php if (!$content): ?>
					<h3>No Record(s) Found</h3>
					<?php else: ?>
					<?php foreach ($content as $k => $c): ?>
					<div class="col-xs-6 col-sm-3 col-lg-2 item dbam-item" data-title="<?= $c['dbam_title'] ?>" data-is-video="<?= $c['dbam_is_video'] ?>" data-src="<?= BASE_URL ?>uploads/dbam/<?= $c['dbam_content'] ?>">
						<?php if ($c['dbam_thumbnail']): ?>
						<div class="thumbnail" style="background-image: url('<?= BASE_URL ?>uploads/dbam/150_150_<?= $c['dbam_thumbnail'] ?>')">
							<span><?=$c['dbam_title']?></span>
						</div>
						<?php if ($c['dbam_is_video']): ?>
						<div class="btn-play"> <i class="glyphicon glyphicon-play"></i> </div>
						<?php endif ?>
						<?php else: ?>
						<?php if ($c['dbam_is_video']): ?>
						<div class="thumbnail video" style="background-image: url('<?= BASE_URL ?>uploads/dbam/150_150_thumb_<?= $c['dbam_content'] ?>.jpg')">
							<span><?=$c['dbam_title']?></span>
							<sup><?= substr($c['dbam_duration'], 3)?></sup>
						</div>
						<div class="btn-play"> <i class="glyphicon glyphicon-play"></i> </div>
						<?php else: ?>
						<div class="thumbnail" style="background-image: url('<?= BASE_URL ?>uploads/dbam/150_150_<?= $c['dbam_content'] ?>')">
							<span><?=$c['dbam_title']?></span>
						</div>
						<?php endif ?>
						<?php endif ?>
					</div>
					<?php endforeach ?>
					<?php endif ?>
				</div>

				<br>
				<br>
				<!-- end content -->
			</div>
		</div> 	
	</div>

</div>

<div class="lytebox-wrap">
	<div class="lytebox-content-holder">
		<!-- per popup -->
		<div class="popup-view lytebox-wrapped-content" id="view-photo">
			<div class="content">
				<button class="popup-close">&times;<span>CLOSE</span></button>
				<h3 class="title" id="view-photo-title">ITEM TITLE</h3>

				<div class="media">
					<img id="dbam-preloader" style="margin-top: 6%;" src="<?= BASE_URL ?>images/preloader.gif">
					<img id="view-photo-content" src="<?= BASE_URL ?><?= DEFAULT_IMAGE ?>">
				</div>

			</div>
		</div>
		<!-- per popup -->
		<div class="popup-view lytebox-wrapped-content" id="view-video">
			<div class="content">
				<button class="popup-close">&times;<span>CLOSE</span></button>
				<h3 class="title" id="view-video-title">ITEM TITLE</h3>

				<div class="media">
					<video id="view-video-content" width="100%" type="video/mp4" height="100%" controls>
						Your browser does not support the video tag.
					</video>
				</div>

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

$("#dbam-gallery").on("click", ".dbam-item", function(e) {
	var mediatype = "photo";
	if ($(this).data("is-video")) {
		mediatype = "video";
		$("#view-" + mediatype + "-content").show();
	}
	$("#view-" + mediatype + "-title").html($(this).data("title"));
	
	if (mediatype == "photo") {
		$("#dbam-preloader").show();
		$("#view-" + mediatype + "-content").hide().prop("src", "").unbind("load")
			.bind("load", function () {
				setTimeout( function () {
					$("#dbam-preloader").fadeOut( function () {
						$("#view-" + mediatype + "-content").fadeIn();
					});
				}, 300);
			});
	}
	$("#view-" + mediatype + "-content").prop("src", "").prop("src", $(this).data("src"));
	
	popup.open({html : '#view-' + mediatype});
});

$(function(){
	squareThumb();
	$(window).resize(squareThumb);
});

function squareThumb(){

	$('.item').css({
		height : $('.item').css('width'),
		width : $('.item').css('width'),
	})
}
</script>

 