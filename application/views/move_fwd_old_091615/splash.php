<div class="lytebox-wrap">
	<div class="splash-popup lytebox-wrapped-content" id="move-fwd-splash">
		<div class="content txtcenter">
			<h1>MOVEFWD</h1>
			<h2>DECIDE TO DO IT. WE'LL MAKE IT HAPPEN.</h2>
			
			<?php /* <p>What's on your bucket list?<br>
			Learn a new skill? Perform in front of a crowd?<br>
			Let Marlboro help you tick items off it, 
			and get to savor once-in-a-lifetime experiences. 
			Right here, right now.</p> */ ?>
			<p>Decide to pursue Your Passions<br>
			and Savor once-in-a-lifetime experiences.</p>	
			<button class="button close-x"><i>CONTINUE</i></button>
		</div>
	</div>
</div>