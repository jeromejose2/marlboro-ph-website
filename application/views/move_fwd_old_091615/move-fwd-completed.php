<?php //print_r($challenges) ?>
<div class="popup lytebox-wrapped-content" style="display: block;">
	<div class="content txtcenter">
		<button class="popup-close">×<span>CLOSE</span></button>

		<h2><?php echo $challenges[0]['move_forward_title'] ?></h2>
		<!-- <h3>10 SLOTS LEFT</h3> -->
		
		<p class="small">Accomplished tasks for this offer</p>
		
		<ul class="challenge-task">
			<?php if($challenges) {
				foreach ($challenges as $key => $value) {
					echo '<li><i class="glyphicon glyphicon-ok"></i> ' . $value['challenge'] . '</li>';
				}
			} ?>
			
		</ul>
		<button onclick="popup.close()" class="button" type="button"><i>OK</i></button>
	</div>
</div>