<div class="wrapper">
	
	<div class="container main row move-fwd-challenge">

		<!-- side navigation -->
		<div class="col-md-3 col-lg-2  col-sm-12 side-rail">
			<a href="<?= BASE_URL ?>move_forward" class="back">BACK</a>
			<nav>
				<a href="<?= BASE_URL ?>move_forward">ACTIVE OFFERS</a>
				<a href="<?= BASE_URL ?>move_forward/past">PAST OFFERS</a>
				<?php if(isset($categories)): ?>
					<?php foreach ($categories as $category): ?>
					<a <?= $category['category_id'] == $offer['category_id'] ? 'class="active"' : '' ?> href="<?= BASE_URL ?>move_forward/category/<?= $category['category_id'] ?>">
						<?= $category['category_name'] ?>
					</a>
					<?php endforeach ?>
				<?php endif; ?>
			</nav>
		</div>
		<!-- end side navigation -->
		
		<div class="col-md-9 col-md-push-3 col-lg-push-2 main-rail">
			<div class="row">
				<div class="col-sm-4 col-md-5">
					<div class="thumb-holder show-gallery" id="move-fwd-show-gallery">
						<div class="thumbnail" style="background-image:url('<?= BASE_URL ?>uploads/move_fwd/main_<?= $offer['move_forward_image'] ?>')"></div>
						<h3 id="show-gallery-h3">GALLERY</h3>
					</div>
				</div>
				
				<div class="col-sm-8 col-md-7">
					<h2><?= $offer['move_forward_title'] ?></h2>
					<?= $offer['move_forward_description'] ?>
					
					<?php if (($chosen_action == 2 && $has_pledge) || ($challenges && $entries_count == $numOfTasks)): ?>
					<a href="javascript:void(0)" class="button"><i>YOU'RE AlREADY DONE</i></a>
					<?php elseif (($chosen_action == 2 && !$has_pledge) || ($challenges && $entries_count < $numOfTasks)): ?>
					<a href="javascript:void(0)" class="button" id="_takechallenge"><i>I WILL TAKE THE CHALLENGE</i></a>
					<?php endif ?>
					<a href="javascript:void(0)" class="button" onclick="popup.open({url:'<?= BASE_URL ?>move_forward/mechanics'})"><i>READ FULL MECHANICS</i></a>
				</div>
				
				<div class="clearfix"></div>

				<div class="col-sm-12 col-md-4 col-md-push-8">

					<div class="participants">
						<span id="participants-preloader">PARTICIPANTS</span>

						<div class="arrow left" id="participants-prev" data-page="0"></div>
						<div class="arrow right" id="participants-next"></div>

						<div class="thumbs" id="participants-thumblist">
							<!-- paricipants -->
						</div>
					</div>

				</div>
				<div class="col-sm-12 col-md-8 col-md-pull-4">
					<?= show_comments(MOVE_FWD, $offer['move_forward_id'], array(), array('category' => 'Comment', 'action' => 'submit', 'label' => "Commented on MoveFwd #{$offer['move_forward_id']}: {$offer['move_forward_title']}", 'value' => 1)) ?>
				</div>
			</div>
		</div>

	</div>

</div>

<!-- popups -->
<div class="lytebox-wrap">
	<div class="lytebox-content-holder">
		<?php if (!$chosen_action): ?>
		<!-- take challenge -->
		<div class="popup lytebox-wrapped-content" id="popup-task-challenge">
			<div class="content txtcenter">
				<button class="popup-close">&times;<span>CLOSE</span></button>

				<h2><?= $offer['move_forward_title'] ?></h2>
				<!-- <h3><?= $offer['slots'] ?> SLOTS LEFT</h3> -->
				
				<?php if(!$offer['is_premium_offer']) {?>
				<p class="small">Decide how you want to redeem this experience.</p>
				<div class="row small">
					<div class="col-sm-5">
						<button class="button" type="button" id="play-challenge-btn"><i>PLAY</i></button>

						<p>Accomplish the task to win the prize</p>

						<?php /* <p>Accomplish the task below to unlock the next task:

							<ul class="list-unstyled txtleft">
								<?php if (isset($challenges[0])): ?>
								<li><?= $challenges[0]['challenge'] ?></li>
								<?php endif ?>
							</ul>

						</p> */ ?>
					</div>

					<div class="col-sm-2"><h2 class="red">OR</h2></div>

					<div class="col-sm-5">
						<button class="button" type="button" id="pledge-challenge-btn"><i>PLEDGE</i></button>
						<p>Decide to pledge <?= number_format($offer['pledge_points']) ?> points</p>
					</div>
					
				</div>
				<?php } else {?>
				<div class="row small">
					<div class="">
						<button class="button" type="button" id="play-challenge-btn"><i>PLAY</i></button>

						<p>Accomplish the task below to unlock the next task:

							<ul class="list-unstyled">
								<?php if (isset($challenges[0])): ?>
								<li><?= $challenges[0]['challenge'] ?></li>
								<?php endif ?>
							</ul>

						</p>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
		<?php endif ?>

		
		<?php if ($challenges && $entries_count < $numOfTasks): ?>
		<!-- play challenge -->
		<div class="popup lytebox-wrapped-content" id="play-challenge">
			<div class="content txtcenter">
				<button class="popup-close">&times;<span>CLOSE</span></button>

				<h2><?= $offer['move_forward_title'] ?></h2>
				<!-- <h3><?= $offer['slots'] ?> SLOTS LEFT</h3> -->
				
				<p class="small">Want to win this experience? Just complete the task below.</p>
				<p class="small has-submitted" style="color: red; display: none">Please give us time to process and verify your submission.</p>
				
				<ul class="challenge-task">
				<?php $i = 1; foreach( $challenges as $k => $c ): ?>
					<?php if( isset($entries[$c['challenge_id']]) ): ?>
						<li><i class="glyphicon glyphicon-ok"></i> <?php echo $c['challenge'] ?></li>
					<?php else: ?>
						<li onclick="$(this).find('input[type=radio]').prop('checked', true)"><input type="radio" name="challenge_chosen" data-challenge-id="<?php echo $c['challenge_id'] ?>" value="<?php echo $k ?>" <?php echo $i == 1 ? 'checked' : '' ?> style="cursor: pointer"> <?php echo $c['challenge'] ?> </li>
						<?php $i++; ?>
					<?php endif; ?>
				<?php endforeach; ?>
				<?php /*
					$locked = false;
					foreach ($challenges as $k => $c): 
						if (!$locked): 
							if (isset($entries[$c['challenge_id']])): ?>
						<li><i class="glyphicon glyphicon-ok"></i> <?= $c['challenge'] ?></li>
						<?php else: $locked = true; ?>
						<li> <?= $c['challenge'] ?></li>
						<?php endif ?>
				<?php else: ?>
				<li class="locked"><i class="glyphicon glyphicon-lock"></i></li>
				<?php endif ?>
				<?php endforeach */ ?>
				</ul>

				<?php /* <div id="continue-buttons">
					<button type="button" class="button class-play-challenge-continue-btn" id="play-challenge-continue-btn"><i>CONTINUE</i></button>
					<button type="button" class="button play-challenge-later-btn" id="play-challenge-later-btn"><i>DO IT LATER</i></button>
				</div> */ ?>

				<?php if ( ! $has_pending): ?>
				<button type="button" class="button class-play-challenge-continue-btn" id="play-challenge-continue-btn"><i>CONTINUE</i></button>
				<button type="button" class="button play-challenge-later-btn" id="play-challenge-later-btn"><i>DO IT LATER</i></button>
				<?php else: ?>
				Please give us time to process and verify your last submission.
				<?php endif ?>

				<?php /* if (!$pending_entry): ?>
				<button type="button" class="button" id="play-challenge-continue-btn"><i>CONTINUE</i></button>
				<button type="button" class="button" id="play-challenge-later-btn"><i>DO IT LATER</i></button>
				<?php else: ?>
				Please give us time to process and verify your last submission.
				<?php endif */ ?>
			</div>
		</div>

		<!-- upload challenge -->
		<?php foreach($challenges as $k => $v): ?>
			<?php if($v['type'] == 'Photo'): ?>
				<div class="submit-challenge-entry-popup popup lytebox-wrapped-content challenge-popup-<?= $k ?>" id="upload-photo-challenge">
					<div class="content txtcenter">
						<button class="popup-close">&times;<span>CLOSE</span></button>

						<h2><?= $offer['move_forward_title'] ?></h2>
						<p><?= $v['challenge'] ?></p>

						<div class="browse">
							<input type="text" placeholder="Browse for File (Max file size: 3MB)" readonly>
						</div>

						
						<div class="upload-preview-fixed preview-<?php echo $k ?>"><span>PHOTO PREVIEW</span></div>
						<form id="move-fwd-photo-upload-<?php echo $k ?>" method="POST" enctype="multipart/form-data" action="<?= BASE_URL ?>move_forward/upload" target="move-forward-upload">
							<input type="file" name="move_fwd_upload" id="upload-task-<?php echo $k ?>" class="hidden upload-task"  onchange="PreviewImage(<?php echo $k ?>);">
							<input type="hidden" name="challenge_id" value="<?= $v['challenge_id'] ?>">
							<input type="hidden" name="type" value="1">
							<div class="error" id="error-photo-preview" style="display: none; text-align: left"></div>
							<textarea name="description" rows="5" cols="83" placeholder="Description" id="description-photo"></textarea>
						</form>

						<button type="button" class="button" data-target="<?php echo $k ?>" id="move-fwd-photo-upload-submit"><i>UPLOAD</i></button>
						<p class="terms">Please be reminded that as per the <a href="#" onclick="popup.open({url:'<?=BASE_URL?>/popups/terms.html', onClose: function(){  popup.open({html:'#upload-photo-challenge'});} })">Terms and Conditions</a>, all photos, images or designs submitted will become the property of PM. Please do not upload or submit any photos, images or designs depicting the act of smoking or showing a lit cigarette or minors (persons less than 18 years old). PM reserves to the right to remove or reject any photo, image or design which it, in its sole and absolute discretion, deems offensive, violates the law or is unsuitable in any other manner. The photo you upload will be subject to moderation.</p>
						<!-- <p class="terms">Please be reminded that as per the <a href="#" onclick="popup.open({url:'<?=BASE_URL?>/popups/terms.html', onClose: function(){  popup.open({html:'#upload-photo-challenge'});} })">Terms and Conditions</a>, all photos, images or designs submitted will become the property of PM. 
							Please do not upload or submit any photos, images or designs depicting the act of smoking or showing a lit cigarette or minors 
							(persons less than 18 years old). PM reserves to the right to remove or reject any photo, image or design which it, in its 
							sole and absolute discretion, deems offensive, violates the law or is unsuitable in any other manner. The photo you upload 
							will be subject to moderation.</p> -->

					</div>
				</div>
			<?php elseif($v['type'] == 'Video'): ?>
				<div class="submit-challenge-entry-popup popup lytebox-wrapped-content challenge-popup-<?= $k ?>" id="upload-video-challenge">
					<div class="content">
						<button class="popup-close">&times;<span>CLOSE</span></button>
						
						<h2><?= $offer['move_forward_title'] ?></h2>
						<p><?= $v['challenge'] ?></p>

						<!-- <p>You can upload a video in four easy steps:
							<ol class="small">
								<li>Log on to Youtube and click "Upload".</li>
								<li>Before uploading the video, be sure the privacy is set to "unlisted" (Remember: If your video is not unlisted, we cannot approve it).</li>
								<li>Make sure your video's name is the same as the MoveFWD offer, and start uploading.</li>
								<li>Paste your video URL to the MoveFWD section of Marlboro.ph.</li>
							</ol>
						</p> -->
						<div class="col-md-4 col-sm-4 infographic">
							<img src="<?=base_url()?>/images/info-1.png" alt="infographic"/>
						</div>
						<div class="col-md-4 col-sm-4 infographic">
							<img src="<?=base_url()?>/images/info-2.png" alt="infographic"/>
						</div>
						<div class="col-md-4 col-sm-4 infographic">
							<img src="<?=base_url()?>/images/info-3.png" alt="infographic"/>
						</div><div class="clearfix"></div><br/>
						<div class="browse">
							<form id="move-fwd-video-upload" method="POST" action="<?= BASE_URL ?>move_forward/upload" target="move-forward-upload">
								<input name="video" type="text" id="video-url" placeholder="Youtube URL">
								<input type="hidden" name="type" value="2">
								<input type="hidden" name="challenge_id" value="<?= $v['challenge_id'] ?>">
							<!-- </form> -->
						</div>

						<div class="error" id="error-video-preview" style="display: none"></div>
						<div class="upload-preview-fixed">
							<span>VIDEO PREVIEW</span>
						</div>
								<textarea name="description" rows="5" cols="83" placeholder="Description" id="description-video"></textarea>
							</form>


						<div class="txtcenter">
							<button type="button" class="button" id="move-fwd-video-upload-submit"><i>SAVE</i></button>
						</div>
						<p class="terms">Please be reminded that as per the <a href="#" onclick="popup.open({url:'<?=BASE_URL?>/popups/terms.html', onClose: function(){  popup.open({html:'#upload-photo-challenge'});} })">Terms and Conditions</a>, all photos, images or designs submitted will become the property of PM. Please do not upload or submit any photos, images or designs depicting the act of smoking or showing a lit cigarette or minors (persons less than 18 years old). PM reserves to the right to remove or reject any photo, image or design which it, in its sole and absolute discretion, deems offensive, violates the law or is unsuitable in any other manner. The photo you upload will be subject to moderation.</p>
					</div>
				</div>
			<?php else: ?>
				<div class="submit-challenge-entry-popup popup lytebox-wrapped-content challenge-popup-<?= $k ?>" id="upload-video-challenge">
					<div class="content txtcenter">
						<button class="popup-close">&times;<span>CLOSE</span></button>
						
						<h2><?= $offer['move_forward_title'] ?></h2>
						<p><?= $v['challenge'] ?></p>

						<div class="browse">
							<form id="move-fwd-text-upload" method="POST" action="<?= BASE_URL ?>move_forward/upload" target="move-forward-upload">
								<textarea name="text" id="text"></textarea>
								<input type="hidden" name="type" value="3">
								<input type="hidden" name="challenge_id" value="<?= $v['challenge_id'] ?>">

						</div><br>
								<textarea name="description" rows="5" cols="83" placeholder="Description" id="description-text"></textarea>
							</form>
						
						<div class="error" id="error-text" style="display:none;"></div>
						<button type="button" class="button" id="move-fwd-text-submit"><i>SAVE</i></button>
						<p class="terms">Please be reminded that as per the <a href="#" onclick="popup.open({url:'<?=BASE_URL?>/popups/terms.html', onClose: function(){  popup.open({html:'#upload-photo-challenge'});} })">Terms and Conditions</a>, all photos, images or designs submitted will become the property of PM. Please do not upload or submit any photos, images or designs depicting the act of smoking or showing a lit cigarette or minors (persons less than 18 years old). PM reserves to the right to remove or reject any photo, image or design which it, in its sole and absolute discretion, deems offensive, violates the law or is unsuitable in any other manner. The photo you upload will be subject to moderation.</p>
					</div>
				</div>
			<?php endif; ?>
			<iframe name="move-forward-upload" id="move-forward-upload" style="border: 0; height: 0; width: 0;"></iframe>

		<?php endforeach; ?>

		<?php /* if (!$pending_entry): ?>
			<?php if ($current['type'] == 'Photo'): ?>
			<div class="submit-challenge-entry-popup popup lytebox-wrapped-content" id="upload-photo-challenge">
				<div class="content txtcenter">
					<button class="popup-close">&times;<span>CLOSE</span></button>

					<h2><?= $offer['move_forward_title'] ?></h2>
					<p><?= $current['challenge'] ?></p>

					<div class="browse">
						<input type="text" placeholder="Browse for File (Max file size: 3MB)" readonly>
					</div>
					
					<div class="upload-preview-fixed"><span>PHOTO PREVIEW</span></div>
					<form id="move-fwd-photo-upload" method="POST" enctype="multipart/form-data" action="<?= BASE_URL ?>move_forward/upload" target="move-forward-upload">
						<input type="file" name="move_fwd_upload" id="upload-task" class="hidden"  onchange="PreviewImage();">
						<input type="hidden" name="challenge_id" value="<?= $current['challenge_id'] ?>">
						<input type="hidden" name="type" value="1">
					</form>

					<button type="button" class="button" id="move-fwd-photo-upload-submit"><i>UPLOAD</i></button>
					<p class="terms">Please be reminded that as per the <a href="#" onclick="popup.open({url:'<?=BASE_URL?>/popups/terms.html', onClose: function(){  popup.open({html:'#upload-photo-challenge'});} })">Terms and Conditions</a>, all photos, images or designs submitted will become the property of PM. Please do not upload or submit any photos, images or designs depicting the act of smoking or showing a lit cigarette or minors (persons less than 18 years old). PM reserves to the right to remove or reject any photo, image or design which it, in its sole and absolute discretion, deems offensive, violates the law or is unsuitable in any other manner. The photo you upload will be subject to moderation.</p>
					<!-- <p class="terms">Please be reminded that as per the <a href="#" onclick="popup.open({url:'<?=BASE_URL?>/popups/terms.html', onClose: function(){  popup.open({html:'#upload-photo-challenge'});} })">Terms and Conditions</a>, all photos, images or designs submitted will become the property of PM. 
						Please do not upload or submit any photos, images or designs depicting the act of smoking or showing a lit cigarette or minors 
						(persons less than 18 years old). PM reserves to the right to remove or reject any photo, image or design which it, in its 
						sole and absolute discretion, deems offensive, violates the law or is unsuitable in any other manner. The photo you upload 
						will be subject to moderation.</p> -->

				</div>
			</div>
			<?php elseif ($current['type'] == 'Video'): ?>
			<!-- upload challenge video -->
			<div class="submit-challenge-entry-popup popup lytebox-wrapped-content" id="upload-video-challenge">
				<div class="content">
					<button class="popup-close">&times;<span>CLOSE</span></button>
					
					<h2><?= $offer['move_forward_title'] ?></h2>
					<p><?= $current['challenge'] ?></p>

					<!-- <p>You can upload a video in four easy steps:
						<ol class="small">
							<li>Log on to Youtube and click "Upload".</li>
							<li>Before uploading the video, be sure the privacy is set to "unlisted" (Remember: If your video is not unlisted, we cannot approve it).</li>
							<li>Make sure your video's name is the same as the MoveFWD offer, and start uploading.</li>
							<li>Paste your video URL to the MoveFWD section of Marlboro.ph.</li>
						</ol>
					</p> -->
					<div class="col-md-4 col-sm-4 infographic">
						<img src="<?=base_url()?>/images/info-1.png" alt="infographic"/>
					</div>
					<div class="col-md-4 col-sm-4 infographic">
						<img src="<?=base_url()?>/images/info-2.png" alt="infographic"/>
					</div>
					<div class="col-md-4 col-sm-4 infographic">
						<img src="<?=base_url()?>/images/info-3.png" alt="infographic"/>
					</div><div class="clearfix"></div><br/>
					<div class="browse">
						<form id="move-fwd-video-upload" method="POST" action="<?= BASE_URL ?>move_forward/upload" target="move-forward-upload">
							<input name="video" type="text" id="video-url" placeholder="Youtube URL">
							<input type="hidden" name="type" value="2">
							<input type="hidden" name="challenge_id" value="<?= $current['challenge_id'] ?>">
						</form>
					</div>
					<div class="error" id="error-video-preview" style="display:none;"></div>
					<div class="upload-preview-fixed">
						<span>VIDEO PREVIEW</span>
					</div>

					<div class="txtcenter">
						<button type="button" class="button" id="move-fwd-video-upload-submit"><i>SAVE</i></button>
					</div>
					<p class="terms">Please be reminded that as per the <a href="#" onclick="popup.open({url:'<?=BASE_URL?>/popups/terms.html', onClose: function(){  popup.open({html:'#upload-photo-challenge'});} })">Terms and Conditions</a>, all photos, images or designs submitted will become the property of PM. Please do not upload or submit any photos, images or designs depicting the act of smoking or showing a lit cigarette or minors (persons less than 18 years old). PM reserves to the right to remove or reject any photo, image or design which it, in its sole and absolute discretion, deems offensive, violates the law or is unsuitable in any other manner. The photo you upload will be subject to moderation.</p>
				</div>
			</div>
			<?php else: ?>
			<!-- upload challenge video -->
			<div class="submit-challenge-entry-popup popup lytebox-wrapped-content" id="upload-video-challenge">
				<div class="content txtcenter">
					<button class="popup-close">&times;<span>CLOSE</span></button>
					
					<h2><?= $offer['move_forward_title'] ?></h2>
					<p><?= $current['challenge'] ?></p>

					<div class="browse">
						<form id="move-fwd-text-upload" method="POST" action="<?= BASE_URL ?>move_forward/upload" target="move-forward-upload">
							<textarea name="text" id="text"></textarea>
							<input type="hidden" name="type" value="3">
							<input type="hidden" name="challenge_id" value="<?= $current['challenge_id'] ?>">
						</form>
					</div><br>
					<div class="error" id="error-text" style="display:none;"></div>
					<button type="button" class="button" id="move-fwd-text-submit"><i>SAVE</i></button>
					<p class="terms">Please be reminded that as per the <a href="#" onclick="popup.open({url:'<?=BASE_URL?>/popups/terms.html', onClose: function(){  popup.open({html:'#upload-photo-challenge'});} })">Terms and Conditions</a>, all photos, images or designs submitted will become the property of PM. Please do not upload or submit any photos, images or designs depicting the act of smoking or showing a lit cigarette or minors (persons less than 18 years old). PM reserves to the right to remove or reject any photo, image or design which it, in its sole and absolute discretion, deems offensive, violates the law or is unsuitable in any other manner. The photo you upload will be subject to moderation.</p>
				</div>
			</div>
			<?php endif ?>
			<iframe name="move-forward-upload" id="move-forward-upload" style="border: 0; height: 0; width: 0;"></iframe>
		<?php endif */ ?>
		<?php endif ?>

		<!-- upload challenge -->
		<div class="popup lytebox-wrapped-content" id="pledge-challenge">
			<div class="content txtcenter">
				<button class="popup-close">&times;<span>CLOSE</span></button>

				<h2>PLEDGE <?= number_format($offer['pledge_points']) ?> POINTS</h2>
				<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p> -->
				<br>
				
				<button class="button" type="button" id="pledge-challenge-continue"><i>PLEDGE</i></button>

			</div>
		</div>

		<div class="popup-gallery lytebox-wrapped-content" id="move-fwd-gallery-popup"></div>

	</div>
</div>

<script type="text/javascript">
	var MoveFwd = {
		PLAY : <?= MOVE_FWD_PLAY ?>,
		PLEDGE : <?= MOVE_FWD_PLEDGE ?>,
		choice : <?= (int) $chosen_action ?>,
		currentOffer : <?= $offer['move_forward_id'] ?>,
		currentOfferName : "<?= $offer['move_forward_title'] ?>",
		alreadyDone : <?= (int) ($entries == $numOfTasks) ?>,
		slots : <?= $offer['slots'] ?>		
	};

	// var entry_ids = <?php echo isset($entries_ids) ? $entries_ids : [] ?>;

	$('#pledge-challenge-continue').click(function() {
		$.ajax({
			beforeSend: function(){
		        $('.confirm-button').attr('disabled','disabled').html('<i><img src="<?=BASE_URL?>images/spinner.gif" /> CONFIRMING...</i>');
		    }
		})
	});
</script>
<script type="text/javascript" src="<?= BASE_URL ?>scripts/move-forward.js?a=<?php echo rand() ?>"></script>