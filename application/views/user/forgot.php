<div class="wrapper registration">
	<div class="container">
		<div class="leather-content">
			<form autocomplete="off" class="content" onsubmit="return validate(this)" action="<?= BASE_URL ?>user/forgot_submit" method="POST">
				<h2>FORGOT PASSWORD</h2>
				<p class="uc">Enter your e-mail to reset your password</p>
				<div id="error"><?= $invalid_forgot_email ? '<span>'.$invalid_forgot_email.'</span>' : '' ?></div>
				<input autocomplete="off" type="text" name="email" placeholder="Email Address" class="req">
				<br>
				<a href="<?= BASE_URL ?>user/login" class="small">Back to login page</a> <br><br>
				<button type="submit" class="button"><i>SUBMIT</i></button>
			</div>
		</div>
	</div>
	<br class="clearfix">
</div>
<script type="text/javascript">
function validate(form){
	var error = false;
	$('.req',form).each(function(){
		var el = $(this);
		var val = $.trim(el.val());
		if( val == '' ){
			error = 'Please input your e-mail address.';
			return false;
		}
	});
	if( error ){
		$('#error').html('<span>'+error+'</span>');
		$('html, body').animate({scrollTop:0})
		return false;
	}
}
</script>