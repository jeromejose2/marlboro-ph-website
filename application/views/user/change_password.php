<div class="wrapper registration">
    <div class="container">
        <div class="leather-content">
            <form autocomplete="off" class="content" onsubmit="return validate_cp(this)" action="<?= BASE_URL ?>user/reset_password?token=<?=$this->input->get('token')?>" method="POST">
                <h2>RESET PASSWORD</h2>
                <p class="uc">Enter your password</p>
                <div id="error"><?= $confirm_password ? '<span>'.$confirm_password.'</span>' : '' ?></div>
                <input autocomplete="off" type="password" name="new_password" placeholder="Password" class="req">
                <input autocomplete="off" type="password" name="confirm_password" placeholder="Confirm Password" class="req">
                
                <br>
                <button type="submit" class="button"><i>SUBMIT</i></button>
            </div>
        </div>
    </div>
    <br class="clearfix">
</div>