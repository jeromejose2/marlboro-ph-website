<div class="wrapper registration ">
	<div class="container">
	  <div class="login-holder-outer">
	    <div class="login-holder">
			<p>Email has been sent for your GIID upload. Please don’t forget to submit a copy of a valid government-issued ID within seven (7) days. Your registration will only be processed upon submission of a valid government-issued ID.</p>
	    </div>
	  </div>
	</div>
</div>

<!-- 
<div class="popup lytebox-wrapped-content" id="popup3">
	<div class="content txtcenter">
		<button type="button" class="popup-close">&times;<span>CLOSE</span></button>
		<br>
 		<p>Email has been sent for your GIID upload. Please don’t forget to submit a copy of a valid government-issued ID within seven (7) days. Your registration will only be processed upon submission of a valid government-issued ID.<p>
		<button class="button" type="button" onclick="closePopup('user/login');"><i>OK</i></button>
	</div>
</div> -->


<?php /* if($success_message){ ?>

	popup.open({html: "#popup3", onClose: function() {
					window.location = BASE_URL + "user/login";
				}});

<?php }*/ ?>

