<!DOCTYPE html>
<html lang="en">
<head>
	<title>Marlboro</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<noscript>
		<meta http-equiv="refresh" content="0;<?= BASE_URL ?>noscript">
	</noscript>
	<link rel="stylesheet" href="<?= BASE_URL ?>styles/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>plugins/jquery-ui/css/smoothness/jquery-ui-1.10.0.custom.min.css">
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>styles/lytebox.css">
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>styles/mainstyle.css">
	<link rel="apple-touch-icon" sizes="57x57" href="<?= BASE_URL ?>images/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?= BASE_URL ?>images/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?= BASE_URL ?>images/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?= BASE_URL ?>images/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?= BASE_URL ?>images/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?= BASE_URL ?>images/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?= BASE_URL ?>images/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?= BASE_URL ?>images/favicon/apple-touch-icon-152x152.png">
	<link rel="icon" type="image/png" href="<?= BASE_URL ?>images/favicon/favicon-196x196.png" sizes="196x196">
	<link rel="icon" type="image/png" href="<?= BASE_URL ?>images/favicon/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="<?= BASE_URL ?>images/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?= BASE_URL ?>images/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?= BASE_URL ?>images/favicon/favicon-16x16.png" sizes="16x16">
	<meta name="msapplication-TileColor" content="#ab0702">
	<meta name="msapplication-TileImage" content="<?= BASE_URL ?>images/favicon/mstile-144x144.png">
	<script type="text/javascript" src="<?= BASE_URL ?>scripts/jquery.min.js"></script>
	<script>
		var MINIMUM_AGE = "<?= MINIMUM_AGE ?>";
		var BASE_URL = "<?= BASE_URL ?>";
		var PROD_MODE = <?= (int) PROD_MODE ?>;
		<?php if (!DEV_MODE): ?>
		$(document).bind("contextmenu", function() {
			return false;
		});
		<?php endif ?>
	</script>
</head>
<body>
		<header class="main">
			<nav class="login">
				<a href="<?= BASE_URL ?>user/login" <?= $this->uri->segment(2) == 'login' ? ' class="active"' : '' ?>>LOGIN</a>
				<a href="<?= BASE_URL ?>user/register" <?= $this->uri->segment(2) == 'register' ? ' class="active"' : '' ?>>REGISTER</a>
			</nav>
			<div class="grunge-foreground"></div>
		</header>

		<section class="main">
			<?= $main_content ?>
		</section>

		<footer class="main">
			<div class="r18">
				This website is for adult smokers 18 years or older residing in the Philippines. Access to this website is subject to age verification
			</div>

			<div class="row">
				<nav class="col-md-6 col-sm-8">
					<a onclick="popup.open({url:'<?= BASE_URL ?>popups/terms.html'})">terms and conditions</a>
				 	<a onclick="popup.open({url:'<?= BASE_URL ?>popups/privacy-content.html'})">Privacy Statement and Consent</a> 
				 	<a onclick="popup.open({url:'<?= BASE_URL ?>popups/contactus.html'})">contact us</a>
				 	<a href="http://www.pmi.com/eng/our_products/health_effects_of_smoking/pages/health_effects_of_smoking.aspx" target="_blank">smoking and health</a>
				</nav>

				<div class="col-md-6 col-sm-4 copyright">copyright &copy; 2013 PMFTC INC. All rights reserved</div>
			</div>
			<div class="government-warning">
				<span>GOVERNMENT WARNING: CIGARETTE SMOKING IS DANGEROUS TO YOUR HEALTH</span>
			</div>
		</footer>


<!-- Latest compiled and minified JavaScript -->
<script src="<?= BASE_URL ?>plugins/jquery-ui/js/jquery-ui-1.10.0.custom.min.js"></script>
<script src="<?= BASE_URL ?>scripts/bootstrap.min.js"></script>
<script src="<?= BASE_URL ?>scripts/lytebox.2.3.js"></script>
<script src="<?= BASE_URL ?>scripts/exec.js"></script>
<script src="<?= BASE_URL ?>scripts/owl.carousel.min.js"></script>
<script src="<?= BASE_URL ?>scripts/registration.js"></script>
<script src="<?= BASE_URL ?>scripts/swfobject.js"></script>
<script src="<?= BASE_URL ?>swf/registration-webcam.js"></script>
<link type="text/css" href="<?= BASE_URL ?>styles/jquery.jscrollpane.css" rel="stylesheet" media="all" />
<script src="<?= BASE_URL ?>scripts/jquery.mousewheel.js"></script>
<script src="<?= BASE_URL ?>scripts/jquery.jscrollpane.min.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="<?= BASE_URL ?>scripts/shiv.js"></script>
  <script src="<?= BASE_URL ?>scripts/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
	function validate(form){
		var error = false;
		$('.req',form).each(function(){
			var el = $(this);
			var val = $.trim(el.val());
			var typ = el.attr('type');
			var name = el.attr('name');
			if( val == '' ){
				error = 'Please input your '+name;
				return false;
			}

			if( typ == 'checkbox'){
				if( !el.is(':checked') ){
					error = 'You need to tick both statements in order to log in';
					return false;
				}
			}
		});
		if( error ){
			$('#error').html('<span>'+error+'</span>');
			$('html, body').animate({scrollTop:0})
			return false;
		}
	}

	function validate_cp(form){
		var error = false;

		$('.req',form).each(function(){
			var el = $(this);
			var val = $.trim(el.val());
			var np = $('input[name=new_password]').val();
			var cp = $('input[name=confirm_password]').val();
			var firstname = '<?php echo $user->PersonDetails->FirstName ?>';
			var lastname = '<?php echo $user->PersonDetails->LastName ?>';
			
			if( (np && cp) == '' ){
				if(np == '')
					error = 'Please input your new password';
				else if(cp == '')
					error = 'Please input the confirm password field';
				return false;
			}else if(np != cp){
				error = 'Password did not match';
				return false;
			}else if(np.length <= 7){
				error = 'Password should be at least 8 characters';
				return false;
			}

			if(!np.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/)){
				error = ' Your password must have any of the following: Uppercase, Numerical, or Special characters.';
				return false;
			}

			if(np.match(new RegExp(firstname+'|'+lastname+'|user|guest|admin|sys|test|pass|super', 'gi'))){
				error = 'Your password must not contain the following: "user", "guest", "admin", "sys", "test", "pass", "super", your first or last name.';
				return false;
			}
		});

		if( error ){
			$('#error').html('<span>'+error+'</span>');
			$('html, body').animate({scrollTop:0})
			return false;
		}
	}



</script>

<?php if (PROD_MODE): ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-42684485-7', 'marlboro.ph');
  ga('send', 'pageview');
</script>
<?php endif ?>
</body>
</html>
