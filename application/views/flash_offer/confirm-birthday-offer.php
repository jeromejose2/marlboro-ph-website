<div class="popup">
	<div class="content">
		<button class="popup-close">&times;<span>CLOSE</span></button>
		
		<h2 class="txtcenter">UPDATE OR CONFIRM YOUR MAILING ADDRESS</h2>
		<div class="error _error" style="text-align: left; display: none"></div>
		<form action="<?=BASE_URL?>flash_offer/confirm_offer" method="post"  target="upload_target" onsubmit="return onSubmitForm();">
		<input type="hidden" name="prize_id">
		<input type="hidden" name="type" value="flash">
		<div class="row  confirm-email">
			<div class="col-sm-6">
				<h3>STREET NAME</h3>
				<input class="_required" alt="Street Name" type="text" name="street_name" value="<?=$spice_userinfos->Addresses[0]->AddressLine1?>">
			</div>

			<div class="col-sm-6">
				<h3>BARANGAY</h3>
				<input class="_required" alt="Barangay" type="text" name="barangay" value="<?=$spice_userinfos->Addresses[0]->Area?>">
			</div>

			<div class="col-sm-6">
				<h3>PROVINCE</h3>
				<select class="_required" alt="Province" name="province">
					<option value="">Select</option>
 					<?php  if($provinces->num_rows()){
								foreach($provinces->result() as $v){ 
										if(strtolower($spice_userinfos->Addresses[0]->Locality) === strtolower($v->province)){?>
											<option selected="selected" value="<?=$v->province_id?>"><?=$spice_userinfos->Addresses[0]->Locality?></option>
								<?php	}else{ ?>
											<option value="<?=$v->province_id?>"><?=$v->province?></option>
								  <?php }
								}
							}  ?>
				</select>
			</div>

			<div class="col-sm-6">
				<h3>CITY/TOWN</h3>
				<select class="_required" alt="City" name="city">
					<option value="">Select</option>
 					<?php if($cities){
							foreach($cities->result() as $c){ 
									if(strtolower($spice_userinfos->Addresses[0]->City)===strtolower($c->city)){?>
										<option selected="selected" value="<?=$c->city_id?>"><?=$spice_userinfos->Addresses[0]->City?></option>
							<?php	}else{ ?>
										<option value="<?=$c->city_id?>"><?=$c->city?></option>
							  <?php }
							}
						}  ?>
				</select>
			</div>

			

			<div class="col-sm-6">
				<h3>ZIP CODE:</h3>
				<input class="_required _numbers-only" alt="Zip Code" type="text" name="zip_code" value="<?=$spice_userinfos->Addresses[0]->PostalCode?>" maxlength="4">
			</div>
		</div>

		<div class="txtcenter">
			<div class="error" id="flash-offer-error"></div>
			<button class="button"  type="submit" id="flash-offer-btn"><i>Submit Confirmation</i></button>
		</div>

		</form>
	</div>
</div>
<iframe id="upload_target" name="upload_target" style="width:0px;height:0px;border:none;"></iframe>
<script type="text/javascript"> 

$("._numbers-only").keypress(validateNumber);

$(function(){

	$('select[name="province"]').change(function(){
		var province = $(this).val();

		$.getJSON(BASE_URL+'profile/get_cities?province='+province,function(cities){
			var items = ['<option value=""></option>'];
 
			 $.each(cities, function(i,obj) {
 				items.push('<option value="'+obj.city_id+'">'+obj.city+'</option>');
			});
			 $('select[name="city"]').html(items.join(''));
		});
	})

});



function onSubmitForm()
{
	var error = false;
	var numbers = /^\d+$/;
	var error_container = $("._error");
	$("._required").each(function() {
		var elem = $(this);
		var value = $.trim($(this).val());
		var name = $(this).attr('alt');

		if( ! value) {
			error = '<span>'+ name +' is required.</span>';
			elem.focus();
			return false;
		} else if(elem.attr('name') == 'zip_code') {
			if(value.length < 4) {
				elem.focus();
				error = '<span>'+ name +' must be valid.</span>';
				return false;
			} else if( ! numbers.test(value)) {
				elem.focus();
				error = '<span>'+ name +' should be a number.</span>';
				return false;
			}
		}
	});

	if(error) {
		error_container.show().html(error);
		return false;
	}
	error_container.css('display', 'none');

	document.getElementById('flash-offer-btn').disabled = true;
	$('#flash-offer-btn').attr('disabled','disabled').html('<i><img src="<?=BASE_URL?>images/spinner.gif" /> SUBMITTING...</i>');
	return true;
}

function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;

    if (event.keyCode == 8 || event.keyCode == 46
     || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 9) {
        return true;
    }
    else if ( key < 48 || key > 57) {
        return false;
    }
    else return true;
}

function form_response(msg)
{
 	popup.open({type:'alert','message':msg, buttonAlign:'center',align:'center',okCaption:'CONTINUE'});
}

</script>