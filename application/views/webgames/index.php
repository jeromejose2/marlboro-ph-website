<div class="wrapper">
	<div class="container webgames">

		<div class="row main txtcenter">

			<?php if($games): foreach($games as $k => $v): ?>
			<!-- per item -->
			<div class="col-sm-6 col-md-3 item">
				<a href="games/<?php echo $v['slug'] ?>">
					<div class="thumb-holder">
						<div class="thumbnail" style="background-image:url('<?php echo BASE_URL . 'uploads/game/400_400_' . $v['image'] ?>')"></div>
					</div>
					<h2><?php echo $v['name'] ?></h2></a>
					<!-- <p><?php echo $v['description'] ?></p> -->
					<a href="games/<?php echo $v['slug'] ?>" class="button"><i>PLAY NOW</i></a>
				<div class="clearfix"></div>
			</div>
			<!-- per item -->
			<?php endforeach; endif; ?>
		</div>
		<br class="clearfix">
	</div>
</div>