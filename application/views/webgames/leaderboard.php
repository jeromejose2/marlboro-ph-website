<div class="popup" id="leadboard" style="display:block;">
	<div class="content txtcenter">
		<button class="popup-close">&times; <span>CLOSE</span></button>

		<h2><?php echo $this->input->get('name') ?> LEADERBOARD</h2>
		<h3><?php echo date('l, F j, Y')?></h3>

		<div class="scrollable-small">
			<ol class="leaderboard">
			<?php if($top_scorers): foreach ($top_scorers as $k => $v): ?>
				<li><?php echo $v['nick_name'] ?> <span><?php echo $v['score']; ?></span></li>
			<?php endforeach; endif; ?>
			</ol>
		</div>

		<button type="button" class="button close-x"><i>PLAY</i></button>
		<button type="button" class="button" onclick="popup.open({html:'#howto'})"><i>HOW TO PLAY</i></button>
	</div>
</div>