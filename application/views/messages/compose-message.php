<div class="popup">
	<div class="content">

		<form action="<?=BASE_URL.'messages/compose'?>" method="post"  target="submit_target" id="send-message-form">

			<button class="popup-close" id="send-message-form-close-btn">&times;<span>CLOSE</span></button>		
			<h2>COMPOSE MESSAGE</h2>


			<div class="message-compose">
				<div class="col-sm-12">
					<h3>RECIPIENT</h3>

					<?php if($registrant){ ?>
						<input type="hidden" name="recipient_id" value="<?=$registrant->registrant_id?>">
						<input type="hidden" name="move_forward_id" value="<?=$move_forward_id?>">
						<input type="text" value="<?=$registrant->nick_name?>" readonly>
					<?php }else{ ?>
						<input type="text" value="Participant not found" read-only>
					<?php } ?>
					
				</div>

				<div class="col-sm-12">
					<h3>SUBJECT</h3>
					<input type="text" name="subject" maxlength="200">
				</div>

				<div class="col-sm-12">
					<h3>MESSAGE</h3>
					<textarea name="message"></textarea>
				</div>

				<div class="col-sm-12">
					<div class="error" id="send-message-form-error-holder"></div>
				</div>
			</div>

			<div class="txtcenter">
				
				<button class="button" type="submit" id="send-message-form-button"><i>SEND</i></button>
			</div>

		</form>

	</div>
</div>
<iframe id="submit_target" name="submit_target"  style="width:0px;height:0px;border:none;"></iframe>
<script type="text/javascript">

$('#send-message-form').on('submit',function(){
	$('#send-message-form-button').html('<i><img src="<?=BASE_URL?>images/spinner.gif"/> SENDING...</li>');
	return true;
});

function form_response(msg){
 	popup.open({type:'alert','message':msg,buttonAlign:'center',align:'center',okCaption:'CONTINUE'});
}
</script>