
<form action="<?=BASE_URL?>messages/delete_messages" method="POST" id="inbox-form2">
	<input type="hidden" name="messages" value="{{message}}">
	<ul class="inbox-message">
		<li>
			<span class="pull-left"> <a class="button-small visible-xs" href="messages.php"><i>BACK TO INBOX</i></a> </span>
			 
			<div class="clearfix"></div>
		</li>

		<li class="head">
			<h1>{{subject}}</h1>

			{{# if subject}}
			<small>{{date_subject_sent}}</small>
			{{else}}
			<small>No message(s) found.</small>
			{{/if}}
		</li>
		{{#each content}}
		<li>
			<span class="name">{{sender_name}}</span>
			<span class="date">{{date_message_sent}}</span>
			<span class="msg">{{message}}</span>					
		</li>
		{{/each}}
		
		{{# if content}}
		<li>
			<textarea name="reply-message" class="reply" placeholder="Your message here"></textarea>
			<div class="pull-right"><button type="button" class="button-small" id="reply-btn" onclick="reply();"><i>REPLY</i></button></div>

			<div class="clearfix"></div>
		</li>
		{{else}}
		<li>No message(s) found.</li>
		{{/if}}
	</ul>
</form> 
<script>
function deleteMessages()
{
 	    popup.open({type: 'confirm',message:'<h3>Are you sure you want to delete these messages?</h3>',onConfirm:function(){ jQuery('#inbox-form2').trigger('submit'); } });			
}
</script>
 