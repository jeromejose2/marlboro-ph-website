<div class="wrapper ">
		
			<div class="row container messages">
				<?php $this->load->view('messages/nav') ?>
				<div class="col-md-8 col-md-push-3 inbox-holder">
					<form action="<?=BASE_URL?>messages/delete_messages" method="POST" id="inbox-form">
						<ul class="inbox">
							
							<?php if($rows){ ?>
							<li>
								<span class="pull-right"> <button class="button-small" type="button" id="delete-message-btn" ><i>DELETE SELECTED</i></button> </span>
								<div class="clearfix"></div>
							</li>
							<?php foreach($rows as $v){?>
									<li <?=!$v->status ? 'class="unread"' : '' ?>>
										<input type="checkbox" name="messages[]" value="<?=$v->message_id?>">
										<a href="<?=BASE_URL.'messages/inbox/'.$v->message_id.'/'.url_title($v->subject)?>">											
											<span class="name"><?=$v->sender_name?></span>
											<span class="date"><?=date('F d, Y',strtotime($v->date_sent)).' at '.date('h:i a',strtotime($v->date_sent))?></span>
											<span class="subject"><?=$v->subject?></span>
											<span class="msg"><?=$v->message?></span>
										</a>
									</li>
							<?php }
	 						 }else{ ?>
	 						 <li style="border-bottom: 0;"><h3>Inbox is empty.</h3></li>
	 						 <?php } ?>
						</ul>
					</form>
				</div>

			</div>
	</div>




<script>

jQuery(document).ready(function(){

 
	jQuery('#delete-message-btn').on('click',function(){
   
		if(jQuery('input:checked').length > 0){		
			popup.open({buttonAlign:'center',align:'center',type: 'confirm',message:'<h3>Are you sure you want to delete?</h3>',onConfirm:function(){ jQuery('#inbox-form').trigger('submit'); } });			
		}else{
			popup.open({buttonAlign:'center',align:'center',type: 'alert',message:'<h3>Please select message to delete.</h3>'});
		}		
 
	});

});


 


</script>