<div class="wrapper">
	
	<div class="container move-fwd-inner">
		<div class="row">
			<div class="col-md-3 col-lg-2  col-sm-12 side-rail">
				<nav>
					<a href="<?= BASE_URL ?>move_forward" <?= !$active ? 'class="active"' : '' ?>>ACTIVE OFFERS</a>
					<?php foreach ($categories as $category): ?>
					<a <?= $active && $active == $category['category_id'] ? 'class="active"' : '' ?> href="<?= BASE_URL ?>move_forward/category/<?= $category['category_id'] ?>">
						<?= $category['category_name'] ?>
					</a>
					<?php endforeach ?>
				</nav>
			</div>

			<div class="col-md-9  col-md-push-3 col-lg-push-2 main-rail">
				<div class="row row-move-fwd">
					<?php if ($offers): ?>
					<?php foreach ($offers as $offer): ?>
					<div class="col-sm-6 item <?= $offer['status'] ? '' : 'inactive' ?> <?= $offer['is_premium_offer'] ? 'premium' : '' ?>">
						<a href="<?= BASE_URL ?>move_forward/offer/<?= $offer['move_forward_id'] ?>/<?= $offer['permalink'] ?>">
							<div class="thumb-holder col-lg-6">
								<div class="thumbnail" style="background-image:url('<?= BASE_URL ?>uploads/move_fwd/thumb_<?= $offer['move_forward_image'] ?>')"></div>
							</div>
						</a>
						<div class="col-lg-6">
							<?php if($offer_status[$offer['move_forward_id']] == NORMAL) { ?>
							<h2><a href="<?= BASE_URL ?>move_forward/offer/<?= $offer['move_forward_id'] ?>/<?= $offer['permalink'] ?>"><?= $offer['move_forward_title'] ?></a></h2>
							<a href="<?= BASE_URL ?>move_forward/offer/<?= $offer['move_forward_id'] ?>/<?= $offer['permalink'] ?>" class="button"><i><?= $offer['status'] ? 'SIGN ME UP' : 'VIEW' ?></i></a>
							<?php } elseif($offer_status[$offer['move_forward_id']] == HAS_APPROVED) { ?>
							<h2><a href="<?= BASE_URL ?>move_forward/offer/<?= $offer['move_forward_id'] ?>/<?= $offer['permalink'] ?>"><?= $offer['move_forward_title'] ?></a></h2>
							<a href="<?= BASE_URL ?>move_forward/offer/<?= $offer['move_forward_id'] ?>/<?= $offer['permalink'] ?>" class="button"><i>Complete Next Task</i></a>
							<?php } elseif($offer_status[$offer['move_forward_id']] == ALL_APPROVED) {?>
							<h2><a href="<?= BASE_URL ?>move_forward/offer/<?= $offer['move_forward_id'] ?>/<?= $offer['permalink'] ?>"><?= $offer['move_forward_title'] ?></a></h2>
							<a href="<?= BASE_URL ?>move_forward/offer/<?= $offer['move_forward_id'] ?>/<?= $offer['permalink'] ?>" class="button"><i>Challenge Accepted</i></a>
							<!-- <a onclick="showCompleted(<?= $offer['move_forward_id'] ?>)" class="button"><i>Challenge Accepted</i></a> -->
							<?php } else {?>
							<h2><a href="<?= BASE_URL ?>move_forward/offer/<?= $offer['move_forward_id'] ?>/<?= $offer['permalink'] ?>"><?= $offer['move_forward_title'] ?></a></h2>
							<a href="<?= BASE_URL ?>move_forward/offer/<?= $offer['move_forward_id'] ?>/<?= $offer['permalink'] ?>" class="button"><i>PLEDGED</i></a>
							<?php } ?>
							
						</div>
						<div class="clearfix"></div>
					</div>
					<?php endforeach ?>
					<?php else: ?>
					<h2>No Offers</h2>
					<?php endif ?>
				</div>
			</div>
		</div>
	</div>

</div>

<?php $this->load->view('move_fwd/splash') ?>

<script type="text/javascript">
	$(document).ready(function(){
		<?php if (!$active && !$visited): ?>
		popup.open({html:'#move-fwd-splash'});
		<?php endif ?>
		toggleMobileSubNav();

		$('.browse').click(function(){
			$('#upload-task').trigger('click');
		});

		//remove later
		//$('#_takechallenge').trigger('click');

	});

	function toggleMobileSubNav(){

		if($(window).width()<768){
			$('.move-fwd-inner .active').click(function(){
				$(this).parent().toggleClass('drop');
				return false;
			});
		}

	}

	function showCompleted(id) {
		popup.loading();
		popup.open({url: '<?php echo BASE_URL ?>move_forward/challenge/' + id});
	}
</script>