
	<div class="popup perks">
		<div class="content">
		<button class="popup-close">&times;<span>CLOSE</span></button>
		

		<div class="row perks-details">
			<div class="col-sm-5">
				
				<div class="carousel">
					<div id="holder">
						<?php foreach ($media as $m): ?>
					  	<img src="<?= BASE_URL ?>uploads/reserve/media/<?= $m['media_content'] ?>">
						<?php endforeach ?>
					</div>
				</div>

			</div>

			<div class="col-sm-7">
				<h2><?= $item['lamp_name'] ?></h2>
				<?= $item['description'] ?>

				<br>
				<div class="fieldset">
					<div class="legend"><span>RESERVATION DETAILS</span></div>
					<div class="alert alert-danger" style="display:none;" id="reserve-error"></div>
					<form id="form-reserve">
						<input type="hidden" value="<?= $item['lamp_id'] ?>" name="id">
						<ul class="specs">
							
							<li>
								<label>BOOKING DATE:</label>
								<input id="booking-date" type="text" name="date">
								<input type="hidden" name="booking_date" id="date-id">
								<!-- <select id="booking-date" name="booking_date">
									<?php foreach ($schedules as $date): ?>
									<?php if (($date['original_slots'] && ((int) $date['slots']) > 0) || !$date['original_slots']): ?>
									<option data-slots="<?= $date['original_slots'] ? $date['slots'] : 'N/A' ?>" <?= $selected_schedule && $selected_schedule == $date['schedule_id'] ? 'selected="selected"' : '' ?> 
										value="<?= $date['schedule_id'] ?>"><?= $date['date'] ?></option>
									<?php endif ?>
									<?php endforeach ?>
								</select> -->
							</li>
							<?php if ($schedules): ?>
							<li>
								<label>SLOTS: </label>
								<div id="sched-slots"><?= $schedules[0]['original_slots'] ? $schedules[0]['slots'] : 'N/A' ?></div>
							</li>
							<li>
								<label>POINTS: </label>
								<div id="sched-points"><?= $schedules[0]['reservation_cost'] ? $schedules[0]['reservation_cost'] : 'N/A' ?></div>
							</li>
							<?php endif ?>
							<li>
								<label>GUEST:</label>
								<div class="guests">
									<?php if (!$selected_guest_names): ?>
									<input type="text" name="guest_name[]" class="guest-name" placeholder="Guest Name">
									<input style="display:none;" type="text" name="guest_email[]" placeholder="Guest Email">
									
									<br>
									<!-- <input type="text" name="guest_name[]" class="guest-name" placeholder="Guest Name">
									<input type="text" name="guest_email[]" placeholder="Guest Email"> -->
									<?php else: ?>
										<?php foreach ($selected_guest_names as $k => $guest): ?>
										<?= $k ? '<br>' : '' ?>
										<input type="text" name="guest_name[]" class="guest-name" value="<?= $guest ?>" placeholder="Guest Name">
										<input type="text" style="display:none;" name="guest_email[]" value="<?= $selected_guest_emails[$k] ?>" placeholder="Guest Email">
										<?php endforeach ?>
									<?php endif ?>
								</div>
								<!-- <button type="button" class="addfield">ADD FIELDS</button> -->

								<div class="clearfix"></div>
							</li>

							<!-- <li>
								<label>MOBILE NUMBER:</label>
								<input type="text" id="reserve-mobile-num" name="mobile_num" maxlength="11" value="<?= !$selected_mobile_num ? $this->session->userdata('user')['mobile_num'] : $selected_mobile_num ?>">
							</li> -->
						</ul>
					</form>
				</div>

				<div class="txtcenter">	
					<button type="button" id="perks-reserve-btn" class="button"><i>CONFIRM</i></button>
				</div>
				
				
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">

$(document).ready(function() {

	$("#holder").owlCarousel({
		items : 1,
		itemsDesktop : [3000,1],
		itemsMobile : [767,1],
		itemsTablet : [991,1],
		pagination : true
	});

	var currentSlot;
	var schedules = <?= json_encode($dates_slots) ?>;
	$("#booking-date").datepicker({
		beforeShowDay : function (date) {
	        var formattedDate = $.datepicker.formatDate('yy-mm-dd', date);
	        return [$.inArray(formattedDate, <?= json_encode($dates) ?>) != -1];
		},
		dateFormat : "yy-mm-dd",
		changeYear : true,
		changeMonth : true,
		onSelect : function (date) {
			if (typeof schedules[date] != "undefined") {
				$("#sched-slots").html(schedules[date]['slots']);
				$("#sched-points").html(schedules[date]['reservation_cost']);
				$("#date-id").val(schedules[date]['schedule_id']);
			}
		}
	});
	// .click( function () {
	// 	currentSlot = $("#booking-date option:selected").data("slots");
	// 	$("#sched-slots").html(currentSlot);
	// });

	$("#reserve-mobile-num").keydown( function(e) {
	    var key = e.charCode || e.keyCode || 0;
	    // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
	    // home, end, period, and numpad decimal
	    return (
	        key == 8 || 
	        key == 9 ||
	        key == 46 ||
	        key == 110 ||
	        key == 190 ||
	        (key >= 35 && key <= 40) ||
	        (key >= 48 && key <= 57) ||
	        (key >= 96 && key <= 105));
	});

	$('.addfield').click(function(){
		$('.fieldset .guests').append('<br>'+
								'<input type="text" name="guest_name[]" class="guest-name" placeholder="Guest Name"> '+
								'<input type="text" name="guest_email[]" placeholder="Guest Email">')
		popup.stabilize();
	});

	$("#perks-reserve-btn").click( function () {
		$("#reserve-error").hide();
		var guestCount = $(".guest-name").filter(function() {
	        return this.value.length !== 0;
	    }).length;
		if (!$("#booking-date").val()) {
			$("#reserve-error").show().html("Invalid date");
		} else if (currentSlot != "N/A" && currentSlot < guestCount) {
			$("#reserve-error").show().html("Not enough slots");
		} else {
			popup.open({url:'<?= BASE_URL ?>perks/reserve/confirm?' + $("#form-reserve").serialize()});
		}
	});
});
</script>