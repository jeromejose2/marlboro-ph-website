	<div class="popup perks">
		<div class="content">
		<button class="popup-close">&times;<span>CLOSE</span></button>
		

		<div class="row perks-details">
			<div class="col-sm-5">
				
				<div class="carousel">
					<div id="holder">
					  <?php if($media) {
						foreach($media as $k => $v) { ?>
							<img src="<?php echo BASE_URL ?>uploads/bid/media/150_150_<?php echo $v['media_content'] ?>"> 
						<?php }
					} ?>
					</div>
				</div>

			</div>

			<div class="col-sm-7">
				<h2><?php echo $item['bid_item_name'] ?></h2>
				<?php echo $item['details'] ?>


				<div class="fieldset">
					<div class="txtcenter"><h2>BIDDING ENDED</h2></div>
				</div>

				<div class="transaction" <?php echo !$bidder ? 'style="display:none"' : '' ?>>
					<h2>CURRENT BID: <span class="red"><span id="high-bid" ><?php echo number_format($bidder['bid']); ?></span> POINTS</span></h2>
					<h4>HIGHEST BIDDER: <span id="high-bidder" class="red"><?php echo $bidder['nick_name']; ?></span></h4>
				</div>
				
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
clearInterval(interval);
$(document).ready(function() {
	$("#holder").owlCarousel({
		items : 1,
		itemsDesktop : [3000,1],
		itemsMobile : [767,1],
		itemsTablet : [991,1],
		pagination : true
	});

	interval = setInterval(function() {
		$.post('<?php echo BASE_URL ?>perks/bid/get_bidder/<?php echo $item["bid_item_id"] ?>', function(data) {
			if(data.bid) {
				$('.transaction').show();
				$('#high-bid').html(data.bid);
				$('#high-bidder').html(data.nick_name);

			} else {
				$('.transaction').hide();
			}
		});
	}, 5000)
});
</script>