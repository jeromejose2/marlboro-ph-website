	<div class="popup perks">
		<div class="content">
		<button class="popup-close">&times;<span>CLOSE</span></button>
		

		<div class="row perks-details">
			<div class="col-sm-5">
				
				<div class="carousel">
					<div id="holder">
						<?php if($media) {
							foreach ($media as $k => $v) { ?>
								<img src="<?php echo BASE_URL ?>uploads/buy/media/150_150_<?php echo $v['media_content'] ?>">	
							<?php }
						} ?>
					</div>
				</div>

			</div>

			<div class="col-sm-7">
				<h2 style="width: 90%;"><?php echo $item['buy_item_name'] ?></h2>
				<?php echo $item['description'] ?>

				<div class="transaction">
					<h2>CREDIT VALUE: <span class="red"><?php echo number_format($item['credit_value']) ?> POINTS</span></h2>
					<form id="buy-form">
					<div class="fieldset">
						<?php if(isset($bid_data['reserved_points']) && $bid_data['reserved_points']): ?>
							<div class="txtcenter"><h2>YOU DON'T HAVE SUFFICIENT POINTS DUE TO PENDING BID ITEM/S. YOU ONLY HAVE <?php echo isset($bid_data['available_points']) ? $bid_data['available_points'] : 0 ?> POINTS ALLOWED TO USE.</h2></div>
						<?php else: ?>
							<div class="txtcenter"><h2>INSUFFICIENT POINTS</h2></div>
						<?php endif; ?>
					</div>
					<input name="id" type="hidden" value="<?php echo $item['buy_item_id'] ?>">
					</form>

				</div>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$("#holder").owlCarousel({
		items : 1,
		itemsDesktop : [3000,1],
		itemsMobile : [767,1],
		itemsTablet : [991,1],
		pagination : true
	});

	$.getJSON('<?php echo BASE_URL ?>api/notifications', function(data) {
		remainingPoints =  data.points - <?php echo $item['credit_value'] ?>;
		if(remainingPoints < 0) {
			$('#confirm-button').addClass('disabled');
			$('#remaining-h4').html('Insufficient points');
		}
		$('#remaining-points').html(remainingPoints);
	})

	$('#confirm-button').click(function() {
		if($(this).hasClass('disabled')) return;
		$.post('<?php echo BASE_URL ?>perks/buy/buy_item', $('#buy-form').serialize(), function(data) {
			if(data.success == 1) {
				popup.dialog({message:"You have successfully bought <?php echo $item['buy_item_name'] ?>" , title : "Buy Success", align: "center"});
			} else {
				popup.dialog({message: data.error , title : "Buy Unsuccessful", align: "center", onClose: function(){location.reload();}});
			}
			
		})
	});
});
</script>