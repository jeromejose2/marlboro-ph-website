	<div class="popup perks">
		<div class="content">
		<button class="popup-close">&times;<span>CLOSE</span></button>
		

		<div class="row perks-details">
			<div class="col-sm-5">
				
				<div class="carousel">
					<div id="holder">
					<?php if($media) {
						foreach($media as $k => $v) { ?>
							<img src="<?php echo BASE_URL ?>uploads/bid/media/150_150_<?php echo $v['media_content'] ?>"> 
						<?php }
					} ?>
					</div>
				</div>

			</div>

			<div class="col-sm-7">
				<h2><?php echo $item['bid_item_name'] ?></h2>
				<?php echo $item['details'] ?>

				<br>
				<div class="fieldset">

					<!-- <div class="txtcenter"><h2>BIDDING ENDED</h2></div> -->
					
					
					<div class="legend"><span>TIME REMAINING</span></div>
					<ul class="countdown">
						
					</ul>
					
				</div>

				<div class="transaction">
					<!-- <h2>CURRENT BID: <span class="red"><span id="high-bid" ><?php echo number_format(@$bidder['bid']); ?></span> POINTS</span></h2> -->
					<?php if(!isset($bidder['nick_name'])) { ?>
					<h4>MINIMUM BID: <span id="high-bidder" class="red"><?php echo $item['starting_bid'] ?> POINTS</span></h4>
					<?php } else {?>
					<h4>CURRENT HIGHEST BIDDER: <span class="red" ><span id="high-bidder" class="red"><?php echo @$bidder['nick_name'] ? @$bidder['nick_name'] : $bidder['first_name']; ?></span>, <span id="high-bid" ><?php echo number_format($bidder['bid']); ?></span> POINTS</span></h4>
					<?php } ?>
					
				</div>
				<div class="txtcenter">	
					<span><input type="text" id="bid" class="bid" onkeydown="return checkDigit(event)"></span>
					<button type="button" class="button" onclick="confirm()"><i>PLACE BID</i></button>
				</div>
				
				
			</div>
		</div>

	</div>
</div>
<script type="text/javascript" src="<?php echo BASE_URL ?>scripts/jquery.countdowntimer.js"></script>
<script type="text/javascript">
clearInterval(interval); 
$(document).ready(function() {
	$("#holder").owlCarousel({
		items : 1,
		itemsDesktop : [3000,1],
		itemsMobile : [767,1],
		itemsTablet : [991,1],
		pagination : true
	});

	var layout = '<li><i>DAYS</i><span>{dnn}</span></li><li><span>:</span></li><li><i>HOURS</i><span>{hnn}</span></li><li><span>:</span></li><li><i>MIN</i><span>{mnn}</span></li><li><span>:</span></li><li><i>SEC</i><span>{snn}</span></li>';
	
	liftoffTime = new Date('<?php echo date("Y/m/d H:i:s", strtotime($item["end_date"])) ?>');
	$('.countdown').countdown({
		until : liftoffTime,
		layout: layout
	}); 

	interval = setInterval(function() {
		$.post('<?php echo BASE_URL ?>perks/bid/get_bidder/<?php echo $item["bid_item_id"] ?>', function(data) {
			if(data.bid) {
				$('.transaction').show();
				$('#high-bid').html(number_format(data.bid));
				$('#high-bidder').html(data.nick_name);

			} else {
				//$('.transaction').hide();
			}
		});
	}, 5000)
});

function checkDigit(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	var BACKSPACE = 8;
	var DELETE = 46;
	var LEFT = 37;
	var RIGHT = 39;
	if (charCode != 0)  {
		if((charCode < 48 || 57 < charCode) && charCode != BACKSPACE && charCode != DELETE && charCode != LEFT && charCode != RIGHT) {
		  evt.preventDefault();		  
		}
		else {
			return true;
		}
	}
}

function number_format (number, decimals, dec_point, thousands_sep) {
  // http://kevin.vanzonneveld.net
  // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +     bugfix by: Michael White (http://getsprink.com)
  // +     bugfix by: Benjamin Lupton
  // +     bugfix by: Allan Jensen (http://www.winternet.no)
  // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  // +     bugfix by: Howard Yeend
  // +    revised by: Luke Smith (http://lucassmith.name)
  // +     bugfix by: Diogo Resende
  // +     bugfix by: Rival
  // +      input by: Kheang Hok Chin (http://www.distantia.ca/)
  // +   improved by: davook
  // +   improved by: Brett Zamir (http://brett-zamir.me)
  // +      input by: Jay Klehr
  // +   improved by: Brett Zamir (http://brett-zamir.me)
  // +      input by: Amir Habibi (http://www.residence-mixte.com/)
  // +     bugfix by: Brett Zamir (http://brett-zamir.me)
  // +   improved by: Theriault
  // +      input by: Amirouche
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // *     example 1: number_format(1234.56);
  // *     returns 1: '1,235'
  // *     example 2: number_format(1234.56, 2, ',', ' ');
  // *     returns 2: '1 234,56'
  // *     example 3: number_format(1234.5678, 2, '.', '');
  // *     returns 3: '1234.57'
  // *     example 4: number_format(67, 2, ',', '.');
  // *     returns 4: '67,00'
  // *     example 5: number_format(1000);
  // *     returns 5: '1,000'
  // *     example 6: number_format(67.311, 2);
  // *     returns 6: '67.31'
  // *     example 7: number_format(1000.55, 1);
  // *     returns 7: '1,000.6'
  // *     example 8: number_format(67000, 5, ',', '.');
  // *     returns 8: '67.000,00000'
  // *     example 9: number_format(0.9, 0);
  // *     returns 9: '1'
  // *    example 10: number_format('1.20', 2);
  // *    returns 10: '1.20'
  // *    example 11: number_format('1.20', 4);
  // *    returns 11: '1.2000'
  // *    example 12: number_format('1.2000', 3);
  // *    returns 12: '1.200'
  // *    example 13: number_format('1 000,50', 2, '.', ' ');
  // *    returns 13: '100 050.00'
  // Strip all characters but numerical ones.
  number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function (n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

function confirm() {
	var empty = /^\s*$/;
	bid = $('#bid').val();
	if(empty.test(bid)) {
		popup.dialog({message: 'Please enter bid.' , title : "BID UNSUCCESSFUL", align: "center"});	
		return false;
	}
	clearInterval(interval);
	popup.open({url:'<?php echo BASE_URL ?>perks/bid/confirm/<?php echo $item['bid_item_id'] ?>/' + bid});
}

</script>