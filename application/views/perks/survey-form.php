
<!-- main content starts here  -->
<style type="text/css">

	.survey-submit{
	    font-family: 'Maybebold';
	    font-size: 22px;
	    background: url(../images/pattern-red.png) 0px -43px repeat-x;
	    padding: 0px 2px;
	    margin-right: 5px;
	    margin-left: 5px;
	    border: 0;
	    text-transform: uppercase;
	    position: relative;
	    text-align: center;
	    display: inline-block;
	    cursor: pointer;
	    outline: none;
	    width: 130px;
	    height: 40px;
	    line-height: 40px;
	    color:#fff !important;
	    margin-top: 15px;

	}


#survey-content  input[type="text"], textarea {
	max-width: 100% !important;
	margin-top: 10px;
	margin-left: 15px;

}
#survey-content  input[type="text"] {
	width: 90%;
}

#survey-form ul li{

}
.survey-submit {
/*	margin-right: 25% !important;
*/}

#survey-form .arrow {
	margin-top: -10% !important;
}

.survey_count {
	display: inline-block;
	position: absolute;
	top: 0;
	right: 200px;
	font-size: 14px;
}

.numberSpan {
	padding: 2px 5px;
	display: inline-block !important;
	background-color:#a70101;
	margin-right:5px;
	color: #fff;
	valign:center;
}

h3.qstyle {
	margin-bottom: 5px;

}

#survey-form ul li span {
	margin-bottom: 5px !important;
}
#survey-form ul li .left{
	left:0 !important;
}
#survey-form ul li .right{
	right:-10% !important;
}

.row.islider {
	max-width:60%;
	margin:0 auto;
}

.jf_error{
	position: absolute;
	color:red;
	top: 8%;
}

@media (max-width: 767px){
	#survey-content  input[type="text"], textarea {
		max-width: 92% !important;
		margin-left: 0px !important;
		margin-top: 10px !important;
	}

	.row.islider {
		max-width:100%;
	}

	.survey-submit {
		margin-right: 11% !important;
	}

	.countCaption {
	padding-right: 7% !important;
	}
}

</style>
<div class="wrapper">

	<div class="container main">
		<div class="row islider">
			<div style="padding:0 30px;" class=" col-lg-11" id="survey-content">
				<!-- content -->
				<h2>SURVEY QUESTIONS</h2>

				<div id="error"></div>

				<form id="survey-form" method="post" class="survey-forms" onsubmit="return onSubmitSurvey();">
					<ul>
						<?php
						if(!empty($questions)){
							$count = count($questions);
                            				foreach($questions as $k => $question){ ?>
							<li  data-id ="checkbox-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?>" id="<?= $k + 1 ?>" <?=($question->options == 2) ? 'class="selectCheckbox"  data-max-limit = "'.$question->checkbox_max.'" data-min-limit="'.$question->checkbox_limit.'"' : ''; ?>>

								<h2 style="font-size:15px; margin: 0;margin-bottom:10px; padding:0;display:block;text-align:right;" class="countCaption">
									<?= $k + 1 ?> OUT OF  <?= $count?>
							         </h2>
		                                <h3 class="qstyle" style="margin: 0;padding:0;">
	                                		<div class="col-md-1"> <span class="numberSpan"><?= $k + 1 ?> </span></div>
	                                		 <div class="col-md-11"><?=$question->question?></div>
	                                	</h3>
		                                	<div class="col-md-offset-1 col-md-11">
		                                    <?php $choices = unserialize($question->choices); ?>

				                         <?php if(!empty($question->options)) {
				                              if($question->options == 1){
				                                if(!empty($choices)) {
				                                  foreach($choices as $k => $val) {
				                                    if( ($val != '') && (strpos($val, 'Others') !== false) ){ ?>
				                                      <br><span><input type="radio" value="<?=$val?>" class="_req" name="radio-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?>" onclick="return showTextbox('radio_input-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?>');"> <?=str_replace('Others ', '', $val)?> </span>
		                                        	  <br><span><input type="text" value="" name="radio_input-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?>" maxlength="1000" placeholder="Max characters (1000)" style="display:none"></span>
				                                    <?php }else if($val != '') { ?>
				                                      <br><span><input type="radio" value="<?=$val?>" class=" _req" name="radio-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?>" required data-msg="Please answer the survey question." onchange="return onChangeTextbox('radio_input-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?>');"> <?=$val?> </span>
				                                <?php }}} ?>
				                        <?php }else if($question->options == 2){ ?>
				                        		<input type="hidden" name="checkbox_limit-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?>" value="<?=$question->checkbox_limit?>">
		                             		<?php if(!empty($choices)) {
				                                  foreach($choices as $k => $val) {
				                                    if( ($val != '') && (strpos($val, 'Others') !== false) ){ ?>
				                                      <br><span><input type="checkbox" value="<?=$val?>" class="checkbox-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?> _req" name="checkbox-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?>[]" id="checkbox-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?>" onclick="return onCheck( 'checkbox_input-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?>', 'checkbox-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?>' );"> <?=str_replace('Others ', '', $val)?> </span>
				                                      <br><span><input type="text" value="" name="checkbox_input-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?>" maxlength="1000" placeholder="Max characters (1000)" style="display:none"></span>
				                                    <?php }else if($val != '') { ?>
				                                      <br><span><input type="checkbox" value="<?=$val?>" class="checkbox-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?> _req" name="checkbox-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?>[]" required data-msg="Please answer the survey question."> <?=$val?> </span>
				                                <?php }}} ?>
				                        <?php }else if($question->options == 3){ ?>
				                                  <select class="_req" name="dropdown-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?>" id="dropdown-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?>" onchange="return dropdownChange('dropdown-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?>')" required data-msg="Please answer the survey question.">
				                                    <option value="">Please select</option>
				                           <?php if(!empty($choices)) {
				                                  foreach($choices as $k => $val) {
				                                    if( ($val != '') && (strpos($val, 'Others') !== false) ){ ?>
				                                    <option value="<?=$val?>"><?=str_replace('Others ', '', $val)?></option>
				                                  <?php }else if($val != '') { ?>
				                                    <option value="<?=$val?>"><?=$val?></option>
			                                <?php }}} ?>
			                                		</select>
				                        <?php }else if( ($question->options == 4) ) {
				                                if(!empty($choices)) {
				                                  foreach($choices as $k => $val) {
				                                    if( ($val != '') && ($val == 'text') ){ ?>
			                                  		<br><textarea style="resize:none;height:100px;width:100%;" name="textarea-<?=$question->survey_id?>-<?=$question->options?>-<?=$question->question_id?>" class="_req" required data-msg="Please answer the survey question."></textarea>
				                              <?php }}}} ?>
				                      <?php } ?>
								</div>
							</li>
                        <?php }} ?>
					</ul>
			    </form>
				<!-- end content -->
			</div><br><br><br><br><br>
			<!-- <div class="col-lg-6">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
										quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
										consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
										cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
										proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<br>
					<img src="http://marlboro-stage2.yellowhub.com/spice/assets_finalist/img/bg/finalist_bg.jpg" class="img-responsive">
			</div>	 -->
		</div>
	</div>

</div>

<script type="text/javascript">
$(function(){
	var elCheckBox = $('.selectCheckbox');
	elCheckBox.each(function(){
		var className = $(this).data('id');
		var max = $(this).data('max-limit');
		var min = $(this).data('min-limit');

		$('.'+className).each(function(){
			$(this).on('click',function(){
				var count = $('.'+className+':checked').length

				if(count > max ){
					if(min == max){
						// $('.jf_error').html('Please choose '+max+' items from the selection.').show();
						$(elCheckBox).append('<input type="hidden" class="hidden" required data-msg="Please choose '+max+' items from the selection.">');
					}else{
						// $('.jf_error').html('Please choose not more than '+max+' items from the selection.').show();
						$(elCheckBox).append('<input type="hidden" class="hidden" required data-msg="Please choose '+max+' items from the selection.">');
					}
				}else if (count < min && count == (min - 1)  ){
					// $('.jf_error').html('Please choose at least '+min+' item/s from the selection.').show();
					$('.'+className+':not(:checked)').prop('disabled', false);
					$(elCheckBox).append('<input type="hidden" class="hidden" required data-msg="Please choose '+max+' items from the selection.">');
				}else if (count < min ){
					$(elCheckBox).append('<input type="hidden" class="hidden" required data-msg="Please choose '+max+' items from the selection.">');
				}else if(count < 1){
					$('.jf_error').html('').show();
					$('.hidden').remove();
				}else if(count == max){
					$('.'+className+':not(:checked)').prop('disabled', true);
					$('.hidden').remove();  
				}else{
					$('.jf_error').html('').show();
					$('.hidden').remove();
				}
			})
		})
	})

})

var srcWidth = $( window ).width();
var xWidth;

if(srcWidth >= 320 && srcWidth <= 360){
	xWidth = srcWidth*2.5;
}else if(srcWidth >= 420 && srcWidth <= 980){
	xWidth = srcWidth;
}else{
	xWidth = 640;
}


$(function(){

	var options={
	     width:xWidth,
	     height:600,
                movement:'horizontal',
                next_button:true,
                prev_button:true,
                button_placement:'bottom',
                submit_button:true,
                submit_class:'survey-submit',
                next_class:'arrow right',
                prev_class:'arrow left',
                error_class:'error',
                input_error_class:'',
                error_element:'p',
                texts:{
                        next:'',
                        prev:'',
                        submit:'submit'
                      },
                speed:400,
                submit_handler:function(){
                	onSubmitSurvey();
                },
                slide_on_url:false,
                error_position:"outside"
            }
	$('#survey-form').jFormslider(options);
	var liCount = $('#survey-form ul li').length;
	var arrowLessCount = liCount - 1;

	$('#survey-form ul li:nth('+arrowLessCount+')').attr('data-callbefore', 'disableLastArrow('+arrowLessCount+')');

	var error = $('div.jf_error');

	$('a.arrow.left').hide();
	$('a.arrow.right').click(function(){
			$('a.arrow.left').show();
	});

	$('a.arrow.left').click(function(){
		$('a.arrow.right').show();
	});

});

function disableLastArrow(lastArrow){
	var last = lastArrow - 1;
	$('.arrow.right').hide();
	return true;
}

function onSubmitSurvey(){
	var error = false;
	$('._req').each(function(){
		var el = $(this);
		var val = $.trim(el.val());
		var typ = el.attr('type');
		var name = el.attr('name');
		if( val == '' ){
			error = 'Sorry. You need to answer all questions to get the bonus points.';
			return false;
		}

	});
	if( error ){
		$('#error').html('<span>'+error+'</span>');
		$('html, body').animate({scrollTop:0})
		return false;
	}else{
		$.ajax({
			url: BASE_URL+'perks/buy/submit_buy_survey',
			type: 'POST',
			data: $('#survey-form').serialize(),
			beforeSend: function(){
 				popup.loading();
 			},
 			success: function(response){
 				var resp = $.parseJSON(response);
 				console.log(resp);
 				console.log(resp.msg);
 				console.log(resp.title);
 				popup.open({
 						type: 'alert',
				 		'message': resp.msg,
				 		buttonAlign:'center',
				 		align:'center',
				 		okCaption:'OK',
				 		title: resp.title,
				 		onConfirm: function(){
				 			console.log('thank you!');
				 			window.location.reload();
				 		}
			 	});
			}
		});

		return false;
	}
}

function showTextbox(textName){

	$('input[name='+textName+'').show();
	$('input[name='+textName+'').attr('required', 'required');
	$('input[name='+textName+'').attr('data-msg', 'Please enter your answer!');

}

function onChangeTextbox(textName) {

	if($('input[type="text"]')){
   		$('input[name='+textName+'').removeAttr('required');
		$('input[name='+textName+'').removeAttr('data-msg');
		$('input[name='+textName+'').hide();
	}
}

function onCheck(textName, id){

	var others = id;

	if($('#'+others).is(':checked')){
		$('input[name='+textName+'').show();
		$('input[name='+textName+'').attr('required', 'required');
		$('input[name='+textName+'').attr('data-msg', 'Please enter your answer!');
	}else{
		$('input[name='+textName+'').removeAttr('required');
		$('input[name='+textName+'').removeAttr('data-msg');
		$('input[name='+textName+'').hide();

	}

}

function dropdownChange(name){

	var number = name.substr(name.indexOf("-") + 1)
	var value =  $('select#'+name+'').val();

	if(value.indexOf("Others") >= 0){
		$('select#'+name+'').parent('div').append('<br><span><input type="text" value="" name="dropdown_input-'+number+'" maxlength="1000" placeholder="Max characters (1000)"></span>');
		$('input[name=dropdown_input-'+number+'').attr('required', 'required');
		$('input[name=dropdown_input-'+number+'').attr('data-msg', 'Please enter your answer!');
	}
	else{
		$('select#'+name+'').parent('div').find('input').remove()
		$('input[name=dropdown_input-'+number+'').removeAttr('required');
		$('input[name=dropdown_input-'+number+'').removeAttr('data-msg');
	}
}



</script>
