<!-- main content starts here  -->
<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>plugins/jquery-ui/css/smoothness/jquery-ui-1.10.0.custom.min.css">
<script src="<?= BASE_URL ?>plugins/jquery-ui/js/jquery-ui-1.10.0.custom.min.js"></script>
<section class="main">
	<div class="wrapper">	
		<div class="container row perks-inner">
			
			<div class="col-md-3 col-lg-2 col-sm-12 side-rail">
				<?php $this->load->view('perks/nav') ?>
			</div>

			<div class="col-md-9 col-md-push-3  col-lg-push-2  main-rail">
				<div class="upload-from-event">
					<button type="button" class="button-small" onclick="popup.open({url:'<?=BASE_URL?>perks/reserve/mechanics'})"><i>Read Terms &amp; Conditions</i></button>
				</div>
				<div class="clearfix"></div>
				<br>
				<div class="row perks-items">
					<?php if ($items):$c = 0; ?>
					<?php foreach ($items as $item): $c++; ?>
					<div class="col-md-6 item">
						<div class="thumb-holder col-sm-6">
							<a href="#" onclick="popup.open({url:'<?= BASE_URL ?>perks/reserve/item?id=<?= $item['lamp_id'] ?>'});return false;">
								<div class="thumbnail" style="background-image:url('<?= BASE_URL ?>uploads/reserve/main_<?= $item['lamp_image'] ?>')"></div>
							</a>
						</div>
						<div class="col-sm-6 copy">
							<h2><a href="#" onclick="popup.open({url:'<?= BASE_URL ?>perks/reserve/item?id=<?= $item['lamp_id'] ?>'});return false;"><?= $item['lamp_name'] ?></a></h2>
							<p><?= word_limiter(strip_tags($item['description']), 200) ?></p>
							<button type="button" class="button" onclick="popup.open({url:'<?= BASE_URL ?>perks/reserve/item?id=<?= $item['lamp_id'] ?>'})"><i>RESERVE NOW!</i></button>
						</div>
						<div class="clearfix"></div>
					</div>
					<?=($c%2==0) ? '<div class="clearfix"></div>' : ''?>
					<?php endforeach ?>
					<?php else: ?>
					<div class="col-md-12">
						<h3>No Record(s) found</h3>
					</div>
					<?php endif ?>
				</div>
			</div>
		</div>

	</div>
	<?php $this->load->view('perks/splash-popup') ?>

</section>
<?php if(!$viewed): ?>
<script type="text/javascript">
$(function(){
	popup.open({html:'#popup1'});
});	
</script>
<?php endif; ?>