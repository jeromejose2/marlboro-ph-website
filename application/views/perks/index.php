<!-- main content starts here  -->
<section class="main">
	<div class="wrapper">	
		<div class="container main">
			
			<div class="row perks-categories">
				<div class="col-md-3 item">
					<div class="thumb-holder col-sm-7 col-md-12">
						<a href="<?php echo BASE_URL ?>perks/bid">
							<div class="thumbnail" style="background-image:url('<?php echo BASE_URL ?>uploads/perks/<?php echo $thumbs['bid']['description'] ?>')"></div>
						</a>
					</div>
					<div class="col-sm-5 col-md-12 copy">
						<!-- <h2><a href="<?php echo BASE_URL ?>perks/bid">BID</a></h2> -->
						<a href="<?php echo BASE_URL ?>perks/bid" class="button"><i>BID</i></a>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="clearfix visible-xs visible-sm"></div>
				
				<div class="col-md-3 item">
					<div class="thumb-holder col-sm-7 col-md-12">
						<a href="<?php echo BASE_URL ?>perks/bid">
							<div class="thumbnail" style="background-image:url('<?php echo BASE_URL ?>uploads/perks/<?php echo $thumbs['buy']['description'] ?>')"></div>
						</a>
					</div>
					<div class="col-sm-5 col-md-12 copy">
						<!-- <h2><a href="<?php echo BASE_URL ?>perks/bid">BUY</a></h2> -->
						<a href="<?php echo BASE_URL ?>perks/buy" class="button"><i>BUY</i></a>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="clearfix visible-xs visible-sm"></div>

				<div class="col-md-3 item">
					<div class="thumb-holder col-sm-7 col-md-12">
						<a href="<?php echo BASE_URL ?>perks/bid">
							<div class="thumbnail" style="background-image:url('<?php echo BASE_URL ?>uploads/perks/<?php echo $thumbs['reserve']['description'] ?>')"></div>
						</a>
					</div>
					<div class="col-sm-5 col-md-12 copy">
						<!-- <h2><a href="<?php echo BASE_URL ?>perks/bid">RESERVE</a></h2> -->
						<a href="<?php echo BASE_URL ?>perks/reserve" class="button"><i>RESERVE</i></a>
					</div>
					<div class="clearfix"></div>
				</div>
				

			</div>

		</div>

	</div>

	<?php $this->load->view('perks/splash-popup') ?>

</section>

<script type="text/javascript">
$(function(){
	popup.open({html:'#popup1'});
});	
</script>
<script type="text/javascript">

	$(function(){

		console.log('User Session Key: <?=$session_key?>');
		console.log("<?php echo @$flash_offer ?>");
		console.log("<?php echo $this->session->userdata('stat') ?>");
		<?php /* console.log('Referral Offer: <?php echo var_dump($referral_offer) ?>'); */ ?>
		<?php if($this->session->userdata('login_promo')!=1) { ?>
			popup.open({url:"login_offer/get_offer"});
		<?php } ?>
		<?php if($referral_offer){  ?>
		 		popup.open({url:"flash_offer/get_flash_offer?prize_id=<?=$referral_offer?>&rand=<?=uniqid()?>&type=referral"});
		 <?php } ?> 

		 <?php if(!$referral_offer && $birthday_offer && $birthdate != '0000-00-00'){ ?>
		 		popup.open({url:"birthday_offer/get_birthday_offer?prize_id=<?=$birthday_offer?>&flash_offer=<?=$flash_offer?>&rand=<?=uniqid()?>",
		 					onClose:function(){		
								<?php if($flash_offer && !$flash_offer_confirmed) { ?>
									 	 popup.open({url:"flash_offer/get_flash_offer?prize_id=<?=$flash_offer?>&rand=<?=uniqid()?>&type=flash"});
								 <?php } ?> 		
		 					}
		 				});
		 <?php } ?>

		 <?php if(!$referral_offer && !$birthday_offer && $flash_offer && !$flash_offer_confirmed) { ?>
		 			popup.open({url:"flash_offer/get_flash_offer?prize_id=<?=$flash_offer?>&rand=<?=uniqid()?>&type=flash"});
		 <?php } ?> 

		 <?php if(!$referral_offer && !$birthday_offer && !$flash_offer && $bids){ ?>
		 		popup.open({url:"perks/bid/confirm_address"});
		 <?php } ?> 
	});

	

</script>