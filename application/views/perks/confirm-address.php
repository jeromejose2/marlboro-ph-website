<div class="popup">
	<div class="content">
		<button class="popup-close">&times;<span>CLOSE</span></button>
		
		<h2 class="txtcenter">UPDATE OR CONFIRM YOUR MAILING ADDRESS</h2>
		<div class="error _error" style="text-align: left; display: none"></div>
		<form action="<?=BASE_URL?>perks/buy/confirm_address" method="post" id="confirm-address-perks-buy"  target="upload_target" onsubmit="return onSubmitForm();">
		<input type="hidden" name="prize_id">
		<div class="row  confirm-email">
			<div class="col-sm-6">
				<h3>STREET NAME</h3>
				<input class="_required" alt="Street Name" type="text" name="street_name" value="<?=$spice_userinfos->Addresses[0]->AddressLine1?>">
			</div>

			<div class="col-sm-6">
				<h3>BARANGAY</h3>
				<input class="_required" alt="Barangay" type="text" name="barangay" value="<?=$spice_userinfos->Addresses[0]->Area?>">
			</div>

			<div class="col-sm-6">
				<h3>PROVINCE</h3>
				<select class="_required" alt="Province" name="province">
					<option value="">Select</option>
 					<?php  if($provinces->num_rows()){
								foreach($provinces->result() as $v){ 
										if(strtolower($spice_userinfos->Addresses[0]->Locality) === strtolower($v->province)){?>
											<option selected="selected" value="<?=$v->province_id?>"><?=$spice_userinfos->Addresses[0]->Locality?></option>
								<?php	}else{ ?>
											<option value="<?=$v->province_id?>"><?=$v->province?></option>
								  <?php }
								}
							}  ?>
				</select>
			</div>

			<div class="col-sm-6">
				<h3>CITY/TOWN</h3>
				<select class="_required" alt="City/Town" name="city">
					<option value="">Select</option>
 					<?php if($cities){
							foreach($cities->result() as $c){ 
									if(strtolower($spice_userinfos->Addresses[0]->City)===strtolower($c->city)){?>
										<option selected="selected" value="<?=$c->city_id?>"><?=$spice_userinfos->Addresses[0]->City?></option>
							<?php	}else{ ?>
										<option value="<?=$c->city_id?>"><?=$c->city?></option>
							  <?php }
							}
						}  ?>
				</select>
			</div>
			

			<div class="col-sm-6">
				<h3>ZIP CODE:</h3>
				<input class="_required _numbers-only" alt="Zip Code" type="text" name="zip_code" value="<?=$spice_userinfos->Addresses[0]->PostalCode?>" maxlength="4">
			</div>
		</div>

		<div class="txtcenter">
			<div class="error" id="buy-offer-error"></div>
			<button class="button"  type="submit" id="buy-offer-btn"><i>Submit Confirmation</i></button>
		</div>
		<input type="hidden" value="<?php echo $id ?>" name="id">
		</form>
	</div>
</div>
<iframe id="upload_target" name="upload_target" style="width:0px;height:0px;border:none;"></iframe>
<script type="text/javascript"> 

$(function(){

	$('select[name="province"]').change(function(){
		var province = $(this).val();

		$.getJSON(BASE_URL+'profile/get_cities?province='+province,function(cities){
			var items = ['<option value=""></option>'];
 
			 $.each(cities, function(i,obj) {
 				items.push('<option value="'+obj.city_id+'">'+obj.city+'</option>');
			});
			 $('select[name="city"]').html(items.join(''));
		});
	})

});

$("._numbers-only").keypress(validateNumber);

function onSubmitForm()
{
	var error = false;
	var numbers = /^\d+$/;
	var error_container = $("._error");
	$("._required").each(function() {
		var elem = $(this);
		var value = $.trim($(this).val());
		var name = $(this).attr('alt');

		if( ! value) {
			error = '<span>'+ name +' is required.</span>';
			elem.focus();
			return false;
		} else if(elem.attr('name') == 'zip_code') {
			if(value.length < 4) {
				elem.focus();
				error = '<span>'+ name +' must be valid.</span>';
				return false;
			} else if( ! numbers.test(value)) {
				elem.focus();
				error = '<span>'+ name +' should be a number.</span>';
				return false;
			}
		}
	});


	error_container.css('display', 'none');

	document.getElementById('buy-offer-btn').disabled = true;
	$('#buy-offer-btn').attr('disabled','disabled').html('<i><img src="<?=BASE_URL?>images/spinner.gif" /> SUBMITTING...</i>');
	
	if(buy_data) {
		for(i = 0; i < buy_data.length; i++) {
			if(buy_data[i].name != 'id') {
				// console.log(buy_data[i].name);
				$('#confirm-address-perks-buy').append('<input type="hidden" name="'+buy_data[i].name+'" value="'+buy_data[i].value+'">');
			}
		}
	}

	if(error) {
		error_container.show().html(error);
		$('#buy-offer-btn').html('<i>SUBMIT CONFIRMATION</i>');
		document.getElementById('buy-offer-btn').disabled = false;
		return false;
	}

	return true;
}

function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;

    if (event.keyCode == 8 || event.keyCode == 46
     || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 9) {
        return true;
    }
    else if ( key < 48 || key > 57) {
        return false;
    }
    else return true;
}

function form_response(msg)
{
	var has_survey = $('input[name=has_survey]').val();
	var buy_id = $('input[name=id]').val();
 	// popup.open({type:'alert','message':'You have successfully updated your mailing address', buttonAlign:'center',align:'center',okCaption:'CONTINUE',onClose: function(){location.reload();}});

 	popup.open({type:'alert', 'message':msg, buttonAlign:'center',align:'center',okCaption:'CONTINUE',onConfirm: function(){
	 		
	 		$.ajax({
	 			url: BASE_URL+'perks/buy/check_if_has_survey',
	 			type: 'JSON',
	 			beforeSend: function(){
	 				popup.loading();
	 			},
	 			success: function(response){
	 				var resp = $.parseJSON(response);
		 			if(resp.has_survey == 0){
							
							popup.open({
							 		type:'confirm',
							 		title: 'Quick Survey',
							 		'message':'Would you like to earn extra '+resp.points+' points by answering our quick survey?', 
							 		buttonAlign:'center',
							 		align:'center',
							 		okCaption:'YES',
							 		cancelCaption: 'NO',
							 		onConfirm: function(){	
							 			popup.loading();
							 			window.location.href=BASE_URL+'perks/buy/save_to_session?buy_id='+buy_id;
							 		},
							 		onCancel: function(){
							 			console.log('thank you!');
							 			location.reload();
							 		}
						 	}); 
					}else{
						window.location.reload();
					}
				}
	 		});
		 	
	 	}
	});
}



</script>