<div class="wrapper">
	<div class="container">
		<div class="leather-content refer-a-friend">
			<form autocomplete="off" class="content" onsubmit="return validate(this)" action="<?= BASE_URL ?>refer" method="POST">
				<h2>REFER-A-FRIEND!</h2>
				<small>Invite your adult smoker friends to be part of the MARLBORO community and be rewarded with 300 points when your friend signs up and logs on.</small>
				<small>The more friends you invite, the higher the chance for you to win in our promos!</small>
				<div id="error"><?=$errors?></div>
				<br>
				<ul class="items">

					<?php if($post){ 

						foreach($post['name'] as $key=>$v){ ?>

					<li>
						<input autocomplete="off" value="<?=$v?>" type="text" name="name[]" placeholder="Name" class="req">
						<input autocomplete="off" value="<?=$post['email'][$key]?>" type="text" name="email[]" placeholder="Email address" class="req">
					</li>

					<?php } 

					 }else{ ?>

					<li>
						<input autocomplete="off" type="text" name="name[]" placeholder="Name" class="req">
						<input autocomplete="off" type="text" name="email[]" placeholder="Email address" class="req">
					</li>

					<?php } ?>
					
				</ul>
				<span class="add"> <i class="glyphicon glyphicon-plus"></i> Invite More</span>

				<small>
					<input type="checkbox" class="req" value="true"> I confirm that the person I am inviting to be part of the MARLBORO community is a legal age (min 18 years old) smoker, and that the email provided is used solely by him/her.
				</small>

				<a  class="button" href="<?= BASE_URL ?>profile"><i>CANCEL</i></a>
				<button type="submit" class="button"><i>SEND INVITATION</i></button>
			</form>
		</div>
	</div>
	<br class="clearfix">
</div>
<script id="add-template" type="text/x-handlebars-template">
  	<li>
		<input autocomplete="off" type="text" name="name[]" placeholder="name" class="req">
		<input autocomplete="off" type="text" name="email[]" placeholder="email address" class="req">
		<span class="remove glyphicon glyphicon-remove"></span>
	</li>
</script>

<script src="<?=BASE_URL?>/scripts/handlebars-v1.3.0.js"></script>
<script type="text/javascript">
function validate(form){
	var error = false;
	var eformat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var atpos;
    var dotpos;
    


	$('.req',form).each(function(){

		var el = $(this);
		var val = $.trim(el.val());

		
		if( val == '' ){
			error = 'Please input '+ el.attr('placeholder');
			el.focus();
			return false;
		}
		if(el.attr('name')=='email[]'){

			atpos = val.indexOf("@");
			dotpos = val.lastIndexOf(".");

			if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=val.length) {
				error = 'Please input a valid email address.';
				el.focus();
				return false;
			}
		 }
		if(el.attr('type')=='checkbox'){
			if(!el.is(':checked')){
				error = 'Please confirm to proceed.';
				return false;
			}
		}
		
	});

	if( error ){
		$('#error').html('<span>'+error+'</span>');
		return false;
	}
}
 
$(function(){
	$('.add').click(function(){
		var items = $('ul.items');
		if(items.children().length>=5){
			return;
		}
		var source   = $("#add-template").html();
		var template = Handlebars.compile(source);

		items.append(template);

		$('.remove').unbind('click').bind('click',function(){
			$(this).parent().remove();
		});
	})
})
</script>