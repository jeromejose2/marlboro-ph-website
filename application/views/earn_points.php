<div class="wrapper earnpoints fixed">
	<div class="holder-earnpoints">
		<h1 class="page">EARN POINTS</h1>
		<p>Earn points with MARLBORO and use them to redeem items and exclusive merchandise.</p>
		
		<div class="row">

			<div class="col-xs-7 col-sm-8 txtright webgames">
				<h3>Challenge yourself with our fun web games</h3>
			</div>
			<div class="col-xs-5 col-sm-4 webgames">
				<a href="<?= BASE_URL ?>games" class="button"><i>PLAY NOW</i></a>
			</div>
			<div class="clearfix"></div>

			<div class="col-xs-7 col-sm-8 txtright">
				<h3>
					Get bonus points with each friend you successfully refer! Just ask your adult smoker friends to input your Referral code when they register.
				</h3>
			</div>
			<div class="col-xs-5 col-sm-4">
				<a href="<?= BASE_URL ?>profile" class="button"><i>REFER A FRIEND</i></a>
			</div>
			<div class="clearfix"></div>

			<!-- <div class="col-xs-7 col-sm-8 txtright">
				<h3>INVITE YOUR FRIENDS AND INCREASE YOUR CHANCES OF WINNING</h3>
			</div>
			<div class="col-xs-5 col-sm-4">
				<a href="<?= BASE_URL ?>refer" class="button"><i>REFER A FRIEND</i></a>
			</div> -->
			<div class="col-xs-7 col-sm-8 txtright">
				<h3>Watch our videos to learn how to cross the “maybe” out of your life</h3>
			</div>
			<div class="col-xs-5 col-sm-4">
				<a href="<?= BASE_URL ?>about/dont-be-a-maybe" class="button"><i>WATCH VIDEOS</i></a>
			</div>
			<div class="clearfix"></div>

		</div>

		<p>Visit the website everyday to keep earning points and reward yourself.</p>
	</div>
</div>
