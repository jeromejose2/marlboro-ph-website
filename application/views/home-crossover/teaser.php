
	<div class="crossover">	

		<div class="container">
			<div class="copy">
				<div class="heading"><img src="<?=BASE_URL?>styles/crossover/img/heading.png"></div>
				

				<div class="watch">
					<span>WATCH</span>
					<button id="play-trailer"></button>
					<span>TRAILER</span>
				</div>

				<span class="soon">SOON</span>
			</div>
		</div>

	</div>

<div class="lytebox-wrap crossover">
	<div class="lytebox-content-holder">
		<div class="popup lytebox-wrapped-content" id="popup1">
			<div class="popup-close">&times; <span>Close</span></div>
			<video controls id="teaser-video">
				<source src="<?=BASE_URL?>styles/crossover/img/teaser.mp4"  type="video/mp4">
			</video>
	</div>
</div>

<script>
$(function(){
	$('#play-trailer').click(function(){
		var video = document.getElementById('teaser-video');
		popup.show({
			html: '#popup1',
			onOpen : function(){
			},
			onClose : function(){
				video.stop();

			}
		})
	})
});
</script>
<link rel="stylesheet/less" type="text/css" href="<?=BASE_URL?>styles/crossover/home-teaser.less">
<script src="<?=BASE_URL?>scripts/less.js"></script>