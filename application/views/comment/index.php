<div id="commentbox" class="comments"></div>

<script id="comments-target-form" type="text/x-handlebars-template">
	<form id="comment-form-upload-{{origin}}-{{suborigin}}{{reply}}" target="comment-form-target-{{origin}}-{{suborigin}}{{reply}}" action="<?= BASE_URL ?>comment/upload" enctype="multipart/form-data" method="POST">
		<input type="file" name="attachment" class="hidden" id="comment-attach-{{origin}}-{{suborigin}}{{reply}}">
	</form>
	<iframe name="comment-form-target-{{origin}}-{{suborigin}}{{reply}}" style="width: 0px; height: 0px; border: 0;" id="comment-form-target-{{origin}}-{{suborigin}}{{reply}}"></iframe>
</script>

<script id="comments-container" type="text/x-handlebars-template">
	<div class="add-comment">
		<figure>
			<?php if ($user_pic): ?>
				<?php if( ! $this->session->userdata('is_cm')): ?>
					<img src="<?= BASE_URL ?>uploads/profile/<?= $user_id ?>/<?= $user_pic ?>">
				<?php else: ?>
					<img src="<?= $user_pic ?>">
				<?php endif; ?>
			<?php else: ?>
			<img src="<?= BASE_URL ?><?= DEFAULT_IMAGE ?>">
			<?php endif ?>
		</figure>
		<span><?= $user_name ?></span>
		<button type="button" class="comments-show-attach-popup">ATTACH PHOTO</button>
		<textarea class="comment-textarea comment-text"></textarea>
		<i class="comment-submit-btn"> GO </i>
		<div class="terms">Comments/Replies are subject to moderation. PM reserves the right to reject or remove any comment as per the <a onclick="popup.open({url:'<?= BASE_URL ?>popups/terms.html'})">Terms and Conditions</a>.</div>
	</div>
	<ul class="comments-ul level-1">
		<li id="comments-attached-preview-{{origin}}-{{suborigin}}" style="display:none;" data-type="comment">
			<div class="attachment">
				<div class="remove">Remove Attachment</div>
				<img src="<?= BASE_URL ?><?= DEFAULT_IMAGE ?>">
			</div>
		</li>
		<li id="comments-preloader-{{origin}}-{{suborigin}}" style="border-bottom: 0;"><h3>Loading...</h3></li>
	</ul>
	<button type="button" style="display:none;" class="comments-show-more comments-next-btn" data-page="">SHOW MORE</button>
</script>

<script id="comments-list" type="text/x-handlebars-template">
	{{#each comments}}
		<li class="{{@key}}">
			<figure><img src="{{profile_picture}}"></figure>
			<span class="name">{{name}}</span>
			<span class="{{@key}}-text message">{{text}}</span>
			{{#if media}}
			<div class="attachment">
				<img src="{{media}}" class="comment-image">
			</div>
			{{/if}}
			<span class="timestamp">{{date}}</span>
			<button class="reply">Reply</button>
			<div class="reply" data-comment-id="{{id}}">
				<button type="button" class="comments-reply-show-attach-popup">ATTACH PHOTO</button>
				<figure>
					<?php if ($user_pic): ?>
						<?php if( ! $this->session->userdata('is_cm')): ?>
							<img src="<?= BASE_URL ?>uploads/profile/<?= $user_id ?>/<?= $user_pic ?>">
						<?php else: ?>
							<img src="<?= $user_pic ?>">
						<?php endif; ?>
					<?php else: ?>
					<img src="<?= BASE_URL ?><?= DEFAULT_IMAGE ?>">
					<?php endif ?>
				</figure>
				<textarea class="comment-reply-text comment-text"></textarea>
				<i class="comment-submit-reply-btn"> GO </i>
				<div class="terms">Comments/Replies are subject to moderation. PM reserves the right to reject or remove any comment as per the <a onclick="popup.open({url:'<?= BASE_URL ?>popups/terms.html'})">Terms and Conditions</a>.</div>
			</div>
			<div class="comments">
				<ul>
					<li class="comments-reply-attached-preview" id="comments-reply-attached-preview-{{id}}" data-type="reply" style="display:none;">
						<div class="attachment">
							<div class="remove">Remove Attachment</div>
							<img src="<?= BASE_URL ?><?= DEFAULT_IMAGE ?>">
						</div>
					</li>
					{{#each replies}}
					<li>
						<figure><img src="{{profile_picture}}"></figure>
						<span class="name">{{name}}</span>
						<span class="{{@key}}-text message">
							{{text}}
						</span>
						{{#if media}}
						<div class="attachment">
							<img src="{{media}}" class="comment-image">
						</div>
						{{/if}}
						<span class="timestamp">{{date}}</span>
					</li>
					{{/each}}
				</ul>
				<!-- <button type="button" style="margin-bottom: 5px;" class="comments-show-more comments-reply-next-btn">SHOW MORE</button> -->
			</div>
		</li>
	{{/each}}
</script>

<script type="text/javascript" src="<?= BASE_URL ?>scripts/handlebars-v1.3.0.js"></script>
<script type="text/javascript">
$(document).ready( function() {
	var commentBox = CommentBox.create("#commentbox", { 
			origin : <?= $origin ?>, 
			suborigin : <?= $suborigin ?>
		});
	<?php if (isset($recipient_id)): ?>
	commentBox.setRecipient(<?= $recipient_id ?>);
	<?php endif ?>
	commentBox.loadComments();
	<?php if ($ga): ?>
	commentBox.setAnalytics(<?= json_encode($ga) ?>);
	<?php endif ?>

	$(document).on('click', '.comment-image', function() {
		src = $(this).attr('src').replace('150_150_', '');
		popup.loading();
		popup.open({message: '<img src="' + src + '" width="100%" />'});
	});
});

</script>