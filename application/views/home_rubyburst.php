
  <div class="splash-contents container text-center">
    <div class="container-featured-photo">
      <img src="<?= base_url() ?>rubyburst2/img/product-pack-open.png" alt="Product">
    </div>
    <div class="container-featured-logo text-uppercase">
      <figure class="photo-container">
        <img src="<?= base_url() ?>rubyburst2/img/logo-alt.png" alt="Marlboro Rubyburst">
      </figure>
      <a href="<?= base_url() ?>rubyburst" class="btn btn-std-transparent">Discover A Fusion Of Fresh Taste</a>
    </div>
  </div>

<script type="text/javascript">

  $(function(){

    console.log('User Session Key: <?=$session_key?>');
    console.log("<?php echo @$flash_offer ?>");
    console.log("<?php echo $this->session->userdata('stat') ?>");
    <?php /* console.log('Referral Offer: <?php echo var_dump($referral_offer) ?>'); */ ?>
    <?php if($this->session->userdata('login_promo')==1) { ?>
      popup.open({url:"login_offer/get_offer"});
    <?php } ?>
    <?php if($referral_offer){  ?>
        popup.open({url:"flash_offer/get_flash_offer?prize_id=<?=$referral_offer?>&rand=<?=uniqid()?>&type=referral"});
     <?php } ?> 

     <?php if(!$referral_offer && $birthday_offer && $birthdate != '0000-00-00'){ ?>
        popup.open({url:"birthday_offer/get_birthday_offer?prize_id=<?=$birthday_offer?>&flash_offer=<?=$flash_offer?>&rand=<?=uniqid()?>",
              onClose:function(){   
                <?php if($flash_offer && !$flash_offer_confirmed) { ?>
                     popup.open({url:"flash_offer/get_flash_offer?prize_id=<?=$flash_offer?>&rand=<?=uniqid()?>&type=flash"});
                 <?php } ?>     
              }
            });
     <?php } ?>

     <?php if(!$referral_offer && !$birthday_offer && $flash_offer && !$flash_offer_confirmed) { ?>
          console.log("WORKING");

          popup.open({url:"flash_offer/get_flash_offer?prize_id=<?=$flash_offer?>&rand=<?=uniqid()?>&type=flash"});
     <?php } ?> 

     <?php if(!$referral_offer && !$birthday_offer && !$flash_offer && $bids){ ?>
        popup.open({url:"perks/bid/confirm_address"});
     <?php } ?> 
  });

  

</script>