
<div class="wrapper">
    <div id="preload">
            <div id="preload-login">
                <img src='<?=BASE_URL ?>/crossover_assets/img/login_preload.gif' alt="preload"/><br/><br/>
                <div class="textcontent">
                    <span>Breaking down boundaries</span>
                </div>
            </div>

    </div>

    <div class="interstitial-pagination">
    </div>

    <section class="interstitial explore">
        <div class="content">
            <h1 class="_animate" data-animate="fadeInDown">EXPLORE</h1>
            <div class="copy _animate" data-animate="fadeInDown">
                <p>Travel to places you've only dreamt of stepping into. Journey into the unknown and discover wonders. </p>
            </div>
            <div class="next"></div>
        </div>
    </section>

    <section class="interstitial experience">
        <div class="content">
            <div class="prev"></div>
            <h1 class="_animate" data-animate="fadeInDown">EXPERIENCE</h1>
            <div class="copy _animate"  data-animate="fadeInDown">
                <p>Jump right into an experience of a lifetime. Don't look back. The time is now, and the place is here.</p>
            </div>
            <div class="next"></div>
        </div>
    </section>

    <section class="interstitial nolimit">
        <div class="content">
            <div class="prev"></div>
            <h1 class="_animate" data-animate="fadeInDown">LIMITLESS</h1>
            <div class="copy _animate" data-animate="fadeInDown">
                <p>It all begins when you step out of your comfort zone. Just keep going, and reward yourself for pushing your limits.</p>
            </div>

            <div class="next"></div>
        </div>
    </section>

    <section class="interstitial login">
        <div class="content">
            <div class="login-header _animate"  data-animate="fadeInDown">
                <div class="figure">
                    <img src="<?= BASE_URL ?>crossover_assets/img/login-go.png">
                </div>
                <div class="caption">
                    <p>Sign-up right now and open your eyes to a world of adventure, exploration, and rewards. </p>
                </div>
            </div>
            <div class="box _animate"  data-animate="fadeInUp">
                <form autocomplete="off" onsubmit="return validate(this)" method="POST" action="<?= BASE_URL ?>user/auth">
                    <h2>ALREADY REGISTERED?</h2>
                    <p>Please provide your log-in details below</p>
                    <div id="error"><?= $invalid_message ? '<span>'.$invalid_message.'</span>' : '' ?></div>
                    <input type="text" name="username" placeholder="Username" class="req input">
                    <input type="password" name="password" placeholder="Password" class="req input">

                    <div class="misc">
                        <a href="<?= BASE_URL ?>reset_password">Forgot your password?</a>

                        <p>Registered but haven't submitted your documents yet? <a href="<?= BASE_URL ?>submit_giid">Click here</a></p>
                    </div>

                    <button type="submit" class="button">LOGIN</button>
                    <a href="<?= BASE_URL ?>crossover/preview/user/login?#login" class="button button-preview">PREVIEW</a>
                </form>
            </div>
        </div>


    </section>

    <section class="main register">
        <div class="lytebox-wrap">
            <div class="popup lytebox-wrapped-content" id="popup5">
                <div class="content text-center">
                    <button class="popup-close">&times;</button>
                    <form autocomplete="off" onsubmit="return submit_form()" class="content" method="POST">
                        <h2>UPLOAD GIID</h2>
                        <p>ENTER YOUR E-MAIL TO UPLOAD YOUR GIID DOCUMENT</p>
                        <div id="error" style="display:none; margin:15px"> <span></span> </div>
                        <p><input autocomplete="off" type="text" name="email" placeholder="E-MAIL" class="req"></p>
                        <button class="button" type="button">SUBMIT</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

<style>
    .button-preview:hover {
        text-decoration: none !important;
        color: #FFF;
    }
</style>

<script type="text/javascript">
    $(function(){

        <?php if(isset($invalid_message)) { ?>
            loginInit.skip();
        <?php } ?>

        <?php if($this->session->userdata('response_message_error')){ ?>
            popup.open({title: "<span style='color:red'><center>ERROR:</center></span>", message:"<span style='font-family: 'Maybebold'; 25px;'><center><?php echo $this->session->userdata('response_message_error'); ?></center></span>"});
            loginInit.skip();
        <?php $this->session->set_userdata('response_message_error', ''); } ?>

        <?php if(isset($errorUrl)){ ?>
            popup.open({title: "<span style='color:red'><center>ERROR:</center></span>", message:"<span style='font-family: 'Maybebold'; 25px;'><center>AN ERROR OCCURED. PLEASE TRY AGAIN!</center></span>"});
            loginInit.skip();
        <?php }else if(isset($successUrl)){ ?>
            popup.open({title: "<center>CONGRATULATIONS!</center>", message:"<span style='font-family: 'Maybebold'; 25px;'><center>You have successfully changed your password. You may now login.</center></span>"});
            loginInit.skip();
        <?php }else if(isset($invalidToken)){ ?>
            popup.open({title: "<span style='color:red'><center>ERROR:</center></span>", message:"<span style='font-family: 'Maybebold'; 25px;'><center>INVALID TOKEN!</center></span>"});
            loginInit.skip();
        <?php }else if(isset($successForgot)){ ?>
            popup.open({title: "<center>FORGOT PASSWORD</center>", message:"<span style='font-family: 'Maybebold'; 25px;'><center>We have sent you an email.</center></span>"});
            loginInit.skip();
        <?php } ?>
    });

</script>

<script type="text/javascript">
jQuery(document).ready(function($) {
    var words = ['Exploring environments','Equipping adventure gear','Starting the engine','Setting the sails up','Checking camera settings','Fixing the kayaks','Calculating your kilometers','Navigating the map','Accessing network'];
    $(function() {
          setInterval(function() {
            var thisWord = words[Math.floor(Math.random() * words.length)];
            $(".textcontent span").html(thisWord);
          },3000);
    });
});
</script>

<script type="text/javascript">



$(function(){

   if(window.location.hash) {
         var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
         if(hash == 'login'){
            loginInit.skip();
            parent.location.hash = '';
         }
      }

});

function submit_form(){

    popup.loading();

    $.ajax({
      type: "POST",
      url: BASE_URL + "user/submit_track_code",
      data: {email:$('input[name="email"]').val()},
      dataType: 'json',
      success: function(response){

            popup.close();

            $('input[name="email"]').val('');

            if(response.error==1){

                $('#error').html("<span>"+response.message+"</span>");

            }else{

                popup.open({
                        title : "Success",
                        align : "center",
                        message : response.message,
                        type: "alert",
                        buttonAlign : 'center',
                        onClose : function(){

                            window.location = BASE_URL + "user/login";

                        }
                    });

            }


        }

    });


    return false;

}

</script>
