<form id="form-reg" class="registration" autocomplete="off" target="upload-giid-target" action="<?= BASE_URL ?>user/submit_late_giid" enctype="multipart/form-data" method="POST">
<div class="container">
    <div class="text-center">
        <p>
             Registration is free. Join us and earn access to exclusive content and special offers. To complete the registration process, a copy of a valid government-issued ID has to be submitted for age verification. Upon verification, an email will be sent to you to activate your account. For accuracy of registered data, please review and confirm the details provided.
        </p>
    </div>
       <div id="error" style="display:none; margin:15px"> <span></span> </div>
        <!-- REGISTRATION PART 3 -->
        <div class="row registration-step-3  steps" style="display:block;">
            <p class="text-center">PLEASE ATTACH A SOFT COPY OF YOUR VALID GOVERNMENT-ISSUED ID (GIID) <a href="#" style="color:#EC1D2D " onclick="popup.show({html:'#popup2'})">SEE VALID GIID LIST</a></p>
            <div class="col-md-12">
                <div class="col-md-6">
                    <span class="label"> <strong> SELECT GIID TYPE</strong></span>
                    <span class="input">
                        <select name="giid_type" id="giid_type" class="req" title="Please select GIID type.">
                            <option value="">Loading GIID Types...</option>
                        </select>
                    </span>
                </div>
                <div class="col-md-6">
                    <span class="label"><strong> GIID NUMBER</strong></span>
                    <span class="input"><input class="req" title="Please input GIID number." name="giid_number" type="text" autocomplete="off"></span>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6 col-md-offset-3 text-center">
                    <p>To continue with registration, please submit a copy of your GIID within seven (7) days. Failure to do so will restart the  registration process.</p>
                    <button class="button" type="button" onclick="popup.show({html:'#popup1'})">UPLOAD GIID NOW</button>
                  <!--   <button class="button" type="button" id="upload-gid-later">UPLOAD GIID LATER</button> -->
                </div>
                <div class="clearfix"></div>
                <h3>By clicking the "CONFIRM" button below, I hereby declare and confirm that:</h3>
               <ol>
                    <li>The information I provided is accurate.</li>
                    <li>I am an adult smoker 18 years old or above.</li>
                    <li>I am a resident of the Philippines.</li>
                    <li>The contact number(s) and email address I provided are used personally and exclusively by me and cannot be accessed or shared by other persons.</li>
                    <li>I have read, and I affirm my agreement to, the Terms and Conditions and Privacy Statement and Consentof this Website.</li>
                    <li>I affirm giving my consent to PMFTC Inc. ("PM") to use my personal information as stated in the Privacy Statement and Consent, including but not limited to:
                        <ol class="abc" style="list-style-type:lower-alpha !important;">
                            <li>Storing and using my personal information to allow PM and its affiliates and duly authorized representatives to verify my age and identity in compliance with applicable laws and internal policies and/or to contact me</li>
                            <li>Providing me, using mail, email, SMS, telephone, etc., with communications related to tobacco products, including but not limited to information and material on brand launches, packaging changes, events, marketing activities, regulation of tobacco products and political topics that may be of relevance to tobacco products;</li>
                            <li>Processing my information in countries other than the country in which I live (for example, locating the database in one country and accessing the database remotely from another); and</li>
                            <li>Obtain publicly available information I posted on social media sites to allow PM to better understand me and tailor their messages and activities;</li>
                            <li>Disclosing my personal information to service providers for the above purposes, to PM affiliates to do the above for their own purposes, and (if required by law) to relevant authorities.</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="text-center clearfix">
                <button class="button" type="submit">CONFIRM</button>
            </div>
        </div>

    <!-- end container -->
</div>
<div class="lytebox-wrap">
    <div class="popup lytebox-wrapped-content" id="popup1">
        <div class="content text-center">
            <button class="popup-close" type="button">&times;</button>
            <h2>UPLOAD YOUR VALID GOVERNMENT-ISSUED ID</h2>
            <small>Only pdf, jpg, png and gif formats will be accepted</small>
            <br>
<!--
            <input class="m-auto " type="file" id="giid" onchange="PreviewImage();">  <br>
            <div class="browse">
                <input type="text" placeholder="Browse for file (Max file size: 2MB)" readonly>
            </div>
            <div class="upload-preview"></div>
            <a onclick="popup.show({html:'#popup4'})" class="visible-md visible-lg">Upload via webcam</a> -->

            <input class="m-auto hidden" type="file" id="giid" name="giid" onchange="PreviewImage();">
            <div class="browse">
                <input type="text" placeholder="Browse for file (Max file size: 2MB)" readonly>
            </div>
            <div class="upload-preview"></div>
            <button type="button" class="button webcam" id="show-webcam">UPLOAD VIA WEBCAM</button>

            <br>
            <button class="button" type="button" onclick="popup.close()">SUBMIT</button>
        </div>
    </div>
    <div class="popup lytebox-wrapped-content" id="popup2">
        <div class="content text-center">
            <button class="popup-close" type="button">&times;</button>
            <h2>VALID GOVERNMENT-ISSUED ID LIST</h2>
            <div class="row">
                <ul class="col-sm-5 col-sm-offset-1 list-unstyled">
                    <li>Passport</li>
                    <li>Driver's License</li>
                    <li>NBI Clearance</li>
                    <li>SSS ID (with birthday only)</li>
                </ul>
                <ul class="col-sm-6 list-unstyled">
                    <li>Voter's ID</li>
                    <li>PRC ID</li>
                    <li>Unified Multi-Purpose ID</li>
                    <li>Alien Certificate of Registration</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="popup lytebox-wrapped-content" id="popup3">
        <div class="content text-center">
            <button class="popup-close" type="button">&times;</button>
            <br>
            <h3>Thank you for your submission.</h3>
            <p></p>
            <p>
                A tracking code for your registration will be sent to your registered email address. Please don't forget to submit a copy of a valid government-issued ID within seven (7) days. Your registration will only be processed upon submission of a valid government-issued ID.
            <p>
            <button class="button" type="button" onclick="closePopup('user/login');">OK</button>
        </div>
    </div>
    <div class="popup lytebox-wrapped-content" id="popup5">
        <div class="content text-center">
            <button class="popup-close" type="button">&times;</button>
            <br>
            <h3>Thank you for your submission.</h3>
            <p></p>
            <p>
                Thank you for registering to Marlboro.ph. Please allow us seven (7) days to verify your registration details. You will receive an e-mail with your log-in details once your membership has been verified.
            <p>
            <button class="button" type="button" onclick="closePopup('user/login');">OK</button>
        </div>
    </div>
    <div class="popup lytebox-wrapped-content" id="popup4">
        <div class="content text-center">
            <button class="popup-close" type="button">&times;</button>
            <h2>UPLOAD YOUR VALID GOVERNMENT-ISSUED ID</h2>
            <div class="upload-webcam" id="flashcontent"></div>
            <button class="button" type="button">TAKE PHOTO</button>
            <button class="button" type="button">SUBMIT</button>
        </div>
    </div>
</div>
<input type="hidden" name="captured_image_ba" id="captured-image-ba">
<input type="hidden" name="__token" id="__token" value="<?= $token ?>">
</form>
<iframe name="upload-giid-target" id="upload-giid-target" width="0" height="0" border="0"></iframe>
