
<section class="subpage">
	<div class="box">
    <form autocomplete="off" class="content" onsubmit="return validate(this)" action="<?= BASE_URL ?>xp_user/forgot_submit" method="POST">
		<h2>FORGOT PASSWORD?</h2>
		<p>ENTER YOUR E-MAIL TO RESET YOUR PASSWORD</p>
        <div id="error"><?= $invalid_forgot_email ? '<span>'.$invalid_forgot_email.'</span>' : '' ?></div>
		<input autocomplete="off" type="text" name="email" placeholder="Email Address" class="req input">

		<div class="misc">
			<a href="<?= BASE_URL ?>xp_user/login" class="medium">Back to login page</a>
		</div>

		<button class="button" type="submit">SUBMIT</button>
    </form>
	</div>
</section>

<script type="text/javascript">
function validate(form){
    var error = false;
    $('.req',form).each(function(){
        var el = $(this);
        var val = $.trim(el.val());
        if( val == '' ){
            error = 'Please input your e-mail address.';
            return false;
        }
    });
    if( error ){
        $('#error').html('<span>'+error+'</span>');
        $('html, body').animate({scrollTop:0})
        return false;
    }
}
</script>
