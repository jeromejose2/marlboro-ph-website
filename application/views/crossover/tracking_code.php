
<section class="subpage">
	<div class="box">
		<form autocomplete="off" onsubmit="return submit_form()" class="content" method="POST">
			<h2>UPLOAD GIID?</h2>
			<p>ENTER YOUR E-MAIL TO UPLOAD YOUR GIID DOCUMENT</p>
			<div id="error"></div>
			<input autocomplete="off" type="text" name="email" placeholder="Email Address" class="input req">
			<br>
			<div class="misc">
				<a href="<?= BASE_URL ?>user/login"> Back to login page</a>
			</div>
			<button type="submit"  class="button">SUBMIT</button>
		</form>
	</div>
</section>

<script type="text/javascript">


function submit_form(){

	popup.loading();

	$.ajax({
	  type: "POST",
	  url: BASE_URL + "user/submit_track_code",
	  data: {email:$('input[name="email"]').val()},
	  dataType: 'json',
	  success: function(response){

			popup.close();

			$('input[name="email"]').val('');

			if(response.error==1){

				$('#error').html("<span>"+response.message+"</span>");

			}else{

				popup.open({
						title : "Success",
						align : "center",
						message : response.message,
						type: "alert",
						buttonAlign : 'center',
						onClose : function(){

							window.location = BASE_URL + "user/login";

						}
					});

			}


		}

	});


	return false;

}

</script>
