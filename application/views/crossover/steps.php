<div class="scroll-reg-content">
<form id="form-reg" class="registration" autocomplete="off" target="upload-giid-target" action="<?= BASE_URL ?>user/confirm" enctype="multipart/form-data" method="POST">
<div class="container">
    <div class="text-center">
        <p >
                    Registration is free. Join us and earn access to exclusive content and special offers. To complete the registration process, a copy of a valid government-issued ID has to be submitted for age verification. Upon verification, an email will be sent to you to activate your account. For accuracy of registered data, please review and confirm the details provided.
                </p>
    </div>

       <div id="error" style="display:none; margin:15px"> <span></span> </div>

        <div class="row registration-step-1 steps active">
            <div class="col-md-6">
                <ul>
                    <li>
                        <span class="label">  <strong>* ARE YOU A SMOKER?</strong> </span>
                        <span class="input">
                            <label class="radio-label"><input type="radio" class="req" value="1" name="smoker" title="Are you a smoker? Please select your answer."> Yes</label>
                            <label class="radio-label"><input type="radio" class="req" value="0" id="not-smoker" name="smoker" title="Are you a smoker? Please select your answer."> No</label>
                        </span>
                    </li>
                    <li>
                        <span class="label"> <strong> * NAME</strong></span>
                    </li>
                    <li>
                        <span class="label">Nick Name</span>
                        <span class="input"><input type="text" maxlength="15" autocomplete="off" name="nname" class="req" title ="Please enter your nick name."></span>
                    </li>
                    <li>
                        <span class="label">First Name</span>
                        <span class="input"><input type="text" maxlength="15" autocomplete="off" name="fname" class="req" title ="Please enter your first name."></span>
                    </li>
                    <li>
                        <span class="label">Middle Name</span>
                        <span class="input"><input type="text" maxlength="15" autocomplete="off" name="mname"></span>
                    </li>
                    <li>
                        <span class="label">Last Name</span>
                        <span class="input"><input type="text" nmaxlength="15" autocomplete="off" name="lname" class="req" title ="Please enter your last name."></span>
                    </li>
                    <li>
                        <span class="label"> <strong>* BIRTHDAY</strong> </span>
                        <span class="input"><input type="text" autocomplete="off" readonly="readonly" name="birthday" id="birthdate" class="req" title ="Please enter your birthdate."> </span>
                    </li>
                    <li>
                        <span class="label"> <strong> * GENDER</strong> </span>
                        <span class="input">
                            <label class="radio-label"><input type="radio" value="M" name="gender" class="req" title="Select your gender."> Male</label>
                            <label class="radio-label"><input type="radio" value="F" name="gender" class="req" title="Select your gender."> Female</label>
                        </span>
                    </li>
                    <li>
                        <span class="label"> <strong>* MOBILE NUMBER </strong></span>
                        <span class="input">
                            <select class="select-s req" name="mobile_prefix" title="Please enter your mobile number.">
                                <option value="">---</option>
                                <?php foreach ($mobile_prefixes as $prefix): ?>
                                <option value="<?= $prefix['mobile_prefix'] ?>"><?= $prefix['mobile_prefix'] ?></option>
                                <?php endforeach ?>
                            </select>
                            <input type="text" autocomplete="off" id="mobile_number" class="input-m req" name="mobile_number" maxlength="7"  title="Please enter your mobile number.">
                        </span>
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul>

                    <li>
                        <span class="label"> <strong>* EMAIL  ADDRESS</strong></span>
                        <span class="input"><input type="text" autocomplete="off" id="email" class="req" name="email" value="<?=$email?>" title="Please enter your email address."> </span>
                    </li>
                    <li>
                        <span class="label"><strong>* MAILING ADDRESS</strong></span>
                    </li>
                    <li>
                        <span class="label">House and Street</span>
                        <span class="input"><input type="text" autocomplete="off" class="req" name="street" title="Please enter your House and Street details"></span>
                    </li>
                    <li>
                        <span class="label">Barangay</span>
                        <span class="input"><input type="text" autocomplete="off" class="req" name="brgy" title="Please enter your barangay."></span>
                    </li>
                    <li>
                        <span class="label">Province </span>
                        <span class="input">
                            <select class="req" id="province-select" name="province" title="Select a Province.">
                                <option value="">Province</option>
                                    <?php foreach ($provinces as $province): ?>
                                    <option value="<?= $province['province_id'] ?>"><?= $province['province'] ?></option>
                                    <?php endforeach ?>
                            </select>
                        </span>
                    </li>
                    <li>
                        <span class="label">City</span>
                        <span class="input">
                        <select class="req" id="cities-select" name="city" title="Select a City.">
                            <option>City</option>
                        </select>
                        </span>
                    </li>
                    <li>
                        <span class="label">Zip Code</span>
                        <span class="input"><input type="text" maxlength="4" autocomplete="off" class="req" name="zip_code" title="Please enter your zip code."></span>
                    </li>
                    <li>
                        <span class="label"> <strong>REFERRAL CODE:</strong> </span>
                        <span class="input"> <input name="referred_code" type="text" value="<?=$referral_code?>" autocomplete="off" style="margin-bottom: 5px"><span style="color: white;">Referral code is optional. You may enter a referral code if an existing member invited you to join.</span> </span>
                    </li>
                    <?php /* <li>
                        <span class="label"> <strong>OUTLET CODE:</strong></span>
                        <span class="input"> <input id="reg-outlet-code" name="outlet_code" type="text" autocomplete="off"></span>
                    </li> */ ?>
                </ul>
            </div>
            <div class="clearfix"></div>
            <div class="text-center">
                <button type="button" class="button" id="reg-part-1">NEXT</button>
            </div>
        </div>
        <!-- REGISTRATION PART 2 -->
        <div class="row registration-step-2  steps">
            <div class="col-md-6">
                <ul>
                    <li>
                        <span class="label"><strong>* WHAT BRAND DO YOU SMOKE MOST FREQUENTLY?</strong></span>
                        <span class="input">
                            <select name="current_brand" id="current_brand" class="req" title="Select a Primary Brand.">
                                <option value="">Loading brand...</option>
                            </select>
                        </span>
                    </li>
                    <li>
                        <span class="label"><strong> * WHAT OTHER BRANDS DO YOU SMOKE ASIDE FROM YOUR REGULAR BRAND?</strong></span>

                        <span class="input">
                            <select name="first_alternate_brand" id="first_alternate_brand" class="req" title="Select a Secondary Brand.">
                                <option value="">Loading brand...</option>
                            </select>
                        </span>
                    </li>
                    <li class="brand-unavailable">
                        <span class="label"><strong> * WHAT WHOULD YOU DO IF YOUR REGULAR BRAND IS UNAVAILABLE?</strong></span>
                        <span class="input">
                            <?php foreach ($alternate_purchase as $purchase): ?>
                            <label class="radio-label">
                                <input class="req" value="<?= $purchase['alternate_id'] ?>" title="Choose your answer." name="alternate_purchase" type="radio">
                                <span><?= $purchase['alternate_value'] ?></span>
                            </label>
                            <?php endforeach ?>
                        </span>
                    </li>
                </ul>

            </div>
            <br>
            <div class="clearfix"></div>
            <div class="text-center">
                <button type="button" class="button" id="reg-part-2">NEXT</button>
            </div>
        </div>

        <!-- REGISTRATION PART 3 -->
        <div class="row registration-step-3  steps">
            <p class="text-center">PLEASE ATTACH A SOFT COPY OF YOUR VALID GOVERNMENT-ISSUED ID (GIID) <a href="#" style="color:#EC1D2D " onclick="popup.show({html:'#popup2'})">SEE VALID GIID LIST</a></p>
            <div class="col-md-12">
                <div class="col-md-6 col-sm-12 giid-no-float">
                    <span class="label"> <strong> SELECT GIID TYPE</strong></span>
                    <span class="input">
                        <select name="giid_type" id="giid_type" class="req" title="Please select GIID type.">
                            <option value="">Loading GIID Types...</option>
                        </select>
                    </span>
                </div>
                <div class="col-md-6 col-sm-12 giid-no-float">
                    <span class="label"><strong> GIID NUMBER</strong></span>
                    <span class="input"><input class="req" title="Please input GIID number." name="giid_number" type="text" autocomplete="off"></span>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6 col-md-offset-3 text-center">
                    <p>To continue with registration, please submit a copy of your GIID within seven (7) days. Failure to do so will restart the  registration process.</p>
                    <button class="button" type="button" onclick="popup.show({html:'#popup1'})">UPLOAD GIID NOW</button>
                    <button class="button" type="button" id="upload-gid-later">UPLOAD GIID LATER</button>
                </div>
                <div class="clearfix"></div>
                <h3>By clicking the "CONFIRM" button below, I hereby declare and confirm that:</h3>
                <ol>
                    <li>The information I provided is accurate.</li>
                    <li>I am an adult smoker 18 years old or above.</li>
                    <li>I am a resident of the Philippines.</li>
                    <li>The contact number(s) and email address I provided are used personally and exclusively by me and cannot be accessed or shared by other persons.</li>
                    <li>I have read, and I affirm my agreement to, the Terms and Conditions and Privacy Statement and Consentof this Website.</li>
                    <li>I affirm giving my consent to PMFTC Inc. ("PM") to use my personal information as stated in the Privacy Statement and Consent, including but not limited to:
                        <ol class="abc" style="list-style-type:lower-alpha !important;">
                            <li>Storing and using my personal information to allow PM and its affiliates and duly authorized representatives to verify my age and identity in compliance with applicable laws and internal policies and/or to contact me</li>
                            <li>Providing me, using mail, email, SMS, telephone, etc., with communications related to tobacco products, including but not limited to information and material on brand launches, packaging changes, events, marketing activities, regulation of tobacco products and political topics that may be of relevance to tobacco products;</li>
                            <li>Processing my information in countries other than the country in which I live (for example, locating the database in one country and accessing the database remotely from another); and</li>
                            <li>Obtain publicly available information I posted on social media sites to allow PM to better understand me and tailor their messages and activities;</li>
                            <li>Disclosing my personal information to service providers for the above purposes, to PM affiliates to do the above for their own purposes, and (if required by law) to relevant authorities.</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="text-center clearfix">
                <button class="button" type="submit">CONFIRM</button>
            </div>
        </div>

    <!-- end container -->
</div>
<div class="lytebox-wrap">
    <div class="popup lytebox-wrapped-content" id="popup1">
        <div class="content text-center">
            <button class="popup-close" type="button">&times;</button>
            <h2>UPLOAD YOUR VALID GOVERNMENT-ISSUED ID</h2>
            <small>Only pdf, jpg, png and gif formats will be accepted</small>
            <br>
<!--
            <input class="m-auto " type="file" id="giid" onchange="PreviewImage();">  <br>
            <div class="browse">
                <input type="text" placeholder="Browse for file (Max file size: 2MB)" readonly>
            </div>
            <div class="upload-preview"></div>
            <a onclick="popup.show({html:'#popup4'})" class="visible-md visible-lg">Upload via webcam</a> -->

            <input class="m-auto hidden" type="file" id="giid" name="giid" onchange="PreviewImage();">
            <div class="browse">
                <input type="text" placeholder="Browse for file (Max file size: 2MB)" readonly>
            </div>
            <div class="upload-preview"></div>
            <button type="button" class="button webcam" id="show-webcam">UPLOAD VIA WEBCAM</button>

            <br>
            <button class="button" type="button" onclick="popup.close()">SUBMIT</button>
        </div>
    </div>
    <div class="popup lytebox-wrapped-content" id="popup2">
        <div class="content text-center">
            <button class="popup-close" type="button">&times;</button>
            <h2>VALID GOVERNMENT-ISSUED ID LIST</h2>
             <div class="row">
                <ul class="col-sm-5 col-sm-offset-1 list-unstyled">
                    <li>Passport</li>
                    <li>Driver's License</li>
                    <li>NBI Clearance</li>
                    <li>SSS ID (with birthday only)</li>
                </ul>
                <ul class="col-sm-6 list-unstyled">
                    <li>Voter's ID</li>
                    <li>PRC ID</li>
                    <li>Unified Multi-Purpose ID</li>
                    <li>Alien Certificate of Registration</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="popup lytebox-wrapped-content" id="popup3">
        <div class="content text-center">
            <button class="popup-close" type="button">&times;</button>
            <br>
            <h3>Thank you for your submission.</h3>
            <p></p>
            <p>
                Please don't forget to submit a copy of a valid government-issued ID within seven (7) days. Your registration will only be processed upon submission of a valid government-issued ID.
            </p>
            <p>
                For the meantime, you may access the <a href="<?php echo site_url('crossover/preview/user/login') ?>" target="_blank">preview website</a> by entering the code below:
            </p>
            <p>
                <b><?php echo $this->session->flashdata('login_code'); ?></b>
            </p>
            <p>
                <small><em>Please note that this code will expire in 7 days after your first login.</em></small>
            </p>
            <button class="button" type="button" onclick="closePopup('user/login');">OK</button>
        </div>
    </div>
    <div class="popup lytebox-wrapped-content" id="popup4">
        <div class="content text-center">
            <button class="popup-close" type="button">&times;</button>
            <h2>UPLOAD YOUR VALID GOVERNMENT-ISSUED ID</h2>
            <div class="upload-webcam" id="flashcontent"></div>
            <button class="button" type="button" id="webcam-take-photo">TAKE PHOTO</button>
            <button class="button" type="button" id="webcam-photo-submit" style="display:none;">SUBMIT</button>
        </div>
    </div>
</div>
<input type="hidden" name="captured_image_ba" id="captured-image-ba">
<input type="hidden" name="__token" id="__token" value="<?= $token ?>">
</form>
<iframe name="upload-giid-target" id="upload-giid-target" width="0" height="0" border="0"></iframe>
</div>
