<!DOCTYPE html>
<html lang="en">
<head>
	<title>Marlboro</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<base href="<?=BASE_URL?>">
	<noscript>
		<meta http-equiv="refresh" content="0;noscript">
	</noscript>
	<link rel="stylesheet" href="styles/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="styles/lytebox.css">
	<link type="text/css" href="styles/jquery.jscrollpane.css" rel="stylesheet" media="all" />
	<link rel="stylesheet" type="text/css" href="styles/mainstyle.css">
	<link rel="stylesheet" href="styles/owl.carousel.css">
	<link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-touch-icon-152x152.png">
	<link rel="icon" type="image/png" href="images/favicon/favicon-196x196.png" sizes="196x196">
	<link rel="icon" type="image/png" href="images/favicon/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="images/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="images/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="images/favicon/favicon-16x16.png" sizes="16x16">
	<meta name="msapplication-TileColor" content="#ab0702">
	<meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
	<script src="scripts/jquery.min.js"></script>
	<script src="scripts/jFormslider.js"></script>


	<script>
		window.BASE_URL = "<?= BASE_URL ?>";
		window.DEFAULT_IMAGE = "<?= DEFAULT_IMAGE ?>";
		window.PROD_MODE = <?= (int) PROD_MODE ?>;
		window.Origin = {
			MOVE_FWD : <?= MOVE_FWD ?>,
			MOVE_FWD_GALLERY : <?= MOVE_FWD_GALLERY ?>,
			MOVE_FWD_COMMENTS_PHOTOS : <?= MOVE_FWD_COMMENTS_PHOTOS ?>,
			MOVE_FWD_COMMENT_REPLIES_PHOTOS : <?= MOVE_FWD_COMMENT_REPLIES_PHOTOS ?>
		};
		<?php if (!DEV_MODE): ?>
		$(document).bind("contextmenu", function() {
			return false;
		});
		<?php endif ?>

	</script>

	<!-- RUBY BURST -->

	<!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="<?= base_url().'/rubyburst2/marl-includes/vendor.css' ?>">


    <link rel="stylesheet" href="<?= base_url() ?>rubyburst2/css/main_rubyburst.css" />

	<!-- EOF RUBYBURST -->
	<body>
	<!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <header id="header" class="main text-uppercase home-mode">
        <div class="container">
            <!-- <a class="logo" href="<?= base_url() ?>"><span class="sr-only">MARLBORO LOGO</span></a> -->
            <div class="green-line"></div>
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-collapse collapse" id="nav-collapse">
                <ul class="main-nav nav navbar-nav navbar-right" role="tablist">
                    <li class="dropdown">
                        <a href="<?= base_url() ?>about/product_info" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ABOUT <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= base_url() ?>about/dont-be-a-maybe">Don't be a maybe</a></li>
                            <li><a href="<?= base_url() ?>about/brand_history">Brand History</a></li>
                            <li><a href="<?= base_url() ?>about/product_info">Product Info</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="<?= base_url() ?>events" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">BACKSTAGE PASS <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= base_url() ?>events/hot">WHAT'S HOT</a></li>
                            <li><a href="<?= base_url() ?>events/upcoming">UPCOMING EVENTS</a></li>
                            <li><a href="<?= base_url() ?>events/past">PAST EVENTS</a></li>
                            <li><a href="<?= base_url() ?>events/videos">VIDEOS</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="<?= base_url() ?>events" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">CROSS|OVER <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= base_url() ?>finalists">FINALISTS</a></li>
                            <li><a href="<?= base_url() ?>events/past">GALLERY</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="<?= base_url() ?>perks" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">PERKS <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= base_url() ?>perks/buy">BUY</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="<?= base_url() ?>move_forward" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">MOVEFWD <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= base_url() ?>move_forward">ACTIVE OFFERS</a></li>
                            <li><a href="<?= base_url() ?>move_forward/past">PAST OFFERS</a></li>
                        </ul>
                    </li>
                    <li class="hidden-xs"><a href="<?= base_url() ?>games">GAMES</a></li>
                    <li class="dropdown"><a href="<?= base_url() ?>profile">MY ACCOUNT</a></li>
                    <li id="profiling_nav"></li>
                </ul>
                <ul class="sub-nav nav navbar-nav navbar-right">
                    <li><a href="<?= base_url() ?>points">POINTS  :  <span id="points"><?= $this->session->userdata('user_points') ?></span><span class="glyphicon glyphicon-cog"></span></a></li>
                    <li>
                        <a id="notifications-link" href="<?= base_url() ?>notifications">NOTIFICATIONS <span class="glyphicon glyphicon-exclamation-sign"></span>
                        </a>
                    </li>
                    <li><a href="<?= base_url() ?>faq"> FAQ <span class="glyphicon glyphicon-question-sign"></span></a></li>
                    <li><a href="<?= base_url() ?>welcome/logout">LOGOUT <span class="glyphicon glyphicon-log-out"></span></a> </li>
                </ul>
            </div>
        </div>
    </header>
    <section class="main splash full-body" data-bg="img/home-bg-alt-large.jpg">

    	<?php
			$source = $this->uri->segment(1);

			if($this->uri->segment(1)=='events'){
				$source = 'backstage_pass';
			}else if(!$this->uri->segment(1)){
				$source = "welcome";
			}

			$hidden_pack = $this->session->userdata($source);

			?>
    	
    	<?php echo $main_content; ?>
    
    </section>
	<!-- <footer class="main">
		<div class="r18">
			This website is for adult smokers 18 years or older residing in the Philippines. Access to this website is subject to age verification
		</div>

		<div class="row">
			<nav class="col-md-6 col-sm-8">
				<a onclick="popup.open({url:'<?= BASE_URL ?>popups/terms.html'})">terms and conditions</a>
			 	<a onclick="popup.open({url:'<?= BASE_URL ?>popups/privacy-content.html'})">Privacy Statement and Consent</a>
			 	<a onclick="popup.open({url:'<?= BASE_URL ?>popups/contactus.html'})">contact us</a>
			 	<a target="_blank" href="http://www.pmi.com/eng/our_products/health_effects_of_smoking/pages/health_effects_of_smoking.aspx">smoking and health</a>
			</nav>

			<div class="col-md-6 col-sm-4 copyright">copyright &copy; 2013 PMFTC INC. All rights reserved</div>
		</div>
		<div class="government-warning">
			<span>GOVERNMENT WARNING: CIGARETTE SMOKING IS DANGEROUS TO YOUR HEALTH</span>
		</div>
	</footer> -->

	<footer id="footer" class="text-uppercase login-mode">
    <div class="container">
      <div class="green-line"></div>
    </div>
    <section class="container-footer-actions">
      <div class="container-age-warning">
        <div class="container text-center">
          <p>This website is for adult smokers 18 years or older residing in the Philippines. Access to this website is subject to age verification.</p>
        </div>
      </div>
      <div class="container-links-copyright">
        <div class="container">
          <div class="container-footer-links col-md-6 col-sm-7 col-xs-12">
            <nav>
              <ul class="list-unstyled list-inline">
                <li><a id="trigger-terms" href="#">Terms and Conditions</a></li>
                <li><a id="trigger-privacy" href="#">Privacy Policy</a></li>
                <li><a id="trigger-contact" href="#">Contact Us</a></li>
                <li><a target="_blank" href="http://www.pmi.com/eng/our_products/health_effects_of_smoking/pages/health_effects_of_smoking.aspx">Smoking and Health</a></li>
              </ul>
            </nav>
          </div>
          <div class="container-copyright col-md-4 col-sm-5 col-xs-12">
            <p>Copyright © 2016 pmftc inc. all rights reserved</p>
          </div>
        </div>
      </div>
    </section>
    <section class="container-govt-warning text-center">
      <p>Government warning: Cigarette smoking <span>is dangerous to your health</span></p>
    </section>
  </footer>


  <div id="popup-terms" class="std-popup mfp-hide">
    <div class="pop-container">
      <div class="pop-header">
        <h3>Terms &amp; Conditions</h3>
      </div>
      <div class="pop-contents">
      <p>This website (the "<b>Website</b>") is owned and operated by or on behalf of PMFTC Inc. ("<b>PM</b>") based
        in the Philippines for verified adult smokers who have been provided with a personal username
        and password of access. In the course of logging onto the Website, you will have been asked
        to read and agree to these Terms and Conditions. Therefore, by logging onto the Website, you
        will be assumed to have read and agreed to these Terms and Conditions. </p><h4>Website Users</h4><p>The Website is intended for verified adult smokers who have been provided with a username
        and password for access. Your username and password are strictly personal and must not be
        shared with anyone else. </p><h4>Use of the Website and Materials</h4><p>The Website and all its contents (including but not limited to messages posted on the forum,
        if any, software, files, graphics, data, images and other material) ("<b>Material</b>") are the exclusive
        property of PM, its affiliates, and/or its licensors and are protected by the intellectual property
        laws of the Philippines as well as in other countries. The Website and the Material are provided
        solely for your personal use. Any other use of the Website or the Material, including but not
        limited to:</p><ul>
          <li>using the Website or Materials for business purposes; </li>
          <li>distributing, sharing, copying, reproducing or publishing the Material; or</li>
          <li> modifying, distributing, transmitting, performing, broadcasting, reproducing, publishing
            licensing, or reverse engineering parts or all of the Website,
            without the prior written permission of PM, is expressly prohibited.</li>
        </ul><h4>Forum</h4><p>To the extent the Website includes a forum that allows interaction between users, you are
        hereby made aware (i) that PM, its affiliates and/or representatives acting on their behalf
        regularly monitors the information posted on the forum by users, and (ii) that PM reserves the
        right (which may be exercised at their sole discretion without notice) to delete, remove, move
        or edit comments or messages you have posted. PM and its affiliates also reserve themselves
        the right to terminate, or have terminated, your access to and use of the Website and/or forum
        at their sole discretion. You hereby waive any rights including any rights that you may have in
        regard to messages you have posted on this forum.</p><p>Without limiting the right of PM to reject or remove any comment or message posted in the
        Website, any comment or message you post or any submission you make must not: </p><ul>
          <li>contain contact information (i.e., phone numbers, etc.);</li>
          <li>contain swear words or other language likely to offend;</li>
          <li>be racist, sexist, sexually explicit, abusive, or otherwise objectionable;</li>
          <li>be derogatory to other users or other brands;</li>
          <li>break the law, or condones or encourages unlawful activity; </li>
          <li>refer to or contain a cigarette or the use of a cigarette;</li>
          <li>refer to or contain alcoholic products;</li>
          <li>refer to or contain a minor (i.e., a person less than eighteen (18) years old);</li>
          <li>refer to or contain a cartoon; or</li>
          <li>be otherwise unsuitable in any manner.</li>
        </ul><p>The comments made on the forum (or blog) shall focus on aspects unrelated to smoking
        and health. Smoking is dangerous and addictive. More information about this is
        available at: <a href="http://www.pmi.com/eng/our_products/health_effects_of_smoking/pages/&#10;&#9;&#9;&#9;health_effects_of_smoking.aspx" target="_blank">http://www.pmi.com/eng/our_products/health_effects_of_smoking/pages/
        health_effects_of_smoking.aspx</a> </p><p>Any comments made about smoking and health will be removed from the site.</p><p>You acknowledge that you are solely responsible for any information, data, personal views
        or beliefs, or any material whatsoever that you submit for display on the forum, including the
        accuracy, legality, reliability and appropriateness of all such information, data, personal views or
        material. In posting messages on the forum, you must not:</p><ul>
          <li>post, link or otherwise publish any messages containing any form of advertising or
            promotion for products or services, chain messages or “spam”;</li>

        <li>post, link to or otherwise publish any messages with recommendations to buy or refrain
          from buying a particular security or which contain confidential information of another
          party or which otherwise have the purpose of affecting the price or value of any traded security;</li>

        <li>post, link to or otherwise publish any messages that are unlawful, threatening,
          discriminatory, abusive, libelous, indecent, infringe copyright or other rights of third
          parties or which contain any form of illegal content;</li>

        <li>disguise the origin of any message;</li>

        <li>impersonate any person or entity (including forum users or hosts) or misrepresent any
          affiliation with any person or entity;</li>

        <li>post or otherwise publish any messages unrelated to the forum;</li>

        <li>post or transmit any messages that contain software viruses, files or code designed to
        interrupt, destroy or limit the functionality of the Website or any computer software or equipment;</li>

        <li>collect or store other users' personal data; or</li>

        <li>restrict or inhibit any other user from using the forum.</li>
        </ul><p>PM may contribute material to the forum in order to stimulate discussions and ensure the smooth running of the forum. </p><p>The forum may also contain messages from special guests invited by PM. The opinions of such
        guests are personal and do not necessarily represent the views of PM. Additionally, messages
        will be submitted by users over whom PM has no control. It is possible that some users will
        breach the above-listed rules, but PM has no responsibility in that regard. You acknowledge
        that PM cannot guarantee the accuracy, integrity or quality of the messages posted on the
        forum and that PM cannot be held liable for their contents. You also acknowledge that you bear
        all risk associated with your use of the forum and should not rely on messages in making (or
        refraining from making) any specific investment or other decisions. </p><h4>Posting Developed Materials</h4><p>You acknowledge, represent and warrant that all tangible and intangible materials, including
        texts, drawings, designs, photographs, videos, sketches, and all other materials and ideas,
        which you have posted on the Website ("<b>Developed Materials</b>") are your original work for which
        you have the related intellectual property rights or work to which you have obtained exhaustive
        intellectual property rights. You also acknowledge, represent and warrant that such Developed
        Materials do not and will not infringe the intellectual property rights of any third party.</p><p>  You acknowledge and agree that by posting the Developed Materials on the Website you
        relinquish all rights to such Developed Materials and that such Developed Materials shall
        belong to PM (unless PM notifies you otherwise) and shall be subject to PM’s and its designees’
        unrestricted use.</p><p>You acknowledge and agree that by posting Developed Materials on the Website, you
        automatically and irrevocably assign to PM all intellectual property rights therein, free and clear
        of any liens, claims or other encumbrances, to the fullest extent permitted by law, as from the
        date of creation of such Developed Materials and you waive any moral rights therein to the
        fullest extent permitted by law. </p><p>All Developed Materials may be used, altered, duplicated and/or reproduced in all forms and
        for all media, whether known or unknown, including all electronic media, worldwide, by PM
        and its designees. You hereby agree to any alteration, modification or combination of any
        Developed Materials. PM may combine any Developed Materials with any other elements,
        information, materials, works or designs to which PM has or may obtain rights from any source.
        You acknowledge and agree that you shall not receive any form of compensation for use of
        the Developed Materials in any manner and waive any right to be named as the author of such
        Developed Materials.</p><h4>Disclaimer</h4><p>Your use of the Website is at your sole risk. The Website is provided on an "as is" and "as
        available" basis. PM and its affiliates expressly disclaim all warranties of any kind, whether
        express or implied, including but not limited to the implied warranties that the Website and
        Materials are non-infringing, accurate and appropriate; that access to the Website will be
        uninterrupted and error-free; that the Website will be secure; that the Website or the servers
        that make the Website available will be virus-free; or that information on the Website will be
        complete, accurate, appropriate and timely. If you download any Materials from the Website,
        you do so at your own discretion and risk. You will be solely responsible for any damage to
        your computer system or loss of data that results from the download of any such Materials. In
        addition to the above, you assume the entire cost of all necessary servicing, repair or correction.</p><h4>Limitation of Liability</h4><p>To the fullest extent permitted under applicable law, you understand and agree that neither PM
        nor any of its respective affiliates, subsidiaries or third party content providers shall be liable for
        any direct, indirect, incidental, special, exemplary, consequential, punitive or any other damages
        relating to or resulting from your use or inability to use the Website or from any actions we take
        or fail to take as a result of your communication with us. These include damages for errors,
        omissions, inaccuracies, interruptions, defects, delays, computer viruses, your loss of profits,
        loss of data, unauthorized access to and alteration of your transmissions and data, and other
        tangible and intangible losses. </p><h4>Indemnification</h4><p>You agree to indemnify, defend and hold harmless PM and any of its affiliates or subsidiaries,
        and its officers, directors, employees, agents, licensors and suppliers from and against all
        losses, liabilities, expenses, damages and costs, including reasonable attorneys' fees and court
        costs, arising out of or resulting from any violation of these Terms and Conditions (including but
        not limited to negligent or wrongful conduct) by you or any other person accessing the Website
        using your username and password. If you cause a technical disruption of the Website or the
        systems transmitting the Website to you or others, you agree to be responsible for any and all
        losses, liabilities, expenses, damages and costs, including reasonable attorneys' fees and court
        costs, arising or resulting from that disruption.</p><h4>Jurisdiction and Governing Law</h4><p>The laws of the Philippines govern these Terms and Conditions and your use of the Website,
        and you irrevocably consent to the exclusive jurisdiction of the courts located in Tanauan
        City, Batangas, for any action to enforce these Terms and Conditions. The Website has been
        designed to comply with the laws of the Philippines. </p><h4>How to Contact Us </h4><p>For any questions you may have about these Terms and Conditions you can contact our adult-
        restricted hotline at 895-9999 (Metro Manila) or 1-800-10-895-9999 (Provincial Toll-Free).</p>
      </div>
    </div>
  </div>
  <div id="popup-privacy" class="std-popup mfp-hide">
    <div class="pop-container">
      <div class="pop-header">
        <h3>Privacy Statement &amp; Consent</h3>
      </div>
      <div class="pop-contents">
        <p>We at PMFTC Inc. ("<b>PM</b>") respect the privacy of our consumers. We are committed to
        appropriately safeguarding personal information that you provide to us when you visit and
        use this Website (the “Website”). This section includes a description of the types of personal
        information we collect via the Website and your consent on how we can use it. It also describes
        the measures we take to protect the security of the information and how you can contact us. </p><h4>Information We Collect and How We Use It</h4><p>In order to be able to sign up to the Website you must be at least 18 years of age and you
        must provide us with personal information, such as smoking status, name, age, country of
        residence and e-mail address/telephone number, to enable us to verify your eligibility to be
        granted access to the Website. When you visit the Website, your browser automatically sends
        us an IP (Internet Protocol) address and certain other information (such as the type of browser
        you use). In addition, we may monitor and/or register your activities on the Website, including
        your submissions to the forum, if any. However, we do not use client-side cookies to store your
        personal and access information.</p><h4>Consent of use </h4><p>You acknowledge and give consent to PM to:</p><ul>
          <li>store and use your personal information mentioned under the previous section to allow
          PM to verify your age and compliance with PMI internal policies, and/or contact you;</li>

          <li>provide you with, using any means (mail, email, SMS, etc.), communications related to
          tobacco products, including but not limited to information and material on brand
          launches, packaging changes, events, marketing activities and/or the regulation of
          tobacco products, to the extent permitted under applicable laws;</li>

          <li>to obtain and analyze publicly available personal information about you from online third
          party social media sites, in order to communicate with you in a more personal and
          effective manner;</li>

          <li>process your personal information in countries other than the country in which you live
          (for example, locating the database in one country and accessing the database remotely
          from another); and</li>

          <li>disclose your personal information to PM’s service providers for the above purposes, to
          a PM affiliate to do the above for its own purposes and (if required by law) to authorities.
          PM may not disclose your personal information to third parties for other purposes.</li>
        </ul><h4>How We Protect Personal Information</h4><p>The security of personal information is a high priority for us. We maintain appropriate
        administrative, technical and physical safeguards designed to protect against unauthorized
        disclosure, use, alteration and destruction of the personal information that is provided to us
        when you visit and use the Website. We use secure socket layer (SSL) and digital certificate
        encryptions and technologies to keep the transmission of your personal information secure.</p><h4>How to Contact Us </h4><p>For any questions you may have about this Privacy Statement you can contact our adult-
        restricted hotline at 895-9999 (Metro Manila) or 1-800-10-895-9999 (Provincial Toll-Free).</p><h4>How to Unsubscribe from our Database</h4><p>PM is committed to respecting and protecting your privacy. Unless otherwise provided, none of
        the information you provide will be sold or transferred to non-affiliated parties of PM without your
        consent. If you wish to withdraw your name from our database, please call our adult-restricted
        hotline 895-9999 (Metro Manila) or 1-800-10-895-9999 (Provincial Toll-Free) or text MLIST
        &lt;FULL NAME&gt;, &lt;DATE OF BIRTH IN MM/DD/YYY&gt;, &lt;MESSAGE&gt; to 0908-895-9999 (For
        example: MLIST JUAN DELA CRUZ, 08/12/1982, HOW DO I GET OFF THE LIST?) or email us at info@marlboro.ph</p>
      </div>
    </div>
  </div>
  <div id="popup-contact"  class="std-popup mfp-hide">
    <div class="pop-container">
      <div class="pop-header">
        <h3>You can contact us at our adult-restricted hotline:</h3>
      </div>
      <div class="pop-contents">

        <p>895-9999 (Metro Manila) or 1-800-10-895-9999 (Provincial Toll-Free), or text MLIST &lt;full name&gt;, &lt;date of birth in mm/dd/yyyy&gt;, &lt;message&gt; to 0908-895-9999.</p>
        <p>Standard SMS rates apply.</p>
      </div>
    </div>
  </div>



<!-- RUBYBURST -->
<script src="<?= base_url().'/rubyburst2/marl-includes/jquery.magnific-popup.min.js' ?>"></script>
  <script src="<?= base_url().'/rubyburst2/marl-includes/jquery.mCustomScrollbar.js' ?>"></script>
  <script src="<?= base_url().'/rubyburst2/marl-includes/jquery.mousewheel.min.js' ?>"></script>

  <script src="<?= base_url().'/rubyburst2/js/vendor_rubyburst.js' ?>"></script>

  <script src="<?= base_url().'/rubyburst2/js/main_rubyburst.min.js' ?>"></script>
<!-- EOF RUBYBURST -->
<!-- Latest compiled and minified JavaScript -->
<script type="text/javascript"> var is_cm = <?php echo @$this->session->userdata("is_cm") ?>; </script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/lytebox.2.3.js"></script>
<script src="scripts/exec.js"></script>
<script src="scripts/owl.carousel.min.js"></script>
<script src="scripts/lib.js"></script>
<script src="scripts/app.js"></script>
<script src="scripts/jquery.mousewheel.js"></script>
<script src="scripts/jquery.jscrollpane.min.js"></script>
<script src="scripts/jquery.touchSwipe.min.js"></script>

<?php if (PROD_MODE): ?>
<script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  	<?php
	  	$userId = $this->session->userdata('user_id');
		// New Google Analytics code to set User ID.
		// $userId is a unique, persistent, and non-personally identifiable string ID.
		if (!$userId) {
		 	$gacode = "ga('create', 'UA-42684485-7', { 'userId': '%s' });";
		  	echo sprintf($gacode, $userId);
		}else{
			echo "ga('create', 'UA-42684485-7', 'marlboro.ph');";
		}
	?>

  // Set the user ID using signed-in user_id.
  ga('send', 'pageview');
</script>
<?php endif ?>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="<?= BASE_URL ?>scripts/shiv.js"></script>
  <script src="<?= BASE_URL ?>scripts/respond.min.js"></script>
<![endif]-->
<script>
<?php if ($this->session->userdata('edm_popup')): ?>
	popup.open({ url : "<?= BASE_URL.$this->session->flashdata('edm_popup') ?>" });
<?php endif ?>

User.getTopInterest();
</script>

<script type="text/javascript">
    setTimeout(function() { window.location.href = "<?=site_url()?>welcome/logout"; }, 1000 * 60 * 60 * 24);
</script>
<!-- visit <?= page_visit() ?> -->

</body>
</html>
