<div class="popup" id="upload">
	<div class="content">

		<form action="<?php echo ($row) ?  BASE_URL.'backstage_pass/upload_photo/'.$row->backstage_event_id :  BASE_URL.'backstage_pass/upload_photo/'; ?>" onsubmit="return onSubmitPhoto();" method="post" target="upload_target" enctype="multipart/form-data">
			
			<button class="popup-close" id="btn-photo-upload-close-popup">&times;<span>CLOSE</span></button>					
			
				<h2 class="txtcenter"><?=($row) ? $row->title : ''?></h2>
				
				<div class="browse">
					<input type="text" placeholder="Browse for File (Max file size: 3MB)" readonly>
					<input type="file" id="upload-photo" class="hidden"  onchange="PreviewImage();" name="photo">
				</div>

				<div class="upload-preview-fixed">
					<span>PHOTO PREVIEW</span>
				</div>

				<div class="txtcenter">
					<div class="error" id="error-photo-upload"></div>
					<button type="button" class="button close-x" id="btn-photo-upload-cancel"><i>CANCEL</i></button>
					<button type="submit" class="button" id="btn-photo-upload-save"><i>SAVE</i></button>
				</div>

				
				<p class="terms">Please be reminded that as per the <a href="#" onclick="popup.open({url:'<?=BASE_URL?>/popups/terms.html'})">Terms and Conditions</a>, all photos, images or designs submitted will become the property of PM. 
					Please do not upload or submit any photos, images or designs depicting the act of smoking or showing a lit cigarette or minors 
					(persons less than 18 years old). PM reserves to the right to remove or reject any photo, image or design which it, in its 
					sole and absolute discretion, deems offensive, violates the law or is unsuitable in any other manner. The photo you upload 
					will be subject to moderation.</p>


		</form>

	</div>
</div>
<iframe id="upload_target" name="upload_target"  style="width:0px;height:0px;border:0px solid #fff;"></iframe>
<script type="text/javascript">
$(function(){
	
	$('.browse input[type="text"]').unbind("click").bind("click",function(){
		$('#upload-photo').trigger('click');
	});	

});

function PreviewImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("upload-photo").files[0]);
    oFReader.onload = function (oFREvent) {
    	$('.upload-preview-fixed').html('<img src="'+oFREvent.target.result+'"/>').show();
    };
}

function onSubmitPhoto()
{
	$('#btn-photo-upload-close-popup').hide();
	$('#btn-photo-upload-cancel').hide();
 	$('#btn-photo-upload-save').addClass('disabled').attr('disabled','disabled').html('<i><img src="<?=BASE_URL?>images/spinner.gif" /> SAVING...</i>');
	return true;
}

function form_response(msg)
{
 	popup.open({type:'alert','message':msg,buttonAlign:'center',align:'center',okCaption:'CONTINUE'});
}

function showButtons()
{
	$('.popup-close').show();
	$('#btn-photo-upload-cancel').show();
}
</script> 
