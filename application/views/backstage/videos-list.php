<!-- main content starts here  -->
<div class="wrapper">
		<div class="container main">
			<div class="row">
				<?php echo $this->load->view('backstage/nav') ?>

				<div class="col-md-9 col-lg-10  col-md-push-3 col-lg-push-2 main-rail">
					
					<!-- <div class="upload-from-event">
						<button type="button" class="button-small" onclick="popup.open({url:'<?=BASE_URL?>videos/get_upload_form'})">
							<i>
						 		<span class="hidden-xs">UPLOAD YOUR OWN VIDEO VIA YOUTUBE</span>
						 		<span class="visible-xs">UPLOAD YOUR OWN VIDEO</span>
						 	</i>
						</button>
					</div>

					<div class="clearfix"></div> -->

					<div class="row row-listing">
						<!-- start items -->
						<?php if($rows){ 
								foreach($rows as $v){ ?>
								<div class="col-xs-6 col-sm-4 item">
									<a class="about-video-entry" data-id="<?= $v->about_video_id ?>" data-title="<?= $v->title ?>" data-href="<?=BASE_URL.'events/videos/content/'.$v->url_title?>" href="javascript:void(0)">
										<h3><?=$v->title?></h3>
										<div class="thumb-holder">
											<div class="thumbnail" style="background-image:url('<?=$v->video_thumbnail?>')">
												<sup><?= substr($v->video_duration, 3)?></sup>
											</div>
											<div class="btn-play"> <i class="glyphicon glyphicon-play"></i> </div>
										</div>
										<div class="clearfix"></div>
									</a>
								</div>
						<?php 	}
						}else{ ?><div class="col-lg-6 item">No video(s) found.</div><?php } ?>					 					
						<!-- end items -->
						
					</div>
				</div>
			</div>
		</div>

	</div>	
<!-- main content ends here  -->
