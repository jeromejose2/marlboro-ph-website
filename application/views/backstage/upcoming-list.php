
<!-- main content starts here  -->
 	<div class="wrapper">
		
		<div class="container main">
			<div class="row">
				<!-- <div class="col-md-3 col-lg-2  col-sm-12 side-rail sticky">
					<nav>
						<a href="<?=BASE_URL?>events/hot"> <i class="glyphicon glyphicon-fire"></i>What's HOT</a>
						<a href="<?=BASE_URL?>events/upcoming" class="active"> <i class="glyphicon glyphicon-star"></i>Upcoming Events</a>
						<a href="<?=BASE_URL?>events/past"> <i class="glyphicon glyphicon-time"></i>Past Events</a>
					</nav>
				</div> -->
				<?php echo $this->load->view('backstage/nav') ?>

				<div class="col-md-9 col-lg-10  col-md-push-3 col-lg-push-2 main-rail">

					<div class="row row-listing">
							<!-- start items -->
						
						<div class="col-sm-12">
							<div class="row row-filter">
								<form id="filters">
									<div class="col-sm-3">
										<select name="venue">
											<option value="">- Select venue name -</option>
											<option value="">All Venues</option>
											<?php if($venues->num_rows()){

				                                  foreach($venues->result() as $r){ 
				                                    if($this->input->get('venue_id')==$r->venue_id){?>
				                                      <option selected="selected" value="<?=$r->venue_id?>"><?=$r->name?></option>
				                          <?php     }else{ ?>
				                                      <option value="<?=$r->venue_id?>"><?=$r->name?></option>
				                          <?php     }
				                                  }

				                                } ?>
										</select>
									</div>
									<div class="col-sm-3">	
										<select name="region">
											<option value="">- Select Region -</option>
											<option value="">All Regions</option>
											<?php if($regions->num_rows()){
			                                      foreach($regions->result() as $r){ 
			                                        if($this->input->get('region_id')==$r->region_id){?>
			                                          <option selected="selected" value="<?=$r->region_id?>"><?=$r->name?></option>
			                              <?php     }else{ ?>
			                                          <option value="<?=$r->region_id?>"><?=$r->name?></option>
			                              <?php     }
			                                        
			                                      }
			                                    } ?>
										</select>
									</div>
									<div class="col-sm-3">	
										<input type="text" name="start_date" class="from" placeholder="Start Date">
									</div>
									<div class="col-sm-3">	
										<input type="text" name="end_date" class="to" placeholder="End Date">
									</div> 
								</form>
							</div>
						</div>

						<div class="clearfix"></div>

						<div id="upcoming-events-content">

						</div>

						<script id="upcoming-events-template" type="text/x-handlebars-template">
						{{# if albums}}
							{{#each albums}}
							<div class="col-sm-6 item">
								<div class="row">
									<div class="col-sm-6">
										<a href="javascript:void(0)" onclick="popup.open({url:'<?=base_url('backstage_pass/event_photo?event={{backstage_event_id}}')?>'})">
											<div class="thumb-holder">
												<div class="thumbnail" style="background-image:url('{{image}}')"></div>
											</div>
											<div class="clearfix"></div>
										</a>
									</div>
									<div class="col-sm-6">
										<div class="title">
											<h3><a href="javascript:void(0)">{{album_name}}</a></h3>
											<h5>{{event}}</h5>
										</div>
										<p>{{description}}</p>
										<!-- <a class="button-small" onclick="popup.open({url:'<?=base_url('backstage_pass/get_google_map?event={{backstage_event_id}}')?>'})" href="javascript:void(0)"><i>VIEW MAP</i></a> -->
										{{# if attendance}}
											<a class="button-small" id="im-going"><i>Going</i></a>
										{{else}}
											<a class="button-small" id="im-going" onclick="popup.open({url:'<?=BASE_URL?>backstage_pass/attend_event?event={{backstage_event_id}}'});"><i> <i class="glyphicon glyphicon-ok-circle"></i> I'M GOING</i></a>
										{{/if}}
									</div>
								</div>
							</div>
							{{{new_line}}}
							{{/each}}
						<div class="col-lg-6 text-center"><div id="load-more"></div></div>
						{{else}}	
								<div class="col-lg-12 item"><div style="margin-top:100px; text-align: center">There are no upcoming events for now. Come back soon so you don't miss out on the Marlboro parties near you!</div></div>
						{{/if}}
						</script>

						<!-- end items -->
						
					</div>
				</div>
			</div> 	
		</div>

	</div> 

<!-- jquery ui -->
<link href="<?php echo BASE_URL ?>plugins/jquery-ui/css/smoothness/jquery-ui-1.10.0.custom.css" rel="stylesheet">
<script src="<?php echo BASE_URL ?>plugins/jquery-ui/js/jquery-1.9.0.js"></script>
<script src="<?php echo BASE_URL ?>plugins/jquery-ui/js/jquery-ui-1.10.0.custom.js"></script>
<!-- /jquery ui -->

<script type="text/javascript" src="<?=BASE_URL?>scripts/handlebars-v1.3.0.js"></script> 
<script type="text/javascript" src="<?=BASE_URL?>scripts/backstagepass/upcoming_events.js"></script>
 
<script>
 

$(function(){  

	var properties = {
						url:'<?=BASE_URL?>api/upcoming_events',
						handlebar_template:'#upcoming-events-template',
						template_content:'#upcoming-events-content',
						load_more: '#load-more',
						total_group: <?=$total_group?>,
						retrieving_data: false,
						track_group: 0,
						rows: []
  					};
	backstage_pass.upcoming_events.init(properties);

}); 

function markCalendar(token)
{
	backstage_pass.upcoming_events.markGooglecalendar(token);
}

</script>