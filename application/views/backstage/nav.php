<div class="col-md-3 col-lg-2  col-sm-12 side-rail sticky">
	<nav>
		<a href="<?=BASE_URL?>events/hot" class="<?php echo $this->uri->segment(2) == 'hot' ? 'active' : '' ?>"> <i class="glyphicon glyphicon-fire"></i>What's HOT</a>
		<a href="<?=BASE_URL?>events/upcoming" class="<?php echo $this->uri->segment(2) == 'upcoming' ? 'active' : '' ?>"> <i class="glyphicon glyphicon-star"></i>Upcoming Events</a> 
		<a href="<?=BASE_URL?>events/past" class="<?php echo $this->uri->segment(2) == 'past' || $this->uri->segment(2) == 'content' || $this->uri->segment(2) == 'thumbnails' ? 'active' : '' ?>"> <i class="glyphicon glyphicon-time"></i>Past Events</a>
		<!-- <a href="<?=BASE_URL?>about/photos"> <i class="glyphicon glyphicon-camera"></i>PHOTOS</a> -->
		<a href="<?=BASE_URL?>events/videos" class="<?php echo $this->uri->segment(2) == 'videos' ? 'active' : '' ?>"> <i class="glyphicon glyphicon-film"></i>VIDEOS</a>
		<!-- <a href="<?=BASE_URL?>about/news"> <i class="glyphicon glyphicon-list"></i>NEWS</a> -->
	</nav>
</div>