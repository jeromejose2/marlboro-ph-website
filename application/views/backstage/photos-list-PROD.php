<!-- main content starts here  -->
 	<div class="wrapper">
		
		<div class="container main">
			<div class="row">	
				<div class="col-md-3 col-lg-2  col-sm-12 side-rail sticky">
					<nav>
						<a href="<?=BASE_URL?>backstage_pass" class="active"> <i class="glyphicon glyphicon-camera"></i>PHOTOS</a>
					</nav>
				</div>

				<div class="col-md-9 col-lg-10 col-md-push-3 col-lg-push-2  main-rail">
					<div class="row row-listing">
						<!-- start items --> 			 


						<?php if($rows){
							foreach($rows as $v){  
	 							?>
						<div class="col-lg-6 item">
							<a href="<?=BASE_URL.'backstage_pass/thumbnails/'.$v['url_title']?>">
								<h3><?=$v['album_name']?></h3>
								<div class="album-holder">

									<?php 
									$count = 0;
									foreach($v['photos']->result() as $p){ $count++;
											if(file_exists("uploads/backstage/photos/thumbnails/331_176_".$p->image)){ ?>
												<div class="thumbnail" style="background-image:url('<?=BASE_URL."uploads/backstage/photos/thumbnails/331_176_".$p->image?>')"></div>
										<?php } 
											if($count==3) break; ?>
	  								<?php } ?>  
								</div>
							</a>
						</div>

						<?php 	}
						}else{ ?><div class="col-lg-6 item">No photo(s) found.</div>

					   <?php } ?>
	 

						<!-- end items -->
						
					</div>
				</div>
			</div>
		</div>

	</div>

	

 <!-- main content ends here  --> 
</html>