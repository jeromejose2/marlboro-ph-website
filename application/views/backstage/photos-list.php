<!-- hey your the one -->
<!-- main content starts here  -->
 	<div class="wrapper">
		
		<div class="container main">
			<div class="row">	
				<!-- <div class="col-md-3 col-lg-2  col-sm-12 side-rail sticky">
					<nav>
 
 						<a href="<?=BASE_URL?>events/hot"> <i class="glyphicon glyphicon-fire"></i>What's HOT</a>
						<a href="<?=BASE_URL?>events/upcoming"> <i class="glyphicon glyphicon-star"></i>Upcoming Events</a> 
 						<a href="<?=BASE_URL?>events/past" class="active"> <i class="glyphicon glyphicon-time"></i>Past Events</a>
 					</nav>
				</div> -->
				<?php echo $this->load->view('backstage/nav') ?>

				<div class="col-md-9 col-lg-10 col-md-push-3 col-lg-push-2  main-rail">
					<div class="row row-listing">

						<div class="col-sm-8 col-md-10">
							<div class="row row-filter">
								<form id="filters">
									<div class="col-sm-4">
										<select name="venue">
											<option value="">- Select venue name -</option>
											<option value="">All Venues</option>
											<?php if($venues->num_rows()){

				                                  foreach($venues->result() as $r){ 
				                                    if($this->input->get('venue_id')==$r->venue_id){?>
				                                      <option selected="selected" value="<?=$r->venue_id?>"><?=$r->name?></option>
				                          <?php     }else{ ?>
				                                      <option value="<?=$r->venue_id?>"><?=$r->name?></option>
				                          <?php     }
				                                  }

				                                } ?>
										</select>
									</div>
									<div class="col-sm-4">	
										<select name="region">
											<option value="">- Select Region -</option>
											<option value="">All Regions</option>
											<?php if($regions->num_rows()){
			                                      foreach($regions->result() as $r){ 
			                                        if($this->input->get('region_id')==$r->region_id){?>
			                                          <option selected="selected" value="<?=$r->region_id?>"><?=$r->name?></option>
			                              <?php     }else{ ?>
			                                          <option value="<?=$r->region_id?>"><?=$r->name?></option>
			                              <?php     }
			                                        
			                                      }
			                                    } ?>
										</select>
									</div>
									<div class="col-sm-4">	
										<select name="event_type">
											<option value=""> All Events</option>
											<?php if($event_types->num_rows()){
			                                      foreach($event_types->result() as $r){ 
			                                        if(strtolower($this->input->get('event_type'))==strtolower($r->name)){?>
			                                          <option selected="selected" value="<?=$r->name?>"><?=$r->name?></option>
			                              <?php     }else{ ?>
			                                          <option value="<?=$r->name?>" <?php echo $r->name == 'CROSS|OVER' ? 'selected' : '' ?>><?=$r->name?></option>
			                              <?php     }
			                                        
			                                      }
			                                    } ?>

										</select>
									</div>
								</form>
							</div>
						</div>

						<div class="clearfix"></div>

						<div id="past-events-content">						 

						</div>

						<script id="past-events-template" type="text/x-handlebars-template">
						  {{# if albums}}
							{{#each albums}}
							<div class="col-lg-6 item">
								<a href="{{url}}">
									<div class="title">
										<h3>{{album_name}}</h3>
										<h5>{{event}}</h5>
									</div>
									<div class="album-holder">
										{{#each photos}}
										 <div class="thumbnail" style="background-image:url('{{this.thumbnail}}')">
										 	{{# if this.duration}}
										 		<sup>{{this.video_duration}}</sup>
										 	{{/if}}
										 </div>
										{{/each}}
									</div>
								</a>
							</div>
							{{/each}}
							<div class="col-lg-6 text-center"><div id="load-more"></div></div>
						  {{else}}	
								<div class="col-lg-12 item"><div style="margin-top:100px; text-align: center">There are no past events for now. Come back soon so you don't miss out on the Marlboro parties near you!</div></div>
						  {{/if}}
						</script>
						<!-- end items -->
						
					</div>
				</div>
			</div>
		</div>

	</div>
<script type="text/javascript" src="<?=BASE_URL?>scripts/handlebars-v1.3.0.js"></script> 
<script type="text/javascript" src="<?=BASE_URL?>scripts/backstagepass/past_events.js"></script>
 
<script> 
jQuery(document).ready(function(){
	
	var properties = {
						url:'<?=base_url()."api/".$this->uri->segment(1)."_".$this->uri->segment(2)?>',
						handlebar_template:'#past-events-template',
						template_content:'#past-events-content',
						load_more: '#load-more',
						total_group: <?=$total_group?>,
						retrieving_data: false,
						track_group: 0,
						rows: []
  					};
	backstage_pass.past_events.init(properties);
});
</script>
 
</html>