 		
<!-- main content starts here  -->
 	<div class="wrapper">
		
		<div class="container main">
			<div class="row">
				<!-- <div class="col-md-3 col-lg-2  col-sm-12 side-rail sticky">

					<nav>
 
 						<a href="<?=BASE_URL?>events/hot"> <i class="glyphicon glyphicon-fire"></i>What's HOT</a>
						<a href="<?=BASE_URL?>events/upcoming"> <i class="glyphicon glyphicon-star"></i>Upcoming Events</a> 
						<a href="<?=BASE_URL?>events/past" class="active"> <i class="glyphicon glyphicon-time"></i>Past Events</a>
 					</nav>

				</div> -->
				<?php echo $this->load->view('backstage/nav') ?>

				<div class="col-md-9 col-lg-8  col-md-push-3 col-lg-push-2  main-rail">
					<div class="title">
						<h2><?=($row) ? $row->title : 'Content not found.'?><h2>
						<h5><?php
						if($row){

							$event = $row->venue;
							if($row->venue && $row->region_name) 
								$event .= ', ';

							if($row->region_name) 
								$event .= $row->region_name;		

		 					$schedule = simplify_datetime_range($row->start_date,$row->end_date);
		 					$event .= $schedule ? ' - '.$schedule : '';
		 					echo $event;

						} ?></h5>
						<h3><a href="<?=BASE_URL?>backstage_pass/thumbnails/<?=$this->uri->segment(3)?>">Back to Photos</a></h3>
					</div>
					<?php if($row): ?>
					<div class="article-stat">
						<span>COMMENTS: <?=(int)@$total_comments?></span>
						<span>VIEWS: <?=($row_photo) ? (int)$row_photo->views : '0'?></span>
					</div>
					<figure class="theater">

						<?php if($prev){ ?><div class="arrow left" onclick="redirect('<?=BASE_URL.'backstage_pass/content/'.$url_title.'/'.$prev->photo_id?>');"></div> <?php } ?>
						<?php if($next){ ?><div class="arrow right" onclick="redirect('<?=BASE_URL.'backstage_pass/content/'.$url_title.'/'.$next->photo_id?>');"></div> <?php } ?>
	  					<?php if($row_photo){ 

	  								if($row_photo->media_type=='photo' && file_exists('uploads/backstage/photos/'.$row_photo->media)){?>

										<img src="<?=BASE_URL.'uploads/backstage/photos/'.$row_photo->media?>">

							  <?php }else if($row_photo->media_type=='video' && file_exists('uploads/backstage/photos/'.$row_photo->media)){ 

										$video_mime_type = get_mime_by_extension('uploads/backstage/photos/'.$row_photo->media);?>

										<video id="video" width="100%" height="100%"  controls="controls" autoplay>
					  						<source src="<?=BASE_URL.'uploads/backstage/photos/'.$row_photo->media?>" type="<?=$video_mime_type?>" />
											<script type="text/javascript">
											function tryFlash(){
												// flash fallback
												jwplayer("video").setup({ flashplayer: "<?=BASE_URL?>scripts/jwplayer/player.swf",
																		modes: [{ type: "html5" }, { type: "flash", src: "<?=BASE_URL?>scripts/jwplayer/player.swf", autostart:true}]});	
	 										}
											function initVideo(){
												var video = document.getElementById("video");
												// check if html5 video tag is supported if not fallback to flash
												 if(!video.play ? true : false)
													tryFlash();
											}
											initVideo();
											</script>
										</video>

									<?php } 

							}else{ ?>

									<div class="no-image" style="background-image:url(<?=BASE_URL.DEFAULT_IMAGE?>)"></div>
						<?php } ?>

					</figure>
					<?php endif; ?>
				</div>

				<div class="col-md-8 col-md-push-3 col-lg-push-2">
					<?php
					if($row)
						echo show_comments(BACKSTAGE_PHOTOS, $row_photo->photo_id, $row_photo->user_type == 'registrant' ? array('recipient_id' => $row_photo->uploader_id) : array(), array('category' => 'Comment', 'action' => 'submit', 'label' => 'Commented on Backstage Pass Photo #'.$row_photo->photo_id.': '.$row->title, 'value' => 1)); ?>
				</div>
			</div>
		</div>

	</div> 

<script type="text/javascript">
function redirect(url){
	top.location.href=url;
}
</script>
	
<!-- main content ends here  --> 