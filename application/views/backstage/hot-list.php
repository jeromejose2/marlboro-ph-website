
<!-- main content starts here  -->
 	<div class="wrapper">
		
		<div class="container main">
			<div class="row">
				<!-- <div class="col-md-3 col-lg-2  col-sm-12 side-rail sticky">
					<nav>
						<a href="<?=BASE_URL?>events/hot" class="active"> <i class="glyphicon glyphicon-fire"></i>What's HOT</a>
						<a href="<?=BASE_URL?>events/upcoming"> <i class="glyphicon glyphicon-star"></i>Upcoming Events</a>
						<a href="<?=BASE_URL?>events/past"> <i class="glyphicon glyphicon-time"></i>Past Events</a>
					</nav>
				</div> -->
				<?php echo $this->load->view('backstage/nav') ?>

				<div class="clearfix"></div>

				<div class="col-md-9 col-lg-10  col-md-push-3 col-lg-push-2 main-rail">
					<!-- content -->
					<?php 
					$total_rows = count($rows);
					if(count($rows) > 1){ ?>
							<div class="arrow right"></div>
							<div class="arrow left"></div>
					<?php }
							
					if($rows){ ?>
						<div class="hot-events">
							<?php foreach($rows as $v){ ?>

									<div class="item">
										<div class="row">
											<div class="col-sm-6">
												<a data-id="#" data-title="#" data-href="#" href="javascript:void(0)" onclick="popup.open({url:'<?=base_url('backstage_pass/event_photo?event='.$v['backstage_event_id'])?>'})">
													<div class="thumb-holder">
														<div class="thumbnail" style="background-image:url('<?=$v['image']?>')"></div>
													</div>
													<div class="clearfix"></div>
												</a>
											</div>

											<div class="col-sm-6">
												<div class="title">
													<h3><a href="javascript:void(0)"><?=$v['title']?></a></h3>
													<?=$v['date_address']?>
												</div>
												<p><?=$v['description']?></p>
												<?php if($v['venue']){ ?>
												<!-- <a class="button-small"  href="javascript:void(0)" onclick="popup.open({url:'<?=base_url('backstage_pass/get_google_map?event='.$v['backstage_event_id'])?>'})"><i> <i class="glyphicon glyphicon-map-marker"></i> VIEW MAP &nbsp;</i></a> -->
												<?php } ?>												
												<?php if(!$v['attendance'] && $v['schedule']){ ?>
												<a class="button-small" href="javascript:void(0)" onclick="popup.open({url:'<?=base_url('backstage_pass/attend_event?event='.$v['backstage_event_id'])?>'})"><i> <i class="glyphicon glyphicon-ok-circle"></i> I'M GOING &nbsp;</i></a>
												<?php }else{ ?>	
												<a class="button-small" id="im-going"><i>Going</i></a>
												<?php } ?>													
											</div>
										</div>
									</div>

							<?php }?>

						</div>
					<?php }else{ echo "<div style=\"margin-top:100px;text-align: center\">There are no upcoming events for now. Come back soon so you don't miss out on the Marlboro parties near you!</div>"; } ?>

					<!-- end content -->
				</div>
			</div> 	
		</div>

	</div>
  
<!-- main content ends here  -->
<script type="text/javascript">

$(function(){
	$('.browse').click(function(){
		$('#upload-photo').trigger('click');
	});
});

function PreviewImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("upload-photo").files[0]);
    oFReader.onload = function (oFREvent) {
    	$('.upload-preview-fixed').html('<img src="'+oFREvent.target.result+'"/>').show();
    };
};

window.onload = function(){ 
	$(".hot-events").owlCarousel({
		singleItem: true,
		autoPlay : 5000
	}); 

	var owl = $(".hot-events").data('owlCarousel');

	$('.arrow.left').click(function(){
		owl.prev();	
	});
	$('.arrow.right').click(function(){
		owl.next();	
	});
}

function markCalendar(token)
{
	$.ajax({
		 	url:BASE_URL+'backstage_pass/mark_google_calendar',
		 	data:{token:token,event_id:$('input[name="event_id"]').val()},
		 	type: 'post',
		 	dataType: 'json',
		 	beforeSend: function(){
		 		$('.mark-my-calendar').html('<i><img src="'+BASE_URL+'images/spinner.gif" /> Please wait...</i>');
		 	},
		 	success: function(data){
 		 		popup.open({type:'alert',message:data.response,buttonAlign:'center',align:'center',okCaption:'CONTINUE',bgClose:false,close:false,onConfirm: function(){
		 			location.href=BASE_URL+'events/hot';
		 		}}); 
			}
	});
}
</script>