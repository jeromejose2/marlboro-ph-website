<?php

class Hook
{
	public function post_controller_constructor()
	{
		$ci = & get_instance();

		if (PROD_MODE && (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on')) {
			$url = 'https://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
			redirect($url);
		}
		
		$route_class = strtolower($ci->router->class);
		if (!in_array($route_class, array('api', 'noscript', 'spice_api')) && $ci->uri->segment(1) != 'gateway') {
			if ($route_class == 'edm' && !$ci->uri->segment(2)) {
				$url = $ci->input->get('url');
				if (!$url) {
					$url = '/';
				}
				$popup = $ci->input->get('popup');
				if ($popup) {
					$ci->session->set_userdata('edm_popup', ltrim($popup, '/'));
				}
				$ci->session->set_userdata('redirect_url', $url);
			}

			$user_session = $ci->session->userdata('user');		

			if (!$user_session && $route_class != 'user') {

				redirect('user/login');
			} elseif ($user_session && $ci->uri->segment(2) == 'autologin') {

                    if( ! isset($_GET['token'])) {
                        $url = 'user/autologin?token='.$ci->input->get('token');
                        $ci->session->set_userdata('redirect_url', $url);

                        if($redirect_url = $ci->session->flashdata('redirect_url')) {
                            redirect($redirect_url);
                            var_dump($redirect);
                        }

                        redirect('/');    
                    }

            } elseif ($user_session && $route_class == 'user') {

				if($redirect_url = $ci->session->flashdata('redirect_url')) {
					redirect($redirect_url);
				}

				redirect('/');

			} elseif ($user_session && $ci->session->userdata('redirect_url')) {

				redirect($ci->session->flashdata('redirect_url'));

			}

			// if ($user_session && ($ci->uri->segment(1) == 'move_forward' || $ci->uri->segment(1) == 'games' || $ci->uri->segment(1) == 'events' || $ci->uri->segment(1) == 'perks') ) {

			// 	redirect('user/login');
			// }

			# BEGIN remove this if everything is ok
			// if($_SERVER['REMOTE_ADDR'] != '180.232.124.116' && $_SERVER['REMOTE_ADDR'] != '203.177.63.10' && $_SERVER['REMOTE_ADDR'] != '121.54.44.136' && $_SERVER['REMOTE_ADDR'] != '112.198.99.165') {
			// 	$segment = $ci->uri->segment(1);
			// 	if($segment == 'move_forward' || $segment == 'perks') {
			// 		redirect('user/login');
			// 	}
			// }
			# END remove this if everything is ok

			if(!$ci->input->is_ajax_request() && !in_array($route_class, array('api', 'noscript', 'spice_api','user')) && $ci->uri->segment(1) != 'gateway'){

				$ci->load->library('spice');
				$response = $ci->spice->checkSession($ci->session->userdata('_spice_member_session_key'));
 
				if(!$response){
					$ci->session->sess_destroy();
					redirect('user/login');
				}


			}

			$ci->unread_notifications = $ci->Notification_Model->get_unread_count_by_user($ci->session->userdata('user_id'));
			$ci->nav_movefwd_categories = $ci->Movefwd_Model->get_categories();
		}
	}
}