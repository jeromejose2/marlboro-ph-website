<?php

class Finalist_Comment_Model extends CI_Model
{
	
	public function save($registrant_id, $finalist_id, $comment, $type, $comment_id = FALSE)
	{
		if( ! $registrant_id) {
			return FALSE;
		}

		$data = array();
		$data['registrant_id'] = $registrant_id;
		$data['finalist_id'] = $finalist_id;
		$data['comment'] = $comment;
		switch ($type) {
			case 'comment':
				$this->db->insert('tbl_finalist_comments', $data);
				break;
			case 'reply':				
				$data['comment_id'] = $comment_id;
				$this->db->insert('tbl_finalist_comment_replies', $data);
				break;			
			default:
				return FALSE;
				break;
		}

		return $this->db->insert_id();
	}

	public function get_comments($finalist_id)
	{
		if( ! $finalist_id) {
			return FALSE;
		}

		$this->db->select('fc.*, r.nick_name, r.first_name')->from('tbl_finalist_comments fc');
		$this->db->join('tbl_registrants r', 'r.registrant_id = fc.registrant_id');
		$this->db->where('fc.status', 1);
		$this->db->where('fc.finalist_id', $finalist_id);
		$comments = $this->db->get()->result_array();

		if($comments) {
			foreach($comments as $k => $v) {
				if($v['nick_name'] == 0 || $v['nick_name'] == '0') {
					$comments[$k]['nick_name'] = $v['first_name'];
				}
			}
		}		

		if($comments) {
			foreach($comments as $k => $v) {
				$this->db->select('fcr.*, r.nick_name, r.first_name')->from('tbl_finalist_comment_replies fcr');
				$this->db->join('tbl_registrants r', 'r.registrant_id = fcr.registrant_id');
				$this->db->where('fcr.status', 1);
				$this->db->where('fcr.finalist_id', $v['finalist_id']);
				$this->db->where('fcr.comment_id', $v['finalist_comment_id']);
				$this->db->order_by('date_added', 'DESC');
				$replies = $this->db->get()->result_array();

				if($replies) {
					foreach($replies as $k => $v) {
						if($v['nick_name'] == 0 || $v['nick_name'] == '0') {
							$replies[$k]['nick_name'] = $v['first_name'];
						}
					}
				}

				if($replies) {
					$comments[$k]['replies'] = $replies;
				} else {
					$comments[$k]['replies'] = array();
				}
			}
			return $comments;
		} else {
			return array();
		}
	}

}