<?php

class Webgames extends CI_Model
{
	public function get_games() {
		$games = $this->db->select('*')
				 		  ->from('tbl_games')
				 		  ->where('status', 1)
				 		  ->order_by('game_id')
				 		  ->get();
		return $games;
	}

	public function get_game($game) {
		$game  = $this->db->select('*')
				 		  ->from('tbl_games')
				 		  ->where('slug', $game)
				 		  ->get()
				 		  ->row();
		return $game;
	}

	public function get_leaderboard($id) {
		
		$top_scorer = $this->db->select('r.first_name, r.middle_initial, r.third_name, r.nick_name, MAX(s.score) AS score, r.registrant_id')
								->from('tbl_game_scores s')
								->join('tbl_registrants r', 'r.registrant_id = s.registrant_id')
								->group_by('r.registrant_id')
								->order_by('score', 'DESC')
								->where('game_id', $id)
								->limit(10, 0)
								->get();
		return $top_scorer;


	}

	public function save_user_game($post) {
		$this->db->insert('tbl_game_scores', $post);
		return $this->db->insert_id();
	}

	public function get_game_words() {
		$words = $this->db->select('*')
						  ->from('tbl_game_words')
						  ->get();
		return $words;
	}
}