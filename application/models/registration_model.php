<?php

class Registration_Model extends CI_Model
{
	public function get_mobile_prefixes()
	{
		return $this->db->select()
			->from('tbl_mobile_prefix')
			->get()
			->result_array();
	}

	public function get_brands()
	{
		return $this->db->select()
			->from('tbl_brands')
			->get()
			->result_array();
	}

	public function get_alternate_purchase()
	{
		return $this->db->select()
			->from('tbl_alternate_purchase')
			->get()
			->result_array();
	}

	public function get_giid_types()
	{
		return $this->db->select()
			->from('tbl_giid_types')
			->get()
			->result_array();
	}

	public function get_user_by_login($username)
	{
		return (array) $this->db->select()
				->from('tbl_registrants')
				->where('email_address', $username)
				->get()
				->row();
	}

	public function save_invalid_login($id, $current_attempts)
	{
		if (++$current_attempts >= LOGIN_ATTEMPTS) {
			$this->db->set('date_blocked', date('Y-m-d H:i:s'));
		}
		$this->db->set('login_attempts', 'login_attempts + 1', false);
		$this->db->where('registrant_id', $id);
		$this->db->update('tbl_registrants');
		if (!$this->db->affected_rows()) {
			return false;
		}
		return true;
	}

	public function reset_invalid_login($id)
	{
		$this->db->set('login_attempts', 0);
		$this->db->set('date_blocked', null);
		$this->db->where('registrant_id', $id);
		$this->db->update('tbl_registrants');
		if (!$this->db->affected_rows()) {
			return false;
		}
		return true;
	}

	public function get_provinces()
	{
		return $this->db->select()
			->from('tbl_provinces')
			->get()
			->result_array();
	}

	public function get_province_by_id($id)
	{
		return (array) $this->db->select()
			->from('tbl_provinces')
			->where('province_id', $id)
			->get()
			->row();
	}

	public function get_user_by_id($id)
	{
		return (array) $this->db->select()
			->from('tbl_registrants')
			->where('registrant_id', $id)
			->get()
			->row();
	}

	public function get_user_by_refCode($code)
	{
		return (array) $this->db->select()
			->from('tbl_registrants')
			->where('referral_code', $code)
			->get()
			->row();
	}

	public function isFirstLogin($id)
	{
		$result =  $this->db->select()
			->from('tbl_login')
			->where('registrant_id', $id)
			->get()
			->row();

		if($result){
			return false;
		}else{
			return true;
		}
	}

	public function get_city_by_id($id, $province = null)
	{
		$this->db->select()
			->from('tbl_cities')
			->where('city_id', $id);
		if ($province) {
			$this->db->where('province_id', $province);
		}
		return (array) $this->db->get()->row();
	}

	public function get_cities($provinceId = null)
	{
		$this->db->from('tbl_cities');
		if ($provinceId) {
			$this->db->where('province_id', $provinceId);
		}
		return $this->db->get()->result_array();
	}

	public function referral_code(){
		$code = 'aa1000';
		$result = $this->db->order_by('referral_code','desc')->limit(1)->get('tbl_registrants')->result();
		if($result){
			if($result[0]->referral_code!=''){
				$code = $result[0]->referral_code;
			}
		}

		return $this->generate_referral_code($code);
	}
	private function generate_referral_code($code){

		$digit = substr($code, 2);

		$digit++;
		$chr1 = ord($code[0]);
		$chr2 = ord($code[1]);
		$chrStart = 97;
		$chrEnd   = 122;


		if($digit>9999){
			$digit = 1000;
			$chr2++;
		}

		if($chr2>$chrEnd){
			$chr1++;
			$chr2=$chrStart;
		}

		$chr1 = $chr1 > $chrEnd ? $chrStart : $chr1;
		return chr($chr1) . chr($chr2) . $digit; 
	}
	public function get_celebrants() {
		$lower_limit = date('Y') - 110;
		$upper_limit = date('Y') - 18;
		echo '<pre>';
		for($i = $lower_limit; $i <= $upper_limit; $i++) {
			$today = date('m-d');
			$date_time = DateTime::createFromFormat('Y-m-d', $i . '-' . $today);
			$birthday = sprintf(
						   '/Date(%s%s)/',
						   $date_time->format('U') * 1000,
						   $date_time->format('O')
						);
			$celebrants = $this->spice->SearchPerson(array('DateOfBirth' => $birthday));
			print_r($celebrants);	
		}

		
	}
}