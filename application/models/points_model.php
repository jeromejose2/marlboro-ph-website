<?php

set_time_limit(0);

class Points_Model extends CI_Model
{
	private $user = null;

	private $earn_origins = array(
		MOVE_FWD => 'earn_move_forward_activity',
		EXPLORE => 'earn_explore',
		BIRTHDAY_OFFERS => 'earn_birthday_offers',
		FLASH_OFFERS => 'earn_flash_offers',
		WEBGAMES => 'earn_webgame',
		COMMENT => 'earn_comment',
		PERKS_BUY => 'earn_perks_buy',
		PERKS_BID => 'earn_perks_bid',
		PERKS_RESERVE => 'earn_perks_reserve',
		HIDDEN_MARLBORO => 'earn_hidden_marlboro',
		USER_STATEMENT => 'earn_user_statement',
		REGISTRATION => 'earn_registration',
		LOGIN => 'earn_login',
		COMMENT_RECEIVE => 'earn_comment_receive',
		COMMENT_REPLY_RECEIVE => 'earn_comment_reply_receive',
		MOVE_FWD_GALLERY => 'earn_move_forward_gallery',
		MOVE_FWD_COMPLETE => 'earn_move_forward_complete',
		ABOUT_NEWS => 'earn_about_news',
		ABOUT_VIDEOS => 'earn_about_videos',
		ABOUT_PHOTOS => 'earn_about_photos',
		MOVE_FWD_WINNER => 'earn_move_forward_winner',
		HIDDEN_MARLBORO => 'earn_hidden_marlboro',
		PREMIUM_HIDDEN_MARLBORO => 'earn_premium_hidden_marlboro',
		BACKSTAGE_PHOTOS => 'earn_backstage_photos',
		REFERRAL => 'earn_referral',
		REFEREE_FIRSTLOGIN => 'earn_referee_first_login',
		REFERER_FIRSTLOGIN => 'earn_referer_first_login',
		SURVEY => 'earn_survey'
	);

	private $spend_origins = array(
		MOVE_FWD_PLEDGE_ENTRY => 'spend_move_forward_pledge',
		PERKS_BUY => 'spend_perks_buy',
		PERKS_BID => 'spend_perks_bid',
		PERKS_RESERVE => 'spend_perks_reserve'
	);

	public function updatePoints(){

		$response = false;
		$this->spice_library->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);

		$sql = 'SELECT * FROM tbl_login JOIN tbl_registrants ON tbl_registrants.registrant_id = tbl_login.registrant_id WHERE DATE(date_login) = "'.date('Y-m-d').'" GROUP BY tbl_registrants.person_id';
        $result = $this->db->query($sql)->result_array();

        foreach ($result as $k => $v) {
            $person_id = $v['person_id'];
            $total_points = $v['total_points'];
            $total_km = $v['total_km_points'];
            
            $spice_userinfo = $this->spice_library->GetPerson(array('PersonId' => $person_id));
            $spice_userinfo = $this->spice_library->parseJSON($spice_userinfo);
            $spice_userinfo = $spice_userinfo->ConsumerProfiles;

	        if(isset($spice_userinfo->PersonDetails->FirstName)){

	        	$total_derivations = count($spice_userinfo->Derivations);
						
				for($i = 0; $i<$total_derivations; $i++){
						if($spice_userinfo->Derivations[$i]->AttributeCode=='ca_points'){
        					$spice_userinfo->Derivations[$i]->AttributeValue = $total_points;
						}else if($spice_userinfo->Derivations[$i]->AttributeCode=='ca_total_km'){
        					$spice_userinfo->Derivations[$i]->AttributeValue = $total_km;
						}

				}
      	
	        	$Derivations = json_decode(json_encode($spice_userinfo->Derivations),true);

	            $params = array('person_id'=> $person_id,
                        		'updates'=> array(
                                        'Derivations'=> $Derivations
                                )
							);

        		$response = $this->spice_library->updatePerson($params);
        		$response = $this->spice_library->parseJSON($response);

        		// print_r($params);
        		// echo "<br><pre>";
        		// print_r($response);
		    }
        }
	
		return $response;
    }
    
	public function set_user(stdClass $user)
	{
		$this->user = $user;
		return $this;
	}

	public function get_user()
	{
		return $this->user;
	}

	public function spend($origin, $params = array())
	{
		if (!isset($this->spend_origins[$origin])) {
			show_error('Invalid Origin ID for spending points');
		}

		if (method_exists($this, $this->spend_origins[$origin])) {
			$newParams = array();
			$newParams['date'] = isset($params['date']) ? $params['date'] : null;
			$newParams['suborigin_id'] = isset($params['suborigin']) ? $params['suborigin'] : 0;
			$newParams['points'] = isset($params['points']) ? (int) $params['points'] : null;
			$newParams['registrant_id'] = isset($params['registrant']) ? $params['registrant'] : $this->session->userdata('user_id');
			$newParams['remarks'] = isset($params['remarks']) ? $params['remarks'] : null;
			$newParams['user'] = $this->db->select()
				->from('tbl_registrants')
				->where('registrant_id', $newParams['registrant_id'])
				->limit(1)
				->get()
				->row();
			if (!$newParams['user']) {
				return false;
			}
			$resultPoints = call_user_func_array(
				array($this, $this->spend_origins[$origin]),
				array($origin, $newParams)
			);

			

			if ($resultPoints) {
				$this->db->set('total_points', 'total_points - '.$resultPoints, false);
				$this->db->where('registrant_id', $newParams['registrant_id']);
				$this->db->update('tbl_registrants');

				$this->session->set_userdata('user_points', $newParams['user']->total_points - $resultPoints);
				return $resultPoints;
			}
		}
		return false;
	}

	private function change_status($registrant, array $points, $status)
	{
		if (!$points) {
			return false;
		}
		$where = array();
		$params = array();
		foreach ($points as $point) {
			if (!$point) {
				continue;
			}
			$this->db->where('registrant_id', $registrant)
				->where('origin_id', $point['origin'])
				->where('suborigin_id', isset($point['suborigin']) ? $point['suborigin'] : 0)
				->update('tbl_points', array(
					'point_status' => $status
				));
			if ($this->db->affected_rows()) {
				$where[] = '(registrant_id = ? AND origin_id = ? AND suborigin_id = ?)';
				$params[] = $registrant;
				$params[] = $point['origin'];
				$params[] = isset($point['suborigin']) ? $point['suborigin'] : 0;;
			}
		}

		if ($where) {
			$sql = 'SELECT * FROM tbl_points WHERE '.implode(' OR ', $where);
			$result = $this->db->query($sql, $params)->result_array();
			$sum = 0;
			foreach ($result as &$res) {
				$sum += $res['points'];
				$res = $res['points'];
			}

			$value = null;
			if ($status == POINT_ACTIVE_STATUS) {
				$value = 'total_points + '.$sum;
				$operator = '+';
			} elseif ($status == POINT_REJECTED_STATUS) {
				$value = 'total_points - '.$sum;
				$operator = '-';
			}

			if ($value) {
				$this->db->set('total_points', $value, false)
					->where('registrant_id', $registrant)
					->update('tbl_registrants');

				if ($this->db->affected_rows()) {
					return $result;
				}
			}
		}
		
		return false;
	}

	public function activate($registrant, array $points)
	{
		return $this->change_status($registrant, $points, POINT_ACTIVE_STATUS);
	}

	public function deactivate($registrant, array $points)
	{
		return $this->change_status($registrant, $points, POINT_REJECTED_STATUS);
	}

	private function spend_move_forward_pledge($origin, $params)
	{
		if ($params['points'] === null) {
			$mf = $this->db->select()
				->from('tbl_move_forward')
				->where('move_forward_id', $params['suborigin_id'])
				->where('status', 1)
				->limit(1)
				->get()
				->row();
			if (!$mf) {
				return false;
			}
			$params['points'] = (int) $mf->pledge_points;
		}

		if ($params['points'] > $params['user']->total_points) {
			return false;
		}

		if (!$params['date']) {
			$params['date'] = date('Y-m-d');
		} elseif (is_string($params['date'])) {
			$params['date'] = date('Y-m-d', strtotime($params['date']));
		} elseif (is_int($params['date'])) {
			$params['date'] = date('Y-m-d', $params['date']);
		}

		if ($params['points']) {
			$this->db->insert('tbl_points', array(
				'points' => $params['points'] * -1,
				'origin_id' => MOVE_FWD_PLEDGE_ENTRY,
				'suborigin_id' => $params['suborigin_id'],
				'registrant_id' => $params['registrant_id'],
				'remarks' => isset($params['remarks']) ? $params['remarks'] : null
			));
			if ($this->db->affected_rows()) {
				// $this->earn_move_forward_activity(MOVE_FWD, $params);
				return $params['points'];
			}
		}

		return false;
	}

	private function spend_perks_reserve($origin, $params) 
	{
		if ($params['points'] > $params['user']->total_points) {
			return false;
		}

		if (!$params['date']) {
			$params['date'] = date('Y-m-d');
		} elseif (is_string($params['date'])) {
			$params['date'] = date('Y-m-d', strtotime($params['date']));
		} elseif (is_int($params['date'])) {
			$params['date'] = date('Y-m-d', $params['date']);
		}

		if ($params['points']) {
			$this->db->insert('tbl_points', array(
				'points' => $params['points'] * -1,
				'origin_id' => PERKS_RESERVE,
				'suborigin_id' => $params['suborigin_id'],
				'registrant_id' => $params['registrant_id'],
				'remarks' => isset($params['remarks']) ? $params['remarks'] : null
			));
			if ($this->db->affected_rows()) {
				return $params['points'];
			}
		}

		return false;
	}

	private function spend_perks_buy($origin, $params)
	{
		if ($params['points'] > $params['user']->total_points) {
			return false;
		}

		if (!$params['date']) {
			$params['date'] = date('Y-m-d');
		} elseif (is_string($params['date'])) {
			$params['date'] = date('Y-m-d', strtotime($params['date']));
		} elseif (is_int($params['date'])) {
			$params['date'] = date('Y-m-d', $params['date']);
		}

		if ($params['points']) {
			$this->db->insert('tbl_points', array(
				'points' => $params['points'] * -1,
				'origin_id' => PERKS_BUY,
				'suborigin_id' => $params['suborigin_id'],
				'registrant_id' => $params['registrant_id'],
				'remarks' => isset($params['remarks']) ? $params['remarks'] : null,
				'date_earned' => date('Y-m-d H:i:s')
			));
			if ($this->db->affected_rows()) {
				return $params['points'];
			}
		}

		return false;
	}

	private function spend_perks_bid($origin, $params)
	{
		if ($params['points'] > $params['user']->total_points) {
			return false;
		}

		if (!$params['date']) {
			$params['date'] = date('Y-m-d');
		} elseif (is_string($params['date'])) {
			$params['date'] = date('Y-m-d', strtotime($params['date']));
		} elseif (is_int($params['date'])) {
			$params['date'] = date('Y-m-d', $params['date']);
		}

		if ($params['points']) {
			$this->db->insert('tbl_points', array(
				'points' => $params['points'] * -1,
				'origin_id' => PERKS_BID,
				'suborigin_id' => $params['suborigin_id'],
				'registrant_id' => $params['registrant_id'],
				'remarks' => isset($params['remarks']) ? $params['remarks'] : null,
				'date_earned' => date('Y-m-d H:i:s')
			));
			if ($this->db->affected_rows()) {
				return $params['points'];
			}
		}

		return false;
	}

	public function has_pledge($userId, $offerId)
	{
		$count = $this->db->from('tbl_points')
			->where('registrant_id', $userId)
			->where('origin_id', MOVE_FWD_PLEDGE_ENTRY)
			->where('suborigin_id', $offerId)
			->count_all_results();
		return $count ? true : false;
	}

	private function earn_about_news($origin, $params)
	{
		return 0;
	}

	public function earn($origin, $params = array())
	{

		if (!isset($this->earn_origins[$origin])) {
			show_error('Invalid Origin ID for earning points');
		}
		
		if (method_exists($this, $this->earn_origins[$origin])) {

			$newParams = array();
			$newParams['date'] = isset($params['date']) ? $params['date'] : null;
			$newParams['suborigin_id'] = isset($params['suborigin']) ? $params['suborigin'] : 0;
			$newParams['comment_id'] = isset($params['comment']) ? $params['comment'] : null;
			$newParams['registrant_id'] = isset($params['registrant']) ? $params['registrant'] : $this->session->userdata('user_id');
			$newParams['remarks'] = isset($params['remarks']) ? $params['remarks'] : null;
			$newParams['type'] = isset($params['type']) ? (int) $params['type'] : null;
			$newParams['records'] = isset($params['records']) ? $params['records'] : array();

			echo $this->user;
			if (!$this->user) {
				$newParams['user'] = $this->db->select()
					->from('tbl_registrants')
					->where('registrant_id', $newParams['registrant_id'])
					->limit(1)
					->get()
					->row();

				if (!$newParams['user']) {
					return false;
				}
			} else {
				$newParams['user'] = $this->user;
			}
			
			$resultPoints = call_user_func_array(
				array($this, $this->earn_origins[$origin]), 
				array($origin, $newParams)
			);
			$return = $resultPoints;

			if ($resultPoints) {
				if ($origin === WEBGAMES) {
					// $return = isset($resultPoints[GAME_FINISH_LEVEL]) ? $resultPoints[GAME_FINISH_LEVEL] : 0;
					if (is_array($resultPoints) && !($resultPoints = array_sum($resultPoints))) {
						return false;
					}
				}
				if($origin === LOGIN) {
					if ($resultPoints > 0) {
						$this->db->set('total_points', 'total_points + '.$resultPoints, false);
						$this->db->where('registrant_id', $newParams['registrant_id']);
						$this->db->update('tbl_registrants');
						$this->session->set_userdata('user_points', $newParams['user']->total_points + $resultPoints);
					}
				} else {
					if($resultPoints > 0) {
						if($resultPoints == REFEREE_FIRST_LOGIN_POINT) {
							$this->db->set('total_points', 'total_points + '.$resultPoints, FALSE);
							// $this->db->set('credit_km_points', 'credit_km_points + '.$resultPoints, FALSE);
							$this->db->where('registrant_id', $newParams['registrant_id']);
							$this->db->update('tbl_registrants');
						} else if($resultPoints == REFERER_FIRST_LOGIN_POINT) {
							$this->db->set('total_points', 'total_points + '.$resultPoints, FALSE);
							// $this->db->set('credit_km_points', 'credit_km_points + '.$resultPoints, FALSE);
							$this->db->where('registrant_id', $newParams['registrant_id']);
							$this->db->update('tbl_registrants');
						} else {
							$this->db->set('total_points', 'total_points + '.$resultPoints, false);
							$this->db->where('registrant_id', $newParams['registrant_id']);
							$this->db->update('tbl_registrants');
							$this->session->set_userdata('user_points', $newParams['user']->total_points + $resultPoints);
						}

						// $this->db->set('total_km_points', 'total_km_points + '.$resultPoints, FALSE);
						// $this->db->set('credit_km_points', 'credit_km_points + '.$resultPoints, FALSE);
						// $this->db->where('registrant_id', $newParams['user']->registrant_id);
						// $this->db->update('tbl_registrants');
						// $this->db = $this->db->load('crossover_staging', TRUE);
						// $this->db->set('total_km_points', 'total_km_points + '.$resultPoints, FALSE);
						// $this->db->set('credit_km_points', 'credit_km_points + '.$resultPoints, FALSE);
						// $this->db->where('user_id', $newParams['registrant_id']);
						// $this->db->update('tbl_registrants');
						// $this->db->close();
					}
				}				
			}
			return $return;

		}
		return false;
	}

	public function get_total_points($userId)
	{
		return $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('registrant_id', $userId)
			->select_sum('total_points')
			->get()
			->row()
			->total_points;
	}

	public function get_user_points($from_session = true)
	{
		if ($from_session) {
			return $this->session->userdata('user_points');
		}
		$user = $this->db->select('total_points')
			->from('tbl_registrants')
			->where('registrant_id', $this->session->userdata('user_id'))
			->get()
			->row();
		if (!$user) {
			return false;
		}
		return $user->total_points;
	}

	public function reached_login_limit($userId, $month = null, $year = null)
	{
		if (!$month) {
			$month = date('n');
		}
		if (!$year) {
			$year = date('Y');
		}
		$count = $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('registrant_id', $userId)
			->where('origin_id', LOGIN)
			->where('MONTH(date_earned)', (int) $month)
			->where('YEAR(date_earned)', $year)
			->select_sum('points')->get()->row()->points;
		return $count >= LOGIN_MONTHLY_LIMIT ? true : false;
	}

	public function has_registration_points($userId)
	{
		return $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('origin_id', REGISTRATION)
			->where('registrant_id', $userId)
			->count_all_results();
	}

	public function has_statement_points($userId)
	{
		return $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('origin_id', USER_STATEMENT)
			->where('registrant_id', $userId)
			->count_all_results();
	}

	private function earn_referral($origin, $params)
	{
		
		$exists = $this->db->select()
			->from('tbl_points')
			->where('registrant_id', $params['registrant_id'])
			->where('suborigin_id', $params['suborigin_id'])
			->where('origin_id', REFERRAL)
			->count_all_results();
		if ($exists) {
			return false;
		}

		$insert = $this->db->insert('tbl_points', array(
			'points' => REFERER_BASE_POINT,
			'registrant_id' => $params['registrant_id'],
			'origin_id' => REFERRAL,
			'suborigin_id' => $params['suborigin_id'],
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null
		));

		if ($this->db->affected_rows()) {
			return REFERER_BASE_POINT;
		}
		return 0;
	}

	private function earn_referee_first_login($origin, $params)
	{
		
		$exists = $this->db->select()
			->from('tbl_points')
			->where('registrant_id', $params['registrant_id'])
			->where('suborigin_id', $params['suborigin_id'])
			->where('origin_id', REFEREE_FIRSTLOGIN)
			->count_all_results();
		if ($exists) {
			return false;
		}

		$insert = $this->db->insert('tbl_points', array(
			'points' => REFEREE_FIRST_LOGIN_POINT,
			'registrant_id' => $params['registrant_id'],
			'origin_id' => REFEREE_FIRSTLOGIN,
			'suborigin_id' => $params['suborigin_id'],
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null
		));

		if ($this->db->affected_rows()) {
			return REFEREE_FIRST_LOGIN_POINT;
		}
		return 0;
	}

	private function earn_referer_first_login($origin, $params)
	{
		
		$exists = $this->db->select()
			->from('tbl_points')
			->where('registrant_id', $params['registrant_id'])
			->where('suborigin_id', $params['suborigin_id'])
			->where('origin_id', REFERER_FIRSTLOGIN)
			->count_all_results();
		if ($exists) {
			return false;
		}

		$insert = $this->db->insert('tbl_points', array(
			'points' => REFERER_FIRST_LOGIN_POINT,
			'registrant_id' => $params['registrant_id'],
			'origin_id' => REFERER_FIRSTLOGIN,
			'suborigin_id' => $params['suborigin_id'],
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null
		));

		if ($this->db->affected_rows()) {
			return REFERER_FIRST_LOGIN_POINT;
		}
		return 0;
	}

	private function earn_webgame($origin, $params)
	{
		if (!$params['date']) {
			$params['date'] = date('Y-m-d');
		} elseif (is_string($params['date'])) {
			$params['date'] = date('Y-m-d', strtotime($params['date']));
		} elseif (is_int($params['date'])) {
			$params['date'] = date('Y-m-d', $params['date']);
		}

		$totalAddPoints = array();
		foreach ($params['records'] as $record) {

			if (!$record) {
				continue;
			}
			$pointsId = null;

			if ($record['achievement'] === GAME_FINISH_LEVEL) {

				$monthSum = $this->db->from('tbl_points p')
					->where('p.point_status', POINT_ACTIVE_STATUS)
					->join('tbl_game_achievements gp', 'p.points_id = gp.points_id')
					->where('p.registrant_id', $params['registrant_id'])
					->where('p.origin_id', WEBGAMES)
					//->where('p.suborigin_id', $params['suborigin_id'])
					->where('gp.achievement', GAME_FINISH_LEVEL)
					->where('MONTH(p.date_earned)', date('n', strtotime($params['date'])))
					->where('YEAR(p.date_earned)', date('Y', strtotime($params['date'])))
					->select_sum('p.points')
					->get()
					->row()
					->points;
				if ($monthSum >= GAME_FINISH_LEVEL_MONTHLY_LIMIT) {
					return false;
				}

				$dailySum = $this->db->from('tbl_points p')
					->where('p.point_status', POINT_ACTIVE_STATUS)
					->join('tbl_game_achievements gp', 'p.points_id = gp.points_id')
					->where('p.registrant_id', $params['registrant_id'])
					->where('p.origin_id', WEBGAMES)
					//->where('p.suborigin_id', $params['suborigin_id'])
					->where('gp.achievement', GAME_FINISH_LEVEL)
					->where('DATE(p.date_earned)', $params['date'])
					->select_sum('p.points')
					->get()
					->row()
					->points;
				if ($dailySum >= GAME_FINISH_LEVEL_DAILY_LIMIT) {
					return false;
				}

				if (!isset($record['points']) || $record['points'] > GAME_FINISH_LEVEL_BASE_POINT) {
					return false;
				}

				if ((GAME_FINISH_LEVEL_DAILY_LIMIT - $dailySum) < $record['points']) {
					$record['points'] = GAME_FINISH_LEVEL_DAILY_LIMIT - $dailySum;
				}

				$this->db->insert('tbl_points', array(
					'origin_id' => $origin,
					'suborigin_id' => $params['suborigin_id'],
					'points' => $record['points'],
					'registrant_id' => $params['registrant_id'],
					'remarks' => isset($record['remarks']) ? $record['remarks'] : null //{{ name }} accomplished '.$params['game_title']
				));
				$pointsId = $this->db->insert_id();
				$totalAddPoints[GAME_FINISH_LEVEL] = $record['points'];

			} elseif ($record['achievement'] === GAME_REACH_LEADERBOARD) {

				$weeklySum = $this->db->from('tbl_points p')
					->where('p.point_status', POINT_ACTIVE_STATUS)
					->join('tbl_game_achievements gp', 'p.points_id = gp.points_id')
					->where('p.origin_id', WEBGAMES)
					//->where('p.suborigin_id', $params['suborigin_id'])
					->where('gp.achievement', GAME_REACH_LEADERBOARD)
					->where('p.registrant_id', $params['registrant_id'])
					->where('YEARWEEK(p.date_earned, 0) = YEARWEEK("'.$params['date'].'", 0)')
					->select_sum('p.points')
					->get()
					->row()
					->points;
				if ($weeklySum < GAME_REACH_LEADERBOARD_WEEKLY_LIMIT) {
					$record['points'] = GAME_REACH_LEADERBOARD_BASE_POINT;
					$pointsId = $this->db->insert('tbl_points', array(
						'origin_id' => $origin,
						'suborigin_id' => $params['suborigin_id'],
						'points' => $record['points'],
						'registrant_id' => $params['registrant_id'],
						'remarks' => isset($record['remarks']) ? $record['remarks'] : null
					));
					$pointsId = $this->db->insert_id();
					$totalAddPoints[GAME_REACH_LEADERBOARD] = $record['points'];
				}

			} elseif ($record['achievement'] === GAME_TOP_SCORER) {

				$weeklySum = $this->db->from('tbl_points p')
					->where('p.point_status', POINT_ACTIVE_STATUS)
					->join('tbl_game_achievements gp', 'p.points_id = gp.points_id')
					->where('p.origin_id', WEBGAMES)
					//->where('p.suborigin_id', $params['suborigin_id'])
					->where('gp.achievement', GAME_TOP_SCORER)
					->where('p.registrant_id', $params['registrant_id'])
					->where('YEARWEEK(p.date_earned, 0) = YEARWEEK("'.$params['date'].'", 0)')
					->select_sum('p.points')
					->get()
					->row()
					->points;
				if ($weeklySum < GAME_TOP_SCORER_WEEKLY_LIMIT) {
					$record['points'] = GAME_TOP_SCORER_BASE_POINT;
					$this->db->insert('tbl_points', array(
						'origin_id' => $origin,
						'suborigin_id' => $params['suborigin_id'],
						'points' => $record['points'],
						'registrant_id' => $params['registrant_id'],
						'remarks' => isset($record['remarks']) ? $record['remarks'] : null
					));
					$pointsId = $this->db->insert_id();
					$totalAddPoints[GAME_TOP_SCORER] = $record['points'];
				}

			}

			if ($pointsId) {
				$this->db->insert('tbl_game_achievements', array(
					'points_id' => $pointsId,
					'achievement' => $record['achievement']
				)); 
			}

		}

		return $totalAddPoints;
	}

	private function earn_move_forward_gallery($origin, $params)
	{
		if (!$params['registrant_id']) {
			return false;
		}

		if (!$params['date']) {
			$params['date'] = date('Y-m-d');
		} elseif (is_string($params['date'])) {
			$params['date'] = date('Y-m-d', strtotime($params['date']));
		} elseif (is_int($params['date'])) {
			$params['date'] = date('Y-m-d', $params['date']);
		}

		$points = $this->db->from('tbl_points p')
			->where('p.point_status', POINT_ACTIVE_STATUS)
			->where('p.origin_id', MOVE_FWD_GALLERY)
			->where('p.registrant_id', $params['registrant_id'])
			->where('MONTH(p.date_earned)', date('n', strtotime($params['date'])))
			->where('YEAR(p.date_earned)', date('Y', strtotime($params['date'])))
			->select_sum('p.points')
			->get()
			->row()
			->points;
		if ($points >= MOVE_FWD_FINISH_CHALLENGE_MONTHLY_LIMIT) {
			return false;
		}

		$points = $this->db->from('tbl_points p')
			->where('p.point_status', POINT_ACTIVE_STATUS)
			->where('p.origin_id', MOVE_FWD_GALLERY)
			->where('p.registrant_id', $params['registrant_id'])
			->where('DATE(p.date_earned)', $params['date'])
			->select_sum('p.points')
			->get()
			->row()
			->points;
		if ($points >= MOVE_FWD_FINISH_CHALLENGE_DAILY_LIMIT) {
			return false;
		}

		$this->db->insert('tbl_points', array(
			'points' => MOVE_FWD_FINISH_CHALLENGE_BASE_POINT,
			'registrant_id' => $params['registrant_id'],
			'origin_id' => MOVE_FWD_GALLERY,
			'suborigin_id' => $params['suborigin_id'],
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null //{{ name }} accomplished a task'
		));
		if ($this->db->affected_rows()) {
			return MOVE_FWD_FINISH_CHALLENGE_BASE_POINT;
		}
		return 0;
	}

	private function earn_move_forward_activity($origin, $params)
	{
		if (!$params['registrant_id']) {
			return false;
		}

		if (!$params['date']) {
			$params['date'] = date('Y-m-d');
		} elseif (is_string($params['date'])) {
			$params['date'] = date('Y-m-d', strtotime($params['date']));
		} elseif (is_int($params['date'])) {
			$params['date'] = date('Y-m-d', $params['date']);
		}

		$points = $this->db->from('tbl_points p')
			->where('p.point_status', POINT_ACTIVE_STATUS)
			->where('p.origin_id', MOVE_FWD)
			->where('p.registrant_id', $params['registrant_id'])
			->where('MONTH(p.date_earned)', date('n', strtotime($params['date'])))
			->where('YEAR(p.date_earned)', date('Y', strtotime($params['date'])))
			->select_sum('p.points')
			->get()
			->row()
			->points;
		if ($points >= MOVE_FWD_COMPLETE_ACTIVITY_MONTHLY_LIMIT) {
			return false;
		}

		$this->db->insert('tbl_points', array(
			'points' => MOVE_FWD_COMPLETE_ACTIVITY_BASE_POINT,
			'registrant_id' => $params['registrant_id'],
			'origin_id' => MOVE_FWD,
			'suborigin_id' => $params['suborigin_id'],
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null
		));
		if ($this->db->affected_rows()) {
			return MOVE_FWD_COMPLETE_ACTIVITY_BASE_POINT;
		}
		return 0;
	}

	private function earn_move_forward_winner($origin, $params)
	{
		if (!$params['registrant_id']) {
			return false;
		}

		$points = $params['type'] === 2 ? MOVE_FWD_WINNER_VIDEO_BASE_POINT : MOVE_FWD_WINNER_PHOTO_BASE_POINT;
		$this->db->insert('tbl_points', array(
			'points' => $points,
			'registrant_id' => $params['registrant_id'],
			'origin_id' => MOVE_FWD_WINNER,
			'suborigin_id' => $params['suborigin_id'],
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null
		));

		if (!$this->db->affected_rows()) {
			return 0;
		}
		return $points;
	}

	private function earn_backstage_photos($origin, $params)
	{
		if (!$params['registrant_id']) {
			return false;
		}

		if (!$params['date']) {
			$params['date'] = date('Y-m-d');
		} elseif (is_string($params['date'])) {
			$params['date'] = date('Y-m-d', strtotime($params['date']));
		} elseif (is_int($params['date'])) {
			$params['date'] = date('Y-m-d', $params['date']);
		}

		$points = $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('origin_id', BACKSTAGE_PHOTOS)
			->where('registrant_id', $params['registrant_id'])
			->where('MONTH(date_earned)', date('n', strtotime($params['date'])))
			->where('YEAR(date_earned)', date('Y', strtotime($params['date'])))
			->select_sum('points')
			->get()
			->row()
			->points;
		if ($points >= BACKSTAGE_PHOTOS_APPROVE_MONTHLY_LIMIT) {
			return false;
		}

		$points = $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('origin_id', BACKSTAGE_PHOTOS)
			->where('registrant_id', $params['registrant_id'])
			->where('DATE(date_earned)', $params['date'])
			->select_sum('points')
			->get()
			->row()
			->points;
		if ($points >= BACKSTAGE_PHOTOS_APPROVE_DAILY_LIMIT) {
			return false;
		}

		$this->db->insert('tbl_points', array(
			'points' => BACKSTAGE_PHOTOS_APPROVE_BASE_POINT,
			'registrant_id' => $params['registrant_id'],
			'origin_id' => BACKSTAGE_PHOTOS,
			'suborigin_id' => $params['suborigin_id'],
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null
		));
		if (!$this->db->affected_rows()) {
			return 0;
		}
		return BACKSTAGE_PHOTOS_APPROVE_BASE_POINT;
	}

	private function earn_about_photos($origin, $params)
	{
		if (!$params['registrant_id']) {
			return false;
		}

		if (!$params['date']) {
			$params['date'] = date('Y-m-d');
		} elseif (is_string($params['date'])) {
			$params['date'] = date('Y-m-d', strtotime($params['date']));
		} elseif (is_int($params['date'])) {
			$params['date'] = date('Y-m-d', $params['date']);
		}

		$points = $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('origin_id', ABOUT_PHOTOS)
			->where('registrant_id', $params['registrant_id'])
			->where('MONTH(date_earned)', date('n', strtotime($params['date'])))
			->where('YEAR(date_earned)', date('Y', strtotime($params['date'])))
			->select_sum('points')
			->get()
			->row()
			->points;
		if ($points >= ABOUT_PHOTOS_APPROVE_MONTHLY_LIMIT) {
			return false;
		}

		$points = $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('origin_id', ABOUT_PHOTOS)
			->where('registrant_id', $params['registrant_id'])
			->where('DATE(date_earned)', $params['date'])
			->select_sum('points')
			->get()
			->row()
			->points;
		if ($points >= ABOUT_PHOTOS_APPROVE_DAILY_LIMIT) {
			return false;
		}

		$this->db->insert('tbl_points', array(
			'points' => ABOUT_PHOTOS_APPROVE_BASE_POINT,
			'registrant_id' => $params['registrant_id'],
			'origin_id' => ABOUT_PHOTOS,
			'suborigin_id' => $params['suborigin_id'],
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null
		));
		if (!$this->db->affected_rows()) {
			return 0;
		}
		return ABOUT_PHOTOS_APPROVE_BASE_POINT;
	}

	private function earn_about_videos($origin, $params)
	{
		if (!$params['registrant_id']) {
			return false;
		}

		if (!$params['date']) {
			$params['date'] = date('Y-m-d');
		} elseif (is_string($params['date'])) {
			$params['date'] = date('Y-m-d', strtotime($params['date']));
		} elseif (is_int($params['date'])) {
			$params['date'] = date('Y-m-d', $params['date']);
		}

		$points = $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('origin_id', ABOUT_VIDEOS)
			->where('registrant_id', $params['registrant_id'])
			->where('MONTH(date_earned)', date('n', strtotime($params['date'])))
			->where('YEAR(date_earned)', date('Y', strtotime($params['date'])))
			->select_sum('points')
			->get()
			->row()
			->points;
		if ($points >= ABOUT_VIDEOS_APPROVE_MONTHLY_LIMIT) {
			return false;
		}

		$points = $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('origin_id', ABOUT_PHOTOS)
			->where('registrant_id', $params['registrant_id'])
			->where('DATE(date_earned)', $params['date'])
			->select_sum('points')
			->get()
			->row()
			->points;
		if ($points >= ABOUT_VIDEOS_APPROVE_DAILY_LIMIT) {
			return false;
		}

		$this->db->insert('tbl_points', array(
			'points' => ABOUT_VIDEOS_APPROVE_BASE_POINT,
			'registrant_id' => $params['registrant_id'],
			'origin_id' => ABOUT_VIDEOS,
			'suborigin_id' => $params['suborigin_id'],
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null
		));
		if (!$this->db->affected_rows()) {
			return 0;
		}
		return ABOUT_VIDEOS_APPROVE_BASE_POINT;
	}

	private function earn_move_forward_complete($origin, $params)
	{
		if (!$params['registrant_id']) {
			return false;
		}

		if (!$params['date']) {
			$params['date'] = date('Y-m-d');
		} elseif (is_string($params['date'])) {
			$params['date'] = date('Y-m-d', strtotime($params['date']));
		} elseif (is_int($params['date'])) {
			$params['date'] = date('Y-m-d', $params['date']);
		}

		$points = $this->db->from('tbl_points p')
			->where('p.point_status', POINT_ACTIVE_STATUS)
			->where('p.origin_id', MOVE_FWD_COMPLETE)
			->where('p.registrant_id', $params['registrant_id'])
			->where('MONTH(p.date_earned)', date('n', strtotime($params['date'])))
			->where('YEAR(p.date_earned)', date('Y', strtotime($params['date'])))
			->select_sum('p.points')
			->get()
			->row()
			->points;
		if ($points >= MOVE_FWD_COMPLETE_ALL_MONTHLY_LIMIT) {
			return false;
		}

		$this->db->insert('tbl_points', array(
			'points' => MOVE_FWD_COMPLETE_ALL_BASE_POINT,
			'registrant_id' => $params['registrant_id'],
			'origin_id' => MOVE_FWD_COMPLETE,
			'suborigin_id' => 0,
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null 
		));
		if ($this->db->affected_rows()) {
			return MOVE_FWD_COMPLETE_ALL_BASE_POINT;
		}
		return 0;
	}

	private function earn_explore($origin, $params)
	{

	}

	private function earn_birthday_offers($origin, $params)
	{

	}

	private function earn_flash_offers($origin, $params)
	{

	}

	private function earn_comment($origin, $params)
	{
		if (!$params['registrant_id']) {
			return false;
		}

		if (!$params['date']) {
			$params['date'] = date('Y-m-d');
		} elseif (is_string($params['date'])) {
			$params['date'] = date('Y-m-d', strtotime($params['date']));
		} elseif (is_int($params['date'])) {
			$params['date'] = date('Y-m-d', $params['date']);
		}

		$points = $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('registrant_id', $params['registrant_id'])
			->where('origin_id', COMMENT)
			->where('MONTH(date_earned)', date('n', strtotime($params['date'])))
			->where('YEAR(date_earned)', date('Y', strtotime($params['date'])))
			->select_sum('points')
			->get()
			->row()
			->points;
		if ($points >= COMMENT_MONTHLY_LIMIT) {
			return false;
		}

		$points = $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('registrant_id', $params['registrant_id'])
			->where('origin_id', COMMENT)
			->where('DATE(date_earned)', $params['date'])
			->select_sum('points')
			->get()
			->row()
			->points;
		if ($points >= COMMENT_DAILY_LIMIT) {
			return false;
		}

		$this->db->insert('tbl_points', array(
			'points' => COMMENT_BASE_POINT,
			'registrant_id' => $params['registrant_id'],
			'origin_id' => COMMENT,
			'suborigin_id' => $params['suborigin_id'],
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null
		));
		if (!$this->db->affected_rows()) {
			return false;
		}
		return COMMENT_BASE_POINT;
	}

	private function earn_comment_reply_receive($origin, $params)
	{
		if (!$params['registrant_id']) {
			return false;
		}
		
		if (!$params['date']) {
			$params['date'] = date('Y-m-d');
		} elseif (is_string($params['date'])) {
			$params['date'] = date('Y-m-d', strtotime($params['date']));
		} elseif (is_int($params['date'])) {
			$params['date'] = date('Y-m-d', $params['date']);
		}

		$points = $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('registrant_id', $params['registrant_id'])
			->where('origin_id', COMMENT_REPLY_RECEIVE)
			->where('MONTH(date_earned)', date('n', strtotime($params['date'])))
			->where('YEAR(date_earned)', date('Y', strtotime($params['date'])))
			->select_sum('points')
			->get()
			->row()
			->points;
		if ($points >= COMMENT_REPLY_RECEIVE_MONTHLY_LIMIT) {
			return false;
		}

		$points = $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('registrant_id', $params['registrant_id'])
			->where('origin_id', COMMENT_REPLY_RECEIVE)
			->where('DATE(date_earned)', $params['date'])
			->select_sum('points')
			->get()
			->row()
			->points;
		if ($points >= COMMENT_REPLY_RECEIVE_DAILY_LIMIT) {
			return false;
		}

		$this->db->insert('tbl_points', array(
			'points' => COMMENT_REPLY_RECEIVE_BASE_POINT,
			'registrant_id' => $params['registrant_id'],
			'origin_id' => COMMENT_REPLY_RECEIVE,
			'suborigin_id' => $params['suborigin_id'],
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null
		));
		if (!$this->db->affected_rows()) {
			return false;
		}
		return COMMENT_REPLY_RECEIVE_BASE_POINT;
	}

	private function earn_comment_receive($origin, $params)
	{
		if (!$params['registrant_id']) {
			return false;
		}

		if (!$params['date']) {
			$params['date'] = date('Y-m-d');
		} elseif (is_string($params['date'])) {
			$params['date'] = date('Y-m-d', strtotime($params['date']));
		} elseif (is_int($params['date'])) {
			$params['date'] = date('Y-m-d', $params['date']);
		}

		$points = $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('registrant_id', $params['registrant_id'])
			->where('origin_id', COMMENT_RECEIVE)
			->where('MONTH(date_earned)', date('n', strtotime($params['date'])))
			->where('YEAR(date_earned)', date('Y', strtotime($params['date'])))
			->select_sum('points')
			->get()
			->row()
			->points;
		if ($points >= COMMENT_RECEIVE_MONTHLY_LIMIT) {
			return false;
		}

		$points = $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('registrant_id', $params['registrant_id'])
			->where('origin_id', COMMENT_RECEIVE)
			->where('DATE(date_earned)', $params['date'])
			->select_sum('points')
			->get()
			->row()
			->points;
		if ($points >= COMMENT_RECEIVE_DAILY_LIMIT) {
			return false;
		}

		$this->db->insert('tbl_points', array(
			'points' => COMMENT_RECEIVE_BASE_POINT,
			'registrant_id' => $params['registrant_id'],
			'origin_id' => COMMENT_RECEIVE,
			'suborigin_id' => $params['suborigin_id'],
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null
		));
		if (!$this->db->affected_rows()) {
			return false;
		}
		return COMMENT_RECEIVE_BASE_POINT;
	}

	private function earn_perks_buy($origin, $params)
	{

	}

	private function earn_perks_bid($origin, $params)
	{

	}

	private function earn_perks_reserve($origin, $params)
	{

	}

	private function earn_hidden_marlboro($origin, $params)
	{
		if (!$params['date']) {
			$params['date'] = date('Y-m-d');
		} elseif (is_string($params['date'])) {
			$params['date'] = date('Y-m-d', strtotime($params['date']));
		} elseif (is_int($params['date'])) {
			$params['date'] = date('Y-m-d', $params['date']);
		}

		$points = $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('registrant_id', $params['registrant_id'])
			->where('origin_id', HIDDEN_MARLBORO)
			->where('MONTH(date_earned)', date('n', strtotime($params['date'])))
			->where('YEAR(date_earned)', date('Y', strtotime($params['date'])))
			->select_sum('points')
			->get()
			->row()
			->points;
		if ($points >= HIDDEN_MARLBORO_MONTHLY_LIMIT) {
			return false;
		}

		$points = $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('registrant_id', $params['registrant_id'])
			->where('origin_id', HIDDEN_MARLBORO)
			->where('DATE(date_earned)', $params['date'])
			->select_sum('points')
			->get()
			->row()
			->points;
		if ($points >= HIDDEN_MARLBORO_DAILY_LIMIT) {
			return false;
		}

		$this->db->insert('tbl_points', array(
			'registrant_id' => $params['registrant_id'],
			'origin_id' => HIDDEN_MARLBORO,
			'suborigin_id' => $params['suborigin_id'],
			'points' => HIDDEN_MARLBORO_BASE_POINT,
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null 
		));
		return HIDDEN_MARLBORO_BASE_POINT;
	}

	private function earn_premium_hidden_marlboro($origin, $params)
	{
		if (!$params['date']) {
			$params['date'] = date('Y-m-d');
		} elseif (is_string($params['date'])) {
			$params['date'] = date('Y-m-d', strtotime($params['date']));
		} elseif (is_int($params['date'])) {
			$params['date'] = date('Y-m-d', $params['date']);
		}

		$points = $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('registrant_id', $params['registrant_id'])
			->where('origin_id', PREMIUM_HIDDEN_MARLBORO)
			->where('MONTH(date_earned)', date('n', strtotime($params['date'])))
			->where('YEAR(date_earned)', date('Y', strtotime($params['date'])))
			->select_sum('points')
			->get()
			->row()
			->points;
		if ($points >= PREMIUM_HIDDEN_MARLBORO_MONTHLY_LIMIT) {
			return false;
		}

		$points = $this->db->from('tbl_points')
			->where('point_status', POINT_ACTIVE_STATUS)
			->where('registrant_id', $params['registrant_id'])
			->where('origin_id', PREMIUM_HIDDEN_MARLBORO)
			->where('DATE(date_earned)', $params['date'])
			->select_sum('points')
			->get()
			->row()
			->points;
		if ($points >= PREMIUM_HIDDEN_MARLBORO_DAILY_LIMIT) {
			return false;
		}

		$this->db->insert('tbl_points', array(
			'registrant_id' => $params['registrant_id'],
			'origin_id' => PREMIUM_HIDDEN_MARLBORO,
			'suborigin_id' => $params['suborigin_id'],
			'points' => PREMIUM_HIDDEN_MARLBORO_BASE_POINT,
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null 
		));
		return PREMIUM_HIDDEN_MARLBORO_BASE_POINT;
	}

	private function earn_registration($origin, $params)
	{
		if ($this->has_registration_points($params['registrant_id'])) {
			return false;
		}
		$this->db->insert('tbl_points', array(
			'registrant_id' => $params['registrant_id'],
			'origin_id' => $origin,
			'suborigin_id' => 0,
			'points' => REGISTRATION_BASE_POINT,
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null 
		));
		return REGISTRATION_BASE_POINT;
	}

	private function earn_user_statement($origin, $params)
	{
		if ($this->has_statement_points($params['user']->registrant_id)) {
			return false;
		}

		$this->db->insert('tbl_points', array(
			'origin_id' => $origin,
			'suborigin_id' => 0,
			'registrant_id' => $params['registrant_id'],
			'points' => USER_STATEMENT_BASE_POINT,
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null 
		));
		return USER_STATEMENT_BASE_POINT;
	}

	private function earn_login($origin, $params)
	{
		if (!$params['date']) {
			$params['date'] = date('Y-m-d');
		} elseif (is_string($params['date'])) {
			$params['date'] = date('Y-m-d', strtotime($params['date']));
		} elseif (is_int($params['date'])) {
			$params['date'] = date('Y-m-d', $params['date']);
		}
		$strtime = strtotime($params['date']);
		if ($params['user']->last_login && 
				(
					date('Y-m-d', strtotime($params['user']->last_login)) == $params['date'] ||
					$this->reached_login_limit($params['user']->registrant_id, date('n', $strtime), date('Y', $strtime))
				)
			)
		{
			return false;
		}

		$this->db->insert('tbl_points', array(
			'origin_id' => $origin,
			'suborigin_id' => $params['suborigin_id'],
			'points' => LOGIN_BASE_POINT,
			'registrant_id' => $params['registrant_id'],
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null
		));
		return LOGIN_BASE_POINT;
	}

	public function earn_login_km($origin, $params)
	{
		$row = $this->db->select()->from('tbl_registrants')->where('registrant_id', $this->session->userdata('user_id'))->get()->row();
		if($row) {
			if(file_exists('uploads/profile/'.$row->registrant_id.'/'.$row->current_picture) && $row->current_picture) {
				$this->session->set_userdata('profile_picture', base_url().'uploads/profile/'.$row->registrant_id.'/'.$row->current_picture);
			} else {
				$this->session->set_userdata('profile_picture', 'http://marlboro-stage2.yellowhub.com/spice/images/default-profile-photo.png');
			}
		}

		if (!$params['date']) {
			$params['date'] = date('Y-m-d');
		} elseif (is_string($params['date'])) {
			$params['date'] = date('Y-m-d', strtotime($params['date']));
		} elseif (is_int($params['date'])) {
			$params['date'] = date('Y-m-d', $params['date']);
		}
		$strtime = strtotime($params['date']);
		
		$registrant_id = isset($params['registrant']) ? $params['registrant'] : $this->session->userdata('user_id');

		if (!$this->user) {
			$params['user'] = $this->db->select()
				->from('tbl_registrants')
				->where('registrant_id', $registrant_id)
				->limit(1)
				->get()
				->row();

			if (!$params['user']) {
				return false;
			}
		} else {
			$params['user'] = $this->user;
		}

		if( in_array( $_SERVER['REMOTE_ADDR'], array( '127.0.0.1', '::1' ) ) )
        { //check if localhost or in staging / prod server
                $this->db = $this->load->database('crossover_local', TRUE);
        }else
        {
                $this->db = $this->load->database('crossover_staging', TRUE);
        }
        

        $user = $this->db->select()
                ->from('tbl_registrants')
                ->where('user_id', $registrant_id)
                ->limit(1)
                ->get()
                ->row();

        $isLoggedInToMobile = $this->db->select()
		                ->from('tbl_login')
		                ->where('registrant_id', $user->registrant_id)
		                ->where('source', 1)
		                ->where('DATE(date_login)', date('Y-m-d'))
		                ->limit(1)
		                ->get()
		                ->row();
        
        $resultPoints = 20;

        $return = $resultPoints;
        $user_count = count($user);

        if($user_count != 0 ){
        	if(!$isLoggedInToMobile){
                if ($return) {
                    if ($return > 0) {


                    	if( in_array( $_SERVER['REMOTE_ADDR'], array( '127.0.0.1', '::1' ) ) )
				        { //check if localhost or in staging / prod server
				                $this->db = $this->load->database('default', TRUE);
				        }else
				        {
				                $this->db = $this->load->database('actual', TRUE);
				        }

                    	if ($params['user']->last_login && 
								(
									date('Y-m-d', strtotime($params['user']->last_login)) == date('Y-m-d') ||
									$this->reached_login_limit($params['user']->registrant_id, date('n', $strtime), date('Y', $strtime))
								)
							)
						{
							return false;
						}
						$this->db->close();

						if( in_array( $_SERVER['REMOTE_ADDR'], array( '127.0.0.1', '::1' ) ) )
				        { //check if localhost or in staging / prod server
				                $this->db = $this->load->database('crossover_local', TRUE);
				        }else
				        {
				                $this->db = $this->load->database('crossover_staging', TRUE);
				        }

                    	$this->db->insert('tbl_points', array(
			                'origin_id' => $origin,
			                'points_earned' => $resultPoints,
			                'user_id' => $registrant_id,
			            ));

			            $this->db->insert('tbl_login', array(
			                'registrant_id' => $user->registrant_id,
			                'date_login' => date('Y-m-d H:i:s'),
			                'source' => '2'
			            ));

                        $this->db->set('total_km_points', 'total_km_points + '.$return, false);
                        $this->db->set('credit_km_points', 'credit_km_points + '.$return, false);
                        $this->db->set('first_name', $this->session->userdata('user')['first_name']);
                        $this->db->set('middle_initial', $this->session->userdata('user')['middle_initial']);
                        $this->db->set('third_name', $this->session->userdata('user')['last_name']);
                        $this->db->set('nick_name', $this->session->userdata('user')['nick_name']);
                        $this->db->set('person_id', $this->session->userdata('user')['person_id']);
                        $this->db->where('user_id', $registrant_id);
                        $this->db->update('tbl_registrants');
                        $this->session->set_userdata('total_km_points', $user->total_km_points + $return);
                        $this->session->set_userdata('credit_km_points', $user->credit_km_points + $return);
                    }
                }
            }else{
            	$this->db->insert('tbl_login', array(
	                'registrant_id' => $user->registrant_id,
	                'date_login' => date('Y-m-d H:i:s'),
	                'source' => '2'
	            ));
            }
        }else{      
        	if(!$isLoggedInToMobile){
                if ($return) {
                    if ($return > 0) {

                        $this->db->insert('tbl_points', array(
			                'origin_id' => $origin,
			                'points_earned' => $resultPoints,
			                'user_id' => $registrant_id,
			            ));

			            $this->db->insert('tbl_registrants', array(
                            'person_id' => $this->session->userdata('user')['person_id'],
                            'user_id' => $registrant_id,
                            'total_km_points' => $return,
                            'credit_km_points' => $return,
                            'first_name' => $this->session->userdata('user')['first_name'],
                            'third_name' => $this->session->userdata('user')['last_name'],
                            'middle_initial' => $this->session->userdata('user')['middle_initial'],
                            'nick_name' => $this->session->userdata('user')['nick_name'],
                        ));
                        
			            $this->db->insert('tbl_login', array(
			                'registrant_id' => $this->db->insert_id(),
			                'date_login' => date('Y-m-d H:i:s'),
			                'source' => '2'
			            ));

                        $this->session->set_userdata('xover_first_login', '1');

                        $this->db->close();
                        $this->db = $this->load->database('actual', TRUE);

                        $this->db->set('total_km_points', 'total_km_points + '.$return, false);
                        $this->db->set('credit_km_points', 'credit_km_points + '.$return, false);
                        $this->db->where('registrant_id', $registrant_id);
                        $this->db->update('tbl_registrants');

                    }
                }
            }else{
            	$this->db->insert('tbl_login', array(
	                'registrant_id' => $user->registrant_id,
	                'date_login' => date('Y-m-d H:i:s'),
	                'source' => '2'
	            ));
            }
        }

        return $return;

	}

	private function earn_survey($origin, $params)
	{

		$survey = $this->db->select()->from('tbl_survey')->where('id',  $params['suborigin_id'])->get()->row();
		$points = $survey->points;

		$exists = $this->db->select()
			->from('tbl_points')
			->where('registrant_id', $params['registrant_id'])
			->where('suborigin_id', $params['suborigin_id'])
			->where('origin_id', SURVEY)
			->count_all_results();
		if ($exists) {
			return false;
		}

		$insert = $this->db->insert('tbl_points', array(
			'points' => $points ,
			'registrant_id' => $params['registrant_id'],
			'origin_id' => SURVEY,
			'suborigin_id' => $params['suborigin_id'],
			'remarks' => isset($params['remarks']) ? $params['remarks'] : null
		));

		if ($this->db->affected_rows()) {
			return $points;
		}
		return 0;
	}
}