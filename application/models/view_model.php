<?php

class View_Model extends CI_Model
{
	public function visit($origin, $suborigin)
	{
		$date = date('Y-m-d');
		$user = $this->session->userdata('user_id');
		if ($user) {
			$count = $this->db->from('tbl_views')
				->where('registrant_id', $user)
				->where('origin_id', $origin)
				->where('suborigin_id', $suborigin)
				->where('DATE(date_viewed)', $date)
				->count_all_results();
			if ($count < VIEW_LIMIT_PER_DAY) {
				$this->db->insert('tbl_views', array(
					'registrant_id' => $user,
					'origin_id' => $origin,
					'suborigin_id' => $suborigin,
					'ip_address' => $this->input->ip_address()
				));
				if ($this->db->affected_rows()) {
					return true;
				}
			}	
		}
		return false;
	}

	public function get_visits_count($origin, $suborigin)
	{
		if ($this->session->userdata('user_id')) {
			return $this->db->from('tbl_views')
				->where('origin_id', $origin)
				->where('suborigin_id', $suborigin)
				->count_all_results();
		}
		return false;
	}
}