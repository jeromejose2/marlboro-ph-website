<?php

class Movefwd_Model extends CI_Model
{
	public function get_categories()
	{
		return $this->db->select()
			->from('tbl_categories')
			->where('origin_id', MOVE_FWD)
			->where('is_deleted', 0)
			->get()
			->result_array();
	}

	public function has_pledge_done($user, $offer)
	{
		$count = $this->db->from('tbl_move_forward_choice')
			->where('move_forward_choice_status', 1)
			->where('move_forward_choice_started', 1)
			->where('move_forward_id', $offer)
			->where('registrant_id', $user)
			->count_all_results();
		return $count ? true : false;
	}

	public function choose_action($userId, $offerId, $action)
	{
		if ($action === MOVE_FWD_PLAY || $action === MOVE_FWD_PLEDGE) {
			$data = array(
				'registrant_id' => $userId,
				'move_forward_id' => $offerId,
				'move_forward_choice' => $action
			);
			if ($action === MOVE_FWD_PLEDGE) {
				$data['move_forward_choice_status'] = 1;
				$data['move_forward_choice_done'] = date('Y-m-d H:i:s');
			}
			$this->db->insert('tbl_move_forward_choice', $data);
			if ($this->db->affected_rows()) {
				return true;
			}
		}
		return false;
	}

	public function is_complete_all_activity($registrant_id, &$move_forward = 0, &$gallery = 0, $move_forward_id = false)
	{
		require_once 'administrator/models/move_fwd_model.php';
		return (new Move_fwd_model)->is_complete_all_activity($registrant_id, $move_forward, $gallery, $move_forward_id);
	}

	public function get_entries_by_offer($offerId)
	{
		return $this->db->select('r.current_picture, r.first_name, r.third_name, mfg.*, c.*')
			->from('tbl_move_forward_gallery mfg')
			->join('tbl_challenges c', 'c.challenge_id = mfg.challenge_id')
			->join('tbl_registrants r', 'r.registrant_id = mfg.registrant_id')
			->join('tbl_move_forward mf', 'mf.move_forward_id = c.move_forward_id')
			->order_by('mfg.mfg_date_created', 'DESC')
			->where('c.move_forward_id', $offerId)
			->where('mfg.mfg_status', 1)
			->get()
			->result_array();
	}

	public function decrement_slot($id)
	{
		$this->db->set('slots', 'slots - 1', false);
		$this->db->where('move_forward_id', $id);
		$this->db->where('status', 1);
		$this->db->update('tbl_move_forward');
		if ($this->db->affected_rows()) {
			return true;
		}
		return false;
	}

	public function pledge_done($userId, $offerId)
	{
		$this->db->where('registrant_id', $userId)
			->where('move_forward_id', $offerId)
			->where('move_forward_choice', MOVE_FWD_PLEDGE)	
			->update('tbl_move_forward_choice', array(
				'move_forward_choice_status' => 1,
				'move_forward_choice_done' => date('Y-m-d H:i:s')
			));
		if ($this->db->affected_rows()) {
			return true;
		}
		return false;
	}

	public function get_entries($userId, $offerId)
	{
		$entries = array();
		$tmp = $this->db->select()
			->from('tbl_move_forward_gallery mfg')
			->join('tbl_challenges c', 'c.challenge_id = mfg.challenge_id')
			->where('mfg.registrant_id', $userId)
			->where('c.move_forward_id', $offerId)
			->where('mfg.mfg_status', 1)
			// ->where('mfg_is_public', 1)
			->limit(MOVE_FWD_ENTRY_LIMIT)
			->get()
			->result_array();
		foreach ($tmp as $t) {
			$entries[$t['challenge_id']] = $t;
		}
		unset($tmp);
		return $entries;
	}

	public function get_pending_entry($user, $challengeId)
	{
		return (array) $this->db->select()
			->from('tbl_move_forward_gallery mfg')
			->where('mfg.challenge_id', $challengeId)
			->where('mfg.mfg_status', 0)
			->where('mfg.registrant_id', $user)
			->limit(1)
			->get()
			->row();
	}

	public function get_offers_by_category($category_id)
	{
		return $this->db->select()
			->from('tbl_move_forward')
			->where('category_id', $category_id)
			->where('is_deleted', 0)
			->where('platform_restriction !=', MOBILE_ONLY)
			->order_by('status', 'DESC')
			->get()
			->result_array();
	}

	public function get_challenges_by_offer($offerId)
	{
		return $this->db->select()
			->from('tbl_challenges c')
			->join('tbl_move_forward mf', 'c.move_forward_id = mf.move_forward_id')
			->where('c.move_forward_id', $offerId)
			->where('mf.status', 1)
			->where('mf.is_deleted', 0)
			->limit(MOVE_FWD_ENTRY_LIMIT)
			->get()
			->result_array();
	}

	public function get_chosen_action($userId, $moveForwardId)
	{
		$choice = $this->db->select()
			->from('tbl_move_forward_choice')
			->where('registrant_id', $userId)
			->where('move_forward_id', $moveForwardId)
			->where('move_forward_choice_started', 1)
			->limit(1)
			->get()
			->row();
		if (!$choice) {
			return false;
		}
		return $choice->move_forward_choice == MOVE_FWD_PLEDGE ? MOVE_FWD_PLEDGE : MOVE_FWD_PLAY;
	}

	public function get_offer_by_permalink($permalink)
	{
		return (array) $this->db->select()
			->from('tbl_move_forward')
			->where('permalink', $permalink)
			->where('is_deleted', 0)
			->limit(1)
			->get()
			->row();
	}

	public function get_participants($offer_id, $page = 1, $limit = 9)
	{
		return $this->db->select('r.registrant_id, r.current_picture')
			->from('tbl_move_forward_choice mfc')
			->join('tbl_registrants r', 'mfc.registrant_id = r.registrant_id')
			->join('tbl_move_forward mf', 'mf.move_forward_id = mfc.move_forward_id')
			->where('mfc.move_forward_id', $offer_id)
			->where('mfc.move_forward_choice_started', 1)
			->where('mfc.registrant_id !=', $this->session->userdata('user_id'))
			->order_by('mfc.move_forward_choice_date', 'DESC')
			->limit($limit, ($page - 1) * $limit)
			->get()
			->result_array();
	}

	public function get_participants_count($offer_id)
	{
		return $this->db->from('tbl_move_forward_choice mfc')
			->join('tbl_registrants r', 'mfc.registrant_id = r.registrant_id')
			->join('tbl_move_forward mf', 'mf.move_forward_id = mfc.move_forward_id')
			->where('mfc.move_forward_id', $offer_id)
			->where('mfc.move_forward_choice_started', 1)
			->where('mfc.registrant_id !=', $this->session->userdata('user_id'))
			->count_all_results();
	}

	public function get_active_offer($id)
	{
		return (array) $this->db->select()
			->from('tbl_move_forward')
			->where('move_forward_id', $id)
			->where('status', 1)
			->where('is_deleted', 0)
			->limit(1)
			->get()
			->row();
	}

	public function get_active_offers()
	{
		return $this->db->select()
			->from('tbl_move_forward')
			->where('is_deleted', 0)
			->where('status', 1)
			->where('platform_restriction !=', MOBILE_ONLY)
			->get()
			->result_array();
	}

	public function get_offer($id, $link = null)
	{
		$offer = $this->db->select()
			->from('tbl_move_forward')
			->where('move_forward_id', $id)
			->where('platform_restriction !=', MOBILE_ONLY)
			->where('is_deleted', 0);
		if ($link) {
			$offer->where('permalink', $link);
		}
		return (array) $offer->limit(1)->get()->row();
	}
	
	public function get_tasks_count($id)
	{
		return $this->db->select()
			->from('tbl_challenges')
			->where('move_forward_id', $id)
			->count_all_results();
	}

	public function get_gallery($id)
	{
		$gallery = $this->db->select('c.*, 0 as comment_is_reply', false)
			->from('tbl_comments c')
			// ->join('tbl_registrants r', 'r.registrant_id = c.registrant_id')
			->where('c.origin_id', MOVE_FWD)
			->where('c.suborigin_id', $id)
			->where('c.comment_status', 1)
			->where('c.comment_image IS NOT NULL')
			->where('c.comment_image !=', '')
			->order_by('c.comment_date_created', 'DESC')
			->get()
			->result_array();

		if ($gallery) {

			$commentIds = array();
			foreach ($gallery as $gal) {
				$commentIds[] = $gal['comment_id'];
			}

			$replies = $this->db->select('cr.*, 1 as comment_is_reply', false)
				->from('tbl_comment_replies cr')
				// ->join('tbl_registrants r', 'r.registrant_id = cr.registrant_id')
				->where('cr.comment_reply_status', 1)
				->where('cr.comment_reply_image IS NOT NULL')
				->where('cr.comment_reply_image !=', '')
				->where_in('cr.comment_id', $commentIds)
				->order_by('cr.comment_reply_date_created', 'DESC')
				->get()
				->result_array();

			foreach ($replies as $reply) {
				$gallery[] = array(
					'comment_id' => $reply['comment_reply_id'],
					'comment_is_reply' => $reply['comment_is_reply'],
					'comment_image' => $reply['comment_reply_image'],
					'registrant_id' => $reply['registrant_id']
				);
			}

			unset($replies, $commentIds);

		}
		
		return $gallery;
	}

	public function has_viewed_splash() {
		$has_session = true;
		if (!$this->session->userdata('move_fwd')) {
			$has_session = false;
			$this->session->set_userdata('move_fwd', 1);
		}
		$splash_count = $this->db->select('*')
								->from('tbl_settings')
								->where('type', 'move_fwd_splash_count')
								->get()
								->row();


		$viewed = $this->db->select('*')
							->from('tbl_page_visits')
							->join('tbl_login', 'tbl_login.login_id = tbl_page_visits.login_id')
							->where('section', 'move_forward')
							->where('registrant_id', $this->session->userdata('user_id'))
							->get()
							->num_rows();
		return $viewed > (int) $splash_count->description || $has_session ? true : false;
	}
}