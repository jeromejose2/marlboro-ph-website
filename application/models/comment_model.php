<?php

class Comment_Model extends CI_Model
{
	public function save($origin, $suborigin, $comment, $media = null, $recipient = 0)
	{
		if (!$this->session->userdata('user_id')) {
			return false;
		}

		$data = array();
		$data['origin_id'] = $origin;
		$data['suborigin_id'] = $suborigin;
		$data['comment'] = $comment;
		$data['recipient_id'] = $recipient;
		if ($media) {
			$data['comment_image'] = $media;
		}
		$data['registrant_id'] = $this->session->userdata('user_id');
		if ($this->session->userdata('is_cm')) {
			$data['comment_status'] = 1;
		}
		$this->db->insert('tbl_comments', $data);
		return $this->db->insert_id();
	}

	public function get_comments_count($origin, $suborigin)
	{
		return $this->db->from('tbl_comments c')
			->join('tbl_registrants r', 'r.registrant_id = c.registrant_id')
			->where('c.origin_id', $origin)
			->where('c.suborigin_id', $suborigin)
			->where('c.comment_status', 1)
			->count_all_results();
	}

	public function get_comments_replies_count($origin, $suborigin)
	{
		$this->db->select('c.comment_id')
			->from('tbl_comments c')
			->join('tbl_registrants r', 'r.registrant_id = c.registrant_id')
			->where('c.origin_id', $origin);

		if ($suborigin) {
			if (is_array($suborigin)) {
				$this->db->where_in('c.suborigin_id', $suborigin);
			} else {
				$this->db->where('c.suborigin_id', $suborigin);
			}
		} else {
			show_error('Comment_Model::get_comments_replies_count suborigin must not be empty');
		}
		
		$comments = $this->db->where('c.comment_status', 1)
						->get()
						->result_array();

		$count = count($comments);
		if ($count) {
			foreach ($comments as &$comment) {
				$comment = $comment['comment_id'];
			}
			$count += $this->db->from('tbl_comment_replies')
				->where('comment_reply_status', 1)
				->where_in('comment_id', $comments)
				->count_all_results();
		}
		
		return $count;
	}

	public function get($origin, $suborigin, $page = 1, $limit = 20)
	{
		$comments = array();
		if (!$page) {
			$page = 1;
		}
		$offset = ($page - 1) * $limit;
		$tmp = $this->db->select('r.current_picture, r.nick_name, r.first_name, r.first_name, r.is_cm, c.*')
			->from('tbl_comments c')
			->join('tbl_registrants r', 'r.registrant_id = c.registrant_id')
			->where('c.origin_id', $origin)
			->where('c.suborigin_id', $suborigin)
			->where('c.comment_status', 1)
			->order_by('c.comment_date_created', 'DESC')
			->limit($limit, $offset)
			->get()
			->result_array();
		foreach ($tmp as $t) {
			if($t['is_cm'] == 1) {
				$tname = 'Marlboro';
				$tprofile_picture = 'https://marlboro.ph/uploads/profile/181493/f6ce5d77133e5859df8074a3295f8fce.png';
			} else {
				$tname = $t['nick_name'] != '0' ? $t['nick_name'] : $t['first_name'];
				$tprofile_picture = $t['current_picture'] && checkImageIfExist(BASE_URL.'uploads/profile/'.$t['registrant_id'].'/'.$t['current_picture']) ? BASE_URL.'uploads/profile/'.$t['registrant_id'].'/'.$t['current_picture'] : BASE_URL.DEFAULT_IMAGE;
			}
			$comments['c'.$t['comment_id']] = array(
				'id' => $t['comment_id'],
				'name' => $tname,
				'profile_picture' => $tprofile_picture,
				'media' => $t['comment_image'] ? BASE_URL.'uploads/profile/'.$t['registrant_id'].'/comments/150_150_'.$t['comment_image'] : '',
				'text' => $t['comment'],
				'date' => date('m/d/Y g:i A', strtotime($t['comment_date_created'])),
				'replies' => array()
			);
		}
		if ($commentIds = array_keys($comments)) {
			array_walk($commentIds, function (&$item) {
				$item = (int) str_replace('c', '', $item);
			});
			$replies = $this->db->select('r.nick_name, r.current_picture, r.first_name, r.is_cm, cr.*')
				->from('tbl_comment_replies cr')
				->join('tbl_registrants r', 'cr.registrant_id = r.registrant_id')
				->where_in('cr.comment_id', $commentIds)
				->order_by('cr.comment_reply_date_created', 'desc')
				->where('cr.comment_reply_status', 1)
				->get()
				->result_array();
			foreach ($replies as $reply) {
				if($reply['is_cm'] == 1) {
					$rname = 'Marlboro';
					$rprofile_picture = 'https://marlboro.ph/uploads/profile/181493/f6ce5d77133e5859df8074a3295f8fce.png';
				} else {
					$rname = $reply['nick_name'] != '0' ? $reply['nick_name'] : $reply['first_name'];
					$rprofile_picture = $reply['current_picture'] && checkImageIfExist(BASE_URL.'uploads/profile/'.$reply['registrant_id'].'/'.$reply['current_picture']) ? BASE_URL.'uploads/profile/'.$reply['registrant_id'].'/'.$reply['current_picture'] : BASE_URL.DEFAULT_IMAGE;
				}
				if (!$reply['comment_reply_to_id']) {
					$comments['c'.$reply['comment_id']]['replies']['r'.$reply['comment_reply_id']] = array(
						'id' => $reply['comment_reply_id'],
						'name' => $rname,
						'profile_picture' => $rprofile_picture,
						'media' => $reply['comment_reply_image'] ? BASE_URL.'uploads/profile/'.$reply['registrant_id'].'/comments/150_150_'.$reply['comment_reply_image'] : '',
						'text' => $reply['comment_reply'],
						'date' => date('m/d/Y g:i A', strtotime($reply['comment_reply_date_created'])),
						'replies' => array()
					);
				} else {
					$comments['c'.$reply['comment_id']]['replies'][$reply['comment_reply_to_id']]['replies']['r'.$reply['comment_reply_id']] = array(
						'id' => $reply['comment_reply_id'],
						'name' => $rname,
						'profile_picture' => $reply['current_picture'] ? BASE_URL.'uploads/profile/'.$reply['registrant_id'].'/'.$reply['current_picture'] : BASE_URL.DEFAULT_IMAGE,
						'media' => $rprofile_picture, #$reply['comment_reply_image'] ? BASE_URL.'uploads/profile/'.$reply['registrant_id'].'/comments/150_150_'.$reply['comment_reply_image'] : '',
						'text' => $reply['comment_reply'],
						'date' => date('m/d/Y g:i A', strtotime($reply['comment_reply_date_created']))
					);
				}
			}
		}
		unset($tmp, $commentIds);
		return $comments;
	}

	public function reply_to($comment_id, $text, $media = null)
	{
		if (!$this->session->userdata('user_id')) {
			return false;
		}

		$data = array();
		$data['comment_reply'] = $text;
		$data['comment_id'] = $comment_id;
		if ($media) {
			$data['comment_reply_image'] = $media;
		}
		$data['registrant_id'] = $this->session->userdata('user_id');
		if ($this->session->userdata('is_cm')) {
			$data['comment_reply_status'] = 1;
		}
		$this->db->insert('tbl_comment_replies', $data);
		return $this->db->insert_id();
	}

	public function get_replies($commentId, $page = 1, $limit = 20)
	{
		return $this->db->select()
			->from('tbl_comment_replies cr')
			->join('tbl_registrants r', 'r.registrant_id = cr.registrant_id')
			->where('comment_id', $commentId)
			->limit($limit)
			->get()
			->result_array();
	}
}