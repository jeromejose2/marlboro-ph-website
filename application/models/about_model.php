<?php

class About_Model extends CI_Model
{
	public function get_brand_history()
	{
		return $this->db->select('brand_history_title, brand_history_year_desc, brand_history_year, brand_history_description, brand_history_media, brand_history_type, brand_history_title_type, brand_history_explore_copy, brand_history_explore_link')
			->from('tbl_brand_history')
			->where('brand_history_status', 1)
			->order_by('brand_history_year')
			->get()
			->result_array();
	}

	public function get_product_info()
	{
		return $this->db->select('i.*, SUM(m.product_info_id) as product_media_num', false)
			->from('tbl_product_info i')
			->join('tbl_product_media m', 'm.product_info_id = i.product_info_id', 'left')
			->where('i.product_info_status', 1)
			->group_by('i.product_info_id')
			->get()
			->result_array();
	}

	public function get_dbam_content()
	{
		return $this->db->select("dbam_id, dbam_title, dbam_duration, dbam_thumbnail, dbam_content, dbam_type, IF(dbam_type = 1, '', 1) as dbam_is_video", false)
			->from('tbl_dbam')
			->where('dbam_status', 1)
			->order_by('dbam_id', 'random')
			->get()
			->result_array();
	}

	public function get_product_info_gallery($id)
	{
		return $this->db->select()
			->from('tbl_product_media m')
			->join('tbl_product_info i', 'm.product_info_id = i.product_info_id')
			->where('i.product_info_id', $id)
			->where('i.product_info_status', 1)
			->get()
			->result_array();
	}
}