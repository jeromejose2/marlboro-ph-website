<?php

class Perks_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
	}
	public function get_buy_items() {
		$items = $this->db->select('*')
						  ->from('tbl_buy_items')
						  ->where('status !=', 0)
						  ->where('is_deleted', 0)
						  ->where('platform_restriction !=', MOBILE_ONLY)
						  ->order_by('created_date', 'DESC')
						  ->get()
						  ->result_array();
		return $items;
	}

	public function get_buy_item($id) {
		$items = $this->db->select('*')
						  ->from('tbl_buy_items')
						  ->where('buy_item_id', $id)
						  ->get()
						  ->row();
		return $items;	
	}

	public function get_property_item($id, $properties, $values) {
		if($properties) {
			$stocks = $this->count_stock(@$values[0], @$values[1], $id);
			if($stocks) {
				return (int) $stocks['stocks'];
			}
			return (int) 0;
		} else {
			return (int) 0;
		}
	}

	public function get_buy_item_media($id) {
		$media = $this->db->select('*')
						  ->from('tbl_media')
						  ->where('origin_id', PERKS_BUY)
						  ->where('suborigin_id', $id)
						  ->get()
						  ->result_array();
		return $media;	
	}

	public function get_properties($id) {
		$properties = $this->db->select('*')
								->from('tbl_properties p')
								->where('p.buy_item_id', $id)
								->order_by('p.property_id')
								->get()
								->result_array();

		$property_ids = $property_values = array();
		if($properties) {
			foreach ($properties as $key => $value) {
				$property_ids[] = $value['property_id'];
			}

			$property_values = $this->db->select('*')
								->from('tbl_property_values v')
								->where_in('property_id', $property_ids)
								->order_by('v.property_id')
								->order_by('v.property_value_id')
								->get()
								->result_array();
		}

		
		$properties_arr = array();
		if($properties) {
			foreach ($properties as $pk => $pv) {
				if($property_values) {
					$pvalues = array();
					$pstocks = array();
					foreach ($property_values as $vk => $vv) {
						if($vv['property_id'] == $pv['property_id'])
							$pvalues[] = $vv['value'];
							//$pstocks[] = $vv['stock'];		
					}
					$pv['pvalues'] = $pvalues;
					$pv['pstocks'] = $pstocks;
					
				}
				$properties_arr[] = $pv;
			}
		}
		// echo '<pre>';
		// print_r($properties_arr);
		// die();
		return $properties_arr;
	}

	public function get_property_id($item_id, $value) {
		$property = (array) $this->db->select('*')
									 ->from('tbl_properties')
									 ->join('tbl_property_values', 'tbl_properties.property_id=tbl_property_values.property_id')
									 ->where('value', $value)
									 ->where('buy_item_id', $item_id)
									 ->get()
									 ->row();
		$property_id = isset($property['property_value_id']) ? $property['property_value_id'] : 0;
		return $property_id; 
	}

	public function get_properties_with_stocks($item_id, $property1, &$single_property_stock = 0) {
		$property_value_id = $this->get_property_id($item_id, $property1);
		$properties = $this->db->select('value, stocks')
							  	->from('tbl_property_stocks')
							  	->join('tbl_property_values', 'tbl_property_stocks.property_value2_id=tbl_property_values.property_value_id')
							  	->where('property_value1_id', $property_value_id)
							  	->where('stocks >', 0)
							  	->get()
							  	->result_array();

		$single_p_stock = $this->db->select('stocks')
								   ->from('tbl_property_stocks')
								   ->where('buy_item_id', $item_id)
								   ->where('property_value1_id', $property_value_id)
								   ->where('property_value2_id', 0)
								   ->get()
								   ->row();
		$single_property_stock = @$single_p_stock->stocks;

		return $properties;
	}

	public function count_stock($property1, $property2, $item_id) {
		$property_id1 = $this->get_property_id($item_id, $property1);
		$property_id2 = $this->get_property_id($item_id, $property2);
		$stock_count = (array) $this->db->select('*')
										->from('tbl_property_stocks')
										->where('property_value1_id', $property_id1)
										->where('property_value2_id', $property_id2)
										->where('buy_item_id', $item_id)
										->get()
										->row();
		return $stock_count;
	}

	public function check_stocks($id) {
		$param = array();
		$properties = $this->input->post('prop');
		$values = $this->input->post('val');
		$stocks = array();

		if($properties) {
			$stocks = $this->count_stock(@$values[0], @$values[1], $this->input->post('id'));
		} else {
			$item_stock = $this->db->select()->from('tbl_buy_items')->where('buy_item_id', $this->input->post('id'))->get()->row();
			if($item_stock) {
				$stocks['stocks'] = $item_stock->stock;
			} else {
				$stocks['stocks'] = 0;
			}
		}

		return $stocks;
	}

	public function save_buy_item(&$id, $param = false) {

		if(!$param) {
			$param['buy_item_id'] = $this->input->post('id');
			$param['registrant_id'] = $this->session->userdata('user_id');	
		}
		$this->db->insert('tbl_buys', $param);
		$id = $this->db->insert_id();

		$param = array();
		$properties = $this->input->post('prop');
		$values = $this->input->post('val'); 
		$stocks = array();
		
		if($properties) {
			foreach ($properties as $key => $value) {
				$param['buy_id'] = $id;
				$param['property'] = $value;
				$param['value'] = @$values[$key];
				$this->db->insert('tbl_buy_properties', $param);	
			}
			$stocks = $this->count_stock( @$values[0], @$values[1], $this->input->post('id'));
			if($stocks) {
				$param = array();
				$param['stocks'] = $stocks['stocks'] - 1;
				$this->db->where('property_stock_id', $stocks['property_stock_id']);
				$this->db->update('tbl_property_stocks', $param);
			}
		} else {
			$properties_db = $this->db->select('*')
									->from('tbl_properties')
									->where('buy_item_id', $this->input->post('id'))
									->get()
									->result_array();
			if($properties_db) {
				// $this->db->where('buy_id', $id);
				// $this->db->delete('tbl_buys');
			}
			// $stocks['stocks'] = 1;	

			$item_stock = $this->db->select()->from('tbl_buy_items')->where('buy_item_id', $this->input->post('id'))->get()->row();
			if($item_stock) {
				$stocks['stocks'] = $item_stock->stock;
			} else {
				$stocks['stocks'] = 0;
			}
		}
		return $stocks;
	}

	public function has_bought($item_id, $user_id = false) {
		if(!$user_id)
			$user_id = $this->session->userdata('user_id');
		$item = $this->db->select('*')
						 ->from('tbl_buys')
						 ->where('registrant_id', $user_id)
						 ->where('buy_item_id', $item_id)
						 ->get()
						 ->row();
		return $item;
	}

	public function deduct_stock($item_id, $stock) {
		$param['stock'] = $stock;
		if($stock == 0) {
			$param['status'] = 0;
		}
		$this->db->where('buy_item_id', $item_id);
		$this->db->update('tbl_buy_items', $param);
	}

	public function deduct_stock_db($item_id) {
		$item = $this->db->select('*')->from('tbl_buy_items')
			->where('buy_item_id', $item_id)
			->get()
			->row();

		$this->db->set('stock', 'stock-1', false);
		$this->db->where('buy_item_id', $item_id);
		$this->db->update('tbl_buy_items');
	}

	public function get_buy_details($id) {
		$buy = $this->db->select('*')
						->from('tbl_buys b')
						->join('tbl_buy_items i', 'b.buy_item_id = i.buy_item_id')
						->where('b.buy_id', $id)
						->get()
						->row();
		return (array) $buy;
	}

	public function get_bid_items() {
		$items = $this->db->select('*')
						  ->from('tbl_bid_items')
						  ->where('status', 1)
						  ->where('is_deleted', 0)
						  ->where('platform_restriction !=', MOBILE_ONLY)
						  ->order_by('created_date', 'DESC')
						  ->get()
						  ->result_array();
		return $items;
	}

	public function get_bid_item($id) {
		$items = $this->db->select('*')
						  ->from('tbl_bid_items')
						  ->where('bid_item_id', $id)
						  ->get()
						  ->row();
		if(date('YmdHis', strtotime($items->end_date)) <= date('YmdHis') && $items->has_ended == 0) {
			$this->db->where('bid_item_id', $id);
			$this->db->update('tbl_bid_items', array('has_ended' => 1));
			$items = $this->db->select('*')
						  ->from('tbl_bid_items')
						  ->where('bid_item_id', $id)
						  ->get()
						  ->row();
		} elseif(date('YmdHis', strtotime($items->end_date)) > date('YmdHis') && $items->has_ended == 1) {
			$this->db->where('bid_item_id', $id);
			$this->db->update('tbl_bid_items', array('has_ended' => 0));
			$items = $this->db->select('*')
						  ->from('tbl_bid_items')
						  ->where('bid_item_id', $id)
						  ->get()
						  ->row();
		}
		return $items;	
	}

	public function get_bid_item_media($id) {
		$media = $this->db->select('*')
						  ->from('tbl_media')
						  ->where('origin_id', PERKS_BID)
						  ->where('suborigin_id', $id)
						  ->get()
						  ->result_array();
		return $media;	
	}

	public function get_user_won_bid() {
		$registrant_id = $this->session->userdata('user_id');
		$query = "SELECT b.*, i.bid_item_name, r.first_name, r.third_name FROM tbl_bids b
				 JOIN tbl_registrants r ON b.registrant_id = r.registrant_id
				 JOIN tbl_bid_items i ON i.bid_item_id = b.bid_item_id
				 WHERE r.registrant_id = $registrant_id
				 AND r.total_points >= b.bid
				 AND b.is_winner = 1
				 AND b.is_confirmed = 0";
		$bids = $this->db->query($query)->result_array();
		return $bids;
	}

	public function get_user_confirmed_bid() {
		$registrant_id = $this->session->userdata('user_id');
		$query = "SELECT b.*, i.bid_item_name, r.first_name, r.third_name FROM tbl_bids b
				 JOIN tbl_registrants r ON b.registrant_id = r.registrant_id
				 JOIN tbl_bid_items i ON i.bid_item_id = b.bid_item_id
				 WHERE r.registrant_id = $registrant_id
				 AND r.total_points >= b.bid
				 AND b.is_winner = 1
				 AND b.is_confirmed = 1";
		$bids = $this->db->query($query)->result_array();
		return $bids;
	}

	public function confirm_bid() {
		$bids = $this->get_user_won_bid();
		$bid_ids = array();
		if($bids) {
			foreach ($bids as $key => $value) {
				$bid_ids[] = $value['bid_id'];
			}
		}

		$this->db->where_in('bid_id', $bid_ids);
		$this->db->update('tbl_bids', array('is_confirmed' => 1, 'confirm_date'	=> date('Y-m-d H:i:s')));
	}

	public function get_lamp($id) {
		return (array) $this->db->select()
			->from('tbl_lamps')
			->where('lamp_id', $id)
			->where('status', 1)
			->where('is_deleted', 0)
			->limit(1)
			->get()
			->row();
	}

	public function get_lamps() {
		return $this->db->select()
			->from('tbl_lamps')
			->where('status', 1)
			->where('is_deleted', 0)
			->order_by('created_date', 'DESC')
			->get()
			->result_array();
	}

	public function get_lamp_media($id) {
		return $this->db->select()
			->from('tbl_media')
			->where('origin_id', PERKS_RESERVE)
			->where('suborigin_id', $id)
			->order_by('media_date_created', 'DESC')
			->get()
			->result_array();
	}

	public function get_lamp_schedules($id) {
		list($today, $time) = explode(' ', date('Y-m-d H:i:s'));
		return $this->db->select()
			->from('tbl_schedules s')
			->join('tbl_lamps l', 'l.lamp_id = s.lamp_id')
			->where('s.lamp_id', $id)
			->where("(s.date > '{$today}' OR (s.date = '{$today}' AND '{$time}' < l.cut_off))")
			->where('s.date <= DATE_ADD("'.$today.'", INTERVAL '.PERKS_RESERVE_SCHEDULES_MONTH_LIMIT.' month)')
			->order_by('s.date')
			->get()
			->result_array();
	}

	public function get_lamp_schedule_by_id($id, $lamp_id = null) {
		$sql = $this->db->select()
			->from('tbl_schedules')
			->where('schedule_id', $id)
			->limit(1);

		if ($lamp_id !== null) {
			$sql->where('lamp_id', $lamp_id);
		}

		return (array) $sql->get()->row();
	}

	public function reserve($data, $pts = null) {
		$this->db->insert('tbl_lamp_reservations', $data);
		if (!$this->db->affected_rows()) {
			return false;
		}
		$id = $this->db->insert_id();
		if (!isset($this->Points_Model)) {
			$this->load->model('Points_Model');
		}
		$sched = array();
		if (!$pts) {
			$sched = (array) $this->db->select()
				->from('tbl_schedules')
				->where('schedule_id', $data['schedule_id'])
				->get()
				->row();
			if (!$sched) {
				return false;
			}
		} else {
			$sched['reservation_cost'] = $pts;
		}
		
		$result = $this->Points_Model->spend(PERKS_RESERVE, array(
			'suborigin' => $id,
			'registrant' => $this->session->userdata('user_id'),
			'points' => $sched['reservation_cost']
		));
		if (!$result) {
			return false;
		}
		return true;
	}

	public function get_bidder($id) {

		$query = "SELECT `b`.`bid`, `r`.`nick_name`, `r`.`registrant_id`, `r`.`total_points` 
				  FROM (`tbl_bids` b) 
				  LEFT JOIN `tbl_registrants` r ON `r`.`registrant_id` = `b`.`registrant_id` 
				  WHERE `bid_item_id` = $id
				  AND is_winner = 1
				  ORDER BY `bid` DESC";

		$bidder = $this->db->query($query)->row();
		if(!$bidder) {
			$query = "SELECT `b`.`bid`, `r`.`nick_name`, `r`.`registrant_id`, `r`.`total_points` 
				  FROM (`tbl_bids` b) 
				  LEFT JOIN `tbl_registrants` r ON `r`.`registrant_id` = `b`.`registrant_id` 
				  WHERE `bid_item_id` = $id
				  AND `r`.`total_points` >= b.bid
				  ORDER BY `bid` DESC";
			$bidder = $this->db->query($query)->row();
		}

		
		if(isset($bidder->registrant_id) && $bidder->registrant_id == $this->session->userdata('user_id'))
			$bidder->nick_name = 'You'; 
		if(isset($bidder->bid))
			$bidder->bid = $bidder->bid;
		return $bidder;	
	}

	public function save_bid($param = false) {
		if(!$param) {
			$param['bid_item_id'] = $this->input->post('id');
			$param['bid'] = $this->input->post('bid');
			$param['registrant_id'] = $this->session->userdata('user_id');
		}
		$this->db->insert('tbl_bids', $param);
	}

	public function get_perks_thumbs() {
		$perks = $this->db->select('*')
						  ->from('tbl_settings')
						  ->where_in('type', array('bid', 'buy', 'reserve'))
						  ->get()
						  ->result_array();
		$perks_arr = array();
		foreach ($perks as $key => $value) {
			$perks_arr[$value['type']] = $value;
		}
		return $perks_arr;
	}

	public function has_viewed_splash() {
		$has_session = true;
		if (!$this->session->userdata('perks_view')) {
			$has_session = false;
			$this->session->set_userdata('perks_view', 1);
		}
		$splash_count = $this->db->select('*')
								->from('tbl_settings')
								->where('type', 'perks_splash_count')
								->get()
								->row();

		$viewed = $this->db->select('*')
							->from('tbl_page_visits')
							->join('tbl_login', 'tbl_login.login_id = tbl_page_visits.login_id')
							->where('section', 'perks')
							->where('registrant_id', $this->session->userdata('user_id'))
							->get()
							->num_rows();
		return $viewed > (int) $splash_count->description || $has_session ? true : false;
	}

	public function get_mechanics($what = 'bid') {
		$mechanics = (array) $this->db->select('*')
							 ->from('tbl_settings')
							 ->where('type', $what . '_mechanics')
							 ->get()
							 ->row();
		return $mechanics;
	}

	public function check_bid_points($registrant_id) {
		$items = $this->get_bid_items();
		$reserved_points = 0;
		$item_names = array();
		if($items) {
			foreach ($items as $key => $value) {
				$bidder = $this->get_bidder($value['bid_item_id']);
				if(isset($bidder->registrant_id) && $bidder->registrant_id == $registrant_id && $value['has_ended'] == 0) {
					$reserved_points += $bidder->bid;
					$item_names[] = $value['bid_item_name'];
				} 
			}
		}

		$registrant = $this->db->select('*')
								->from('tbl_registrants')
								->where('registrant_id', $registrant_id)
								->get()
								->row();

		$data['reserved_points'] = $reserved_points;
		$data['available_points'] = $registrant->total_points - $reserved_points;
		$data['items'] = $item_names;
		
		return $data;
	}
}
