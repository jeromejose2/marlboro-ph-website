(function($, window, document){
  var helperMethods = {
    settings: {
      findContainer: $('.find-page'),
      splashContainer: $('.splash'),
      detailerContainer: $('.detailer-main'),
      loginPage: $('.login-layout')
    },
    viewPort: function (){
        var e = window;
        var a = 'inner';

        //For Windows Mobile Devices (http://quirksmode.org/mobile/viewports2.html)
        if (!('innerWidth' in window)){
            a = 'client';
            e = document.documentElement || document.body;
        }
        return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
    },
    easing: function (t) { return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1 }
  };

  var moduleGlobal = {
    settings: {
        mobileDropButton: $('#mobile-drop-trigger'),
        siteHeader: $('#header'),
        triggerTerms: $('#trigger-terms'),
        triggerPrivacy: $('#trigger-privacy'),
        triggerContact: $('#trigger-contact')
    },
    bindUIActions: function() {
      var parent = this;

      parent.settings.mobileDropButton.on('click', function(e){
        e.preventDefault();

        parent.settings.siteHeader.toggleClass('active');


      });

      parent.settings.triggerTerms.on('click', function(e){
        $.magnificPopup.open({

            items: {
              src: '#popup-terms'
            },
            type: 'inline',

            callbacks: {
              open: function() {
                console.log("Hello");
                $('#popup-terms .pop-contents').mCustomScrollbar();
              }
            }
          }, 0);

      });

      parent.settings.triggerPrivacy.on('click', function(e){

        $.magnificPopup.open({

            items: {
              src: '#popup-privacy'
            },
            type: 'inline',

            callbacks: {
              open: function() {
                 $('#popup-privacy .pop-contents').mCustomScrollbar();
              }
            }
          }, 0);

      });

      parent.settings.triggerContact.on('click', function(e){
        $.magnificPopup.open({

            items: {
              src: '#popup-contact'
            },
            type: 'inline',

            callbacks: {
              open: function() {
                $('#popup-contact .pop-contents').mCustomScrollbar();
              }
            }
          }, 0);

      });

    },
    init: function() {
      this.bindUIActions();
    }
  };

  var moduleLogin = {
    bindUIActions: function() {
      var parent = this;

      $(window).on('resize', function(e){
        parent.centerLoginContainer();
      });
    },
    init: function() {
      var parent = this,
          viewport = new helperMethods.viewPort();

      parent.bindUIActions();
      parent.centerLoginContainer();


    },
    centerLoginContainer: function() {
      var parent = this,
          viewport = new helperMethods.viewPort(),
          loginContainer = helperMethods.settings.loginPage.find('.wrapper');

      if(viewport.width > 1368) {

        $(loginContainer[0]).css({
          'top': '50%',
          'margin-top': (((loginContainer.outerHeight() / 2) + 60) * -1)
        });
      }

    }
  };

  var moduleHome = {
    settings: {
      animateSpinner: document.getElementById('animate-spinner'),
      animateSpinnerCtx: false,
      spinnerImages: {},
      midLoader: $('.mid-loader'),
      'asset_url': '/firm-filter/'
    },
    bindUIActions: function() {
      var parent = this;

      if(is_first_login == 0) { 
        /* Play popup video */
        parent.startVideo();
      }

      // Start downloading spinner iamge frames
      parent.loadImageFrames({
        splashCanvas: parent.settings.animateSpinner,
        directoryFile: 'spinner',
        imgGroup: parent.settings.spinnerImages,
        assignCtx: 'animateSpinnerCtx',
        start: 153,
        end: 153
      });

    },
    init: function() {
      var parent = this;

      parent.bindUIActions();
    },
    startVideo: function() {
      var parent = this;
      var popVideo = $('#popup-video video');
      var viewPort = new helperMethods.viewPort();

      // After the video ended playing
      popVideo.on('ended', function(e){
        var timeOut = 0;


          $.magnificPopup.close();

          parent.settings.midLoader.addClass('active');

          timeOut = setTimeout(function(){

            $.magnificPopup.open({
                items: {
                  src: '#popup-detailer'
                },
                type: 'inline',
                callbacks: {
                  open: function() {

                    parent.settings.midLoader.removeClass('active');
                    parent.startBenefits();

                  }
                }
            }, 0);

          }, 1000);

      });


      $.magnificPopup.open({

          items: {
            src: '#popup-video'
          },
          type: 'inline',

          callbacks: {
            open: function() {
              var timeOut = 0;

              $('#popup-video .pop-contents').mCustomScrollbar();


              $('.mfp-close').on('click', function(e){
                $.magnificPopup.close();


                parent.settings.midLoader.addClass('active');

                timeOut = setTimeout(function(){

                  $.magnificPopup.open({
                      items: {
                        src: '#popup-detailer'
                      },
                      type: 'inline',
                      callbacks: {
                        open: function() {

                          parent.settings.midLoader.removeClass('active');
                          parent.startBenefits();

                        }
                      }
                  }, 0);

                }, 1000);

                return false;
              });

              popVideo.get(0).play();
            },
            close: function() {
              // console.log('closed');
            }
          }
      }, 0);
    },
    startBenefits: function() {

      $(".benefits-slider").owlCarousel({
        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true,
        navigationText : [ '<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
        afterUpdate: function() {
            var containerHeight = $('.benefits-slider').outerHeight();

            $('.benefits-slider').find('.slide.final').css('height', containerHeight);
        },
        afterInit: function() {
            var containerHeight = $('.benefits-slider').outerHeight();

            $('.benefits-slider').find('.slide.final').css('height', containerHeight);
        },
        rewindNav: false
      });

      $('.benefits-close-pop').on('click', function(e){
        $.magnificPopup.close();
      });
    },
    loadImageFrames: function (config) {
      var parent = this,
          start = config.start ? config.start : 1,
          end = config.end ? config.end : 127,
          context = config.splashCanvas.getContext('2d'),
          viewport = new helperMethods.viewPort();

      // Canvas resizing on older browsers are not working (http://caniuse.com/#feat=canvas)


      for(var i = start; i <= end; i++) {
          var filename = parent.settings.asset_url+'img/' + config.directoryFile + '/' + i + '.png'; // Filename of each image
          var img = null;

          // console.log(filename);


          if(viewport.width <= 640) {
            img = new Image(441, 279);
          } else {
            img = new Image(441, 279);
          }
          img.src = filename;
          img.onload = parent.onIMGLoad({
                        current: i,
                        start: start,
                        max: end,
                        context: context,
                        assignContext: config.assignCtx,
                        pushedImage: img,
                        imgGroup: config.imgGroup
                      });
      }

    },
    onIMGLoad: function (obj) {
        // console.log(this.settings.productImages);


        function F() {
          console.log(obj.current);

            obj.imgGroup[obj.current] = obj.pushedImage;

            // If splash frames are all downloaded
            if (obj.current === obj.max) {

              moduleHome.settings[obj.assignContext] = obj.context;

              moduleHome.animateSpinner({
                ctx: moduleHome.settings.animateSpinnerCtx,
                imgGroup: moduleHome.settings.spinnerImages,
                start: 153,
                end: 154,
                duration: 100
              });
            }

        }

        return F;
    },
    setImage: function (newLocation, context, imgGroup, size) {
      var viewport = new helperMethods.viewPort(),
          canvasSize = size ? size : {
            default: {
              width: 441,
              height: 279
            },
            small: {
              width: 441,
              height: 279
            }
          };

        // drawImage takes 5 arguments: image, x, y, width, height
        if(imgGroup[newLocation]) {
            // console.log(imgGroup[newLocation].src);
            // console.log(context);

            if(viewport.width <= 640) {
              context.clearRect( 0, 0, canvasSize.small.width, canvasSize.small.height );
              context.drawImage(imgGroup[newLocation], 0, 0, canvasSize.small.width, canvasSize.small.height );
            } else {
              context.clearRect( 0, 0, canvasSize.default.width, canvasSize.default.height);
              context.drawImage(imgGroup[newLocation], 0, 0, canvasSize.default.width, canvasSize.default.height);
            }
        }
    },
    animateSpinner: function(config) {
      var parent = this,
          counter = config.start ? config.start : 0,
          intervalID = null,
          transitionSpeed = config.duration / (config.end - config.start),
          progress = 0,
          end = config.advanceDone ? config.advanceDone : config.end;


      intervalID = setInterval(function(){

        // console.log(counter);

        if(counter === end) {
          config.done ? config.done() : false;
          clearInterval(intervalID);
        } else {
          parent.setImage(counter, config.ctx, config.imgGroup, config.size)
          counter++;
        }

      }, transitionSpeed);

    }
  };

  moduleGlobal.init();

  if(helperMethods.settings.loginPage.length > 0) {
    moduleLogin.init();
  }


  if(helperMethods.settings.splashContainer.length > 0) {
    moduleHome.init();
  }

})(jQuery, window, document);
