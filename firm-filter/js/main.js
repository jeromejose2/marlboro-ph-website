(function($, window, document){
  var helperMethods = {
    settings: {
      findContainer: $('.find-page'),
      splashContainer: $('.splash'),
      detailerContainer: $('.detailer-main'),
      loginPage: $('.login-layout')
    },
    viewPort: function (){
        var e = window;
        var a = 'inner';

        //For Windows Mobile Devices (http://quirksmode.org/mobile/viewports2.html)
        if (!('innerWidth' in window)){
            a = 'client';
            e = document.documentElement || document.body;
        }
        return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
    },
    easing: function (t) { return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1 }
  };

  var moduleGlobal = {
    settings: {
        mobileDropButton: $('#mobile-drop-trigger'),
        siteHeader: $('#header'),
        triggerTerms: $('#trigger-terms'),
        triggerPrivacy: $('#trigger-privacy'),
        triggerContact: $('#trigger-contact')
    },
    bindUIActions: function() {
      var parent = this;

      parent.settings.mobileDropButton.on('click', function(e){
        e.preventDefault();
        console.log('hello');

        parent.settings.siteHeader.toggleClass('active');


      });

      parent.settings.triggerTerms.on('click', function(e){
        $.magnificPopup.open({

            items: {
              src: '#popup-terms'
            },
            type: 'inline',

            callbacks: {
              open: function() {
                console.log("Hello");
                $('#popup-terms .pop-contents').mCustomScrollbar();
              }
            }
          }, 0);

      });

      parent.settings.triggerPrivacy.on('click', function(e){
        $.magnificPopup.open({

            items: {
              src: '#popup-privacy'
            },
            type: 'inline',

            callbacks: {
              open: function() {
                 $('#popup-privacy .pop-contents').mCustomScrollbar();
              }
            }
          }, 0);

      });


      parent.settings.triggerContact.on('click', function(e){
        $.magnificPopup.open({

            items: {
              src: '#popup-contact'
            },
            type: 'inline',

            callbacks: {
              open: function() {
                $('#popup-contact .pop-contents').mCustomScrollbar();
              }
            }
          }, 0);

      });
    },
    init: function() {
      this.bindUIActions();
    }
  };

  var moduleLogin = {
    bindUIActions: function() {
      var parent = this;

      $(window).on('resize', function(e){
        parent.centerLoginContainer();
      });
    },
    init: function() {
      var parent = this,
          viewport = new helperMethods.viewPort();

      parent.bindUIActions();
      parent.centerLoginContainer();


    },
    centerLoginContainer: function() {
      var parent = this,
          viewport = new helperMethods.viewPort(),
          loginContainer = helperMethods.settings.loginPage.find('.wrapper');

      if(viewport.width > 1368) {

        $(loginContainer[0]).css({
          'top': '50%',
          'margin-top': (((loginContainer.outerHeight() / 2) + 60) * -1)
        });
      }

    }
  };

  moduleGlobal.init();

  if(helperMethods.settings.loginPage.length > 0) {
    moduleLogin.init();
  }

})(jQuery, window, document);
