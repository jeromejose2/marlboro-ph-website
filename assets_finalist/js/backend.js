var Marl = {
	interval : 3000,
	points : $("#points"),
	notifications : $("#notifs"),
	notificationsUrl : BASE_URL+"api/notifications",
	likes : $("#likes-value"),
	likeButton : $(".like-this-team-btn"),
	likeThisTeamUrl : BASE_URL+"api/finalists",
	commentUrl : BASE_URL+"finalists/addComment"
};

paginate('initial');
qwert(FD.GALLERY[0].file, 'image');

$(".cmmntbx").keypress(function() {
	$(this).removeClass('input-error-outline');
});

function getNotifications() {
	setInterval(function() {
		$.getJSON(Marl.notificationsUrl, function (response){
			if( ! response.error) {
				Marl.points.html(response.points);
				response.notifications ? Marl.notifications.css('color', '#F00') : Marl.notifications.css('color', '#000');
			}
		});
	}, Marl.interval);	
}

function enterComment() {
	var input = $(".comment-box-main");
	if( ! $.trim(input.val())) {
		input.addClass('input-error-outline').focus();
		return false;
	}
	$(".comment-box-main").attr('disabled', true);
	$(".comment-main-btn").html("<img src='"+BASE_URL+"assets_finalist/img/loader.gif'>").attr('onclick', 'javascript:void(0)');
	$.post(Marl.commentUrl, {
		"comment" : $.trim($(".comment-box-main").val()),
		"fdid" : FD.FDID,
		"type" : "comment"
	}, function(){
		$(".comment-box-main").attr('disabled', false).val('');
		$(".comment-main-btn").html("ENTER COMMENT").attr('onclick', 'enterComment()');

		// $("#view-gallery").modal('hide');
		$("#pledge-point-thankyou").modal('show');
	});
}

function enterReplyComment(comment_id, comment_origin_id) {
	var input = $(".comment-box-"+comment_id);
	if( ! $.trim(input.val())) {
		input.addClass('input-error-outline').focus();
		return false;
	}
	$(".comment-box-"+comment_id).attr('disabled', true);
	$(".reply-btn-"+comment_id).html("<img src='"+BASE_URL+"assets_finalist/img/loader.gif'>").attr('onclick', 'javascript:void(0)');
	$.post(Marl.commentUrl, {
		"comment" : $.trim($(".comment-box-"+comment_id).val()),
		"fdid" : FD.FDID,
		"comment_id" : comment_origin_id,
		"type" : "reply"
	}, function(){
		$(".comment-box-"+comment_id).attr('disabled', false).val('');
		$(".reply-btn-"+comment_id).html("ENTER COMMENT").attr('onclick', "enterReplyComment("+comment_id+", "+comment_origin_id+")");
		$(".rply-btn-"+comment_id).show();
		$(".reply-"+comment_id).hide();

		// $("#view-gallery").modal('hide');
		$("#pledge-point-thankyou").modal('show');
	});
}

function paginate(param) {

	switch(param) {
		case 'next':
			if(FD.PROGRESS_COUNT == MAX - 2) {
				$(".next-cont").removeClass("active");
			}
			if(FD.PROGRESS_COUNT == MAX - 1) {
				return;
			}
			$(".prev-cont").addClass("active");
			FD.OFFSET = FD.OFFSET + 4;
			FD.PROGRESS_COUNT = FD.PROGRESS_COUNT + 1;
			var DATA = {'items' : array_slice(FD.GALLERY, FD.OFFSET, 4)};
			var Template = $("#gallery").html();
			var Template = Handlebars.compile(Template);
			$(".thumb-listing").html(Template(DATA));
			break;
		case 'prev':
			if(FD.PROGRESS_COUNT == 1) {
				$(".prev-cont").removeClass("active");
			}
			if(FD.PROGRESS_COUNT <= 0) {
				return;
			}
			$(".next-cont").addClass("active");
			FD.OFFSET = FD.OFFSET - 4;
			FD.PROGRESS_COUNT = FD.PROGRESS_COUNT - 1;
			var DATA = {'items' : array_slice(FD.GALLERY, FD.OFFSET, 4)};
			var Template = $("#gallery").html();
			var Template = Handlebars.compile(Template);
			$(".thumb-listing").html(Template(DATA));
			break;
		default:
			var DATA = {'items' : array_slice(FD.GALLERY, FD.OFFSET, 4)};
			var Template = $("#gallery").html();
			var Template = Handlebars.compile(Template);
			$(".thumb-listing").html(Template(DATA));
			break;
	}
}

function qwert(image, type) {
	if(type == 'image') {
		$(".prev_gllry").css({"background-image": 'url('+image+')', "background-position": "center", "background-size": "contain", "background-repeat": "no-repeat"}).html("<img name='preview' src='"+image+"' alt='' style='opacity: 0;'>");
		$("img[name=preview]").attr("src", image);
	} else if(type == 'video') {
		$(".prev_gllry").css({"background-image": ""}).html("<video controls id='gallery-vid' style='width: 100%'><source src='"+image+"' type='video/mp4'></video>");
		$("#gallery-vid")[0].play();
		$("#view-gallery").on('hidden.bs.modal', function() {
			$("#gallery-vid")[0].pause();
		});
	} else {
		return false;
	}	

	$('.prev_gllry').css({'overflow':'visible'});
}

$(".like-this-team").on("click", function (response){
	$('#like-content').hide();
	$('#like-preloader').show();
	$("#like-this-team").modal("show");
	$.post(Marl.likeThisTeamUrl, {fdid : FD.FDID}, function (response){ 
		response.resp == "up" ? Marl.likeButton.html("LIKE THIS TEAM").css('background-color', '#AAAAAA') : Marl.likeButton.html("LIKE THIS TEAM").css('background-color', '#dd001f');
		Marl.likes.html(response.likes);
		// if(response.resp == "up") {
		// 	$("#like-this-team").modal("show");
		// 	$('#like-content').show();
		// 	$('#like-preloader').hide();
		// }
		if(response.resp == "up") {
			// $("#like-this-team").modal("show");
			$('#like-content').show();
			$('#like-preloader').hide();
		} else if(response.resp == "down") {
			$("#like-this-team").modal("hide");
		}
	});
});

function array_slice(arr, offst, lgth, preserve_keys) {

  var key = '';

  if (Object.prototype.toString.call(arr) !== '[object Array]' ||
    (preserve_keys && offst !== 0)) {
    var lgt = 0,
      newAssoc = {};
    for (key in arr) {
      lgt += 1;
      newAssoc[key] = arr[key];
    }
    arr = newAssoc;

    offst = (offst < 0) ? lgt + offst : offst;
    lgth = lgth === undefined ? lgt : (lgth < 0) ? lgt + lgth - offst : lgth;

    var assoc = {};
    var start = false,
      it = -1,
      arrlgth = 0,
      no_pk_idx = 0;
    for (key in arr) {
      ++it;
      if (arrlgth >= lgth) {
        break;
      }
      if (it == offst) {
        start = true;
      }
      if (!start) {
        continue;
      }++arrlgth;
      if (this.is_int(key) && !preserve_keys) {
        assoc[no_pk_idx++] = arr[key];
      } else {
        assoc[key] = arr[key];
      }
    }
    return assoc;
  }

  if (lgth === undefined) {
    return arr.slice(offst);
  } else if (lgth >= 0) {
    return arr.slice(offst, offst + lgth);
  } else {
    return arr.slice(offst, lgth);
  }
}