(function($, window, document, _){
  var helperMethods = {
    settings: {
      findContainer: $('.find-page'),
      splashContainer: $('.splash'),
      detailerContainer: $('.detailer-main')
    },
    viewPort: function (){
        var e = window;
        var a = 'inner';

        //For Windows Mobile Devices (http://quirksmode.org/mobile/viewports2.html)
        if (!('innerWidth' in window)){
            a = 'client';
            e = document.documentElement || document.body;
        }
        return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
    }
  };

  var moduleGlobal = {
    settings: {
        mobileDropButton: $('#mobile-drop-trigger'),
        siteHeader: $('#header'),
        triggerTerms: $('#trigger-terms'),
        triggerPrivacy: $('#trigger-privacy'),
        triggerContact: $('#trigger-contact')
    },
    bindUIActions: function() {
      var parent = this;

      parent.settings.mobileDropButton.on('click', function(e){
        e.preventDefault();
        console.log('hello');

        parent.settings.siteHeader.toggleClass('active');


      });

      parent.settings.triggerTerms.on('click', function(e){
        $.magnificPopup.open({

            items: {
              src: '#popup-terms'
            },
            type: 'inline',

            callbacks: {
              open: function() {
                console.log("Hello");
                $('#popup-terms .pop-contents').mCustomScrollbar();
              }
            }
          }, 0);

      });

      parent.settings.triggerPrivacy.on('click', function(e){
        $.magnificPopup.open({

            items: {
              src: '#popup-privacy'
            },
            type: 'inline',

            callbacks: {
              open: function() {
                 $('#popup-privacy .pop-contents').mCustomScrollbar();
              }
            }
          }, 0);

      });


      parent.settings.triggerContact.on('click', function(e){
        $.magnificPopup.open({

            items: {
              src: '#popup-contact'
            },
            type: 'inline',

            callbacks: {
              open: function() {
                $('#popup-contact .pop-contents').mCustomScrollbar();
              }
            }
          }, 0);

      });
    },
    init: function() {
      this.bindUIActions();
    }
  };

  var moduleFind = {
        settings: {
          branchGroups: $('.branch-groups'),
          previousActiveBranchObj: false
        },
        bindUIActions: function() {
          var parent = this;

          parent.settings.branchGroups.on('click', '.branch', function(e) {
            var $this = $(this);

            console.log(this);



            if(!parent.settings.previousActiveBranchObj) {
              parent.settings.previousActiveBranchObj = $this;
              $this.addClass('active');
            } else {

              // When closing the same button, return the previousActiveBranchObj to default
              if($this.attr('data-branch-id') === parent.settings.previousActiveBranchObj.attr('data-branch-id')) {
                $this.removeClass('active');
                parent.settings.previousActiveBranchObj = false;
              } else {
                parent.settings.previousActiveBranchObj.removeClass('active');
                $this.addClass('active');
                parent.settings.previousActiveBranchObj = $this;
              }

            }



          });

          parent.settings.branchGroups.on('mouseleave', '.branch', function(e) {
            var $this = $(this);

            if(!$this.hasClass('active')) {

              if($this.hasClass('purple')) {
                $this.removeClass('purple');
                $this.addClass('green');
              } else {
                $this.removeClass('green');
                $this.addClass('purple');
              }

            }


          });
        },
        init: function() {
          this.bindUIActions();
        }
      };


  var moduleSplash = {
        settings: {
          splashContainer: $('.splash'),
          maskContainer: $('.mask'),
          splashLoader: $('#splash-loader'),
          splashLoaderInner: $('#chev-inner'),
          splashLoader: $('#main-stroke'),
          animateProduct: document.getElementById('animate-product'),
          animateProductCtx: false,
          logoContainer: $('.container-featured-logo'),
          productImages: {}
        },
        bindUIActions: function() {
          var parent = this;
          $(window).on('resize', function(e){
            parent.setupParentContainer();
          });
        },
        init: function() {

          this.loadImageFrames();
          this.setupParentContainer();

          this.bindUIActions();
          this.marlLoaderAnimation();
        },
        marlLoaderAnimation: function() {
          var parent = this,
              viewPort = new helperMethods.viewPort()
              afterLoadSequence = [];

          parent.settings.maskContainer.velocity({
            p: {
              background: 'transparent',
              opacity: 1,
              display: 'block'
            },
            o: {
              duration: 0
            }
          });

          /* Start Borderline Loading */
          this.settings.splashLoader.velocity({
            p: {
              strokeDashoffset: 745 + 745
            },
            o: {
              duration: 2000,
              easing: 'linear',
              complete: function() {

                // When loading of frames are ready
                if(parent.settings.animateProductCtx) {

                  parent.settings.splashLoaderInner.velocity({
                    p: {
                      opacity: 0
                    },
                    o: {
                      duration: 500,
                      complete: function() {


                        parent.settings.splashContainer
                          .transition({
                            scale: 1,
                            duration: 1000
                          });

                        parent.settings.maskContainer.velocity({
                          p: {
                            opacity: 0
                          },
                          o: {
                            duration: 1,
                            display: 'none',
                            delay: 200,
                            duration: 800,
                            complete: function() {
                              // Start product animation
                              // parent.animateProductCanvas();
                              parent.playImages(parent.settings.animateProductCtx, parent.settings.productImages)
                            }
                          }
                        });
                      }
                    }
                  });

                } else {
                  // While frames are not completed, return this on loader
                  parent.settings.splashLoader.velocity({
                    p: {
                      strokeDashoffset: 745
                    },
                    o: {
                      duration: 0
                    }
                  });

                  parent.marlLoaderAnimation();
                }





              }
            }
          });



        },
        animateProductCanvas: function() {

        },
        setupParentContainer: function() {
          /* Resize the Container On Page Load */
          var container = this.settings.splashContainer.find('.container'),
              viewPort = new helperMethods.viewPort();

          console.log(viewPort);
          console.log(this.settings.splashContainer.width());
          console.log(container.outerWidth());

          if(viewPort.width < 1025) {
            container.outerWidth(viewPort.width);

            this.settings.splashContainer.css({
              left: -((this.settings.splashContainer.width() - container.outerWidth()) / 2)
            });

          }

        },
        loadImageFrames: function () {
          var parent = this,
              start = 0,
              end = 181,
              context = parent.settings.animateProduct.getContext('2d'),
              viewport = new helperMethods.viewPort();

          // Canvas resizing on older browsers are not working (http://caniuse.com/#feat=canvas)

          if(viewport.width <= 640) {
            $(parent.settings.animateProduct).
              attr('width', 123)
              .attr('height', 196);
          } else {
            $(parent.settings.animateProduct).
              attr('width', 280)
              .attr('height', 395);
          }

          for(var i = start; i <= end; i++) {
              var j = i < 10 ? '000' + i :  i >= 10 && i < 100 ? '00' + i : '0' + i;
              var filename = 'img/product/Box-Rotate' + j + '.png'; // Filename of each image
              var img = null;

              if(viewport.width <= 640) {
                img = new Image(123, 196);
              } else {
                img = new Image(280, 395);
              }
              img.src = filename;
              img.onload = this.onIMGLoad({
                            current: i,
                            start: start,
                            max: end,
                            context: context,
                            pushedImage: img
                          });

          }
        },
        onIMGLoad: function (obj) {
            // console.log(this.settings.productImages);


            function F() {
              // console.log(obj.current);

                moduleSplash.settings.productImages[obj.current] = obj.pushedImage;

                // If products are all downloaded
                if (obj.current === obj.max) {

                  // moduleSplash.playImages(obj.context, moduleSplash.settings.productImages);
                  moduleSplash.settings.animateProductCtx = obj.context;
                }

            }

            return F;
        },
        setImage: function (newLocation,context,imgGroup) {
          var viewport = new helperMethods.viewPort();

            // drawImage takes 5 arguments: image, x, y, width, height
            if(imgGroup[newLocation]) {
                // console.log(imgGroup[newLocation].src);
                // console.log(context);

                if(viewport.width <= 640) {
                  context.clearRect( 0, 0, 123, 196 );
                  context.drawImage(imgGroup[newLocation], 0, 0, 123, 196);
                } else {
                  context.clearRect( 0, 0, 280, 395 );
                  context.drawImage(imgGroup[newLocation], 0, 0, 280, 395);
                }
            }
        },
        playImages: function(ctx, imgGroup) {
          var parent = this,
              counter = 0,
              intervalID = null;

          intervalID = setInterval(function(){
            if(counter === 182) {
              console.log('done');
              clearInterval(intervalID);
              parent.showLogoContainer();
            } else {
              parent.setImage(counter, ctx, imgGroup)
              counter++;
            }
          }, 20);

        },
        showLogoContainer: function() {
          var parent = this,
              sequence = [];

          sequence = [
            {
              e: parent.settings.logoContainer,
              p: {
                opacity: 0
              },
              o: {
                visibility: 'hidden'
              }
            },
            {
              e: parent.settings.logoContainer,
              p: {
                opacity: 1
              },
              o: {
                visibility: 'visible'
              }
            }
          ];

          $.Velocity.RunSequence(sequence);

        }
      };


  var moduleDetailer = {
    settings: {
      containerCig: $('.container-cig'),
      containerDescription: $('.container-description'),
      detailerContainer: $('.detailer-main'),
      firstSlideContainer: $('.first-slide'),
      logoContainer: $('.container-featured-logo'),
      secondSlideContainer: $('.second-slide'),
      firstSlideText: $('.first-description'),
      secondSlideText: $('.second-description'),
      thirdSlideText: $('.third-description'),
      fourthSlideText: $('.fourth-description'),
      loaderContainer: $('.loader'),
      splashLoader: $('#main-stroke'),
      animatePurpleSplash: document.getElementById('purple-splash'),
      animatePurpleSplashCtx: false,
      animateGreenSplash: document.getElementById('green-splash'),
      animateGreenSplashCtx: false,
      purpleImages: {},
      greenImages: {},
      slideNavigation: $('.slide-navigation'),
      currentStage: 1
    },
    bindUIActions: function() {
      var parent = this;
      $(window).on('resize', function(e){
        parent.setupParentContainer();
      });

      // Event listener for sequence navigation
      $(parent.settings.slideNavigation).on('click', 'button', function(e){
        var $this = $(this),
            container = parent.settings.slideNavigation;

        if($this.attr('data-action') == 'next') {

          container.attr('data-stage', ++parent.settings.currentStage);
          parent.sequenceController('next');

        } else if ($this.attr('data-action') == 'prev') {

          container.attr('data-stage', --parent.settings.currentStage);
          parent.sequenceController('prev');

        }

      });

    },
    init: function() {
      var viewport = new helperMethods.viewPort(),
          parent = this;


      if(viewport.width <= 640) {
        $(parent.settings.animatePurpleSplash).
          attr('width', 230)
          .attr('height', 298);

        $(parent.settings.animateGreenSplash).
          attr('width', 230)
          .attr('height', 298);
      } else {
        $(parent.settings.animatePurpleSplash).
          attr('width', 230)
          .attr('height', 298);

        $(parent.settings.animateGreenSplash).
          attr('width', 230)
          .attr('height', 298);
      }

      // Load Images ASAP
      parent.loadImageFrames(parent.settings.animatePurpleSplash, 'splash-purple/purple', parent.settings.purpleImages, 'animatePurpleSplashCtx');

      parent.loadImageFrames(parent.settings.animateGreenSplash, 'splash-green/green', parent.settings.greenImages, 'animateGreenSplashCtx');



      parent.bindUIActions();
      parent.setupParentContainer();

      // Start sequence controller
      parent.sequenceController();
    },
    showLoaderAnimation: function(execute, parentSettingsCtx) {
      var parent = this;



      parent.settings.loaderContainer.velocity('transition.fadeIn');

      parent.settings.splashLoader.velocity({
        p: {
          strokeDashoffset: 745
        },
        o: {
          duration: 0
        }
      });

      parent.marlLoaderAnimation(execute, parentSettingsCtx);
    },
    marlLoaderAnimation: function(execute, parentSettingsCtx) {
      var parent = this;

      console.log(parent.settings.animatePurpleSplashCtx);

      /* Start Borderline Loading */
      this.settings.splashLoader.velocity({
        p: {
          strokeDashoffset: 745 + 745
        },
        o: {
          duration: 2000,
          easing: 'linear',
          complete: function() {
            // When loading of splash frames are ready
            if(parent.settings[parentSettingsCtx]) {
              parent.settings.loaderContainer.velocity('transition.fadeOut');
              execute();
            } else {
              // While frames are not completed, return this on loader
              parent.settings.splashLoader.velocity({
                p: {
                  strokeDashoffset: 745
                },
                o: {
                  duration: 0
                }
              });

              parent.marlLoaderAnimation(execute, parentSettingsCtx);
            }

          }
        }
      });

    },
    sequenceController: function(action) {
      var parent = this;

      if(parent.settings.currentStage == 2 && action == 'next') {
        parent.settings.slideNavigation
          .find('.prev')
          .addClass('active');
      } else if (parent.settings.currentStage == 1 && action == 'prev') {
        parent.settings.slideNavigation
          .find('.prev')
          .removeClass('active');
      }


      if(parent.settings.currentStage == 5 && action == 'next') {
        parent.settings.slideNavigation
          .find('.next')
          .removeClass('active');
      } else if (parent.settings.currentStage == 4 && action == 'prev') {
        parent.settings.slideNavigation
          .find('.next')
          .addClass('active');
      }


      if(parent.settings.currentStage == 1 && !action) {
        // Autoplay first sequence
        parent.firstSequence();
      } else if(parent.settings.currentStage == 2 && action == 'next') {
        parent.secondSequence();
      } else if(parent.settings.currentStage == 3 && action == 'next') {
        parent.thirdSequence();
      } else if(parent.settings.currentStage == 4 && action == 'next') {
        parent.fourthSequence();
      } else if(parent.settings.currentStage == 5 && action == 'next') {
        parent.fifthSequence();
      }



      if(parent.settings.currentStage == 1 && action == 'prev') {
        parent.firstSequenceBack();
      } else if(parent.settings.currentStage == 2 && action == 'prev') {
        parent.secondSequenceBack();
      } else if(parent.settings.currentStage == 3 && action == 'prev') {
        parent.thirdSequenceBack();
      } else if(parent.settings.currentStage == 4 && action == 'prev') {
        parent.fourthSequenceBack();
      }

    },
    firstSequence: function() {
      var parent = this,
          sequence = [],
          viewPort = new helperMethods.viewPort();


      if(viewPort.width >= 1026) {

        sequence = [
          { //Setup Initial Position
            e: this.settings.containerCig,
            p: {
              translateX: '474px',
              translateY: '322px'
            },
            o: {
              duration: 0
            }
          },
          {
            // Animate cigarette to final position
            e: this.settings.containerCig,
            p: {
              translateX: '300px',
              translateY: '0px'
            },
            o: {
              duration: 1000
            }
          },
          { //Hide Text First
            e: this.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Hide Text First
            e: this.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Show Text
            e: this.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block'
            }
          },
          { //Show Text
            e: this.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block'
            }
          }

        ];

      }

      if(viewPort.width <= 1025) {

        sequence = [
          { //Setup Initial Position
            e: this.settings.containerCig,
            p: {
              translateX: '178px',
              translateY: '318px',
              rotateZ: '8deg'
            },
            o: {
              duration: 0
            }
          },
          {
            // Animate cigarette to final position
            e: this.settings.containerCig,
            p: {
              translateX: '87px',
              translateY: '151px',
              rotateZ: '8deg'
            },
            o: {
              duration: 1000
            }
          },
          { //Hide Text First
            e: this.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Hide Text First
            e: this.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Show Text
            e: this.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block'
            }
          },
          { //Show Text
            e: this.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block'
            }
          },

        ];
      }


      if(viewPort.width <= 640) {

        sequence = [
          { //Setup Initial Position
            e: this.settings.containerCig,
            p: {
              translateX: '170px',
              translateY: '500px',
              rotateZ: '1deg',
            },
            o: {
              duration: 0
            }
          },
          {
            // Animate cigarette to final position
            e: this.settings.containerCig,
            p: {
              translateX: '15px',
              translateY: '151px',
              rotateZ: '1deg'
            },
            o: {
              duration: 1000
            }
          },
          { //Hide Text First
            e: this.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Hide Text First
            e: this.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Show Text
            e: this.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block'
            }
          },
          { //Show Text
            e: this.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block'
            }
          }

        ];
      }


      if(viewPort.width <= 480) {

        sequence = [
          { //Setup Initial Position
            e: this.settings.containerCig,
            p: {
              translateX: '0px',
              translateY: '324px',
              rotateZ: '7deg',
              scale: .6
            },
            o: {
              duration: 0
            }
          },
          {
            // Animate cigarette to final position
            e: this.settings.containerCig,
            p: {
              translateX: '-69px',
              translateY: '112px',
              rotateZ: '7deg',
              scale: .6
            },
            o: {
              duration: 1000
            }
          },
          { //Hide Text First
            e: this.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Hide Text First
            e: this.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Show Text
            e: this.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block'
            }
          },
          { //Show Text
            e: this.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block'
            }
          },

        ];
      }

      if(viewPort.width <= 320) {

        sequence = [
          { //Setup Initial Position
            e: this.settings.containerCig,
            p: {
              translateX: '10px',
              translateY: '304px',
              rotateZ: '0deg',
              scale: .6
            },
            o: {
              duration: 0
            }
          },
          {
            // Animate cigarette to final position
            e: this.settings.containerCig,
            p: {
              translateX: '-78px',
              translateY: '136px',
              rotateZ: '0deg',
              scale: .6
            },
            o: {
              duration: 1000
            }
          },
          { //Hide Text First
            e: this.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Hide Text First
            e: this.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Show Text
            e: this.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block'
            }
          },
          { //Show Text
            e: this.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block'
            }
          },

        ];
      }


      $.Velocity.RunSequence(sequence);
    },
    secondSequence: function() {
      var parent = this,
          sequence = [];


      sequence = [
        { //Hide first slide text
          e: parent.settings.firstSlideText,
          p: {
            opacity: '0',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'none'
          }
        },
        { //Hide Text First
          e: parent.settings.secondSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.secondSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        {
          e: parent.settings.secondSlideText,
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.secondSlideText.find('.first-line'),
          p: {
            opacity: '1',
            translateX: '0px',
            begin: function() {
              moduleDetailer.animateSplash(moduleDetailer.settings.animatePurpleSplashCtx, moduleDetailer.settings.purpleImages, 1, 84, false, 2000);
            }
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.secondSlideText.find('.second-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        }

      ];


      parent.showLoaderAnimation(function(){
        $.Velocity.RunSequence(sequence);
      }, 'animatePurpleSplashCtx');

    },
    thirdSequence: function() {
      var parent = this,
          sequence = [];


      sequence = [
        { //Hide first slide text
          e: parent.settings.secondSlideText,
          p: {
            opacity: '0',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'none',
            begin: function() {
              console.log("test");
              moduleDetailer.animateSplash(moduleDetailer.settings.animatePurpleSplashCtx, moduleDetailer.settings.purpleImages, 84, 128, false, 2000);
            }
          }
        },
        { //Hide Text First
          e: parent.settings.thirdSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.thirdSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        {
          e: parent.settings.thirdSlideText,
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.thirdSlideText.find('.first-line'),
          p: {
            opacity: '1',
            translateX: '0px',
            begin: function() {
              moduleDetailer.animateSplash(moduleDetailer.settings.animateGreenSplashCtx, moduleDetailer.settings.greenImages, 1, 83, false, 2000);
            }
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.thirdSlideText.find('.second-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        }

      ];


      parent.showLoaderAnimation(function(){
        $.Velocity.RunSequence(sequence);
      }, 'animateGreenSplashCtx');

    },
    fourthSequence: function() {
      var parent = this,
          sequence = [];


      sequence = [
        { //Hide first slide text
          e: parent.settings.thirdSlideText,
          p: {
            opacity: '0',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'none',
            begin: function() {
              moduleDetailer.animateSplash(moduleDetailer.settings.animateGreenSplashCtx, moduleDetailer.settings.greenImages, 84, 127, false, 2000);
            }
          }
        },
        { //Hide Text First
          e: parent.settings.fourthSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.fourthSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.fourthSlideText.find('.third-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        {
          e: parent.settings.fourthSlideText,
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.fourthSlideText.find('.first-line'),
          p: {
            opacity: '1',
            translateX: '0px',
            begin: function() {

              moduleDetailer.animateSplash(moduleDetailer.settings.animatePurpleSplashCtx, moduleDetailer.settings.purpleImages, 1, 84, false, 2000);

              moduleDetailer.animateSplash(moduleDetailer.settings.animateGreenSplashCtx, moduleDetailer.settings.greenImages, 1, 84, false, 2000);
            }
          },
          o: {
            duration: 1000,
            delay: 500,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.fourthSlideText.find('.second-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.fourthSlideText.find('.third-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        }

      ];

      $.Velocity.RunSequence(sequence);

    },
    fifthSequence: function() {
      var parent = this,
          sequence = [];


      sequence = [
        { //Hide first slide text
          e: parent.settings.firstSlideContainer,
          p: {
            opacity: '0'
          },
          o: {
            duration: 1000,
            display: 'none'
          }
        },
        { //Hide Div first
          e: parent.settings.secondSlideContainer,
          p: {
            opacity: '0'
          },
          o: {
            duration: 0
          }
        },
        { //Show Text
          e: parent.settings.secondSlideContainer,
          p: {
            opacity: '1'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        {
          e: parent.settings.logoContainer,
          p: {
            opacity: 0
          },
          o: {
            visibility: 'hidden'
          }
        },
        {
          e: parent.settings.logoContainer,
          p: {
            opacity: 1
          },
          o: {
            visibility: 'visible',
            begin: function() {

              moduleDetailer.animateSplash(moduleDetailer.settings.animatePurpleSplashCtx, moduleDetailer.settings.purpleImages, 126, 128, false, 2000);

              moduleDetailer.animateSplash(moduleDetailer.settings.animateGreenSplashCtx, moduleDetailer.settings.greenImages, 126, 128, false, 2000);
            }
          }
        }

      ];

      $.Velocity.RunSequence(sequence);

    },
    firstSequenceBack: function() {
      var parent = this,
          sequence = [],
          viewPort = new helperMethods.viewPort();



      sequence = [
        { //Hide first slide text
          e: parent.settings.secondSlideText,
          p: {
            opacity: '0',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'none',
            begin: function() {
              console.log("test");
              moduleDetailer.animateSplash(moduleDetailer.settings.animatePurpleSplashCtx, moduleDetailer.settings.purpleImages, 84, 128, false, 2000);
            }
          }
        },
        { //Hide Text First
          e: parent.settings.secondSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.secondSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.firstSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.firstSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Show first slide text
          e: parent.settings.firstSlideText,
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        { //Show Text
          e: this.settings.firstSlideText.find('.first-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        { //Show Text
          e: this.settings.firstSlideText.find('.second-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        }
      ];

      $.Velocity.RunSequence(sequence);
    },
    secondSequenceBack: function() {
      var parent = this,
          sequence = [];


      sequence = [
        {
          e: parent.settings.thirdSlideText,
          p: {
            opacity: '0',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'none',
            begin: function() {
              moduleDetailer.animateSplash(moduleDetailer.settings.animateGreenSplashCtx, moduleDetailer.settings.greenImages, 84, 127, false, 2000);
            }
          }
        },
        { //Hide Text First
          e: parent.settings.thirdSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.thirdSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.secondSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.secondSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        {
          e: parent.settings.secondSlideText,
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.secondSlideText.find('.first-line'),
          p: {
            opacity: '1',
            translateX: '0px',
            begin: function() {
              moduleDetailer.animateSplash(moduleDetailer.settings.animatePurpleSplashCtx, moduleDetailer.settings.purpleImages, 1, 84, false, 2000);
            }
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.secondSlideText.find('.second-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        }

      ];


      $.Velocity.RunSequence(sequence);
    },
    thirdSequenceBack: function() {
      var parent = this,
          sequence = [];


      sequence = [
        {
          e: parent.settings.fourthSlideText,
          p: {
            opacity: '0',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'none',
            begin: function() {
              moduleDetailer.animateSplash(moduleDetailer.settings.animatePurpleSplashCtx, moduleDetailer.settings.purpleImages, 84, 128, false, 2000);

              moduleDetailer.animateSplash(moduleDetailer.settings.animateGreenSplashCtx, moduleDetailer.settings.greenImages, 84, 128, false, 2000);
            }
          }
        },
        { //Hide Text First
          e: parent.settings.fourthSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.fourthSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.fourthSlideText.find('.third-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.thirdSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.thirdSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        {
          e: parent.settings.thirdSlideText,
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.thirdSlideText.find('.first-line'),
          p: {
            opacity: '1',
            translateX: '0px',
            begin: function() {
              moduleDetailer.animateSplash(moduleDetailer.settings.animateGreenSplashCtx, moduleDetailer.settings.greenImages, 1, 84, false, 2000);
            }
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.thirdSlideText.find('.second-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        }

      ];


      $.Velocity.RunSequence(sequence);
    },
    fourthSequenceBack: function() {
      var parent = this,
          sequence = [];

      sequence = [
        { //Hide Div first
          e: parent.settings.secondSlideContainer,
          p: {
            opacity: '0'
          },
          o: {
            duration: 1000
          }
        },
        {
          e: parent.settings.fourthSlideText,
          p: {
            opacity: '0',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'none'
          }
        },
        { //Hide first slide text
          e: parent.settings.firstSlideContainer,
          p: {
            opacity: '1'
          },
          o: {
            duration: 500,
            display: 'block'
          }
        },
        {
          e: parent.settings.logoContainer,
          p: {
            opacity: 0
          },
          o: {
            visibility: 'hidden'
          }
        },
        { //Hide Text First
          e: parent.settings.fourthSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.fourthSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.fourthSlideText.find('.third-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        {
          e: parent.settings.fourthSlideText,
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.fourthSlideText.find('.first-line'),
          p: {
            opacity: '1',
            translateX: '0px',
            begin: function() {

              moduleDetailer.animateSplash(moduleDetailer.settings.animatePurpleSplashCtx, moduleDetailer.settings.purpleImages, 1, 84, false, 2000);

              moduleDetailer.animateSplash(moduleDetailer.settings.animateGreenSplashCtx, moduleDetailer.settings.greenImages, 1, 84, false, 2000);
            }
          },
          o: {
            duration: 1000,
            delay: 500,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.fourthSlideText.find('.second-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.fourthSlideText.find('.third-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        }

      ];

      $.Velocity.RunSequence(sequence);
    },

    setupParentContainer: function() {
      /* Resize the Container On Page Load */
      var container = this.settings.detailerContainer.find('.container'),
          viewPort = new helperMethods.viewPort();

      console.log(viewPort);
      console.log(this.settings.detailerContainer.width());
      console.log(container.outerWidth());

      if(viewPort.width < 1025) {
        container.outerWidth(viewPort.width);

        this.settings.detailerContainer.css({
          left: -((this.settings.detailerContainer.width() - container.outerWidth()) / 2)
        });

      }

    },
    loadImageFrames: function (splashCanvas, directoryFile, imgGroup, assignCtx) {
      var parent = this,
          start = 1,
          end = 127,
          context = splashCanvas.getContext('2d'),
          viewport = new helperMethods.viewPort();

      // Canvas resizing on older browsers are not working (http://caniuse.com/#feat=canvas)


      for(var i = start; i <= end; i++) {
          var filename = 'img/' + directoryFile + '_' + i + '.png'; // Filename of each image
          var img = null;

          if(viewport.width <= 640) {
            img = new Image(230, 298);
          } else {
            img = new Image(230, 298);
          }
          img.src = filename;
          img.onload = this.onIMGLoad({
                        current: i,
                        start: start,
                        max: end,
                        context: context,
                        assignContext: assignCtx,
                        pushedImage: img,
                        imgGroup: imgGroup
                      });

      }
    },
    onIMGLoad: function (obj) {
        // console.log(this.settings.productImages);


        function F() {
          // console.log(obj.current);

            obj.imgGroup[obj.current] = obj.pushedImage;

            // If splash frames are all downloaded
            if (obj.current === obj.max) {

              moduleDetailer.settings[obj.assignContext] = obj.context;

            }

        }

        return F;
    },
    setImage: function (newLocation, context, imgGroup) {
      var viewport = new helperMethods.viewPort();

        // drawImage takes 5 arguments: image, x, y, width, height
        if(imgGroup[newLocation]) {
            console.log(imgGroup[newLocation].src);
            // console.log(context);

            if(viewport.width <= 640) {
              context.clearRect( 0, 0, 230, 298 );
              context.drawImage(imgGroup[newLocation], 0, 0, 230, 298);
            } else {
              context.clearRect( 0, 0, 230, 298 );
              context.drawImage(imgGroup[newLocation], 0, 0, 230, 298);
            }
        }
    },
    animateSplash: function(ctx, imgGroup, start, end, done, duration) {
      var parent = this,
          counter = start ? start : 0,
          intervalID = null,
          transitionSpeed = duration / (end - start);

      intervalID = setInterval(function(){

        if(counter === end) {
          done ? done() : false;
          clearInterval(intervalID);
        } else {
          parent.setImage(counter, ctx, imgGroup)
          counter++;
        }
      }, transitionSpeed);

    }
  }



  if(helperMethods.settings.findContainer.length > 0) {
    console.log("Find Page");
    moduleFind.init();
  }

  if(helperMethods.settings.splashContainer.length > 0) {
    console.log("Splash Page")
    moduleSplash.init();
  }

  if(helperMethods.settings.detailerContainer.length > 0) {
    console.log("Detailer Page");
    moduleDetailer.init();
  }

  moduleGlobal.init();
})(jQuery, window, document, window._);
