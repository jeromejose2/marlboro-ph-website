(function($, window, document, _){
  var helperMethods = {
    settings: {
      findContainer: $('.find-page'),
      splashContainer: $('.splash'),
      detailerContainer: $('.detailer-main')
    },
    viewPort: function (){
        var e = window;
        var a = 'inner';

        //For Windows Mobile Devices (http://quirksmode.org/mobile/viewports2.html)
        if (!('innerWidth' in window)){
            a = 'client';
            e = document.documentElement || document.body;
        }
        return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
    },
    easing: function (t) { return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1 }
  };


  var asset_url = "//"+window.location.host+'/rubyburst2/';

  var moduleGlobal = {
    settings: {
        mobileDropButton: $('#mobile-drop-trigger'),
        siteHeader: $('#header'),
        triggerTerms: $('#trigger-terms'),
        triggerPrivacy: $('#trigger-privacy'),
        triggerContact: $('#trigger-contact')
    },
    bindUIActions: function() {
      var parent = this;

      parent.settings.mobileDropButton.on('click', function(e){
        e.preventDefault();
        console.log('hello');

        parent.settings.siteHeader.toggleClass('active');


      });

      parent.settings.triggerTerms.on('click', function(e){
        $.magnificPopup.open({

            items: {
              src: '#popup-terms'
            },
            type: 'inline',

            callbacks: {
              open: function() {
                console.log("Hello");
                $('#popup-terms .pop-contents').mCustomScrollbar();
              }
            }
          }, 0);

      });

      parent.settings.triggerPrivacy.on('click', function(e){
        $.magnificPopup.open({

            items: {
              src: '#popup-privacy'
            },
            type: 'inline',

            callbacks: {
              open: function() {
                 $('#popup-privacy .pop-contents').mCustomScrollbar();
              }
            }
          }, 0);

      });


      parent.settings.triggerContact.on('click', function(e){
        $.magnificPopup.open({

            items: {
              src: '#popup-contact'
            },
            type: 'inline',

            callbacks: {
              open: function() {
                $('#popup-contact .pop-contents').mCustomScrollbar();
              }
            }
          }, 0);

      });
    },
    init: function() {
      this.bindUIActions();
    }
  };

  var moduleFind = {
        settings: {
          branchGroups: $('.branch-groups'),
          previousActiveBranchObj: false
        },
        bindUIActions: function() {
          var parent = this;

          parent.settings.branchGroups.on('click', '.branch', function(e) {
            var $this = $(this);


            if(!parent.settings.previousActiveBranchObj) {
              parent.settings.previousActiveBranchObj = $this;
              $this.addClass('active');
            } else {

              // When closing the same button, return the previousActiveBranchObj to default
              if($this.attr('data-branch-id') === parent.settings.previousActiveBranchObj.attr('data-branch-id')) {
                $this.removeClass('active');
                parent.settings.previousActiveBranchObj = false;
              } else {
                parent.settings.previousActiveBranchObj.removeClass('active');
                $this.addClass('active');
                parent.settings.previousActiveBranchObj = $this;
              }

            }

          });


          // If touchscreen
          if(!Modernizr.touch) {

            parent.settings.branchGroups.on('mouseleave', '.branch', function(e) {
              var $this = $(this);

              if(!$this.hasClass('active')) {

                if($this.hasClass('purple')) {
                  $this.removeClass('purple');
                  $this.addClass('green');
                } else {
                  $this.removeClass('green');
                  $this.addClass('purple');
                }

              }

            });

          } else {

            parent.settings.branchGroups.on('touchend', '.branch', function(e) {
              var $this = $(this);

              if(!$this.hasClass('active')) {

                if($this.hasClass('purple')) {
                  $this.removeClass('purple');
                  $this.addClass('green');
                } else {
                  $this.removeClass('green');
                  $this.addClass('purple');
                }

              }

            });
          }

          $(window).on('resize', function(){
            parent.setBranchAddressWidth();
          });

        },
        init: function() {
          this.bindUIActions();
          this.setBranchAddressWidth();
        },
        setBranchAddressWidth: function() {
          var parent = this,
              singleBranch = parent.settings.branchGroups.find('.branch');




          parent.settings.branchGroups.find('.branch-address').each(function(i){
            var $this = $(this);

            $this.css('width', singleBranch.outerWidth());

          });
        }
      };


  var moduleSplash = {
        settings: {
          splashContainer: $('.splash'),
          maskContainer: $('.mask'),
          svgLoader: $('#svg-loader'),
          splashLoaderInner: $('#chev-inner'),
          splashLoader: $('#main-stroke'),
          animateProduct: document.getElementById('animate-product'),
          animateProductCtx: false,
          logoContainer: $('.container-featured-logo'),
          productImages: {},
          logoRatio: 574 / 395
        },
        bindUIActions: function() {
          var parent = this;
          $(window).on('resize', function(e){
            window.location.reload();
          });
        },
        init: function() {

          this.setupParentContainer();

          this.bindUIActions();
          this.marlLoaderAnimation();
          this.loadImageFrames();
        },
        marlLoaderAnimation: function() {
          var parent = this,
              viewPort = new helperMethods.viewPort()
              afterLoadSequence = [];

          parent.settings.maskContainer.velocity({
            p: {
              background: 'transparent',
              opacity: 1,
              display: 'block'
            },
            o: {
              duration: 0
            }
          });

          /* Start Borderline Loading */
          parent.settings.splashLoader.velocity({
            p: {
              strokeDashoffset: 745 + 745
            },
            o: {
              duration: 4000,
              easing: 'linear',
              complete: function() {

                parent.settings.splashContainer.css({
                  'background-image':  'url(' + parent.settings.splashContainer.attr('data-bg') + ')'
                });

                // When loading of frames are ready
                if(parent.settings.animateProductCtx) {

                  $(parent.settings.splashContainer).addClass('complete');

                  parent.settings.splashLoaderInner.velocity({
                    p: {
                      opacity: 0
                    },
                    o: {
                      duration: 500,
                      complete: function() {


                        parent.settings.splashContainer
                          .transition({
                            delay: 1000,
                            scale: 1,
                            duration: 1000,
                            // height: viewPort.height
                            backgroundPosition: viewPort.height < viewPort.width ? '50% 0' : false
                          });

                        parent.settings.maskContainer.velocity({
                          p: {
                            opacity: 0
                          },
                          o: {
                            duration: 1,
                            display: 'none',
                            delay: 1600,
                            duration: 400,
                            complete: function() {
                              // Start product animation
                              parent.playImages(parent.settings.animateProductCtx, parent.settings.productImages);



                            }
                          }
                        });
                      }
                    }
                  });

                } else {
                  // While frames are not completed, return this on loader
                  parent.settings.splashLoader.velocity({
                    p: {
                      strokeDashoffset: 745
                    },
                    o: {
                      duration: 0
                    }
                  });

                  parent.marlLoaderAnimation();
                }





              }
            }
          });



        },
        setupParentContainer: function() {
          /* Resize the Container On Page Load */
          var parent = this,
              container = parent.settings.splashContainer.find('.container'),
              viewPort = new helperMethods.viewPort();

          console.log(viewPort);
          console.log(parent.settings.splashContainer.width());
          console.log(container.outerWidth());

          if(viewPort.width > viewPort.height) {


            $('body').velocity({
              p: {
                height: viewPort.height
              },
              o: {
                duration: 0,
                begin: function() {
                  $(this).addClass('splash-page');
                }
              }
            });

            parent.settings.svgLoader.attr('width', viewPort.width);
            parent.settings.svgLoader.attr('height', viewPort.width / parent.settings.logoRatio);

            parent.settings.splashContainer.velocity({
              p: {
                width: viewPort.width,
                height: viewPort.width / parent.settings.logoRatio
              },
              o: {
                duration: 0
              }
            });

          } else {
            console.log("Smaller!");

            parent.settings.splashContainer.velocity({
              p: {
                width: viewPort.height * parent.settings.logoRatio,
                height: viewPort.height
              },
              o: {
                duration: 0,
                complete: function() {

                  var pushLeft = (parent.settings.splashContainer.width() - viewPort.width) / 2;

                  console.log(parent.settings.splashContainer.width() - container.outerWidth());

                  if((viewPort.width < viewPort.height)) {
                    console.log(parent.settings.splashContainer.width() )

                    parent.settings.splashContainer.css({
                      left: pushLeft > 0 ? pushLeft * -1 : pushLeft
                    });

                  }
                }
              }
            });

          }


        },
        loadImageFrames: function () {
          var parent = this,
              start = 1,
              end = 182,
              context = parent.settings.animateProduct.getContext('2d'),
              viewport = new helperMethods.viewPort();

          // Canvas resizing on older browsers are not working (http://caniuse.com/#feat=canvas)

          if(viewport.width <= 640) {
            $(parent.settings.animateProduct).
              attr('width', 123)
              .attr('height', 196);
          } else {
            $(parent.settings.animateProduct).
              attr('width', 280)
              .attr('height', 395);
          }

          for(var i = start; i <= end; i++) {
              var filename = asset_url+'img/product/pack_' + i + '.png'; // Filename of each image
              var img = null;

              if(viewport.width <= 640) {
                img = new Image(123, 196);
              } else {
                img = new Image(280, 395);
              }
              img.src = filename;
              img.onload = this.onIMGLoad({
                            current: i,
                            start: start,
                            max: end,
                            context: context,
                            pushedImage: img
                          });

          }
        },
        onIMGLoad: function (obj) {
            // console.log(this.settings.productImages);


            function F() {
              // console.log(obj.current);

                moduleSplash.settings.productImages[obj.current] = obj.pushedImage;

                // If products are all downloaded
                if (obj.current === obj.max) {

                  // moduleSplash.playImages(obj.context, moduleSplash.settings.productImages);
                  moduleSplash.settings.animateProductCtx = obj.context;
                }

            }

            return F;
        },
        setImage: function (newLocation,context,imgGroup) {
          var viewport = new helperMethods.viewPort();

            // drawImage takes 5 arguments: image, x, y, width, height
            if(imgGroup[newLocation]) {
                // console.log(imgGroup[newLocation].src);
                // console.log(context);

                if(viewport.width <= 640) {
                  context.clearRect( 0, 0, 123, 196 );
                  context.drawImage(imgGroup[newLocation], 0, 0, 123, 196);
                } else {
                  context.clearRect( 0, 0, 280, 395 );
                  context.drawImage(imgGroup[newLocation], 0, 0, 280, 395);
                }
            }
        },
        playImages: function(ctx, imgGroup) {
          var parent = this,
              counter = 1,
              intervalID = null;

          intervalID = setInterval(function(){
            if(counter === 182) {
              console.log('done');
              clearInterval(intervalID);
              parent.showLogoContainer();
            } else {
              parent.setImage(counter, ctx, imgGroup)
              counter++;
            }
          }, 30);

        },
        showLogoContainer: function() {
          var parent = this,
              sequence = [];

          sequence = [
            {
              e: parent.settings.logoContainer,
              p: {
                opacity: 0
              },
              o: {
                visibility: 'hidden'
              }
            },
            {
              e: parent.settings.logoContainer,
              p: {
                opacity: 1
              },
              o: {
                visibility: 'visible'
              }
            }
          ];

          $.Velocity.RunSequence(sequence);

        }
      };


  var moduleDetailer = {
    settings: {
      containerCig: $('.container-cig'),
      containerDescription: $('.container-description'),
      detailerContainer: $('.detailer-main'),
      firstSlideContainer: $('.first-slide'),
      logoContainer: $('.container-featured-logo'),
      secondSlideContainer: $('.second-slide'),
      firstSlideText: $('.first-description'),
      secondSlideText: $('.second-description'),
      thirdSlideText: $('.third-description'),
      fourthSlideText: $('.fourth-description'),
      loaderContainer: $('.loader'),
      splashLoader: $('#main-stroke'),
      animatePurpleSplash: document.getElementById('purple-splash'),
      animatePurpleSplashCtx: false,
      animateGreenSplash: document.getElementById('green-splash'),
      animateGreenSplashCtx: false,
      animateProduct: document.getElementById('animate-product'),
      animateProductCtx: false,
      purpleImages: {},
      greenImages: {},
      productImages: {},
      slideNavigation: $('.slide-navigation'),
      currentStage: 1
    },
    bindUIActions: function() {
      var parent = this;
      $(window).on('resize', function(e){
        parent.setupParentContainer();
      });

      // Event listener for sequence navigation
      $(parent.settings.slideNavigation).on('click', 'button', function(e){
        var $this = $(this),
            container = parent.settings.slideNavigation;

        if($this.attr('data-action') == 'next') {

          container.attr('data-stage', ++parent.settings.currentStage);
          parent.sequenceController('next');

        } else if ($this.attr('data-action') == 'prev') {

          container.attr('data-stage', --parent.settings.currentStage);
          parent.sequenceController('prev');

        }

      });

    },
    init: function() {
      var viewport = new helperMethods.viewPort(),
          parent = this;

      // Setup canvas dimensions for image frames
      if(viewport.width <= 640) {
        $(parent.settings.animatePurpleSplash).
          attr('width', 230)
          .attr('height', 298);

        $(parent.settings.animateGreenSplash).
          attr('width', 230)
          .attr('height', 298);


        $(parent.settings.animateProduct).
          attr('width', 123)
          .attr('height', 196);
      } else {
        $(parent.settings.animatePurpleSplash).
          attr('width', 230)
          .attr('height', 298);

        $(parent.settings.animateGreenSplash).
          attr('width', 230)
          .attr('height', 298);


        $(parent.settings.animateProduct).
          attr('width', 280)
          .attr('height', 395);
      }

      // Load Images Frames ASAP
      parent.loadImageFrames({
        splashCanvas: parent.settings.animatePurpleSplash,
        directoryFile: 'splash-purple/purple',
        imgGroup: parent.settings.purpleImages,
        assignCtx: 'animatePurpleSplashCtx',
        start: 0,
        end: 158
      });


      parent.loadImageFrames({
        splashCanvas: parent.settings.animateGreenSplash,
        directoryFile: 'splash-green/green',
        imgGroup: parent.settings.greenImages,
        assignCtx: 'animateGreenSplashCtx',
        start: 0,
        end: 158
      });


      parent.loadImageFrames({
        splashCanvas: parent.settings.animateProduct,
        directoryFile: 'product/pack',
        imgGroup: parent.settings.productImages,
        assignCtx: 'animateProductCtx',
        start: 102,
        end: 181
      });

      parent.bindUIActions();
      parent.setupParentContainer();

      // Init sequence controller
      parent.sequenceController();
    },
    showLoaderAnimation: function(execute, parentSettingsCtx) {
      var parent = this;



      parent.settings.loaderContainer.velocity('transition.fadeIn');

      parent.settings.splashLoader.velocity({
        p: {
          strokeDashoffset: 745
        },
        o: {
          duration: 0
        }
      });

      parent.marlLoaderAnimation(execute, parentSettingsCtx);
    },
    marlLoaderAnimation: function(execute, parentSettingsCtx) {
      var parent = this;

      console.log(parent.settings.animatePurpleSplashCtx);

      /* Start Borderline Loading */
      this.settings.splashLoader.velocity({
        p: {
          strokeDashoffset: 745 + 745
        },
        o: {
          duration: 2000,
          easing: 'linear',
          complete: function() {
            // When loading of splash frames are ready
            if(parent.settings[parentSettingsCtx.green] && parent.settings[parentSettingsCtx.purple] && parent.settings[parentSettingsCtx.products]) {
              parent.settings.loaderContainer.velocity('transition.fadeOut');
              execute();
            } else {
              // While frames are not completed, return this on loader
              parent.settings.splashLoader.velocity({
                p: {
                  strokeDashoffset: 745
                },
                o: {
                  duration: 0
                }
              });

              parent.marlLoaderAnimation(execute, parentSettingsCtx);
            }

          }
        }
      });

    },
    toggleActiveNavs: function(action){
      var parent = this;

      if(parent.settings.currentStage == 2 && action == 'next') {
        parent.settings.slideNavigation
          .find('.prev')
          .addClass('active');
      } else if (parent.settings.currentStage == 1 && action == 'prev') {
        parent.settings.slideNavigation
          .find('.prev')
          .removeClass('active');
      }


      if(parent.settings.currentStage == 5 && action == 'next') {
        parent.settings.slideNavigation
          .find('.next')
          .removeClass('active');
      } else if (parent.settings.currentStage == 4 && action == 'prev') {
        parent.settings.slideNavigation
          .find('.next')
          .addClass('active');
      }

    },
    toggleSlideNavigation: function(action) {
      var parent = this;


      if(parent.settings.slideNavigation.hasClass('active')) {

        parent.settings.slideNavigation.velocity({
          p: {
            opacity: 1
          },
          o: {
            duration: 200,
            display: 'block',
            complete: function() {

              parent.settings.slideNavigation.removeClass('active');
              parent.toggleActiveNavs(action);
            }
          }
        });

      } else {
        parent.settings.slideNavigation.velocity({
          p: {
            opacity: 0
          },
          o: {
            duration: 200,
            display: 'none',
            complete: function() {
              parent.toggleActiveNavs(action);
              parent.settings.slideNavigation.addClass('active');
            }
          }
        });
      }


    },
    sequenceController: function(action) {
      var parent = this;


      parent.toggleSlideNavigation(action);


      if(parent.settings.currentStage == 1 && !action) {
        // Autoplay first sequence
        parent.showLoaderAnimation(function(){

          parent.firstSequence();

        }, {
          green: 'animateGreenSplashCtx',
          purple: 'animatePurpleSplashCtx',
          products: 'animateProductCtx'
        });

      } else if(parent.settings.currentStage == 2 && action == 'next') {
        parent.secondSequence();
      } else if(parent.settings.currentStage == 3 && action == 'next') {
        parent.thirdSequence();
      } else if(parent.settings.currentStage == 4 && action == 'next') {
        parent.fourthSequence();
      } else if(parent.settings.currentStage == 5 && action == 'next') {
        parent.fifthSequence();
      }



      if(parent.settings.currentStage == 1 && action == 'prev') {
        parent.firstSequenceBack();
      } else if(parent.settings.currentStage == 2 && action == 'prev') {
        parent.secondSequenceBack();
      } else if(parent.settings.currentStage == 3 && action == 'prev') {
        parent.thirdSequenceBack();
      } else if(parent.settings.currentStage == 4 && action == 'prev') {
        parent.fourthSequenceBack();
      }

    },
    firstSequence: function() {
      var parent = this,
          sequence = [],
          viewPort = new helperMethods.viewPort();


      if(viewPort.width >= 1026) {

        sequence = [
          { //Setup Initial Position
            e: parent.settings.containerCig,
            p: {
              translateX: '510px',
              translateY: '420px'
            },
            o: {
              duration: 0
            }
          },
          {
            // Animate cigarette to final position
            e: parent.settings.containerCig,
            p: {
              translateX: '320px',
              translateY: '0px'
            },
            o: {
              duration: 1000
            }
          },
          { //Hide Text First
            e: parent.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Hide Text First
            e: parent.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Show Text
            e: parent.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block'
            }
          },
          { //Show Text
            e: parent.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block',
              complete: function() {
                // Show the slide navigation after the end of animation sequence
                parent.toggleSlideNavigation();
              }
            }
          }

        ];

      }


      if(viewPort.width >= 1680) {

        sequence = [
          { //Setup Initial Position
            e: parent.settings.containerCig,
            p: {
              translateX: '580px',
              translateY: '857px',
              scale: 1.2,
              rotateZ: '1deg',
            },
            o: {
              duration: 0
            }
          },
          {
            // Animate cigarette to final position
            e: parent.settings.containerCig,
            p: {
              translateX: '340px',
              translateY: '363px',
              scale: 1.2,
              rotateZ: '1deg',
            },
            o: {
              duration: 1000
            }
          },
          { //Hide Text First
            e: parent.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Hide Text First
            e: parent.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Show Text
            e: parent.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block'
            }
          },
          { //Show Text
            e: parent.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block',
              complete: function() {
                // Show the slide navigation after the end of animation sequence
                parent.toggleSlideNavigation();
              }
            }
          }

        ];

      }




      if(viewPort.width <= 1025) {

        sequence = [
          { //Setup Initial Position
            e: parent.settings.containerCig,
            p: {
              translateX: '300px',
              translateY: '637px',
              rotateZ: '8deg'
            },
            o: {
              duration: 0
            }
          },
          {
            // Animate cigarette to final position
            e: parent.settings.containerCig,
            p: {
              translateX: '87px',
              translateY: '151px',
              rotateZ: '8deg'
            },
            o: {
              duration: 1000
            }
          },
          { //Hide Text First
            e: parent.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Hide Text First
            e: parent.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Show Text
            e: parent.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block'
            }
          },
          { //Show Text
            e: parent.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block',
              complete: function() {
                // Show the slide navigation after the end of animation sequence
                parent.toggleSlideNavigation();
              }
            }
          },

        ];
      }


      if(viewPort.width <= 640) {

        sequence = [
          { //Setup Initial Position
            e: parent.settings.containerCig,
            p: {
              translateX: '300px',
              translateY: '637px',
              rotateZ: '1deg',
            },
            o: {
              duration: 0
            }
          },
          {
            // Animate cigarette to final position
            e: parent.settings.containerCig,
            p: {
              translateX: '15px',
              translateY: '151px',
              rotateZ: '1deg'
            },
            o: {
              duration: 1000
            }
          },
          { //Hide Text First
            e: parent.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Hide Text First
            e: parent.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Show Text
            e: parent.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block'
            }
          },
          { //Show Text
            e: parent.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block',
              complete: function() {
                // Show the slide navigation after the end of animation sequence
                parent.toggleSlideNavigation();
              }
            }
          }

        ];
      }


      if(viewPort.width <= 480) {

        sequence = [
          { //Setup Initial Position
            e: parent.settings.containerCig,
            p: {
              translateX: '35px',
              translateY: '392px',
              rotateZ: '7deg',
              scale: 0
            },
            o: {
              duration: 0
            }
          },
          {
            // Animate cigarette to final position
            e: parent.settings.containerCig,
            p: {
              translateX: '-69px',
              translateY: '112px',
              rotateZ: '7deg',
              scale: .6
            },
            o: {
              duration: 1000
            }
          },
          { //Hide Text First
            e: parent.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Hide Text First
            e: parent.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Show Text
            e: parent.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block'
            }
          },
          { //Show Text
            e: parent.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block',
              complete: function() {
                // Show the slide navigation after the end of animation sequence
                parent.toggleSlideNavigation();
              }
            }
          },

        ];
      }

      if(viewPort.width <= 320) {

        sequence = [
          { //Setup Initial Position
            e: parent.settings.containerCig,
            p: {
              translateX: '43px',
              translateY: '388px',
              rotateZ: '0deg',
              scale: .6
            },
            o: {
              duration: 0
            }
          },
          {
            // Animate cigarette to final position
            e: parent.settings.containerCig,
            p: {
              translateX: '-78px',
              translateY: '136px',
              rotateZ: '0deg',
              scale: .6
            },
            o: {
              duration: 1000
            }
          },
          { //Hide Text First
            e: parent.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Hide Text First
            e: parent.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '0',
              translateX: '300px'
            },
            o: {
              duration: 0
            }
          },
          { //Show Text
            e: parent.settings.firstSlideText.find('.first-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block'
            }
          },
          { //Show Text
            e: parent.settings.firstSlideText.find('.second-line'),
            p: {
              opacity: '1',
              translateX: '0px'
            },
            o: {
              duration: 1000,
              display: 'block',
              complete: function() {
                // Show the slide navigation after the end of animation sequence
                parent.toggleSlideNavigation();
              }
            }
          },

        ];
      }


      $.Velocity.RunSequence(sequence);
    },
    secondSequence: function() {
      var parent = this,
          sequence = [];


      sequence = [
        { //Hide first slide text
          e: parent.settings.firstSlideText,
          p: {
            opacity: '0',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'none'
          }
        },
        { //Hide Text First
          e: parent.settings.secondSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.secondSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        {
          e: parent.settings.secondSlideText,
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-purple'),
          p: {
            opacity: '0'
          },
          o: {
            duration: 0,
            display: 'none'
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-purple'),
          p: {
            opacity: '1'
          },
          o: {
            duration: 250,
            loop: 2,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.secondSlideText.find('.first-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            sequenceQueue: false,
            duration: 1000,
            display: 'block'
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-purple'),
          p: {
            opacity: '0' //Originally 1
          },
          o: {
            duration: 250,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.secondSlideText.find('.second-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            sequenceQueue: false,
            delay: 100,
            duration: 1000,
            display: 'block',
            begin: function() {

              moduleDetailer.animateSplash({
                ctx: moduleDetailer.settings.animatePurpleSplashCtx,
                imgGroup: moduleDetailer.settings.purpleImages,
                start: 0,
                end: 113,
                duration: 2500
              });

            },
            complete: function() {
              // Show the slide navigation after the end of animation sequence
              parent.toggleSlideNavigation();
            }
          }
        }

      ];


      $.Velocity.RunSequence(sequence);



    },
    thirdSequence: function() {
      var parent = this,
          sequence = [];


      sequence = [
        { //Hide first slide text
          e: parent.settings.secondSlideText,
          p: {
            opacity: '0',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'none',
            begin: function() {


              moduleDetailer.animateSplash({
                ctx: moduleDetailer.settings.animatePurpleSplashCtx,
                imgGroup: moduleDetailer.settings.purpleImages,
                start: 113,
                end: 159,
                done: false,
                duration: 1000
              });

            }
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-purple'),
          p: {
            opacity: '0'
          },
          o: {
            sequenceQueue: false,
            duration: 200,
            display: 'none'
          }
        },
        { //Hide Text First
          e: parent.settings.thirdSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.thirdSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        {
          e: parent.settings.thirdSlideText,
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-green'),
          p: {
            opacity: '0'
          },
          o: {
            duration: 0,
            display: 'none'
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-green'),
          p: {
            opacity: '1'
          },
          o: {
            duration: 250,
            loop: 2,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.thirdSlideText.find('.first-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block',
            sequenceQueue: false
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-green'),
          p: {
            opacity: '0' //Originally 1
          },
          o: {
            duration: 250,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.thirdSlideText.find('.second-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            delay: 100,
            display: 'block',
            sequenceQueue: false,
            begin: function() {

              moduleDetailer.animateSplash({
                ctx: moduleDetailer.settings.animateGreenSplashCtx,
                imgGroup: moduleDetailer.settings.greenImages,
                start: 0,
                end: 113,
                done: false,
                duration: 2500
              });

            },
            complete: function() {
              // Show the slide navigation after the end of animation sequence
              parent.toggleSlideNavigation();
            }
          }
        }

      ];

      $.Velocity.RunSequence(sequence);

    },
    fourthSequence: function() {
      var parent = this,
          sequence = [];


      sequence = [
        { //Hide first slide text
          e: parent.settings.thirdSlideText,
          p: {
            opacity: '0',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'none',
            begin: function() {


              moduleDetailer.animateSplash({
                ctx: moduleDetailer.settings.animateGreenSplashCtx,
                imgGroup: moduleDetailer.settings.greenImages,
                start: 113,
                end: 159,
                done: false,
                duration: 1000
              });

            }
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-green'),
          p: {
            opacity: '0'
          },
          o: {
            sequenceQueue: false,
            duration: 200,
            display: 'none'
          }
        },
        { //Hide Text First
          e: parent.settings.fourthSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.fourthSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.fourthSlideText.find('.third-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        {
          e: parent.settings.fourthSlideText,
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-purple'),
          p: {
            opacity: '0'
          },
          o: {
            duration: 0,
            display: 'none'
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-green'),
          p: {
            opacity: '0'
          },
          o: {
            duration: 0,
            display: 'none'
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-purple'),
          p: {
            opacity: '1'
          },
          o: {
            duration: 250,
            loop: 2,
            display: 'block',
            sequenceQueue: false
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-green'),
          p: {
            opacity: '1'
          },
          o: {
            duration: 250,
            loop: 2,
            display: 'block',
            sequenceQueue: false
          }
        },
        { //Show Text
          e: parent.settings.fourthSlideText.find('.first-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            delay: 500,
            display: 'block',
            sequenceQueue: false
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-purple'),
          p: {
            opacity: '0' //Originally 1
          },
          o: {
            duration: 250,
            display: 'block',
            sequenceQueue: false
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-green'),
          p: {
            opacity: '0' //Originally 1
          },
          o: {
            duration: 250,
            display: 'block',
            sequenceQueue: false
          }
        },
        { //Show Text
          e: parent.settings.fourthSlideText.find('.second-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block',
            delay: 100,
            sequenceQueue: false,
            begin: function() {


              moduleDetailer.animateSplash({
                ctx: moduleDetailer.settings.animatePurpleSplashCtx,
                imgGroup: moduleDetailer.settings.purpleImages,
                start: 0,
                end: 113,
                done: false,
                duration: 2500
              });



              moduleDetailer.animateSplash({
                ctx: moduleDetailer.settings.animateGreenSplashCtx,
                imgGroup: moduleDetailer.settings.greenImages,
                start: 0,
                end: 113,
                done: false,
                duration: 2500
              });

            }
          }
        },
        { //Show Text
          e: parent.settings.fourthSlideText.find('.third-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block',
            complete: function() {
              // Show the slide navigation after the end of animation sequence
              parent.toggleSlideNavigation();
            }
          }
        }

      ];

      $.Velocity.RunSequence(sequence);

    },
    fifthSequence: function() {
      var parent = this,
          sequence = [];


      sequence = [
        { //Hide first slide text
          e: parent.settings.firstSlideContainer,
          p: {
            opacity: '0'
          },
          o: {
            duration: 1000,
            display: 'none'
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-purple'),
          p: {
            opacity: '0'
          },
          o: {
            sequenceQueue: false,
            duration: 200,
            display: 'none'
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-green'),
          p: {
            opacity: '0'
          },
          o: {
            sequenceQueue: false,
            duration: 200,
            display: 'none'
          }
        },
        { //Hide Div first
          e: parent.settings.secondSlideContainer,
          p: {
            opacity: '0'
          },
          o: {
            duration: 0,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.secondSlideContainer,
          p: {
            opacity: '1'
          },
          o: {
            delay: 200,
            duration: 1000,
            display: 'block',
            begin: function() {

              moduleDetailer.animateSplash({
                ctx: moduleDetailer.settings.animateProductCtx,
                imgGroup: moduleDetailer.settings.productImages,
                start: 102,
                end: 181,
                done: false,
                duration: 2000,
                size: {
                  default: {
                    width: 280,
                    height: 395
                  },
                  small: {
                    width: 123,
                    height: 196
                  }
                }
              });

            }
          }
        },
        {
          e: parent.settings.logoContainer,
          p: {
            opacity: 0
          },
          o: {
            visibility: 'hidden'
          }
        },
        {
          e: parent.settings.logoContainer,
          p: {
            opacity: 1
          },
          o: {
            visibility: 'visible',
            begin: function() {


              moduleDetailer.animateSplash({
                ctx: moduleDetailer.settings.animatePurpleSplashCtx,
                imgGroup: moduleDetailer.settings.purpleImages,
                start: 158,
                end: 159,
                done: false,
                duration: 500
              });



              moduleDetailer.animateSplash({
                ctx: moduleDetailer.settings.animateGreenSplashCtx,
                imgGroup: moduleDetailer.settings.greenImages,
                start: 158,
                end: 159,
                done: false,
                duration: 500
              });

            },
            complete: function() {
              // Show the slide navigation after the end of animation sequence
              parent.toggleSlideNavigation();
            }
          }
        }

      ];

      $.Velocity.RunSequence(sequence);

    },
    firstSequenceBack: function() {
      var parent = this,
          sequence = [],
          viewPort = new helperMethods.viewPort();



      sequence = [
        { //Hide first slide text
          e: parent.settings.secondSlideText,
          p: {
            opacity: '0',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'none',
            begin: function() {

              moduleDetailer.animateSplash({
                ctx: moduleDetailer.settings.animatePurpleSplashCtx,
                imgGroup: moduleDetailer.settings.purpleImages,
                start: 113,
                end: 159,
                done: false,
                duration: 1000
              });

            }
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-purple'),
          p: {
            opacity: '0'
          },
          o: {
            sequenceQueue: false,
            duration: 200,
            display: 'none'
          }
        },
        { //Hide Text First
          e: parent.settings.secondSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.secondSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.firstSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.firstSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Show first slide text
          e: parent.settings.firstSlideText,
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        { //Show Text
          e: this.settings.firstSlideText.find('.first-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        { //Show Text
          e: this.settings.firstSlideText.find('.second-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block',
            complete: function() {
              // Show the slide navigation after the end of animation sequence
              parent.toggleSlideNavigation();
            }
          }
        }
      ];

      $.Velocity.RunSequence(sequence);
    },
    secondSequenceBack: function() {
      var parent = this,
          sequence = [];


      sequence = [
        {
          e: parent.settings.thirdSlideText,
          p: {
            opacity: '0',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'none',
            begin: function() {

              moduleDetailer.animateSplash({
                ctx: moduleDetailer.settings.animateGreenSplashCtx,
                imgGroup: moduleDetailer.settings.greenImages,
                start: 113,
                end: 159,
                done: false,
                duration: 1000
              });

            }
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-green'),
          p: {
            opacity: '0'
          },
          o: {
            sequenceQueue: false,
            duration: 200,
            display: 'none'
          }
        },
        { //Hide Text First
          e: parent.settings.thirdSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.thirdSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.secondSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.secondSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        {
          e: parent.settings.secondSlideText,
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-purple'),
          p: {
            opacity: '1'
          },
          o: {
            duration: 250,
            loop: 2,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.secondSlideText.find('.first-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            sequenceQueue: false,
            duration: 1000,
            display: 'block'
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-purple'),
          p: {
            opacity: '0' //Originally 1
          },
          o: {
            duration: 250,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.secondSlideText.find('.second-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            sequenceQueue: false,
            delay: 100,
            duration: 1000,
            display: 'block',
            begin: function() {

              moduleDetailer.animateSplash({
                ctx: moduleDetailer.settings.animatePurpleSplashCtx,
                imgGroup: moduleDetailer.settings.purpleImages,
                start: 1,
                end: 113,
                done: false,
                duration: 2500
              });

            },
            complete: function() {
              // Show the slide navigation after the end of animation sequence
              parent.toggleSlideNavigation();
            }
          }
        }

      ];


      $.Velocity.RunSequence(sequence);
    },
    thirdSequenceBack: function() {
      var parent = this,
          sequence = [];


      sequence = [
        {
          e: parent.settings.fourthSlideText,
          p: {
            opacity: '0',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'none',
            begin: function() {

              moduleDetailer.animateSplash({
                ctx: moduleDetailer.settings.animatePurpleSplashCtx,
                imgGroup: moduleDetailer.settings.purpleImages,
                start: 113,
                end: 159,
                done: false,
                duration: 1000
              });


              moduleDetailer.animateSplash({
                ctx: moduleDetailer.settings.animateGreenSplashCtx,
                imgGroup: moduleDetailer.settings.greenImages,
                start: 113,
                end: 159,
                done: false,
                duration: 1000
              });

            }
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-purple'),
          p: {
            opacity: '0'
          },
          o: {
            sequenceQueue: false,
            duration: 200,
            display: 'none'
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-green'),
          p: {
            opacity: '0'
          },
          o: {
            sequenceQueue: false,
            duration: 200,
            display: 'none'
          }
        },
        { //Hide Text First
          e: parent.settings.fourthSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.fourthSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.fourthSlideText.find('.third-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.thirdSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.thirdSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        {
          e: parent.settings.thirdSlideText,
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-green'),
          p: {
            opacity: '1'
          },
          o: {
            duration: 250,
            loop: 2,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.thirdSlideText.find('.first-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block',
            sequenceQueue: false
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-green'),
          p: {
            opacity: '0' //Originally 1
          },
          o: {
            duration: 250,
            display: 'block'
          }
        },
        { //Show Text
          e: parent.settings.thirdSlideText.find('.second-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            delay: 100,
            display: 'block',
            sequenceQueue: false,
            begin: function() {

              moduleDetailer.animateSplash({
                ctx: moduleDetailer.settings.animateGreenSplashCtx,
                imgGroup: moduleDetailer.settings.greenImages,
                start: 0,
                end: 113,
                done: false,
                duration: 2500
              });

            },
            complete: function() {
              // Show the slide navigation after the end of animation sequence
              parent.toggleSlideNavigation();
            }
          }
        }

      ];


      $.Velocity.RunSequence(sequence);
    },
    fourthSequenceBack: function() {
      var parent = this,
          sequence = [];

      sequence = [
        { //Hide Div first
          e: parent.settings.secondSlideContainer,
          p: {
            opacity: '0'
          },
          o: {
            duration: 1000,
            display: 'none'
          }
        },
        {
          e: parent.settings.fourthSlideText,
          p: {
            opacity: '0',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'none'
          }
        },
        { //Hide first slide text
          e: parent.settings.firstSlideContainer,
          p: {
            opacity: '1'
          },
          o: {
            duration: 500,
            display: 'block'
          }
        },
        {
          e: parent.settings.logoContainer,
          p: {
            opacity: 0
          },
          o: {
            visibility: 'hidden'
          }
        },
        { //Hide Text First
          e: parent.settings.fourthSlideText.find('.first-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.fourthSlideText.find('.second-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        { //Hide Text First
          e: parent.settings.fourthSlideText.find('.third-line'),
          p: {
            opacity: '0',
            translateX: '300px'
          },
          o: {
            duration: 0
          }
        },
        {
          e: parent.settings.fourthSlideText,
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block'
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-purple'),
          p: {
            opacity: '1'
          },
          o: {
            duration: 250,
            loop: 2,
            display: 'block',
            sequenceQueue: false
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-green'),
          p: {
            opacity: '1'
          },
          o: {
            duration: 250,
            loop: 2,
            display: 'block',
            sequenceQueue: false
          }
        },
        { //Show Text
          e: parent.settings.fourthSlideText.find('.first-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            delay: 500,
            display: 'block',
            sequenceQueue: false
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-purple'),
          p: {
            opacity: '0' //Originally 1
          },
          o: {
            duration: 250,
            display: 'block',
            sequenceQueue: false
          }
        },
        {
          e: parent.settings.containerCig.find('.glow-green'),
          p: {
            opacity: '0' //Originally 1
          },
          o: {
            duration: 250,
            display: 'block',
            sequenceQueue: false
          }
        },
        { //Show Text
          e: parent.settings.fourthSlideText.find('.second-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block',
            delay: 100,
            sequenceQueue: false,
            begin: function() {


              moduleDetailer.animateSplash({
                ctx: moduleDetailer.settings.animatePurpleSplashCtx,
                imgGroup: moduleDetailer.settings.purpleImages,
                start: 0,
                end: 113,
                done: false,
                duration: 2500
              });


              moduleDetailer.animateSplash({
                ctx: moduleDetailer.settings.animateGreenSplashCtx,
                imgGroup: moduleDetailer.settings.greenImages,
                start: 0,
                end: 113,
                done: false,
                duration: 2500
              });

            }
          }
        },
        { //Show Text
          e: parent.settings.fourthSlideText.find('.third-line'),
          p: {
            opacity: '1',
            translateX: '0px'
          },
          o: {
            duration: 1000,
            display: 'block',
            complete: function() {
              // Show the slide navigation after the end of animation sequence
              parent.toggleSlideNavigation();
            }
          }
        }

      ];

      $.Velocity.RunSequence(sequence);
    },

    setupParentContainer: function() {
      /* Resize the Container On Page Load */
      var container = this.settings.detailerContainer.find('.container'),
          viewPort = new helperMethods.viewPort(),
          parent = this;

      // console.log(viewPort);
      // console.log(this.settings.detailerContainer.width());
      // console.log(container.outerWidth());


      if(viewPort.width >= 1280) {
        parent.settings.detailerContainer.velocity({
          p:{
            height: viewPort.height - $('#footer').outerHeight()
          },
          o: {
            duration: 0
          }
        });
      }

      if(viewPort.width < 1025) {
        container.outerWidth(viewPort.width);

        this.settings.detailerContainer.css({
          left: -((this.settings.detailerContainer.width() - container.outerWidth()) / 2)
        });

      }

    },
    loadImageFrames: function (config) {
      var parent = this,
          start = config.start ? config.start : 1,
          end = config.end ? config.end : 127,
          context = config.splashCanvas.getContext('2d'),
          viewport = new helperMethods.viewPort();

      // Canvas resizing on older browsers are not working (http://caniuse.com/#feat=canvas)


      for(var i = start; i <= end; i++) {
          var filename = asset_url+'img/' + config.directoryFile + '_' + i + '.png'; // Filename of each image
          var img = null;

          console.log(filename);


          if(viewport.width <= 640) {
            img = new Image(230, 298);
          } else {
            img = new Image(230, 298);
          }
          img.src = filename;
          img.onload = this.onIMGLoad({
                        current: i,
                        start: start,
                        max: end,
                        context: context,
                        assignContext: config.assignCtx,
                        pushedImage: img,
                        imgGroup: config.imgGroup
                      });
      }
    },
    onIMGLoad: function (obj) {
        // console.log(this.settings.productImages);


        function F() {
          // console.log(obj.current);

            obj.imgGroup[obj.current] = obj.pushedImage;

            // If splash frames are all downloaded
            if (obj.current === obj.max) {

              moduleDetailer.settings[obj.assignContext] = obj.context;

            }

        }

        return F;
    },
    setImage: function (newLocation, context, imgGroup, size) {
      var viewport = new helperMethods.viewPort(),
          canvasSize = size ? size : {
            default: {
              width: 230,
              height: 298
            },
            small: {
              width: 230,
              height: 298
            }
          };

        // drawImage takes 5 arguments: image, x, y, width, height
        if(imgGroup[newLocation]) {
            console.log(imgGroup[newLocation].src);
            // console.log(context);

            if(viewport.width <= 640) {
              context.clearRect( 0, 0, canvasSize.small.width, canvasSize.small.height );
              context.drawImage(imgGroup[newLocation], 0, 0, canvasSize.small.width, canvasSize.small.height );
            } else {
              context.clearRect( 0, 0, canvasSize.default.width, canvasSize.default.height);
              context.drawImage(imgGroup[newLocation], 0, 0, canvasSize.default.width, canvasSize.default.height);
            }
        }
    },
    animateSplash: function(config) {
      var parent = this,
          counter = config.start ? config.start : 0,
          intervalID = null,
          transitionSpeed = config.duration / (config.end - config.start),
          progress = 0;


      intervalID = setInterval(function(){

        console.log(counter);

        if(counter === config.end) {
          config.done ? config.done() : false;
          clearInterval(intervalID);
        } else {
          parent.setImage(counter, config.ctx, config.imgGroup, config.size)
          counter++;
        }
      }, transitionSpeed);

    }
  }



  if(helperMethods.settings.findContainer.length > 0) {
    console.log("Find Page");
    moduleFind.init();
  }

  if(helperMethods.settings.splashContainer.length > 0) {
    console.log("Splash Page")
    moduleSplash.init();
  }

  if(helperMethods.settings.detailerContainer.length > 0) {
    console.log("Detailer Page");
    moduleDetailer.init();
  }

  moduleGlobal.init();
})(jQuery, window, document, window._);
