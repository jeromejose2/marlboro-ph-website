var Fullscreen = new Fullscreen;

$(function(){
	mainLayout()
	$(window).resize(mainLayout);
	
});

var windowSize = 0;
function mainLayout(){

	var win = $(window);
	var doc = $(document);
	var body = $('body');
	var wrapper = $('.wrapper');
	var wrapperFixed = $('.wrapper.fixed, .main.fixed');
	var footer = $('footer.main');
	var header = $('header.main');
	var section = $('section.main');
	var govwarn = $('.government-warning');
	var winHeight = win.height();
	var docHeight = doc.height();
	var per15 = winHeight*0.15;

	/* mobile */
	govwarn.height( per15 );
	body.css({paddingBottom : footer.height()  });

	var footHeight = footer.outerHeight();
	var headHeight = header.outerHeight();

	if(windowSize == winHeight ) return;

	windowSize = winHeight;
	

	var containerHeight = parseInt(body.css('padding-bottom')) + parseInt(body.css('padding-top'));
		containerHeight =  win.height() - containerHeight;
		containerHeight =  win.width() > 767 ? containerHeight < 350 ? 350 : containerHeight : containerHeight;
		
	//if( !wrapper.hasClass('fixed') )
	wrapper.css({ minHeight: containerHeight});
	
	wrapperFixed.css({ height: containerHeight});
}


(function () {
  "use strict";
  $ = this.jQuery || require('jquery');
  
  function offsetRelative(selector) {
    var $el = this;
    var $parent = $el.parent();
    if (selector) {
      $parent = $parent.closest(selector);
    }
 
    var elOffset     = $el.offset();
    var parentOffset = $parent.offset();
 
    return {
      left: elOffset.left - parentOffset.left,
      top: elOffset.top - parentOffset.top
    };
  }
  
  // Exports
  $.fn.offsetRelative || ($.fn.offsetRelative = offsetRelative);
  if (typeof module !== "undefined" && module !== null) {
    module.exports = $.fn.offsetRelative;
  }
}).call(this);


function Fullscreen(){
	var win = $(window);
	var header = $('header.main');
	var footer = $('footer.main');
	var body   = $('body');
	var wrapper= $('.wrapper.parallax');
	var bodyPT = parseInt(body.css('padding-top'));

	var wrapHt = wrapper.height();

	this.enter = function(){
		$(header).fadeOut()
		body.css({paddingTop : 0 })
		wrapper.css({height : win.height() - footer.height() })
	}

	this.exit = function(){
		$(header).fadeIn()
		body.css({paddingTop : bodyPT })
		wrapper.css({height : win.height() - (footer.height() + bodyPT ) })
		mainLayout();
		wrapper.css({minHeight : 0});
	}
}