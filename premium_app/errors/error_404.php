<!DOCTYPE html>
<html>
<head>
	<title>Page Not Found</title>
	<noscript>
		<meta http-equiv="refresh" content="0;//<?= $_SERVER['SERVER_NAME'] ?><?= !DEV_MODE ? '/' : str_replace('premium2.php', '', $_SERVER['SCRIPT_NAME']) ?>noscript">
	</noscript>
	<link rel="shortcut icon" href="//<?= $_SERVER['SERVER_NAME'] ?><?= !DEV_MODE ? '/' : str_replace('premium2.php', '', $_SERVER['SCRIPT_NAME']) ?>error_assets/images/favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="//<?= $_SERVER['SERVER_NAME'] ?><?= !DEV_MODE ? '/' : str_replace('premium2.php', '', $_SERVER['SCRIPT_NAME']) ?>error_assets/css/style.css">
</head>
<body>
	<header><div></div></header>

	<section>
		<h2><?= $heading ?></h2>
		<h3><?= $message ?></h3>
	</section>


	<footer>
		<div></div>
		<span>GOVERNMENT WARNING: CIGARETTE SMOKING IS DANGEROUS TO YOUR HEALTH</span>
	</footer>

</body>
</html>