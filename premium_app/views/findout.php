<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL . ASSETS_DIR ?>css/animate.css">
<!-- main content starts here  -->
<section class="main">
	
	<div class="page-ctrl">
		<button class="prev disable" data-next="0">&lt;</button>
		<button class="next"  data-next="2">&gt;</button>
	</div>

	<div class="wrapper detailer-wrapper detailer-01">	
		<div class="container">
					
				<div class="detailer-copy copy-01">
						<img src="<?php echo BASE_URL . ASSETS_DIR ?>images/detailer-title.png" class="img-responsive mauto">
						<div class="line-separator"></div>
						<h5>PERFECTION IN EVERY DETAIL. </h5>
				</div>
		</div>
	</div>

	<div class="wrapper detailer-wrapper detailer-02 hidden">	
		<div class="container">
			<div class="detailer-copy copy-02">	
				<h2><img src="<?php echo BASE_URL . ASSETS_DIR ?>images/detailer-title-02.png" class="img-responsive"></h2>
				<p>For the makers of Ferrari, Design is everything. 
					<br>It is their birthright and their legacy. </p>

				<p>The design language for Marlboro Premium Black is simple and it starts with 'Perfection'. </p>

				<p>To design an offer that would stand in a class of its own-that would personify perfection-Marlboro and Pininfarina 
				proved to be the ultimate match, reflecting both brands' remarkable legacies. </p>
			</div>
		</div>
	</div>

	<div class="wrapper detailer-wrapper detailer-03 hidden">	
		<div class="container">
			<div class="detailer-copy copy-03">	
				<h2><img src="<?php echo BASE_URL . ASSETS_DIR ?>images/detailer-title-03.png" class="img-responsive"></h2>

				<p>The art of freshness is understood only by a few And perfected only by Marlboro.</p>

				<p>Marlboro Premium Black introduces 
				an exclusive innovation in freshness
				protection and maneuverability:
				the PRO-FRESH seal. The seal is engineered 
				to automatically open in a single,  
				smooth movement, sealing itself closed for lasting freshness.</p>
			</div>
		</div>
	</div>

	<div class="wrapper detailer-wrapper detailer-04 hidden">	
		<div class="container">
			<div class="detailer-copy copy-04">	
				<h2><img src="<?php echo BASE_URL . ASSETS_DIR ?>images/detailer-title-04.png" class="img-responsive"></h2>

				<p>Perfection takes time, dedication, practice, skill, and passion. Perfection understands the balance between too much and too little. Perfection sees the finest detail atop the finest detail, and perfects it. </p>

				<p>Perfection is a minimal black matte finish. Perfection is a fine yet determined red line cutting across an all-black design. Perfection is lined in metallic red,
				contrasting against the crisp-white paper within.</p>

				<p>Perfection is not earned; it is achieved. </p>
			</div>
		</div>
	</div>

	<div class="wrapper detailer-wrapper detailer-05 hidden">	
		<div class="container">
			<div class="detailer-copy copy-06">	
				<h2><img src="<?php echo BASE_URL . ASSETS_DIR ?>images/detailer-title-06.png" class="img-responsive"></h2>

				<p>Tobacco leaves are varied  <br>
				all over the world. <br>
				Some big, some small, <br>
				some bright, some dark,<br>
				some round, some long.</p>

				<p>Few Perfect.</p>

				<p>using only imported tobaccos sourced from around the world, The tobaccos selected for Marlboro Premium Black are beyond compare. high quality, smooth tasting, superior. 
				Perfect.</p>
				<p>Everything the discerning adult smoker desires. </p>
			</div>
		</div>
	</div>

	<div class="wrapper detailer-wrapper detailer-06 hidden">	
		<div class="container">
			<div class="detailer-copy copy-05">	
				
				<p>DESIGNED BY PININFARINA<br>
				SEALED WITH PRO-FRESH TECHNOLOGY<br>
				BLENDED WITH THE FINEST TOBACCOS</p>
				<div class="line-separator"></div>
				<img src="<?php echo BASE_URL . ASSETS_DIR ?>images/detailer-title-05.png" class="img-responsive">
			</div>
		</div>
	</div>


	<!-- <div class="wrapper detailer-wrapper detailer-07 hidden">	
		<div class="container">
			<div class="detailer-copy copy-07">	
				<p>Take our quiz and find out what your premium personality is.</p>
				<a class="btn" href="<?php echo site_url() ?>/quiz"><i>START</i></a>
			</div>
		</div>
	</div> -->

</section>
<!-- main content ends here  -->

<script>
$(function(){

		$('.page-ctrl button').click(function(){
			var max = 7;
			var el   = $(this);
			var page = el.data('next');	
			var next = page+1; 
				next = next > max ? max : next; 
			var prev = page-1; 
				prev = prev < 1 ? 1 : prev;
			var active = $('.detailer-0'+page);
			var current = el.hasClass('next') ? active.prev() : active.next();
			var siblings = active.siblings('.wrapper').not( current );
			
			
			if(el.hasClass('disable')) return false;
			if(el.hasClass('pause')) return false;
			el.addClass('pause');
			
			siblings.addClass('hidden').removeClass('animated fadeOut fadeIn')

			
			current.addClass('animated fadeOut');
			setTimeout(function(){
				active.removeClass('hidden').addClass('animated fadeIn');
				current.addClass('hidden').removeClass('animated fadeOut fadeIn');
			},700)
			
			$('h2',active)
				.addClass('animated fadeInLeft')
				.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
					$('h2',active).removeClass('animated fadeInLeft');
					el.removeClass('pause');
				});

			setTimeout(function(){
					el.removeClass('pause');
				},700)

			$('.page-ctrl .next').data('next',next);
			$('.page-ctrl .prev').data('next',prev);


			if(page == max && next == max){
				//window.location="<?php echo BASE_URL ?>premium/quiz";
			}else{
				if($('.page-ctrl .next').hasClass('disable'))
				$('.page-ctrl .next').removeClass('disable');
			}

			if(page == 1 && next == 2){
				$('.page-ctrl .prev').addClass('disable');
			}else{
				if($('.page-ctrl .prev').hasClass('disable'))
				$('.page-ctrl .prev').removeClass('disable');
			}
	});

	});
</script>