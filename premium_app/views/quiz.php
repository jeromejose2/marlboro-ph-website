<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL . ASSETS_DIR ?>css/animate.css">
<!-- main content starts here  -->
<section class="main">
	<div class="wrapper quiz">	
		<div class="container">

			<!-- 	<div class="page-ctrl">
					<button class="prev disable" data-next="0">&lt;</button>
					<button class="next"  data-next="2">&gt;</button>
				</div> -->
					
				<div class="premium-quiz">
						
					<div class="quiz quiz-start">
						<h3>WHAT TYPE OF PREMIUM ARE YOU?</h3>

						<p>Take our quiz and find out what your premium personality is.</p>
						<div class="line-separator"></div>

						<button class="btn"><i>START</i></button>

						<div class="bg-start"></div>
					</div>


					<?php 
					if($quizzes) {
						foreach ($quizzes as $key => $value) { ?>
						<div class="quiz  quiz-0<?php echo $key + 1 ?>">
							<span class="question"><?php echo $key + 1 ?>. <?php echo $value['question'] ?></span>
							<ul class="answers">
								<?php if($value['choices']) {
									foreach ($value['choices'] as $ck => $cv) { ?>
									<li data-choice="<?php echo $value['question_id'] . '_' . $cv['choice_label'] ?>" class="<?php echo $key == count($quizzes) - 1 ? 'last' : '' ?>"><span> <?php echo $cv['choice'] ?></span></li>	
									<?php }
								} ?>
							</ul>
						</div>	
						<?php }
					}
					?>

					<div class="quiz  result">
						<h3>WHAT TYPE OF PREMIUM ARE YOU?</h3> <br><br>
						<div class="row">
							<div class="col-sm-5 col-sm-push-7">
								<img id="result-image" src="<?php echo BASE_URL . ASSETS_DIR ?>images/quiz-result-watch.jpg" class="img-responsive">
								<br>
							</div>
							<div class="col-sm-7 col-sm-pull-5 txtleft">
								<span class="question">SIMPLE AND SHARP</span>
								<p id="result-copy"></p>
								<!-- <a href="<?php echo site_url() ?>/find-premium-black" class="btn"><i>FIND PREMIUM BLACK</i></a> -->
							</div>
						</div>
					
					</div>

				</div>
				
		</div>

	</div>

</section>
<!-- main content ends here  -->
<script>
var answers = [];
$(function(){
	// $('.quiz-start .btn').click(function(){
	// 	$('.quiz-start p').addClass('animated fadeOutRight')
	// 	$('.quiz-start .line-separator').addClass('animated fadeOut')
	// 	$('.quiz-start .btn').addClass('animated fadeOutDown')
	// 	$('.quiz-start h3').addClass('animated fadeOutLeft').addClass('animated fadeOutDown').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
	// 		$('.quiz-start').hide();
	// 		$('.quiz-01').addClass('animated fadeInDown show');
	// 	});
		
	// });

	// $('.quiz .answers li').click(function(){
	// 	var el = $(this);
	// 	var parent = el.parent().parent();
	// 	var next = parent.next();

	// 	el.addClass('selected');
	// 	//el.siblings().addClass('animated fadeOutDown');
		
	// 	if(next.length==0) return;

	// 	setTimeout(function(){
	// 		parent.addClass('animated fadeOutDown');
	// 	},500);
	// 	setTimeout(function(){
	// 		parent.addClass('hidden').removeClass('animated').removeClass('fadeOutDown');
	// 		next.addClass('animated fadeInDown show');
	// 	},1000);
	// })
	var Quiz = new PremiumQuiz();
	Quiz.init();
})


function PremiumQuiz(){
	
	var thisClass = this;
	var page = 1;
	var el = {
		answers : $('.answers li'),
		prev : $('.page-ctrl .prev'),
		next : $('.page-ctrl .next'),
		start : $('.quiz-start .btn'),
		quiz : $('.premium-quiz'),
		quizes : $('.premium-quiz .quiz'),
	}

	this.init = function(){

		el.next.click(function(){
			thisClass.navigate('next');
			
		});

		el.prev.click(function(){
			thisClass.navigate('prev');
		});

		el.answers.click(function(){
			var me = $(this);
			me.addClass('selected');
			me.siblings().removeClass('selected');

			setTimeout(function(){thisClass.navigate('next')},300)
		});

		el.start.click(function(){
			thisClass.navigate('next');
		})
	}

	this.navigate = function(move){
			
		if(move=='next'){
			el.prev.removeClass('disable');

			if(page==el.quizes.length-1){
				displayResult();
				el.next.addClass('disable');
			}
				
			if(page==el.quizes.length) {
				return;
			}
		}

		if(move=='prev'){
			el.next.removeClass('disable');

			if(page==2){
				el.prev.addClass('disable');
			}
				
			if(page==1) return;
		}

		el.quiz.children(':nth-child('+page+')').removeClass('show animated fadeIn').addClass('hidden');
			
		page = (move == 'next') ? page + 1 : page - 1 ;

		el.quiz.children(':nth-child('+page+')').addClass('show animated fadeIn').removeClass('hidden');
	}

}
</script>

<script>
$(function(){
	



	// $('.quiz-start .btn').click(function(){
	// 	$('.quiz-start p').addClass('animated fadeOutRight')
	// 	$('.quiz-start .line-separator').addClass('animated fadeOut')
	// 	$('.quiz-start .btn').addClass('animated fadeOutDown')
	// 	$('.quiz-start h3').addClass('animated fadeOutLeft').addClass('animated fadeOutDown').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
	// 		$('.quiz-start').hide();
	// 		$('.quiz-01').addClass('animated fadeInDown show');
	// 	});
		

	//});

	$('.quiz .answers li').click(function(){
		var el = $(this);
		var parent = el.parent().parent();
		var next = parent.next();

		// el.addClass('selected');
		// el.siblings().addClass('animated fadeOutDown');
		
		if(next.length==0) return;
		
		answers.push(el.data('choice'));
		if(el.hasClass('last'))  {
			displayResult();
		}
		// setTimeout(function(){parent.addClass('animated fadeOutDown');},800);
		// setTimeout(function(){
		// 	parent.addClass('hidden').removeClass('animated').removeClass('fadeOutDown');
		// 	next.addClass('animated fadeInDown show');
		// },1600);

		
	});

});
function getResult(answers) {
		var firstAns = '';
		var scores = [0, 0, 0];
		var scoresLabel = ['A', 'B', 'C']
		if(answers) {
			for(i in answers) {
				splitAns = answers[i].split('_');
				if(i == 0)
					firstAns = splitAns[1];
				if(splitAns[1] == 'A')
					scores[0]++;
				else if(splitAns[1] == 'B')
					scores[1]++;
				else
					scores[2]++;
			}
		}	
		tempScores = scores.slice();
		scores.sort(function(a, b){return b-a});
		if(scores[0] == scores[1])
			return firstAns;
		else {
			for(i = 0; i<3; i++) {
				if(tempScores[i] == scores[0]) 
					return scoresLabel[i];
			}
		}
	}

function sortObjectByKey(obj){
    var keys = [];
    var sortedObj = {};

    for(var key in obj){
        if(obj.hasOwnProperty(key)){
            keys.push(key);
        }
    }

    // sort keys
    keys.sort();

    // create new array based on Sorted Keys
    jQuery.each(keys, function(i, key){
        sortedObj[key] = obj[key];
    });

    return sortedObj;
};

function displayResult() {
	resultValue = '';
	$('.page-ctrl').hide();
	mostChoice = getResult(answers);
	if(mostChoice == 'A') {
		resultValue = 'Flash and Flair';
		resultImage = '<?php echo BASE_URL . ASSETS_DIR ?>images/quiz-result-watch.jpg'
		resultCopy = 'You light up the room the moment you walk in, and effortlessly stand out from the crowd. You look good in designer clothes, enjoy the taste of fine wine, and have no problems flaunting your style for the whole world to see. You are as premium as premium gets, that\'s why Marlboro Premium Black suits you. With a pack specially designed by Pininfarina, the makers of Ferrari, a Pro-Fresh seal to preserve freshness, and the flavor that comes from the blend of the finest tobaccos, Marlboro Premium Black is for people who demand nothing but the best, like you.<br><br>Be seen with the rest of the social elite at these lounges:<br>71 Gramercy, Hyve, and Prive'
	} else if(mostChoice == 'B') {
		resultValue = 'Practical Perfection';
		resultImage = '<?php echo BASE_URL . ASSETS_DIR ?>images/quiz-result-shades.jpg'
		resultCopy = 'You make practical look premium. You choose your purchases wisely, and invest in the things that matter to you. This is why Marlboro Premium Black is perfect for you. With a pack specially designed by Pininfarina, the makers of Ferrari, a Pro-Fresh seal to preserve freshness, and the flavor that comes from the blend of the finest tobaccos, Marlboro Premium Black gives you perfection in every detail: a worthwhile investment.<br><br>Here are perfect places for you to be seen in:<br>Prime, Draft, and Craft'	
	} else {
		resultValue ='Simple and Sharp';
		resultImage = '<?php echo BASE_URL . ASSETS_DIR ?>images/quiz-result-cuff.jpg'
		resultCopy = 'You are at the top of your game, and you expect the same from everyone and everything around you. While down-to-earth and easygoing, you know how to indulge and you appreciate the fine details. This is why Marlboro Premium Black is best for you: With a pack specially designed by Pininfarina, the makers of Ferrari, a Pro-Fresh seal to preserve freshness, and the flavor that comes from the blend of the finest tobaccos, Marlboro Premium Black gives you perfection in every detail.<br><br>Here are some places that simply fit your style:<br>Publiko, Tipsy Pig, and Black Olive'	
	}
	$('.result').find('.question').html(resultValue);
	$('.result').find('#result-copy').html(resultCopy);
	$('#result-image').prop('src', resultImage);
	$.post('<?php echo site_url() ?>/quiz/submit', {data: answers, result: resultValue}, function() {
		
	})
}
</script>