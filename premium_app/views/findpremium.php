<!-- main content starts here  -->
<section class="main">
	<div class="wrapper find-premium">	
		<div class="container">
					
			<div class="title">
				<img src="<?php echo BASE_URL . ASSETS_DIR ?>images/find-premium-black.png">
				<div class="line-separator"></div>
			</div>

			<ul class="find">
				<?php if($stores) {
					foreach ($stores as $key => $value) { 
						if($value['stores'])
							echo '<li class="head">' . $value['region'] .  '</li>';
						if($value['stores']) {
							foreach ($value['stores'] as $sk => $sv) { ?>
								<li><span><?php echo $sv['store_name'] ?></span>
									<small><?php echo $sv['address'] ?> <?php echo $sv['telephone'] ? 'Tel. No. ' . $sv['telephone']  : '' ?></small>
								</li>		
							<?php }
						}
						?>
					<?php }
				} ?>
			</ul>
				
		</div>

	</div>

</section>
<!-- main content ends here  -->
<script>
$(function(){
	$('ul.find li').click(function(){
		var el = $(this);

		el.toggleClass('active');
		el.siblings().removeClass('active');
	})
});
</script>