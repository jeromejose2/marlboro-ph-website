<?php

if (!function_exists('imagecreate')) {
	die('Image GD Library must be installed')
}

class Captcha
{
	private $font = CAPTCHA_FONT;

	public function set_font($font)
	{
		$this->font = $font;
		return $this;
	}

	public function output($code, $width = '120', $height = '40')
	{
		//font size will be 50% of the image height
		$font_size = $height * 0.50;
		$image = @imagecreate($width, $height) or die('Cannot initialize new GD image stream');
		
		/* set the colours */
		$background_color = imagecolorallocate($image, 255, 255, 255);
		$text_color = imagecolorallocate($image, 0, 0, 0);
		$noise_color = imagecolorallocate($image, 125, 125, 125);
		$line_color = imagecolorallocate($image, 255, 255, 255);
		$rand_amount  = mt_rand('1','5');
		$rand_amount2 = mt_rand('20','50');
		
		//generate random dots
		for ($i = 0; $i <= $rand_amount; $i++) {
			$size = rand('10',$height);
			imagefilledellipse($image, mt_rand(10, $width), mt_rand(10, $height), $size,$size, $noise_color);
		}
		
		//generate random lines
		for ($i = 0; $i <= $rand_amount2; $i++) {
			imageline($image, mt_rand(0,$width), mt_rand(0,$height), mt_rand(0,$width), mt_rand(0,$height), $line_color );
		}
		
		//create textbox and add text
		$textbox = imagettfbbox($font_size, 0, $this->font, $code) or die('Error in imagettfbbox function');
		$x = ($width - $textbox[4])/2;
		$y = ($height - $textbox[5])/2;
		imagettftext($image, $font_size, 0, $x, $y, $text_color, $this->font , $code) or die('Error in imagettftext function');
		
		//output captcha image to browser 
		header('Content-Type: image/jpeg');
		imagejpeg($image);
		imagedestroy($image);
	}

}