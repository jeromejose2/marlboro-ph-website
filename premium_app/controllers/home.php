<?php
class Home extends CI_Controller
{
	function __construct() {
		parent::__construct();
	}

	function index() {
		if(!$this->session->userdata('user_id'))
			redirect(BASE_URL);
		redirect('find-out-more');
		$data['main_content'] = $this->main_content();
		$this->load->view('template', $data);
	}

	function main_content() {
		$content = $this->load->view('home', null, true);
		return $content;
	}
}