<?php
class Findout extends CI_Controller
{
	function __construct() {
		parent::__construct();
	}

	function index() {
		if(!$this->session->userdata('user_id'))
			redirect(BASE_URL);
		$data['main_content'] = $this->main_content();
		$this->load->view('template', $data);
	}

	function main_content() {
		$content = $this->load->view('findout', null, true);
		return $content;
	}
}