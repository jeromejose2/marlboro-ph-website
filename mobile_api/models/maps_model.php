<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Maps_Model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		define('BASE_URL', base_url());
	}

	public function get_all_items($lat, $long, $radius) {
		$events = $this->get_event_items($lat, $long, $radius);	
		$packs = $this->get_pack_items($lat, $long, $radius);
		$bars = $this->get_bar_items($lat, $long, $radius);
		
		if(!isset($events['data']))
			$events['data'] = array();
		$data = $events['data'];
		if(isset($packs['data']))
			$data = array_merge($data, $packs['data']);
		if(isset($bars['data']))
			$data = array_merge($data, $bars['data']);
		
		if($data)
			return array('error' => '0', 'message' => 'Success', 'data' => $data);
		else 
			return array('error' => '1', 'message' => 'No matching records found');
		
	}

	public function get_pack_items($lat, $long, $radius) {
		$having = $radius > 0 ? "HAVING distance <= $radius" : ""; 
		$query = "	SELECT 
				 	lamp_promotion_id AS id, 
				    lamp_image AS image_url,
				    promotion_title AS title,
				    description AS `desc`,
				    lat AS loc_lat,
				    lng AS loc_lng,
				    (SELECT ((ACOS(SIN($lat * PI() / 180) * SIN(lat * PI() / 180) + COS($lat * PI() / 180) * COS(lat * PI() / 180) * COS(($long - lng) * PI() / 180)) * 180 / PI()) * 60 * 1.1515)) AS `distance`
					FROM tbl_lamp_promotions p
					LEFT JOIN tbl_lamps l
					ON l.lamp_id = p.lamp_id
					WHERE p.status = 1
					AND lat != ''
					AND lng != ''
					$having
					";
		$items = $this->db->query($query)->result_array();
		$item_arr = array();
		if($items) {
			foreach ($items as $key => $value) {
				$value['image_url']	= BASE_URL . 'uploads/perks/main_' . $value['image_url'];
				$value['desc'] = strip_tags(html_entity_decode($value['desc']));
				$value['type'] = TYPE_PACK;
				$value['type_string'] = TYPE_STRING_PACK;
				$value['from'] = 0;
				$value['to'] = 0;
				$item_arr[] = $value;			
			}
		}
		if($item_arr)
			return array('error' => '0', 'message' => 'Success', 'data' => $item_arr);
		else 
			return array('error' => '1', 'message' => 'No matching records found');

	}


	public function get_glist_item($id) {
		$item = $this->db->select("lamp_promotion_id AS id, 
								    lamp_image AS image_url,
								    promotion_title AS title,
								    description AS `desc`,
								    lat AS loc_lat,
								    lng AS loc_lng")
						 ->from('tbl_move_forward')
						 ->where('move_forward_id', $id)
						 ->get()
						 ->row();
		$item = (array) $item;
		if(!$item)
			return array('error' => '1', 'message' => 'No matching records found');
		$item['image_url']	= BASE_URL . 'uploads/perks/main_' . $item['image_url'];
		$item['desc'] = strip_tags(html_entity_decode($item['desc']));
		$value['type'] = TYPE_GLIST;
		$value['type_string'] = TYPE_STRING_GLIST;
		$value['from'] = 0;
		$value['to'] = 0;
		return array('error' => '0', 'message' => 'Success', 'data' => $item);
	}

	public function get_event_items($lat, $long, $radius) {
		//$having = $radius > 0 ? "HAVING distance <= radius" : ""; 
		$having = "HAVING distance <= radius"; 
		$query = "	SELECT 
				 	backstage_event_id AS id, 
				    image AS image_url,
				    title AS title,
				    description AS `desc`,
				    lat AS loc_lat,
				    lng AS loc_lng,
				    start_date, end_date,
				    radius,
				    (SELECT ((ACOS(SIN($lat * PI() / 180) * SIN(lat * PI() / 180) + COS($lat * PI() / 180) * COS(lat * PI() / 180) * COS(($long - lng) * PI() / 180)) * 180 / PI()) * 60 * 1.1515)) AS `distance`
					FROM tbl_backstage_events	
					WHERE status = 1
					AND platform_restriction != 1
					AND end_date >= '" . date('Y-m-d H:i:s') . "'
					AND lat != ''
					AND lng != ''
					$having
					";
		$items = $this->db->query($query)->result_array();
		$item_arr = array();
		if($items) {
			foreach ($items as $key => $value) {
				$value['image_url']	= BASE_URL . 'uploads/backstage/events/371_176_' . $value['image_url'];
				$value['desc'] = strip_tags(html_entity_decode($value['desc']));
				$value['type'] = TYPE_EVENT;
				$value['type_string'] = TYPE_STRING_EVENT;
				$value['from'] = 1000 * strtotime($value['start_date']);
				$value['to'] = 1000 * strtotime($value['end_date']);
				$value['distance'];
				$value['loc_lat'] = $value['loc_lat'] ? $value['loc_lat'] : 0;
				$value['loc_lng'] = $value['loc_lng'] ? $value['loc_lng'] : 0;
				unset($value['start_date']);
				unset($value['end_date']);
				$item_arr[] = $value;			
			}
		}
		if($item_arr)
			return array('error' => '0', 'message' => 'Success', 'data' => $item_arr);
		else 
			return array('error' => '1', 'message' => 'No matching records found');
	}

	public function get_movefwd_items($lat, $long, $radius) {
		$having = $radius > 0 ? "HAVING distance <= $radius" : ""; 
		$query = "	SELECT 
				 	move_forward_id AS id, 
				    move_forward_image AS image_url,
				    move_forward_title AS title,
				    move_forward_description AS `desc`,
				    lat AS loc_lat,
				    lng AS loc_lng,
				    (SELECT ((ACOS(SIN($lat * PI() / 180) * SIN(lat * PI() / 180) + COS($lat * PI() / 180) * COS(lat * PI() / 180) * COS(($long - lng) * PI() / 180)) * 180 / PI()) * 60 * 1.1515)) AS `distance`
					FROM tbl_move_forward	
					WHERE status = 1
					AND platform_restriction != 1
					AND lat != ''
					AND lng != ''
					$having
					";
		$items = $this->db->query($query)->result_array();
		$item_arr = array();
		if($items) {
			foreach ($items as $key => $value) {
				$value['image_url']	= BASE_URL . 'uploads/move_fwd/main_' . $value['image_url'];
				$value['desc'] = strip_tags(html_entity_decode($value['desc']));
				$value['type'] = TYPE_MOVEFWD;
				$value['type_string'] = TYPE_STRING_MOVEFWD;
				$value['from'] = 0;
				$value['to'] = 0;
				$value['loc_lat'] = $value['loc_lat'] ? $value['loc_lat'] : 0;
				$value['loc_lng'] = $value['loc_lng'] ? $value['loc_lng'] : 0;
				$item_arr[] = $value;			
			}
		}
		if($item_arr)
			return array('error' => '0', 'message' => 'Success', 'data' => $item_arr);
		else 
			return array('error' => '1', 'message' => 'No matching records found');
	}

	public function get_bar_items($lat, $long, $radius) {
		$having = $radius > 0 ? "HAVING distance <= $radius" : ""; 
		$query = "	SELECT 
				 	lamp_id AS id, 
				    lamp_image AS image_url,
				    lamp_name AS title,
				    description AS `desc`,
				    lat AS loc_lat,
				    lng AS loc_lng,
				    (SELECT ((ACOS(SIN($lat * PI() / 180) * SIN(lat * PI() / 180) + COS($lat * PI() / 180) * COS(lat * PI() / 180) * COS(($long - lng) * PI() / 180)) * 180 / PI()) * 60 * 1.1515)) AS `distance`
					FROM tbl_lamps	
					WHERE lat != ''
					AND lng != ''
					$having
					";
		$items = $this->db->query($query)->result_array();
		$item_arr = array();
		if($items) {
			foreach ($items as $key => $value) {
				$value['image_url']	= BASE_URL . 'uploads/reserve/' . $value['image_url'];
				$value['desc'] = strip_tags(html_entity_decode($value['desc']));
				$value['type'] = TYPE_BAR;
				$value['type_string'] = TYPE_STRING_BAR;
				$value['from'] = 0;
				$value['to'] = 0;
				$value['loc_lat'] = $value['loc_lat'] ? $value['loc_lat'] : 0;
				$value['loc_lng'] = $value['loc_lng'] ? $value['loc_lng'] : 0;
				$item_arr[] = $value;			
			}
		}
		if($item_arr)
			return array('error' => '0', 'message' => 'Success', 'data' => $item_arr);
		else 
			return array('error' => '1', 'message' => 'No matching records found');
	}

}