<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		define('BASE_URL', base_url());
		require_once('application/models/perks_model.php');
		require_once('application/models/notification_model.php');
		require_once('application/models/points_model.php');
		$this->perks = new Perks_model();
		$this->points = new Points_Model();
	}

	public function update_details($user_id, $nick, $gender, $mobile, $street, $barangay, $province, $city) {
		//$post['registrant_id'] = $user_id;
		
		$post['nick_name'] = $nick;
		$post['gender'] = $gender;
		$post['mobile_phone'] = $mobile;
		$post['street_name'] = $street;
		$post['barangay'] = $barangay;
		$post['province'] = $province;
		$post['city'] = $city;

		$this->db->where('registrant_id', $user_id);
		$this->db->update('tbl_registrants', $post);

	}

	public function get_messages($user_id, &$message = '') {
		$user = $this->db->select('registrant_id')
						 ->from('tbl_registrants')
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row();
		if(!$user) {
			$message = 'Invalid uid';
			return false;
		}

		$messages = $this->db->select('message_id AS id, message, message_date_sent AS sent, sender_id, (SELECT nick_name FROM tbl_registrants WHERE registrant_id = sender_id) AS sender, (SELECT current_picture FROM tbl_registrants WHERE registrant_id = sender_id) AS current_picture')
							->from('tbl_messages')
							->where('recipient_id', $user_id)
							->get()
							->result_array();
		$items_arr = array();
		if($messages) {
			foreach ($messages as $key => $value) {
				$value['sent'] = strtotime($value['sent']) * 1000;
				$value['profile_picture'] = base_url() . 'uploads/profile/' . $value['sender_id'] . '/' . $value['current_picture'];
				unset($value['current_picture']); 
				$items_arr[] = $value;
			}
		}
		return $items_arr;
	}

	public function get_notifications($user_id, &$message = '') {
		$user = $this->db->select('registrant_id')
						 ->from('tbl_registrants')
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row();
		if(!$user) {
			$message = 'Invalid uid';
			return false;
		}

		$messages = $this->db->select('notification_id AS id, notification_message AS message, notification_date_created, notification_status AS status')
							->from('tbl_notifications')
							->where('registrant_id', $user_id)
							->where('notification_deleted', 0)
							->order_by('notification_date_created', 'DESC')
							->get()
							->result_array();
		$items_arr = array();
		if($messages) {
			foreach ($messages as $key => $value) {
				$value['date'] = strtotime($value['notification_date_created']) * 1000;
				unset($value['notification_date_created']);
				$items_arr[] = $value;
			}
		}
		return $items_arr;
	}

	public function read_notification($user_id, $id, &$message = '') {
		$notification = $this->db->select('notification_id')
					 ->from('tbl_notifications')
					 ->where('notification_id', $id)
					 ->where('registrant_id', $user_id)
					 ->get()
					 ->row();
		if(!$notification) {
			$message = 'Invalid id';
			return false;
		}

		$post['notification_status'] = 1;
		$post['notification_date_read'] = date('Y-m-d H:i:s');
		$this->db->where('notification_id', $id);
		$this->db->update('tbl_notifications', $post);
	
	}



	public function login($username, $password, $device_id, $device_name = '', $is_overwrite, &$message = '') {
		if(!$device_name) {
			$message = 'Device name is required';
			return false;	
		}
		if ($username && filter_var($username, FILTER_VALIDATE_EMAIL) && $password) {
			$user = (array) $this->db->select()
						->from('tbl_registrants')
						->where('email_address', $username)
						->get()
						->row();

			if($user) {
				$this->load->library('Encrypt');

				$today = date('Y-m-d H:i:s');
				$is_blocked = $user['login_attempts'] >= LOGIN_ATTEMPTS;
				
				if ($is_blocked && $user['date_blocked']) {
					if (strtotime($today) >= strtotime(LOGIN_RETURN, strtotime($user['date_blocked']))) {
						$is_blocked = false;
					} else {
						$message = 'Your account was blocked due to invalid login attempts';
						return false;
					}
				}
				$access_granted = false;
				if ($user['from_migration'] && !$user['encryption_changed']) {
					$access_granted = $this->encrypt->password($password, $user['salt']) === $user['password'];
				} else {
					$access_granted = $this->encrypt->decode($user['password'], $user['salt']) === $password;
				}

				if (!$is_blocked && $access_granted) {
					$this->db->insert('tbl_login', array(
							'registrant_id' => $user['registrant_id'],
							'date_login' => $today
						));

					$tasks = $this->get_tasks($user['registrant_id']);
					$tasks_arr = array();
					if($tasks) {
						foreach ($tasks as $key => $value) {
							if($value['status'] == 2)
								$tasks_arr[] = $value;	
						}
					}

					if($device_id != $user['device_id'] && !$is_overwrite && $user['device_name']) {
						$message = 'Your account is currently logged in to ' . $user['device_name'];
						return 2;	
					}

					$this->db->where('registrant_id', $user['registrant_id']);
					$this->db->update('tbl_registrants', array('device_id'	=> $device_id, 'device_name' => $device_name));

					$gender = $user['gender'] == 'M' ? 0 : 1;

					$this->db->insert('tbl_login', array('registrant_id' => $user['registrant_id'], 'source' => 2, 'date_login' => date('Y-m-d H:i:s')));
					//echo $this->db->last_query();

					return array('id'	 			=> $user['registrant_id'],
								 'nickname'			=> $user['nick_name'],
								 'gender'			=> $gender,
								 'mobile'			=> $user['mobile_phone'],
								 'street'		    => $user['street_name'],
								 'barangay'		    => $user['barangay'],
								 'city'		    	=> $user['city'],
								 'city_name'		=> $this->get_city_name($user['city']),
								 'province'		    => $user['province'],
								 'province_name'   	=> $this->get_province_name($user['province']),
								 'points'			=> $user['total_points'],
								 'password'			=> $user['password'],
								 'image_url'		=> base_url() . 'uploads/profile/' . $user['registrant_id'] . '/' . $user['current_picture']);
				} else {
					$message = 'Your account is blocked';
					return false;
				}
			} else {
				$message = 'Invalid username or password';
				return false;
			}

		} else {
			$message = 'Invalid email or password';
			return false;
		}
	}

	public function logout($username, $password, &$message = '') {
		if ($username && filter_var($username, FILTER_VALIDATE_EMAIL) && $password) {
			$user = (array) $this->db->select()
						->from('tbl_registrants')
						->where('email_address', $username)
						->get()
						->row();

			if($user) {
				$this->db->where('registrant_id', $user['registrant_id']);
				$this->db->update('tbl_registrants', array('device_name' => '', 'device_id' => ''));
				return true;
			} else {
				$message = 'Invalid username or password';
				return false;
			}
		} else {
			$message = 'Invalid username or password';
			return false;
		}
	}

	public function save_bid($user_id, $id, $bid, &$message = '') {
		$user = $this->db->select('*')
						 ->from('tbl_registrants')
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row_array();
		if(!$user) {
			$message = 'Invalid uid';
			return false;
		}
		
		$item = $this->db->select('*')
						->from('tbl_bid_items')
						->where('bid_item_id', $id)
						->get()
						->row_array();

		if(!$item) {
			$message = 'Invalid id';
			return false;	
		}	
		$bidder = (array) $this->perks->get_bidder($id); 
		$required_bid = $item['starting_bid'] >= @$bidder['bid'] ? $item['starting_bid'] : $bidder['bid'];
		$bid_data = $this->perks->check_bid_points($user_id);

		if(!is_numeric($bid)) {
			$message = 'Bid amount is invalid.';
			return false;
		} elseif(!$item) {
			
			$message = 'Item does not exist.';	
			return false;
		} elseif($user['total_points'] < $bid) {
			
			$message = 'Insufficient points.';	
			return false;
		} elseif($user['total_points'] >= $bid && $bid_data['available_points'] < $bid) {
			
			$message = 'You only have ' . $bid_data['available_points'] . ' points available for you to use. You placed a bid for ' . implode(', ', $bid_data['items']) . ', and ' . $bid_data['reserved_points'] . ' points are blocked off while you are still the highest bidder.';	
			return false;
		} elseif(isset($bidder['registrant_id']) && $bidder['registrant_id'] == $this->session->userdata('user_id')) {
			
			$message = 'You are currently the highest bidder.';	
			return false;
		} elseif(strtotime(date('Y-m-d H:i:s')) > strtotime($item['end_date']) || $item['status'] == 0) {
			
			$message = 'Sorry, this item is no longer active for bidding.';	
			return false;
		} elseif(strtotime(date('Y-m-d H:i:s')) < strtotime($item['start_date'])) {
			
			$message = 'Sorry, this item is not yet active for bidding.';	
			return false;
		} elseif($bid < $required_bid) {
			
			$message = 'Please place a bid higher than  ' . $required_bid . ' points.';	
			return false;
		} elseif(@$bidder['bid'] >= $bid) {
			
			$message = 'Please place a bid higher than  ' . $bidder['bid'] . ' points.';
			return false;
		} else {

			if(isset($bidder['registrant_id'])) {
				$this->notification_model = new Notification_Model();
				$message = 'You are no longer the highest bidder of ' . $item['bid_item_name'] . '. Should you wish to bid again, please do so here: <a href="' . BASE_URL . 'perks/bid/' . $item['bid_item_id'] . '">' . BASE_URL . 'perks/bid/' . $item['bid_item_id'] . '</a>';
				$param = array('message'=>$message,'suborigin'=>$id);
				$this->notification_model->notify($bidder['registrant_id'],PERKS_BID, $param);	
			}
			$data['success'] = 1;
			$param = array();
			$param['bid_item_id'] = $this->input->post('id');
			$param['bid'] = $this->input->post('bid_amt');
			$param['registrant_id'] = $this->input->post('user_id');
			$param['bid_date']  = date('Y-m-d H:i:s');
			$param['source'] = 2;
			$this->perks->save_bid($param);
		}
		return 1;
		
	} 

	public function save_buy($user_id, $id, &$message = '') {
		$user = $this->db->select('*')
						 ->from('tbl_registrants')
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row_array();
		if(!$user) {
			$message = 'Invalid uid';
			return false;
		}
		
		$item = (array) $this->perks->get_buy_item($id);

		if(!$item) {
			$message = 'Invalid id';
			return false;	
		}	
		
		$bid_data = $this->perks->check_bid_points($user_id);	
		if($user['total_points'] < $item['credit_value']) {
			$message = "You don’t have enough points to buy " . $item['buy_item_name'] . ".";	
			return false;
		} elseif($item['status'] != 1) {
			$message = $item['buy_item_name'] . " is not yet active for buying.";	
			return false;
		} elseif($user['total_points'] >= $item['credit_value'] && $bid_data['available_points'] < $item['credit_value']) {
			$message = 'You only have ' . $bid_data['available_points'] . ' points available for you to use. You placed a bid for ' . implode(', ', $bid_data['items']) . ', and ' . $bid_data['reserved_points'] . ' points are blocked off while you are still the highest bidder.';	
			return false;
		} else {
			if($this->perks->has_bought($id, $user_id)) {
				$message = 'You already bought ' . $item['buy_item_name'] . '.';
				return false;
			} elseif($item['status'] == 0 || $item['stock'] <= 0) { 
				$message = 'Sorry, this item is out of stock.';
				return false;
			} else {
				$param['buy_item_id'] = $id;
				$param['registrant_id'] = $user_id;	
				$param['source'] = 2;

				$properties = $this->db->select('property_name')
									->from('tbl_properties')
									->where('buy_item_id', $id)
									->get()
									->result_array();
				$prop = array();
				if($properties) {
					foreach ($properties as $key => $value) {
						$prop[] = $value['property_name'];
					}
				}

				$_POST['prop'] = $prop;
				$_POST['val'] = array($this->input->post('prop1Selection'), $this->input->post('prop2Selection'));

				$item_stock = $this->perks->save_buy_item($id, $param);
				$param = array();
				# deduct stock count	
				if(isset($item_stock['stocks']) && $item_stock['stocks'] - 1 >= 0) {
					$data['success'] = 1;
					$data['id'] = $id;
					$this->points->spend(PERKS_BUY, array(
											'points' => $item['credit_value'],
											'suborigin' => $id,
											'remarks' => '{{ name }} bought ' . $item['buy_item_name'] . ' on Perks',
											'registrant' => $user_id
											));
					$this->perks->deduct_stock($this->input->post('id'), $item['stock'] - 1);
					$this->notification_model = new Notification_Model();
					//$buy = $this->Perks_Model->get_buy_details($id);
					$message = 'You have successfully purchased ' . $item['buy_item_name'] . ' for ' . $item['credit_value'] . ' points. <br><br>
								' . $item['buy_item_name'] . ' will be delivered to your registered mailing address within 60 days. Please ensure that all your contact details are up-to-date. <br><br>
								Continue logging on to MARLBORO.PH to earn more points!';

	 				$param = array('message'=>$message,'suborigin'=>$id);
					$this->notification_model->notify($user_id,PERKS_BUY,$param);

				// } else {
				// 		$message = 'Sorry, ' . @$values[0] . ' ' . @$values[1] . ' ' .$item['buy_item_name'] . ' is no longer available';
				}
				
				
				return 1;
			}
		}
	}

	public function get_purchases($user_id, &$message = '') {
		$user = $this->db->select('*')
						 ->from('tbl_registrants')
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row_array();
		if(!$user) {
			$message = 'Invalid uid';
			return false;
		}

		$purchases = $this->db->select('buy_item_id AS id, buy_date AS date_purchased')
							  ->from('tbl_buys')
							  ->where('registrant_id', $user_id)
							  ->get()
							  ->result_array();
		$item_arr = array();
		if($purchases) {
			foreach ($purchases as $key => $value) {
				$value['date_purchased'] = strtotime($value['date_purchased']) * 1000;
				$item_arr[] = $value;
			}
		}
		return $item_arr;
	}

	public function get_tasks($user_id, &$message = '') {
		$user = $this->db->select('*')
						 ->from('tbl_registrants')
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row_array();
		if(!$user) {
			$message = 'Invalid uid';
			return false;
		}

		$purchases = $this->db->select('move_forward_title AS title, move_forward_description AS description, mf.move_forward_id AS id, move_forward_choice, move_forward_choice_status, move_forward_choice_date AS task_date')
							  ->from('tbl_move_forward mf')
							  ->join('tbl_move_forward_choice mfc', 'mf.move_forward_id = mfc.move_forward_id')
							  ->where('registrant_id', $user_id)
							  ->get()
							  ->result_array();
		$item_arr = array();
		if($purchases) {
			foreach ($purchases as $key => $value) {

				if($value['move_forward_choice'] == 1 && $value['move_forward_choice_status'] == 0)
					$value['status'] =  1;
				else 
					$value['status'] = 2;

				if($value['move_forward_choice_status'] == 1)
					$value['is_active'] = 1;
				else
					$value['is_active'] = 0;

				unset($value['move_forward_choice']);
				unset($value['move_forward_choice_status']);

				$value['task_date'] = strtotime($value['task_date']) * 1000;
				$item_arr[] = $value;
			}
		}
		return $item_arr;
	}

	public function get_updates($user) {
		$feeds_arr = array();
		$feeds = $this->db->select('points_id AS id, remarks AS message, date_earned AS sent, r.registrant_id, r.first_name, r.third_name, nick_name')
				->from('tbl_points p')
				->where('p.remarks IS NOT NULL')
				->join('tbl_registrants r', 'r.registrant_id = p.registrant_id')
				->order_by('p.date_earned', 'DESC')
				->limit(10)
				->get()
				->result_array();
			foreach ($feeds as $feed) {
				$feed['sent'] = strtotime($feed['sent']) * 1000;
				if ((int) $feed['registrant_id'] === $user) {
					$feed['message'] = str_replace(array('{{ name }}', ' is '), array('You', ' are '), $feed['message']);
				} else {
					$feed['message'] = str_replace('{{ name }}', $feed['nick_name'], $feed['message']);
				}
				$feed['message'] = strtoupper($feed['message']);
				$feeds_arr[] = $feed;
			}

		return $feeds_arr;
	}

	public function get_cities($province_id) {
		$cities = $this->db->select('*')
						 ->from('tbl_cities')
						 ->where('province_id', $province_id)
						 ->order_by('city')
						 ->get()->result_array();
		return $cities;
	}

	public function get_provinces() {
		$provinces = $this->db->select('*')
						 ->from('tbl_provinces')
						 ->order_by('province')
						 ->get()->result_array();
		return $provinces;
	}

	public function get_mobile_prefixes() {
		$prefixes = $this->db->select('mobile_prefix')
						 ->from('tbl_mobile_prefix')
						 ->order_by('mobile_prefix')
						 ->get()->result_array();
		$prefixes_arr = array();
		if($prefixes) {
			foreach ($prefixes as $key => $value) {
				$prefixes_arr[] = $value['mobile_prefix'];
			}
		}
		return $prefixes_arr;
	}

	public function sync_to_calendar($id, $user_id) {
		$this->db->insert('tbl_calendar_sych', array('id'	=> $id, 'registrant_id' => $user_id));
	}

	private function get_province_name($province_id) {
		$province = $this->db->select('province')
						->from('tbl_provinces')
						->where('province_id', $province_id)
						->get()
						->row_array();
		return isset($province['province']) ? $province['province'] : '';
	}

	private function get_city_name($city_id) {
		$city = $this->db->select('city')
						->from('tbl_cities')
						->where('city_id', $city_id)
						->get()
						->row_array();
		return isset($city['city']) ? $city['city'] : '';
	}
	
}