<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {

	var $_miles_to_meters;
	public function __construct()
	{
		parent::__construct();

		$this->load->model('mobile_model');
		$this->_miles_to_meters = 1609.34;

	}

	public function index() {
		$lat 	= $this->input->post('lat') ? $this->input->post('lat') : '12.2323232323';
		$lng 	= $this->input->post('lng') ? $this->input->post('lng') : '12.232323232323';
		$radius = $this->input->post('radius') ? $this->input->post('radius') : 0;
		$radius = (double) $radius / $this->_miles_to_meters;
		$this->mobile_model->get_glist_items($lat, $lng, $radius);
		$this->load->view('test');
	}
}

?>
