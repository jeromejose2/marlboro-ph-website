<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movefwd extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('movefwd_model');
	}

	public function display_error() {
		if(!$this->login_model->check()) {
			$return = array('error' => 1,
							'message' => 'Invalid app id or secret');
			$this->output->set_content_type('application/json')->_display(json_encode($return));
			exit;
		}

		if(!$this->validate_list_input()) {
 			$return = array('error' => 1,
							'message' => strip_tags(validation_errors()));
			$this->output->set_content_type('application/json')->_display(json_encode($return));
			exit;	
 		} 
		
	}


	public function pledge() {

		$this->display_error();

		$user_id = $this->input->post('user_id');
		$id = $this->input->post('id');

		$pledged = $this->movefwd_model->save_pledge($user_id, $id, $message);
		if($pledged === false) {
			$return = array('error' => 1, 'message' => $message);	
		} else {
			$return = array('error' => 0, 'message' => 'Success', 'data' => 1);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($return));	
	}


	public function startTask() {

		$this->display_error();

		$user_id = $this->input->post('user_id');
		$id = $this->input->post('id');

		$pledged = $this->movefwd_model->save_play($user_id, $id, $message);
		if($pledged === false) {
			$return = array('error' => 1, 'message' => $message);	
		} else {
			$return = array('error' => 0, 'message' => 'Success', 'data' => $pledged);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($return));	
	}

	private function validate_list_input($is_update = false) {

		$this->load->library('form_validation');

		$rules = array(
			   array(
					 'field'   => 'lat',
					 'label'   => 'lat',
					 'rules'   => 'decimal'
				  ),
			   array(
					 'field'   => 'lng',
					 'label'   => 'lng',
					 'rules'   => 'decimal'
				  ),
			   array(
					 'field'   => 'radius',
					 'label'   => 'radius',
					 'rules'   => 'decimal'
				  ),
			   array(
					 'field'   => 'user_id',
					 'label'   => 'user_id',
					 'rules'   => 'numeric'
				  ),
			   array(
					 'field'   => 'id',
					 'label'   => 'id',
					 'rules'   => 'numeric'
				  ),
			   array(
					 'field'   => 'name',
					 'label'   => 'name',
					 'rules'   => 'xss_clean'
				  ),
			   array(
					 'field'   => 'type',
					 'label'   => 'type',
					 'rules'   => 'numeric'
				  )
			);	
		
 
		 $this->form_validation->set_rules($rules);
		
		 return $this->form_validation->run();
 	}

	

}

?>
