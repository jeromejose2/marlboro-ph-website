<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Maps extends CI_Controller {

	var $_miles_to_meters;
	public function __construct()
	{
		parent::__construct();

		$this->load->model('maps_model');
		$this->_miles_to_meters = 1609.34;

	}

	public function display_error() {
		if(!$this->login_model->check()) {
			$return = array('error' => 1,
							'message' => 'Invalid app id or secret');
			$this->output->set_content_type('application/json')->_display(json_encode($return));
			exit;
		}

		if(!$this->validate_list_input()) {
 			$return = array('error' => 1,
							'message' => strip_tags(validation_errors()));
			$this->output->set_content_type('application/json')->_display(json_encode($return));
			exit;	
 		} 
		
	}

	private function listContents($lat, $lng, $radius, $type) {
		
		$return = array();
		switch($type) {
			case TYPE_PACK: 
				$return = $this->maps_model->get_pack_items($lat, $lng, $radius);
				break;
			case TYPE_EVENT: 
				$return = $this->maps_model->get_event_items($lat, $lng, $radius);
				break;
			case TYPE_BAR: 
				$return = $this->maps_model->get_bar_items($lat, $lng, $radius);
				break;
			default:
				$return = $this->maps_model->get_all_items($lat, $lng, $radius);
				break;
		}
		
		return $return;
			 
	}

	public function listContentsForMap() {

		$this->display_error();

		$lat 	= $this->input->post('lat') ? $this->input->post('lat') : '12.2323232323';
		$lng 	= $this->input->post('lng') ? $this->input->post('lng') : '12.232323232323';
		$radius = $this->input->post('radius') ? $this->input->post('radius') : 0;
		$radius = (double) $radius / $this->_miles_to_meters;
		$type = $this->input->post('type');
		$type 	= strlen($type) ? $type : -1;
		

		if($this->validate_list_input()) {
			$return = $this->listContents($lat, $lng, $radius, $type);
		} else {
			$return = array('error' => 1, 'message' => strip_tags(validation_errors()));
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($return));
	}

	public function listContentsPack() {
		$this->display_error();
		$lat 	= $this->input->post('lat') ? $this->input->post('lat') : '12.2323232323';
		$lng 	= $this->input->post('lng') ? $this->input->post('lng') : '12.232323232323';
		$radius = $this->input->post('radius') ? $this->input->post('radius') : 0;
		$radius = (double) $radius / $this->_miles_to_meters;
		$this->listContents($lat, $lng, $radius, TYPE_PACK);
	}

	public function listContentsEvents() {
		$this->display_error();
		$lat 	= $this->input->post('lat') ? $this->input->post('lat') : '12.2323232323';
		$lng 	= $this->input->post('lng') ? $this->input->post('lng') : '12.232323232323';
		$radius = $this->input->post('radius') ? $this->input->post('radius') : 0;
		$radius = (double) $radius / $this->_miles_to_meters;
		$this->listContents($lat, $lng, $radius, TYPE_EVENT);
	}

	public function listContentsBar() {
		$this->display_error();
		$lat 	= $this->input->post('lat') ? $this->input->post('lat') : '12.2323232323';
		$lng 	= $this->input->post('lng') ? $this->input->post('lng') : '12.232323232323';
		$radius = $this->input->post('radius') ? $this->input->post('radius') : 0;
		$radius = (double) $radius / $this->_miles_to_meters;
		$this->listContents($lat, $lng, $radius, TYPE_BAR);
	}

	private function validate_list_input($is_update = false) {

		$this->load->library('form_validation');

		$rules = array(
			   array(
					 'field'   => 'lat',
					 'label'   => 'lat',
					 'rules'   => 'decimal'
				  ),
			   array(
					 'field'   => 'lng',
					 'label'   => 'lng',
					 'rules'   => 'decimal'
				  ),
			   array(
					 'field'   => 'radius',
					 'label'   => 'radius',
					 'rules'   => 'decimal'
				  ),
			   array(
					 'field'   => 'user_id',
					 'label'   => 'user_id',
					 'rules'   => 'numeric'
				  ),
			   array(
					 'field'   => 'id',
					 'label'   => 'id',
					 'rules'   => 'numeric'
				  ),
			   array(
					 'field'   => 'name',
					 'label'   => 'name',
					 'rules'   => 'xss_clean'
				  ),
			   array(
					 'field'   => 'type',
					 'label'   => 'type',
					 'rules'   => 'numeric'
				  )
			);	
		
 
		 $this->form_validation->set_rules($rules);
		
		 return $this->form_validation->run();
 	}

		

}

?>
