<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Events extends CI_Controller {

	var $_miles_to_meters;
	public function __construct()
	{
		parent::__construct();

		$this->load->model('mobile_model');
		$this->_miles_to_meters = 1609.34;
		
	}

	public function display_error() {
		if(!$this->login_model->check()) {
			$return = array('error' => 1,
							'message' => 'Invalid app id or secret');
			$this->output->set_content_type('application/json')->_display(json_encode($return));
			exit;
		}

		if(!$this->validate_list_input()) {
 			$return = array('error' => 1,
							'message' => strip_tags(validation_errors()));
			$this->output->set_content_type('application/json')->_display(json_encode($return));
			exit;	
 		} 
		
	}

	private function listContents($lat, $lng, $radius, $type, $user_id = 0) {
		
		$return = array();
		switch($type) {
			case TYPE_BUY: 
				$return = $this->mobile_model->get_buy_items($lat, $lng, $radius, $user_id);
				break;
			case TYPE_BID: 
				$return = $this->mobile_model->get_bid_items($lat, $lng, $radius, $user_id);
				break;
			case TYPE_BUYBID:
				$return = $this->mobile_model->get_buy_bid_items($lat, $lng, $radius, $user_id);
				break;
			case TYPE_MOVEFWD:
				$return = $this->mobile_model->get_movefwd_items($lat, $lng, $radius);
				break;
			case TYPE_EVENT:
				$return = $this->mobile_model->get_event_items($lat, $lng, $radius);
				break;
			case TYPE_GLIST:
				$return = $this->mobile_model->get_glist_items($lat, $lng, $radius);
				break;
			default:
				$return = $this->mobile_model->get_all_items($lat, $lng, $radius);
				break;
		}
		
		return $return;
			 
	}

	public function listContentsForList() {

		$this->display_error();

		$lat 		= $this->input->post('lat') ? $this->input->post('lat') : '12.2323232323';
		$lng 		= $this->input->post('lng') ? $this->input->post('lng') : '12.232323232323';
		$radius 	= $this->input->post('radius') ? $this->input->post('radius') : 0;
		$radius 	= (double) $radius / $this->_miles_to_meters;
		$type 		= $this->input->post('type');
		$type 		= strlen($type) ? $type : -1;
		$user_id 	= $this->input->post('user_id');

		if($this->validate_list_input()) {
			$return = $this->listContents($lat, $lng, $radius, $type, $user_id);	
		} else {
			$return = array('error' => 1, 'message' => strip_tags(validation_errors()));
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($return));
	}

	public function listContentsEvents() {

		$this->display_error();

		$lat 	= $this->input->post('lat') ? $this->input->post('lat') : '12.2323232323';
		$lng 	= $this->input->post('lng') ? $this->input->post('lng') : '12.232323232323';
		$radius = $this->input->post('radius') ? $this->input->post('radius') : 0;
		$radius = (double) $radius / $this->_miles_to_meters;
		$this->listContents($lat, $lng, $radius, TYPE_EVENT);
	}

	public function listContentsGlist() {

		$this->display_error();

		$lat 	= $this->input->post('lat') ? $this->input->post('lat') : '12.2323232323';
		$lng 	= $this->input->post('lng') ? $this->input->post('lng') : '12.232323232323';
		$radius = $this->input->post('radius') ? $this->input->post('radius') : 0;
		$radius = (double) $radius / $this->_miles_to_meters;
		$this->listContents($lat, $lng, $radius, TYPE_GLIST);
	}

	public function listContentsBuy() {

		$this->display_error();

		$lat 	= $this->input->post('lat') ? $this->input->post('lat') : '12.2323232323';
		$lng 	= $this->input->post('lng') ? $this->input->post('lng') : '12.232323232323';
		$radius = $this->input->post('radius') ? $this->input->post('radius') : 0;
		$radius = (double) $radius / $this->_miles_to_meters;
		$user_id = $this->input->post('user_id') ? $this->input->post('user_id') : '';
		$this->listContents($lat, $lng, $radius, TYPE_BUY, $user_id);
	}

	public function listContentsBid() {

		$this->display_error();

		$lat 	= $this->input->post('lat') ? $this->input->post('lat') : '12.2323232323';
		$lng 	= $this->input->post('lng') ? $this->input->post('lng') : '12.232323232323';
		$radius = $this->input->post('radius') ? $this->input->post('radius') : 0;
		$radius = (double) $radius / $this->_miles_to_meters;
		$user_id = $this->input->post('user_id') ? $this->input->post('user_id') : '';
		$this->listContents($lat, $lng, $radius, TYPE_BID, $user_id);
	}

	public function listContentsBuybid() {
		$this->display_error();
		$lat 	= $this->input->post('lat') ? $this->input->post('lat') : '12.2323232323';
		$lng 	= $this->input->post('lng') ? $this->input->post('lng') : '12.232323232323';
		$radius = $this->input->post('radius') ? $this->input->post('radius') : 0;
		$radius = (double) $radius / $this->_miles_to_meters;
		$user_id = $this->input->post('user_id') ? $this->input->post('user_id') : '';
		$this->listContents($lat, $lng, $radius, TYPE_BUYBID, $user_id);
	}

	public function listContentsMovefwd() {
		$this->display_error();
		$lat 	= $this->input->post('lat') ? $this->input->post('lat') : '12.2323232323';
		$lng 	= $this->input->post('lng') ? $this->input->post('lng') : '12.232323232323';
		$radius = $this->input->post('radius') ? $this->input->post('radius') : 0;
		$radius = (double) $radius / $this->_miles_to_meters;
		$this->listContents($lat, $lng, $radius, TYPE_MOVEFWD);
	}

	public function readContentById() {
		$this->display_error();
		$id = $this->input->post('id');
		$type = $this->input->post('type');
		$user_id = $this->input->post('user_id');

		if(!$this->validate_list_input()) {
			$return = array('error' => 1, 'message' => strip_tags(validation_errors()));
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
			return;
		}

		$return = array();
		if(!$id) {
			$return = array('error' => 1, 'message' => 'No matching records found');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
			return;
		}
			

		$return = array();
		switch($type) {
			case TYPE_BUY: 
				$return = $this->mobile_model->get_buy_item($id);
				break;
			case TYPE_BID: 
				$return = $this->mobile_model->get_bid_item($id);
				break;
			case TYPE_BUYBID:
				$return = $this->mobile_model->get_bid_item($id);
				break;
			case TYPE_MOVEFWD:
				$return = $this->mobile_model->get_movefwd_item($id, $user_id);
				break;
			case TYPE_EVENT:
				$return = $this->mobile_model->get_event_item($id);
				break;
			case TYPE_BAR:
				$return = $this->mobile_model->get_bar_item($id);
				break;
			default:
				$return = $this->mobile_model->get_buy_item($id);
				break;
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($return));
	}


	private function validate_list_input($is_update = false) {

		$this->load->library('form_validation');

		$rules = array(
			   array(
					 'field'   => 'lat',
					 'label'   => 'lat',
					 'rules'   => 'decimal'
				  ),
			   array(
					 'field'   => 'lng',
					 'label'   => 'lng',
					 'rules'   => 'decimal'
				  ),
			   array(
					 'field'   => 'radius',
					 'label'   => 'radius',
					 'rules'   => 'decimal'
				  ),
			   array(
					 'field'   => 'user_id',
					 'label'   => 'user_id',
					 'rules'   => 'numeric'
				  ),
			   array(
					 'field'   => 'id',
					 'label'   => 'id',
					 'rules'   => 'numeric'
				  ),
			   array(
					 'field'   => 'name',
					 'label'   => 'name',
					 'rules'   => 'xss_clean'
				  ),
			   array(
					 'field'   => 'type',
					 'label'   => 'type',
					 'rules'   => 'numeric'
				  )
			);	
		
 
		 $this->form_validation->set_rules($rules);
		
		 return $this->form_validation->run();
 	}

	public function saveGuestList() {
		$this->display_error();
		$id = $this->input->post('id');
		$name = $this->input->post('name');	
		$user_id = $this->input->post('user_id');

		if(!$this->validate_list_input()) {
			$return = array('error' => 1, 'message' => strip_tags(validation_errors()));
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
			return;
		}

		$return = $this->mobile_model->save_glist($id, $user_id, $name);
		$this->output->set_content_type('application/json')->set_output(json_encode($return));
	}

}

?>
