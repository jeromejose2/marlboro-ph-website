<?php include 'header.php';?>
<script>
var baseUrl = '';
</script>
<script type="text/javascript" src="js-parallax/preloadjs-0.4.1.min.js"></script>
	<script type="text/javascript" src="js-parallax/easeljs-0.7.1.min.js"></script>
	<script type="text/javascript" src="js-parallax/tweenjs-0.5.1.min.js"></script>
	<script type="text/javascript" src="js-parallax/premiumApp.js"></script>
	<script>
	</script>
	<style>
		#stage-container {
			width: 100%;
			height: 100%;
		}
			#stage {
				background-color: #000;
			}
			#scroll-container {
				display: none;
				position: absolute;
				width: 100%;
				bottom: 20px;
				text-align: center;
			}
				.scroll-down {
					color: #fff;
					font-family: 'Blair';
					font-size: 12px;
					display: block;
					cursor: default;
				}
			@media all and (min-width: 768px) {
			
			}
			@media all and (max-width: 767px) {
			}

		#preloader-container {
			position: absolute;
			width: 150px;
			background-image: url('images/car-before.png');
			background-repeat: no-repeat;
			margin-left: -75px;
			left: 50%;
			margin-top: 200px;
		}
			#preloader {
				width: 0px;
				height: 48px;
				background-image: url('images/car-after.png');
				background-repeat: no-repeat;
			}
			.preloader-footer {
				width: 300px;
				height: 22px;
				margin-left: -73px;
			}
	</style>		
<section class="main">
	<div class="wrapper fixed parallax">

		<!-- content -->
		<div id="preloader-container">
			<div id="preloader"></div>
			<img class="preloader-footer" src="images/preloader-footer.png" />
		</div>
		<div id="stage-container">
			<canvas id="stage"></canvas>	
		</div>
		<div id="scroll-container">
			<span class="scroll-down">Scroll Down</span>
			<img class="arrow-down" src="images/arrow-down.png" />
		</div>
		<!-- content -->
		
	</div>
</section>

<script>
window.onload = initPremiumApp;
</script>
<?php include 'footer.php';?>

</body>
</html>
