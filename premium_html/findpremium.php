<?php include 'header.php';?>
		
<!-- main content starts here  -->
<section class="main">
	<div class="wrapper find-premium">	
		<div class="container">
					
			<div class="title">
				<img src="images/find-premium-black.png">
				<div class="line-separator"></div>
			</div>

			<ul class="find">
				<li class="head">REGION I</li>
				<li><span>Borough</span>
					<small>The Podium Mall, 12 ADB Avenue, Ortigas Center, Mandaluyong. Tel. No. 570-8906</small>
				</li>
				<li><span>Borough</span>
					<small>The Podium Mall, 12 ADB Avenue, Ortigas Center, Mandaluyong. Tel. No. 570-8906</small>
				</li>
				<li><span>Borough</span>
					<small>The Podium Mall, 12 ADB Avenue, Ortigas Center, Mandaluyong. Tel. No. 570-8906</small>
				</li>
				<li><span>Borough</span>
					<small>The Podium Mall, 12 ADB Avenue, Ortigas Center, Mandaluyong. Tel. No. 570-8906</small>
				</li>
				<li><span>Borough</span>
					<small>The Podium Mall, 12 ADB Avenue, Ortigas Center, Mandaluyong. Tel. No. 570-8906</small>
				</li>
				<li><span>Borough</span>
					<small>The Podium Mall, 12 ADB Avenue, Ortigas Center, Mandaluyong. Tel. No. 570-8906</small>
				</li>
				<li><span>Borough</span>
					<small>The Podium Mall, 12 ADB Avenue, Ortigas Center, Mandaluyong. Tel. No. 570-8906</small>
				</li>
				<li><span>Borough</span>
					<small>The Podium Mall, 12 ADB Avenue, Ortigas Center, Mandaluyong. Tel. No. 570-8906</small>
				</li>
			</ul>

			<ul class="find">
				<li class="head">REGION II</li>
				<li><span>Borough</span>
					<small>The Podium Mall, 12 ADB Avenue, Ortigas Center, Mandaluyong. Tel. No. 570-8906</small>
				</li>
				<li><span>Borough</span>
					<small>The Podium Mall, 12 ADB Avenue, Ortigas Center, Mandaluyong. Tel. No. 570-8906</small>
				</li>
				<li><span>Borough</span>
					<small>The Podium Mall, 12 ADB Avenue, Ortigas Center, Mandaluyong. Tel. No. 570-8906</small>
				</li>
				<li><span>Borough</span>
					<small>The Podium Mall, 12 ADB Avenue, Ortigas Center, Mandaluyong. Tel. No. 570-8906</small>
				</li>
				<li><span>Borough</span>
					<small>The Podium Mall, 12 ADB Avenue, Ortigas Center, Mandaluyong. Tel. No. 570-8906</small>
				</li>
				</li>
			</ul>

			<ul class="find">
				<li class="head">REGION III</li>
				<li><span>Borough</span>
					<small>The Podium Mall, 12 ADB Avenue, Ortigas Center, Mandaluyong. Tel. No. 570-8906</small>
				</li>
				<li><span>Borough</span>
					<small>The Podium Mall, 12 ADB Avenue, Ortigas Center, Mandaluyong. Tel. No. 570-8906</small>
				</li>
				<li><span>Borough</span>
					<small>The Podium Mall, 12 ADB Avenue, Ortigas Center, Mandaluyong. Tel. No. 570-8906</small>
				</li>
			</ul>
				
		</div>

	</div>

</section>
<!-- main content ends here  -->
<script>
$(function(){
	$('ul.find li').click(function(){
		var el = $(this);

		el.toggleClass('active');
		el.siblings().removeClass('active');
	})
});
</script>
<?php include 'footer.php';?>
</body>
</html>
