<?php

require_once 'application/helpers/site_helper.php';

class Edm
{
	//FOR EACH LIST YOU NEED TO SPECIFY ALL THE PARAMETERS IN THIS AREA AND AS WELL ADD THOSE PARAMETERS IN THE $DATA XML
	public function add_recipient($list_id, $email, $user_name, $first_name, $last_name, $password)
	{
		// $list_id = "3308474"; // LIST ID HERE. MAKE SURE ALL OF THE PARAMETERS ARE ADDED ABOVE AND BELOW THIS FUNCTION
		$sock = fsockopen(EDM_HOST, EDM_PORT, $errno, $errstr, EDM_TIMEOUT);
		$data = "xml=<?xml version=\"1.0\"?><Envelope><Body>";
		$data .= "<Login><USERNAME>".EDM_ADMIN_USERNAME."</USERNAME>";
		$data .= "<PASSWORD>".EDM_ADMIN_PASSWORD."</PASSWORD></Login>";
		$data .= "<AddRecipient><LIST_ID>".$list_id."</LIST_ID>";
		$data .= "<CREATED_FROM>1</CREATED_FROM><UPDATE_IF_FOUND>true</UPDATE_IF_FOUND>";
		$data .= "<COLUMN><NAME>EMAIL</NAME><VALUE>".$email."</VALUE></COLUMN>";
		$data .= "<COLUMN><NAME>FIRST_NAME</NAME><VALUE>".$first_name."</VALUE></COLUMN>";
		$data .= "<COLUMN><NAME>PASSWORD</NAME><VALUE>".$password."</VALUE></COLUMN>";
		$data .= "<COLUMN><NAME>USER_NAME</NAME><VALUE>".$user_name."</VALUE></COLUMN>";
		$data .= "<COLUMN><NAME>LAST_NAME</NAME><VALUE>".$last_name."</VALUE></COLUMN>";
		$data .= "</AddRecipient><Logout/></Body></Envelope>";
		if (!$sock) {
			print("Could not connect to host:". $errno . $errstr);
			return false;
		}
		$size = strlen($data);
		fputs ($sock, "POST /servlet/".EDM_SERVLET." HTTP/1.1\n");
		fputs ($sock, "Host: ".EDM_HOST."\n");
		fputs ($sock, "Content-type: application/x-www-form-urlencoded\n");
		fputs ($sock, "Content-length: ".$size."\n");
		fputs ($sock, "Connection: close\n\n");
		fputs ($sock, $data);
		$buffer = "";
		while (!feof($sock)) {
			$buffer .= fgets($sock);
		}
		fclose($sock);
		return $buffer;
	}

	public function send($mailid, $email, $username, $firstname, $password)
	{
		// $email = "mark.lupango@ph.arcww.com";
		// $firstname = "bummi aja";
		// $username = "impstrg@yahoo.com";
		// $password = "12312e3r3frrtert2343rtw";
		// // $list_id = 3308474;
		// $mailid = 5471141;

		$sock = fsockopen(EDM_HOST, EDM_PORT, $errno, $errstr, EDM_TIMEOUT); // open socket on port 80 w/ timeout of 20
		$data = "xml=<?xml version=\"1.0\" encoding=\"UTF-8\" ?><Envelope><Body>";
		$data .= "<Login>";
		$data .= "<USERNAME>".EDM_ADMIN_USERNAME."</USERNAME>";
		$data .= "<PASSWORD>".EDM_ADMIN_PASSWORD."</PASSWORD>";
		$data .= "</Login>";
		$data .= "<SendMailing>
		<MailingId>".$mailid."</MailingId>
		<RecipientEmail>".$email."</RecipientEmail>
			<COLUMN>
				<NAME>EMAIL</NAME>
				<VALUE>".$email."</VALUE>
			</COLUMN>
			<COLUMN>
				<NAME>FIRST_NAME</NAME>
				<VALUE>".$firstname."</VALUE>
			</COLUMN>
			<COLUMN>
				<NAME>USER_NAME</NAME>
				<VALUE>".$username."</VALUE>
			</COLUMN>
			<COLUMN>
				<NAME>PASSWORD</NAME>
				<VALUE>".$password."</VALUE>
			</COLUMN>
		</SendMailing></Body>
		</Envelope>";
		if (!$sock) {
			print("Could not connect to host:".$errno.$errstr);
			return false;
		}
		$size = strlen($data);
		fputs($sock, "POST /servlet/".EDM_SERVLET." HTTP/1.1\n");
		fputs($sock, "Host: ".EDM_HOST."\n");
		fputs($sock, "Content-type: application/x-www-form-urlencoded\n");
		fputs($sock, "Content-length: ".$size."\n");
		fputs($sock, "Connection: close\n\n");
		fputs($sock, $data);
		$buffer = "";
		while (!feof($sock)) {
			$buffer .= fgets($sock);
		}
		fclose($sock);

		$xml = stristr($buffer, '<Envelope>');
		if ($xml !== false) {
			$xml = stristr_reverse($xml, '</Envelope>');
			$xml = new SimpleXMLElement($xml);
			$error = array();
			foreach ($xml->Body->children() as $child) {
				if ($child->getName() == 'Fault' && isset($child->FaultString)) {
					$error[] = (string) $child->FaultString;
				}
			}
			if ($error) {
				return $error;
			}
		}
		return true;
	}

	//FOR EACH LIST YOU NEED TO SPECIFY ALL THE PARAMETERS IN THIS AREA AND AS WELL ADD THOSE PARAMETERS IN THE $DATA XML
	public function add_recipient_for_tracking_code($list_id, $email, $first_name, $last_name, $tracking_code)
	{
		// $list_id = "3308474"; // LIST ID HERE. MAKE SURE ALL OF THE PARAMETERS ARE ADDED ABOVE AND BELOW THIS FUNCTION
		$sock = fsockopen(EDM_HOST, EDM_PORT, $errno, $errstr, EDM_TIMEOUT);
		$data = "xml=<?xml version=\"1.0\"?><Envelope><Body>";
		$data .= "<Login><USERNAME>".EDM_ADMIN_USERNAME."</USERNAME>";
		$data .= "<PASSWORD>".EDM_ADMIN_PASSWORD."</PASSWORD></Login>";
		$data .= "<AddRecipient><LIST_ID>".$list_id."</LIST_ID>";
		$data .= "<CREATED_FROM>1</CREATED_FROM><UPDATE_IF_FOUND>true</UPDATE_IF_FOUND>";
		$data .= "<COLUMN><NAME>EMAIL</NAME><VALUE>".$email."</VALUE></COLUMN>";
		$data .= "<COLUMN><NAME>FIRST_NAME</NAME><VALUE>".$first_name."</VALUE></COLUMN>";
		$data .= "<COLUMN><NAME>LAST_NAME</NAME><VALUE>".$last_name."</VALUE></COLUMN>";
		$data .= "<COLUMN><NAME>TRACKING_CODE</NAME><VALUE>".$tracking_code."</VALUE></COLUMN>";
		$data .= "</AddRecipient><Logout/></Body></Envelope>";
		if (!$sock) {
			print("Could not connect to host:". $errno . $errstr);
			return false;
		}
		$size = strlen($data);
		fputs ($sock, "POST /servlet/".EDM_SERVLET." HTTP/1.1\n");
		fputs ($sock, "Host: ".EDM_HOST."\n");
		fputs ($sock, "Content-type: application/x-www-form-urlencoded\n");
		fputs ($sock, "Content-length: ".$size."\n");
		fputs ($sock, "Connection: close\n\n");
		fputs ($sock, $data);
		$buffer = "";
		while (!feof($sock)) {
			$buffer .= fgets($sock);
		}
		fclose($sock);
		return $buffer;
	}

	public function send_tracking_code($mailid, $email, $firstname, $tracking_code)
	{
		// $email = "mark.lupango@ph.arcww.com";
		// $firstname = "bummi aja";
		// $username = "impstrg@yahoo.com";
		// $password = "12312e3r3frrtert2343rtw";
		// // $list_id = 3308474;
		// $mailid = 5471141;

		$sock = fsockopen(EDM_HOST, EDM_PORT, $errno, $errstr, EDM_TIMEOUT); // open socket on port 80 w/ timeout of 20
		$data = "xml=<?xml version=\"1.0\" encoding=\"UTF-8\" ?><Envelope><Body>";
		$data .= "<Login>";
		$data .= "<USERNAME>".EDM_ADMIN_USERNAME."</USERNAME>";
		$data .= "<PASSWORD>".EDM_ADMIN_PASSWORD."</PASSWORD>";
		$data .= "</Login>";
		$data .= "<SendMailing>
		<MailingId>".$mailid."</MailingId>
		<RecipientEmail>".$email."</RecipientEmail>
			<COLUMN>
				<NAME>EMAIL</NAME>
				<VALUE>".$email."</VALUE>
			</COLUMN>
			<COLUMN>
				<NAME>FIRST_NAME</NAME>
				<VALUE>".$firstname."</VALUE>
			</COLUMN>
			<COLUMN>
				<NAME>TRACKING_CODE</NAME>
				<VALUE>".$tracking_code."</VALUE>
			</COLUMN>
		</SendMailing></Body>
		</Envelope>";
		if (!$sock) {
			print("Could not connect to host:".$errno.$errstr);
			return false;
		}
		$size = strlen($data);
		fputs($sock, "POST /servlet/".EDM_SERVLET." HTTP/1.1\n");
		fputs($sock, "Host: ".EDM_HOST."\n");
		fputs($sock, "Content-type: application/x-www-form-urlencoded\n");
		fputs($sock, "Content-length: ".$size."\n");
		fputs($sock, "Connection: close\n\n");
		fputs($sock, $data);
		$buffer = "";
		while (!feof($sock)) {
			$buffer .= fgets($sock);
		}
		fclose($sock);
		return $buffer;
	}
}