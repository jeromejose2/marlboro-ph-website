<!DOCTYPE html>
<html lang="en">
<head>
	<title>Marlboro</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo BASE_URL . ASSETS_DIR ?>css/bootstrap.css">
	<link rel="stylesheet/less" type="text/css" href="<?php echo BASE_URL . ASSETS_DIR ?>css/mainstyle.css?r=<?=rand()?>">
	<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL . ASSETS_DIR ?>css/lytebox.css">
	<script src="<?php echo BASE_URL . ASSETS_DIR ?>js/less.js"></script>
	<script src="<?php echo BASE_URL . ASSETS_DIR ?>js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo BASE_URL . ASSETS_DIR ?>js/lytebox.2.2.js"></script>
	<body>
	<header class="main">
		<div class="container">
			<a class="logo" href="index.php"><span class="sr-only">MARLBORO LOGO</span></a>
			<button class="navbar-toggle"  data-toggle="collapse" data-target="#nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="collapse navbar-collapse" id="nav-collapse">
				<ul class="main-nav nav navbar-nav">
					<!-- <li><a href="<?php echo site_url() ?>">HOME</a></li> -->
					<li><a href="<?php echo site_url() ?>/find-out-more">FIND OUT MORE</a></li>
					<li><a href="<?php echo site_url() ?>/quiz">QUIZ</a></li>
					<!-- <li><a href="<?php echo site_url() ?>/find-premium-black">FIND PREMIUM BLACK</a></li> -->
					<li><a href="<?php echo BASE_URL ?>">RETURN TO MAIN SITE</a></li> 
				</ul>
			</div>
			
		</div>
		
	</header>		

	<?php echo $main_content; ?>

	<footer class="main">
	<div class="r18">
		This website is for adult smokers 18 years or older residing in the Philippines. Access to this website is subject to age verification
	</div>

	<div class="row">
		<nav class="col-md-6 ">
			<a onclick="popup.open({url:'<?php echo BASE_URL?>popups/terms.html'})">terms and conditions</a>
		 	<a onclick="popup.open({url:'<?php echo BASE_URL?>popups/privacy-content.html'})">Privacy Statement and Consent</a> 
		 	<a onclick="popup.open({url:'<?php echo BASE_URL?>popups/contactus.html'})">contact us</a>
		 	<a>smoking and health</a>
		</nav>

		<div class="col-md-6 copyright">copyright &copy; 2013 PMFTC INC. All rights reserved</div>
	</div>
	<div class="government-warning">
		<span>GOVERNMENT WARNING: CIGARETTE SMOKING IS DANGEROUS TO YOUR HEALTH</span>
	</div>
</footer>


<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo BASE_URL . ASSETS_DIR ?>js/bootstrap.min.js"></script>
<script src="<?php echo BASE_URL . ASSETS_DIR ?>js/exec.js?v=<?=rand()?>"></script>


<!--[if lt IE 9]>
	<script src="js/shiv.js"></script>
<![endif]-->