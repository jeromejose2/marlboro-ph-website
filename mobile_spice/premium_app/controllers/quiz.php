<?php
class Quiz extends CI_Controller
{
	function __construct() {
		parent::__construct();
		$this->load->model('quiz_model');
		
	}

	function index() {
		$data['main_content'] = $this->main_content();
		$this->load->view('template', $data);
	}

	function main_content() {
		$quizzes = $this->quiz_model->get_quizzes();
		$quiz_arr = array();
		if($quizzes) {
			$cur_quiz = $quizzes[0]['question_id'];
			$choices = array();
			foreach ($quizzes as $key => $value) {
				if($cur_quiz != $value['question_id']) {
					$quizzes[$key-1]['choices'] = $choices;
					$choices = array();
					$quiz_arr[] = $quizzes[$key-1];
				} 
				$choices[] = $value;
				$cur_quiz = $value['question_id'];
			}

			$quizzes[$key-1]['choices'] = $choices;
			$quiz_arr[] = $quizzes[$key-1];
		}
		$data['quizzes'] = $quiz_arr;
		$content = $this->load->view('quiz', $data, true);
		return $content;
	}

	function submit() {
		$this->quiz_model->save_quiz();
	}
}