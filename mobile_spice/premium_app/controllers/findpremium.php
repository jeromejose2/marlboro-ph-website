<?php
class Findpremium extends CI_Controller
{
	function __construct() {
		parent::__construct();
		$this->load->model('Fp_model');
	}

	function index() {
		if(!$this->session->userdata('user_id'))
			redirect(BASE_URL);
		$data['main_content'] = $this->main_content();
		$this->load->view('template', $data);
	}

	function main_content() {
		$stores = $this->Fp_model->get_stores();
		$data['stores'] = $stores;
		$content = $this->load->view('findpremium', $data, true);
		return $content;
	}
}