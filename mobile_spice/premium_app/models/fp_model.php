<?php

class Fp_model extends CI_Model
{
	function get_stores() {
		$regions = array('NCR', 'North and Central Luzon', 'South Luzon', 'Visayas', 'Mindanao');
		$stores = array();
		if($regions) {
			foreach ($regions as $kr => $region) {
				$stores[$kr]['region'] = $region;
				$stores[$kr]['stores'] = $this->db->select('*')
											 ->from('tbl_stores s')
											 ->where('region', $region)
											 ->order_by('s.region')
											 ->order_by('s.store_name')
											 ->get()
											 ->result_array();	
			}
		}
		return $stores;

	}
}