<?php

class Fp_model extends CI_Model
{
	function get_stores() {
		//$regions = array('NCR', 'North Central Luzon', 'Visayas', 'Mindanao', 'Stag');
		$regions = $this->db->select('*')
							->from('tbl_store_regions r')
							->order_by('r.order')
							->get()
							->result_array();

		$stores = array();
		if($regions) {
			foreach ($regions as $kr => $region) {
				$stores[$kr]['region'] = $region['region'];
				$stores[$kr]['stores'] = $this->db->select('*')
											 ->from('tbl_stores s')
											 ->where('region', $region['store_region_id'])
											 ->order_by('s.store_name')
											 ->get()
											 ->result_array();	
			}
		}
		return $stores;

	}
}