<?php

class Quiz_model extends CI_Model
{
	function get_quizzes() {
		$quizzes = $this->db->select('*')
							->from('tbl_premium_quiz_choices')
							->join('tbl_premium_quiz_questions', 'tbl_premium_quiz_questions.question_id=tbl_premium_quiz_choices.question_id', 'left')
							->order_by('tbl_premium_quiz_questions.order')
							->order_by('tbl_premium_quiz_choices.choice_label')
							->get()
							->result_array();
		return $quizzes;
	}

	function save_quiz() {
		$param['registrant_id'] = $this->session->userdata('user_id'); 
		$param['result'] = $this->input->post('result');
		$this->db->insert('tbl_premium_user_quiz', $param);
		$quiz_id = $this->db->insert_id();
		if($this->input->post('data')) {
			foreach ($this->input->post('data') as $key => $value) {
				$split = explode('_', $value);
				$param = array();
				$param['user_quiz_id'] = $quiz_id;
				$param['choice_id'] = $this->get_choice_id($split[0], $split[1]);
				$this->db->insert('tbl_premium_user_answers', $param);
			}
		}
	}

	function get_choice_id($question_id, $choice) {
		$quizzes = $this->get_quizzes();
		if($quizzes) {
			foreach ($quizzes as $key => $value) {
				if($value['choice_label'] == $choice && $value['question_id'] == $question_id) 
					return $value['choice_id'];
			}
		}
		return 0;
	}
}