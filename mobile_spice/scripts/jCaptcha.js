// JavaScript Document
(function( $ ) {
  $.fn.jCaptcha = function() {
  	var selector = this;
	$.get('captcha/api.php?captcha_img',function(result){
		selector.html(result);
		$('.refreshCaptcha').click(function(){
			selector.jCaptcha();
		});
	});

  };
})( jQuery );

function validateCode(a,b){
	var status = false;
	$.post('captcha/api.php',{a:a,b:b},function(response){
		response = response.replace(/^\s+|\s+$/g, '');

		$('#captcha_stat').val(response);
		
		if(response=='wait' && b!=''){
			status='<img src="captcha/preloader.gif">';
		}
		else if(response=='false'){
			status='<img src="captcha/cross.png">';
		}
		else if(response=='true'){
			status='<img src="captcha/check.png">';
		}
		
		$('.captcha-status').html(status);
	});
}