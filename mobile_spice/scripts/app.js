var El = {
	points : $("#points"),
	newsfeed : $("#newsfeed-list"),
	newsfeedContent : $('#newsfeed-content'),
	newsfeedBox : $('#newsfeed-content .newsfeed-box'),
	newsfeedToggle : $("#newsfeed-toggle"),
	notificationsLink : $("#notifications-link"),
	notificationsUnread : $("#notifications-unopened-num"),
	aboutVideoEntries : $(".about-video-entry"),
	aboutNewsEntries : $(".about-news-entry")
};

User.signedOut( function() {
	popup.open({
		title : "Expired session", 
		message : "The page will reload", 
		onClose : function() {
			window.location = BASE_URL + "user/login";
		},
		onOpen : function() {
			setTimeout( function() {
				window.location = BASE_URL + "user/login";
			}, 3000);
		}
	});
});

User.multipleSessionDetected( function() {
	popup.open({
		title : "Multiple session detected", 
		message : "The page will reload", 
		onClose : function() {
			window.location = BASE_URL + "user/login";
		},
		onOpen : function() {
			setTimeout( function() {
				window.location = BASE_URL + "user/login";
			}, 3000);
		}
	});
});

Task.notify( function(response) {
	if (!response.error) {
		El.points.text(response.points);
		if (response.notifications) {
			El.notificationsLink.addClass("notify");
		} else {
			El.notificationsLink.removeClass("notify");
		}
		if (El.notificationsUnread.length && response.notifications) {
			El.notificationsUnread.text("(" + response.notifications + ")");
		} else if (!response.notifications) {
			El.notificationsUnread.text("");
		}
	}
});

El.newsfeedToggle.click( function() {

	if ($(this).hasClass("collapsed"))
		return;

	El.newsfeed.css("height", 60)
		.html("<li><center><b>Loading...</b></center></li>");

	User.getNewsfeed( function(feeds) {

		El.newsfeed.css("height", "")
			.html("");
		for (var i in feeds) {
			El.newsfeed.append("<li>" + feeds[i] + "</li>");
		}
		El.newsfeedBox.jScrollPane({
			showArrows: true
		});

	});

});

El.aboutVideoEntries.click( function(e) {
	var self = $(this);
	popup.loading();
	GA.sendEvent({
		category : "About Videos",
		action : "click",
		label : "About Videos #" + self.data("id") + ": " + self.data("title"),
		value : 1,
		callback : function() {
			setTimeout( function() {
				window.location = self.data("href");
			}, 300);
		}
	});
});

El.aboutNewsEntries.click( function(e) {
	var self = $(this);
	popup.loading();
	GA.sendEvent({
		category : "About News",
		action : "click",
		label : "About News #" + self.data("id") + ": " + self.data("title"),
		value : 1,
		callback : function() {
			setTimeout( function() {
				window.location = self.data("href");
			}, 300);
		}
	});
});