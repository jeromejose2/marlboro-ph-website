
$(function(){
	mainLayout()
	$(window).resize(mainLayout);


	if($(window).width() < 991){
		$('.navbar-toggle').click(function(){
			var newsfeed = $('.newsfeed');
			if(newsfeed.hasClass('in'))
			newsfeed.collapse('hide');
		});

		$('#newsfeed-toggle').click(function(){
			var nav = $('#nav-collapse');
			if(nav.hasClass('in'))
			nav.collapse('hide');
		});
	}
	$('.newsfeed .close').click(function(){
		var newsfeed = $('.newsfeed');
		if(newsfeed.hasClass('in'))
		newsfeed.collapse('hide');
	});

	$('footer.main, header.main').on('touchmove',function(e){
		e.preventDefault(); 
	});

	/*execute scrollbars*/
	
	if($(window).width() > 991){
		var newsfeedScroll = false;

		// $('#newsfeed-toggle').click(function(){
		// 	if(!newsfeedScroll){
		// 		setTimeout(function(){
		// 			$('#newsfeed-content').jScrollPane();
		// 			newsfeedScroll = true;
		// 		},100)
		// 	}
		// })
	}

	
});


var windowSize = 0;
function mainLayout(){

	var win = $(window);
	var doc = $(document);
	var body = $('body');
	var wrapper = $('.wrapper');
	var wrapperFixed = $('.wrapper.fixed, .main.fixed');
	var footer = $('footer.main');
	var header = $('header.main');
	var section = $('section.main');
	var govwarn = $('.government-warning');
	var winHeight = win.height();
	var docHeight = doc.height();
	var per15 = winHeight*0.15;

	
	/* mobile */
	govwarn.height( per15 );
	body.css({paddingBottom : per15 });

	var footHeight = footer.outerHeight();
	var headHeight = header.outerHeight();


	if(windowSize == winHeight ) return;

	windowSize = winHeight;
	
	// if(win.width() < 768 ){
	// 	body.css({paddingBottom : footHeight });
	// }

	/*tablet*/
	// if(win.width() >= 768){
		var offset = win.width() < 992 ? 0 : 40;

		var containerHeight = parseInt(body.css('padding-bottom')) + parseInt(body.css('padding-top'));
			containerHeight =  win.height() - containerHeight;
			containerHeight = containerHeight-offset;
			containerHeight =  win.width() > 767 ? containerHeight < 520 ? 520 : containerHeight : containerHeight;
			

		wrapper.css({ minHeight: containerHeight});
		wrapperFixed.css({ height: containerHeight});
	// }

	//$('header, footer').fadeTo(0,0.3)
}


function swipePrevNext(){
	$("figure.theater").swipe( {
		swipeLeft :function() {
			$('.arrow.right',this).trigger('click');
		},
		swipeRight :function() {
			$('.arrow.left',this).trigger('click');
		},
	 	threshold:100
	});
}

(function () {
  "use strict";
  $ = this.jQuery || require('jquery');
  
  function offsetRelative(selector) {
    var $el = this;
    var $parent = $el.parent();
    if (selector) {
      $parent = $parent.closest(selector);
    }
 
    var elOffset     = $el.offset();
    var parentOffset = $parent.offset();
 
    return {
      left: elOffset.left - parentOffset.left,
      top: elOffset.top - parentOffset.top
    };
  }
  
  // Exports
  $.fn.offsetRelative || ($.fn.offsetRelative = offsetRelative);
  if (typeof module !== "undefined" && module !== null) {
    module.exports = $.fn.offsetRelative;
  }
}).call(this);