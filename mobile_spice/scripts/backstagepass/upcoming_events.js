(function(window, document, $){
	
	'use strict';

	var backstage_pass = window.backstage_pass = {};

	backstage_pass.upcoming_events = {

		init: function(obj){
			this.properties = obj;
  			this.retrieveData();
  			this.bindEvents();
  			this.datePickers();
 		},
		httpRequest: function(obj){
			$.ajax(obj);
		},
		getfilters: function(){
			var that = this;
	    	var data = $('#filters').serialize()+'&track_group='+that.properties.track_group;
			return data;
		},
		retrieveData: function(){
			
			var data = this.getfilters();
			var that = this;
  
 			this.httpRequest({
			 	url: this.properties.url,
			 	data:data,
			 	dataType: 'json',
			 	beforeSend: function(){
 			 		that.properties.retrieving_data = true;
			 	},
			 	success: function(data){	
 			 		that.renderPage(data);
			 		that.properties.retrieving_data = false;
			 		that.properties.track_group++;
			 	}
			 });

		},
		renderPage: function(data){

			var tpl = jQuery(this.properties.handlebar_template).html();
			var template = Handlebars.compile(tpl);
			var rows = this.properties.rows;
			var index = (rows.length > 0) ? parseInt(rows.length) : 0;

			for(var i in data){
				rows[index] = data[i];
				index++; 				
			}

			this.properties.rows = rows;
 			 
 			$(this.properties.template_content).html(template({albums:this.properties.rows}));
			this.parseScrool();

		},
		parseScrool: function(){

			var that = this;
			$(window).scroll(function(){
 
			 	if($(window).scrollTop() == $(document).height() - $(window).height() && that.isScrolledIntoView(that.properties.load_more)==true && that.properties.retrieving_data == false && that.properties.track_group < that.properties.total_group)
			    {
			    	that.retrieveData();
			    }

			});
		},
		isScrolledIntoView: function(elem){

			var docViewTop = $(window).scrollTop();
		    var docViewBottom = docViewTop + $(window).height();

		    var elemTop = $(elem).offset().top;
		    var elemBottom = elemTop + $(elem).height();

		    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));

		},
		bindEvents: function(){

			var that = this; 

			$('select').on('change',function(){	
				that.filterRows();
			});

		}, 
		filterRows: function(){

			var that = this; 

			that.properties.rows = [];
		    that.properties.track_group = 0;
		    that.retrieveData();
			 
			that.httpRequest({
			 	url:that.properties.url,
			 	data:$.extend(that.getfilters(),{total_rows:1}),
			 	dataType: 'json',
			 	success: function(data){
			 		that.properties.total_group = data.total_group;
			 	}
			});

		},
		datePickers: function(){

			var that = this;
			var min_date = new Date();
			min_date.setDate(min_date.getDate()+1);

			$('input[name="start_date"]').datepicker({
				  changeMonth: true,
				  numberOfMonths: 1,
				  dateFormat:'yy-mm-dd',
				  minDate:min_date,
				  onClose: function( selectedDate ) {
					$( 'input[name="end_date"]').datepicker( "option", "minDate", selectedDate );
 				  },
 				  onSelect: function(data){
  				  	that.filterRows();
 				  }
			});

			$('input[name="end_date"]').datepicker({
				  defaultDate: "+1w",
				  changeMonth: true,
				  numberOfMonths: 1,
				  dateFormat:'yy-mm-dd',
				  onClose: function( selectedDate ) {
					$('input[name="start_date"]').datepicker( "option", "maxDate", selectedDate );
	 			  },
 				  onSelect: function(data){
  				  	that.filterRows();
 				  }
			});

		},
		markGooglecalendar: function(token){

			var that = this;
			this.filterRows();

			this.httpRequest({
			 	url:BASE_URL+'backstage_pass/mark_google_calendar',
			 	data:{token:token,event_id:$('input[name="event_id"]').val()},
			 	type: 'post',
			 	dataType: 'json',
			 	beforeSend: function(){
			 		$('.mark-my-calendar').html('<i><img src="'+BASE_URL+'images/spinner.gif" /> Please wait...</i>');
			 	},
			 	success: function(data){
			 		that.log(data);
			 		popup.open({type:'alert',message:data.response,buttonAlign:'center',align:'center',okCaption:'CONTINUE',bgClose:false,close:false}); 
 			 	}
			});

		},
		log: function(data){
			console.log(data);
		}

	};	


}(window, document, jQuery));