angular.module('profiling',['ui.sortable'])
.controller('profilingCtrl',['$scope','$http','$parse',function($scope,$http,$parse){

	$scope.user = {};
	$scope.interests = {};
	$scope.selectedInterests = [];
	$scope.categories = [];
	$scope.categoryOrder = [];
   
	$scope.getInterests = function(){

		$http.get('profile/get_interests').success(function(response){
			$scope.interests = response;
		});

	};

	$scope.getMyInterest = function(){

		$http.get('profile/get_myinterest').success(function(response){
			$scope.selectedInterests = response.selectedInterests;
			$scope.categories = response.categories;
			$scope.categoryOrder = response.categoryOrder;
 		});



	};


	$scope.selectInterest = function(name,cat_id,sub_id,key){
		
		var sub_interests = [],sub_key = '',c,action,cat;

		if($scope.user.saving)
			return false;

 		sub_interests = $scope.interests[key].sub_interests;
 		
		angular.forEach(sub_interests,function(interests, i){ // loop sub interests
			
			c=0;
			action = 'add';

			if(sub_id == interests.id){ // sub interest ID
 
				angular.forEach($scope.selectedInterests, function(item, indexSelected){

					
 		 			if(interests.is_selected && item.sub_interest_id == sub_id){

 		   				$scope.selectedInterests.splice(indexSelected,1);
		   				$scope.interests[key].sub_interests[i].is_selected = '';
		   				action = 'remove';

	 				}

		 		});


		 		if(action=='add'){

		 			$scope.selectedInterests.push({interest_name:name,interest_id:cat_id,sub_interest_id:sub_id});
		 			$scope.interests[key].is_selected = 'active';
		 			$scope.interests[key].sub_interests[i].is_selected = 'active';

		 			if($scope.categories.indexOf(cat_id) == -1)
		 				$scope.categories.push(cat_id);

		 			$scope.categoryOrder[$scope.categories.indexOf(cat_id)] = {id:cat_id,name:name};

		 			if($scope.categories.length > 3){
		 				$scope.interests[key].is_selected = '';
		 				$scope.interests[key].sub_interests[i].is_selected = '';
			 			popup.open({type:'alert',message:"<h4>You can select up to 3 interests only.</h4>",buttonAlign:'center',align:'center',okCaption:'CONTINUE',bgClose:false});
			 			$scope.categories.splice($scope.categories.indexOf(cat_id),1);
		 				$scope.categoryOrder.splice($scope.categoryOrder.indexOf(cat_id),1);
		 				$scope.selectedInterests.splice($scope.selectedInterests.length - 1,1);
			 	        return false;
			 		}

		 		}


		 		angular.forEach(sub_interests,function(interest,k){

		 			if(interest.is_selected)
		 				c++;
		 		});

		 		

 
		 		if(c > 2 && $scope.categories.length < 4){

		 			$scope.selectedInterests.splice($scope.selectedInterests.length - 1,1);
		 			$scope.interests[key].sub_interests[i].is_selected = '';		 		     
		 			popup.open({type:'alert',message:"<h4>You can select up to 2 sub interests only.</h4>",buttonAlign:'center',align:'center',okCaption:'CONTINUE',bgClose:false});

		 		}else if(c < 1 && $scope.categories.length < 4){

		 			$scope.interests[key].is_selected = '';
		 			$scope.categories.splice($scope.categories.indexOf(cat_id),1);
		 			$scope.categoryOrder.splice($scope.categoryOrder.indexOf(cat_id),1);

		 		}

		 		return false;

		 	}
		

		});

				  				 

	};


	$scope.removeInterest = function(id){

		$scope.categoryOrder.splice($scope.categories.indexOf(id),1);
		$scope.categories.splice($scope.categories.indexOf(id),1);
 		
 
		angular.forEach($scope.interests,function(val,i){

			if(val.interest_id == id){

				$scope.interests[i].is_selected = '';

				angular.forEach($scope.interests[i].sub_interests,function(value,k){
  					$scope.interests[i].sub_interests[k].is_selected = '';
 				});
				return false;

				
			}

		});


		angular.forEach($scope.selectedInterests, function(item, i){
					
	 			if(item.interest_id == id){
	   				$scope.selectedInterests.splice(i,1);
 				}
 		});


  	};

 	 

	$scope.saveInterests = function(){

	 

		$scope.user.saving = true;

		$http.post('profile/save_interests',{selected:$scope.selectedInterests,order:$scope.categoryOrder}).success(function(response){
			
			$scope.user = response;
			popup.open({type:'alert',message:response.message,buttonAlign:'center',align:'center',okCaption:'CONTINUE',bgClose:false,close:false});
			User.getTopInterest();
				
		});

		 
		

	};
 
	$scope.clearInterests = function(){

		$scope.selectedInterests = [];
		$scope.categories = [];
		$scope.categoryOrder = [];

		angular.forEach($scope.interests,function(val,i){

				$scope.interests[i].is_selected = '';
 				angular.forEach($scope.interests[i].sub_interests,function(val,k){
 						$scope.interests[i].sub_interests[k].is_selected = '';
 				});

 		});

	};

	$scope.dragControlListeners = {
	    accept: function (sourceItemHandleScope, destSortableScope) {return true},
	    itemMoved: function (event) { },
	    orderChanged: function(event) {

	    	$scope.categories = [];

	    	angular.forEach($scope.categoryOrder,function(val,i){
	    		$scope.categories.push(val.id);
	    	});
	    	
	    }
 	};

	 
	$scope.getMyInterest();
	$scope.getInterests();

}])
.config(function($httpProvider) {
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
});