<?php
/**
 * 
 * @author anthony
 * 
 */
class Spice
{
    /**
     * 
     * @var string
     */
    const APP_ID = 1010;

    /**
     * 
     * @var string
     */
    const MARKET_CODE = 'PH';

    /**
     * 
     * @var string
     */
    const API_URL = 'https://sit.mrm-pmi.com:4803/PMI.MRM.Services.RESTService.svc/';

    /**
     * 
     * @var string
     */
    const USER_KEY = '__spice_user';

    /**
     * 
     * @var string
     */
    const TOKEN_KEY = '__spice_token';

    /**
     * 
     * @var string
     */
    const PERSON_ID_KEY = '__spice_person_id';

    /**
     * 
     * @var string
     */
    private $_token;

    /**
     * 
     * @var array
     */
    private $_user;

    /**
     * 
     * @var int
     */
    private $_personId;

    /**
     * 
     * @var string
     */
    private $_error;

    /**
     * 
     * @return void
     */
    public function __construct($sessionKey = null)
    {
        if (!session_id()) {
            session_start();
        }
        if (isset($_SESSION[self::PERSON_ID_KEY])) {
            $this->_personId = $_SESSION[self::PERSON_ID_KEY];
        }
        if (isset($_SESSION[self::USER_KEY])) {
            $this->_user = unserialize($_SESSION[self::USER_KEY]);
        }
        if (isset($_SESSION[self::TOKEN_KEY])) {
            $this->_token = $_SESSION[self::TOKEN_KEY];
        } elseif ($sessionKey) {
            $this->set_session_key($sessionKey);
        }
    }

    /**
     * 
     * @return int
     */
    public function get_person_id()
    {
        return $this->_personId ? $this->_personId : null;
    }

    /**
     * 
     * @param string $key
     * @return Spice
     */
    public function set_session_key($token)
    {
        $this->_token = $token;
        $_SESSION[self::TOKEN_KEY] = $token;
        return $this;
    }

    /**
     * 
     * @return boolean
     */
    public function is_logged_in()
    {
        return $this->_token ? true : false;
    }

    /**
     * 
     * @param string $code
     * @param string $message
     * @return Spice
     */
    private function _set_error($code, $message)
    {
        $this->_error = "TransactionStatus: {$code}.\nTransactionStatusMessage: {$message}\n";
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function error()
    {
        return $this->_error;
    }

    /**
     * 
     * @param string $username
     * @param string $password
     * @return boolean
     */
    public function login($username, $password)
    {
        if ($this->_token) {
            throw new Exception('Currently logged in. Call logout method.');
        }

        $response = $this->create_session($username, $password);
        if ($response->ResponseHeader->TransactionStatus) {
            $this->_set_error(
                $response->ResponseHeader->TransactionStatus,
                $response->ResponseHeader->TransactionStatusMessage
            );
            return false;
        }
        
        $this->set_session_key($response->SessionKey);
        $this->_personId = $response->PersonId;
        $_SESSION[self::PERSON_ID_KEY] = $response->PersonId;
        return true;
    }

    /**
     * 
     * @return boolean
     */
    public function logout()
    {
        if (!$this->_token) {
            throw new Exception('There is no current user. Call login method.');
        }
        
        $response = $this->endSession();
        if ($response->TransactionStatus) {
            $this->_set_error(
                $response->TransactionStatus,
                $response->TransactionStatusMessage
            );
            return false;
        }

        unset($_SESSION[self::USER_KEY], $_SESSION[self::TOKEN_KEY], $_SESSION[self::PERSON_ID_KEY]);
        $this->_token = null;
        $this->_user = null;
        $this->_personId = null;
        return true;
    }

    /**
     * 
     * @return stdClass
     */
    public function get_current_user()
    {
        if ($this->_user) {
            return $this->_user;
        } elseif (!$this->_token || !$this->_personId) {
            return null;
        }

        $user = $this->getPersonById($this->_personId);
        if ($user->MessageResponseHeader->TransactionStatus) {
            $this->_set_error(
                $response->MessageResponseHeader->TransactionStatus,
                $response->MessageResponseHeader->TransactionStatusMessage
            );
        }

        $this->_user = $user->ConsumerProfiles;
        $_SESSION[self::USER_KEY] = serialize($user->ConsumerProfiles);
        return $this->_user;
    }

    /**
     * 
     * @param string $loginName
     * @param string $password
     * @return string
     */
    private function _encrypt_password($loginName, $password)
    {
        if (strlen($loginName) < 30) {
            $loginName = str_pad($loginName, 30, ' ', STR_PAD_RIGHT);
        }
        $loginName = substr($loginName, 0, 30);
        return base64_encode(sha1($password.$loginName, true).$loginName);
    }

    /**
     * 
     * @param string $sessionKey
     * @return array
     */
    private function _create_header($sessionKey = '')
    {
        return [
            'ApplicationId' => self::APP_ID,
            'DeviceId' => '',
            'LanguageCode' => '',
            'MarketCode' => self::MARKET_CODE,
            'SessionKey' => $sessionKey,
            'SubMarketCode' => ''
        ];
    }

    /**
     * 
     * @param string $method
     * @param array $data
     * @return stdClass
     */
    public function api($method, array $data)
    {
        $curl = curl_init(self::API_URL.trim($method, '/').'/');
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_CAINFO, __DIR__.'/spice/AddTrustExternalCARoot.crt');
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-type: application/json',
            'Accept: application/json'
        ]);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        $response = curl_exec($curl);
        if (!$response) {
            return null;
        }

        if (curl_errno($curl)) {
            throw new Exception('CURL Error: '.curl_error($curl));
        }
        curl_close($curl);
        $response = new SimpleXMLElement($response);
        return json_decode(json_encode((array) $response));
    }

    /**
     * 
     * @param array $info
     * @return stdClass
     */
    public function track_action($info)
    {
        $data = [

        ];
        return $this->api('TrackAction', $data);
    }

    /**
     * 
     * @param array $info
     * @return stdClass
     */
    public function manage_person($info)
    {
        $data = [

        ];
        return $this->api('ManagePerson', $data);
    }



    /**
     * 
     * @param array $filters
     * @return stdClass
     */
    public function search_person($filters)
    {
        unset($filters['MessageRequestHeader']);
        if (!isset($filters['ProfileType'])) {
            $filters['ProfileType'] = 'C';
        }
        $data = array_merge(
            ['MessageRequestHeader' => $this->_create_header($this->_token)],
            $filters
        );
        return $this->api('SearchPerson', $data);
    }

    /**
     * 
     * @param string $loginName
     * @param string $password
     * @return stdClass
     */
    public function create_session($loginName, $password)
    {
        $data = [
            'EncryptedPassword' => $this->_encrypt_password($loginName, $password),
            'LoginName' => $loginName,
            'MessageRequestHeader' => $this->_create_header()
        ];
        return $this->api('CreateSession', $data);
    }

    /**
     * 
     * @param string $loginName
     * @param string $password
     * @return stdClass
     */
    public function create_login($loginName, $password)
    {

    }

    /**
     * 
     * @param string $sessionKey
     * @return stdClass
     */
    public function check_session()
    {
        $data = [
            'MessageRequestHeader' => $this->_create_header($this->_token),
            'Source' => ''
        ];
        return $this->api('CheckSession', $data);
    }

    /**
     * 
     * @param string $sessionKey
     * @return stdClass
     */
    public function end_session()
    {
        $data = [
            'ApplicationId' => self::APP_ID,
            'DeviceId' => '',
            'LanguageCode' => '',
            'MarketCode' => self::MARKET_CODE,
            'SessionKey' => $this->_token,
            'SubMarketCode' => ''
        ];
        return $this->api('EndSession', $data);
    }

    /**
     * 
     * @param int $id
     * @param string $type
     * @return stdClass
     */
    public function get_person_by_id($id, $type = 'C')
    {
        $data = [
            'LoginName' => '',
            'MessageRequestHeader' => $this->_create_header($this->_token),
            'PersonId' => (int) $id,
            'ProfileType' => $type
        ];
        return $this->api('GetPerson', $data);
    }

    /**
     * 
     * @param string $loginName
     * @param string $type
     * @return stdClass
     */
    public function get_person_by_login_name($loginName, $type = 'C')
    {
        $data = [
            'LoginName' => $loginName,
            'MessageRequestHeader' => $this->_create_header($this->_token),
            'PersonId' => '',
            'ProfileType' => $type
        ];
        return $this->api('GetPerson', $data);
    }

    /**
     * 
     * @param string $filepath
     * @return stdClass
     */
    public function verify_age($filepath)
    {
        $data = [
            'MessageRequestHeader' => $this->_create_header($this->_token),
            'PersonId' => $this->_personId
        ];
        if ($filepath = realpath($filepath)) {
            // $finfo = finfo_open(FILEINFO_MIME_TYPE);
            // $fileType = finfo_file($finfo, $filepath);
            $data['VerificationImages'] = [
                'IdDocumentImage' => base64_encode(file_get_contents($filepath)),
                'IdDocumentFileType' => strtoupper(pathinfo($filepath, PATHINFO_EXTENSION)),
                'IdDocumentMarketCode' => 'PH'
            ];
            $data['VerificationInfo'] = [
                'VerificationStatus' => 'Pending'
            ];
        }
        return $this->api('AgeVerificationRequest', $data);
    }

    /**
     * 
     * @return array
     */
    public function get_bulk_reference_data()
    {
        $data = [
            'MessageRequestHeader' => $this->_create_header($this->_token),
            'ReturnType' => 'xml',
            'Table' => 'ReferenceData'
        ];

        $return = [];
        $bulk = $this->api('GetBulk', $data);
        if (!is_string($bulk->MessageBody)) {
            return null;
        }
        $body = new SimpleXMLElement($bulk->MessageBody);
        foreach ($body->children() as $b) {
            $k = (string) $b->GroupCode;
            if (!isset($return[$k])) {
                $return[$k] = [];
            }
            $return[$k][] = (array) $b;
        }
        return $return;
    }

    /**
     * 
     * @param array $filters
     * @return stdClass
     */
    public function get_brand($filters = array())
    {
        $data = array_merge(
            ['MessageRequestHeader' => $this->_create_header($this->_token)],
            $filters
        );
        return $this->api('GetBrand', $data);
    }
}