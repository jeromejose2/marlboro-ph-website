<?php

/**
 * 
 * @author anthony
 * 
 */
class NW_Input extends CI_Input
{
	/**
	* Fetch an item from the GET array
	*
	* @access	public
	* @param	string
	* @param	bool
	* @return	string
	*/
	public function get($index = NULL, $xss_clean = FALSE)
	{
		return html_escape(parent::get($index, $xss_clean));
	}

	/**
	* Fetch an item from the POST array
	*
	* @access	public
	* @param	string
	* @param	bool
	* @return	string
	*/
	public function post($index = NULL, $xss_clean = FALSE)
	{
		return html_escape(parent::post($index, $xss_clean));
	}
}