<?php

class Registration_Model extends CI_Model
{
	public function get_mobile_prefixes()
	{
		return $this->db->select()
			->from('tbl_mobile_prefix')
			->get()
			->result_array();
	}

	public function get_brands()
	{
		return $this->db->select()
			->from('tbl_brands')
			->get()
			->result_array();
	}

	public function get_alternate_purchase()
	{
		return $this->db->select()
			->from('tbl_alternate_purchase')
			->get()
			->result_array();
	}

	public function get_giid_types()
	{
		return $this->db->select()
			->from('tbl_giid_types')
			->get()
			->result_array();
	}

	public function get_user_by_login($username)
	{
		return (array) $this->db->select()
				->from('tbl_registrants')
				->where('email_address', $username)
				->get()
				->row();
	}

	public function save_invalid_login($id, $current_attempts)
	{
		if (++$current_attempts >= LOGIN_ATTEMPTS) {
			$this->db->set('date_blocked', date('Y-m-d H:i:s'));
		}
		$this->db->set('login_attempts', 'login_attempts + 1', false);
		$this->db->where('registrant_id', $id);
		$this->db->update('tbl_registrants');
		if (!$this->db->affected_rows()) {
			return false;
		}
		return true;
	}

	public function reset_invalid_login($id)
	{
		$this->db->set('login_attempts', 0);
		$this->db->set('date_blocked', null);
		$this->db->where('registrant_id', $id);
		$this->db->update('tbl_registrants');
		if (!$this->db->affected_rows()) {
			return false;
		}
		return true;
	}

	public function get_provinces()
	{
		return $this->db->select()
			->from('tbl_provinces')
			->get()
			->result_array();
	}

	public function get_province_by_id($id)
	{
		return (array) $this->db->select()
			->from('tbl_provinces')
			->where('province_id', $id)
			->get()
			->row();
	}

	public function get_user_by_id($id)
	{
		return (array) $this->db->select()
			->from('tbl_registrants')
			->where('registrant_id', $id)
			->get()
			->row();
	}

	public function get_city_by_id($id, $province = null)
	{
		$this->db->select()
			->from('tbl_cities')
			->where('city_id', $id);
		if ($province) {
			$this->db->where('province_id', $province);
		}
		return (array) $this->db->get()->row();
	}

	public function get_cities($provinceId = null)
	{
		$this->db->from('tbl_cities');
		if ($provinceId) {
			$this->db->where('province_id', $provinceId);
		}
		return $this->db->get()->result_array();
	}
}