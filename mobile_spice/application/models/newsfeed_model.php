<?php

class Newsfeed_Model extends CI_Model
{
	public function get($limit = 10)
	{
		$feeds = array();
		if ($this->session->userdata('user_id')) {
			$user = (int) $this->session->userdata('user_id');
			$feeds = $this->db->select()
				->from('tbl_points p')
				->where('p.remarks IS NOT NULL')
				->join('tbl_registrants r', 'r.registrant_id = p.registrant_id')
				->order_by('p.date_earned', 'DESC')
				->limit($limit)
				->get()
				->result_array();
			foreach ($feeds as &$feed) {
				if ((int) $feed['registrant_id'] === $user) {
					$feed = str_replace(array('{{ name }}', ' is '), array('You', ' are '), $feed['remarks']);
				} else {
					$feed = str_replace('{{ name }}', $feed['first_name'].' '.$feed['third_name'], $feed['remarks']);
				}
			}
		}
		return $feeds;
	}
}