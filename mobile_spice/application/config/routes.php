<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "welcome";
$route['404_override'] = '';

$route['fonts/(:any)'] = 'assets/fonts/$1';
$route['images/(:any)'] = 'assets/images/$1';
$route['plugins/(:any)'] = 'assets/plugins/$1';
$route['popups/(:any)'] = 'assets/popups/$1';
$route['scripts/(:any)'] = 'assets/scripts/$1';
$route['styles/(:any)'] = 'assets/styles/$1';
$route['swf/(:any)'] = 'assets/swf/$1';
$route['uploads/(:any)'] = 'assets/uploads/$1';


$route['swf/uploader.xml'] = 'profile/swf';

$route['user/swf/uploader.xml'] = 'user/swf';




$route['about'] = "photos";

$route['about/photos'] = "photos";
$route['about/photos/thumbnails/(:any)'] = "photos/thumbnails/$1";
$route['about/photos/content/(:any)'] = "photos/content/$1";
$route['about/photos/content/(:any)/(:num)'] = "photos/content/$1/$1";

$route['about/videos'] = "videos";
$route['about/videos/content/(:any)'] = "videos/content/$1";

$route['about/news'] = "news";
$route['about/news/content/(:any)'] = "news/content/$1";

$route['reset_password'] = 'user/forgot';
$route['submit_giid'] = 'user/tracking_code';
$route['events/past'] = 'backstage_pass/photos';



/* End of file routes.php */
/* Location: ./application/config/routes.php */