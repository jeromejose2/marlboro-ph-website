
<!-- main content starts here  -->
 	<div class="wrapper">
		
		<div class="container main">
			<div class="row">
				<div class="col-lg-12" id="faq-content">
					<!-- content -->
					<h2><?php echo $title; ?></h2>
					<?php echo $faq['description']; ?>
				    <div class="clearfix"></div>
					<!-- end content -->
				</div>
			</div>
		</div>

	</div>


	<script>
	$(function(){

		$('#faq-content a[href^="#"]').click(function(){
			var me = $(this);
			var target = $(me.attr('href'));
			if(!target) return false;
			$('html, body').animate({
				scrollTop : target.offset().top - 120
			})
		})

	});
	</script>

 