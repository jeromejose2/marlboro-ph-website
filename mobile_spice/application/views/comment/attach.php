<div class="popup">
	<div class="content txtcenter">
		<button class="popup-close">&times;<span>CLOSE</span></button>

		<h2>ATTACH PHOTO</h2>
		<div id="comment-error-attach-<?= $origin ?>-<?= $suborigin ?><?= $reply ?>" class="error"></div>
		<div id="comment-browse-<?= $origin ?>-<?= $suborigin ?><?= $reply ?>" class="browse">
			<input type="text" id="comment-browse-input-<?= $origin ?>-<?= $suborigin ?><?= $reply ?>" placeholder="Browse for File (Max file size: 3MB)" readonly="readonly">
		</div>
		<div class="upload-preview-fixed"  id="comment-attach-preview-<?= $origin ?>-<?= $suborigin ?><?= $reply ?>"><span>PHOTO PREVIEW</span></div>

		<button class="button lytebox-ok" id="comment-attach-upload-<?= $origin ?>-<?= $suborigin ?><?= $reply ?>"><i>UPLOAD</i></button>
		
		<p class="terms">Please be reminded that as per the 
			<a href="#" onclick="popup.open({url:'<?=BASE_URL?>/popups/terms.html', onClose: function(){  popup.open({url:'<?=BASE_URL?>comment/attach?origin=<?= $origin ?>&suborigin=<?= $suborigin ?><?= $reply ?>'});} })">Terms and Conditions</a>
		, all photos, images or designs submitted will become the property of PM. 
		Please do not upload or submit any photos, images or designs depicting the act of smoking or showing a lit cigarette or minors 
		(persons less than 18 years old). PM reserves to the right to remove or reject any photo, image or design which it, in its 
		sole and absolute discretion, deems offensive, violates the law or is unsuitable in any other manner. The photo you upload 
		will be subject to moderation.</p>


	</div>
</div>