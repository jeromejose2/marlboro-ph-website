<div class="wrapper fixed">
	<div class="container">
		<div class="leather-content">
			<form autocomplete="off" class="content" id="reset-form" action="<?= BASE_URL ?>user/reset_submit" method="POST">
				<h2>CHECK YOUR EMAIL</h2>
				<p class="uc">An email was sent.</p>
				<br>
				<a href="<?= BASE_URL ?>user/login" class="small">Back to login page</a> <br>
			</div>
		</div>
	</div>
	<br class="clearfix">
</div>