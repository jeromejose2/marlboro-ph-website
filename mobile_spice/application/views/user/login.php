<div class="wrapper registration ">
	<div class="container">
		<div class="login-holder">
			<form autocomplete="off" class="content" onsubmit="return validate(this)" method="POST" action="<?= BASE_URL ?>user/auth">
				<h2>ALREADY REGISTERED?</h2>
				<h3>PLEASE PROVIDE YOUR LOGIN DETAILS BELOW</h3>
				<div id="error"><?= $invalid_message ? '<span>'.$invalid_message.'</span>' : '' ?></div>
				<input autocomplete="off" type="text" name="username" placeholder="Username" class="req">
				<input autocomplete="off" type="password" name="password" placeholder="Password" class="req">
				<br>
				<div class="copy">
					<p><a href="<?= BASE_URL ?>reset_password">Forgot your password?</a></p>

					<p>
						<label><input type="checkbox"  class="req"> I am an adult smoker 18 years or older, residing in the Phlippines.</label>
						<label><input type="checkbox"  class="req"> I have read and understood the <a onclick="popup.load({url:'<?= BASE_URL ?>popups/terms.html'})" href="#">Terms and Conditions</a> and <a onclick="popup.load({url : '<?= BASE_URL ?>popups/privacy-content.html'})" href="#">Privacy Statement and Consent</a>.</label>
					</p>

					<p>Member must tick both statements to login. <br>
					Registered but haven't submitted your documents yet? <a href="<?= BASE_URL ?>submit_giid">Click here</a></p>
				</div>
				<button type="submit" class="button"><i>LOGIN</i></button>
			</div>
		</div>
	</div>
</div>