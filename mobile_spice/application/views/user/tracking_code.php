<div class="wrapper registration">
	<div class="container">
		<div class="leather-content">
			<form autocomplete="off" class="content" onsubmit="return validate(this)" method="POST" action="<?= BASE_URL ?>user/submit_track_code">
				<h2>SUBMIT TRACKING CODE</h2>
				<p class="uc">ENTER THE TRACKING CODE SENT TO YOUR E-MAIL<br> TO SUBMIT YOUR VALID GOVERNMENT-ISSUED ID</p>
				<div id="error"><?= $invalid ? '<span>'.$invalid.'</span>' : '' ?></div>
				<input autocomplete="off" type="text" name="code" placeholder="Tracking Code" class="req">
				<br>
				<a href="<?= BASE_URL ?>user/login" class="small">Back to login page</a> <br><br>
				<button type="submit" class="button"><i>SUBMIT</i></button>
			</div>
		</div>
	</div>
	<br class="clearfix">
</div>
<script type="text/javascript">
function validate(form){
	var error = false;
	$('.req',form).each(function(){
		var el = $(this);
		var val = $.trim(el.val());
		if( val == '' ){
			error = 'Please input your tracking code.';
			return false;
		}
	});
	if( error ){
		$('#error').html('<span>'+error+'</span>');
		$('html, body').animate({scrollTop:0})
		return false;
	}
}
</script>