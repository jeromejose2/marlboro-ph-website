<form id="form-reg" class="registration" autocomplete="off" target="upload-giid-target" action="<?= BASE_URL ?>user/confirm" enctype="multipart/form-data" method="POST">
<div class="wrapper registration">
	<div class="container main">

		<p class="uc txtcenter">Registration is free. Register and gain access to exclusive content and special offers. To complete the registration process, a copy of a valid government-issued id has to be submitted for age verification. Upon verification, your username and password will be sent to you. For accuracy of registered data, please review and confirm the details provided.</p> <br>
			<div id="error-msg"><span></span><div><i></i></div></div>
			<!-- REGISTRATION PART 1 -->
			<div class="row reg-page registration-step-1">
				<div class="col-md-6">
					<ul class="list-unstyled">
						<li>
							<span class="label-main-l"> <span class="ribbon"><i> * ARE YOU A SMOKER?</i></span> </span>
							<span class="input-s">	
								<label class="radio-label"><input type="radio" class="req" value="1" name="smoker" title="Are you a smoker?"> YES</label>
								<label class="radio-label"><input type="radio" class="req" value="0" id="not-smoker" name="smoker" title="Are you a smoker?"> NO</label>
							</span>
						</li>
						<li>
							<span class="label-main"> <span class="ribbon"><i> * NAME</i></span> </span> <br>
							<span class="label-sub">NICK NAME</span> 
							<span class="input"><input type="text" maxlength="15" autocomplete="off" name="nname" class="req" title ="Please enter your nick name"></span>
							
							<span class="label-sub">FIRST NAME</span>
							<span class="input"><input type="text" maxlength="15" autocomplete="off" name="fname" class="req" title ="Please enter your first name"></span>
							
							<span class="label-sub">MIDDLE NAME</span>
							<span class="input"><input type="text" maxlength="15" autocomplete="off" name="mname"></span>
							
							<span class="label-sub">LAST NAME </span>
							<span class="input"><input type="text" maxlength="15" autocomplete="off" name="lname" class="req" title ="Please enter your last name"></span>
						</li>
						<li>
							<span class="label-main"> <span class="ribbon"><i> * BIRTHDAY</i></span> </span>
							<span class="input"><input type="text" autocomplete="off" readonly="readonly" name="birthday" id="birthdate" class="req" title ="Please enter your birthdate"> </span>
						</li>
						<li>
							<span class="label-main"> <span class="ribbon"><i> * GENDER</i></span> </span>
							<span class="input">
								<label class="radio-label"><input type="radio" value="M" name="gender" class="req" title="Select your gender."> MALE</label>
								<label class="radio-label"><input type="radio" value="F" name="gender" class="req" title="Select your gender."> FEMALE</label>
							</span>
						</li>
						<li>
							<span class="label-main"> <span class="ribbon"><i> * <span class="hidden-xs">MOBILE</span> NUMBER</i></span> </span>
							<span class="input">
								<select class="select-s req" name="mobile_prefix" title="please select your mobile prefix.">
									<option value="">-</option>
									<?php foreach ($mobile_prefixes as $prefix): ?>
									<option value="<?= $prefix['mobile_prefix'] ?>"><?= $prefix['mobile_prefix'] ?></option>
									<?php endforeach ?>
								</select>
								<input type="text" autocomplete="off" id="mobile-number" class="input-m req" name="mobile_number" maxlength="7" title="Please enter valid mobile number.">
							</span>
						</li>
						<li class="visible-md visible-lg">* REQUIRED FIELDS</li>
					</ul>
				</div>
				<div class="col-md-6">
					<ul class="list-unstyled">

						<li>
							<span class="label-main"><span class="ribbon"><i> * EMAIL<span class="hidden-xs"> ADDRESS</span></i></span> </span>
							<span class="input"><input type="text" autocomplete="off" class="req" id="email" name="email" title="Please enter your email address."> </span>
						</li>
						<li>
							<span class="label-main"><span class="ribbon"><i> * <span class="hidden-xs">MAILING</span> ADDRESS</i></span> </span> <br>
							<span class="label-sub">HOUSE AND STREET</span> 
							<span class="input"><input type="text" autocomplete="off" class="req" name="street" title="Please enter your House and Street details"></span> 
							
							<span class="label-sub">BARANGAY</span>
							<span class="input"><input type="text" autocomplete="off" class="req" name="brgy" title="Please enter your barangay"></span> 
							
							<span class="label-sub">PROVINCE </span> 
							<span class="input">
								<select class="req" id="province-select" name="province" title="select one">
									<option value="">PROVINCE</option>
									<?php foreach ($provinces as $province): ?>
									<option value="<?= $province['province_id'] ?>"><?= $province['province'] ?></option>
									<?php endforeach ?>
								</select>
							</span>

							<span class="label-sub">CITY</span>
							<span class="input">
							<select class="req" id="cities-select" name="city" title="select one">
								<option value="">CITY</option>
							</select>
							</span>

							<span class="label-sub">ZIP CODE</span>
							<span class="input"><input type="text" maxlength="4" autocomplete="off" class="req" name="zip_code" title="Please enter zip code"></span> 
						</li>
						<li> 
							<span class="label-main"><span class="ribbon"><i> REFERRAL CODE:</i></span> </label> </span>
							<span class="input"> <input name="referred_code" type="text" autocomplete="off"></span>
						</li>
						<li> 
							<span class="label-main"><span class="ribbon"><i> OUTLET CODE:</i></span> </label> </span>
							<span class="input"> <input id="reg-outlet-code" name="outlet_code" type="text" autocomplete="off"></span>
						</li>
						<li class="hidden-md hidden-lg">* REQUIRED FIELDS</li>
					</ul>
				</div>
				<div class="txtcenter clearfix">
					<button type="button" class="button" id="reg-part-1"><i>NEXT</i></button>
				</div>
			</div>

			<!-- REGISTRATION PART 2 -->
			<div class="row reg-page registration-step-2">
				<div class="col-md-7 col-md-offset-3">
					<ul class="list-unstyled">
						<li>
							 <span class="ribbon large"><i> * WHAT BRAND DO YOU SMOKE MOST FREQUENTLY?</i></span> <br>
							<span class="label-spacer"></span>	
							<span class="input-f"> 
								<select name="current_brand" class="req" title="select one.">
									<option value="">Select Brand</option>
									<?php foreach ($brands as $brand): ?>
									<option value="<?= $brand['brand_id'] ?>"><?= $brand['brand_name'] ?></option>
									<?php endforeach ?>
								</select>
							</span>
						</li>
						<li>
							<span class="ribbon large"><i> * WHAT OTHER BRANDS DO YOU SMOKE ASIDE FROM YOUR REGULAR BRAND?</i></span><br>
							<span class="label-spacer"></span>		
							<span class="input-f">
								<select name="first_alternate_brand" class="req" title="select one.">
									<option value="">Select Brand</option>
									<?php foreach ($brands as $brand): ?>
									<option value="<?= $brand['brand_id'] ?>"><?= $brand['brand_name'] ?></option>
									<?php endforeach ?>
								</select>
							</span>
						</li>
						<li class="brand-unavailable">
							<span class="ribbon large"><i> * WHAT WHOULD YOU DO IF YOUR REGULAR BRAND IS UNAVAILABLE?</i></span> <br>
							<span class="label-spacer"></span>
							<span class="input-f">
								<?php foreach ($alternate_purchase as $purchase): ?>
								<label class="radio-label">
									<input class="req" value="<?= $purchase['alternate_id'] ?>" title="select one." name="alternate_purchase" type="radio"> 
									<span><?= $purchase['alternate_value'] ?></span>
								</label>
								<?php endforeach ?>
							</span>
						</li>
						<li>* REQUIRED FIELDS</li>
					</ul>
				</div>
				<div class="txtcenter clearfix">
					<br><br>
					<button type="button" class="button" id="reg-part-2"><i>NEXT</i></button>
				</div>
			</div>

			<!-- REGISTRATION PART 3 -->
			<div class="row reg-page registration-step-3">
				<p class="uc txtcenter">PLEASE ATTACH A SOFT COPY OF YOUR VALID GOVERNMENT-ISSUED ID (GIID) <a href="#" class="red" onclick="popup.open({html:'#popup2'})">SEE VALID GIID LIST</a></p>
				<div class="col-md-12">
					<div class="col-md-6">
						<span class="label-main"> <span class="ribbon"><i> SELECT GIID TYPE</i></span></span>
						<span class="input">
							<select name="giid_type" class="req" title="Please select GIID type.">
								<option value="">Select</option>
								<?php foreach ($giid_types as $type): ?>
								<option value="<?= $type['giid_type_id'] ?>"><?= $type['giid_type'] ?></option>
								<?php endforeach ?>
							</select>
						</span>
					</div>
					<div class="col-md-6">
						<span class="label-main"><span class="ribbon"><i> GIID NUMBER</i></span></span>
						<span class="input"><input class="req" title="Please input GIID number." name="giid_number" type="text" autocomplete="off"></span>
					</div>
					<br class="clearfix">
					<div class="col-md-5 upload-holder">
						<button class="button req" type="button" title="Please upload file" name="upload_giid_button" onclick="popup.open({html:'#popup1'})"><i>UPLOAD GIID NOW</i></button>
						<button class="button" type="button" id="upload-gid-later"><i>UPLOAD GIID LATER</i></button>
					</div>
					<div class="col-md-7">
						<p>To continue with registration, please submit a copy of your GIID within seven (7) days. Failure to do so will restart registration process.</p>
					</div>
					<br class="clearfix">
					<h3>By clicking the "CONFIRM" button below, I hereby declare and confirm that:</h3>
					<ol>
						<li>The information I provided is accurate.</li>
						<li>I am an adult smoker 18 years old or above.</li>
						<li>I am a resident of the Philippines.</li>
						<li>The contact number(s) and email address I provided are used personally and exclusively by me and cannot be accessed or shared by other persons.</li>
						<li>I have read, and I affirm my agreement to, the Terms and Conditions and Privacy Statement and Consentof this Website.</li>
						<li>I affirm giving my consent to PMFTC Inc. ("PM") to use my personal information as stated in the Privacy Statement and Consent, including but not limited to:
							<ol class="abc">
								<li>Storingand using my personal information to allow PM and its affiliates and duly authorized representatives to verify my age and identity in compliance with applicable laws and internal policies and/or to contact me;</li>
								<li>Providing me, using mail, email, SMS, telephone, etc., with communications related to tobacco products, including but not limited to information and material on brand launches, packaging changes, events, marketing activities, regulation of tobacco products and political topics that may be of relevance to tobacco products;</li>
								<li>Processing my information in countries other than the country in which I live (for example, locating the database in one country and accessing the database remotely from another); and</li>
								<li>Disclosing my personal information to service providers for the above purposes, to PM affiliates to do the above for their own purposes, and (if required by law) to relevant authorities.</li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="txtcenter clearfix">
					<button class="button" type="submit"><i>CONFIRM</i></button>
				</div>
			</div>
			<nav class="reg-page">
				<a href="#"> &lt; </a>
				<a href="#" class="active"> 1 </a>
				<a href="#"> 2 </a>
				<a href="#"> 3 </a>
				<a href="#"> &gt; </a>
			</nav>
	</div>
	<br class="clearfix">
</div>
<div class="lytebox-wrap">
	<div class="lytebox-content-holder">
		<div class="popup lytebox-wrapped-content" id="popup1">
			<div class="content txtcenter">
				<button type="button" class="popup-close">&times;<span>CLOSE</span></button>
				<h2>UPLOAD YOUR VALID GOVERNMENT-ISSUED ID</h2>
				<small>Only pdf, jpg, png and gif formats will be accepted</small>
				<br><br>

				<input class="m-auto hidden" type="file" id="giid" name="giid" onchange="PreviewImage();">
				<div class="browse">
					<input type="text" placeholder="Browse for file (Max file size: 3MB)" readonly>
				</div>
				<div class="upload-preview"></div>
				<button type="button" class="button webcam" id="show-webcam"><i>UPLOAD VIA WEBCAM</i></button>

				<br>
				<button class="button" type="button" onclick="popup.close()"><i>SUBMIT</i></button>
			</div>
		</div>
		<div class="popup lytebox-wrapped-content" id="popup2">
			<div class="content txtcenter">
				<button type="button" class="popup-close">&times;<span>CLOSE</span></button>
				<h2>VALID GOVERNMENT-ISSUED ID LIST</h2>
				<div class="row">
					<ul class="col-sm-5 col-sm-offset-1 list-unstyled">
						<li>Passport</li>
						<li>Driver's License</li>
						<li>NBI Clearance</li>
						<li>SSS ID (with birthday only)</li>
					</ul>
					<ul class="col-sm-6 list-unstyled">
						<li>Voter's ID</li>
						<li>PRC ID</li>
						<li>Unified Multi-Purpose ID</li>
						<li>Alien Certificate of Registration</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="popup lytebox-wrapped-content" id="popup5">
			<div class="content txtcenter">
				<button type="button" class="popup-close">&times;<span>CLOSE</span></button>
				<br>
				<h3>Thank you for your submission.</h3>
				<p>Thank you for registering to Marlboro.ph. Please allow us seven (7) days to verify your registration details. You will receive an e-mail with your log-in details once your membership has been verified.<p>
				<button class="button" type="button" onclick="closePopup('user/login');"><i>OK</i></button>
			</div>
		</div>
		<div class="popup lytebox-wrapped-content" id="popup3">
			<div class="content txtcenter">
				<button type="button" class="popup-close">&times;<span>CLOSE</span></button>
				<h3>Thank you for your submission.</h3>
				<p>A tracking code for your registration will be sent to your registered email address. Please don't forget to submit a copy of a valid government-issued ID within seven (7) days. Your registration will only be processed upon submission of a valid government-issued ID.<p>
				<button class="button" type="button" onclick="closePopup('user/login');"><i>OK</i></button>
			</div>
		</div>
		<div class="popup lytebox-wrapped-content" id="popup4">
			<div class="content txtcenter">
				<button type="button" class="popup-close">&times;<span>CLOSE</span></button>
				<h2>UPLOAD YOUR VALID GOVERNMENT-ISSUED ID</h2>
				<div class="upload-webcam" id="flashcontent"></div>
				<div id="captured-alert" class="hide"><div id="error" class="success"><span>Successfully captured, you may submit your GIID.</span></div></div>
				<button class="button" type="button" id="webcam-take-photo"><i>TAKE PHOTO</i></button>
				<button class="button" type="button" id="webcam-photo-submit" style="display:none;"><i>SUBMIT</i></button>
			</div>
		</div>
	</div>
</div>
<input type="hidden" name="captured_image_ba" id="captured-image-ba">
<input type="hidden" name="__token" id="__token" value="<?= $token ?>">
</form>
<iframe name="upload-giid-target" id="upload-giid-target" width="0" height="0" border="0"></iframe>