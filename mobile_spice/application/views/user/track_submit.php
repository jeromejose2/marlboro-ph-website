<form id="form-reg" autocomplete="off" class="registration" target="upload-giid-target" action="<?= BASE_URL ?>user/submit_late_giid" enctype="multipart/form-data" method="POST">
<div class="wrapper registration">
	<div class="container main">

		<p class="uc txtcenter">Registration is free. Register and gain access to exclusive content and special offers. To complete the registration process, a copy of a valid government-issued id has to be submitted for age verification. Upon verification, your username and password will be sent to you. For accuracy of registered data, please review and confirm the details provided.</p> <br>
			<div id="error-msg"><span></span><div><i></i></div></div>
			<!-- REGISTRATION PART 3 -->
			<div class="row registration-step-3" style="display:block;">
				<p class="uc txtcenter">PLEASE ATTACH A SOFT COPY OF YOUR VALID GOVERNMENT-ISSUED ID (GIID) <a href="#" class="red" onclick="popup.open({html:'#popup2'})">SEE VALID GIID LIST</a></p>
				<div class="col-md-12">
					<div class="col-md-6">
						<span class="label-main"> <span class="ribbon"><i> SELECT GIID TYPE</i></span></span>
						<span class="input">
							<select title="Select one" name="giid_type" class="req">
								<option value="">Select</option>
								<?php foreach ($giid_types as $type): ?>
								<option value="<?= $type['giid_type_id'] ?>"><?= $type['giid_type'] ?></option>
								<?php endforeach ?>
							</select>
						</span>
					</div>
					<div class="col-md-6">
						<span class="label-main"><span class="ribbon"><i> GIID NUMBER</i></span></span>
						<span class="input"><input autocomplete="off" class="req" title="Please enter GIID number" name="giid_number" type="text"></span>
					</div>
					<br class="clearfix">
					<div class="col-md-5 upload-holder">
						<button class="button req" type="button" name="upload_giid_button" onclick="popup.open({html:'#popup1'})"><i>UPLOAD GIID NOW</i></button>
						<button class="button" type="button" id="upload-track-giid-later"><i>UPLOAD GIID LATER</i></button>
					</div>
					<div class="col-md-7">
						<p>To continue with registration, please submit a copy of your GIID within seven (7) days. Failure to do so will restart registration process.</p>
					</div>
					<br class="clearfix">
					<h3>By clicking the "CONFIRM" button below, I hereby declare and confirm that:</h3>
					<ol>
						<li>The information I provided is accurate.</li>
						<li>I am an adult smoker 18 years old or above.</li>
						<li>I am a resident of the Philippines.</li>
						<li>The contact number(s) and email address I provided are used personally and exclusively by me and cannot be accessed or shared by other persons.</li>
						<li>I have read, and I affirm my agreement to, the Terms and Conditions and Privacy Statement and Consentof this Website.</li>
						<li>I affirm giving my consent to PMFTC Inc. ("PM") to use my personal information as stated in the Privacy Statement and Consent, including but not limited to:
							<ol class="abc">
								<li>Storingand using my personal information to allow PM and its affiliates and duly authorized representatives to verify my age and identity in compliance with applicable laws and internal policies and/or to contact me;</li>
								<li>Providing me, using mail, email, SMS, telephone, etc., with communications related to tobacco products, including but not limited to information and material on brand launches, packaging changes, events, marketing activities, regulation of tobacco products and political topics that may be of relevance to tobacco products;</li>
								<li>Processing my information in countries other than the country in which I live (for example, locating the database in one country and accessing the database remotely from another); and</li>
								<li>Disclosing my personal information to service providers for the above purposes, to PM affiliates to do the above for their own purposes, and (if required by law) to relevant authorities.</li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="txtcenter clearfix">
					<button class="button" type="submit"><i>CONFIRM</i></button>
				</div>
			</div>
			<nav class="reg-page">
				<a href="#"> &lt; </a>
				<a href="#" > 1 </a>
				<a href="#"> 2 </a>
				<a href="#" class="active"> 3 </a>
				<a href="#"> &gt; </a>
			</nav>
	</div>
	<br class="clearfix">
</div>
<div class="lytebox-wrap">
	<div class="popup lytebox-wrapped-content" id="popup1">
		<div class="content txtcenter">
			<button type="button" class="popup-close">&times;<span>CLOSE</span></button>
			<h2>UPLOAD YOUR VALID GOVERNMENT-ISSUED ID</h2>
			<small>Only pdf, jpg, png and gif formats will be accepted</small>
			<br><br>

			<input class="m-auto hidden" type="file" id="giid" name="giid" onchange="PreviewImage();">
			<div class="browse">
				<input type="text" placeholder="Browse" readonly>
			</div>
			<div class="upload-preview"></div>
			<button type="button" class="button webcam" id="show-webcam"><i>UPLOAD VIA WEBCAM</i></button>

			<br>
			<button class="button" type="button" onclick="popup.close()"><i>SUBMIT</i></button>
		</div>
	</div>
	<div class="popup lytebox-wrapped-content" id="popup2">
		<div class="content txtcenter">
			<button type="button" class="popup-close">&times;<span>CLOSE</span></button>
			<h2>VALID GOVERNMENT-ISSUED ID LIST</h2>
			<div class="row">
				<ul class="col-sm-5 col-sm-offset-1 list-unstyled">
					<li>Passport</li>
					<li>Driver's License</li>
					<li>NBI Clearance</li>
					<li>SSS ID (with birthday only)</li>
				</ul>
				<ul class="col-sm-6 list-unstyled">
					<li>Voter's ID</li>
					<li>PRC ID</li>
					<li>Unified Multi-Purpose ID</li>
					<li>Alien Certificate of Registration</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="popup lytebox-wrapped-content" id="popup5">
		<div class="content txtcenter">
			<button type="button" class="popup-close">&times;<span>CLOSE</span></button>
			<br>
			<h3>Thank you for your submission.</h3>
			<p>Thank you for registering to Marlboro.ph. Please allow us seven (7) days to verify your registration details. You will receive an e-mail with your log-in details once your membership has been verified.<p>
			<button class="button" type="button" onclick="closePopup('user/login');"><i>OK</i></button>
		</div>
	</div>
	<div class="popup lytebox-wrapped-content" id="popup3">
		<div class="content txtcenter">
			<button type="button" class="popup-close">&times;<span>CLOSE</span></button>
			<br>
			<h3>Thank you for your submission.</h3>
			<p>A tracking code for your registration will be sent to your registered email address. Please don’t forget to submit a copy of a valid government-issued ID within seven (7) days. Your registration will only be processed upon submission of a valid government-issued ID.<p>
			<button class="button" type="button" onclick="closePopup('user/login');"><i>OK</i></button>
		</div>
	</div>
	<div class="popup lytebox-wrapped-content" id="popup4">
		<div class="content txtcenter">
			<button type="button" class="popup-close">&times;<span>CLOSE</span></button>
			<br>
			<h2>UPLOAD YOUR VALID GOVERNMENT-ISSUED ID</h2>
			<div class="upload-webcam" id="flashcontent"></div>
			<div id="captured-alert" class="hide"><div id="error" class="success"><span>Successfully captured, you may submit your GIID.</span></div></div>
			<button class="button" type="button" id="webcam-take-photo"><i>TAKE PHOTO</i></button>
			<button class="button" type="button" id="webcam-photo-submit" style="display:none;"><i>SUBMIT</i></button>
		</div>
	</div>
</div>
<input type="hidden" name="captured_image_ba" value="" id="captured-image-ba">
<input type="hidden" name="__token" id="__token" value="<?= $token ?>">
</form>
<iframe name="upload-giid-target" id="upload-giid-target" width="0" height="0" border="0"></iframe>