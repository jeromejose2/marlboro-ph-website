<div class="wrapper">
	
	<div class="container main row move-fwd-challenge">

		<!-- side navigation -->
		<div class="col-md-3 col-lg-2  col-sm-12 side-rail">
			<a href="<?= BASE_URL ?>move_forward" class="back">BACK</a>
			<nav>
				<a href="<?= BASE_URL ?>move_forward">ACTIVE OFFERS</a>
				<?php foreach ($categories as $category): ?>
				<a <?= $offer['category_id'] == $category['category_id'] ? 'class="active"' : '' ?> href="<?= BASE_URL ?>move_forward/category/<?= $category['category_id'] ?>">
					<?= $category['category_name'] ?>
				</a>
				<?php endforeach ?>
			</nav>
		</div>
		<!-- end side navigation -->
		
		<div class="col-md-9 col-md-push-3 col-lg-push-2 main-rail">
			<div class="row">
				<div class="col-sm-4 col-md-5">
					<div class="thumb-holder show-gallery" id="move-fwd-show-gallery">
						<div class="thumbnail" style="background-image:url('<?= BASE_URL ?>uploads/move_fwd/main_<?= $offer['move_forward_image'] ?>')"></div>
						<h3 id="show-gallery-h3">GALLERY</h3>
					</div>
				</div>
				
				<div class="col-sm-8 col-md-7">
					<h2><?= $offer['move_forward_title'] ?></h2>
					<?= $offer['move_forward_description'] ?>
					<!-- <a href="javascript:void(0)" class="button" onclick="popup.open({url:'<?= BASE_URL ?>move_forward/mechanics'})"><i>READ FULL MECHANICS</i></a> -->
				</div>
				
				<div class="clearfix"></div>

				<div class="col-sm-12 col-md-4 col-md-push-8">

					<div class="participants">
						<span id="participants-preloader">PARTICIPANTS</span>

						<div class="arrow left" id="participants-prev" data-page="0"></div>
						<div class="arrow right" id="participants-next"></div>

						<div class="thumbs" id="participants-thumblist">
							<!-- paricipants -->
						</div>
					</div>

				</div>
				<div class="col-sm-12 col-md-8 col-md-pull-4">
					<?= show_comments(MOVE_FWD, $offer['move_forward_id'], array(), array('category' => 'Comment', 'action' => 'submit', 'label' => 'Commented on MoveFwd: '.$offer['move_forward_title'], 'value' => 1)) ?>
				</div>
			</div>
		</div>

	</div>

</div>

<!-- popups -->
<div class="lytebox-wrap">
	<div class="popup-gallery lytebox-wrapped-content" id="move-fwd-gallery-popup"></div>
</div>
<script type="text/javascript">
	var MoveFwd = {
		PLAY : <?= MOVE_FWD_PLAY ?>,
		PLEDGE : <?= MOVE_FWD_PLEDGE ?>,
		currentOffer : <?= $offer['move_forward_id'] ?>
	};
</script>
<script type="text/javascript" src="<?= BASE_URL ?>scripts/move-forward.js"></script>