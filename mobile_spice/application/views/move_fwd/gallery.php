<!-- <div class="popup-gallery"> -->
	<div class="content txtcenter">
		<button class="popup-close">&times; <span>CLOSE</span></button>

		<div class="row">
			<div class="col-md-7">
				<h2><?= $offer['move_forward_title'] ?></h2>
				<div class="gallery-stat">
					<span>COMMENTS: <?= $comments_count ?></span>
					<span id="move-fwd-visits-count">VIEWS: <?= $visits ?></span>
				</div>
				
				<figure class="theater" id="gallery-current-preview">
					<div class="no-image" style="background-image:url(<?=BASE_URL.DEFAULT_IMAGE?>)"></div>
				</figure>

				<div class="carousel">
					<div class="arrow left" id="move-fwd-gallery-prev"></div>
					<div class="arrow right" id="move-fwd-gallery-next"></div>

					<div id="gallery-thumbs-list" class="thumbs">
						<?php if ($entries): ?>
						<?php foreach ($entries as $entry): ?>
							<?php if($entry['type'] == 'Video' && preg_match('![?&]{1}v=([^&]+)!', $entry['mfg_content'].'&', $data)): ?>
							<!-- <?= $entry['move_forward_gallery_id'] ?> -->
							<div class="thumbnail"
								data-type="video" 
								data-src="<?= $entry['mfg_content'] ?>" 
								data-id="<?= $entry['move_forward_gallery_id'] ?>"
								data-recipient="<?= $entry['registrant_id'] ?>"
								style="background-image: url('//img.youtube.com/vi/<?= $data[1] ?>/mqdefault.jpg');">
								<!-- <img src="//img.youtube.com/vi/<?= $data[1] ?>/mqdefault.jpg"> -->
							</div>
							<?php elseif ($entry['type'] == 'Photo'): ?>
							<!-- <?= $entry['move_forward_gallery_id'] ?> -->
							<div class="thumbnail"
								data-type="photo"
								data-src="<?= BASE_URL ?>uploads/profile/<?= $entry['registrant_id'] ?>/move_fwd/<?= $entry['mfg_content'] ?>" 
								data-id="<?= $entry['move_forward_gallery_id'] ?>"
								data-recipient="<?= $entry['registrant_id'] ?>"
								style="background-image: url('<?= BASE_URL ?>uploads/profile/<?= $entry['registrant_id'] ?>/move_fwd/115_71_<?= $entry['mfg_content'] ?>');">
								<!-- <img src="<?= BASE_URL ?>uploads/profile/<?= $entry['registrant_id'] ?>/move_fwd/115_71_<?= $entry['mfg_content'] ?>"> -->
							</div>
							<?php endif ?>
						<?php endforeach ?>
						<?php else: ?>
						<div class="thumbnail"><div class="no-image" style="background-image:url(<?=BASE_URL.DEFAULT_IMAGE?>)"></div></div>
						<div class="thumbnail"><div class="no-image" style="background-image:url(<?=BASE_URL.DEFAULT_IMAGE?>)"></div></div>
						<div class="thumbnail"><div class="no-image" style="background-image:url(<?=BASE_URL.DEFAULT_IMAGE?>)"></div></div>
						<?php endif ?>
					</div>

				</div>
			
			</div>

			<div class="col-md-5">
				<div id="comments-move-fwd-gallery" class="comments">
					<h3>No Comments</h3>
				</div>
			</div>

		</div>
	</div>
<!-- </div> -->

<script type="text/javascript">
<?php if ($entries): ?>
var galleryIsNotEmpty = true;
function showComments(entryId, recipientId) {
	$("#comments-move-fwd-gallery").html("");
	CommentBox.create("#comments-move-fwd-gallery", {
		origin : Origin.MOVE_FWD_GALLERY,
		suborigin : entryId,
		onCloseSuccessPopup : function() {
			popup.open({html : "#move-fwd-gallery-popup"});
		},
		onCloseAttachPopup : function() {
			popup.open({html : "#move-fwd-gallery-popup"});
		}
	})
	.setRecipient(recipientId)
	.loadComments();
}

var itemsPerSlide = 3;
var currentGalleryIndex = 0;
var galleryThumbsCount = <?= count($entries) ?>;
var galleryThumbsList = $("#gallery-thumbs-list");
galleryThumbsList.owlCarousel({
	items : itemsPerSlide,
	itemsDesktop : [3000,3],
	itemsMobile : [767,3],
	itemsTablet : [991,4],
	pagination : true,
	scrollPerPage : true
});

var galleryThumbs = galleryThumbsList.data("owlCarousel");
var galleryThumbnails = galleryThumbsList.find(".thumbnail");

$("#move-fwd-gallery-prev").click( function() {
	galleryThumbs.prev();
	var index = --currentGalleryIndex * itemsPerSlide;
	if (index < 0) {
		currentGalleryIndex = Math.floor((galleryThumbsCount - 1) / itemsPerSlide);
		index = currentGalleryIndex * itemsPerSlide;
	}
	galleryThumbnails.eq(index).trigger("click");
});

$("#move-fwd-gallery-next").click( function() {
	galleryThumbs.next();
	var index = ++currentGalleryIndex * itemsPerSlide;
	if (index >= galleryThumbsCount) {
		index = 0;
		currentGalleryIndex = index;
	}
	galleryThumbnails.eq(index).trigger("click");
});

var galleryCurrentPreview = $("#gallery-current-preview");
var currentGalleryItemId = null;
galleryThumbnails.click( function() {
	
	var self = $(this);
	var id = self.data("id");
	var recipient = self.data("recipient");
	if (!id) {
		return;
	}
	var type = self.data("type");
	var src = self.data("src");
	if (type == "photo") {
		galleryCurrentPreview.html("<img src=\"" + src + "\">");
	} else if (type == "video") {
		var start = src.indexOf("?v=");
	    start = start + 3;
		var end = src.indexOf("&");
		var videoID = end < 0 ? src.substring(start) : src.substring(start, end);
		var obj = '<object width="100%" height="100%">';
			obj +='<param name="movie" value="//www.youtube.com/v/' + videoID + '?hl=en_US&amp;version=3"></param>';
			obj +='<param name="allowFullScreen" value="false"></param>';
			obj +='<param name="allowscriptaccess" value="always"></param>';
			obj +='<param name="wmode" value="opaque"></param>';
			obj +='<embed src="//www.youtube.com/v/' + videoID + '?hl=en_US&amp;version=3" type="application/x-shockwave-flash" width="100%" height="100%" allowscriptaccess="always" allowfullscreen="true">';
			obj +='</embed>';
			obj +='</object>';
		galleryCurrentPreview.html(obj);
	}
	if (id != currentGalleryItemId) {
		showComments(id, recipient);
		currentGalleryItemId = id;
	}

}).first().trigger("click");
<?php else: ?>
var galleryIsNotEmpty = false;
$("#gallery-thumbs-list").owlCarousel({
	items : 3,
	itemsDesktop : [3000,3],
	itemsMobile : [767,3],
	itemsTablet : [991,4],
	pagination : true,
	scrollPerPage : true
});
<?php endif ?>
</script>
