
<section class="main">
	<div class="wrapper premium">	

		<div class="countdown teaser-01">
			<div class="container">
				<img src="<?=BASE_URL?>images/black-is-coming.png" class="img-responsive">
				<div class="line"></div>
				<ul><!-- js generated countdown timer hre --></ul>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo BASE_URL ?>scripts/jquery.countdowntimer.js"></script>
<script>
var layout = '<li><span>{dnn}</span><i>DAYS</i></li>';
var	spacer = '<li><span>:</span><i>&nbsp;</i></li>';
	layout+= spacer+'<li><span>{hnn}</span><i>HOURS</i></li>';
	layout+= spacer+'<li><span>{mnn}</span><i>MIN</i></li>';
	layout+= spacer+'<li><span>{snn}</span><i>SEC</i></li>';
	
liftoffTime = new Date('<?php echo date("Y/m/d H:i:s", strtotime("2014/07/25")) ?>');
$('.countdown ul').countdown({
	until : liftoffTime,
	layout: layout
}); 
</script>
<script type="text/javascript">

	$(function(){


		 <?php if($birthday_offer){ ?>
		 		popup.open({url:"birthday_offer/get_birthday_offer?prize_id=<?=$birthday_offer?>&flash_offer=<?=$flash_offer?>&rand=<?=uniqid()?>",
		 					onClose:function(){		
								<?php if($flash_offer){ ?>
									  popup.open({url:'flash_offer/get_flash_offer?prize_id=<?=$flash_offer?>&rand=<?=uniqid()?>'});
 								<?php } ?> 		
		 					}
		 				});
		 <?php } ?>

		 <?php if(!$birthday_offer && $flash_offer){ ?>
		 		popup.open({url:"flash_offer/get_flash_offer?prize_id=<?=$flash_offer?>&rand=<?=uniqid()?>"});
		 <?php } ?> 


		  <?php if($bids){ ?>
		 		popup.open({url:"perks/bid/bid_notification"});
		 <?php } ?> 
	});
</script>