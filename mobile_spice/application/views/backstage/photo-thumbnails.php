	
<!-- main content starts here  -->
 	<div class="wrapper">
		
		<div class="container main">
			<div class="row">	
				<div class="col-md-3 col-lg-2  col-sm-12 side-rail sticky">
					<nav>
						<?php /*
						<a href="<?=BASE_URL?>events/hot"> <i class="glyphicon glyphicon-fire"></i>What's HOT</a>
						<a href="<?=BASE_URL?>events/upcoming"> <i class="glyphicon glyphicon-star"></i>Upcoming Events</a>
						<?php */ ?>
						<a href="<?=BASE_URL?>events/past" class="active"> <i class="glyphicon glyphicon-time"></i>Past Events</a>
					</nav>
				</div>

				<div class="col-md-9 col-lg-10  col-md-push-3 col-lg-push-2 main-rail">
					<div class="pull-left title">
						<h2><?=($row) ? $row->title : 'Content not found.'?></h2>
						<h5><?php

						if($row){

							$event = $row->venue;
							if($row->venue && $row->region_name) 
								$event .= ', ';

							if($row->region_name) 
								$event .= $row->region_name;		

		 					$schedule = simplify_datetime_range($row->start_date,$row->end_date);
		 					$event .= $schedule ? ' - '.$schedule : '';
		 					echo $event;

						}
						
	 					?> </h5>
						<h3><a href="<?=BASE_URL?>events/past">Back to photos</a></h3>
					</div>
					<div class="upload-from-event">
						<button type="button" class="button-small" onclick="popup.open({url:'<?=BASE_URL?>backstage_pass/upload_form/<?=$backstage_event_id?>'})"><i>UPLOAD YOUR PHOTOS</i></button>
					</div>
					<div class="clearfix"></div>

					<div class="row row-listing">

						<!-- start items -->
						<?php if($row_photos->num_rows()){ 

								foreach($row_photos->result() as $p){ ?>

									<div class="col-xs-6 col-sm-4 item">
										<a href="<?=BASE_URL.'backstage_pass/content/'.$row->url_title.'/'.$p->photo_id?>">
											<div class="thumb-holder">
											<?php if(file_exists("uploads/backstage/photos/thumbnails/331_176_".$p->image)){ ?>
													<div class="thumbnail" style="background-image:url('<?=BASE_URL."uploads/backstage/photos/thumbnails/331_176_".$p->image?>')"></div>
										 	<?php } ?>
											</div>
											<div class="clearfix"></div>
										</a>
									</div>

								<?php
								}
							} ?>
						<!-- end items -->
						
					</div>
				</div>
			</div>
		</div>
	</div> 
 
	
<!-- main content ends here  -->
