
<!-- main content starts here  -->
 	<div class="wrapper">
		
		<div class="container main">
			<div class="row">
				<div class="col-md-3 col-lg-2  col-sm-12 side-rail sticky">
					<nav>
						<a href="<?=BASE_URL?>events/hot" class="active"> <i class="glyphicon glyphicon-fire"></i>What's HOT</a>
						<a href="<?=BASE_URL?>events/upcoming"> <i class="glyphicon glyphicon-star"></i>Upcoming Events</a>
						<a href="<?=BASE_URL?>events/past"> <i class="glyphicon glyphicon-time"></i>Past Events</a>
					</nav>
				</div>

				<div class="clearfix"></div>

				<div class="col-md-9 col-lg-10  col-md-push-3 col-lg-push-2 main-rail">
					<!-- content -->

							<div class="arrow right"></div>
							<div class="arrow left"></div>
						<div class="hot-events">

							<?php for ($i=0; $i<= 10; $i++) { ?>
								<div class="item">
									<div class="row">
										<div class="col-sm-6">
											<a data-id="#" data-title="#" data-href="#" href="javascript:void(0)">
												<div class="thumb-holder">
													<div class="thumbnail" style="background-image:url('<?=BASE_URL;?>images/sample-category-music.jpg')"></div>
												</div>
												<div class="clearfix"></div>
											</a>
										</div>

										<div class="col-sm-6">
											<div class="title">
												<h3><a href="javascript:void(0)">EVENT TITLE</a></h3>
												<h5>BONIFACIO GLOBAL CITY OPEN FIELD</h5>
												<h5>April 25, 2014 6:30PM</h5>
											</div>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
											tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
											quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
											<a class="button-small"  href="javascript:void(0)" onclick="popup.open({html:'#map'})"><i> <i class="glyphicon glyphicon-map-marker"></i> VIEW MAP &nbsp;</i></a>
											<a class="button-small" href="javascript:void(0)"><i> <i class="glyphicon glyphicon-ok-circle"></i> IM GOING &nbsp;</i></a>
										</div>
									</div>
								</div> 
							<?php } ?>
						</div>

					<!-- end content -->
				</div>
			</div> 	
		</div>

	</div>

	<!-- popups -->
	<div class="lytebox-wrap">
		<!-- upload photo -->
		<div class="popup lytebox-wrapped-content" id="map">
			<div class="content map-embed">
				<button class="popup-close">&times;<span>CLOSE</span></button>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3861.754724133764!2d121.02108800000003!3d14.556014!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c90f7130824b%3A0x843a06c1738ae528!2sPhilip+Morris+Philippines+Inc.!5e0!3m2!1sen!2s!4v1398673498320" width="400" height="300" frameborder="0" style="border:0"></iframe>
			</div>
		</div>
	</div>
	<!-- end popup -->

 
	
<!-- main content ends here  -->
<script type="text/javascript">
$(function(){
	$('.browse').click(function(){
		$('#upload-photo').trigger('click');
	});
});
function PreviewImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("upload-photo").files[0]);
    oFReader.onload = function (oFREvent) {
    	$('.upload-preview-fixed').html('<img src="'+oFREvent.target.result+'"/>').show();
    };
};
</script>



<script>
window.onload = function(){ 
	$(".hot-events").owlCarousel({
		singleItem: true,
		autoPlay : 5000
	}); 

	var owl = $(".hot-events").data('owlCarousel');

	$('.arrow.left').click(function(){
		owl.prev();	
	});
	$('.arrow.right').click(function(){
		owl.next();	
	});
}
</script>