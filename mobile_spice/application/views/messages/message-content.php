<section class="main">
	<div class="wrapper ">
		
			<div class="row container messages">
				<?php $this->load->view('messages/nav', ['inner' => true]) ?>

				<div class="col-md-8 col-md-push-3 inbox-holder" id="messages-content"><img src="<?=BASE_URL?>images/spinner.gif"/> Loading...</div>

 
			</div>
	</div>
</section>

<script type="text/javascript" src="<?=BASE_URL?>scripts/handlebars-v1.3.0.js"></script> 

<script>


var tpl_status = false;
var tpl = false;


jQuery(document).ready(function(){
	retriveData("<?=BASE_URL.'api/messages?message_id='.$message_id?>");    
});


function reply(){

	var message = $('textarea[name="reply-message"]').val();

	jQuery.ajax({
		  dataType:"json",
		  type:"POST",
		  url: "<?=BASE_URL?>api/messages",
		  data: "message_id=<?=$message_id?>&message="+message,
		  beforeSend: function(){
		  	jQuery('#reply-btn').html('<i><img src="<?=BASE_URL?>images/spinner.gif"/> Sending...</i>');
		  },
		  success: function(data){
 		  	retriveData("<?=BASE_URL.'api/messages?message_id='.$message_id?>");
		  }
	}); 

}
 

function retriveData(dataSource) {
     jQuery.getJSON(dataSource, renderDataVisualsTemplate);
}


function renderDataVisualsTemplate(json_data){

	if(tpl_status==false){		

	    getTemplateAjax('<?=BASE_URL?>messages/getTemplate', function(template) {
	        handlebarsDebugHelper();
	        tpl_status = true;
	        jQuery('#messages-content').html(template(json_data));
	    });

	}else{

  		template = Handlebars.compile(tpl);
		jQuery('#messages-content').html(template(json_data));

	}


}

function getTemplateAjax(path, callback) {

    var source, template;

    jQuery.ajax({
        url: path,
         
        success: function (template_source) {
            tpl = template_source;
         	tpl_status = true;
        	template = Handlebars.compile(template_source);
        	if (callback) callback(template);
        }

    });

}

function handlebarsDebugHelper(){
    Handlebars.registerHelper("debug", function(optionalValue) {
        console.log("Current Context");
        console.log("====================");
        console.log(this);
    });
}

</script>