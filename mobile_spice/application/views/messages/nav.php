<div class="col-md-3 side-rail sticky <?= !empty($inner) ? 'hidden-xs' : '' ?>">
	<nav>
		<a href="<?= BASE_URL ?>messages/inbox" <?= $this->uri->segment(1) == 'messages' ? 'class="active"' : '' ?>> 
			<i class="glyphicon glyphicon-envelope"></i>INBOX
			<span id="unread-inbox-message"></span>
		</a>
		<a href="<?= BASE_URL ?>notifications" <?= $this->uri->segment(1) == 'notifications' ? 'class="active"' : '' ?>> 
			<i class="glyphicon glyphicon-exclamation-sign"></i>NOTIFICATIONS 
			<span id="notifications-unopened-num"><?= $this->unread_notifications ? '(<span class="red">'.$this->unread_notifications.'</span>)' : '' ?></span>
		</a>
		<a href="<?= BASE_URL ?>trash" <?= $this->uri->segment(1) == 'trash' ? 'class="active"' : '' ?>> 
			<i class="glyphicon glyphicon-trash"></i>TRASH
		</a>

	</nav>
</div>

<script type="text/javascript">

jQuery(document).ready(function(){

	getTotalUnreadInboxMessages();

});



function getTotalUnreadInboxMessages()
{
	jQuery.getJSON('<?=BASE_URL?>messages/totalUnreadMessages', function(data){
		var n = parseInt(data.total_rows);

		if(n>0){
			jQuery('#unread-inbox-message').html('(<span class="red">'+n+'</span>)');
		}

	});
}
</script>