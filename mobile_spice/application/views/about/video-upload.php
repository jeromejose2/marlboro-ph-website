<div class="popup">
	<div class="content">
		<form action="<?=BASE_URL?>videos/submit_yvideo" onsubmit="return onSubmitVideo();" method="post" target="upload_target">
			<button class="popup-close" id="btn-close-popup-video">&times;<span>CLOSE</span></button>
			
			<h3>UPLOAD YOUR OWN VIDEO VIA YOUTUBE</h3>

			<div class="browse">
				<input type="text" placeholder="Video Title" name="title">
			</div>

			<div class="browse">
				<input type="text" placeholder="Youtube URL (eg. https://www.youtube.com/watch?v=Yty6Kazof8g)" name="url">
			</div>

			<div class="upload-preview-fixed">
				<span>VIDEO PREVIEW</span>
			</div>
			
			<div class="txtcenter">
				<div class="error" id="error-video-upload"></div>
				<button type="button" class="button close-x" id="btn-cancel-video"><i>CANCEL</i></button>
				<button type="submit" class="button" id="btn-submit-video"><i>SAVE</i></button>
			</div>
		</form>
		<iframe id="upload_target" name="upload_target"  style="width:0px;height:0px;border:0px solid #fff;"></iframe>

			
	</div>
	</div>

<script type="text/javascript">

$(function(){

	$('input[name="url"]').bind('keydown keyup change',function(){
		videoPreview($(this).val());
	});

});

function showButtons()
{
	$('#btn-cancel-video').show();
	$('#btn-close-popup-video').show();
}

function onSubmitVideo()
{ 
 	$('#btn-submit-video').addClass('disabled').attr('disabled','disabled').html('<i><img src="<?=BASE_URL?>images/spinner.gif" /> SAVING...</i>');
 	$('#btn-cancel-video').hide();
	$('#btn-close-popup-video').hide(); 		 			
	return true;
}

function form_response(msg, video_id)
{
	popup.loading();
	GA.sendEvent({
		category : "Upload on About Videos",
		action : "submit",
		label : "Upload on About Videos",
		value : 1,
		callback : function() {
			setTimeout( function() {
				popup.open({type:'alert','message':msg,buttonAlign:'center',align:'center',okCaption:'CONTINUE'});
			}, 300);
		}
	});
}

function log(data){
	console.log(data);
}

function videoPreview(str){

	var start = str.indexOf("v=");
	    start = start+2;
	var end   = str.indexOf("&");
	var videoID = end<0 ? str.substring(start) : str.substring(start,end);

	$('.upload-preview-fixed').html('');

 
	var obj = '<object width="100%" height="100%">';
		obj +='<param name="movie" value="//www.youtube.com/v/'+videoID+'?hl=en_US&amp;version=3"></param>';
		obj +='<param name="allowFullScreen" value="true"></param>';
		obj +='<param name="allowscriptaccess" value="always"></param>';
		obj +='<embed src="//www.youtube.com/v/'+videoID+'?hl=en_US&amp;version=3" type="application/x-shockwave-flash" width="100%" height="100%" allowscriptaccess="always" allowfullscreen="true">';
		obj +='</embed>';
		obj +='</object>';

	$('.upload-preview-fixed').html(obj);
}
</script> 