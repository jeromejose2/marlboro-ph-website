
<!-- main content starts here  -->
 	<div class="wrapper">
		
		<div class="container main">
			<div class="row">
				<div class="col-md-3 col-lg-2  col-sm-12 side-rail sticky">
					<nav>
						<a href="<?=BASE_URL?>about/photos"> <i class="glyphicon glyphicon-camera"></i>PHOTOS</a>
						<a href="<?=BASE_URL?>about/videos"> <i class="glyphicon glyphicon-film"></i>VIDEOS</a>
						<a href="<?=BASE_URL?>about/news" class="active"> <i class="glyphicon glyphicon-list"></i>NEWS</a>
					</nav>
				</div>

				<div class="col-md-9 col-lg-10  col-md-push-3 col-lg-push-2 main-rail">

					<div class="row row-listing">
							<!-- start items -->
						<?php if($rows->num_rows()){

								$count = 0;
								

								foreach($rows->result() as $v){  $count++; ?>

									<div class="col-sm-6 item">
										<div class="row">
											<div class="col-sm-6">
												<a class="about-news-entry" data-id="<?= $v->about_news_id ?>" data-title="<?= $v->title ?>" data-href="<?=BASE_URL?>about/news/content/<?=$v->url_title?>" href="javascript:void(0)">
													<div class="thumb-holder">
														<?php if(file_exists('uploads/about/news/331_176_'.$v->image)){ ?>
															<div class="thumbnail" style="background-image:url('<?=BASE_URL.'uploads/about/news/331_176_'.$v->image?>')"></div>
														<?php }else{ ?>
															<div class="thumbnail" style="background-image:url('<?=BASE_URL.DEFAULT_IMAGE?>')"></div>
														<?php } ?>
														
													</div>
													<div class="clearfix"></div>
												</a>
											</div>

											<div class="col-sm-6">
												<h3><a class="about-news-entry" data-id="<?= $v->about_news_id ?>" data-title="<?= $v->title ?>" data-href="<?=BASE_URL?>about/news/content/<?=$v->url_title?>" href="javascript:void(0)"><?=$v->title?></a></h3>
												<p><?=character_limiter($v->description,150)?></p>
												<a class="button about-news-entry" data-id="<?= $v->about_news_id ?>" data-title="<?= $v->title ?>" data-href="<?=BASE_URL?>about/news/content/<?=$v->url_title?>" href="javascript:void(0)"><i>READ MORE</i></a>
											</div>
										</div>
									</div> 
						<?php 	if( ($count%2)==0 ){ ?> 
							<div class="clearfix"></div>
						<?php }} }else{ ?>
							<div class="col-lg-6 item">No content found.</div>
					   <?php } ?>
						<!-- end items -->
						
					</div>
				</div>
			</div> 	
		</div>

	</div>

	<!-- popups -->
	<div class="lytebox-wrap">
		<!-- upload photo -->
		<div class="popup lytebox-wrapped-content" id="upload">
			<div class="content">
				<button class="popup-close">&times;<span>CLOSE</span></button>
				
				<div class="row">
					<div class="col-sm-8  txtleft">
						<h3>UPLOAD YOUR OWN PHOTO FROM THE EVENT</h3>
						<h2>BE MARLBORO LAUNCH PARTY</h2>
					</div>
					<div class="col-sm-4  txtright">
						<button type="button" class="button browse"><i>BROWSE PHOTO</i></button>
						<input type="file" id="upload-photo" class="hidden"  onchange="PreviewImage();">
					</div>

					<div class="clearfix"></div>

					<div class="upload-preview-fixed">
						<span>PHOTO PREVIEW</span>
					</div>

					<div class="txtcenter">
						<button type="button" class="button close-x"><i>CANCEL</i></button>
						<button type="button" class="button"><i>SAVE</i></button>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<!-- end popup -->

 
	
<!-- main content ends here  -->
<script type="text/javascript">
$(function(){
	$('.browse').click(function(){
		$('#upload-photo').trigger('click');
	});
});
function PreviewImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("upload-photo").files[0]);
    oFReader.onload = function (oFREvent) {
    	$('.upload-preview-fixed').html('<img src="'+oFREvent.target.result+'"/>').show();
    };
};
</script>