<div class="popup" id="upload">
	<div class="content">

		<form action="<?php echo ($row) ?  BASE_URL.'photos/upload_photo/'.$row->backstage_event_id :  BASE_URL.'photos/upload_photo/'; ?>" onsubmit="return onSubmitPhoto();" method="post" target="upload_target" enctype="multipart/form-data">
			
			<button class="popup-close" id="btn-photo-upload-close-popup">&times;<span>CLOSE</span></button>					
			<div class="row">
				<div class="col-sm-8  txtleft">
 					<h2><?=($row) ? $row->title : ''?></h2>
				</div>
				<div class="col-sm-4  txtright">
					<button type="button" class="button browse"><i>BROWSE PHOTO</i></button>
					<small class="red">MAXIMUM FILE SIZE OF 3MB</small>
					<input type="file" id="upload-photo" class="hidden"  onchange="PreviewImage();" name="photo">
				</div>

				<div class="clearfix"></div>

				<div class="upload-preview-fixed">
					<span>PHOTO PREVIEW</span>
				</div>

				<p class="terms">
					Please be reminded that as per the Terms and Conditions, all photos, images or designs submitted will become the property of PM. 
					Please do not upload or submit any photos, images or designs depicting the act of smoking or showing a lit cigarette or minors 
					(persons less than 18 years old). PM reserves to the right to remove or reject any photo, image or design which it, in its sole and 
					absolute discretion, deems offensive, violates the law or is unsuitable in any other manner. The photo you upload will be subject 
					to moderation.
				</p>

				<div class="txtcenter">
					<div class="error" id="error-photo-upload"></div>
					<button type="button" class="button close-x" id="btn-photo-upload-cancel"><i>CANCEL</i></button>
					<button type="submit" class="button" id="btn-photo-upload-save"><i>SAVE</i></button>
				</div>					
			</div>

		</form>

	</div>
</div>
<iframe id="upload_target" name="upload_target"  style="width:0px;height:0px;border:0px solid #fff;"></iframe>
<script type="text/javascript">
$(function(){
	
	$('.browse').click(function(){
		$('#upload-photo').trigger('click');
	});

});

function PreviewImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("upload-photo").files[0]);
    oFReader.onload = function (oFREvent) {
    	$('.upload-preview-fixed').html('<img src="'+oFREvent.target.result+'"/>').show();
    };
}

function onSubmitPhoto()
{
	$('#btn-photo-upload-close-popup').hide();
	$('#btn-photo-upload-cancel').hide();
 	$('#btn-photo-upload-save').addClass('disabled').attr('disabled','disabled').html('<i><img src="<?=BASE_URL?>images/spinner.gif" /> SAVING...</i>');
	return true;
}

function form_response(msg)
{
 	popup.open({type:'alert','message':msg,buttonAlign:'center',align:'center'});
}

function showButtons()
{
	$('.popup-close').show();
}
</script> 
