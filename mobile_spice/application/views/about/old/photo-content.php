 		
<!-- main content starts here  -->
 	<div class="wrapper">
		
		<div class="container main">
			<div class="row">
				<div class="col-md-3 col-lg-2  col-sm-12 side-rail sticky">

					<nav>
						<a href="<?=BASE_URL?>about/photos" class="active"> <i class="glyphicon glyphicon-camera"></i>PHOTOS</a>
						<a href="<?=BASE_URL?>about/videos"> <i class="glyphicon glyphicon-film"></i>VIDEOS</a>
						<a href="<?=BASE_URL?>about/news"> <i class="glyphicon glyphicon-list"></i>NEWS</a>
					</nav>

				</div>

				<div class="col-md-9 col-lg-8  col-md-push-3 col-lg-push-2  main-rail">
					<h2><?=($row) ? $row->title : 'Content not found.'?><h2>
					<div class="article-stat">
						<span>COMMENTS: <?=(int)@$total_comments?></span>
						<span>VIEWS: <?=($row_photo) ? (int)$row_photo->views : '0'?></span>
					</div>
					<figure class="theater">

						<?php if($prev){ ?><div class="arrow left" onclick="redirect('<?=BASE_URL.'about/photos/content/'.$url_title.'/'.$prev->photo_id?>');"></div> <?php } ?>
						<?php if($next){ ?><div class="arrow right" onclick="redirect('<?=BASE_URL.'about/photos/content/'.$url_title.'/'.$next->photo_id?>');"></div> <?php } ?>
	  					<?php if($row_photo && file_exists('uploads/about/photos/'.$row_photo->image)){ ?>
									<img src="<?=BASE_URL.'uploads/about/photos/'.$row_photo->image?>">
						<?php }else{ ?>
									<img src="<?=BASE_URL.DEFAULT_IMAGE?>">
						<?php } ?>

					</figure>
				</div>

				<div class="col-md-8 col-md-push-3 col-lg-push-2">
					<?= show_comments(ABOUT_PHOTOS, $row_photo->photo_id, $row_photo->user_type == 'registrant' ? array('recipient_id' => $row_photo->uploader_id) : array()) ?>
				</div>
			</div>
		</div>

	</div> 

<script type="text/javascript">
$(function(){
	swipePrevNext();
});
function redirect(url){
	top.location.href=url;
}
</script>
	
<!-- main content ends here  --> 