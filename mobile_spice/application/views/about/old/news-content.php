<!-- main content starts here  -->
 	<div class="wrapper">
		
		<div class="container main">
			<div class="row">
				<div class="col-md-3 col-lg-2  col-sm-12 side-rail sticky">
					<nav>
						<a href="<?=BASE_URL?>about/photos"> <i class="glyphicon glyphicon-camera"></i>PHOTOS</a>
						<a href="<?=BASE_URL?>about/videos"> <i class="glyphicon glyphicon-film"></i>VIDEOS</a>
						<a href="<?=BASE_URL?>about/news" class="active"> <i class="glyphicon glyphicon-list"></i>NEWS</a>
					</nav>
				</div>

				<div class="col-md-9 col-lg-8  col-md-push-3 col-lg-push-2  main-rail">
					<h2><?=($row) ? $row->title : 'Content not found.'?></h2>
					<div class="article-stat">
						<span>COMMENTS: <?=(int)@$total_comments?></span>
						<span>VIEWS: <?=($row) ? (int)$row->views : '0'?></span>
					</div>
					<figure class="theater">

						<?php if($prev){ ?>
								<div class="arrow left" onclick="redirect('<?=BASE_URL?>about/news/content/<?=$prev->url_title?>')"></div> 
						<?php } ?>

						<?php if($next){ ?>
								<div class="arrow right" onclick="redirect('<?=BASE_URL?>about/news/content/<?=$next->url_title?>')"></div>
						<?php } ?>	

						<?php if($row && $row->image){ ?>
									<img src="<?=BASE_URL.'uploads/about/news/'.$row->image?>">
						<?php }else{ ?>
									<img src="<?=BASE_URL.DEFAULT_IMAGE?>">
						<?php } ?>
						
					</figure>

					<br>
					
					<article>
						<?=($row) ? $row->description : ''?>					
					</article>
				</div>

				<div class="col-md-8 col-md-push-3 col-lg-push-2">
					<?= show_comments(ABOUT_NEWS, $row->about_news_id, array(), array('category' => 'Comment', 'action' => 'submit', 'label' => 'Commented on About News: '.$row->title, 'value' => 1)) ?>
				</div>
			</div>	
		</div>

	</div>
	<script type="text/javascript">
	$(function(){
		swipePrevNext();
	});

	function redirect(url){
		top.location.href=url;
	}
	</script>

	

 