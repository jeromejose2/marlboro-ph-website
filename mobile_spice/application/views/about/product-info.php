
<!-- main content starts here  -->
	<div class="wrapper">
		
		<div class="container main">
			<div class="row">
				<?php $this->load->view('about/nav') ?>

				<div class="clearfix"></div>

				<div class="col-md-9 col-lg-10  col-md-push-3 col-lg-push-2 main-rail">
					<!-- content -->
					<div class="row">
						<?php if ($products): ?>
						<div class="col-md-5">
							<div id="round-about-container">
								<div class="arrow left"></div>
								<div class="arrow right"></div>
								<ul class="product-roundabout">
									<?php foreach ($products as $product): ?>
									<!-- per item -->
									<li data-content-image="<?= $product['product_info_content_image'] ?>" data-has-media="<?= $product['product_media_num'] ?>" data-title-type="<?= $product['product_info_title_type'] ?>" data-id="<?= $product['product_info_id'] ?>" data-desc="<?=htmlentities($product['product_info_description']) ?>" data-title="<?= $product['product_info_title'] ?>">
										<img class="img-responsive" src="<?= BASE_URL ?>uploads/product_info/<?= $product['product_info_image'] ?>">
									</li>
									<?php endforeach ?>
								</ul>
							</div>
						</div>
						<div class="col-md-7 leathered product-info">
							<div id="product-info" class="leathered-content">
								<div class="row">
									<div id="product-info-content-container" class="col-sm-12">
										<h2 id="product-info-title"><!-- content here via javascript --></h2>
										<div id="product-info-content"><!-- content here via javascript --></div>
									</div>
									<div id="product-info-content-image" class="col-sm-5"><br><br><img class="image-responsive" src=""></div>
								</div>
								<button class="button" type="button" data-id="<?= $products[0]['product_info_id'] ?>" id="product-media"><i>EXPLORE</i></button>
							</div>
						</div>
						<?php else: ?>
						<h3>No Record(s) found</h3>
						<?php endif ?>
						<div class="clearfix"></div>
					</div>
					<!-- end content -->
				</div>
			</div>
		</div>
	</div>

	<script src="<?=BASE_URL?>/scripts/jquery.roundabout.js"></script>
	<script>
	$(function(){
		
		var roundabout = $('.product-roundabout').roundabout(function(target){
			
			if (target.data('content-image')) {
				$("#product-info-content-container").removeClass("col-sm-12").addClass("col-sm-7");
				$("#product-info-content-image").show().find("img").prop("src", '<?= BASE_URL ?>uploads/product_info/' + target.data('content-image'));
			} else {
				$("#product-info-content-container").removeClass("col-sm-7").addClass("col-sm-12");
				$("#product-info-content-image").hide();
			}
			if (target.data('title-type') == 2) {
				$('#product-info-title').html('<img class="img-responsive" src="<?= BASE_URL ?>uploads/product_info/' + target.data('title') + '">');
			} else {
				$('#product-info-title').html(target.data('title'));
			}
			//$('#product-info-content').html(target.data('desc'));
			var html = $('<div></div>').html(target.data('desc'));
			$('#product-info-content').html('').append(html);

			$("#product-media").data("id", target.data('id'));
			if (target.data('has-media')) {
				$("#product-media").show();
			} else {
				$("#product-media").hide();
			}

		});

		$('#round-about-container .left').click(function(){
			roundabout.next();
		});

		$('#round-about-container .right').click(function(){
			roundabout.prev();
		});

		$(".product-roundabout").swipe({
			swipeLeft : function(){
				roundabout.next();
			},
			swipeRight : function(){
				roundabout.prev();
			}
		});

		$('#product-media').click( function() {
			var id = $(this).data("id");
			popup.open({url:'<?= BASE_URL ?>about/gallery?id=' + id});
		});
	});
	</script>


 