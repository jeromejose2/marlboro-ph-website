<div class="col-md-3 col-lg-2  col-sm-12 side-rail sticky">
	<nav>
		<a href="<?= BASE_URL ?>about/dont-be-a-maybe" <?= $this->uri->segment(2) == 'dont-be-a-maybe' ? 'class="active"' : '' ?>> <i class="glyphicon glyphicon-remove-circle"></i>DON'T BE A MAYBE</a>
		<a href="<?= BASE_URL ?>about/brand_history" <?= $this->uri->segment(2) == 'brand_history' ? 'class="active"' : '' ?>> <i class="glyphicon glyphicon-time"></i>BRAND HISTORY</a>
		<a href="<?= BASE_URL ?>about/product_info" <?= $this->uri->segment(2) == 'product_info' ? 'class="active"' : '' ?>> <i class="glyphicon glyphicon-info-sign"></i>PRODUCT INFO</a>
	</nav>
</div>
