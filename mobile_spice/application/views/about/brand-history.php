
<!-- main content starts here  -->
 	<div class="wrapper">
		
		<div class="container main">
			<div class="row">
				<?php $this->load->view('about/nav') ?>

				<div class="clearfix"></div>

				<div class="col-md-9 col-lg-10  col-md-push-3 col-lg-push-2 main-rail">
					<!-- content -->
					<?php if (!empty($history)): ?>
					<div class="leathered" id="brand-timeline-content">
						<div class="leathered-content">
							<div class="row">
								<div class="col-sm-12 text-center" id="brand-history-preloader">
									<br><br><br><img src="<?= BASE_URL ?>images/preloader.gif"><br><br><br>
								</div>
								<div class="col-sm-5" id="brand-history-media-container" style="display:none;">
									<img id="brand-history-media" src="<?= BASE_URL ?>uploads/brand_history/<?= $history[0]['brand_history_media'] ?>">
								</div>
								<div class="col-sm-7" id="brand-history-desc-container" style="display:none;">
								<h2 id="brand-history-title"><?= $history[0]['brand_history_title'] ?></h2>
								<h3 id="brand-history-year" class="red"><?=$history[0]['brand_history_year']?></h3>
								<div id="brand-history-description"><?= $history[0]['brand_history_description'] ?></div>
									<a id="brand-history-explore-btn" href="<?= $history[0]['brand_history_explore_link'] ?>" class="button <?= !$history[0]['brand_history_explore_copy'] || !$history[0]['brand_history_explore_link'] ? 'hidden' : '' ?> "><i><?= $history[0]['brand_history_explore_copy'] ?></i></a>
								</div>
							</div>
						</div>
					</div>


				    <div class="brand-timeline">
				    	<div class="arrow left"></div>
				    	<div class="arrow right"></div>
					    <div class="timeline">
					    	<?php foreach ($history as $k => $record): ?>
					    	<div data-key="<?= $k ?>" class="frame <?= !$k ? 'active' : '' ?>">
					    		<?php if ($record['brand_history_year_desc']): ?>
						    	<span><?= $record['brand_history_year_desc'] ?></span> 
						    	<?php else: ?>
								<span><?= ($k + 1) === count($history) ? 'PRESENT' : $record['brand_history_year'].'<sub>s</sub>' ?></span> 
						    	<?php endif ?>
						    	<div class="dot"></div> 
						    	<div class="line"></div> 
							</div>
					    	<?php endforeach ?>
					    </div>
				    </div>
					<?php else: ?>
					<h2>No Records</h2>
					<?php endif ?>
				    <br><br><br>
				    <div class="clearfix"></div>
					<!-- end content -->
				</div>
			</div> 	
		</div>

	</div>


	<script>
	var brandHistoryData = <?= json_encode($history) ?>;
	$(function(){
		var count = 0;
		$('.timeline').owlCarousel({
			items : 5, 
			itemsDesktop : false, 
			itemsDesktopSmall : false, 
			itemsTablet: false,
			itemsMobile : [360,3],
	        afterInit : function(el){
	              el.trigger('owl.jumpTo',this.$owlItems.length-1);
	              var items = this.$owlItems;
	              setTimeout(function(){
	              	$(items[items.length-1]).trigger('click');
	              },100)
	        }
		});

		var owl = $('.timeline').data('owlCarousel');

		$('.brand-timeline .arrow.left').click(function(){
		    owl.prev();
		});

		$('.brand-timeline .arrow.right').click(function(){
		    owl.next();
		});

		$('.timeline .owl-item').click(function(){
			var data = brandHistoryData[$(this).find(".frame").data("key")];
			$("#brand-history-preloader").show().siblings().hide();

			$("#brand-history-title").html(data.brand_history_title);
			$("#brand-history-year").html(data.brand_history_year);
			$("#brand-history-description").html(data.brand_history_description);

			if (data.brand_history_explore_copy && data.brand_history_explore_link) {
				$("#brand-history-explore-btn").prop("href", data.brand_history_explore_link).html('<i>'+data.brand_history_explore_copy+'</i>').removeClass('hidden');
			} else {
				$("#brand-history-explore-btn").addClass('hidden');
			}

			if (!data.brand_history_media) {
				$("#brand-history-media").prop("src", "");
				setTimeout( function () {
					$("#brand-history-preloader").fadeOut( function() {
						$("#brand-history-desc-container").removeClass("col-sm-7").addClass("col-sm-12").fadeIn();
					});
				}, 300);
			} else {
				$("#brand-history-desc-container").removeClass("col-sm-12").addClass("col-sm-7");
				$("#brand-history-media").unbind("load")
					.bind("load", function() {
						setTimeout( function () {
							$("#brand-history-preloader").fadeOut( function() {
								$(this).siblings().fadeIn();
							});
						}, 300);
					}).prop("src", BASE_URL + "uploads/brand_history/" + data.brand_history_media);
			}

			$('.timeline .owl-item .frame').removeClass('active');
			$('.frame',this).addClass('active');
		});


		<?php if (isset($history[0]) && $history[0]['brand_history_media']): ?>
		setTimeout( function() {
			$("#brand-history-preloader").fadeOut( function() {
				$(this).siblings().fadeIn();
			});
		}, 300);
		<?php else: ?>
		setTimeout( function () {
			$("#brand-history-preloader").fadeOut( function() {
				$("#brand-history-desc-container").removeClass("col-sm-7").addClass("col-sm-12").fadeIn();
			});
		}, 300);
		<?php endif ?>

	});
	</script>

 