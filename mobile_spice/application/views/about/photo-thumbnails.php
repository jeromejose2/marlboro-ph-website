	
<!-- main content starts here  -->
 	<div class="wrapper">
		
		<div class="container row main">
			
			<div class="col-md-3 col-lg-2  col-sm-12 side-rail sticky">
				<nav>
					<a href="<?=BASE_URL?>about/photos" class="active"> <i class="glyphicon glyphicon-camera"></i>PHOTOS</a>
					<a href="<?=BASE_URL?>about/videos"> <i class="glyphicon glyphicon-film"></i>VIDEOS</a>
					<a href="<?=BASE_URL?>about/news"> <i class="glyphicon glyphicon-list"></i>NEWS</a>
				</nav>
			</div>

			<div class="col-md-9 col-lg-10  col-md-push-3 col-lg-push-2 main-rail">
				<div class="pull-left">
					<h2><?=($row) ? $row->title : 'Content not found.'?></h2>
				</div>
				<div class="upload-from-event">
					<button type="button" class="button-small" onclick="popup.open({url:'<?=BASE_URL?>photos/upload_form/<?=$about_event_id?>'})"><i>UPLOAD YOUR PHOTOS</i></button>
				</div>
				<div class="clearfix"></div>

				<div class="row row-listing">

					<!-- start items -->
					<?php if(@$row_photos->num_rows()){ 

							foreach($row_photos->result() as $p){ ?>

								<div class="col-xs-6 col-sm-4 item">
									<a href="<?=BASE_URL.'about/photos/content/'.$url_title.'/'.$p->photo_id?>">
										<div class="thumb-holder">
										<?php if(file_exists("uploads/about/photos/thumbnails/331_176_".$p->image)){ ?>
												<div class="thumbnail" style="background-image:url('<?=BASE_URL."uploads/about/photos/thumbnails/331_176_".$p->image?>')"></div>
									 	<?php } ?>
										</div>
										<div class="clearfix"></div>
									</a>
								</div>

							<?php
							}
						} ?>
					<!-- end items -->
					
				</div>
			</div>
		</div>

	</div>

	
 
	
<!-- main content ends here  -->
