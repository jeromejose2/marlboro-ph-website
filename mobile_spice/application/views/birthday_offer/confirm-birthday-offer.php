<div class="popup">
	<div class="content">
		<button class="popup-close">&times;<span>CLOSE</span></button>
		
		<h2 class="txtcenter">EDIT MAILING ADDRESS</h2>
		<form action="<?=BASE_URL?>birthday_offer/confirm_offer" method="post"  target="upload_target" onsubmit="return onSubmitForm();">
		<input type="hidden" name="prize_id">
		<div class="row  confirm-email">
			<div class="col-sm-6">
				<h3>STREET NAME</h3>
				<input type="text" name="street_name" value="<?=$row->street_name?>">
			</div>

			<div class="col-sm-6">
				<h3>BARANGAY</h3>
				<input type="text" name="barangay" value="<?=$row->barangay?>">
			</div>

			<div class="col-sm-6">
				<h3>PROVINCE</h3>
				<select name="province">
 					<?php  if($provinces->num_rows()){
								foreach($provinces->result() as $v){ 
										if($row->province === strtolower($v->province_id)){?>
											<option selected="selected" value="<?=$v->province_id?>"><?=$v->province?></option>
								<?php	}else{ ?>
											<option value="<?=$v->province_id?>"><?=$v->province?></option>
								  <?php }
								}
							}  ?>
				</select>
			</div>

			<div class="col-sm-6">
				<h3>CITY/TOWN</h3>
				<select name="city">
 					<?php if($cities){
							foreach($cities->result() as $c){ 
									if($row->city===$c->city_id){?>
										<option selected="selected" value="<?=$c->city_id?>"><?=$c->city?></option>
							<?php	}else{ ?>
										<option value="<?=$c->city_id?>"><?=$c->city?></option>
							  <?php }
							}
						}  ?>
				</select>
			</div>

			

			<div class="col-sm-6">
				<h3>ZIP CODE:</h3>
				<input type="text" name="zip_code" value="<?=$row->zip_code?>">
			</div>
		</div>

		<div class="txtcenter">
			<div class="error" id="birthday-offer-error"></div>
			<button class="button"  type="submit" id="birthday-offer-btn"><i>Submit Confirmation</i></button>
		</div>

		</form>
	</div>
</div>
<iframe id="upload_target" name="upload_target" style="width:0px;height:0px;border:none;"></iframe>
<script type="text/javascript"> 

$(function(){

	$('select[name="province"]').change(function(){
		var province = $(this).val();

		$.getJSON(BASE_URL+'profile/get_cities?province='+province,function(cities){
			var items = ['<option value=""></option>'];
 
			 $.each(cities, function(i,obj) {
 				items.push('<option value="'+obj.city_id+'">'+obj.city+'</option>');
			});
			 $('select[name="city"]').html(items.join(''));
		});
	})

});



function onSubmitForm()
{
	document.getElementById('birthday-offer-btn').disabled = true;
	$('#birthday-offer-btn').attr('disabled','disabled').html('<i><img src="<?=BASE_URL?>images/spinner.gif" /> SUBMITTING...</i>');
	return true;
}

function form_response(msg)
{
 	popup.open({type:'alert','message':msg,buttonAlign:'center',align:'center',okCaption:'CONTINUE'});
}

</script>