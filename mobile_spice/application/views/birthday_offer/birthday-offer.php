 
<div class="popup">
	<div class="content txtcenter">
	    <button class="popup-close">&times;<span>CLOSE</span></button> 				
		<div class="row">
			<?php $col = 12; 

			if($birthday_offer){?>

				<?php if(file_exists('uploads/prize/'.$birthday_offer->prize_image) && $birthday_offer->prize_image){ $col = 6;?>
						<div class="col-sm-<?=$col?>">
							<img src="<?=BASE_URL.'uploads/prize/'.$birthday_offer->prize_image?>" width="100%"/>
						</div>
				<?php } ?>			

					<div class="col-sm-<?=$col?>">
						<h2 class="red">HAPPY BIRTHDAY</h2>
						<h3><?=$birthday_offer->description?></h3>

						<?php if($birthday_offer->send_type=='Delivery'){ ?>
								<p class="small">Your gift will be delivered to your registered address</p>
								<a href="javascript:void(0)" class="button" onclick="popup.open({url:'<?=BASE_URL?>birthday_offer/confirm_offer'})"><i>CONFIRM MAILING ADDRESS</i></a>
						<?php }else{ ?>
								<p class="small"><?=$birthday_offer->redemption_address?></p>
								<form action="<?=BASE_URL?>birthday_offer/confirm_offer?flash_offer=<?=$flash_offer?>" method="post"  target="upload_target" onsubmit="return onSubmitForm();">
									<input type="hidden" name="confirm_redemption" value="1">
									<button class="button" type="submit" id="birthday-offer-btn"><i>CONFIRM</i></button>
		 						</form>
						<?php } ?>						
					</div>
					
			<?php }else{ echo 'Birthday offer not found.'; } ?>
		</div>

	</div>
</div>
<iframe id="upload_target" name="upload_target" style="width:0px;height:0px;border:none;"></iframe>
<script type="text/javascript">
 



function onSubmitForm()
{
 	$('#birthday-offer-btn').addClass('disabled').attr('disabled','disabled').html('<i><img src="<?=BASE_URL?>images/spinner.gif" /> CONFIRMING...</i>');
	return true;
}

function form_response(msg)
{
 	popup.open({type:'alert','message':msg,buttonAlign:'center',align:'center',okCaption:'CONTINUE',
 				onConfirm: function(){
 					<?php if($flash_offer){ ?>
 							popup.open({url:"flash_offer/get_flash_offer?prize_id=<?=$flash_offer?>&rand=<?=uniqid()?>"});
 					<?php } ?>
 				}
 			});
}

</script>