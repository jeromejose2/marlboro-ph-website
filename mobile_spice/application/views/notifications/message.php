<div class="wrapper ">
	<div class="row container messages">
		<?php $this->load->view('messages/nav', ['inner' => true]) ?>
		<div class="col-md-8 col-md-push-3 inbox-holder" id="notification-content">
			<ul class="inbox-message">
				<li>
					<span class="pull-left"> <a class="button-small visible-xs" href="<?= BASE_URL ?>notifications"><i>BACK TO NOTIFICATIONS</i></a> </span>
					<span class="pull-right"> <button id="delete-notification" class="button-small" type="button"><i>DELETE</i></button> </span>
					<div class="clearfix"></div>
				</li>
				<li class="head">
					<small><?= date("M d, Y \a\\t h:i A", strtotime($notification['notification_date_created'])) ?></small>
				</li>
				<li>
					<span class="msg"><?= $notification['notification_message'] ?></span>
				</li>
			</ul>
		</div>
	</div>
</div>