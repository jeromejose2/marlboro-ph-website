<div class="wrapper ">
	<div class="row container messages">
		<?php $this->load->view('messages/nav') ?>
		<div class="col-md-8 col-md-push-3 inbox-holder">
			<ul class="inbox">
			<?php if ($notifications): ?>
				<li>
					<span class="pull-right"> <button class="button-small" id="delete-selected-notifications" type="button"><i>DELETE SELECTED</i></button> </span>
					<div class="clearfix"></div>
				</li>
					<?php foreach ($notifications as $notification): ?>
				<li <?= !$notification['notification_status'] ? 'class="unread"' : '' ?>>
					<input type="checkbox" class="notification-message" name="delete" value="<?= $notification['notification_id'] ?>">
					<a href="<?= BASE_URL ?>notifications/message/<?= $notification['notification_id'] ?>">
						<!-- <span class="name">ROBERT CRUZ</span> -->
						<span class="date"><?= date('M d, Y h:i A', strtotime($notification['notification_date_created'])) ?></span>
						<!-- <span class="subject">Hello.</span> -->
						<span class="msg"><?= ellipsize($notification['notification_message'], 50) ?></span>
					</a>
				</li>
				<?php endforeach ?>
			<?php else: ?>
				<li style="border-bottom: 0;">
					<h3>You don't have notifications</h3>
				</li>
			<?php endif ?>
			</ul>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= BASE_URL ?>scripts/notifications.js"></script>