<?xml version="1.0" encoding="utf-8" ?>
<data>
	<global>
		
		<main homePage="<?= BASE_URL ?>"
				name="uploader"
				appName=""
				amf="gateway"
				classFunction="GiidService/submit_photo" />
		
	</global>
	<image>
		<details visible="true" scale="1" conX="0" conY="0" conWidth="380" conHeight="230" mask="0,0,380,230" webcam="640,480" />
	</image>
	<app>
		<feature type="base64" url="<?= BASE_URL ?>swf/base64.swf"/>
	</app>
	<captions>
	</captions>
	<prompts>
		<prompt sizeError="onSizeError()"
					onSaveBA="onSaveBA"
					captionError="captionError()"
					webcamProblem="webcamProblem()"
					onLoad="enableUpload()"
					onImageRequire="onImageError()"
					onImageUpload="onImageUpload()"
					onUploadStart="onUploadStart()"
					onUploadComplete="onUploadComplete()"
					onRedirect="onRedirect()" />
	</prompts>
	<assets>
 	</assets>
</data>