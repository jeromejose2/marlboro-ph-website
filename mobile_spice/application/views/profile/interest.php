<script type="text/javascript" src="scripts/angularjs/angular.js"></script>

<div class="wrapper" ng-app="profiling" ng-controller="profilingCtrl">		

	<div class="profiling">
		<!--
		<header>
			<h2>PROFILING MODULE</h2>
		</header>
	-->

		

 
		<div class="row">
			<div class="col-md-8 interest-col">
				<h3>SELECT YOUR INTERESTS</h3>
				<span class="note">NOTE : You can select up to three interests</span>

				<img ng-if="!interests" src="images/preloader-captcha.gif" />


 				<ul class="items">

					<li ng-class="interest.is_selected" ng-repeat="(key,interest) in interests">
						<div class="icon">
							<img ng-src="{{interest.image}}">
						</div>
						<span class="name">{{interest.interest_name}}</span> 
						<ol>
							<li ng-class="sub_interest.is_selected" ng-click="selectInterest(interest.interest_name,interest.interest_id,sub_interest.id,key)" ng-repeat="sub_interest in interest.sub_interests">{{sub_interest.name}}</li>
						</ol>
					</li>

				</ul>
			</div>
			<div class="col-md-4 rank-col">
				<div class="rank">
					<h3>RANK YOUR INTERESTS</h3>
					<p>Select your interests and rank them according to your preference.</p>
					<p>Drag interests in order of your choice.</p>

					<ul data-sortable="dragControlListeners" ng-model="categoryOrder">
						<li ng-repeat="(key, item) in categoryOrder" data-sortable-item ><div data-sortable-item-handle><i>{{key + 1}}</i> <span>{{item.name}}</span></div><b ng-click="removeInterest(item.id)">&times;</b></li>
					</ul>

					<div class="action">
						<button class="save" ng-click="saveInterests()">
							<img ng-if="user.saving" src="images/spinner.gif" /> 
							<span ng-if="!user.saving" class="glyphicon glyphicon-floppy-disk"></span> SAVE
						</button>
						<button class="clear" ng-click="clearInterests()"><span class="glyphicon glyphicon-remove"></span> clear</button>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

</div>

<link rel="stylesheet" type="text/css" href="scripts/angularjs/ng-sortable.min.css">
<script type="text/javascript" src="scripts/angularjs/ng-sortable.min.js"></script>
<script type="text/javascript" src="scripts/profiling/profilingApp.js"></script>
