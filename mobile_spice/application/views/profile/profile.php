
<!-- main content starts here  -->
	<div class="wrapper">		

			<div class="row profile-book">
				<a class="edit-profile" href="#">EDIT PROFILE</a>
				<div class="col-md-6">
					<div class="row page">
						<div class="col-md-6 profile-pic">
								<figure class="col-xs-6 col-sm-4 col-md-12">
									<?php if(file_exists('uploads/profile/'.$user_id.'/'.$row->current_picture) && $row->current_picture){ ?>
											<img src="<?=BASE_URL?>uploads/profile/<?=$user_id.'/'.$row->current_picture ?>" />
									<?php }else{ ?>
											<img src="<?=BASE_URL?>images/default-profile-photo.png" />
									<?php } ?>									
									<figcaption onclick="popup.open({url:'<?=BASE_URL?>profile/change_photo_form',blurClose : false});"  class="change-photo">CHANGE PHOTO</figcaption>
								</figure>

								<div class="col-xs-6 col-sm-8 col-md-12">
									<h2 class="visible-sm visible-xs"><?=$row->first_name.' '.$row->middle_initial.' '.$row->third_name?></h2>
									<div class="visible-xs">
										<a href="#" class="edit-profile"><i class="glyphicon glyphicon-pencil"></i> EDIT PROFILE</a>
										<a href="#" onclick="popup.open({url:'<?=BASE_URL?>profile/change_photo_form',blurClose : false});" class="change-photo"><i class="glyphicon glyphicon-camera"></i> CHANGE PHOTO</a>
									</div>
								</div> 
						</div>

						<div class="clearfix visible-xs"></div>
						<div class="col-sm-7 col-md-6 maybe-list">

							<ul class="list-unstyled">
								<?php 
								$u_statements = array();

								if($user_statements->num_rows()){
										foreach($user_statements->result() as $v){ $u_statements[] = $v->statement_id; ?>
												<li><span>maybe</span> <?=$v->statement?></li>
									<?php }
								} ?>

							</ul> 

							<h3>cross out maybe in your life</h3>
							<?php if($statements->num_rows()){ ?>
							<button type="button" class="button" onclick="popup.open({url:'<?=BASE_URL?>profile/select_statements'})"><i>CLICK HERE TO CHOOSE</i></button>
							<?php } ?>
						</div>
					</div>
				</div>

				<div class="clearfix visible-sm"></div>

				<div class="col-md-6 page">
					<h2 class="hidden-sm hidden-xs"><?=$row->first_name.' '.$row->middle_initial.' '.$row->third_name?></h2>
					<form autocomplete="off" action="<?=BASE_URL.'profile/update_profile'?>" onsubmit="return profile_update_details();" method="POST" target="upload_target">						

						<div class="error" id="profile-error"></div>
						<ul class="profile-details">
						<li>
							<p class="other-fields small">To change your name, e-mail address and date of birth, 
							please contact our adult restricted hotline at (02) 
							895-9999 or 1-800-10-895-9999 (toll-free for outside Metro Manila) or text 
							MBChange to 0908-895-9999.</p>
						</li>
				 
						<li>

							<span class="name">NICK NAME:</span>
							<span class="value"><?=$row->nick_name?></span>
							<input type="text" name="nick_name" value="<?=$row->nick_name?>">

						</li>

						<li>
							<span class="name">EMAIL ADDRESS:</span>
							<span class=""><?=$row->email_address?></span>
						</li>

						<li>
							<span class="name other-fields">Password:</span>
 							<input type="password" autocomplete="off" placeholder="Current Password" value="" name="old_password" >
							<ul class="other-fields">

								<li>
									<span class="name">&nbsp;</span>
									<input type="password" autocomplete="off"  placeholder="New Password" value="" name="password" >
								</li>

								<li>
									<span class="name">&nbsp;</span>
									<input type="password" autocomplete="off" placeholder="Confirm New Password" value="" name="cpassword">
								</li>
							</ul>
						</li>

						<li>
							<span class="name">GENDER:</span> 
							<span class="value"><?=($row->gender=='M')? "Male" : "Female"?></span>
							
							<label class="gender"><input type="radio" name="gender" <?=($row->gender=='M')? "checked" : ""?> value="M"> Male</label>
							<label class="gender"><input type="radio" name="gender" <?=($row->gender=='F')? "checked" : ""?> value="F"> Female</label>

							<!--
							<select name="gender">
								<option value="M" <?=($row->gender=='M')? "selected" : ""?>>Male</option>
								<option value="F" <?=($row->gender=='F')? "selected" : ""?>>Female</option>
							</select>
							-->
 						</li>

						<li>
							<span class="name">MOBILE NUMBER:</span>
							<span class="value"><?=$row->mobile_phone?></span>
							<input type="text" name="mobile_phone" value="<?=$row->mobile_phone?>">
						</li>

						<li>
							<span class="name">DATE OF BIRTH:</span>
							<span class=""><?=$row->date_of_birth?></span>
						</li>

						<li>
							<span class="name">MAILING ADDRESS:</span>
							<span class="value">
								<?php
								$user_details = json_decode(json_encode($row),TRUE);
 								$mailing_address = array_filter(array_intersect_key($user_details,array('street_name'=>null,'barangay'=>null,'city_name'=>null,'province_name'=>null)));
								echo implode(', ',$mailing_address);
								?>
							</span>

							<ul class="other-fields">

								<li>
									<span class="name">ADDRESS:</span>
									<input type="text" name="street_name" value="<?=$row->street_name?>">
								</li>

								<li>
									<span class="name">BARANGAY:</span>
									<input type="text" name="barangay" value="<?=$row->barangay?>">
								</li>


								<li>
									<span class="name">PROVINCE:</span>
									<select name="province">
											<option value=""></option>
										<?php  if($provinces->num_rows()){
													foreach($provinces->result() as $v){ 
															if($row->province===$v->province_id){?>
																<option selected="selected" value="<?=$v->province_id?>"><?=$v->province?></option>
													<?php	}else{ ?>
																<option value="<?=$v->province_id?>"><?=$v->province?></option>
													  <?php }
													}
												}  ?>
										

									</select>

								</li>

								<li>
									<span class="name">CITY:</span>
									<select name="city">
										<option value=""></option>
										<?php if($cities){
													foreach($cities->result() as $c){ 
															if($row->city===$c->city_id){?>
																<option selected="selected" value="<?=$c->city_id?>"><?=$c->city?></option>
													<?php	}else{ ?>
																<option value="<?=$c->city_id?>"><?=$c->city?></option>
													  <?php }
													}
												}  ?>


									</select>
								</li>

								

								<li>
									<span class="name">ZIP CODE:</span>
									<input type="text" name="zip_code" value="<?=$row->zip_code?>">
								</li>

							</ul>

						</li>
						<li class="other-fields">
							<br>
							<ul class="list-unstyled">
								<li>WHAT BRAND DO YOU SMOKE MOST FREQUENTLY?</li>
								<li>
									<span class="name"></span>
									<select name="current_brand">
										<option value="">---</option>
										<?php if($brands->num_rows()){
												foreach($brands->result() as $v){ 
															if(strtolower($row->current_brand) === strtolower($v->brand_id)){?>
																<option selected="selected" value="<?=$v->brand_id?>"><?=$v->brand_name?></option>
													<?php	}else{ ?>
																<option value="<?=$v->brand_id?>"><?=$v->brand_name?></option>
													  <?php }
												}
										} ?>
									</select>
								</li>
								<li>WHAT OTHER BRANDS DO YOU SMOKE ASIDE FROM YOUR REGULAR BRAND?</li>
								<li>
									<span class="name"></span>
									<select name="first_alternate_brand">
										<option value="">---</option>
										<?php if($brands->num_rows()){
												foreach($brands->result() as $v){ 
															if(strtolower($row->first_alternate_brand) === strtolower($v->brand_id)){?>
																<option selected="selected" value="<?=$v->brand_id?>"><?=$v->brand_name?></option>
													<?php	}else{ ?>
																<option value="<?=$v->brand_id?>"><?=$v->brand_name?></option>
													  <?php }
												}
										} ?>
									</select>
								</li>
								<li>WHAT WHOULD YOU DO IF YOUR REGULAR BRAND IS UNAVAILABLE?</li>
								<li>
									<?php if($alternate_purchase->num_rows()){
											foreach($alternate_purchase->result() as $v){ ?>
												<label class="list"><input type="radio" <?=($row->alternate_purchase_indicator==$v->alternate_id) ? 'checked="checked"' : '' ?> name="alternate_purchase_indicator" value="<?=$v->alternate_id?>"/><?=$v->alternate_value?></label>
											<?php }
									} ?>
									
								</li>
							</ul>
						</li>

						<li class="other-fields">
							<button class="button cancel-profile-update" type="button"><i>CANCEL</i></button>
							<button class="button update" id="update-profile" type="submit"><i>SAVE</i></button>
						</li>

					</ul>

					</form>

					<small class="unsubcribe">
						<b>How to Unsubscribe from our Database</b>

						PM is committed to respecting and protecting your privacy. 

						Unless otherwise provided, none of the information you provide 

						will be sold or transferred to non-affiliated parties of PM without your consent. 

						If you wish to withdraw your name from our database, please call 

						our adult-restricted hotline 895-9999 (Metro Manila) or 1-800-10-895-9999 

						(Provincial Toll-Free) or text MLIST &lt;FULL NAME&gt;, &lt;DATE OF BIRTH IN MM/DD/YYY&gt;, 

						&lt;MESSAGE&gt; to 0908-895-9999 (For example: MLIST JUAN DELA CRUZ, 08/12/1982, 

						HOW DO I GET OFF THE LIST?

					</small>
				</div>
			</div>
	</div>
 

	<iframe id="upload_target" name="upload_target"  style="width:0px;height:0px;border:none;"></iframe> 

<!-- main content ends here  --> 
<script type="text/javascript" src="<?=BASE_URL?>scripts/swfobject.js"></script>
<script type="text/javascript" src="<?=BASE_URL?>swf/flash.js"></script>
<script type="text/javascript" src="<?=BASE_URL?>scripts/profile.js"></script>