<div class="popup" id="webcam">
	<form action="<?=BASE_URL.'profile/swf'?>" target="upload_target" onsubmit="return upload_via_webcam();" method="post" enctype="multipart/form-data">
		<div class="content txtcenter">
			<button type="button" class="popup-close" id="take-photo-close-popup">&times;</button>
			<h2>TAKE PHOTO</h2>
			<div class="upload-webcam" id="flashcontent"></div>
			<div class="error" id="error-via-webcam"></div>
			<div class="alert alert-info hidden">Successfully captured, you may submit your profile photo.</div>
			<button type="button" class="button " onclick="capturePhoto();" id="capture-photo-btn"><i>TAKE PHOTO</i></button>
			<button type="submit" class="button hidden" id="take-photo-btn"><i>SUBMIT</i></button>
		</div>

	<input type="hidden" name="imageString"/>
	</form>
</div>
<script type="text/javascript">

initFlash();

function capturePhoto()
{

	var btn_text = $('#capture-photo-btn').html();
  
	if(btn_text=='<i>TAKE PHOTO</i>'){
	   $('#capture-photo-btn').html('<i>CHANGE</i>');
	   $('#take-photo-btn').removeClass('hidden');
	   $('.alert-info').removeClass('hidden');
       getFlash("flash").captureImage();
	}else if(btn_text=='<i>CHANGE</i>'){
		$('#capture-photo-btn').html('<i>TAKE PHOTO</i>');
		$('#take-photo-btn').addClass('hidden');
		$('.alert-info').addClass('hidden');
 	    getFlash("flash").openWebcam();	   
	} 
	
}

function upload_via_webcam()
{
	 
	$('#capture-photo-btn').hide();
	$('#take-photo-close-popup').hide();
	$('#take-photo-btn').html('<i><img src="'+BASE_URL+'images/spinner.gif" /> Saving...</i>');
 	
	return true;
}

function showButtons()
{
	$('#capture-photo-btn').show();
	$('#take-photo-close-popup').show();
}

</script>
