<div class="popup" id="popup-change-photo">

	<div class="content txtcenter">

		<button class="popup-close" id="change-photo-popup-close">&times;<span>CLOSE</span></button>

		<h2>UPLOAD PHOTO</h2>

		

		<div class="row">

			<div class="col-sm-5">

				<div class="photo-preview"></div>

			</div>

			<div class="col-sm-7">

				<form action="<?=BASE_URL.'profile/change_photo'?>" target="upload_target" onsubmit="return validate_user_photo();" method="post" enctype="multipart/form-data">
					<div class="row zero">

					

						<div class="col-sm-6">

							<button type="button" class="button browse"><i>CHOOSE FILE</i></button>

							<small class="red">MAXIMUM FILE SIZE OF 3MB</small> 

						</div>

						<div class="col-sm-6 hidden-xs  hidden-sm hidden-md">

							<button type="button" class="button" id="use-webcam-btn" onclick="popup.open({url:'<?=BASE_URL?>profile/change_photo_webcam',blurClose : false})"><i>USE WEBCAM</i></button>

							<small class="red">UPLOAD PHOTO USING WEBCAM</small>

							<input type="file" name="user_photo" id="upload-photo" class="hidden"  onchange="PreviewImage();">

						</div>

					

				</div>

				<p class="terms">Please be reminded that as per the Terms and Conditions, all photos, 

					images or designs submitted will become the property of PM. 

					Please do not upload or submit any photos, images or designs 

					depicting the act of smoking or showing a lit cigarette or minors 

					(persons less than 18 years old). PM reserves to the right to 

					remove or reject any photo, image or design which it, in its sole 

					and absolute discretion, deems offensive, violates the law or 

					is unsuitable in any other manner. 

					The photo you upload will be subject to moderation.</p>

				<div class="error" id="user_photo_error"></div>
				<button type="submit" class="button" id="change-photo-btn"><i>SAVE</i></button>

				</form>

			</div>

		</div>

	</div>

</div>
<script type="text/javascript">

$('.browse').click(function(){

	$('#upload-photo').trigger('click');

});


function submit_photo_viaWebCam(token)
{
	$('#take-photo-btn').addClass('disabled').attr('disabled','disabled').html('<i><img src="'+BASE_URL+'images/spinner.gif" /> Uploading...</i>');
	$('#take-photo-close-popup').hide();
	getFlash("flash").forceSubmit({token:token});
}

function validate_user_photo()
{
	var empty = /^\s*$/;

	if(empty.test($('input[name="user_photo"]').val())){
		$('#user_photo_error').html('<span>Please upload profile photo</span>').show();
		return false;
	}else{
		$('#user_photo_error').html('');
		$('#change-photo-btn').addClass('disabled').attr('disabled','disabled').html('<i><img src="'+BASE_URL+'images/spinner.gif" /> SAVING...</i>');
		$('#change-photo-popup-close').hide();
		$('.browse').hide();
		$('#use-webcam-btn').hide();
		return true;
	}

} 

function showButtons()
{
	$('.browse').show();
	$('#use-webcam-btn').show();
	$('#change-photo-popup-close').show();
}


function PreviewImage() {

    var oFReader = new FileReader();

    oFReader.readAsDataURL(document.getElementById("upload-photo").files[0]);

    oFReader.onload = function (oFREvent) {

    	//$('.photo-preview div').html('<img src="'+oFREvent.target.result+'"/>').show();
     	$('.photo-preview').css({backgroundImage : 'url('+oFREvent.target.result+')' });

    };

};

</script>


