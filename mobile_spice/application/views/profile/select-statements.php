<div class="popup" id="maybe">
	<div class="content txtcenter">
		<form action="<?=BASE_URL.'profile/update_statements/'?>" onsubmit="return validate_statements()" method="post" target="upload_target">
		<button type="button" class="popup-close">&times; <span>CLOSE</span></button>
 			<div class="scrollable-small">
				<ul class="bucketlist">
					<?php if($statements->num_rows()){
								foreach($statements->result() as $v){ ?> 
									<li><label><input name="user_statements[]" <?=(in_array($v->statement_id,$user_statements)) ? 'checked="checked"' : ""?> type="checkbox" value="<?=$v->statement_id?>"> <?=$v->statement?></label></li>
								<?php }
						} ?>
				</ul>
			</div>
		<div class="error" id="maybe-statements-error"></div>
		<button type="submit" class="button" id="maybe-btn"><i> SUBMIT </i></button>
		</form>
	</div>
</div>
<script type="text/javascript">

$('input[name="user_statements[]"]').click(function(){

		var tcheck = $('input[name="user_statements[]"]:checked').length;
		
		if($(this).is(':checked')){

			if(tcheck < 4){
				$('#maybe-statements-error').html('<br/>');
				return true;
			}else{
				$('#maybe-statements-error').html('<br/><span>Only 3 maybe statement is allowed.</span>');
				return false;
  			}
			
		}else{
			$('#maybe-statements-error').html('<br/>');
			return true;
		}

	});


function validate_statements()
{
	
	var tcheck = $('input[name="user_statements[]"]:checked').length; 

	if(tcheck < 1){
		$('#maybe-statements-error').html('<br/><span>Please select at least 1 maybe statement.</span><br/>');
		return false;
	}else{
		$('#maybe-statements-error').html('<br/>');
		$('#maybe-btn').addClass('disabled').attr('disabled','disabled').html('<i><img src="'+BASE_URL+'images/spinner.gif" /> SUBMITTING...</i>');
		return true;
	}

}

 

</script>