<script>
	var gameURL = '<?php echo BASE_URL . "scripts/" . $game['slug'] ?>/';
	var gameId = '<?php echo $game['game_id'] ?>';
</script>
<div class="wrapper">

		<div class="container main webgames-inner">

			<h2 class="pull-left"><?php echo $game['name'] ?></h2>
			<div class="pull-right">
				<a href="<?php echo BASE_URL ?>games" class="button"><i>BACK</i></a>
				<a href="#" class="button game-button" onclick="clickPlay()"><i>HOW TO PLAY</i></a>
				<a href="#" class="button game-button" onclick="clickMechanics()"><i>DESCRIPTION</i></a>
				<a href="#" class="button game-button" onclick="clickLeaderboard()"><i>LEADERBOARD</i></a>
			</div>
			<div >
				<iframe src="<?php echo BASE_URL . 'scripts/' .  $game['slug'] ?>?v=<?=rand()?>" width="100%" height="400" scrolling="no" noresize="noresize" frameborder="0" style="z-index:999999999"></iframe>
			</div>
		</div>
	</div>

	<div class="lytebox-wrap">
		<div class="lytebox-content-holder">
			<div class="popup lytebox-wrapped-content" id="howto">
				<div class="content txtcenter">
					<button class="popup-close">&times; <span>CLOSE</span></button>
					<h2>HOW TO PLAY</h2>
					<div class="scrollable-small">
						<p><?php echo nl2br($game['mechanics']) ?></p>
					</div>
					<br>
					<button type="button" class="button close-x"><i>PLAY</i></button>
					<button type="button" class="button" onclick="clickLeaderboard()"><i>LEADERBOARD</i></button>
				</div>
			</div>

			<div class="popup lytebox-wrapped-content" id="gamedesk">
				<div class="content txtcenter">
					<button class="popup-close">&times; <span>CLOSE</span></button>
					<h2><?php echo $game['name'] ?></h2>
					<div class="scrollable-small">
						<p><?php echo $game['description'] ?></p>
					</div>
					<br>
					<button type="button" class="button close-x"><i>PLAY</i></button>
					<button type="button" class="button" onclick="popup.open({url:'<?php echo BASE_URL ?>games/leaderboard?name=<?php echo $game['slug'] ?>'})"><i>LEADERBOARD</i></button>
				</div>
			</div>

			<div class="popup lytebox-wrapped-content" id="alert">
				<div class="content txtcenter">
					<button class="popup-close" onclick="window.location.reload()">&times; <span>CLOSE</span></button>
					<div class="scrollable-small">
						<h2>Test</h2>
						<p>Test</p>
					</div>
					<button type="button" class="button" onclick="window.location.reload()"><i>OK</i></button>
				</div>
			</div>
		</div>
	</div>
	<script>
		var isButtonDisabled = false;
		
		function showAlert() {
			popup.open({
				title : "Game Alert", 
				message : "Sorry, you can't close the tab or open a new one while playing", 
				onClose : function() {
					window.location.reload();
				}
			});
		}

		function disableButtons() {
			$('.game-button').addClass('disabled');
			isButtonDisabled = true;
		}

		function enableButtons() {
			$('.game-button').removeClass('disabled');
			isButtonDisabled = false;
		}

		function clickPlay() {
			if(!isButtonDisabled)
				popup.open({html:'#howto'})
		}

		function clickMechanics() {
			if(!isButtonDisabled)
				popup.open({html:'#gamedesk'})
		}

		function clickLeaderboard() {
			if(!isButtonDisabled)
				popup.open({url:'<?php echo BASE_URL ?>games/leaderboard?name=<?php echo $game['slug'] ?>'})
		}
    </script>