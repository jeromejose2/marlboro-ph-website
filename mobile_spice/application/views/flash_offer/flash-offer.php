<div class="popup">
	<div class="content txtcenter">
		<button class="popup-close">&times;<span>CLOSE</span></button>
		

		<div class="row">
			<?php

			if($row){
					$file = 'uploads/prize/'.$row->media; 	
					$col = 12		;
		 			if($row->type=='Photo' && file_exists($file)){ $col = 6; ?>
						<div class="col-sm-6">
							<img src="<?=BASE_URL.$file?>" width="100%"/>
						</div>
					<?php }else if($row->type=='Video' && file_exists($file)){ 
						$mime_type = get_mime_by_extension($file);
						$col = 6;  ?>
						<script type="text/javascript" src="<?=BASE_URL?>scripts/jwplayer/jwplayer.js"></script>
						<div class="col-sm-6">
		 
			                <video height="250" width="250" id="video"  controls="controls" autoplay>
								<source src="<?=BASE_URL.$file?>" type="<?=$mime_type?>" />
								<script type="text/javascript">
								function tryFlash(){
									// flash fallback
									jwplayer("video").setup({ flashplayer: "<?=BASE_URL?>scripts/jwplayer/player.swf",
															modes: [{ type: "html5" }, { type: "flash", src: "<?=BASE_URL?>scripts/jwplayer/player.swf", autostart:true, }]});	
								}
								function initVideo(){
									var video = document.getElementById("video");
									// check if html5 video tag is supported if not fallback to flash
									if(!video.play ? true : false)
										tryFlash();
								}
								initVideo();
								</script>
							</video>


						</div>
					<?php } ?>
					<div class="col-sm-<?=$col?>">
						<h3><?=$row->prize_name?></h3>
						<p class="small"><?=$row->description?></p>
						<form action="<?=BASE_URL?>flash_offer/confirm_offer" method="post"  target="upload_target" onsubmit="return onSubmitForm();">
							<input type="hidden" name="prize">
		 					<?php if($row->has_prize) { ?>
		 					<button class="button" type="submit" id="flash-offer-btn"><i>CONFIRM</i></button>
		 					<?php } else { ?>
		 					<button class="button" type="button" onclick="popup.close()"><i>OK</i></button>
		 					<?php } ?>
						</form>
					</div>

			<?php }else{ echo 'Flash offer not found.'; } ?>
		</div>

	</div>
</div>
<iframe id="upload_target" name="upload_target" style="width:0px;height:0px;border:none;"></iframe>
<script type="text/javascript">

function onSubmitForm()
{
 	$('#flash-offer-btn').addClass('disabled').attr('disabled','disabled').html('<i><img src="<?=BASE_URL?>images/spinner.gif" /> CONFIRMING...</i>');
	return true;
}

function form_response(msg)
{
 	 	popup.open({type:'alert','message':msg,buttonAlign:'center',align:'center',okCaption:'CONTINUE'});

}

</script>