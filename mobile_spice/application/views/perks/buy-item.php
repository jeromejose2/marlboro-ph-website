	<div class="popup perks">
		<div class="content">
		<button class="popup-close">&times;<span>CLOSE</span></button>
		

		<div class="row perks-details">
			<div class="col-sm-5">
				
				<div class="carousel">
					<div id="holder">
						<?php if($media) {
							foreach ($media as $k => $v) { ?>
								<img src="<?php echo BASE_URL ?>uploads/buy/media/150_150_<?php echo $v['media_content'] ?>">	
							<?php }
						} ?>
					</div>
				</div>

			</div>

			<div class="col-sm-7">
				<h2><?php echo $item['buy_item_name'] ?></h2>
				<?php echo $item['description'] ?>

				<div class="transaction">
					<h2>CREDIT VALUE: <span class="red"><?php echo number_format($item['credit_value']) ?> POINTS</span></h2>
					<h4>AVAILABLE STOCKS: <span class="red" id="stocks"><?php echo $item['stock'] ?></span></h4>
					<?php if($item['total_points'] - $item['credit_value'] >= 0) { ?>
					<h4 id="remaining-h4">REMAINING POINTS AFTER PURCHASE: <span class="red"><span id="remaining-points"><?php echo number_format($item['total_points'] - $item['credit_value']) ?></span> PTS</span></h4>
					<?php } else { ?>
					<h4 id="remaining-h4">Insufficient points</h4>
					<?php }?>
					<form id="buy-form">
					<?php if($properties) {
						foreach ($properties as $key => $value) { ?>
							<div class="specs">
								<label>CHOOSE <?php echo $value['property_name'] ?>:</label>
								<select class="properties" id="property-<?php echo $key + 1 ?>" name="val[]">
									<?php foreach($value['pvalues'] as $vk => $vv) {
										if($key == 1)
											break;
										if($vk == 0)
											echo '<option></option>';
										echo '<option>' . $vv . '</option>';
									} ?>
								</select>
								<input name="prop[]" type="hidden" value="<?php echo $value['property_name'] ?>">
							</div>		
						<?php }
					} ?>
					<span class="property-error red"></span>
					<input name="id" type="hidden" value="<?php echo $item['buy_item_id'] ?>">
					</form>

					<button type="button" class="button <?php echo $item['stock'] == 0 ? 'disabled' : '' ?>" id="confirm-button"><i>CONFIRM</i></button>
					<button type="button" class="button close-x"><i>CANCEL</i></button>
				</div>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
var xhr;
$(document).ready(function() {
	$("#holder").owlCarousel({
		items : 1,
		itemsDesktop : [3000,1],
		itemsMobile : [767,1],
		itemsTablet : [991,1],
		pagination : true
	});

	$.getJSON('<?php echo BASE_URL ?>api/notifications', function(data) {
		remainingPoints =  data.points - <?php echo $item['credit_value'] ?>;
		if(remainingPoints < 0) {
			$('#confirm-button').addClass('disabled');
			$('#remaining-h4').html('Insufficient points');
		}
		$('#remaining-points').html(remainingPoints);
	})

	$('#confirm-button').click(function() {

		if($(this).hasClass('disabled')) return;
		 
		<?php if($properties){ ?>
			if(!$('.properties :selected').text()){
				$('.property-error').html('<h3>Please select a property.</h3><br/>');
				return;
			}else{
				$('.property-error').html('');
			}
							
		<?php } ?>

		$.post('<?php echo BASE_URL ?>perks/buy/buy_item', $('#buy-form').serialize(), function(data) {
			if(data.success == 1) {
				popup.dialog({message:"You have successfully bought <?php echo $item['buy_item_name'] ?> for <?php echo $item['credit_value'] ?> points. <p class='small'><?php echo $item['buy_item_name'] ?> will be delivered to you within 60 days.</p><a class='button' onclick=\"popup.open({url:'<?php echo BASE_URL ?>perks/buy/confirm_address?id=" + data.id + "'})\" href='javascript:void(0)'><i>CONFIRM MAILING ADDRESS</i></a>" , title : "Buy Successful", align: "center"});
			} else {
				popup.dialog({message: data.error , title : "Buy Unsuccessful", align: "center"});
			}
			
		})
	});

	$(document).on('change', '#property-1', function() {
		if(xhr)
			xhr.abort();
		xhr = $.post('<?php echo BASE_URL ?>perks/buy/get_property2', {id: '<?php echo $item['buy_item_id'] ?>', property1: $(this).val()}, function(data) {
			$('#property-2').html('');
			totalStocks = 0;
			for(i in data) {
				$('#property-2').append('<option data-stocks="' + data[i].stocks + '"> ' + data[i].value + '</option>');
				if(i == 0)
					totalStocks = data[i].stocks;
			}
			$('#stocks').html(totalStocks);
		});
	});

	$(document).on('change', '#property-2', function() {
		$('#stocks').html($('#property-2 option:selected').data('stocks'))
	});
});
</script>