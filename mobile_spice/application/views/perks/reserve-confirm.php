	<div class="popup perks">
		<div class="content">
		<button class="popup-close">&times;<span>CLOSE</span></button>
		

		<div class="row perks-details">
			<div class="col-sm-5">
				
				<div class="carousel">
					<div id="holder">
						<?php foreach ($media as $m): ?>
					  	<img src="<?= BASE_URL ?>uploads/reserve/media/<?= $m['media_content'] ?>">
						<?php endforeach ?>
					</div>
				</div>

			</div>

			<div class="col-sm-7">
				<h2><?= $item['lamp_name'] ?></h2>
				<?= $item['description'] ?>

				<br>
				<div class="fieldset">
					<div class="legend"><span>RESERVATION DETAILS</span></div>
					
					<ul class="specs">
						<li>
							<label>BOOKING DATE:</label>
							<div><?= $selected_schedule['date'] ?></div>
						</li>

						<li>
							<label>GUESTS:</label>
							<div class="guests"><?= $guest_count + 1 ?></div>
						</li>

						<!-- <li>
							<label>MOBILE NUMBER:</label>
							<div><?= $mobile_num ?></div>
						</li> -->
					</ul>
				</div>
				<div>
					Your reservation details will be sent to your mobile number. Please confirm or update your number below:<br><br>
					<input type="text" id="reserve-mobile-num" name="mobile_num" maxlength="11" value="<?= $this->session->userdata('user')['mobile_num'] ?>">
				</div>
				<div class="txtcenter">	
					<button type="button" id="reserve-submit-btn" class="button"><i>CONFIRM</i></button>
					<button type="button" id="reserve-back-btn" class="button"><i>BACK</i></button>
				</div>
				
				
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$("#holder").owlCarousel({
		items : 1,
		itemsDesktop : [3000,1],
		itemsMobile : [767,1],
		itemsTablet : [991,1],
		pagination : true
	});

	$("#reserve-back-btn").click( function () {
		popup.open({url:'<?= BASE_URL ?>perks/reserve/item?<?= $this->input->server('QUERY_STRING') ?>'});
	});

	$("#reserve-submit-btn").click( function () {
		popup.loading();
		$.post('<?= BASE_URL ?>api/reserve', '<?= $this->input->server('QUERY_STRING') ?>&mobile_num=' + $("#reserve-mobile-num").val(), function (response) {
			setTimeout( function () {
				if (response.success) {
					popup.open({
						title : "Thank you",
						message : "You are now part of the guestlist for <?= $item['lamp_name'] ?> on <?= $selected_schedule['date'] ?>.",
						align : "center",
						type : "alert",
						buttonAlign : "center"
					});
				} else {
					popup.open({
						title : "Reserve Unsuccessful",
						message : response.error,
						align : "center",
						type : "alert",
						buttonAlign : "center"
					});
				}
			}, 300);
		});
	});
});
</script>