<!-- main content starts here  -->
<section class="main">
	<div class="wrapper">	
		<div class="container row perks-inner">
			<div class="col-md-3 col-lg-2  col-sm-12 side-rail">
				<?php $this->load->view('perks/nav') ?>
			</div>

			<div class="col-md-9 col-md-push-3  col-lg-push-2  main-rail">

				<div class="upload-from-event">
					<button type="button" class="button-small" onclick="popup.open({url:'<?=BASE_URL?>perks/bid/mechanics'})"><i>Read Terms &amp; Conditions</i></button>
				</div>
				<div class="clearfix"></div>
				<br>

				<div class="row perks-items">
					<!-- <div class="pull-right "><a href="perks.php" class="back">BACK</a></div><div class="clearfix"></div> -->
					
					<!-- items -->
					<?php if($bid_items) {$c = 0;
						foreach ($bid_items as $key => $value) { $c++; ?>

							<div class="col-md-6 item <?php echo $value['has_ended'] ? 'inactive' : '' ?>">
								<div class="thumb-holder col-sm-6">
									<div class="thumbnail" style="background-image:url('<?php echo BASE_URL ?>uploads/bid/main_<?php echo $value['bid_item_image'] ?>')"></div>
								</div>
								<div class="col-sm-6 copy">
									<?php if(date('YmdHis', strtotime($value['start_date'])) > date('YmdHis')) { ?>
									<h2><?php echo $value['bid_item_name'] ?></h2>
									<p><?php echo strlen(strip_tags($value['description'])) > 200 ? substr(strip_tags($value['description']), 0, 197) . '...' : strip_tags($value['description']) ?></p>
									<button type="button" id="item-<?php echo $value['bid_item_id'] ?>" class="button disabled"><i>COMING SOON</i></button>
									<?php } else { ?>
									<h2><a href="#" onclick="popup.open({url:'<?php echo BASE_URL ?>perks/bid/item/<?php echo $value['bid_item_id'] ?>'}); return false;"><?php echo $value['bid_item_name'] ?></a></h2>
									<p><?php echo strlen(strip_tags($value['description'])) > 200 ? substr(strip_tags($value['description']), 0, 197) . '...' : strip_tags($value['description']) ?></p>
									<button type="button" id="item-<?php echo $value['bid_item_id'] ?>" class="button" onclick="popup.open({url:'<?php echo BASE_URL ?>perks/bid/item/<?php echo $value['bid_item_id'] ?>'})"><i><?php echo $value['has_ended'] ? 'VIEW' : 'BID NOW!' ?></i></button>
									<?php } ?>
									
								</div>
								<div class="clearfix"></div>
							</div>	
							<?=($c%2==0) ? '<div class="clearfix"></div>' : ''?>
						<?php }
					} ?>
					
				</div>
			</div>
		</div>

	</div>

	<?php $this->load->view('perks/splash-popup') ?>

</section>

<script type="text/javascript">
$(function(){
	<?php if(!$viewed): ?>
		popup.open({html:'#popup1'});
	<?php endif; ?>

	<?php if($this->uri->segment(3)) {
		$existing = false;
		if($bid_items) {
			foreach ($bid_items as $key => $value) {
				if($value['bid_item_id'] == $this->uri->segment(3)) {
					$existing = true;
					break;
				}
			}
		}

		if($existing) { ?>
		$('#item-<?php echo $this->uri->segment(3) ?>').click();
		<?php }
	} 
	?>
	
});	
</script>
