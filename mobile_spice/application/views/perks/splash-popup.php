<div class="lytebox-wrap">
		<div class="splash-popup lytebox-wrapped-content" id="popup1">
			<div class="content txtcenter">
				<h1>PERKS</h1>
				<h2>GET REWARDED FOR MAKING DECISIONS.</h2>
				
				<p>Earn points with Marlboro and use them to bid on or buy exclusive merchandise and surprise items, and be on the guestlist of your favorite bars and clubs.</p>
				<button class="button close-x" type="button"><i>CONTINUE</i></button>
			</div>
		</div>
	</div>