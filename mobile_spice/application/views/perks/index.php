<!-- main content starts here  -->
<section class="main">
	<div class="wrapper">	
		<div class="container main">
			
			<div class="row perks-categories">
				<div class="col-md-3 item">
					<div class="thumb-holder col-sm-7 col-md-12">
						<a href="<?php echo BASE_URL ?>perks/bid">
							<div class="thumbnail" style="background-image:url('<?php echo BASE_URL ?>uploads/perks/<?php echo $thumbs['bid']['description'] ?>')"></div>
						</a>
					</div>
					<div class="col-sm-5 col-md-12 copy">
						<!-- <h2><a href="<?php echo BASE_URL ?>perks/bid">BID</a></h2> -->
						<a href="<?php echo BASE_URL ?>perks/bid" class="button"><i>BID</i></a>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="clearfix visible-xs visible-sm"></div>
				
				<div class="col-md-3 item">
					<div class="thumb-holder col-sm-7 col-md-12">
						<a href="<?php echo BASE_URL ?>perks/bid">
							<div class="thumbnail" style="background-image:url('<?php echo BASE_URL ?>uploads/perks/<?php echo $thumbs['buy']['description'] ?>')"></div>
						</a>
					</div>
					<div class="col-sm-5 col-md-12 copy">
						<!-- <h2><a href="<?php echo BASE_URL ?>perks/bid">BUY</a></h2> -->
						<a href="<?php echo BASE_URL ?>perks/buy" class="button"><i>BUY</i></a>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="clearfix visible-xs visible-sm"></div>

				<div class="col-md-3 item">
					<div class="thumb-holder col-sm-7 col-md-12">
						<a href="<?php echo BASE_URL ?>perks/bid">
							<div class="thumbnail" style="background-image:url('<?php echo BASE_URL ?>uploads/perks/<?php echo $thumbs['reserve']['description'] ?>')"></div>
						</a>
					</div>
					<div class="col-sm-5 col-md-12 copy">
						<!-- <h2><a href="<?php echo BASE_URL ?>perks/bid">RESERVE</a></h2> -->
						<a href="<?php echo BASE_URL ?>perks/reserve" class="button"><i>RESERVE</i></a>
					</div>
					<div class="clearfix"></div>
				</div>
				

			</div>

		</div>

	</div>

	<?php $this->load->view('perks/splash-popup') ?>

</section>

<script type="text/javascript">
$(function(){
	popup.open({html:'#popup1'});
});	
</script>