<?php

class Welcome extends CI_Controller
{
 
	public function index()
	{
		$this->load->model(array('profile_model','Movefwd_Model'));
		$data = array();	

		$user = $this->session->userdata('user');
		$birthday_offer = '';
		$flash_offer = '';
		if($this->session->flashdata('check_offers')){

			$birthday_offer = $this->profile_model->get_row(array('table'=>'tbl_birthday_offers',
															   'where'=>array("month = '".date('m',strtotime($user['birthdate']))."' AND month ='".date('m')."'" =>null,
 															   				  	'YEAR(date_created)' =>date('Y'),
															   				  	'stock >'=>0,
															   				  	'status'=>1,
															   				  	'is_deleted'=>0
															   				  ),
															   'fields'=>'prize_id'
															)
														);
		
			if($birthday_offer){
				
				$user_birthday_prize = $this->profile_model->get_row(array('table'=>'tbl_user_birthday_prizes',
																   			'where'=>array('prize_id' =>$birthday_offer->prize_id,'registrant_id' =>$this->session->userdata('user_id')),
																   			'fields'=>'user_birthday_prize_id'
																			)
																	);
				$birthday_offer = $user_birthday_prize ? '' : $birthday_offer->prize_id;
			
			}


			//if(!$birthday_offer){
			// if ($this->session->userdata('user_id') == '64652' && $this->input->server('REMOTE_ADDR') == NUWORKS_IP) {
			// 	$this->output->enable_profiler(true);
			// }
			$flash_offer = $this->profile_model->get_row(array('table'=>'tbl_flash_offers',
														   'where'=>array('status'=>1,'CURDATE() >= DATE(start_date)'=>null,'CURDATE() <= DATE(end_date) '=>null,'stock >'=>0, 'is_deleted' => 0),
														   'fields'=>'prize_id'
														)
													);
			if($flash_offer){
				$user_flash_prize = $this->profile_model->get_row(array('table'=>'tbl_user_flash_prizes',
															   			'where'=>array('prize_id' =>$flash_offer->prize_id,'registrant_id' =>$this->session->userdata('user_id')),
															   			'fields'=>'user_flash_prize_id'
																		)
																);
				$flash_offer = $user_flash_prize ? '' : $flash_offer->prize_id;

			}else{
				$flash_offer = '';
			}

			//}			

	   }
		
		$data['birthday_offer'] = $birthday_offer;
		$data['flash_offer'] = $flash_offer;
		$data['movefwd_categories'] = $this->Movefwd_Model->get_categories();
		$this->load->layout('home', $data);
	 //	$this->output->enable_profiler(true);
 	}

	public function logout()
	{
		$this->session->clear();
		redirect('user/login');
	}
}