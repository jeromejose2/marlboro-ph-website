<?php

class Move_Forward extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Movefwd_Model');
	}

	public function index()
	{
		$data = array();
		$data['active'] = (int) $this->uri->segment(3);
		$data['categories'] = $this->Movefwd_Model->get_categories();
		$data['visited'] = true;
		if (!$data['active']) {
			if ($this->uri->segment(2)) {
				show_404();
			}
			$data['offers'] = $this->Movefwd_Model->get_active_offers();
			if (!$this->session->userdata('move_fwd')) {
				$data['visited'] = false;
				$this->session->set_userdata('move_fwd', 1);
			}
		} else {
			$found = false;
			foreach ($data['categories'] as $offer) {
				if ($offer['category_id'] == $data['active']) {
					$found = true;
					break;
				}
			}
			if (!$found) {
				show_404();
			}
			$data['offers'] = $this->Movefwd_Model->get_offers_by_category($data['active']);
		}
		$this->load->layout('move_fwd/index', $data);
	}

	public function offer()
	{
		$data = array();
		$user = $this->session->userdata('user_id');
		$offerId = (int) $this->uri->segment(3);
		$permalink = (string) $this->uri->segment(4);
		if (!$permalink) {
			show_404();
		}
		
		// $data['offer'] = $this->Movefwd_Model->get_offer_by_permalink($permalink, $offerId);
		$data['offer'] = $this->Movefwd_Model->get_offer($offerId, $permalink);
		if (!$data['offer']) {
			show_404();
		}

		$data['categories'] = $this->Movefwd_Model->get_categories();

		if ($data['offer']['status']) {

			$data['has_pledge'] = null;
			$data['entries'] = null;
			$data['entries_count'] = 0;
			$data['current'] = null;
			$data['pending_entry'] = null;
			$data['chosen_action'] = $this->Movefwd_Model->get_chosen_action($user, $data['offer']['move_forward_id']);
			$data['challenges'] = $this->Movefwd_Model->get_challenges_by_offer($data['offer']['move_forward_id']);
			$data['current'] = isset($data['challenges'][0]) ? $data['challenges'][0] : null;
			if ($data['chosen_action'] == 1) {
				$data['entries'] = $this->Movefwd_Model->get_entries($user, $data['offer']['move_forward_id']);
				$data['entries_count'] = count($data['entries']);
				$data['current'] = isset($data['challenges'][$data['entries_count']]) ? $data['challenges'][$data['entries_count']] : null;
				if ($data['current']) {
					$data['pending_entry'] = $this->Movefwd_Model->get_pending_entry($user, $data['current']['challenge_id']);
				}
			} elseif ($data['chosen_action'] == 2) {
				$data['has_pledge'] = $this->Movefwd_Model->has_pledge_done($user, $data['offer']['move_forward_id']);
			}
			
			$this->load->layout('move_fwd/offer', $data);

		} else {

			$this->load->layout('move_fwd/inactive_offer', $data);

		}
		
	}

	public function participants()
	{
		$id = $this->input->get('mfid');
		if (!$id) {
			show_404();
		}
		$limit = (int) $this->input->get('limit');
		$page = (int) $this->input->get('page');
		if (!$page) {
			$page = 1;
		}
		$result = array();
		$result['participants'] = $this->Movefwd_Model->get_participants($id, $page, $limit);
		$result['total'] = $this->Movefwd_Model->get_participants_count($id);
		$pages = ceil($result['total'] / $limit);
		$result['prev'] = 0;
		if ($page >= 2) {
			$result['prev'] = $page - 1;
		}
		if ($page < $pages) {
			$result['next'] = ++$page;
		} else {
			$result['next'] = false;
		}
		$this->output->set_content_type('application/json')
			->set_output(json_encode($result));
	}

	public function slots()
	{
		$id = $this->input->get('mfid');
		if ($id) {
			$offer = $this->Movefwd_Model->get_offer($id);
			if ($offer && $offer['slots']) {
				$this->output->set_output($offer['slots'].' SLOTS LEFT');
			}
		}
	}

	public function pledge()
	{
		$result = array(
			'success' => false,
			'error' => '',
			'points' => 0
		);
		$id = $this->input->post('mfid');
		$user = $this->session->userdata('user_id');
		if ($user && $id) {
			$this->load->model('Points_Model');
			$offer = $this->Movefwd_Model->get_active_offer($id);
			if ($offer) {
				$params = array(
					'registrant' => $user,
					'suborigin' => $id,
					'remarks' => '{{ name }} pledged for MoveFWD',
					'points' => $offer['pledge_points']
				);
				if (!$params['points']) {
					$result['error'] = 'Invalid pledge';
				} elseif (!($pledged_points = $this->Points_Model->spend(MOVE_FWD_PLEDGE_ENTRY, $params))) {
					$result['error'] = "Earn more points to be able to pledge for this activity!";
				// } elseif ($this->Movefwd_Model->decrement_slot($id)) {
				} else {
					$this->Notification_Model->notify($user, MOVE_FWD_PLEDGE_ENTRY, array(
						'message' => "You have successfully pledged {$pledged_points} points for {$offer['move_forward_title']}.",
						'suborigin' => $id
					));
					$result['success'] = true;
					$result['points'] = $pledged_points;
				}
			} else {
				$result['error'] = 'Offer not existing';
			}
		}
		
		$this->output->set_content_type('application/json')
			->set_output(json_encode($result));
	}

	public function upload()
	{
		$user = $this->session->userdata('user_id');
		$path = './uploads/profile/';
		if (!is_dir($path)) {
			mkdir($path);
		}
		if (!is_dir($path.$user)) {
			mkdir($path.$user);
		}
		$path = $path.$user.'/move_fwd/';
		if (!is_dir($path)) {
			mkdir($path);
		}

		$content = null;
		$type = (int) $this->input->post('type');
		if ($type == 1) {
			$config = array();
			$config['upload_path'] = $path;
			$config['allowed_types'] = MOVE_FWD_ALLOWED_TYPES;
			$config['max_size'] = FILE_UPLOAD_LIMIT;
			$config['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['move_fwd_upload']['name'], PATHINFO_EXTENSION);

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('move_fwd_upload')) {
				$this->output->set_output($this->upload->display_errors());
				return;
			}
			$this->load->helper('resize');
			resize(array(
				'width' => 115, 
				'height'=> 71, 
				'source_image' => $path.$config['file_name'],
				'new_image_path' => $path,
				'file_name' => $config['file_name']
			));
			$content = $config['file_name'];
		} elseif ($type == 2) {
			$content = (string) $this->input->post('video');
			if (!$content || !validate_youtube_url($content)) {
				$this->output->set_output('Invalid youtube url');
				return;
			}
		} else {
			$content = (string) $this->input->post('text');
			if (!$content) {
				$this->output->set_output('Please input your entry');
				return;
			}	
		}
		
		$this->db->insert('tbl_move_forward_gallery', array(
			'challenge_id' => $this->input->post('challenge_id'),
			'mfg_content' => $content,
			'mfg_status' => $this->session->userdata('is_cm') ? 1 : 0,
			'mfg_is_public' => 1,
			'registrant_id' => $user
		));
	}

	public function splash()
	{
		$this->session->set_userdata('move_fwd', 1);
	}

	public function gallery()
	{
		$data = array();
		$data['offer_id'] = (int) $this->uri->segment(3);
		if (!$data['offer_id']) {
			show_404();
		}
		$data['offer'] = $this->Movefwd_Model->get_offer($data['offer_id']);
		if (!$data['offer']) {
			show_404();
		}

		$this->load->model('View_Model');
		// $data['visits'] = $this->View_Model->get_visits_count(MOVE_FWD, $data['offer_id']);
		// $data['entries'] = $this->Movefwd_Model->get_entries_by_offer($data['offer_id']);
		$data['visits'] = 0;
		$data['entries'] = array();

		$suborigins = array();
		foreach ($data['entries'] as $entry) {
			$suborigins[] = $entry['move_forward_gallery_id'];
		}

		$data['comments_count'] = 0;
		if ($suborigins) {
			$this->load->model('Comment_Model');
			$data['comments_count'] = $this->Comment_Model->get_comments_replies_count(MOVE_FWD_GALLERY, $suborigins);
		}
		
		$this->load->view('move_fwd/gallery', $data);
	}

	public function mechanics()
	{
		$data = array();
		$data['content'] = (array) $this->db->select()
			->from('tbl_settings')
			->where('type', 'move_fwd_mechanics')
			->get()
			->row();
		$this->load->view('move_fwd/move-fwd-mechanics', $data);
	}

	public function _remap()
	{
		// redirect('/');
		$action = $this->uri->segment(2);
		if (!$action || $action == 'category') {
			$this->index();
		} elseif ($action == 'mechanics') {
			$this->mechanics();
		} elseif ($action == 'inactive_offer') {
			$this->inactive_offer();
		} elseif ($action == 'offer') {
			$this->offer();
		} elseif ($action == 'gallery') {
			$this->gallery();
		} elseif ($this->input->is_ajax_request()) {
			if ($action == 'slots') {
				$this->slots();
			} elseif ($action == 'participants') {
				$this->participants();
			} elseif ($this->input->server('REQUEST_METHOD') == 'POST' && $action == 'splash') {
				$this->splash();
			} elseif ($this->input->server('REQUEST_METHOD') == 'POST' && $action == 'pledge') {
				$this->pledge();
			} else {
				show_404();
			}
		} elseif ($this->input->server('REQUEST_METHOD') == 'POST' && $action == 'upload') {
			$this->upload();
		} elseif ($this->uri->segment(4)) {
			show_404();
		}
	}
}