<?php

class User extends CI_Controller
{
	
	public function index()
	{
		redirect('user/login');
	}

	public function login()
	{
		$data = array();
		$this->session->reset_id();
		$data['invalid_message'] = $this->session->flashdata('invalid_login_message');
		$this->load->layout('user/login', $data, 'user/template');
	}

	public function swf()
	{
		$this->load->view('user/swf_xml');
	}

	private function validate_password($password, $user, &$validation_message)
	{
		$count = 0;
		$patterns =  array('lowercase' => 'lowercase', 'uppercase' => 'uppercase', 'digit' => 'digit', 'special character' => 'special character');
		
		if (preg_match('/[a-z]/', $password)) {
			$count++;
			unset($patterns['lowercase']);
		}

		if (preg_match('/[A-Z]/', $password)) {
			$count++;
			unset($patterns['uppercase']);
		}
		
		if (preg_match('/\d/', $password)) {
			$count++;
			unset($patterns['digit']);
		}

		if (preg_match('/\W/', $password)) {
			$count++;
			unset($patterns['special character']);
		}

		if ($count < 3) {
			$last = array_pop($patterns); 
			$validation_message = 'Your password must contained from the following: '.implode(',', $patterns).' or '.$last.'</br>';
			return false;
		}
		
		if (preg_match('/user|guest|admin|sys|test|pass|super|'.$user->first_name.'|'.$user->third_name.'/i', $password, $matches)) {
			$validation_message = 'Your new password must not contained from the following: user,guest,admin,sys,test,pass,super,'.strtolower($user->first_name).' or '.strtolower($user->third_name);
			return false;
		}

		return true;
	}

	public function reset_success()
	{
		if (!$this->session->flashdata('forgot_submit')) {
			redirect('user/login');
		}
		$this->load->layout('user/reset_success', array(), 'user/template');
	}

	public function forgot_submit()
	{
		$email_input = $this->input->post('email');
		if ($email_input) {

			$user = $this->db->select()
				->from('tbl_registrants')
				->where('email_address', $email_input)
				->limit(1)
				->get()
				->row();
				
			if (filter_var($email_input, FILTER_VALIDATE_EMAIL) && $user) {

				if ($user->status == ACCESS_GRANTED_STATUS) {

					$this->load->library('Edm');
					$this->load->library('Encrypt');
					$email = $user->email_address;
					$username = $user->email_address;
					$firstname = $user->first_name;
					$lastname = $user->third_name;
					$password = generate_password();

					$this->edm->add_recipient(EDM_MARLBORO_RESET_LIST_ID, $email, $username, $firstname, $lastname, $password);
					$this->edm->send(EDM_MARLBORO_RESET_MAIL_ID, $email, $username, $firstname, $password);

					$updateUser = array();
					if ($user->from_migration && !$user->encryption_changed) {
						$updateUser['salt'] = generate_password(20);
						$updateUser['encryption_changed'] = 1;
					}

					$updateUser['password'] = $this->encrypt->encode($password, isset($updateUser['salt']) ? $updateUser['salt'] : $user->salt);					
					$this->db->where('registrant_id', $user->registrant_id);
					$this->db->update('tbl_registrants', $updateUser);

					if ($this->db->affected_rows()) {
						$this->session->set_userdata('forgot_submit', 1);
						redirect('user/reset_success');
					}

				} else {
					$this->session->set_userdata('invalid_forgot_email', 'Account pending for approval.');
				}
			} else {
				$this->session->set_userdata('invalid_forgot_email', 'The email you entered is incorrect. Please try again.');
			}
		}
		
		redirect('user/forgot');
	}

	public function auth()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$message = 'The username/password you entered is incorrect. Please try again.';

		if ($username && filter_var($username, FILTER_VALIDATE_EMAIL) && $password) {
			$this->load->model('Registration_Model');
			$user = $this->Registration_Model->get_user_by_login($username);
			if ($user) {

				$this->load->library('Encrypt');

				$today = date('Y-m-d H:i:s');
				$isBlocked = $user['login_attempts'] >= LOGIN_ATTEMPTS;
				if ($isBlocked && $user['date_blocked']) {
					if (strtotime($today) >= strtotime(LOGIN_RETURN, strtotime($user['date_blocked']))) {
						$isBlocked = false;
						$this->Registration_Model->reset_invalid_login($user['registrant_id']);
					} else {
						$message = 'Your account was blocked due to invalid login attempts';
					}
				}

				$accessGranted = false;
				if ($user['from_migration'] && !$user['encryption_changed']) {
					$accessGranted = $this->encrypt->password($password, $user['salt']) === $user['password'];
				} else {
					$accessGranted = $this->encrypt->decode($user['password'], $user['salt']) === $password;
				}

				if (!$isBlocked && $accessGranted) {

					if ($user['status'] == PENDING_CSR || $user['status'] == CSR_APPROVED) {
						$message = 'Your account is pending for approval';
					} elseif ($user['status'] == PENDING_GIID) {
						$message = 'You need to upload your GIID';
					} elseif ($user['status'] == ACCESS_GRANTED_STATUS) {

						$this->db->insert('tbl_login', array(
							'registrant_id' => $user['registrant_id'],
							'date_login' => $today
						));

						if ($this->db->affected_rows()) {

							$this->load->model(array('profile_model','Points_Model'));

							$this->session->set_userdata('user', array(
								'first_name' => $user['first_name'],
								'last_name' => $user['third_name'],
								'middle_initial' => $user['middle_initial'],
								'birthdate' => $user['date_of_birth'],
								'nick_name' => $user['nick_name']
							));
							$this->session->set_userdata('check_offers', 1);
							$this->session->set_userdata('user_points', $user['total_points']);
							$this->session->set_userdata('user_id', $user['registrant_id']);
							$this->session->set_userdata('login_id', $this->db->insert_id());
							$this->session->set_userdata('is_cm', $user['is_cm']);

							$this->Points_Model->earn(LOGIN, array(
								'date' => $today,
								'suborigin' => $this->session->userdata('login_id')
							));

							$this->Points_Model->earn(REGISTRATION);

							$this->db->where('registrant_id', $user['registrant_id']);
							$this->db->update('tbl_registrants', array(
								'last_login' => $today,
								'login_attempts' => 0,
								'date_blocked' => null,
								'login_id' => $this->session->userdata('login_id')
							));

							$user_id = $this->session->userdata('user_id');
							$row = $this->profile_model->get_row(array('table'=>'tbl_hidden_pack_logs','where'=>array('registrant_id'=>$user_id,'DATE(date_added) = CURDATE()'=>null)));

							if(!$row){

								$pages = 'welcome,photos,backstage_pass,move_forward,profile';
								$pages = explode(',',$pages); 
								$total_page = count($pages) - 1;
								$index = rand(0,$total_page);
								$this->session->set_userdata('hidden_pack_display_page',@$pages[$index]);

							}					

						}
					}

				} elseif (!$isBlocked) {
					$this->Registration_Model->save_invalid_login($user['registrant_id'], $user['login_attempts']);
				}
			}
		}

		$this->session->set_userdata('invalid_login_message', $message);
		redirect('user/login');
	}

	public function upload_later()
	{
		if ($this->input->server('REQUEST_METHOD') != 'POST') {
			$this->output->set_content_type('application/json')
				->set_output(json_encode(array('success' => false, 'error' => 'Invalid request')));
			return;
		}

		$this->validate_step_1(true);
		$this->validate_step_2(true);
		$this->validate_step_3(true, false);
		if (!$this->form_validation->run()) {
			$this->session->set_userdata('reg_token', generate_token());
			$this->output->set_content_type('application/json')
				->set_output(json_encode(array('success' => false, 'error' => validation_errors())));
			return;
		}

		$user = array();
		$user['first_name'] = $this->input->post('fname');
		$user['third_name'] = $this->input->post('lname');
		$user['middle_initial'] = $this->input->post('mname');
		$user['nick_name'] = $this->input->post('nname');
		$user['date_of_birth'] = $this->input->post('birthday');
		$user['gender'] = $this->input->post('gender');
		$user['mobile_phone'] = $this->input->post('mobile_number');
		$user['email_address'] = $this->input->post('email');
		$user['province'] = $this->input->post('province');
		$user['street_name'] = $this->input->post('street');
		$user['barangay'] = $this->input->post('brgy');
		$user['city'] = $this->input->post('city');
		$user['zip_code'] = $this->input->post('zip_code');
		$user['current_brand'] = $this->input->post('current_brand');
		$user['first_alternate_brand'] = $this->input->post('first_alternate_brand');
		$user['alternate_purchase_indicator'] = $this->input->post('alternate_purchase');
		$user['referred_by_code'] = $this->input->post('referred_code');
		$user['outlet_code'] = $this->input->post('outlet_code');
		$user['status'] = PENDING_GIID;

		$brand = $this->db->select()
			->from('tbl_brands')
			->where('brand_id', $user['current_brand'])
			->get()
			->row();

		$user['current_brand_code'] = $brand->code;
		$user['current_brand_flavor'] = $brand->flavor;
		$user['current_brand_affinity'] = 0;
		$user['current_brand_tar_level'] = $brand->tar;

		$brand = $this->db->select()
			->from('tbl_brands')
			->where('brand_id', $user['first_alternate_brand'])
			->get()
			->row();

		$user['first_alternate_brand_code'] = $brand->code;
		$user['first_brand_flavor'] = $brand->flavor;
		$user['first_alternate_brand_affinity'] = 0;
		$user['first_brand_tar_level'] = $brand->tar;

		$user['date_created'] = date('Y-m-d H:i:s');
		$user['date_of_capture'] = $user['date_created'];
		$user['tracking_code'] = unique_code();
		$user['date_tracking_code_created'] = $user['date_created'];
		$user['referral_code'] = unique_code();
		$user['salt'] = generate_password(20);
		$this->load->library('Encrypt');
		$user['password'] = $this->encrypt->encode(generate_password(), $user['salt']);

		$this->db->insert('tbl_registrants', $user);
		if ($this->db->affected_rows()) {

			$this->load->library('Edm');
			$email = $user['email_address'];
			$firstname = $user['first_name'];
			$lastname = $user['third_name'];
			$tracking_code = $user['tracking_code'];

			$this->edm->add_recipient_for_tracking_code(EDM_MARLBORO_REGISTER_LIST_ID, $email, $firstname, $lastname, $tracking_code);
			$this->edm->send_tracking_code(EDM_MARLBORO_REGISTER_MAIL_ID, $email, $firstname, $tracking_code);

		}
		$this->output->set_content_type('application/json')
			->set_output(json_encode(array('success' => true, 'error' => '')));
	}

	public function validate_step_1($funcCall = false)
	{
		if (!isset($this->form_validation))
			$this->load->library('form_validation'); 

		$_POST['mobile_number'] = $this->input->post('mobile_prefix').$this->input->post('mobile_number');

		$this->form_validation->set_rules('smoker','Smoker','required|numeric|callback_smoker_check');
		$this->form_validation->set_rules('fname', 'First name', 'required|alpha_space');
		$this->form_validation->set_rules('lname', 'Last name', 'required|alpha_space');
		$this->form_validation->set_rules('mname', 'Middle name', 'alpha_space');
		$this->form_validation->set_rules('nname', 'Nick name', 'required|alpha_space');
		$this->form_validation->set_rules('birthday', 'Birthdate', 'required|callback_birthdate_check');
		$this->form_validation->set_rules('gender', 'Gender', 'required|callback_check_gender');
		$this->form_validation->set_rules('mobile_prefix', 'Mobile Prefix number', 'required|numeric|min_length[4]|max_length[4]|callback_check_mobile_prefix');
		$this->form_validation->set_rules('mobile_number', 'Mobile number', 'required|numeric|min_length[11]|max_length[11]');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
		$this->form_validation->set_rules('street', 'Street', 'required');
		$this->form_validation->set_rules('brgy', 'Barangay', 'required');
		$this->form_validation->set_rules('province', 'Province', 'required|numeric|callback_province_check');
		$this->form_validation->set_rules('city', 'City', 'required|numeric|callback_city_check');
		$this->form_validation->set_rules('zip_code', 'Zip Code', 'required|numeric|min_length[4]|max_length[4]');
		// $this->form_validation->set_rules('referral_code', 'Ref', 'required');
		$this->form_validation->set_rules('outlet_code', 'Outlet Code', 'callback_outlet_code_check');

		$this->form_validation->set_message('email_check', 'E-mail already exists in database');
		$this->form_validation->set_message('outlet_code_check', 'Outlet code is not existing');
		//$this->form_validation->set_message('birthdate_check', 'Sorry, You need to be a legal aged (18 years old and above) smoker in order to register.');

		if (!$funcCall) {
			$result = array(
				'success' => false,
				'error' => ''
			);
			$this->output->set_content_type('application/json');
			if (!$this->form_validation->run()) {
				$result['error'] = validation_errors();
				$this->output->set_output(json_encode($result));
			} else {
				$result['success'] = true;
				$this->output->set_output(json_encode($result));
			}
		}
	}

	public function check_mobile_prefix($val)
	{

		$row = $this->db->get_where('tbl_mobile_prefix',array('mobile_prefix'=>$val))->row();
		if(!$row){
			$this->form_validation->set_message('check_mobile_prefix','Mobile Prefix number does not exists.');
			return false;
		}else{
			return true;
		}

	}

	public function check_gender($val)
	{
		if($val !='F' && $val != 'M')
		{
			$this->form_validation->set_message('check_gender','Gender field may only contain "F" character for female or "M" character for male.');
			return false;
		}else{
			return true;
		}

	}

	public function smoker_check($val)
	{

		if($val != 1){
			$this->form_validation->set_message('smoker_check','Smoker field may only contain "Yes" character.');
			return false;
		}else{
			return true;
		}

	}

	public function token_check($token)
	{
		$success = false;
		if ($token && $this->session->userdata('reg_token') === $token) {
			$success = true;
		} else {
			//$this->form_validation->set_message('token_check', 'Invalid form request. correct:'.$this->session->userdata('reg_token'));
			$this->form_validation->set_message('token_check', 'Invalid form request.');
		}
		return $success;
	}

	public function outlet_code_check($code)
	{
		if (!$code) {
			return true;
		}

		$outlet = $this->db->select()
			->from('tbl_outlet_codes')
			->where('outlet_code', $code)
			->limit(1)
			->get()
			->row();

		return $outlet ? true : false;
	}

	public function email_check($email)
	{
		$exists = $this->db->select()
			->from('tbl_registrants')
			->where('email_address', $email)
			->limit(1)
			->get()
			->row();
		return $exists ? false : true;
	}

	public function birthdate_check($date)
	{
		$date_arr = explode('-',$date);

		$year = (isset($date_arr[0])) ? $date_arr[0] : 0;
		$month = (isset($date_arr[1])) ? $date_arr[1] : 0;
		$day = (isset($date_arr[2])) ? $date_arr[2] : 0;

		if(checkdate($month, $day, $year)){

			$date = new DateTime($date);
			$age = $date->diff(new DateTime);
			if (!$age || $age->y < MINIMUM_AGE) {
				$this->form_validation->set_message('birthdate_check',' Sorry, You need to be a legal aged ('.MINIMUM_AGE.' years old and above) smoker in order to register.');
				return false;
			}
			return true;

		}else{
			$this->form_validation->set_message('birthdate_check','Invalid Birthdate format.');
			return false;
		}

		/*
		$date = new DateTime($date);
		$age = $date->diff(new DateTime);
		if (!$age || $age->y < MINIMUM_AGE) {
			return false;
		}
		return true;
		*/
	}

	public function province_check($p)
	{
		$province = $this->db->select()
			->from('tbl_provinces')
			->where('province_id', $p)
			->limit(1)
			->get()
			->row();
		if (!$province) {
			$this->form_validation->set_message('province_check','Province does not exists.');
			return false;
		}
		return true;
	}

	public function city_check($id)
	{
		
		$where = array('city_id'=>$id,'province_id'=>$this->input->post('province'));
		 
		$city = $this->db->select()
			->from('tbl_cities')
			->where($where)
			->limit(1)
			->get()
			->row();
 
		if (!$city) {
			$this->form_validation->set_message('city_check','City does not exists.');
			return false;
		}
		return true;
	}

	public function validate_step_2($funcCall = false)
	{
		if (!isset($this->form_validation))
			$this->load->library('form_validation');

		$this->form_validation->set_rules('current_brand', 'Current brand', 'required|numeric|callback_current_brand');
		$this->form_validation->set_rules('first_alternate_brand', 'Alternative Brand', 'required|numeric|callback_alternate_brand_check');
		$this->form_validation->set_rules('alternate_purchase', 'Alternative Purchase', 'required|callback_alternate_purchase_check');

		$this->form_validation->set_message('current_brand', 'Current brand does not exists.');
		$this->form_validation->set_message('alternate_brand_check', 'Alternate brand does not exists.');
		$this->form_validation->set_message('alternate_purchase_check', 'Alternate purchase does not exists.');

		if (!$funcCall) {
			$result = array(
				'success' => false,
				'error' => ''
			);
			$this->output->set_content_type('application/json');
			if (!$this->form_validation->run()) {
				$result['error'] = validation_errors();
				$this->output->set_output(json_encode($result));
			} else {
				$result['success'] = true;
				$this->output->set_output(json_encode($result));
			}
		}


	}

	public function current_brand($brandId)
	{
		$brand = $this->db->select()
			->from('tbl_brands')
			->where('brand_id', $brandId)
			->limit(1)
			->get()
			->row();
		if ($brand) {
			return true;
		}
		return false;
	}

	public function alternate_brand_check($brandId)
	{
		$brand = $this->db->select()
			->from('tbl_brands')
			->where('brand_id', $brandId)
			->limit(1)
			->get()
			->row();
		if ($brand) {
			return true;
		}
		return false;
	}

	public function alternate_purchase_check($purchase)
	{
		$alternate = $this->db->select()
			->from('tbl_alternate_purchase')
			->where('alternate_id', $purchase)
			->limit(1)
			->get()
			->row();
		if ($alternate) {
			return true;
		}
		return false;
	}

	public function validate_step_3($funcCall = false, $withGiid = true)
	{
		if (!isset($this->form_validation))
			$this->load->library('form_validation');

		$this->form_validation->set_rules('__token', 'Token', 'callback_token_check');
		if ($withGiid) {
			$this->form_validation->set_rules('giid_number', 'GIID Number', 'required');
			$this->form_validation->set_rules('giid_type', 'GIID Type', 'required|numeric|callback_check_giid_type');
		}

		if (!$funcCall) {
			$result = array(
				'success' => false,
				'error' => ''
			);
			$this->output->set_content_type('application/json');
			if (!$this->form_validation->run()) {
				$result['error'] = validation_errors();
				$this->output->set_output(json_encode($result));
			} else {
				$imageString = $this->input->post('captured_image_ba');
				if (empty($_FILES['giid']['name']) && !$imageString) {
					$result['error'] = 'Please upload a file.';
					$this->output->set_output(json_encode($result));
					return;
				}
				$result['success'] = true;
				$this->output->set_output(json_encode($result));
			}
		}
	}


	public function check_giid_type($val)
	{

		$giid_type = $this->db->select()
			->from('tbl_giid_types')
			->where('giid_type_id', $val)
			->limit(1)
			->get()
			->row();

		if(!$giid_type){
			$this->form_validation->set_message('check_giid_type','GIID type does not exists.');
			return false;
		}		
		return true;
	}

	public function confirm()
	{
		if ($this->input->server('REQUEST_METHOD') != 'POST') {
			$this->output->set_output('Invalid request');
			return;
		}

		$this->validate_step_1(true);
		$this->validate_step_2(true);
		$this->validate_step_3(true);

		if (!$this->form_validation->run()) {
			$this->session->set_userdata('reg_token', generate_token());
			$this->output->set_output(validation_errors());
			return;
		}

		$imageString = $this->input->post('captured_image_ba');
		$user = array();
		$user['first_name'] = $this->input->post('fname');
		$user['third_name'] = $this->input->post('lname');
		$user['middle_initial'] = $this->input->post('mname');
		$user['nick_name'] = $this->input->post('nname');
		$user['date_of_birth'] = $this->input->post('birthday');
		$user['gender'] = $this->input->post('gender');
		$user['mobile_phone'] = $this->input->post('mobile_number');
		$user['email_address'] = $this->input->post('email');
		$user['street_name'] = $this->input->post('street');
		$user['barangay'] = $this->input->post('brgy');
		$user['province'] = $this->input->post('province');
		$user['city'] = $this->input->post('city');
		$user['zip_code'] = $this->input->post('zip_code');
		$user['current_brand'] = $this->input->post('current_brand');
		$user['first_alternate_brand'] = $this->input->post('first_alternate_brand');
		$user['alternate_purchase_indicator'] = $this->input->post('alternate_purchase');
		$user['government_id_number'] = $this->input->post('giid_number');
		$user['government_id_type'] = $this->input->post('giid_type');

		$user['referred_by_code'] = $this->input->post('referred_code');
		$user['outlet_code'] = $this->input->post('outlet_code');

		$brand = $this->db->select()
			->from('tbl_brands')
			->where('brand_id', $user['current_brand'])
			->get()
			->row();

		$user['current_brand_code'] = $brand->code;
		$user['current_brand_flavor'] = $brand->flavor;
		$user['current_brand_affinity'] = 0;
		$user['current_brand_tar_level'] = $brand->tar;

		$brand = $this->db->select()
			->from('tbl_brands')
			->where('brand_id', $user['first_alternate_brand'])
			->get()
			->row();

		$user['first_alternate_brand_code'] = $brand->code;
		$user['first_brand_flavor'] = $brand->flavor;
		$user['first_alternate_brand_affinity'] = 0;
		$user['first_brand_tar_level'] = $brand->tar;

		$user['date_created'] = date('Y-m-d H:i:s');
		$user['date_of_capture'] = $user['date_created'];

		$user['referral_code'] = unique_code();
		$user['salt'] = generate_password(20);
		$this->load->library('Encrypt');
		$user['password'] = $this->encrypt->encode(generate_password(), $user['salt']);
		$user['status'] = PENDING_CSR;

		$this->db->insert('tbl_registrants', $user);
		$userId = $this->db->insert_id();
		$path = './uploads/profile/';

		if (!is_dir($path)) {
			mkdir($path);
		}
		if (!is_dir($path.$userId)) {
			mkdir($path.$userId);
		}
		$path = $path.$userId.'/giid/';
		if (!is_dir($path)) {
			mkdir($path);
		}

		$filename = null;
		if (!$imageString) {
			$config = array();
			$config['upload_path'] = $path;
			$config['allowed_types'] = GIID_FILE_TYPES;
			$config['max_size'] = FILE_UPLOAD_LIMIT;
			$config['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['giid']['name'], PATHINFO_EXTENSION);

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('giid')) {
				$this->db->where('registrant_id', $userId)
					->delete('tbl_registrants');
				$this->output->set_output('ERROR UPLOAD: '.$this->upload->display_errors());
				return;
			}
			$filename = $config['file_name'];
		} else {
			$filename = sha1(uniqid()).'.jpg';
			file_put_contents($path.$filename, gzuncompress(base64_decode($imageString)));
		}

		$this->db->where('registrant_id', $userId);
		$this->db->update('tbl_registrants', array('giid_file' => $filename));
	}

	public function forgot()
	{
		$data = array();
		$data['invalid_forgot_email'] = $this->session->flashdata('invalid_forgot_email');
		$this->load->layout('user/forgot', $data, 'user/template');
	}

	public function tracking_code()
	{
		$data = array();
		$data['invalid'] = $this->session->flashdata('invalid_tracking_code');
		$this->load->layout('user/tracking_code', $data, 'user/template');
	}

	public function submit_track_code()
	{
		if ($this->input->post('code')) {
			$user = (array) $this->db->select()
				->from('tbl_registrants')
				->where('tracking_code', $this->input->post('code'))
				->where('status', PENDING_GIID)
				->limit(1)
				->get()
				->row();
			if ($user) {
				if (time() > strtotime(TRACKING_CODE_EXPIRATION, strtotime($user['date_tracking_code_created']))) {
					$this->session->set_userdata('invalid_tracking_code', 'Tracking code already expired');
				} else {
					$this->session->set_userdata('tracking_code', $user['tracking_code']);
					redirect('user/track_submit');
				}
			} else {
				$this->session->set_userdata('invalid_tracking_code', 'The tracking code you entered is incorrect. Please try again.');
			}
		}
		redirect('submit_giid');
	}

	public function track_submit()
	{
		if (!$this->session->userdata('tracking_code')) {
			redirect('submit_giid');
		}
		$data = array();
		$data['token'] = generate_token();
		$this->session->set_userdata('reg_token', $data['token']);
		$this->load->model('Registration_Model');
		$data['giid_types'] = $this->Registration_Model->get_giid_types();
		$this->load->layout('user/track_submit', $data, 'user/template');
	}

	public function submit_late_giid()
	{
		if ($this->input->server('REQUEST_METHOD') != 'POST') {
			$this->output->set_output('Invalid request');
			return;
		}

		$registrant = $this->db->select()
			->from('tbl_registrants')
			->where('tracking_code', $this->session->userdata('tracking_code'))
			->get()
			->row();
		if (!$registrant) {
			$this->output->set_output('Invalid tracking code');
			return;
		}

		$this->validate_step_3(true);
		if (!$this->form_validation->run()) {
			$this->session->set_userdata('reg_token', generate_token());
			$this->output->set_output(validation_errors());
			return;
		}

		$user = array();
		$user['government_id_number'] = $this->input->post('giid_number');
		$user['government_id_type'] = $this->input->post('giid_type');
		$user['status'] = PENDING_CSR;
		
		$path = './uploads/profile/';
		$imageString = $this->input->post('captured_image_ba');

		if (!is_dir($path)) {
			mkdir($path);
		}
		if (!is_dir($path.$registrant->registrant_id)) {
			mkdir($path.$registrant->registrant_id);
		}
		$path = $path.$registrant->registrant_id.'/giid/';
		if (!is_dir($path)) {
			mkdir($path);
		}

		if (!$imageString) {
			$config = array();
			$config['upload_path'] = $path;
			$config['allowed_types'] = GIID_FILE_TYPES;
			$config['max_size'] = FILE_UPLOAD_LIMIT;
			$config['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['giid']['name'], PATHINFO_EXTENSION);

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('giid')) {
				$this->output->set_output('ERROR UPLOAD: '.$this->upload->display_errors());
				return;
			}
			$user['giid_file'] = $config['file_name'];
		} else {
			$user['giid_file'] = sha1(uniqid()).'.jpg';
			file_put_contents($path.$user['giid_file'], gzuncompress(base64_decode($imageString)));
		}

		$user['tracking_code'] = null;
		$this->db->where('registrant_id', $registrant->registrant_id);
		$this->db->where('tracking_code', $this->session->userdata('tracking_code'));
		$this->db->update('tbl_registrants', $user);
		if ($this->db->affected_rows()) {
			$this->session->unset_userdata('tracking_code');
		}
	}

	public function register()
	{
		$data = array();
		$data['token'] = generate_token();
		$this->load->model('Registration_Model');
		$this->session->set_userdata('reg_token', $data['token']);
		$data['brands'] = $this->Registration_Model->get_brands();
		$data['mobile_prefixes'] = $this->Registration_Model->get_mobile_prefixes();
		$data['provinces'] = $this->Registration_Model->get_provinces();
		$data['alternate_purchase'] = $this->Registration_Model->get_alternate_purchase();
		$data['giid_types'] = $this->Registration_Model->get_giid_types();
		$this->load->layout('user/steps', $data, 'user/template');
	}

	public function cities()
	{
		$this->load->model('Registration_Model');
		$provinceId = $this->input->get('province');
		if (!$provinceId) {
			return;
		}
		$cities = json_encode($this->Registration_Model->get_cities($provinceId));
		$this->output->set_content_type('application/json')
			->set_output($cities);
	}
}