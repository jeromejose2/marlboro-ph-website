<?php if(!defined("BASEPATH")){ exit("No direct script access allowed!"); }

set_time_limit(0);
ini_set('memory_limit', '512M');
class Refer extends CI_Controller {

	var $edm;
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('referral_model');
		require_once 'application/libraries/Edm.php';
		$this->edm = new Edm();
	}

	public function reset_refcode(){
		$this->db->update('tbl_registrants',  array('referral_code' => '' ));
	}
	public function insert_refcode(){
		$chrA = 97;
		$chrZ = 122;
		$nwChr= $chrA;
		$nwChr2 = $chrA;
		$max = 20;
		$count = 1;
		$i = 1000;

		$registrants = $this->db
			->where('status',1)
			->get('tbl_registrants')
			->result();

		if($registrants){
			$code = 'aa1000';
			foreach ($registrants as $r) {

				$this->db
				->where('registrant_id', $r->registrant_id)
				->update('tbl_registrants',  array('referral_code' => $this->generate_referral_code($code) ));
				$code = $this->generate_referral_code($code);
			}
		}
	}
	private function generate_referral_code($code){

		$digit = substr($code, 2);

		$digit++;
		$chr1 = ord($code[0]);
		$chr2 = ord($code[1]);
		$chrStart = 97;
		$chrEnd   = 122;


		if($digit>9999){
			$digit = 1000;
			$chr2++;
		}

		if($chr2>$chrEnd){
			$chr1++;
			$chr2=$chrStart;
		}

		$chr1 = $chr1 > $chrEnd ? $chrStart : $chr1;
		return chr($chr1) . chr($chr2) . $digit; 
	}

	public function ref(){
		$chrA = 97;
		$chrZ = 122;
		$nwChr= $chrA;
		$nwChr2 = $chrA;
		$max = 20;
		$count = 1;
		$i = 1000;
		while(true){

			$count++;
			$i++;
			if($count>=$max){
				break;
			}
			if($i>9999){
				$nwChr++;
				$i=1000;
			}

			if($nwChr>$chrZ){
				$nwChr2++;
				$nwChr=$chrA;
			}
			
			$nwChr = $nwChr > $chrZ ? $chrA : $nwChr; 
			$nwChr2 = $nwChr2 > $chrZ ? $chrA : $nwChr2; 

			echo chr($nwChr2).chr($nwChr).$i;
			echo "<br>";
		}
		
	}

	
	public function index(){

		if($this->input->post()){
			$post = $this->input->post();
			for ($i=0; $i < count($post['email']); $i++) { 

				$result = $this->referral_model->save(
					$post['email'][$i],
					$this->session->userdata('user_id'),
					$this->session->userdata('user')['referral_code'],
					$post['name'][$i]
				);

				if($result=='success'){

					$this->send_referral(
						array('las1_name' => $this->session->userdata('user')['first_name'],
							  'las1_email'=> $this->session->userdata('user')['email'],
							  'las2_name' => $post['name'][$i],
							  'las2_email'=> $post['email'][$i],
							  'ref_code'  => $this->session->userdata('user')['referral_code']));

					$data['result']['success'][] = array(
						'email' => $post['email'][$i],
						'name' => $post['name'][$i],
						'message' => $result['message']);
				}

				if($result=='registered'){
					$data['result']['registered'][] = array(
						'email' => $post['email'][$i],
						'name' => $post['name'][$i],
						'message' => $result['message']);
				}

				if($result=='pending'){
					$data['result']['pending'][] = array(
						'email' => $post['email'][$i],
						'name' => $post['name'][$i],
						'message' => $result['message']);
				}
				
				
			}
			$this->session->unset_userdata('referral_data');
			$this->session->set_userdata('referral_data',$data);
			redirect( BASE_URL .'refer/done');
		}

		$this->load->layout('referral/index', null);
	}

	public function done(){

		$data = $this->session->userdata('referral_data');

		if(!$data){
			redirect( BASE_URL .'refer');
		}
		
		$this->load->layout('referral/msg', $data);
	}

	private function send_edm($las1_email, $las1_name, $las2_name, $code, $las2_email) {

		$recipient_added = $this->edm->add_recipient_referral(EDM_MARLBORO_REFER_LIST_ID, $las2_email, $las2_name);
		$sent = $this->edm->send_recipient_referral(EDM_MARLBORO_REFER_MAIL_ID, $las1_email, $las1_name, $las2_name, $code, $las2_email);

		if($recipient_added !== true)
			$r_error = $recipient_added;

		if($sent !== true)
			$s_error = $sent;
	}

	private function send_referral($v){
		$template = file_get_contents('application/views/referral/edm-template.html');
		$search = array('{{LAS2_FIRSTNAME}}','{{LAS1_FIRSTNAME}}','{{REFERRAL_CODE}}','{{url}}');
		$replace = array($v['las2_name'],$v['las1_name'],$v['ref_code'],BASE_URL);
		$message = str_replace($search, $replace, $template );
		
		/*
		$this->load->library('email');

		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['mailtype'] = 'html';

		$this->email->initialize($config);
		$this->email->from($v['las1_email'],$v['las1_name']);
		$this->email->to($v['las2_email']); 
		$this->email->subject('Join and start collecting points to get instant prizes');

		$this->email->message($message);	
		$this->email->send();
		echo $this->email->print_debugger();
		*/
		$to = $v['las2_email'];
		$subject  = 'Join and start collecting points to get instant prizes';
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: '.$v['las1_name'].' <'.$v['las1_email'].'>' . "\r\n";

		mail($to, $subject, $message, $headers);

	}
}
