<?php
class Games extends CI_Controller
{
	function __construct() {
		parent::__construct();
		$this->load->model('webgames');
		$this->load->library('encrypt');
	}

	function index($game = false) {
		if(!$game) {
			$data['games'] = $this->webgames->get_games()->result_array();
			$this->load->layout('webgames/index', $data);	
		} else {
			$data['game'] = (array) $this->webgames->get_game($game);
			if(!$data['game'])
				redirect('games');
			$data['top_scorers'] = $this->webgames->get_leaderboard($data['game']['game_id'])->result_array();
			$this->load->layout('webgames/inner', $data);	
		}
			
	}

	function save_game() {
		$this->load->model('points_model');
		$this->load->model('notification_model');

		$token = $this->encrypt->decode($_SESSION['token']);
		$end_time = date('Y-m-d H:i:s');
		$start_secs = strtotime($token);
		$end_secs = strtotime($end_time);
		$secs = ($end_secs - $start_secs);
		$game_name = $_REQUEST['name'];
		$game = (array)$this->webgames->get_game($game_name);
		
		$calculated_score = strtolower($game_name) == 'crossout' ? ($this->calculate_score($_REQUEST['ids'], $game_name) - $this->calculate_score($_REQUEST['ids2'], $game_name)) : $this->calculate_score($_REQUEST['ids'], $game_name);
		$registrant_id = $this->session->userdata('user_id');

		$post['game_id'] = $game['game_id'];
		$post['registrant_id'] = $registrant_id;
		$post['score'] = $_REQUEST['score'];
		$post['computed_score'] = $calculated_score;
		$post['start'] = $token;
		$post['end'] = $end_time;

		define('WALLBREAKER', 1);
		define('CROSSOUT', 2);
		define('WORDHUNT', 3);
		define('DOUBTCRASHER', 4);
		define('LEADERBOARD', 3);
		define('TOP_SCORE', 3);

			
		$range[WALLBREAKER]['low'] = 300;
		$range[WALLBREAKER]['medium'] = 500;
		$range[WALLBREAKER]['high'] = 700;
		$range[CROSSOUT]['low'] = 50;
		$range[CROSSOUT]['medium'] = 80;
		$range[CROSSOUT]['high'] = 120;
		$range[WORDHUNT]['low'] = 100;
		$range[WORDHUNT]['medium'] = 200;
		$range[WORDHUNT]['high'] = 300;
		$range[DOUBTCRASHER]['low'] = 300;
		$range[DOUBTCRASHER]['medium'] = 500;
		$range[DOUBTCRASHER]['high'] = 900;
		

		$points = 0;
		if($post['score'] >= $range[$game['game_id']]['low'] && $post['score'] < $range[$game['game_id']]['medium']) {
			$points = 10;
		} elseif($post['score'] >= $range[$game['game_id']]['medium'] && $post['score'] < $range[$game['game_id']]['high']) {
			$points = 20;
		} elseif($post['score'] >= $range[$game['game_id']]['high']) {
			$points = 30;
		}

		//echo $_SESSION['token'] . ' ? ' . str_replace(' ', '+', $_REQUEST['token']);
		if($points == 0 || $_SESSION['token'] != str_replace(' ', '+', $_REQUEST['token']) || $calculated_score != $_REQUEST['score'] || $secs <= 112 || $secs >= 128 || $_REQUEST['score'] > $game['max_score'] ) {
			if($points != 0)
				$post['is_cheater'] = 1;
			$this->webgames->save_user_game($post);
			die('0');
		}
		
		
		
		$game_log_id =  $this->webgames->save_user_game($post);

		

		$leaderboard_array = $this->has_reached_leaderboard($game['game_id'], $registrant_id, $rank) ? array('achievement' => GAME_REACH_LEADERBOARD, 'remarks'	=> '{{ name }} reached the leaderboard for ' . $game['name']): null;
		$top_score_array = $rank === 0 ? array('achievement' => TOP_SCORE, 'remarks'	=> '{{ name }} is a top scorer for ' . $game['name']) : null;
		$is_inserted = $this->points_model->earn(WEBGAMES,  array('suborigin' 	=> $game_log_id, 
																  'date'		=> $end_secs,
																  'records' 	=> array(
																						array(
																							'points' 		=> $points,
																							'achievement' 	=> GAME_FINISH_LEVEL,
																							'remarks'		=> '{{ name }} accomplished '. $game['name'] 
																							),
																							$top_score_array,
																							$leaderboard_array,
																							)
																						));

		if($is_inserted) {
			if($is_inserted[GAME_FINISH_LEVEL]) {
				$this->notification_model->notify($registrant_id, GAME_FINISH_LEVEL, array(
															'message' => 'You accomplished ' . $game['name'] . ' on Web Games. You gained ' . $is_inserted[GAME_FINISH_LEVEL]  . ' points. Thank you! ',
															'suborigin' => $game_log_id
															));
				
			}
			if($leaderboard_array && isset($is_inserted[GAME_REACH_LEADERBOARD])) {
				$this->notification_model->notify($registrant_id, GAME_REACH_LEADERBOARD, array(
																'message' => 'You have reached the leaderboard for ' . $game['name'] . ' on Web Games. You gained ' . $is_inserted[GAME_REACH_LEADERBOARD]  . ' points. Thank you!',
																'suborigin' => $game_log_id
																));
			}

			if($top_score_array && isset($is_inserted[TOP_SCORE])) {
				$this->notification_model->notify($registrant_id, TOP_SCORE, array(
																'message' => 'You are now the top scorer for ' . $game['name'] . ' on Web Games. You gained ' . $is_inserted[TOP_SCORE]  . ' points. Thank you!',
																'suborigin' => $game_log_id
																));
			}
		}

		
		//print_r($is_inserted);
		//echo $points;
		echo $is_inserted !== false && $is_inserted[GAME_FINISH_LEVEL] > 0 ? $is_inserted[GAME_FINISH_LEVEL] : '-1';
	}

	function calculate_score($post = '', $game_name= '') {
		//$post = '49,64,42,40,65,76,58,22,101,19,9,99,104,105,98,37,105,74,37,30,83,81,96,38,14,41,99,67,42,57,34,93,91,108,118,75,88,32,26,43,109,105,19,112,101,36,72,116,27,17,59,40,39,97,7,13,4,117,37,68,70,45,59,27,87,97,78,76,25,52,120,16,47,4,36,117,69,107,84,90,101,76,27,116,60,109';
		//$game_name = 'crossout';
		//$post = $_REQUEST['ids'];
		//$game_name = $_REQUEST['name'];
		$word_ids = explode(',', $post);
		$param['table'] = 'tbl_game_words';
		$words = $this->webgames->get_game_words()->result_array();
		$words_arr = array();
		$score = 0;
		if($words) {
			foreach ($words as $key => $value) {
				$words_arr[$value['game_word_id']] = $value[strtolower($game_name)];
			}
		}
		foreach($word_ids as $k => $v) {
			$score = isset($words_arr[$v]) ? $score + $words_arr[$v] : $score;
		}
		return $score;
	}


	function leaderboard() {
		$game_name = $this->input->get('name');
		$game =  (array) $this->webgames->get_game($game_name);
		$top_scorers = $this->webgames->get_leaderboard($game['game_id'])->result_array();
		$data['top_scorers'] = $top_scorers;
		$this->load->layout('webgames/leaderboard', $data, 'blank');
	}

	function has_reached_leaderboard($game_id, $registrant_id, &$rank = false) {
		$top_scorers = $this->webgames->get_leaderboard($game_id)->result_array(); 
		if($top_scorers) {
			foreach ($top_scorers as $key => $value) {
				if($value['registrant_id'] == $registrant_id) {
					$rank = $key;
					return true;
				}
					
			}
		}
		return false;
 	}

 	function start_game() {
 		$_SESSION['token'] = $this->encrypt->encode(date('Y-m-d H:i:s'));
 		echo $_SESSION['token'];
 	}

 	function _remap($method) {
		if($method == 'save_game')
			$this->save_game();
		elseif($method == 'leaderboard')
			$this->leaderboard();
		elseif($method == 'start_game')
			$this->start_game();
		elseif($method == 'calculate_score')
			$this->calculate_score();
		else
			$this->index($this->uri->segment(2));
	} 
}