<?php

class Comment extends CI_Controller
{
	public function attach()
	{
		$data = array(
			'origin' => $this->input->get('origin'),
			'suborigin' => $this->input->get('suborigin'),
			'reply' => (int) $this->input->get('reply') === 1 ? '-reply' : ''
		);
		$this->load->view('comment/attach', $data);
	}

	public function upload()
	{
		$path = './uploads/profile/';
		if (!is_dir($path)) {
			mkdir($path);
		}
		$userId = $this->session->userdata('user_id');
		if (!$userId) {
			$this->output->set_output('ERROR: Invalid user');
			return;
		}

		if (!is_dir($path.$userId)) {
			mkdir($path.$userId);
		}
		$path = $path.$userId.'/comments/';
		if (!is_dir($path)) {
			mkdir($path);
		}

		$config = array();
		$config['upload_path'] = $path;
		$config['allowed_types'] = FILE_UPLOAD_ALLOWED_TYPES;
		$config['max_size'] = FILE_UPLOAD_LIMIT;
		$config['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['attachment']['name'], PATHINFO_EXTENSION);

		$this->load->library('upload', $config);
		if ($this->upload->do_upload('attachment')) {
			$this->load->helper('resize');
			resize(array(
				'width' => 150,
				'height'=> 150,
				'source_image' => $path.$config['file_name'],
				'new_image_path' => $path,
				'file_name'=> $config['file_name']
			));
			$this->output->set_output($config['file_name']);
		} else {
			$this->output->set_output('ERROR: '.$this->upload->display_errors());
		}
	}
}