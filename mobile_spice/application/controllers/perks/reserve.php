<?php if(!defined("BASEPATH")){ exit('No direct script access allowed!'); }

class Reserve extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Perks_Model');
	}

	public function index()
	{
		$data = array();
		$this->load->helper('text');
		$data['items'] = $this->Perks_Model->get_lamps();
		$data['viewed'] = $this->Perks_Model->has_viewed_splash();
		$this->load->layout('perks/reserve', $data);
	}

	public function mechanics()
	{
		$mechanics = $this->Perks_Model->get_mechanics('reserve');
		$data['mechanics'] = $mechanics['description'];
		$data['what'] = 'Reserve';
		$this->load->view('perks/mechanics-popup', $data);
	}

	public function item()
	{
		$id = $this->input->get('id');
		if (!$id) {
			show_404();
		}

		$data = array();
		$data['selected_schedule'] = $this->input->get('booking_date');
		$data['selected_guest_names'] = $this->input->get('guest_name');
		$data['selected_guest_emails'] = $this->input->get('guest_email');
		$data['selected_mobile_num'] = $this->input->get('mobile_num');

		$data['item'] = $this->Perks_Model->get_lamp($id);
		if (!$data['item']) {
			show_404();
		}
		$data['media'] = $this->Perks_Model->get_lamp_media($id);
		$data['schedules'] = $this->Perks_Model->get_lamp_schedules($id);
		$data['dates'] = array();
		$data['dates_slots'] = array();
		foreach ($data['schedules'] as $sched) {
			$data['dates'][] = $sched['date'];
			$data['dates_slots'][$sched['date']] = array(
				'schedule_id' => $sched['schedule_id'],
				'slots' => $sched['slots'],
				'original_slots' => $sched['original_slots'],
				'reservation_cost' => $sched['reservation_cost']
			);
		}
		$this->load->view('perks/reserve-item', $data);
	}

	public function confirm()
	{
		$data = $this->input->get();
		if (empty($data['id'])) {
			show_404();
		}

		$data['guest_count'] = 0;
		foreach ($data['guest_name'] as $name) {
			if ($name)
				$data['guest_count']++;
		}

		$data['item'] = $this->Perks_Model->get_lamp($data['id']);
		if (!$data['item']) {
			show_404();
		}
		$data['media'] = $this->Perks_Model->get_lamp_media($data['id']);
		$data['selected_schedule'] = $this->Perks_Model->get_lamp_schedule_by_id($data['booking_date']);
		$this->load->view('perks/reserve-confirm', $data);
	}
}