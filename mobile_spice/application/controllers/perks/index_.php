<?php if(!defined("BASEPATH")){ exit('No direct script access allowed!'); }

class Index extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Perks_Model');
	}
	
	public function index() {
		$data['thumbs'] = $this->Perks_Model->get_perks_thumbs();
		$this->load->layout('perks/index', $data); 
	}
}