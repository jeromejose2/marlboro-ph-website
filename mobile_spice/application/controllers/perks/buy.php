<?php if(!defined("BASEPATH")){ exit('No direct script access allowed!'); }

class Buy extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Perks_Model', 'profile_model'));
	}
	
	public function index() {
		$data = array();
		$data['items'] = $this->Perks_Model->get_buy_items();
		$data['viewed'] = $this->Perks_Model->has_viewed_splash();
		$this->load->layout('perks/buy', $data); 
	}

	public function item($id = '') {
		if(!$id) {
			redirect('perks/buy');
		}
		$this->load->model('Registration_Model');
		$data = array();
		$item = (array) $this->Perks_Model->get_buy_item($id);
		$user = $this->Registration_Model->get_user_by_id($this->session->userdata('user_id'));
		$item['total_points'] = $user['total_points'];
		$data['item'] = $item;
		$data['media'] = $this->Perks_Model->get_buy_item_media($id);
		$data['properties'] = $this->Perks_Model->get_properties($id);
		if($item['stock'] == 0) {
			$this->load->view('perks/buy-item-end', $data); 	
		} else {
			$this->load->view('perks/buy-item', $data); 
		}
		
	}

	public function buy_item() {
		if(!$this->input->post('id')) {
			redirect('perks/buy');
		}

		$this->load->model(array('Registration_Model', 'Points_Model'));
		$id = $this->input->post('id');
		$user = $this->Registration_Model->get_user_by_id($this->session->userdata('user_id'));
		$bid_data = $this->Perks_Model->check_bid_points($this->session->userdata('user_id'));
		
		$item = (array) $this->Perks_Model->get_buy_item($id);
		if($user['total_points'] < $item['credit_value']) {
			$data['success'] = 0;
			$data['error'] = "You don’t have enough points to buy " . $item['buy_item_name'] . ".";	
		} elseif($item['status'] != 1) {
			$data['success'] = 0;
			$data['error'] = $item['buy_item_name'] . " is not yet active for buying.";	
		} elseif($user['total_points'] >= $item['credit_value'] && $bid_data['available_points'] < $item['credit_value']) {
			$data['success'] = 0;
			$data['error'] = 'You only have ' . $bid_data['available_points'] . ' points available for you to use. You placed a bid for ' . implode(', ', $bid_data['items']) . ', and ' . $bid_data['reserved_points'] . ' points are blocked off while you are still the highest bidder.';	
		} else {
			if($this->Perks_Model->has_bought($id)) {
				$data['success'] = 0;
				$data['error'] = 'You already bought ' . $item['buy_item_name'] . '.';
			} elseif($item['status'] == 0 || $item['stock'] <= 0) { 
				$data['success'] = 0;
				$data['error'] = 'Sorry, this item is out of stock.';
			} else {
				$item_stock = $this->Perks_Model->save_buy_item($buy_id);
				# deduct stock count													
				if(isset($item_stock['stocks']) && $item_stock['stocks'] - 1 >= 0) {
					$data['success'] = 1;
					$data['id'] = $buy_id;
					$this->Points_Model->spend(PERKS_BUY, array(
											'points' => $item['credit_value'],
											'suborigin' => $id,
											'remarks' => '{{ name }} bought ' . $item['buy_item_name'] . ' on Perks'
											));

					$this->Perks_Model->deduct_stock($id, $item['stock'] - 1);

					$this->load->model('notification_model');
					//$buy = $this->Perks_Model->get_buy_details($id);
					$message = 'You have successfully purchased ' . $item['buy_item_name'] . ' for ' . $item['credit_value'] . ' points. <br><br>
								' . $item['buy_item_name'] . ' will be delivered to your registered mailing address within 60 days. Please ensure that all your contact details are up-to-date. <br><br>
								Continue logging on to MARLBORO.PH to earn more points!';

	 				$param = array('message'=>$message,'suborigin'=>$id);
					$this->notification_model->notify($this->session->userdata('user_id'),PERKS_BUY,$param);

					} else {

						$data['success'] = 0;
						$data['error'] = 'Sorry, you must select required specifications for ' .$item['buy_item_name'] . '.';
					}
				
				
				
			}
		}
		$this->output->set_content_type('application/json')
			 ->set_output(json_encode($data));

	}

	public function get_property2() {
		if(!$this->input->post('property1') || !$this->input->post('id')) {
			redirect('perks/buy');
		}
		$property1 = $this->input->post('property1');
		$id = $this->input->post('id');
		$data = $this->Perks_Model->get_properties_with_stocks($id, $property1, $stocks);
		if($stocks)
			$data[0]['stocks'] = $stocks;
		$this->output->set_content_type('application/json')
			 ->set_output(json_encode($data));
	}

	public function mechanics() {
		$mechanics = $this->Perks_Model->get_mechanics('buy');
		$data['mechanics'] = $mechanics['description'];
		$data['what'] = 'Buy';
		$this->load->view('perks/mechanics-popup', $data); 
	}

	public function confirm_address()
	{

		if($this->input->post()){


			if($this->validate_offer_confirmation()){

				$user_id = $this->session->userdata('user_id');
				$user = $this->session->userdata('user');
				$id = $this->input->post('id');
				
				if(!$this->input->post('confirm_redemption')){
					$this->profile_model->update('tbl_registrants',$this->input->post(),array('registrant_id'=>$user_id));
				}

				$data = array('msg'=>'You have successfully confirmed your mailing address!',
							  'error_holder'=>'',
							  'error'=>false,
 							  'btn'=>'',
							  'btn_text'=>'');

			}else{

				$data = array('msg'=>trim(validation_errors()),
							  'error_holder'=>'birthday-offer-error',
							  'error'=>true,
 							  'btn'=>'birthday-offer-btn',
							  'btn_text'=>'<i>Submit Confirmation</i>');
			}
  			$this->load->view('profile/submit-response',$data);

		}else{

			$row = $this->profile_model->get_row(array('table'=>'tbl_registrants',
															'where'=>array('registrant_id'=>$this->session->userdata('user_id'))
															)
													);
			$data['row'] = $row;
			$data['cities'] =  $this->profile_model->get_rows(array('table'=>'tbl_cities','where'=>array('province_id'=>$row->province)));		
			$data['provinces'] = $this->profile_model->get_rows(array('table'=>'tbl_provinces'));
			$data['id'] = $this->input->get('id');
			$this->load->view('perks/confirm-address',$data);

		}
		
	}

	private function validate_offer_confirmation()
	{

		$this->load->library('form_validation');
		$rules = array(array(
							 'field'   => 'prize_id',
							 'label'   => 'Birthday Offer',
							 'rules'   => 'callback_check_birthday_offer'
						  )
		   			);

		if(!$this->input->post('confirm_redemption')){

			$rules[] = array(array(
							 'field'   => 'street_name',
							 'label'   => 'Street Name',
							 'rules'   => 'trim|required'
						  ),
					   array(
							 'field'   => 'barangay',
							 'label'   => 'Barangay',
							 'rules'   => 'trim|required'
						  ),
					   array(
							 'field'   => 'zip_code',
							 'label'   => 'Zip Code',
							 'rules'   => 'trim|required'
						  ),
					   array(
							 'field'   => 'city',
							 'label'   => 'City',
							 'rules'   => 'trim|required'
						  ),
					   array(
							 'field'   => 'province',
							 'label'   => 'Province',
							 'rules'   => 'trim|required'
						  )
		   			);

		}

		$this->form_validation->set_rules($rules);		
		return $this->form_validation->run();
	}

}