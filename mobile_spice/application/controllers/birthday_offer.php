<?php if(!defined("BASEPATH")){ exit("No direct script access allowed!"); }

class Birthday_offer extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('profile_model');


	}

	public function get_birthday_offer()
	{
		$prize_id = $this->input->get('prize_id');

		$user = $this->session->userdata('user');
		$data['flash_offer'] = $this->input->get('flash_offer');
		$data['birthday_offer'] = $this->profile_model->get_row(array('table'=>'tbl_birthday_offers','where'=>array('prize_id' =>$prize_id)) );
  	    $this->load->view('birthday_offer/birthday-offer',$data);
	}

	public function confirm_offer()
	{

		if($this->input->post()){


			if($this->validate_offer_confirmation()){


				

				$user_id = $this->session->userdata('user_id');
				$user = $this->session->userdata('user');
				
				$birthday_offer = $this->profile_model->get_row(array('table'=>'tbl_birthday_offers',
																	   'where'=>array('month' =>date('m',strtotime($user['birthdate'])),
																	   					'month'=>date('m'),
																	   				  	'YEAR(date_created)' =>date('Y'),
																	   				  	'stock >'=>0,
																	   				  	'is_deleted'=>0
																	   				  )
 																	)
																);
				
				$new_stock = (int)$birthday_offer->stock - 1;

				$data = array('registrant_id'=>$user_id,
							  'registrant_name'=>$user['first_name'].' '.$user['last_name'],
							  'registrant_birthdate'=>$user['birthdate'],
							  'prize_id'=>$birthday_offer->prize_id,							  
							  'prize_name'=>$birthday_offer->prize_name,
							  'prize_description'=>$birthday_offer->description,
							  'prize_month'=>$birthday_offer->month,
							  'prize_image'=>$birthday_offer->prize_image,
							  'send_type'=>$birthday_offer->send_type,
							  'redemption_address'=>$birthday_offer->redemption_address,
							  'redemption_instruction'=>$birthday_offer->redemption_instruction
							  );

				$id = $this->profile_model->insert('tbl_user_birthday_prizes',$data);
				$this->profile_model->update('tbl_birthday_offers',array('stock'=>$new_stock),array('prize_id'=>$birthday_offer->prize_id));

				$this->load->model('notification_model');
				if($birthday_offer->send_type=='Delivery'){
					$message = 'Your birthday gift item from Marlboro will be delivered to your mailing address within 3-5 days.';
				}else{
					$message = 'Please follow the instruction below to redeem your birthday gift item from Marlboro:<br/>'.$birthday_offer->redemption_instruction;
				}

 				$param = array('message'=>$message,'suborigin'=>$id);
				$this->notification_model->notify($user_id,BIRTHDAY_OFFERS,$param);

				if(!$this->input->post('confirm_redemption')){
					$this->profile_model->update('tbl_registrants',$this->input->post(),array('registrant_id'=>$user_id));
				}

				$data = array('msg'=>'<h3>You have successfully confirmed the birthday offer!</h3>',
							  'error_holder'=>'',
							  'error'=>false,
 							  'btn'=>'',
							  'btn_text'=>'');

			}else{

				$data = array('msg'=>trim(validation_errors()),
							  'error_holder'=>'birthday-offer-error',
							  'error'=>true,
 							  'btn'=>'birthday-offer-btn',
							  'btn_text'=>'<i>Submit Confirmation</i>');
			}
  			$this->load->view('profile/submit-response',$data);

		}else{

			$row = $this->profile_model->get_row(array('table'=>'tbl_registrants',
															'where'=>array('registrant_id'=>$this->session->userdata('user_id'))
															)
													);
			$data['row'] = $row;
			$data['cities'] =  $this->profile_model->get_rows(array('table'=>'tbl_cities','where'=>array('province_id'=>$row->province)));		
			$data['provinces'] = $this->profile_model->get_rows(array('table'=>'tbl_provinces'));
			$this->load->view('birthday_offer/confirm-birthday-offer',$data);

		}
		
	}

	private function validate_offer_confirmation()
	{

		$this->load->library('form_validation');
		$rules = array(array(
							 'field'   => 'prize_id',
							 'label'   => 'Birthday Offer',
							 'rules'   => 'callback_check_birthday_offer'
						  )
		   			);

		if(!$this->input->post('confirm_redemption')){

			$rules[] = array(array(
							 'field'   => 'street_name',
							 'label'   => 'Street Name',
							 'rules'   => 'trim|required'
						  ),
					   array(
							 'field'   => 'barangay',
							 'label'   => 'Barangay',
							 'rules'   => 'trim|required'
						  ),
					   array(
							 'field'   => 'zip_code',
							 'label'   => 'Zip Code',
							 'rules'   => 'trim|required'
						  ),
					   array(
							 'field'   => 'city',
							 'label'   => 'City',
							 'rules'   => 'trim|required'
						  ),
					   array(
							 'field'   => 'province',
							 'label'   => 'Province',
							 'rules'   => 'trim|required'
						  )
		   			);

		}

		$this->form_validation->set_rules($rules);		
		return $this->form_validation->run();
	}

	public function check_birthday_offer()
	{

		$user = $this->session->userdata('user');
		$birthday_offer = $this->profile_model->get_row(array('table'=>'tbl_birthday_offers',
															   'where'=>array('month' =>date('m',strtotime($user['birthdate'])),
															   					'month'=>date('m'),
															   				  	'YEAR(date_created)' =>date('Y'),
															   				  	'stock >'=>0
															   				  ),
															   'fields'=>'prize_id'
															)
														);
		
		if($birthday_offer && (int)$this->session->userdata('user_id')){

			$user_birthday_prize = $this->profile_model->get_row(array('table'=>'tbl_user_birthday_prizes',
															   			'where'=>array('prize_id' =>$birthday_offer->prize_id,'registrant_id' =>$this->session->userdata('user_id')),
															   			'fields'=>'user_birthday_prize_id'
																		)
																);				 
 			if($user_birthday_prize){
 				$this->form_validation->set_message('check_birthday_offer','You have already confirmed the birthday offer to your account.');
				return false;
			}else{				
				return true;
			}


		}else{
			$this->form_validation->set_message('check_birthday_offer','No available birthday offer at this period of time.');
			return false;
		}

		return true;
	}



}