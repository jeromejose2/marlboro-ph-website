<?php if(!defined("BASEPATH")){ exit('No direct script access allowed!'); }

class trash extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('profile_model');
	}


	public function index()
	{

 		$user_id = $this->session->userdata('user_id');
 		$message_id = $this->uri->segment(3);
 

 		if($message_id){

 			$this->profile_model->update('tbl_messages',array('message_status'=>1),array('recipient_id'=>$user_id));
 			$data['message_id'] =  $message_id;
 			$this->load->layout('trash/message-content',$data);

 		}else{
 			 
			$rows = $this->profile_model->get_rows(array('table'=>'tbl_messages',
														 'where'=>array('message_reply_to_id'=>0,"is_deleted_by_sender = '$user_id' OR is_deleted_by_recipient = '$user_id' "=>null,"(recipient_id = '$user_id' OR sender_id = '$user_id') "=>null),
 														 'fields'=>'subject,message_id'
														)
													);
			$messages = array();

			if($rows){

				foreach($rows->result() as $v){

					$message_id = $v->message_id;
					$row = $this->profile_model->get_rows(array('table'=>'tbl_messages as m',
																 'where'=>array("(m.message_reply_to_id = '".$message_id."' OR m.message_id = '".$message_id."') "=>null),
																 'order_by'=>array('field'=>'m.message_date_sent','order'=>'DESC'),
																 'join'=>array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=m.sender_id'),
																 'fields'=>'r.first_name,r.middle_initial,r.third_name,m.message_status,m.message,m.message_date_sent'
														)
													)->row();

					$messages[] = array('message_id'=>$message_id,
										'subject'=>$v->subject,
										'sender_name'=>$row->first_name.' '.$row->middle_initial.' '.$row->third_name,
										'message'=>$row->message,
										'status'=>$row->message_status,
										'date_sent'=>$row->message_date_sent
										);
				}

			}

			$data['rows'] = json_decode(json_encode($messages),false);			
			$this->load->layout('trash/group-message',$data);
 			//$this->output->enable_profiler(true);
 		} 		 
		 
	}

	public function delete_messages()
	{

		$message_ids = $this->input->post('messages');
		$user_id = $this->session->userdata('user_id');

		if($message_ids){
			foreach($message_ids as $v){
				$row = $this->profile_model->get_row(array('table'=>'tbl_messages',
															'where'=>array('message_id'=>$v,"(recipient_id = '$user_id' OR sender_id = '$user_id') "=>null)
															)
													);

				if($row){
					$update = ($user_id==$row->sender_id) ? array('is_deleted_by_sender'=>$user_id) : array('is_deleted_by_recipient'=>$user_id);
					$this->profile_model->update('tbl_messages',$update,array('message_id'=>$v));
				}
				
			}
		}
		redirect('messages/inbox');

	}


	public function getTemplate()
	{
		$this->load->view('trash/messages');
	}

 

}