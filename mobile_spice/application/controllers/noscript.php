<?php

class Noscript extends CI_Controller
{
	public function index()
	{
		$this->session->clear();
		$this->load->view('noscript');
	}
}