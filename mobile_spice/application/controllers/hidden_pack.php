<?php if(!defined("BASEPATH")){ exit('No direct script access allowed!'); }

class Hidden_pack extends CI_Controller {

 
	public function index()
	{
		$this->load->model(array('Points_Model','profile_model','notification_model'));
		
		$user_id = $this->session->userdata('user_id');
		$row = $this->profile_model->get_row(array('table'=>'tbl_hidden_pack_logs','where'=>array('registrant_id'=>$user_id,'DATE(date_added) = CURDATE()'=>null)));

		if(!$row){
			$id = $this->profile_model->insert('tbl_hidden_pack_logs',array('registrant_id'=>$user_id,'page_displayed'=>$this->session->userdata('hidden_pack_display_page'),'date_added'=>true));
			$this->session->unset_userdata('hidden_pack_display_page');
			$this->Points_Model->earn(HIDDEN_MARLBORO, array('suborigin' => $id));

			$param = array('message'=>'You have found the hidden marlboro pack. You gained 10 points.','suborigin'=>$id);
			$this->notification_model->notify($user_id,HIDDEN_MARLBORO,$param);

			$data['message'] = "You found the hidden pack and earned 10 points!";
		}else{
			$data['message'] = "You already found the hidden pack and earned 10 points.";
		} 

		$this->load->view('hidden_pack/found-marlboro',$data);

		
	}

}