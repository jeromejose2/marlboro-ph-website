<?php

class Comment_Replies extends NW_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Comment_Model');
	}
	
	public function get()
	{
		$commentId = $this->input->get('id');
		if (!$commentId) {
			show_404();
		}
		$limit = 5;
		$page = (int) $this->input->get('page');
		if (!$page) {
			$page = 1;
		}
		$this->output->set_output(
			json_encode($this->Comment_Model->get_replies($commentId, $limit))
		);
	}
}