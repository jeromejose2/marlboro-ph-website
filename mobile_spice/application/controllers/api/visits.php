<?php

class Visits extends NW_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('View_Model');
	}

	public function post()
	{
		$result = array(
			'success' => false,
			'visits' => 0
		);
		$origin = $this->input->post('origin');
		$suborigin = $this->input->post('suborigin');
		if ($origin && $suborigin) {
			$result['success'] = $this->View_Model->visit($origin, $suborigin);
		}

		$result['visits'] = $this->View_Model->get_visits_count($origin, $suborigin);
		$this->output->set_output(json_encode($result));
	}

	public function get()
	{
		$result = array(
			'visits' => 0
		);
		$origin = $this->input->get('origin');
		$suborigin = $this->input->get('suborigin');
		if ($origin && $suborigin) {
			$result['visits'] = $this->View_Model->get_visits_count($origin, $suborigin);
		}

		$this->output->set_output(json_encode($result));
	}
}