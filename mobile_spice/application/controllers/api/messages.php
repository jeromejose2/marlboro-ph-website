<?php 

class Messages extends NW_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->model('profile_model');
	}

	public function get(){
		
		$id = $this->session->userdata('user_id');
 		$message_id = $this->input->get('message_id');


		$row =  $this->profile_model->get_row(array('table'=>'tbl_messages',
													'where'=>array('message_id'=>$message_id,
																	'message_reply_to_id'=>0,
																	"(recipient_id = '$id' OR sender_id = '$id')"=>null)
													)
												);
		
		$result = array();

		if($row){

			$rows =  $this->profile_model->get_rows(array('table'=>'tbl_messages',
														  'where'=>array("(message_id = '$message_id' OR message_reply_to_id = '$message_id') "=>null),
														  'order_by'=>array('field'=>'message_date_sent','order'=>'asc'),
														  'fields'=>'recipient_id,sender_id,message_id,message_reply_to_id,subject,message,message_date_sent'
														)
													);

			if($rows->num_rows()){

				$subject = array();
				$contents = array();

				foreach($rows->result() as $v){

					if(!$v->message_reply_to_id){

						$subject = array('subject'=>'RE: '.$v->subject,
										'date_subject_sent'=>DATE('F d, Y',strtotime($v->message_date_sent)).' at '.date('h:i a',strtotime($v->message_date_sent)),
											'message'=>$v->message_id
										);
					}

					$sender =  $this->profile_model->get_row(array('table'=>'tbl_registrants',
																	'where'=>array('registrant_id'=>$v->sender_id),
																	'fields'=>'first_name,middle_initial,third_name'
																)
															);

					$recipient =  $this->profile_model->get_row(array('table'=>'tbl_registrants',
																		'where'=>array('registrant_id'=>$v->recipient_id),
																		'fields'=>' first_name,middle_initial,third_name'
																	)
																);

					if($id == $v->recipient_id){
						$this->profile_model->update('tbl_messages',array('message_status'=>1),array('message_id'=>$v->message_id));
					}
					

					$contents[] = array('message_id'=>$v->message_id,
											'sender_name'=>$sender->first_name.' '.$sender->middle_initial.' '.$sender->third_name,
											'recipient_name'=>$recipient->first_name.' '.$recipient->middle_initial.' '.$recipient->third_name,
											'date_message_sent'=>DATE('F d, Y',strtotime($v->message_date_sent)).' at '.date('h:i a',strtotime($v->message_date_sent)),
											'message'=>$v->message
											);			

				}

				$result = array_merge(array('content'=>$contents),$subject);

			}

		}

		

		$this->output->set_output(json_encode($result));

	}


	public function post()
	{

		$message_id = $this->input->post('message_id');
		$user_id = $this->session->userdata('user_id');
		$message = $this->input->post('message');

		$row = $this->profile_model->get_row(array('table'=>'tbl_messages',
												   'where'=>array("(message_id = '$message_id') "=>null,
												   				  "(recipient_id = '$user_id' OR sender_id = '$user_id') "=>null),
													'order_by'=>array('field'=>'message_date_sent','order'=>'DESC'),
													'limit'=>1
													)
											);

		if($row){

			$recipient_id = ($user_id==$row->recipient_id) ? $row->sender_id : $row->recipient_id;
			
			$this->profile_model->insert('tbl_messages',array('sender_id'=>$user_id,
																'recipient_id'=>$recipient_id,
																'message_reply_to_id'=>$message_id,
																'message'=>$message
															)
										);

			//$this->profile_model->update('tbl_messages',array('message_status'=>1),array('recipient_id'=>$user_id));
			$response = array('msg'=>'Successfully sent.');
		}else{
			$response = array('msg'=>'Error, Failed to send.');
		}

		$this->output->set_output(json_encode($response));

	}



}