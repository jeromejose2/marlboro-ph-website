<?php

class Move_Forward_Choose extends NW_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Movefwd_Model');
	}

	public function post()
	{
		$result = array(
			'success' => false,
			'choice' => 0
		);
		$user = (int) $this->session->userdata('user_id');
		$action = (int) $this->input->post('action');
		$offer = (int) $this->input->post('offer');
		if (($action === MOVE_FWD_PLAY || $action === MOVE_FWD_PLEDGE) && $offer) {
			$choice = $this->Movefwd_Model->get_chosen_action($user, $offer);
			if (!$choice) {
				if ($this->Movefwd_Model->choose_action($user, $offer, $action)) {
					$result['success'] = true;
					$result['choice'] = $action;
				}
			} else {
				$result['choice'] = $choice;
			}
		}
		$this->output->set_output(json_encode($result));
	}
}