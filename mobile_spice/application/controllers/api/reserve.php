<?php

class Reserve extends NW_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Perks_Model');
	}
	
	public function post()
	{
		$result = array(
			'success' => false,
			'error' => ''
		);

		$data = $this->input->post();
		if (!$data) {
			show_404();
		}

		$data['registrant_id'] = $this->session->userdata('user_id');
		if ($data['mobile_num'] == $this->session->userdata('user')['mobile_num']) {
			$data['mobile_num'] = null;
		}

		$reserveData = array(
			'registrant_id' => $data['registrant_id'], 
			'lamp_id' => $data['id'],
			'lamp_reserve_mobile' => $data['mobile_num'],
			'lamp_reserve_guests' => array(),
			'schedule_id' => $data['booking_date'],
			'lamp_reserve_date_created' => date('Y-m-d H:i:s')
		);

		foreach ($data['guest_name'] as $k => $g) {
			if (!empty($data['guest_email'][$k]) && !filter_var($data['guest_email'][$k], FILTER_VALIDATE_EMAIL)) {
				$result['error'] = 'Invalid guest email address.';
				break;
			} elseif (!$g) {
				continue;
			}
			$reserveData['lamp_reserve_guests'][] = array(
				'name' => $g,
				'email' => isset($data['guest_email'][$k]) ? $data['guest_email'][$k] : null
			);
		}

		if (!$result['error']) {

			$this->numOfGuests = count($reserveData['lamp_reserve_guests']) + 1;

			$this->load->library('form_validation');
			$this->form_validation->set_rules('mobile_num', 'Mobile number', 'required|numeric|min_length[11]|max_length[11]');
			$this->form_validation->set_rules('booking_date', 'Booking date', 'required|numeric|callback_check_sched');
			$this->form_validation->set_rules('id', 'LAMP', 'required|numeric|callback_check_lamp');

			if ($this->form_validation->run()) {

				$reserveData['lamp_reserve_guests'] = json_encode($reserveData['lamp_reserve_guests']);
				$pts = $this->db->select()
					->from('tbl_schedules')
					->where('schedule_id', $reserveData['schedule_id'])
					->get()
					->row()
					->reservation_cost;
				$bidStatus = $this->Perks_Model->check_bid_points($data['registrant_id']);
				if (!empty($bidStatus['items']) && isset($bidStatus['available_points']) && $bidStatus['available_points'] < $pts) {
					$result['error'] = "You only have {$bidStatus['available_points']} points available for you to use. You placed a bid for ".implode(',', $bidStatus['items']).", and {$bidStatus['reserved_points']} points are blocked off while you are still the highest bidder.";
				} elseif (!$this->Perks_Model->reserve($reserveData, $pts)) {
					$result['error'] = 'Failed to reserve.';
				} else {
					$result['success'] = true;
				}

			} else {
				$result['error'] = validation_errors();
			}

		}
		
		$this->output->set_output(json_encode($result));
	}

	public function check_sched($val)
	{
		$schedules = $this->Perks_Model->get_lamp_schedules((int) $this->input->post('id'));
		foreach ($schedules as $sched) {
			if ($sched['schedule_id'] == $val) {
				if ($sched['original_slots'] && !$sched['slots']) {
					$this->form_validation->set_message('check_sched', 'No more slots for selected booking date.');
					return false;
				} elseif ($sched['original_slots'] && ($sched['slots'] < $this->numOfGuests)) {
					$this->form_validation->set_message('check_sched', 'Not enough slots.');
					return false;
				}
				return true;
			}
		}
		$this->form_validation->set_message('check_sched', 'Booking date is invalid.');
		return false;
	}

	public function check_lamp($val)
	{
		if (!$this->Perks_Model->get_lamp($val)) {
			$this->form_validation->set_message('check_lamp', 'LAMP is invalid.');
			return false;
		}
		return true;
	}
}