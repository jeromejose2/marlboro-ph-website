<?php

class Points extends NW_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Points_Model');
	}

	public function post()
	{
		$origin = (int) $this->input->post('origin');
		$params = array(
			'suborigin' => (int) $this->input->post('suborigin')
		);

		$result = array(
			'success' => true
		);
		if (!$this->Points_Model->earn($origin, $params)) {
			$result['success'] == false;
		}

		$this->output->set_output(json_encode($result));
	}
}