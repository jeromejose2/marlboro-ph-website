<?php if(!defined("BASEPATH")){ exit("No direct script access allowed!"); }

class Flash_offer extends CI_Controller {

	var $prize_id = null;
	var $prize_stock = null;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('profile_model');
	}


	public function get_flash_offer()
	{
		$this->load->helper('file');
		$prize_id = $this->input->get('prize_id');
		$user = $this->session->userdata('user');

		$data['row'] = $this->profile_model->get_row(array('table'=>'tbl_flash_offers','where'=>array('prize_id' =>$prize_id)) );
  	    $this->load->view('flash_offer/flash-offer',$data);
	}

	public function confirm_offer()
	{

		$this->load->library('form_validation');
		$this->form_validation->set_rules('prize','callback_check_flash_offer');
 
		if($this->form_validation->run()){

			$flash_offer = $this->profile_model->get_row(array('table'=>'tbl_flash_offers',
															   'where'=>array('CURDATE() >= DATE(start_date)'=>null,'CURDATE() <= DATE(end_date) '=>null,'stock >'=>0,'is_deleted'=>0)
 															)
														);
			$new_stock = (int)$flash_offer->stock - 1;
			$user_id = $this->session->userdata('user_id');

			$id = $this->profile_model->insert('tbl_user_flash_prizes',array('registrant_id'=>$this->session->userdata('user_id'),'prize_id'=>$flash_offer->prize_id));
			$this->profile_model->update('tbl_flash_offers',array('stock'=>$new_stock),array('prize_id'=>$flash_offer->prize_id));


			$this->load->model('notification_model');
			if($flash_offer->send_type=='Delivery'){
				$message = 'Flash offer gift item from Marlboro will be delivered to your mailing address within 3-5 days.';
			}else{
				$message = 'Please follow the instruction below to redeem your flash offer gift item from Marlboro: <br/>'.$flash_offer->redemption_instruction;
			}

			$param = array('message'=>$message,'suborigin'=>$id);
			$this->notification_model->notify($user_id,FLASH_OFFERS,$param);


			$data = array('msg'=>'<h3>You have successfully confirmed the flash offer!</h3>',
							  'error_holder'=>'',
							  'error'=>false,
 							  'btn'=>'',
							  'btn_text'=>'');

		}else{

			$data = array('msg'=>trim(validation_errors()),
							  'error_holder'=>'flash-offer-error',
							  'error'=>true,
 							  'btn'=>'flash-offer-btn',
							  'btn_text'=>'<i>CONFIRM</i>');
		}

		$this->load->view('profile/submit-response',$data);
		

	}

	private function check_flash_offer()
	{ 

		$flash_offer = $this->profile_model->get_row(array('table'=>'tbl_flash_offers',
															   'where'=>array('status'=>0,'CURDATE() >= DATE(start_date)'=>null,'CURDATE() <= DATE(end_date) '=>null,'stock >'=>0),
															   'fields'=>'prize_id,stock'
															)
														);
	 
			if($flash_offer && (int)$this->session->userdata('user_id')){

				$user_flash_prize = $this->profile_model->get_row(array('table'=>'tbl_user_flash_prizes',
														   			'where'=>array('prize_id' =>$flash_offer->prize_id,'registrant_id' =>$this->session->userdata('user_id')),
														   			'fields'=>'user_flash_prize_id'
																	)
															);
				if($user_flash_prize){
					$this->form_validation->set_message('check_flash_offer','You have already confirmed the birthday offer to your account.');
					return false;
				}else{
					return true;
				}


			}else{

				$this->form_validation->set_message('check_flash_offer','No flash offer at this period of time.');
				return false;

			} 	

		 

	}




}