<?php if(!defined("BASEPATH")){ exit("No direct script access allowed!"); }

class Profile extends CI_Controller {

	
	public function __construct()
	{
		parent:: __construct();
 
		$this->load->model('profile_model');
	}

	public function index()
	{
		
 		$user_id = $this->session->userdata('user_id');
     

		$row = $this->profile_model->get_rows(array('fields'=>'a.*, c.city as city_name, b.province as province_name',
													'table'=>'tbl_registrants as a',
													'where'=>array('a.registrant_id'=>$user_id),
													'join'=>array(array('table'=>'tbl_cities as c','on'=>'c.city_id=a.city','type'=>'left'),
																  array('table'=>'tbl_provinces as b','on'=>'b.province_id=a.province', 'type'=>'left')
															),
													)
											)->row(); 
		$data['user_id'] = $user_id;
 		$data['row'] = $row;
 		$data['cities'] =  $this->profile_model->get_rows(array('table'=>'tbl_cities','province_id'=>$row->province));		
		$data['provinces'] = $this->profile_model->get_rows(array('table'=>'tbl_provinces'));
 		$data['brands'] = $this->profile_model->get_rows(array('table'=>'tbl_brands'));
 		$data['alternate_purchase'] = $this->profile_model->get_rows(array('table'=>'tbl_alternate_purchase'));
 		$data['statements'] = $this->profile_model->get_rows(array('table'=>'tbl_statements','where'=>array('is_deleted'=>0,'status'=>1)));
		$user_statements = $this->profile_model->get_rows(array('table'=>'tbl_user_statements',
		 																'where'=>array('registrant_id'=>$user_id),
		 																'order_by'=>array('field'=>'date_created','order'=>'DESC'),
		 																'limit'=>1
		 																)
																)->row();
 		$user_statement_ids = array(0);

 		if($user_statements){

  			$user_statement_ids = explode(',',$user_statements->statement_id);
 		}

 		$data['user_statements'] = $this->profile_model->get_rows(array('fields'=>'statement_id,statement',
  																	'table'=>'tbl_statements',
  																	'where'=>array('where_in'=>array('field'=>'statement_id','arr'=>$user_statement_ids)
  																				  )
  																	)
  															);

 		$user_statements = $this->profile_model->get_rows(array('table'=>'tbl_user_statements',
		 																'where'=>array('registrant_id'=>$user_id),
		 																'order_by'=>array('field'=>'date_created','order'=>'DESC'),
		 																'limit'=>1
		 																)
																)->row();
 		$user_statement_ids = array(0);

 		if($user_statements){

  			$user_statement_ids = explode(',',$user_statements->statement_id);
 		}

 		$data['user_statements'] = $this->profile_model->get_rows(array('fields'=>'statement_id,statement',
  																	'table'=>'tbl_statements',
  																	'where'=>array('where_in'=>array('field'=>'statement_id','arr'=>$user_statement_ids)
  																				  )
  																	)
  															);
 		$this->load->layout('profile/profile',$data);
 		//$this->output->enable_profiler(TRUE);
		
	}

	public function print_array($data)
	{

		echo "<pre>";
		print_r($data);
		echo "</pre>";

	}

	public function update_profile()
	{

		if($this->input->post()){

			$post_data = $this->input->post();

			if($this->validate_profile()){
				
				if(isset($post_data['password']) && $post_data['password']){
					
					$user = $this->db->select()
						->from('tbl_registrants')
						->where('registrant_id', $this->session->userdata('user_id'))
						->get()
						->row();
					if ($user) {
						$this->load->library('Encrypt');
						$post_data['password'] = $this->encrypt->encode($post_data['password'], $user->salt);
						$post_data['encryption_changed'] = 1;
					}
					
				}else{
					unset($post_data['password']);
				}
				$post_data['current_brand_code'] = $this->db->select()
					->from('tbl_brands')
					->where('brand_id', $post_data['current_brand'])
					->get()
					->row()
					->code;
				$post_data['first_alternate_brand_code'] = $this->db->select()
					->from('tbl_brands')
					->where('brand_id', $post_data['first_alternate_brand'])
					->get()
					->row()
					->code;
				$this->profile_model->update('tbl_registrants',$post_data,array('registrant_id'=>$this->session->userdata('user_id')));
				$data = array('msg'=>'Your profile details has been successfully updated.',
							  'error_holder'=>'profile-error',
							  'error'=>false,
							  'btn'=>'',
							  'btn_text'=>'');
  
			}else{
				$data = array('msg'=>trim(validation_errors()), 'btn' => 'update-profile', 'btn_text' => '<i>SAVE</i>', 'error_holder'=>'profile-error','error'=>true);
			}

		}

		$this->load->view('profile/submit-response',$data);
 
	}

	public function update_statements()
	{

		if($this->input->post()){


			if(count($this->input->post('user_statements')) >= 1 && count($this->input->post('user_statements')) <= 3){

				$this->load->model('Points_Model');
				$data = array('statement_id'=>implode(',',$this->input->post('user_statements')).',',
							  'registrant_id'=>$this->session->userdata('user_id'),
							  'date_modified'=>array('field'=>'date_created','value'=>'NOW()')
							  );

 				$user_statement_id = $this->profile_model->insert('tbl_user_statements',$data);
 				$this->Points_Model->earn(USER_STATEMENT, array('suborigin' => $user_statement_id));
 				$data = array('msg'=>'Your Maybe statement/s has been successfully updated.',
 								'error_holder'=>'',
 								'error'=>false,
 								'user_statement_id'=>$user_statement_id
 								);

			}else{
				$data = array('msg'=>'You must select at least 1 or up to 3 maybe statements.',
							  'error_holder'=>'maybe-statements-error',
							  'error'=>true,
							  'user_statement_id'=>'',
							  'btn'=>'maybe-btn',
							  'btn_text'=>'<i>SUBMIT</i>');
			}

			$this->load->view('profile/submit-response',$data);

		}
	}

	public function get_cities()
	{

		$province_id = $this->input->get('province');
 		$rows = $this->profile_model->get_rows(array('table'=>'tbl_cities',
 													'where'=>array('province_id'=>$province_id),
 													'order_by'=>array('field'=>'city','order'=>'ASC')
 													)
 												);
		$data['data'] = $rows->result_array();
		$this->load->view('profile/json_format',$data);

	}

	public function validate_profile()
	{

		$this->load->library('form_validation');

		$rules = array(
		   array(
				 'field'   => 'nick_name',
				 'label'   => 'Nick name',
				 'rules'   => 'trim|required|alpha_space'
			  ),
		   array(
				 'field'   => 'mobile_phone',
				 'label'   => 'Mobile number',
				 'rules'   => 'trim|required|numeric|min_length[11]|max_length[11]|callback_check_mobile_prefix'
			  ),
		   array(
				 'field'   => 'street_name',
				 'label'   => 'Street Name',
				 'rules'   => 'trim|required'
			  ),
		   array(
				 'field'   => 'barangay',
				 'label'   => 'Barangay',
				 'rules'   => 'trim|required'
			  ),
		   array(
				 'field'   => 'province',
				 'label'   => 'Province',
				 'rules'   => 'trim|required'
			  ),
		   array(
				 'field'   => 'city',
				 'label'   => 'City',
				 'rules'   => 'trim|required'
			  ),
		   array(
				 'field'   => 'zip_code',
				 'label'   => 'Zip Code',
				 'rules'   => 'trim|required'
			  ),
		   array(
				 'field'   => 'current_brand',
				 'label'   => 'Current brand',
				 'rules'   => 'trim|required'
			  ),
		   array(
				 'field'   => 'first_alternate_brand',
				 'label'   => 'First alternate brand',
				 'rules'   => 'trim|required'
			  ) 
		);



		if($this->input->post('old_password') || $this->input->post('password') || $this->input->post('cpassword')){
			$rules[] = array('field'=>'old_password','label'=>'Current Password','rules'=>'callback_password_check');
			$rules[] = array('field'=>'password','label'=>'New password','rules'=>'required|matches[cpassword]|min_length[8]|callback_password_strength');
			$rules[] = array('field'=>'cpassword','label'=>'Confirm New Password','rules'=>'required');
		}

		//$this->form_validation->set_error_delimiters('','');
		$this->form_validation->set_rules($rules);
		
		return $this->form_validation->run();

	}

	public function check_mobile_prefix($mobile_prefix)
	{
		$row = $this->profile_model->get_row(array('table'=>'tbl_mobile_prefix','where'=>array('mobile_prefix'=>substr($mobile_prefix,0,4))));
		if(!$row){
			$this->form_validation->set_message('check_mobile_prefix','Invalid mobile prefix.');
			return false;
		}else{
			return true;
		}
	}

	public function password_check($str)
	{

		$user = $this->db->select()
			->from('tbl_registrants')
			->where('registrant_id', $this->session->userdata('user_id'))
			->get()
			->row();

		if ($user) {
			
	 		$this->load->library('Encrypt');
	 		$exists = false;
	 		if ($user->from_migration && !$user->encryption_changed) {
	 			$exists = $this->encrypt->password($str, $user->salt) === $user->password;
	 		} else {
	 			$exists = $this->encrypt->decode($user->password, $user->salt) === $str;
	 		}

	 		if ($exists){

	 			if($str===$this->input->post('password')){
	 				$this->form_validation->set_message('password_check','New password must differ from old password.');
	 				return false;
	 			}else{
	 				return true;
	 			}
				
			}else{
				$this->form_validation->set_message('password_check','Invalid current password.');
				return false;
			}


		}else{

			$this->form_validation->set_message('password_check','Invalid user.');
			return false;

		}


	}

	public function password_strength($password)
	{

 
 		$count = 0;
  		$patterns =  array('lowercase'=>'lowercase','uppercase'=>'uppercase','digit'=>'digit','special character'=>'special character');
 		$user = $this->session->userdata('user');		
		

		if(preg_match('/[a-z]/',$password)){
			$count++;
			unset($patterns['lowercase']);
 		}

		if(preg_match('/[A-Z]/',$password)){
			$count++;
			unset($patterns['uppercase']);
 		}
		

		if(preg_match('/\d/',$password)){
			$count++;
			unset($patterns['digit']);
 		}

		if(preg_match('/\W/',$password)){
			$count++;
			unset($patterns['special character']);
 		}
 		  

		if($count < 3){

			$last = array_pop($patterns); 
			$validation_message = 'Your password must contain from the following: '.implode(',',$patterns).' or '.$last.'</br>';
 			$this->form_validation->set_message('password_strength',$validation_message);
			return false;
		}
		  

		if(preg_match('/user|guest|admin|sys|test|pass|super|'.$user['first_name'].'|'.$user['last_name'].'/i',$password,$matches)){
 			$validation_message = 'Your new password must not contained from the following: user,guest,admin,sys,test,pass,super,'.strtolower($user['first_name']).' or '.strtolower($user['last_name']);
 			$this->form_validation->set_message('password_strength',$validation_message);
 			return false;
   		}

   		return true;
   		 
	}

	public function change_photo()
	{

		$this->load->helper(array('upload','resize','file'));

		$id = $this->session->userdata('user_id');

		$filename = md5($id.uniqid());
		$path = 'uploads/profile/'.$id;		
		$file_ext = array('png','jpg','gif');
		$length = count($file_ext);
		$count = 0;
		$temp = true;

		while($temp){

 			foreach($file_ext as $v){

				if(!file_exists($path.'/'.$filename.'.'.$v)){
					$count++;
				}
				
			}

			if($count==$length){				
				$temp = false;
			}else{
				$filename = md5($id.uniqid());
				$count = 0;
			}

		}


		if(!is_dir($path)){
			mkdir($path,0777,true);
		}


		$max_size = 3072;		 

		$params = array('upload_path'=>$path,'allowed_types'=>FILE_UPLOAD_ALLOWED_TYPES,'file_name'=>$filename,'max_size'=>$max_size,'max_width'=>0,'max_height'=>0,'do_upload'=>'user_photo');
	    $file = upload($params);

	    if(is_array($file)){

	    	$row = $this->profile_model->get_row(array('table'=>'tbl_registrants','where'=>array('registrant_id'=>$id)));
	    	if(file_exists($path.'/'.$row->new_picture) && $row->new_picture){
	    		unlink($path.'/'.$row->new_picture);
	    	}

	    	if(file_exists($path.'/46_46_'.$row->new_picture) && $row->new_picture){
	    		unlink($path.'/'.$row->new_picture);
	    	}
 
	    	$params = array('width'=>46,'height'=>46,'source_image'=>$path.'/'.$file['file_name'],'new_image_path'=>$path.'/','file_name'=>$file['file_name']);
  	    	resize($params);

	    	$this->profile_model->update('tbl_registrants',
	    								array('new_picture'=>$file['file_name'],'picture_status'=>0,'date_modified'=>1),
	    								array('registrant_id'=>$id)
	    								);
	    	$error = false;
	    	$msg = 'Your photo submission was successfully sent and is now subject to moderation. Please allow 24-48 hours for approval.'; 
	    }else{
	    	$msg = $this->upload->display_errors();
	    	$error = true;
	    }	    	

	    $data = array('msg'=>$msg,
	    			  'error_holder'=>'user_photo_error',
	    			  'error'=>$error,
	    			  'btn'=>'change-photo-btn',
					  'btn_text'=>'<i>SAVE</i>');
	    $this->load->view('profile/submit-response',$data);

	}

	public function change_photo_form()
	{

		$this->load->view('profile/upload-profile');

	}

	public function change_photo_webcam()
	{	
		$this->load->library('encrypt');
		$user_id = $this->session->userdata('user_id');
		$data['token'] = $this->encrypt->encode($user_id);		
		$this->load->view('profile/upload-webcam-profile',$data);

	}
 

	public function select_statements()
	{
		$user_id = $this->session->userdata('user_id');
		$data['statements'] = $this->profile_model->get_rows(array('table'=>'tbl_statements','where'=>array('is_deleted'=>0,'status'=>1)));
		$user_statements = $this->profile_model->get_rows(array('table'=>'tbl_user_statements',
		 																'where'=>array('registrant_id'=>$user_id),
		 																'order_by'=>array('field'=>'date_created','order'=>'DESC'),
		 																'limit'=>1
		 																)
																)->row();
 		$user_statement_ids = array(0);

 		if($user_statements){

  			$user_statement_ids = explode(',',$user_statements->statement_id);
 		}


 		$data['user_statements'] = $user_statement_ids; /* $this->profile_model->get_rows(array('fields'=>'statement_id,statement',
  																	'table'=>'tbl_statements',
  																	'where'=>array('where_in'=>array('field'=>'statement_id','arr'=>$user_statement_ids)
  																				  )
  																	)
  															); */

		$this->load->view('profile/select-statements',$data);

	}

	public function swf()
	{
		if($this->input->post()){


			if($this->input->post('imageString')){

				$imageString = $this->input->post('imageString');
				$filename = sha1(uniqid()).'.jpg';
				$user_id = $this->session->userdata('user_id');
				$path = 'uploads/profile/'.$user_id.'/';
				file_put_contents($path.$filename, gzuncompress(base64_decode($imageString)));

				$data = array('msg'=>'Your photo submission was successfully sent and is now subject to moderation. Please allow 24-48 hours for approval.',
	    			  'error_holder'=>'',
	    			  'error'=>false,
	    			  'btn'=>'change-photo-btn',
					  'btn_text'=>'<i>Submit</i>');

				$this->profile_model->update('tbl_registrants',
 	   									array('new_picture'=>$filename,'picture_status'=>0,'date_modified'=>1),
 	   									array('registrant_id'=>$user_id)
 	   									);


			}else{

				$data = array('msg'=>'Please capture photo via webcam.',
	    			  'error_holder'=>'error-via-webcam',
	    			  'error'=>true,
	    			  'btn'=>'take-photo-btn',
					  'btn_text'=>'<i>Submit</i>');

			}

			
	   		$this->load->view('profile/submit-response',$data);

		}else{
			$this->load->view('profile/swf_xml');
		}
		
	}





}