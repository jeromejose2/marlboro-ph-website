<?php

class Hook
{
	public function post_controller_constructor()
	{
		$ci = & get_instance();

		if (PROD_MODE && (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on')) {
			$url = 'https://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
			redirect($url);
		}
		
		$route_class = strtolower($ci->router->class);
		if (!in_array($route_class, array('api', 'noscript')) && $ci->uri->segment(1) != 'gateway') {

			if ($route_class == 'edm' && !$ci->uri->segment(2)) {
				$url = $ci->input->get('url');
				if (!$url) {
					$url = '/';
				}
				$ci->session->set_userdata('redirect_url', $url);
			}

			$user_session = $ci->session->userdata('user');

			if (!$user_session && $route_class != 'user') {

				redirect('user/login');

			} elseif ($user_session && $route_class == 'user') {

				if ($redirect_url = $ci->session->flashdata('redirect_url')) {
					redirect($redirect_url);
				}
				redirect('/');

			} elseif ($user_session && $ci->session->userdata('redirect_url')) {

				redirect($ci->session->flashdata('redirect_url'));

			}

			$ci->unread_notifications = $ci->Notification_Model->get_unread_count_by_user($ci->session->userdata('user_id'));
		}
	}
}