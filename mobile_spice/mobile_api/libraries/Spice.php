<?php

class Spice{
    
    const APP_ID = 1011;
    const API_URL = 'https://services.mrm-pmi.com/PMI.MRM.Services.RESTService.svc/';
    const MARKET_CODE = 'PH';
    const USER_KEY = '_spice_user';
    const SESSION_KEY = '_spice_session_key';

    const MEMBER_PERSON_ID = '_spice_member_person_id';
    const MEMBER_SESSION_KEY = '_spice_member_session_key';

    const ADMIN_PERSON_ID = '_spice_admin_person_id';
    const ADMIN_SESSION_KEY = '_spice_admin_session_key';

    private $CI;

    var $_spice_admin_session_key = '';
    var $_spice_input = '';
    var $_spice_member_person_id = '';
    var $_spice_member_session_key = '';

    public function __construct() {

        $this->CI = &get_instance();
        $this->_spice_session_table = 'tbl_spice_mobile_sessions';

    }

    public function loginAdmin($username, $password) {

        global $_spice_admin_session_key;

        $has_session = $this->CI->db->where('admin_session_key', $_spice_admin_session_key)->get($this->_spice_session_table)->num_rows();

        if( ! $this->checkSession($has_session)) {

            $response = $this->createSession($username, $password);
            $response = $this->parseJSON($response);

            // print_r($response);

            if($response->ResponseHeader->TransactionStatus) {

                return FALSE;

            }

            $_spice_admin_session_key = $response->SessionKey;

        }
 
    }

    public function loginMember($username, $password){        
       
       //if(!$this->checkSession($this->CI->session->userdata(self::MEMBER_SESSION_KEY))){

            global $_spice_member_person_id;

            $response = $this->createSession($username,$password);
            $response = $this->parseJSON($response);

            if ($response->ResponseHeader->TransactionStatus) {
                return $response;
            }

            $_spice_member_person_id = $response->PersonId;
            $_spice_member_session_key = $response->SessionKey;

            // $this->CI->session->set_userdata(self::MEMBER_SESSION_KEY,$response->SessionKey);
            // $this->CI->session->set_userdata(self::MEMBER_PERSON_ID,$response->PersonId);
            
            return $response;


       // }        
  
    }

    public function endSession($sessionKey) {

        $data = array(
            'ApplicationId' => self::APP_ID,
            'DeviceId' => '',
            'LanguageCode' => '',
            'MarketCode' => self::MARKET_CODE,
            'SessionKey' => $sessionKey,
            'SubMarketCode' => ''
        );

        if($this->CI->db->where('session_key', $sessionKey)->get($this->_spice_session_table)->num_rows()) {

            $this->CI->db->where('session_key', $sessionKey)->delete($this->_spice_session_table);

        }

        return $this->api('EndSession', $data);

    }

    public function extractSpiceDate($date_time){

        $match = preg_match('/\/Date\((\d+)([-+])(\d+)\)\//', $date_time, $date);

        $timestamp = $date[1]/1000;
        $operator = $date[2];
        $hours = $date[3]*36; // Get the seconds

        $datetime = new DateTime();

        $datetime->setTimestamp($timestamp);
        $datetime->modify($operator . $hours . ' seconds');
        return $datetime->format('Y-m-d H:i:s');


    }  

    public function createSession($loginName, $password) {

        // Requried Inputs: MessageRequestHeader, LoginName, EncryptedPassword, ApplicationId, MarketCode

        global $_spice_admin_session_key;
        global $_spice_member_person_id;
        global $_spice_input;

        $data = array(
                        'EncryptedPassword'     => $this->encryptPassword($loginName, $password),
                        'LoginName'             => $loginName,
                        'MessageRequestHeader'  => $this->createHeader()
                    );

        $session = $this->api('CreateSession', $data);
        $readSession = $this->parseJSON($session);

        $_spice_member_person_id = $readSession->PersonId;

        if( ! $this->CI->db->where('session_key', $readSession->SessionKey)->get($this->_spice_session_table)->num_rows()) {

            if($readSession->PersonId != '404183') {
                
                if($readSession->SessionKey) {

                    $session_data = array(
                        'spice_input'       => $_spice_input,
                        'session_key'       => $readSession->SessionKey,
                        'person_id'         => $readSession->PersonId,
                        'admin_session_key' => $_spice_admin_session_key
                    );
                    $this->CI->db->insert($this->_spice_session_table, $session_data);

                }                

            }

        } else {

            if($readSession->PersonId != '404182') {

                $session_data = array(
                    'admin_session_key' => $_spice_admin_session_key
                );
                $this->CI->db->where('session_key', $readSession->SessionKey)->update($this->_spice_session_table, $session_data);

            }

        }

        return $session;

    }

    public function createHeader($sessionKey = '') {

        return array(
                    'ApplicationId' => self::APP_ID,
                    'DeviceId' => '',
                    'LanguageCode' => '',
                    'MarketCode' => self::MARKET_CODE,
                    'SessionKey' => $sessionKey
                    );

    }

    public function getMemberPersonId() {

        global $_spice_member_person_id;

        return $_spice_member_person_id;

    }

    public function trackAction($param, $memberPersonId = '') {
        
        global $_spice_admin_session_key;

        $data = array_merge(array('PersonId' => $memberPersonId,'MessageRequestHeader' => $this->createHeader($_spice_admin_session_key)), $param);

        
        return $this->api('TrackAction', $data); 

    }

    public function trackActionAdmin($param) {

        global $_spice_admin_session_key;

        $this->loginAdmin('MobilePHSysUser', 'MobilePHDefault#14');

        $data = array_merge(array('MessageRequestHeader' => $this->createHeader($_spice_admin_session_key)), $param);
        return $this->api('TrackAction', $data);

    }

    public function getBulk($table) {

        global $_spice_admin_session_key;

        $data = array(
                        'MessageRequestHeader' => $this->createHeader($_spice_admin_session_key),
                        'ReturnType'=>'xml',
                         'Table' => $table
                    );

         return $this->api('GetBulk', $data);

    }

    public function encryptPassword($loginName, $password) {

        if (strlen($loginName) < 30) {
            $loginName = str_pad($loginName, 30, ' ', STR_PAD_RIGHT);
        }
        $loginName = substr($loginName, 0, 30);
        return base64_encode(sha1($password.$loginName, true).$loginName);           

    }

    private function api($method, array $data) {

        global $_spice_input;

        $curl = curl_init(self::API_URL.trim($method, '/').'/');
        $data = json_encode($data);

        $_spice_input = $data;

        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_CAINFO, __DIR__.'/spice/AddTrustExternalCARoot.crt');
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-type: application/json',
            'Accept: application/json'
        ]);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        $response = curl_exec($curl);
        
        if (!$response) {
            return null;
        }

        if (curl_errno($curl)) {
            throw new Exception('CURL Error: '.curl_error($curl));
        }
        curl_close($curl);

        return $response;

    }

    public function updatePerson($params){
        
        $this->loginAdmin('MobilePHSysUser', 'MobilePHDefault#14');

        if(!isset($params['person_id'])){
            return false;
        }

        $response = $this->GetPerson(array('PersonId'=>$params['person_id']));
        $response_json = $this->parseJSON($response);

        if($response_json->MessageResponseHeader->TransactionStatus){
            return false;
        }

        if($response_json->ConsumerProfiles->BrandPreferences){

            $BrandPreferences = json_decode(json_encode($response_json->ConsumerProfiles->BrandPreferences[0]), true);

        }else{

            $BrandPreferences = array(/* 'MultibrandSmoker' =>'1',
                                        'OccasionalSmoker' => '',
                                        'PrimaryBrand' => array(
                                                                'BrandAffinity' => '0',
                                                                'BrandId' => '5313'
                                                            ),

                                        'SecondaryBrand' => array(
                                                                    'BrandAffinity' => '0',
                                                                    'BrandId' => '5313'
                                                                )
                                    */);

        }

        $inputs['ProfileType'] = 'C';
        $updates = array( 'PersonalDetails'=>$response_json->ConsumerProfiles->PersonDetails,
                            'AddressType'=>$response_json->ConsumerProfiles->Addresses,
                            'Derivations'=>$response_json->ConsumerProfiles->Derivations,
                            'BrandPreferences'=>$BrandPreferences,
                            'IdDocumentNumber'=>$response_json->ConsumerProfiles->IdDocumentNumber,
                            'IdDocumentTypeCode'=>$response_json->ConsumerProfiles->IdDocumentTypeCode,
                            'PersonId'=>$response_json->ConsumerProfiles->PersonId                                                                                                       
                        );

        $inputs['ConsumerProfile'] = isset($params['updates']) ? array_merge($updates,$params['updates']) : $updates;
        $inputs = json_decode(json_encode($inputs), true);
        $response = $this->ManagePerson($inputs);
        
        return $response;

    }

    public function ManagePerson($data) {

        global $_spice_admin_session_key;

        $person_id = $data['ConsumerProfile']['PersonId'];

        $session_key = (array) $this->CI->db->select()
                                ->from($this->_spice_session_table)
                                ->where('person_id', $person_id)
                                ->get()
                                ->row();

        $spice_admin_session_key = $session_key ? $session_key['admin_session_key'] : '';

        $data = array_merge(array('MessageRequestHeader' => $this->createHeader($spice_admin_session_key)), $data);

        $data = $this->api('ManagePerson', $data);
        return $data;

    }

    public function SearchPerson($filter){

        global $_spice_admin_session_key;

        $data = array_merge(array('MessageRequestHeader'=>$this->createHeader($_spice_admin_session_key),'ProfileType'=>'C'),
                            $filter);
                            
        $data = $this->api('SearchPerson',$data);
        return $data;

    }

    public function GetPerson($filter) {

        global $_spice_admin_session_key;

        $data = array_merge(array('MessageRequestHeader'=>$this->createHeader($_spice_admin_session_key),'ProfileType'=>'C'),
                            $filter);


        $data = $this->api('GetPerson',$data);
        return $data;

    }

    public function checkSession($sessionKey) {

        $data = [
            'MessageRequestHeader' => $this->createHeader($sessionKey),
            'Source' => ''
        ];

        $response = $this->api('CheckSession', $data);
        $response_json = $this->parseJSON($response);

        return $response && (int)$response_json->ResponseHeader->TransactionStatus === 0 ? true : false;

    }

    public function parseJSON($data) {
        
        $response = json_decode($data);
        return $response;

    }

    public function toArray($data){
        
        $response = json_decode($data,true);
        return $response;
    }

    public function toJSONFormat($data){
        
        $response = json_encode($data);
        return $response;

    }

    public function printArray($data) {

        echo "<br/><br/><pre>";
        print_r($data);
        echo "</pre><br/><br/>";

    }

}