<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movefwd_Model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		define('BASE_URL', base_url());
		require_once('application/models/perks_model.php');
		require_once('application/models/notification_model.php');
		require_once('application/models/points_model.php');
	}

	public function save_pledge($user_id, $id, &$message = '') {
		//////////////////
		$this->perks_model = new Perks_Model();
		$this->notification_model = new Notification_Model();
		$this->points_model = new Points_Model();

		$user = $this->db->select('*')
						 ->from('tbl_registrants')
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row_array();
		if(!$user) {
			$message = 'Invalid uid';
			return false;
		}

		$item = $this->db->select('*')
						 ->from('tbl_move_forward')
						 ->where('move_forward_id', $id)
						 ->get()
						 ->row_array();
		if(!$item) {
			$message = 'Invalid id';
			return false;
		}
		$bid_status = $this->perks_model->check_bid_points($user_id);

		// if (!empty($bid_status['items']) && isset($bid_status['available_points']) && $bid_status['available_points'] < $item['pledge_points'])
		// 	$message = "You only have {$bid_status['available_points']} points available for you to use. You placed a bid for ".implode(',', $bid_status['items']).", and {$bid_status['reserved_points']} points are blocked off while you are still the highest bidder.";
		// 	return false;	
		// }
		if(!empty($bid_status['items']) && isset($bid_status['available_points']) && $bid_status['available_points'] < $item['pledge_points']) {
			$message = 'Test';
			return false;
		}

		$params = array(
			'registrant' => $user_id,
			'suborigin' => $id,
			'remarks' => '{{ name }} pledged for MoveFWD',
			'points' => $item['pledge_points']
		);

		/* Spice trackAction */
		$this->load->library('Spice');
		$this->spice->loginAdmin('MobilePHSysUser', 'MobilePHDefault#14');

		/* MOVE_FWD */
		// $cell_id = 1847;
		// $activity_id = 1122;
		$section_name = 'MOVE_FWD';
		$sec_settings = $this->db->select()->from('tbl_section_settings')->where('name', $section_name)->get()->row();
		$cell_id = $sec_settings->setting_section_id;

		$act_settings = $this->db->select()->from('tbl_activity_settings')->where('name', $section_name)->get()->row();
		$activity_id = $act_settings->setting_activity_id;

		$track_action_data = array(
			'CellId' => $cell_id,
			'ActionList' => array(
				array(
					'ActivityId' => $activity_id,
					'ActionValue' => $item['move_forward_title'] . '|pledged|' . $item['pledge_points']
				)
			)
		);

		$person = (array) $this->db->select()
						->from('tbl_registrants')
						->where('registrant_id', $user_id)
						->get()
						->row();

		if($person) {
			$response_data = $this->spice->trackAction($track_action_data, $person['person_id']);
			$response = $this->spice->parseJSON($response_data);
		}
		
		/* Spice trackAction */

		if($response->MessageResponseHeader->TransactionStatus != 0) {
			$message = $response->MessageResponseHeader->TransactionStatusMessage;
			return FALSE;
		}

		if (!($pledged_points = $this->points_model->spend(MOVE_FWD_PLEDGE_ENTRY, $params))) {
			$message = "Insufficient points";
			return false;	
		}

		if($this->has_pledge_done($user_id, $id)) {
			$message = 'User already pledged for this item';
			return false;
		}

		$this->notification_model->notify($user_id, MOVE_FWD_PLEDGE_ENTRY, array(
			'message' => "You have successfully pledged {$pledged_points} points for {$item['move_forward_title']}.",
			'suborigin' => $id
		));

		$param['move_forward_choice'] = MOVE_FWD_PLEDGE;
		$param['registrant_id'] = $user_id;
		$param['move_forward_id'] = $id;
		$param['move_forward_choice_status'] = 1;
		$param['move_forward_choice_started'] = 1;
		$param['move_forward_choice_done'] = date('Y-m-d H:i:s');
		$param['move_forward_choice_date'] = date('Y-m-d H:i:s');
		$param['source'] = 2;
		$this->db->insert('tbl_move_forward_choice', $param);
	}

	private function get_challenges($id) {
		$challenges = $this->db->select('challenge_id, challenge, type')
							   ->from('tbl_challenges')
							   ->where('move_forward_id', $id)
							   ->order_by('challenge_id')
							   ->get()
							   ->result_array();
		return $challenges;
	}

	public function save_play($user_id, $id, &$message = '') {
		$user = $this->db->select('*')
						 ->from('tbl_registrants')
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row_array();
		if(!$user) {
			$message = 'Invalid uid';
			return false;
		}

		$item = $this->db->select('*')
						 ->from('tbl_move_forward')
						 ->where('move_forward_id', $id)
						 ->get()
						 ->row_array();
		if(!$item) {
			$message = 'Invalid id';
			return false;
		}

		$started = $this->db->select('*')
							->from('tbl_move_forward_choice')
							->where('move_forward_id', $id)
							->where('registrant_id', $user_id)
							->count_all_results();
		if($started) {
			$message = 'User already started this activity';
			return false;	
		}

		$param['move_forward_choice'] = MOVE_FWD_PLAY;
		$param['registrant_id'] = $user_id;
		$param['move_forward_id'] = $id;
		$param['move_forward_choice_status'] = 0;
		$param['move_forward_choice_started'] = 1;
		$param['move_forward_choice_date'] = date('Y-m-d H:i:s');
		$this->db->insert('tbl_move_forward_choice', $param);

		$challenges = $this->get_challenges($id);
		return $challenges[0]['challenge_id'];
	}

	public function has_pledge_done($user, $offer)
	{
		$count = $this->db->from('tbl_move_forward_choice')
			//->where('move_forward_choice_status', 1)
			//->where('move_forward_choice_started', 1)
			->where('move_forward_id', $offer)
			->where('registrant_id', $user)
			->count_all_results();
		return $count ? true : false;
	}
}