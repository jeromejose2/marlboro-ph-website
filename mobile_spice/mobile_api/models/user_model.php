<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		define('BASE_URL', base_url());
		require_once('application/models/perks_model.php');
		require_once('application/models/notification_model.php');
		require_once('application/models/points_model.php');
		$this->perks = new Perks_model();
		$this->Points_Model = new Points_Model();
	}

	public function update_details($user_id, $nick, $gender, $mobile, $street, $barangay, $province, $city) {
		//$post['registrant_id'] = $user_id;

		// $post['nick_name'] = $nick;
		// $post['gender'] = $gender;
		// $post['mobile_phone'] = $mobile;
		// $post['street_name'] = $street;
		// $post['barangay'] = $barangay;
		// $post['province'] = $province;
		// $post['city'] = $city;

		// $this->db->where('registrant_id', $user_id);
		// $this->db->update('tbl_registrants', $post);

		$person = $this->db->get_where('tbl_registrants',array('registrant_id'=>$user_id))->row();

		$person_id = $person ? $person->person_id : 0;

		if($person_id) {
			// $person_id = $person_id->person_id;

			$this->load->library('Spice');
			$this->spice->loginAdmin('MobilePHSysUser', 'MobilePHDefault#14');

			$spice_user_info = $this->spice->GetPerson(array('PersonId' => $person_id));
			$spice_user_info = $this->spice->parseJSON($spice_user_info);

			if($spice_user_info->MessageResponseHeader->TransactionStatus == 0) {
				$spice_user_info = $spice_user_info->ConsumerProfiles;

				$city = $this->db->select()
					->from('tbl_cities')
					->where('city_id', $city)
					->limit(1)
					->get()
					->row();

				$province = $this->db->select()
					->from('tbl_provinces')
					->where('province_id', $province)
					->limit(1)
					->get()
					->row();

				$city = $city ? $city->city : '';
				$province = $province ? $province->province : '';
				
				/*if($spice_user_info->Derivations)
					$spice_user_info->Derivations[0]->AttributeValue = $nick;

				$Derivations = json_decode(json_encode($spice_user_info->Derivations),true);*/

				/* Derivations */
                $this->load->model('registration_model');

				$referral_code = $this->registration_model->referral_code();

                    $total_derivations = count($spice_user_info->Derivations);
                        
                    for($i = 0; $i<$total_derivations; $i++){
                            if($spice_user_info->Derivations[$i]->AttributeCode=='ca_nickname'){
                                $spice_user_info->Derivations[$i]->AttributeValue =  $nick;
                            }

                            if($spice_user_info->Derivations[$i]->AttributeCode=='ca_referral_code'){
                                if($spice_user_info->Derivations[$i]->AttributeValue == '0'){
                                    $spice_user_info->Derivations[$i]->AttributeValue = $referral_code ;
                                }
                            }
                    }
                    
                $Derivations = json_decode(json_encode($spice_user_info->Derivations),true);		
				/* Derivations */

				$data = array(
					'person_id' => $person_id,
					'updates' => array(
						'PersonalDetails' => array(
							'FirstName' => $spice_user_info->PersonDetails->FirstName,
							'MiddleName' => $spice_user_info->PersonDetails->MiddleName,
							'LastName' => $spice_user_info->PersonDetails->LastName,
							'Gender' => $gender == 0 ? 'M' : 'F',
							'MobileTelephoneNumber' => ltrim($mobile, '0'),
							'EmailAddress' => $spice_user_info->PersonDetails->EmailAddress,
							'DateOfBirth' => $spice_user_info->PersonDetails->DateOfBirth
						),
						'AddressType' => array(
								array(
									'AddressLine1' => $street,
									'Area' => $barangay,
									'Locality' => $province,
									'City' => $city,
									'Country' => 'PH',
									'Premise' => 'PH',
								)				
						),
						'Derivations' => $Derivations						
					)
				);

				$update_person = $this->spice->updatePerson($data);
				$update_person_json = $this->spice->parseJSON($update_person); 

 				 
				if ($update_person_json->MessageResponseHeader->TransactionStatus==5) {

					for($i = 0; $i<$total_derivations; $i++){
                            if($spice_user_info->Derivations[$i]->AttributeCode=='ca_referral_code'){
                                if($spice_user_info->Derivations[$i]->AttributeValue == '0'){
                                    $insert_new_user = array('referral_code' => (string)$referral_code);
                    
                                    $this->db->where('person_id', $person_id);
                                    $this->db->update('tbl_registrants', $insert_new_user); 
                                }
                            }
                    }  

                    return $update_person_json->MessageResponseHeader->TransactionStatusMessage;
                                         
                }else{
                    return $update_person_json->MessageResponseHeader->TransactionStatusMessage;
                }

			} else {

				return $spice_user_info->MessageResponseHeader->TransactionStatusMessage;

			}

		} else {

			return 'Person not found.';

		}

		

	}

	public function get_messages($user_id, &$message = '') {
		$user = $this->db->select('registrant_id')
						 ->from('tbl_registrants')
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row();
		if(!$user) {
			$message = 'Invalid uid';
			return false;
		}

		$messages = $this->db->select('message_id AS id, message, message_date_sent AS sent, sender_id, (SELECT nick_name FROM tbl_registrants WHERE registrant_id = sender_id) AS sender, (SELECT current_picture FROM tbl_registrants WHERE registrant_id = sender_id) AS current_picture')
							->from('tbl_messages')
							->where('recipient_id', $user_id)
							->get()
							->result_array();
		$items_arr = array();
		if($messages) {
			foreach ($messages as $key => $value) {
				$value['sent'] = strtotime($value['sent']) * 1000;
				$value['profile_picture'] = base_url() . 'uploads/profile/' . $value['sender_id'] . '/' . $value['current_picture'];
				unset($value['current_picture']); 
				$items_arr[] = $value;
			}
		}
		return $items_arr;
	}

	public function get_notifications($user_id, &$message = '') {
		$user = $this->db->select('registrant_id')
						 ->from('tbl_registrants')
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row();
		if(!$user) {
			$message = 'Invalid uid';
			return false;
		}

		$messages = $this->db->select('notification_id AS id, notification_message AS message, notification_date_created, notification_status AS status')
							->from('tbl_notifications')
							->where('registrant_id', $user_id)
							->where('notification_deleted', 0)
							->order_by('notification_date_created', 'DESC')
							->get()
							->result_array();
		$items_arr = array();
		if($messages) {
			foreach ($messages as $key => $value) {
				$value['date'] = strtotime($value['notification_date_created']) * 1000;
				unset($value['notification_date_created']);
				$items_arr[] = $value;
			}
		}
		return $items_arr;
	}

	public function read_notification($user_id, $id, &$message = '') {
		$notification = $this->db->select('notification_id')
					 ->from('tbl_notifications')
					 ->where('notification_id', $id)
					 ->where('registrant_id', $user_id)
					 ->get()
					 ->row();
		if(!$notification) {
			$message = 'Invalid id';
			return false;
		}

		$post['notification_status'] = 1;
		$post['notification_date_read'] = date('Y-m-d H:i:s');
		$this->db->where('notification_id', $id);
		$this->db->update('tbl_notifications', $post);
	
	}

    public function getUserProfile($user_id, &$message = '') {   

        $this->load->library('Spice');
        $this->spice->loginAdmin('MobilePHSysUser', 'MobilePHDefault#14');


        $user_data = (array) $this->db->select()
                ->from('tbl_registrants')
                ->where('registrant_id', $user_id)
                ->get()
                ->row();

        $response = $this->spice->GetPerson(array('PersonId'=>$user_data['person_id']));
        $response = $this->spice->parseJSON($response);     

        $ConsumerProfiles = $response->ConsumerProfiles;
        $ConsumerProfiles = $this->spice->parseJSON($this->spice->toJSONFormat((array)$ConsumerProfiles));      

        if($user_data) {
            $tasks = $this->get_tasks($user_data['registrant_id']);
            $tasks_arr = array();
            if($tasks) {
                foreach($tasks as $key => $value) {
                    if($value['status'] == 2) {
                        $tasks_arr[] = $value;
                    }
                }
            }
            $gender = $user_data['gender'] == 'M' ? 0 : 1;
            // Return spice data
            $full_address = $ConsumerProfiles->Addresses[0]->AddressLine1.', '.$ConsumerProfiles->Addresses[0]->Area.', '.$ConsumerProfiles->Addresses[0]->City.', '.$ConsumerProfiles->Addresses[0]->Locality;
            $points = $this->db->select('total_points')
                    ->from('tbl_registrants')
                    ->where('person_id', $ConsumerProfiles->PersonId)
                    ->get()
                    ->row()->total_points;
            $user_gender = $ConsumerProfiles->PersonDetails->Gender == 'M' ? 0 : 1;
            
            return array(
                    'id'            => $user_data['registrant_id'],
                    'nickname'      => $user_data['nick_name'],
                    'gender'        => $user_gender,
                    'mobile'        => '0' . $ConsumerProfiles->PersonDetails->MobileTelephoneNumber,
                    "street"        => $ConsumerProfiles->Addresses[0]->AddressLine1,
                    "barangay"      => $ConsumerProfiles->Addresses[0]->Area,
                    "city"          => $this->get_city_id($ConsumerProfiles->Addresses[0]->City),
                    "city_name"     => $ConsumerProfiles->Addresses[0]->City,
                    "province"      => $this->get_province_id($ConsumerProfiles->Addresses[0]->Locality),
                    "province_name" => $ConsumerProfiles->Addresses[0]->Locality,
                    'points'        => $points,
                    'password'      => $user_data['password'],
                    'image_url'     => str_replace('mobile_spice/', '', base_url()) . 'uploads/profile/' . $user_data['registrant_id'] . '/' . $user_data['current_picture'],
                    'tasks_completed'   => $tasks_arr
                );
        } else {
            $message = 'User not found.';
            return $message;
        }

        $return = '';

        return $return;

    }

	public function login($username, $password, $device_id, $device_name = '', $is_overwrite, &$message = '') {

		if(!$device_name) {
			$message = 'Device name is required';
			return false;	
		}

		if( ! $username) {
			$message = 'Username is required';
			return FALSE;
		}

		if( ! $password) {
			$message = 'Password is required';
			return FALSE;
		}

		$message = 'The username/password you entered is incorrect. Please try again.';

		if(filter_var($username, FILTER_VALIDATE_EMAIL) && $password) {

			$this->load->model('Registration_Model');
			$this->load->library('Spice');
			$this->spice->loginAdmin('MobilePHSysUser', 'MobilePHDefault#14');

			$registrant = FALSE;
			$accessGranted = $this->spice->loginMember($username, $password);
			$message = $accessGranted->ResponseHeader->TransactionStatusMessage;

			if(!$accessGranted->ResponseHeader->TransactionStatus && (int)$accessGranted->PersonId){

                $response = $this->spice->GetPerson(array('LoginName'=>$username));
                $response = $this->spice->parseJSON($response);


                $RegistrationDate = $this->spice->extractSpiceDate($response->ConsumerProfiles->RegistrationDate);
                
                $AgeVerificationStatus = $response->ConsumerProfiles->AgeVerificationStatus;
 
                if(!$AgeVerificationStatus){

                    return 'Your Age Verification status has not been verified.';
                    // redirect('user/login');
 
                }                           

                $ConsumerProfiles = $response->ConsumerProfiles;
                $ConsumerProfiles = $this->spice->parseJSON($this->spice->toJSONFormat((array)$ConsumerProfiles));
                
                $total_derivations = count($ConsumerProfiles->Derivations);
                        
                for($i = 0; $i<$total_derivations; $i++){

                    if($ConsumerProfiles->Derivations[$i]->AttributeCode=='ca_referral_code'){
                        $Derivations_referral = $ConsumerProfiles->Derivations[$i];
                    }
                    // else{
                    //     $Derivations_referral = '';
                    // }
                }
                
                $Derivations_referral = $Derivations_referral ? $Derivations_referral : '';


                // $Derivations_referral = $ConsumerProfiles->Derivations[REFERRAL_CODE];
                
                $is_registered =  $this->db->select()->from('tbl_registrants')->where('person_id',$ConsumerProfiles->PersonId)->get()->row();

                $province = strtolower($ConsumerProfiles->Addresses[0]->Locality);
                $city = strtolower($ConsumerProfiles->Addresses[0]->City);
                $street_name = $ConsumerProfiles->Addresses[0]->AddressLine1;
                $barangay = $ConsumerProfiles->Addresses[0]->Area;
                $zip_code = $ConsumerProfiles->Addresses[0]->PostalCode;

                $date_of_birth = $this->spice->extractSpiceDate($ConsumerProfiles->PersonDetails->DateOfBirth);
                
                $province = $this->db->select()->from('tbl_provinces')->where('LOWER(province)',$province)->get()->row();
                $province = $province ? $province->province_id : '';

                $city = $this->db->select()->from('tbl_cities')->where(array('LOWER(city) '=>$city,'province_id'=>$province))->get()->row();
                $city = $city ? $city->city_id : '';
                $nick_name = '';
                $outlet_code = '';
                // $referral_code = $this->Registration_Model->referral_code();
                $Derivations = array();

                if($ConsumerProfiles->Derivations){

                    $total_derivations = count($ConsumerProfiles->Derivations);
                        
                    for($i = 0; $i<$total_derivations; $i++){
                        if($ConsumerProfiles->Derivations[$i]->AttributeCode=='ca_nickname'){
                            $nick_name = $ConsumerProfiles->Derivations[$i]->AttributeValue;
                        }

                        if($ConsumerProfiles->Derivations[$i]->AttributeCode=='ca_outletcode'){
                            $outlet_code = $ConsumerProfiles->Derivations[$i]->AttributeValue;
                        }

                        if($ConsumerProfiles->Derivations[$i]->AttributeCode=='ca_personal_referral_code'){
	                        $referral_code = $ConsumerProfiles->Derivations[$i]->AttributeValue ? $ConsumerProfiles->Derivations[$i]->AttributeValue : $this->Registration_Model->referral_code();
					// changes
					if( ! $ConsumerProfiles->Derivations[$i]->AttributeValue || $ConsumerProfiles->Derivations[$i]->AttributeValue == '0') {
						$ConsumerProfiles->Derivations[$i]->AttributeValue = $referral_code;
					}
					// changes
                        }else{
                            // $ConsumerProfiles->Derivations[$i]->AttributeValue = '0';
                        }
                    }

                    // $nick_name = $ConsumerProfiles->Derivations[NICKNAME]->AttributeValue;
                    // $outlet_code = $ConsumerProfiles->Derivations[OUTLET_CODE]->AttributeValue;

                    $Derivations = json_decode(json_encode($ConsumerProfiles->Derivations),true);                    
                }           
                
                if( ! $ConsumerProfiles->Derivations[$i]->AttributeValue || $ConsumerProfiles->Derivations[$i]->AttributeValue == '0') {
				    $has_referral = $this->db->select('referral_code')->from('tbl_registrants')->where('person_id', (string)$ConsumerProfiles->PersonId)->get()->row();
    				if($has_referral->referral_code) {
    					$insert_new_user = array('person_id'=>(string)$ConsumerProfiles->PersonId,
    								 'first_name'=>(string)$ConsumerProfiles->PersonDetails->FirstName,
    								 'third_name'=>(string)$ConsumerProfiles->PersonDetails->LastName,
    								 'middle_initial'=>(string)$ConsumerProfiles->PersonDetails->MiddleName,
    								 'nick_name'=>$nick_name,
    								 'outlet_code'=>$outlet_code,
    								 'referred_by_code'=> ($Derivations_referral->AttributeValue && $Derivations_referral->AttributeCode==='ca_referral_code') ? $Derivations_referral->AttributeValue : ''
    									);
    				} else {
    					$insert_new_user = array('person_id'=>(string)$ConsumerProfiles->PersonId,
    								 'first_name'=>(string)$ConsumerProfiles->PersonDetails->FirstName,
    								 'third_name'=>(string)$ConsumerProfiles->PersonDetails->LastName,
    								 'middle_initial'=>(string)$ConsumerProfiles->PersonDetails->MiddleName,
    								 'nick_name'=>$nick_name,
    								 'outlet_code'=>$outlet_code,
    								 'referral_code' => (string) $referral_code,
    								 'referred_by_code'=> ($Derivations_referral->AttributeValue && $Derivations_referral->AttributeCode==='ca_referral_code') ? $Derivations_referral->AttributeValue : ''
    									);
    				}					
    			} else {
    				$insert_new_user = array('person_id'=>(string)$ConsumerProfiles->PersonId,
    								 'first_name'=>(string)$ConsumerProfiles->PersonDetails->FirstName,
    								 'third_name'=>(string)$ConsumerProfiles->PersonDetails->LastName,
    								 'middle_initial'=>(string)$ConsumerProfiles->PersonDetails->MiddleName,
    								 'nick_name'=>$nick_name,
    								 'outlet_code'=>$outlet_code,
    								 'referred_by_code'=> ($Derivations_referral->AttributeValue && $Derivations_referral->AttributeCode==='ca_referral_code') ? $Derivations_referral->AttributeValue : ''
    									);
    			}

                // $insert_new_user = array('person_id'=>(string)$ConsumerProfiles->PersonId,
                //                      'first_name'=>(string)$ConsumerProfiles->PersonDetails->FirstName,
                //                      'third_name'=>(string)$ConsumerProfiles->PersonDetails->LastName,
                //                      'middle_initial'=>(string)$ConsumerProfiles->PersonDetails->MiddleName,
                //                      'nick_name'=>$nick_name,
                //                      'outlet_code'=>$outlet_code,
                //                      'referral_code' => (string)$referral_code,
                //                      'referred_by_code'=> (isset($Derivations_referral->AttributeValue) && isset($Derivations_referral->AttributeCode) && $Derivations_referral->AttributeValue && $Derivations_referral->AttributeCode==='ca_referral_code') ? $Derivations_referral->AttributeValue : ''
                //                         );
             
                if($is_registered){

                    $this->db->where('person_id', $ConsumerProfiles->PersonId);
                    $this->db->update('tbl_registrants', $insert_new_user); 

                }else{

                    $this->db->set('date_created',"'$RegistrationDate'",FALSE);
                    $this->db->insert('tbl_registrants', $insert_new_user);

                    
                    $params = array('person_id'=> $ConsumerProfiles->PersonId,
                                    'updates'=> array('Derivations' => $Derivations)
                                );

                    $response = $this->spice->updatePerson($params);
                    $response_json = $this->spice->parseJSON($response);

                }

                $registrant = $this->db->select()->from('tbl_registrants')->where('person_id',$ConsumerProfiles->PersonId)->get()->row();       
             
                $user = array('login_attempts'=>$registrant->login_attempts,
                              'date_blocked'=>$registrant->date_blocked,
                              'person_id'=>$ConsumerProfiles->PersonId,
                              'registrant_id'=>$registrant->registrant_id,
                              'referred_by_code'=>$registrant->referred_by_code,
                              'registrant_id'=>$registrant->registrant_id,
                              'first_name'=>$ConsumerProfiles->PersonDetails->FirstName,
                              'third_name'=>$ConsumerProfiles->PersonDetails->LastName,
                              'middle_initial'=>$ConsumerProfiles->PersonDetails->MiddleName,
                              'date_of_birth'=>$ConsumerProfiles->PersonDetails->DateOfBirth,
                              'nick_name'=>$registrant->nick_name,
                              'mobile_phone'=>$ConsumerProfiles->PersonDetails->MobileTelephoneNumber,
                              'referral_code'=>$registrant->referral_code,
                              'email_address'=>$ConsumerProfiles->Login[0]->LoginName,
                              'total_points'=>$registrant->total_points,
                              'status'=>$ConsumerProfiles->AgeVerificationStatus,
                              'is_cm'=>$registrant->is_cm,
                              'device_id' => $registrant->device_id,
                              'device_name' => $registrant->device_name
                          );                    

                $this->load->library('Encrypt');

                $today = date('Y-m-d H:i:s');                

                // check if from referral;
                $isFirstLogin = false;
                if($user['referred_by_code']){

 
                    $referer = $this->Registration_Model->get_user_by_refCode($user['referred_by_code']);
                    if($referer){
                        $isFirstLogin = $this->Registration_Model->isFirstLogin($user['registrant_id']);
                    }
                    
                }
 
                $this->db->insert('tbl_login', array(
                    'registrant_id' => $user['registrant_id'],
                    'date_login' => $today
                ));
                $insert_id = $this->db->insert_id();

                if ($this->db->affected_rows()) {

                    $this->load->model(array('profile_model'));

                    $user_data = (array) $this->db->select()
                            ->from('tbl_registrants')
                            ->where('registrant_id', $user['registrant_id'])
                            ->get()
                            ->row();

                    $this->session->set_userdata('user', array(
                        'first_name' => $user['first_name'],
                        'last_name' => $user['third_name'],
                        'middle_initial' => $user['middle_initial'],
                        'birthdate' => $user['date_of_birth'],
                        'nick_name' => $user['nick_name'],
                        'mobile_num' => $user['mobile_phone'],
                        'referral_code' => $user['referral_code'],
                        'person_id'=> $user['person_id'],
                        'email' => $user['email_address']
                    ));

                    $this->session->set_userdata('check_offers', 1);
                    $this->session->set_userdata('user_points', $user['total_points']);
                    $this->session->set_userdata('user_id', $user['registrant_id']);
                    $this->session->set_userdata('login_id', $insert_id);
                    $this->session->set_userdata('is_cm', $user['is_cm']);
                    $this->session->set_userdata('flash_offer_sess', 1);
                    $this->session->set_userdata('app_id', Spice::APP_ID);

                    if($device_id != $user['device_id'] && !$is_overwrite && $user['device_name']) {
						$message = 'Your account is currently logged in to ' . $user['device_name'] ;
						return 2;	
					}

                    $this->db->where('registrant_id', $user['registrant_id']);
                    $this->db->update('tbl_registrants', array(
                        'last_login' => $today,
                        'login_attempts' => 0,
                        'date_blocked' => null,
                        'login_id' => $this->session->userdata('login_id'),
                        'device_id' => $device_id,
                        'device_name' => $device_name,
                        'app_id' => $this->session->userdata('app_id')
                    ));

                    if(!$isFirstLogin){

                        $this->Points_Model->earn(LOGIN, array(
                        'date' => $today,
                        'suborigin' => $this->session->userdata('login_id')
                        ));

                        if(!$user['referred_by_code']){
                            
                            $this->Points_Model->earn(REGISTRATION);                             
                        }
                        
                        //SPICE INSERT DERIVATIONS
                        $this->spice->loginAdmin('MobilePHSysUser', 'MobilePHDefault#14');

                        $response = $this->spice->GetPerson(array('PersonId' => (int)$accessGranted->PersonId ));
                        $spice_userinfo = $this->spice->parseJSON($response);
                        $spice_userinfo = $spice_userinfo->ConsumerProfiles;

                        $consumer_attributes = $this->profile_model->get_rows(array('table'=>'tbl_consumer_attr_spice'))->result_array();
                        
                        $total_derivations = count($spice_userinfo->Derivations);
                        $total_address = count($spice_userinfo->Addresses);
                        $total_cas = count($consumer_attributes);
                        $derivations = array();
                        $var = array();
                        $country = 'PH';
                        
                        // print_r($spice_userinfo); die();
                        for($i = 0; $i<$total_cas; $i++){       
                                $var = array('AttributeCode'=> $consumer_attributes[$i]['attribute_code'],'AttributeValue'=> '0');
                            for($j = 0; $j<$total_derivations; $j++){
                                if($consumer_attributes[$i]['attribute_code'] == $spice_userinfo->Derivations[$j]->AttributeCode){
                                    $var = array('AttributeCode'=> $spice_userinfo->Derivations[$j]->AttributeCode,'AttributeValue'=> $spice_userinfo->Derivations[$j]->AttributeValue);
                                    break;
                                }
                            }
                            
                            $derivations[] = $var;

                        }


                        if($total_address != 0){
                            if($spice_userinfo->Addresses[0]->Country == ''){
                                $country = 'PH';
                            }

                            $params = array('person_id'=> $accessGranted->PersonId,
                                            'updates'=> array(
                                                'AddressType' => array(
                                                    array(
                                                        'AddressLine1' => $spice_userinfo->Addresses[0]->AddressLine1,
                                                        'Area' => $spice_userinfo->Addresses[0]->Area,
                                                        'Locality' => $spice_userinfo->Addresses[0]->Locality,
                                                        'City' => $spice_userinfo->Addresses[0]->City, 
                                                        'Country' => $country,
                                                        'Premise'=> $country,
                                                        'PostalCode' => $spice_userinfo->Addresses[0]->PostalCode
                                                    )
                                                ),
                                                'Derivations'=> $derivations,
                                        )
                            );
                        }else{
                                $params = array('person_id'=> $this->spice->getMemberPersonId(),
                                            'updates'=> array(
                                                'Derivations'=> $derivations,
                                        )
                                );
                        }

                        $response = $this->spice->updatePerson($params);
                        $response_json = $this->spice->parseJSON($response);


                        if ($response_json->MessageResponseHeader->TransactionStatus != 5) {
                            return $response_json->MessageResponseHeader->TransactionStatusMessage;
                        }
                    
                    }else{
                        /// Insert spice data to website db
				        $this->notification_model = new Notification_Model();

                        $this->Points_Model->earn(
                            REFEREE_FIRSTLOGIN,
                            array(
                                'registrant' => $user['registrant_id'],
                                'suborigin' => $this->session->userdata('login_id'),
                                'remarks' => 'first login as LAS2'
                            ));

                        $earn = $this->Points_Model->earn(
                            REFERER_FIRSTLOGIN,
                            array(
                                'registrant' => $referer['registrant_id'],
                                'suborigin' => $this->session->userdata('login_id'),
                                'remarks' => 'LAS2 ('. $user['registrant_id'] .') logged-in'
                            ));

                        $notifyLas2 = "Welcome to the MARLBORO community! Here’s ".REFEREE_FIRST_LOGIN_POINT." points to get you started. ".
                                    "Discover the many exciting offers you can redeem on PERKS and continue visiting".
                                    " the website to get more points. And while you’re at it, why don’t you invite".
                                    " your adult smoker friends as well?";

                        $this->notification_model->notify($user['registrant_id'], REFEREE_FIRSTLOGIN, 
                            array('message' => $notifyLas2,'suborigin'=>0));

                        $notifyLas1First = "Congratulations! Your friend, ".$user['first_name']." ".$user['third_name'].", ".
							"is now part of the MARLBORO community! You get 100 points ".
							"for the successful referral. Don't forget to invite ".$user['nick_name']." ".
							"to log in and experience the exclusive PERKS in the website.";

						$this->notification_model->notify($referer['registrant_id'], REFERER_FIRSTLOGIN,
							array('message' => $notifyLas1First,'suborigin'=>0));


                        $notifyLas1 = "Congratulations! You get 200 more points! Your friend, ".
                            $user['first_name']." ".$user['third_name'].", has just logged in to MARLBORO.PH.".
                            " Invite your friends to be active members of the community,".
                            " and get more points to win freebies.";
                        
                        $this->notification_model->notify($referer['registrant_id'], REFERER_FIRSTLOGIN, 
                            array('message' => $notifyLas1,'suborigin'=>0));

                    }

					if($user_data) {
						$tasks = $this->get_tasks($user_data['registrant_id']);
						$tasks_arr = array();
						if($tasks) {
							foreach($tasks as $key => $value) {
								if($value['status'] == 2) {
									$tasks_arr[] = $value;
								}
							}
						}
						$gender = $user_data['gender'] == 'M' ? 0 : 1;
						// Return spice data
						$full_address = $ConsumerProfiles->Addresses[0]->AddressLine1.', '.$ConsumerProfiles->Addresses[0]->Area.', '.$ConsumerProfiles->Addresses[0]->City.', '.$ConsumerProfiles->Addresses[0]->Locality;
						$points = $this->db->select('total_points')
								->from('tbl_registrants')
								->where('person_id', $ConsumerProfiles->PersonId)
								->get()
								->row()->total_points;
						$user_gender = $ConsumerProfiles->PersonDetails->Gender == 'M' ? 0 : 1;
						return array(
								'id'			=> $user_data['registrant_id'],
								'nickname'		=> $user_data['nick_name'],
								'gender'		=> $user_gender,
								'mobile'		=> '0' . $ConsumerProfiles->PersonDetails->MobileTelephoneNumber,
								"street"		=> $ConsumerProfiles->Addresses[0]->AddressLine1,
						        "barangay"		=> $ConsumerProfiles->Addresses[0]->Area,
						        "city"			=> $this->get_city_id($ConsumerProfiles->Addresses[0]->City),
						        "city_name"		=> $ConsumerProfiles->Addresses[0]->City,
						        "province"      => $this->get_province_id($ConsumerProfiles->Addresses[0]->Locality),
						        "province_name" => $ConsumerProfiles->Addresses[0]->Locality,
								'points'		=> $points,
								'password'		=> $user_data['password'],
								'image_url'		=> str_replace('mobile_spice/', '', base_url()) . 'uploads/profile/' . $user_data['registrant_id'] . '/' . $user_data['current_picture'],
								'tasks_completed'	=> $tasks_arr
							);
					} else {
						$message = 'User not found.';
						return $message;
					}

    			} else {
    				return $message;
    			}
            } else {
            	$message = $accessGranted->ResponseHeader->TransactionStatusMessage;
            	return $message;
            }

		} // END filter_var($username, FILTER_VALIDATE_EMAIL) && $password

        $return = '';

        return $return;

	}

	public function logoutEndSession($user_id, &$message = '') { 

        $this->load->library('Spice');

		$user = (array) $this->db->select()
					->from('tbl_registrants')
					->where('registrant_id', $user_id)
					->get()
					->row();

		if($user) {
			$this->db->where('registrant_id', $user['registrant_id']);
			$this->db->update('tbl_registrants', array('device_name' => '', 'device_id' => ''));

			// For spice session
			$this->load->library('Spice');
			$this->spice->loginAdmin('MobilePHSysUser', 'MobilePHDefault#14');

			$session = (array) $this->db->select()
							->from('tbl_spice_mobile_sessions')
							->where('person_id', $user['person_id'])
							->get()
							->row();

			$session_key = $session ? $session['session_key'] : '';

			$this->spice->endSession($session_key);

				/*$spice_session = (array) $this->db->select()
									->from('tbl_spice_mobile_sessions')
									->where('person_id', $user['person_id'])
									->get()
									->row();
				if($spice_session) {
					$this->db->where('person_id', $spice_session['person_id'])->delete();
				}*/
			// End for spice session
			return true;
		} else {
			// $message = 'Invalid username or password';
			$message = 'Invalid user id';
			return false;
		}
	}

    public function clearDeviceId($username, &$message = '') { 

        $this->load->library('Spice');
        $this->spice->loginAdmin('MobilePHSysUser', 'MobilePHDefault#14');

        $response = $this->spice->GetPerson(array('LoginName'=> $username));
        $response = $this->spice->parseJSON($response);
        $ConsumerProfiles = $response->ConsumerProfiles;
        $ConsumerProfiles = $this->spice->parseJSON($this->spice->toJSONFormat((array)$ConsumerProfiles));

        $user = (array) $this->db->select()
                    ->from('tbl_registrants')
                    ->where('person_id', $ConsumerProfiles->PersonId)
                    ->get()
                    ->row();

        if($user) {
            $this->db->where('registrant_id', $user['registrant_id']);
            $this->db->update('tbl_registrants', array('device_name' => '', 'device_id' => ''));
           
            return true;
        } else {
            $message = 'Invalid username.';
            // $message = 'Invalid user id';
            return false;
        }
       
    }

    public function setActiveDevice($email, $device_id, $device_name, &$message = '') { 

        $this->load->library('Spice');
        $this->spice->loginAdmin('MobilePHSysUser', 'MobilePHDefault#14');

        $response = $this->spice->GetPerson(array('LoginName'=>$email));
        $response = $this->spice->parseJSON($response);
        
        $ConsumerProfiles = $response->ConsumerProfiles;
        $ConsumerProfiles = $this->spice->parseJSON($this->spice->toJSONFormat((array)$ConsumerProfiles));

        $user_data = (array) $this->db->select()
                    ->from('tbl_registrants')
                    ->where('person_id', $ConsumerProfiles->PersonId)
                    ->get()
                    ->row();

        if($user_data) {
            $this->db->where('registrant_id', $user_data['registrant_id']);
            $this->db->update('tbl_registrants', array('device_name' => $device_name, 'device_id' => $device_id));

            $tasks = $this->get_tasks($user_data['registrant_id']);
            $tasks_arr = array();
            if($tasks) {
                foreach($tasks as $key => $value) {
                    if($value['status'] == 2) {
                        $tasks_arr[] = $value;
                    }
                }
            }
            $gender = $user_data['gender'] == 'M' ? 0 : 1;
            // Return spice data
            $full_address = $ConsumerProfiles->Addresses[0]->AddressLine1.', '.$ConsumerProfiles->Addresses[0]->Area.', '.$ConsumerProfiles->Addresses[0]->City.', '.$ConsumerProfiles->Addresses[0]->Locality;
            $points = $this->db->select('total_points')
                    ->from('tbl_registrants')
                    ->where('person_id', $ConsumerProfiles->PersonId)
                    ->get()
                    ->row()->total_points;
            $user_gender = $ConsumerProfiles->PersonDetails->Gender == 'M' ? 0 : 1;
            return array(
                    'id'            => $user_data['registrant_id'],
                    'nickname'      => $user_data['nick_name'],
                    'gender'        => $user_gender,
                    'mobile'        => '0' . $ConsumerProfiles->PersonDetails->MobileTelephoneNumber,
                    "street"        => $ConsumerProfiles->Addresses[0]->AddressLine1,
                    "barangay"      => $ConsumerProfiles->Addresses[0]->Area,
                    "city"          => $this->get_city_id($ConsumerProfiles->Addresses[0]->City),
                    "city_name"     => $ConsumerProfiles->Addresses[0]->City,
                    "province"      => $this->get_province_id($ConsumerProfiles->Addresses[0]->Locality),
                    "province_name" => $ConsumerProfiles->Addresses[0]->Locality,
                    'points'        => $points,
                    'password'      => $user_data['password'],
                    'image_url'     => str_replace('mobile_spice/', '', base_url()) . 'uploads/profile/' . $user_data['registrant_id'] . '/' . $user_data['current_picture'],
                    'tasks_completed'   => $tasks_arr
                );
        } else {
            $message = 'User not found.';
            return $message;
        }

        
    }

	public function save_bid($user_id, $id, $bid, &$message = '') {
		$user = $this->db->select('*')
						 ->from('tbl_registrants')
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row_array();
		if(!$user) {
			$message = 'Invalid uid';
			return false;
		}
		
		$item = $this->db->select('*')
						->from('tbl_bid_items')
						->where('bid_item_id', $id)
						->get()
						->row_array();

		if(!$item) {
			$message = 'Invalid id';
			return false;	
		}	
		$bidder = (array) $this->perks->get_bidder($id); 
		$required_bid = $item['starting_bid'] >= @$bidder['bid'] ? $item['starting_bid'] : $bidder['bid'];
		$bid_data = $this->perks->check_bid_points($user_id);

		
		if(!is_numeric($bid)) {
			$message = 'Bid amount is invalid.';
			return false;
		} elseif(!$item) {
			
			$message = 'Item does not exist.';	
			return false;
		} elseif($user['total_points'] < $bid) {
			
			$message = 'Insufficient points.';	
			return false;
		} elseif($user['total_points'] >= $bid && $bid_data['available_points'] < $bid) {
			
			$message = 'You only have ' . $bid_data['available_points'] . ' points available for you to use. You placed a bid for ' . implode(', ', $bid_data['items']) . ', and ' . $bid_data['reserved_points'] . ' points are blocked off while you are still the highest bidder.';	
			return false;
		} elseif(isset($bidder['registrant_id']) && $bidder['registrant_id'] == $this->session->userdata('user_id')) {
			
			$message = 'You are currently the highest bidder.';	
			return false;
		} elseif(strtotime(date('Y-m-d H:i:s')) > strtotime($item['end_date']) || $item['status'] == 0) {
			
			$message = 'Sorry, this item is no longer active for bidding.';	
			return false;
		} elseif(strtotime(date('Y-m-d H:i:s')) < strtotime($item['start_date'])) {
			
			$message = 'Sorry, this item is not yet active for bidding.';	
			return false;
		} elseif($bid < $required_bid) {
			
			$message = 'Please place a bid higher than  ' . $required_bid . ' points.';	
			return false;
		} elseif(@$bidder['bid'] >= $bid) {
			
			$message = 'Please place a bid higher than  ' . $bidder['bid'] . ' points.';
			return false;
		} else {

			/* Spice trackAction */
			$this->load->library('Spice');
			$this->spice->loginAdmin('MobilePHSysUser', 'MobilePHDefault#14');

			$sec_settings = $this->db->select()->from('tbl_section_settings')->where('name', 'PERKS_BID')->get()->row();
			$cell_id = $sec_settings->setting_section_id;

			$act_settings = $this->db->select()->from('tbl_activity_settings')->where('name', 'PERKS_BID')->get()->row();
			$activity_id = $act_settings->setting_activity_id;

			$track_action_data = array(
				'CellId' => $cell_id,
				'ActionList' => array(
					array(
						'ActivityId' => $activity_id,
						'ActionValue' => $item['bid_item_name'] . '||' . $bid
					)
				)
			);

			// PersonId
			$person = (array) $this->db->select()
							->from('tbl_registrants')
							->where('registrant_id', $user_id)
							->get()
							->row();

			if($person) {
				$response = $this->spice->trackAction($track_action_data, $person['person_id']);
				$response = $this->spice->parseJSON($response);
			}		
			/* Spice trackAction */

			if($response->MessageResponseHeader->TransactionStatus != 0) {
				$message = $response->MessageResponseHeader->TransactionStatusMessage;
				return false;
			}

			if(isset($bidder['registrant_id'])) {
				$this->notification_model = new Notification_Model();
				$message = 'You are no longer the highest bidder of ' . $item['bid_item_name'] . '. Should you wish to bid again, please do so here: <a href="' . BASE_URL . 'perks/bid/' . $item['bid_item_id'] . '">' . BASE_URL . 'perks/bid/' . $item['bid_item_id'] . '</a>';
				$param = array('message'=>$message,'suborigin'=>$id);
				$this->notification_model->notify($bidder['registrant_id'],PERKS_BID, $param);	
			}
			$data['success'] = 1;
			$param = array();
			$param['bid_item_id'] = $this->input->post('id');
			$param['bid'] = $this->input->post('bid_amt');
			$param['registrant_id'] = $this->input->post('user_id');
			$param['bid_date']  = date('Y-m-d H:i:s');
			$param['source'] = 2;
			$this->perks->save_bid($param);
		}
		return 1;
		
	} 

	public function save_buy($user_id, $id, &$message = '') {
		$user = $this->db->select('*')
						 ->from('tbl_registrants')
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row_array();
		if(!$user) {
			$message = 'Invalid uid';
			return false;
		}
		
		$item = (array) $this->perks->get_buy_item($id);

		if(!$item) {
			$message = 'Invalid id';
			return false;	
		}

		$person = (array) $this->db->select()
						->from('tbl_registrants')
						->where('registrant_id', $user_id)
						->get()
						->row();
		
		
		$bid_data = $this->perks->check_bid_points($user_id);	
		if($user['total_points'] < $item['credit_value']) {
			$message = "You don’t have enough points to buy " . $item['buy_item_name'] . ".";	
			return false;
		} elseif($item['status'] != 1) {
			$message = $item['buy_item_name'] . " is not yet active for buying.";	
			return false;
		} elseif($user['total_points'] >= $item['credit_value'] && $bid_data['available_points'] < $item['credit_value']) {
			$message = 'You only have ' . $bid_data['available_points'] . ' points available for you to use. You placed a bid for ' . implode(', ', $bid_data['items']) . ', and ' . $bid_data['reserved_points'] . ' points are blocked off while you are still the highest bidder.';	
			return false;
		} else {
			if($this->perks->has_bought($id, $user_id)) {
				$message = 'You already bought ' . $item['buy_item_name'] . '.';
				return false;
			} elseif($item['status'] == 0 || $item['stock'] <= 0) { 
				$message = 'Sorry, this item is out of stock.';
				return false;
			} else {
				/* Spice trackAction */
				$this->load->library('Spice');
				$this->spice->loginAdmin('MobilePHSysUser', 'MobilePHDefault#14');

				$sec_settings = $this->db->select()->from('tbl_section_settings')->where('name', 'PERKS_BUY')->get()->row();
				$cell_id = $sec_settings->setting_section_id;

				$act_settings = $this->db->select()->from('tbl_activity_settings')->where('name', 'PERKS_BUY')->get()->row();
				$activity_id = $act_settings->setting_activity_id;
				
				$track_action_data = array(
					'CellId' => $cell_id,
					'ActionList' => array(
						array(
							'ActivityId' => $activity_id,
							'ActionValue' => $item['buy_item_name']
						)
					)
				);

				if($person) {
					$response = $this->spice->trackAction($track_action_data, $person['person_id']);
					$response = $this->spice->parseJSON($response);
				}		

				if($response->MessageResponseHeader->TransactionStatus != 0) {
					$message = $response->MessageResponseHeader->TransactionStatusMessage;
					return FALSE;
				}
				/* Spice trackAction */

				$param['buy_item_id'] = $id;
				$param['registrant_id'] = $user_id;	
				$param['source'] = 2;

				$properties = $this->db->select('property_name, property_id')
									->from('tbl_properties')
									->where('buy_item_id', $id)
									->get()
									->result_array();
				$prop = array();
				$prop_ids = array();
				if($properties) {
					foreach ($properties as $key => $value) {
						$prop[] = $value['property_name'];
						$prop_ids[] = $value['property_id'];
					}
				}

				$_POST['prop'] = $prop;
				$_POST['val'] = array($this->input->post('prop1_selection'), $this->input->post('prop2_selection'));

				// if(count($prop) != count($_POST['val'])) {
				// 	$message = 'Property is required';
				// 	return false;	
				// }

				foreach($prop as $k => $p) {

					if($k == 0 && !$this->input->post('prop1_selection')) {
						$message = 'Property 1 is required';
						return false;	
					}

					if($k == 1 && !$this->input->post('prop2_selection')) {
						$message = 'Property 2 is required';
						return false;	
					}

					$property_check = $this->db->select()
											 ->from('tbl_property_values')
											 ->where_in('property_id', $prop_ids)
											 ->where('value', $_POST['val'][$k])
											 ->get()
											 ->result_array();
					if(!$property_check) {
						$message = 'Invalid property name/value';
						return false;	
					}
				} 
				
				$item_stock = $this->perks->save_buy_item($id, $param);
				$param = array();
				# deduct stock count	
				if(isset($item_stock['stocks']) && $item_stock['stocks'] - 1 >= 0) {
					$data['success'] = 1;
					$data['id'] = $id;
					$this->Points_Model->spend(PERKS_BUY, array(
											'points' => $item['credit_value'],
											'suborigin' => $id,
											'remarks' => '{{ name }} bought ' . $item['buy_item_name'] . ' on Perks',
											'registrant' => $user_id
											));
					$this->perks->deduct_stock($this->input->post('id'), $item['stock'] - 1);
					$this->notification_model = new Notification_Model();
					//$buy = $this->Perks_Model->get_buy_details($id);
					$message = 'You have successfully purchased ' . $item['buy_item_name'] . ' for ' . $item['credit_value'] . ' points. <br><br>
								' . $item['buy_item_name'] . ' will be delivered to your registered mailing address within 60 days. Please ensure that all your contact details are up-to-date. <br><br>
								Continue logging on to MARLBORO.PH to earn more points!';

	 				$param = array('message'=>$message,'suborigin'=>$id);
					$this->notification_model->notify($user_id,PERKS_BUY,$param);

				// } else {
				// 		$message = 'Sorry, ' . @$values[0] . ' ' . @$values[1] . ' ' .$item['buy_item_name'] . ' is no longer available';
				}
				
				
				return 1;
			}
		}
	}

	public function get_purchases($user_id, &$message = '') {
		$user = $this->db->select('*')
						 ->from('tbl_registrants')
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row_array();
		if(!$user) {
			$message = 'Invalid uid';
			return false;
		}

		$purchases = $this->db->select('buy_item_id AS id, buy_date AS date_purchased')
							  ->from('tbl_buys')
							  ->where('registrant_id', $user_id)
							  ->get()
							  ->result_array();
		$item_arr = array();
		if($purchases) {
			foreach ($purchases as $key => $value) {
				$value['date_purchased'] = strtotime($value['date_purchased']) * 1000;
				$item_arr[] = $value;
			}
		}
		return $item_arr;
	}

	public function get_tasks($user_id, &$message = '') {
		$user = $this->db->select('*')
						 ->from('tbl_registrants')
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row_array();
		if(!$user) {
			$message = 'Invalid uid';
			return false;
		}

		$purchases = $this->db->select('move_forward_title AS title, move_forward_description AS description, mf.move_forward_id AS id, move_forward_choice, move_forward_choice_status, move_forward_choice_date AS task_date')
							  ->from('tbl_move_forward mf')
							  ->join('tbl_move_forward_choice mfc', 'mf.move_forward_id = mfc.move_forward_id')
							  ->where('registrant_id', $user_id)
							  ->get()
							  ->result_array();
		$item_arr = array();
		if($purchases) {
			foreach ($purchases as $key => $value) {

				if($value['move_forward_choice'] == 1 && $value['move_forward_choice_status'] == 0)
					$value['status'] =  1;
				else 
					$value['status'] = 2;

				if($value['move_forward_choice_status'] == 1)
					$value['is_active'] = 1;
				else
					$value['is_active'] = 0;

				unset($value['move_forward_choice']);
				unset($value['move_forward_choice_status']);

				$value['task_date'] = strtotime($value['task_date']) * 1000;
				$item_arr[] = $value;
			}
		}
		return $item_arr;
	}

	public function get_updates($user) {
		$feeds_arr = array();
		$feeds = $this->db->select('points_id AS id, remarks AS message, date_earned AS sent, r.registrant_id, r.first_name, r.third_name, nick_name')
				->from('tbl_points p')
				->where('p.remarks IS NOT NULL')
				->join('tbl_registrants r', 'r.registrant_id = p.registrant_id')
				->order_by('p.date_earned', 'DESC')
				->limit(10)
				->get()
				->result_array();
			foreach ($feeds as $feed) {
				$feed['sent'] = strtotime($feed['sent']) * 1000;
				if ((int) $feed['registrant_id'] === $user) {
					$feed['message'] = str_replace(array('{{ name }}', ' is '), array('You', ' are '), $feed['message']);
				} else {
					$feed['message'] = str_replace('{{ name }}', $feed['nick_name'], $feed['message']);
				}
				$feed['message'] = strtoupper($feed['message']);
				$feeds_arr[] = $feed;
			}

		return $feeds_arr;
	}

	public function get_cities($province_id) {
		$cities = $this->db->select('*')
						 ->from('tbl_cities')
						 ->where('province_id', $province_id)
						 ->order_by('city')
						 ->get()->result_array();
		return $cities;
	}

	public function get_provinces() {
		$provinces = $this->db->select('*')
						 ->from('tbl_provinces')
						 ->order_by('province')
						 ->get()->result_array();
		return $provinces;
	}

	public function get_mobile_prefixes() {
		$prefixes = $this->db->select('mobile_prefix')
						 ->from('tbl_mobile_prefix')
						 ->order_by('mobile_prefix')
						 ->get()->result_array();
		$prefixes_arr = array();
		if($prefixes) {
			foreach ($prefixes as $key => $value) {
				$prefixes_arr[] = $value['mobile_prefix'];
			}
		}
		return $prefixes_arr;
	}

	public function sync_to_calendar($id, $user_id, &$message = '') {
		$this->db->insert('tbl_calendar_sych', array('id'	=> $id, 'registrant_id' => $user_id));
	}

	private function get_province_name($province_id) {
		$province = $this->db->select('province')
						->from('tbl_provinces')
						->where('province_id', $province_id)
						->get()
						->row_array();
		return isset($province['province']) ? $province['province'] : '';
	}

	private function get_city_name($city_id) {
		$city = $this->db->select('city')
						->from('tbl_cities')
						->where('city_id', $city_id)
						->get()
						->row_array();
		return isset($city['city']) ? $city['city'] : '';
	}

	private function get_province_id($name) {
		$province = $this->db->select('province_id')
						->from('tbl_provinces')
						->where('province', $name)
						->get()
						->row_array();
		return isset($province['province_id']) ? $province['province_id'] : '';
	}

	private function get_city_id($name) {
		$city = $this->db->select('city_id')
						->from('tbl_cities')
						->where('city', $name)
						->get()
						->row_array();
		return isset($city['city_id']) ? $city['city_id'] : '';
	}
	
}