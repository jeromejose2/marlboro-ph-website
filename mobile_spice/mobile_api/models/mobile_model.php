<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mobile_Model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		define('BASE_URL', base_url());
		define('BUY_BID', 33);
	}

	public function get_all_items($lat, $long, $radius) {
		$user_id = $this->input->post('user_id');
		$buy_bid = $this->get_buy_bid_items($lat, $long, $radius, $user_id);
		$move_fwd = $this->get_movefwd_items($lat, $long, $radius);
		$events = $this->get_event_items($lat, $long, $radius);	
		$glist = array('data' => array());//$this->get_glist_items($lat, $long, $radius);

		if(!isset($buy_bid['data']))
			$buy_bid['data'] = array();
		if(!isset($move_fwd['data']))
			$move_fwd['data'] = array();
		
		$data = array_merge($buy_bid['data'], $move_fwd['data']);
		if(!isset($events['data']))
				$events['data'] = array();
		$data = array_merge($data, $events['data']);
		$data = array_merge($data, @$glist['data']);

		if($data)
			return array('error' => '0', 'message' => 'Success', 'data' => $data);
		else 
			return array('error' => '1', 'message' => 'No matching records found');
		
	}
	
	public function get_buy_items($lat, $long, $radius, $user_id) {
		//$having = $radius > 0 ? "HAVING distance <= $radius" : ""; 
		$user = $this->check_user($user_id);
		if(!$user)
			return array('error' => '1', 'message' => 'Invalid uid');
		$buys = $this->db->select('buy_item_id')
						->from('tbl_buys')
						->where('registrant_id', $user_id)
						->get()
						->result_array();
		$buy_ids = array(0);
		if($buys) {
			foreach ($buys as $key => $value) {
				$buy_ids[] = $value['buy_item_id'];
			}
		}
		$query = "	SELECT 
				 	buy_item_id AS id, 
				    buy_item_image AS image_url,
				    buy_item_name AS title,
				    description AS `desc`,
				    lat AS loc_lat,
				    lng AS loc_lng,
				    (SELECT ((ACOS(SIN($lat * PI() / 180) * SIN(lat * PI() / 180) + COS($lat * PI() / 180) * COS(lat * PI() / 180) * COS(($long - lng) * PI() / 180)) * 180 / PI()) * 60 * 1.1515)) AS `distance`
					FROM tbl_buy_items
					WHERE status = 1
					AND stock > 0
					AND is_deleted = 0
					AND platform_restriction != 1
					AND buy_item_id NOT IN (" . implode(',', $buy_ids) . ")
					";
		$items = $this->db->query($query)->result_array();
		$item_arr = array();
		if($items) {
			foreach ($items as $key => $value) {
				//$features = explode('<br />', $value['details']);
				$value['image_url']	= str_replace('mobile_spice/', '', BASE_URL) . 'uploads/buy/' . $value['image_url'];
				$value['desc'] = strip_tags(html_entity_decode($value['desc']));
				$value['type'] = TYPE_BUY;
				$value['type_string'] = TYPE_STRING_BUY;
				$value['from'] = 0;
				$value['to'] = 0;
				$value['loc_lat'] = $value['loc_lat'] ? $value['loc_lat'] : 0;
				$value['loc_lng'] = $value['loc_lng'] ? $value['loc_lng'] : 0;
				// $properties = $this->get_properties($value['id']);
				// $value['features'] = $properties;
				// $value['bid_deadline'] = 0;
				// $value['highest_bid'] = 0;
				// $value['highest_bidder'] = null;
				// $value['minimum_bid'] = 0;
				// $value['status'] = null;
				// $value['index'] = $key;
				//unset($features[0]);
				//unset($value['details']);
				//$value['features'] = str_replace(array('<p>', '</p>'), '', implode('\n', $features));
				$item_arr[] = $value;			
				
			}
		}
		if($item_arr)
			return array('error' => '0', 'message' => 'Success', 'data' => $item_arr);
		else 
			return array('error' => '1', 'message' => 'No matching records found');
	}

	// private function get_properties($id) {
	// 	$query = "SELECT stocks AS available_units, 
	// 				IFNULL((SELECT value FROM tbl_property_values WHERE property_value_id = property_value1_id), '') AS property1,
	// 				IFNULL((SELECT value FROM tbl_property_values WHERE property_value_id = property_value2_id), '') AS property2  
	// 			FROM tbl_property_stocks
	// 			WHERE buy_item_id = $id";
	// 	$properties = $this->db->query($query)->result_array();
	// 	return $properties;
	// }

	private function get_properties($id) {
		$query = "SELECT *  
				FROM tbl_properties
				WHERE buy_item_id = $id
				ORDER BY property_id";
		$properties = $this->db->query($query)->result_array();
		$properties_arr = array();
		if($properties) {
			foreach ($properties as $key => $value) {
				$property_values = $this->db->select("value")
											->from('tbl_property_values')
											->where('property_id', $value['property_id'])
											->get()
											->result_array();

				$property_values_arr = array();
				if($property_values) {
					foreach ($property_values as $kp => $kv) {
						$property_values_arr[] = $kv['value'];
					}
				}
				$value['property_values'] = $property_values_arr;
				$properties_arr[] = $value;
			}
		}
		return $properties_arr;
	}

	public function get_bid_items($lat, $long, $radius, $user_id) {
		//$having = $radius > 0 ? "HAVING distance <= $radius" : ""; 

		// $user_bids = $this->db->select('user_bid_id')
		// 						->from('tbl_user_bids')
		// 						->where('registrant_id', $user_id)
		// 						->get()
		// 						->result_array();
		// $user_bid_ids = array();
		// if($user_bids) {
		// 	foreach ($user_bids as $key => $value) {
		// 		$user_bid_ids[] = $value['user_bid_id'];	
		// 	}
		// }

		$bid_with_winners = $this->db->select('bid_item_id')->from('tbl_bids')->where('is_winner', 1)->get()->result_array();
		$bid_ids = array();
		if($bid_with_winners) {
			foreach ($bid_with_winners as $key => $value) {
				$bid_ids[] = $value['bid_item_id'];	
			}
		}

		$user = $this->check_user($user_id);
		if(!$user)
			return array('error' => '1', 'message' => 'Invalid uid');		

		$query = "	SELECT 
				 	bid_item_id AS id, 
				    bid_item_image AS image_url,
				    bid_item_name AS title,
				    description AS `desc`,
				    lat AS loc_lat,
				    lng AS loc_lng,
				    start_date,
				    end_date,
				    (SELECT COUNT(*) FROM tbl_bids WHERE registrant_id = $user_id) AS has_bid,
				    (SELECT ((ACOS(SIN($lat * PI() / 180) * SIN(lat * PI() / 180) + COS($lat * PI() / 180) * COS(lat * PI() / 180) * COS(($long - lng) * PI() / 180)) * 180 / PI()) * 60 * 1.1515)) AS `distance`
					FROM tbl_bid_items
					WHERE status = 1
					AND end_date >= '" . date('Y-m-d H:i:s') . "'
					AND is_deleted = 0
					AND has_ended = 0
					AND platform_restriction != 1
					AND bid_item_id NOT IN (" . implode(',', $bid_ids) . ")
					ORDER BY has_bid DESC
					";

		$items = $this->db->query($query)->result_array();
		$item_arr = array();
		if($items) {
			foreach ($items as $key => $value) {

				//$features = explode('<br />', $value['details']);
				$highest_bid = $this->get_bidder($value['id']);
				$value['image_url']	= str_replace('mobile_spice/', '' ,BASE_URL) . 'uploads/bid/' . $value['image_url'];
				$value['desc'] = strip_tags(html_entity_decode($value['desc']));
				$value['type'] = TYPE_BID;
				$value['type_string'] = TYPE_STRING_BID;
				$value['from'] = 1000 * strtotime($value['start_date']);
				$value['to'] = 1000 * strtotime($value['end_date']);
				$value['loc_lat'] = $value['loc_lat'] ? $value['loc_lat'] : 0;
				$value['loc_lng'] = $value['loc_lng'] ? $value['loc_lng'] : 0;
				$value['has_bid'] = $value['has_bid'] ? 1 : 0;
				// $value['bid_deadline'] = $value['to'];
				// $value['highest_bid'] = isset($highest_bid->bid) ? $highest_bid->bid : 0;
				// $value['highest_bidder'] = isset($highest_bid->registrant_id) ? $highest_bid->registrant_id : 0;
				// $value['status'] = null;
				// $value['index'] = $key;
				// $value['price'] = $value['highest_bid'];
				// $value['available_units'] = 0;
				//unset($features[0]);
				//$value['features'] = str_replace(array('<p>', '</p>'), '', implode('\n', $features));
				unset($value['start_date']);
				unset($value['end_date']);
				//unset($value['details']);
				$item_arr[] = $value;			
			}
		}
		if($item_arr)
			return array('error' => '0', 'message' => 'Success', 'data' => $item_arr);
		else 
			return array('error' => '1', 'message' => 'No matching records found');
	}

	public function get_bidder($id) {

		$query = "SELECT `b`.`bid`, `r`.`nick_name`, `r`.`registrant_id`, `r`.`total_points` 
				  FROM (`tbl_bids` b) 
				  LEFT JOIN `tbl_registrants` r ON `r`.`registrant_id` = `b`.`registrant_id` 
				  WHERE `bid_item_id` = $id
				  AND is_winner = 1
				  ORDER BY `bid` DESC";

		$bidder = $this->db->query($query)->row();
		if(!$bidder) {
			$query = "SELECT `b`.`bid`, `r`.`nick_name`, `r`.`registrant_id`, `r`.`total_points` 
				  FROM (`tbl_bids` b) 
				  LEFT JOIN `tbl_registrants` r ON `r`.`registrant_id` = `b`.`registrant_id` 
				  WHERE `bid_item_id` = $id
				  AND `r`.`total_points` >= b.bid
				  ORDER BY `bid` DESC";
			$bidder = $this->db->query($query)->row();
		}

		
		if(isset($bidder->bid))
			$bidder->bid = $bidder->bid;
		return $bidder;	
	}


	public function get_buy_bid_items($lat, $lng, $radius, $user_id) {
		$buy = $this->get_buy_items($lat, $lng, $radius, $user_id);
		$bid = $this->get_bid_items($lat, $lng, $radius, $user_id);
		if(!isset($buy['data']))
			$buy['data'] = array();
		if(!isset($bid['data']))
			$bid['data'] = array();
		$data = @array_merge($buy['data'], $bid['data']);
		if($data)
			return array('error' => '0', 'message' => 'Success', 'data' => $data);
		else
			return array('error' => '1', 'message' => 'No matching records found');
	}

	public function get_movefwd_items($lat, $long, $radius) {
		//$having = $radius > 0 ? "HAVING distance <= $radius" : ""; 
		$query = "	SELECT 
				 	move_forward_id AS id, 
				    move_forward_image AS image_url,
				    move_forward_title AS title,
				    move_forward_description AS `desc`,
				    lat AS loc_lat,
				    lng AS loc_lng,
				    (SELECT ((ACOS(SIN($lat * PI() / 180) * SIN(lat * PI() / 180) + COS($lat * PI() / 180) * COS(lat * PI() / 180) * COS(($long - lng) * PI() / 180)) * 180 / PI()) * 60 * 1.1515)) AS `distance`
					FROM tbl_move_forward	
					WHERE is_deleted = 0
					AND platform_restriction != 1
					";
		$items = $this->db->query($query)->result_array();
		$item_arr = array();
		if($items) {
			foreach ($items as $key => $value) {
				$value['image_url']	= str_replace('mobile_spice/', '', BASE_URL) . 'uploads/move_fwd/' . $value['image_url'];
				$value['desc'] = strip_tags(html_entity_decode($value['desc']));
				$value['type'] = TYPE_MOVEFWD;
				$value['type_string'] = TYPE_STRING_MOVEFWD;
				$value['from'] = 0;
				$value['to'] = 0;
				$value['loc_lat'] = $value['loc_lat'] ? $value['loc_lat'] : 0;
				$value['loc_lng'] = $value['loc_lng'] ? $value['loc_lng'] : 0;
				// $value['bid_deadline'] = 0;
				// $value['highest_bid'] = 0;
				// $value['highest_bidder'] = null;
				// $value['minimum_bid'] = 0;
				// $value['status'] = $this->get_movefwd_item_status(1, $value['id']);
				// $value['index'] = $key;
				// $value['available_units'] = 0;
				//$value['features'] = $this->get_challenges($value['id']);
				//$value['features'] = null;
				$item_arr[] = $value;			
			}
		}
		if($item_arr)
			return array('error' => '0', 'message' => 'Success', 'data' => $item_arr);
		else 
			return array('error' => '1', 'message' => 'No matching records found');
	}

	private function get_movefwd_item_status($user_id, $id) {
		$status = $this->db->select('*')
						   ->from('tbl_move_forward_choice')
						   ->where('move_forward_id', $id)
						   ->where('registrant_id', $user_id)
						   ->get()
						   ->row();
		$status = (array) $status;
		if(!$status)
			return 0;
		if($status['move_forward_choice'] == 2 && $status['move_forward_choice_status'] == 1)
			return 1;
		else 
			return 0;
	}

	private function is_complete_movefwd($user_id, $id) {
		
	}

	private function get_challenges($id, $user_id, &$is_completed = false) {
		$challenges = $this->db->select('challenge_id AS id, challenge AS task, type')
							   ->from('tbl_challenges')
							   ->where('move_forward_id', $id)
							   ->order_by('id')
							   ->get()
							   ->result_array();
		$challenges_arr = array();
		$approved = 0;
		if($challenges) {
			foreach ($challenges as $key => $value) {
				$count = $this->db->select('*')
						 ->from('tbl_move_forward_gallery')
						 ->where('challenge_id', $value['id'])
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row_array();
				$status = $count ? $count['mfg_status'] : 0;
				if(!$count)
					$status = 0;
				elseif($count && ($count['mfg_status'] == 0 || $count['mfg_status'] == 2))
					$status = 2;

				if($status == 1)
					$approved++;
				$value['status'] = $status;
				$challenges_arr[] = $value;
			}
			if($approved == count($challenges))
				$is_completed = true;
		}
		return $challenges_arr;
	}

	public function get_event_items($lat, $long) {
		//$having = $radius > 0 ? "HAVING distance <= $radius" : ""; 
		// $query = "	SELECT 
		// 		 	backstage_event_id AS id, 
		// 		    image AS image_url,
		// 		    title AS title,
		// 		    description AS `desc`,
		// 		    lat AS loc_lat,
		// 		    lng AS loc_lng,
		// 		    start_date, end_date,
		// 		    radius,
		// 		    is_featured AS featured,
		// 		    (SELECT ((ACOS(SIN($lat * PI() / 180) * SIN(lat * PI() / 180) + COS($lat * PI() / 180) * COS(lat * PI() / 180) * COS(($long - lng) * PI() / 180)) * 180 / PI()) * 60 * 1.1515)) AS `distance`
		// 			FROM tbl_backstage_events	
		// 			WHERE status = 1
		// 			AND is_deleted = 0
		// 			AND end_date >= '" . date('Y-m-d H:i:s') . "'
		// 			AND platform_restriction != 1
		// 			";
		$query = "	SELECT 
				 	backstage_event_id AS id, 
				    image AS image_url,
				    title AS title,
				    description AS `desc`,
				    lat AS loc_lat,
				    lng AS loc_lng,
				    start_date, end_date,
				    radius,
				    is_gallery as gallery,
				    is_featured AS featured,
				    (SELECT ((ACOS(SIN($lat * PI() / 180) * SIN(lat * PI() / 180) + COS($lat * PI() / 180) * COS(lat * PI() / 180) * COS(($long - lng) * PI() / 180)) * 180 / PI()) * 60 * 1.1515)) AS `distance`
					FROM tbl_backstage_events	
					WHERE status = 1
					AND is_deleted = 0
					AND end_date >= '" . date('Y-m-d H:i:s') . "'
					AND platform_restriction != 1
					"; 
		$items = $this->db->query($query)->result_array();
		$item_arr = array();
		if($items) {
			foreach ($items as $key => $value) {
				$value['image_url']	= str_replace('mobile_spice/', '', BASE_URL) . 'uploads/backstage/events/' . $value['image_url'];
				$value['desc'] = strip_tags(html_entity_decode($value['desc']));
				$value['type'] = TYPE_EVENT;
				$value['type_string'] = TYPE_STRING_EVENT;
				$value['from'] = 1000 * strtotime($value['start_date']);
				$value['to'] = 1000 * strtotime($value['end_date']);
				//$value['distance'];
				$value['loc_lat'] = $value['loc_lat'] ? $value['loc_lat'] : 0;
				$value['loc_lng'] = $value['loc_lng'] ? $value['loc_lng'] : 0;
				// $value['bid_deadline'] = 0;
				// $value['highest_bid'] = 0;
				// $value['highest_bidder'] = null;
				// $value['minimum_bid'] = 0;
				// $value['status'] = null;
				// $value['index'] = $key;
				// $value['price'] = 0;
				// $value['available_units'] = 0;
				// $value['features'] = null;
				unset($value['start_date']);
				unset($value['end_date']);
				$item_arr[] = $value;			
			}
		}
		if($item_arr)
			return array('error' => '0', 'message' => 'Success', 'data' => $item_arr);
		else 
			return array('error' => '1', 'message' => 'No matching records found');
	}

	public function get_buy_item($id) {
		$item = $this->db->select("buy_item_id AS id,
								  buy_item_image AS image_url,
								  buy_item_name AS title,
								  description AS `desc`,
								  lat AS loc_lat,
								  lng AS loc_lng,
								  details,
								  credit_value AS price, 
								  stock AS available_units,
								  radius")
						 ->from('tbl_buy_items')
						 ->where('buy_item_id', $id)
						 ->get()
						 ->row();
		$item = (array) $item;
		if(!$item)
			return array('error' => '1', 'message' => 'No matching records found');
		$features = explode('<br />', $item['details']);
		$item['image_url']	= str_replace('mobile_spice/', '', BASE_URL) . 'uploads/buy/' . $item['image_url'];
		$item['desc'] = strip_tags(html_entity_decode($item['desc']));
		$item['type'] = TYPE_BUY;
		$item['type_string'] = TYPE_STRING_BUY;
		$item['from'] = 0;
		$item['to'] = 0;
		$item['loc_lat'] = $item['loc_lat'] ? $item['loc_lat'] : 0;
		$item['loc_lng'] = $item['loc_lng'] ? $item['loc_lng'] : 0;
		$properties = $this->get_properties($item['id']);
		$item['features'] = $properties;
		$item['bid_deadline'] = 0;
		$item['highest_bid'] = 0;
		$item['highest_bidder'] = 0;
		$item['minimum_bid'] = 0;
		$item['status'] = 0;
		$item['index'] = 0;
		$item['property1'] = isset($properties[0]['property_name']) ? $properties[0]['property_name'] : '';
		$item['property1values'] = isset($properties[0]['property_values']) ? $properties[0]['property_values'] : array();
		$item['property2'] = isset($properties[1]['property_name']) ? $properties[1]['property_name'] : '';
		$item['property2values'] = isset($properties[1]['property_values']) ? $properties[1]['property_values'] : array();
		$item['features'] = str_replace(array('<p>', '</p>'), '', implode('\n', $features));
		unset($item['details']);
		return array('error' => '0', 'message' => 'Success', 'data' => $item);
	}

	public function get_bar_item($id) {
		$item = $this->db->select("lamp_id AS id,
								  lamp_image AS image_url,
								  lamp_name AS title,
								  description AS `desc`,
								  lat AS loc_lat,
								  lng AS loc_lng,
								  radius")
						 ->from('tbl_lamps')
						 ->where('lamp_id', $id)
						 ->get()
						 ->row();
		$item = (array) $item;
		if(!$item)
			return array('error' => '1', 'message' => 'No matching records found');
		$item['image_url']	= str_replace('mobile_spice/', '', BASE_URL) . 'uploads/reserve/' . $item['image_url'];
		$item['desc'] = strip_tags(html_entity_decode($item['desc']));
		$item['type'] = TYPE_BAR;
		$item['type_string'] = TYPE_STRING_BAR;
		$item['from'] = 0;
		$item['to'] = 0;
		$item['loc_lat'] = $item['loc_lat'] ? $item['loc_lat'] : 0;
		$item['loc_lng'] = $item['loc_lng'] ? $item['loc_lng'] : 0;
		$properties = $this->get_properties($item['id']);
		$item['features'] = $properties;
		$item['bid_deadline'] = 0;
		$item['highest_bid'] = 0;
		$item['highest_bidder'] = 0;
		$item['minimum_bid'] = 0;
		$item['status'] = 0;
		$item['index'] = 0;
		$item['available_units'] = 0;
		$item['features'] = '';
		return array('error' => '0', 'message' => 'Success', 'data' => $item);
	}

	public function get_bid_item($id) {
		$item = $this->db->select("bid_item_id AS id, 
								    bid_item_image AS image_url,
								    bid_item_name AS title,
								    description AS `desc`,
								    details,
								    lat AS loc_lat,
								    lng AS loc_lng,
								    start_date,
								    end_date,
								    starting_bid AS minimum_bid,
								    radius")
						 ->from('tbl_bid_items')
						 ->where('bid_item_id', $id)
						 ->get()
						 ->row();
		$item = (array) $item;
		if(!$item)
			return array();
		$features = explode('<br />', $item['details']);
		$highest_bid = $this->get_bidder($item['id']);
		$item['image_url']	= str_replace('mobile_spice/', '', BASE_URL) . 'uploads/bid/' . $item['image_url'];
		$item['desc'] = strip_tags(html_entity_decode($item['desc']));
		$item['type'] = TYPE_BID;
		$item['type_string'] = TYPE_STRING_BID;
		$item['from'] = 1000 * strtotime($item['start_date']);
		$item['to'] = 1000 * strtotime($item['end_date']);
		unset($item['start_date']);
		unset($item['end_date']);
		$item['loc_lat'] = $item['loc_lat'] ? $item['loc_lat'] : 0;
		$item['loc_lng'] = $item['loc_lng'] ? $item['loc_lng'] : 0;
		$item['bid_deadline'] = $item['to'];
		$item['highest_bid'] = isset($highest_bid->bid) ? $highest_bid->bid : 0;
		$item['highest_bidder'] = isset($highest_bid->registrant_id) ? $highest_bid->registrant_id : 0;
		$item['status'] = 0;
		$item['index'] = 0;
		$item['price'] = $item['highest_bid'];
		$item['available_units'] = 0;
		unset($features[0]);
		$item['features'] = str_replace(array('<p>', '</p>'), '', implode('\n', $features));
		unset($item['start_date']);
		unset($item['end_date']);
		unset($item['details']);
		return array('error' => '0', 'message' => 'Success', 'data' => $item);
	}

	public function get_movefwd_item($id, $user_id) {
		$user = $this->check_user($user_id);
		if(!$user)
			return array('error' => '1', 'message' => 'Invalid uid');				
		$item = $this->db->select("move_forward_id AS id, 
								    move_forward_image AS image_url,
								    move_forward_title AS title,
								    move_forward_description AS `desc`,
								    lat AS loc_lat,
								    lng AS loc_lng,
								    pledge_points AS price,
								    radius,
								    status")
						 ->from('tbl_move_forward')
						 ->where('move_forward_id', $id)
						 ->get()
						 ->row();
		$item = (array) $item;
		if(!$item)
			return array('error' => '1', 'message' => 'No matching records found');
		$item['image_url']	= str_replace('mobile_spice/', '', BASE_URL) . 'uploads/move_fwd/' . $item['image_url'];
		$item['desc'] = strip_tags(html_entity_decode($item['desc']));
		$item['type'] = TYPE_MOVEFWD;
		$item['type_string'] = TYPE_STRING_MOVEFWD;
		$item['loc_lat'] = $item['loc_lat'] ? $item['loc_lat'] : 0;
		$item['loc_lng'] = $item['loc_lng'] ? $item['loc_lng'] : 0;
		$item['bid_deadline'] = 0;
		$item['highest_bid'] = 0;
		$item['highest_bidder'] = 0;
		$item['minimum_bid'] = 0;
		//$item['status'] = $this->get_movefwd_item_status($user_id, $item['id']);
		$item['pledged'] = $this->get_movefwd_item_status($user_id, $item['id']);
		$item['index'] = 0;
		$item['available_units'] = 0;
		$item['features'] = '';
		$item['tasks'] = $this->get_challenges($id, $user_id, $is_completed);

		$status = 0; //not started
		if($item['status'] == 0)
			$status = 3; // inactive
		elseif($item['status'] == 1)
			$status = 1; // active
		elseif($item['pledged']  == 1 || $is_completed)
			$status = 2; // completed
		return array('error' => '0', 'message' => 'Success', 'data' => $item);
	}

	public function get_event_item($id) {
		$item = $this->db->select("backstage_event_id AS id, 
								    image AS image_url,
								    title AS title,
								    description AS `desc`,
								    lat AS loc_lat,
								    lng AS loc_lng,
								    start_date, end_date,
								    radius,
								    is_featured AS featured,
								    is_gallery, 
								    venue")
						 ->from('tbl_backstage_events')
						 ->where('backstage_event_id', $id)
						 ->get()
						 ->row();
		$item = (array) $item;
		if(!$item)
			return array('error' => '1', 'message' => 'No matching records found');
		$item['image_url']	= str_replace('mobile_spice/', '', BASE_URL) . 'uploads/backstage/events/' . $item['image_url'];
		$item['desc'] = strip_tags(html_entity_decode($item['desc']));
		$item['type'] = TYPE_EVENT;
		$item['type_string'] = TYPE_STRING_EVENT;
		$item['from'] = 1000 * strtotime($item['start_date']);
		$item['to'] = 1000 * strtotime($item['end_date']);
		$item['loc_lat'] = $item['loc_lat'] ? $item['loc_lat'] : 0;
		$item['loc_lng'] = $item['loc_lng'] ? $item['loc_lng'] : 0;
		$item['bid_deadline'] = 0;
		$item['highest_bid'] = 0;
		$item['highest_bidder'] = 0;
		$item['minimum_bid'] = 0;
		$item['status'] = 0;
		$item['index'] = 0;
		$item['price'] = 0;
		$item['available_units'] = 0;
		$item['features'] = '';
		unset($item['start_date']);
		unset($item['end_date']);
		return array('error' => '0', 'message' => 'Success', 'data' => $item);
	}

	public function get_glist_items($lat, $long, $radius) {
		//$having = $radius > 0 ? "HAVING distance <= $radius" : ""; 
		$query = "	SELECT 
				 	lamp_event_id AS id, 
				    lamp_image AS image_url,
				    event_title AS title,
				    description AS `desc`,
				    lat AS loc_lat,
				    lng AS loc_lng,
				    radius,
				    (SELECT ((ACOS(SIN($lat * PI() / 180) * SIN(lat * PI() / 180) + COS($lat * PI() / 180) * COS(lat * PI() / 180) * COS(($long - lng) * PI() / 180)) * 180 / PI()) * 60 * 1.1515)) AS `distance`
					FROM tbl_lamp_events e
					LEFT JOIN tbl_lamps l
					ON l.lamp_id = e.lamp_id
					WHERE e.status = 1
					
					";
		$items = $this->db->query($query)->result_array();
		$item_arr = array();
		if($items) {
			foreach ($items as $key => $value) {
				$value['image_url']	= str_replace('mobile_spice/', '', BASE_URL) . 'uploads/perks/' . $value['image_url'];
				$value['desc'] = strip_tags(html_entity_decode($value['desc']));
				$value['type'] = TYPE_GLIST;
				$value['type_string'] = TYPE_STRING_GLIST;
				$value['from'] = 0;
				$value['to'] = 0;
				$value['loc_lat'] = $value['loc_lat'] ? $value['loc_lat'] : 0;
				$value['loc_lng'] = $value['loc_lng'] ? $value['loc_lng'] : 0;
				// $value['bid_deadline'] = 0;
				// $value['highest_bid'] = 0;
				// $value['highest_bidder'] = null;
				// $value['minimum_bid'] = 0;
				// $value['status'] = null;
				// $value['index'] = $key;
				// $value['price'] = 0;
				// $value['available_units'] = 0;
				// $value['features'] = null;
				$item_arr[] = $value;			
			}
		}
		if($item_arr)
			return array('error' => '0', 'message' => 'Success', 'data' => $item_arr);
		else 
			return array('error' => '1', 'message' => 'No matching records found');

	}

	public function get_glist_item($id) {
		$item = $this->db->select("lamp_event_id AS id, 
								    lamp_image AS image_url,
								    event_title AS title,
								    description AS `desc`,
								    lat AS loc_lat,
								    lng AS loc_lng")
						 ->from('tbl_lamp_events')
						 ->join('tbl_lamps', 'tbl_lamps.lamp_id', 'tbl_lamp_events.lamp_id', 'left')
						 ->where('lamp_event_id', $id)
						 ->get()
						 ->row();
		$item = (array) $item;
		if(!$item)
			return array('error' => '1', 'message' => 'No matching records found');
		$item['image_url']	= str_replace('mobile_spice/', '', BASE_URL) . 'uploads/perks/' . $item['image_url'];
		$item['desc'] = strip_tags(html_entity_decode($item['desc']));
		$item['type'] = TYPE_GLIST;
		$item['type_string'] = TYPE_STRING_GLIST;
		$item['from'] = 0;
		$item['to'] = 0;
		$item['loc_lat'] = $item['loc_lat'] ? $item['loc_lat'] : 0;
		$item['loc_lng'] = $item['loc_lng'] ? $item['loc_lng'] : 0;
		return array('error' => '0', 'message' => 'Success', 'data' => $item);
	}

	public function save_glist($id, $user_id, $name) {
		$user = $this->check_user($user_id);
		if(!$user)
			return array('error' => '1', 'message' => 'Invalid uid');	
		$item = $this->db->select()
						->from('tbl_lamp_events')
						->where('lamp_event_id', $id)
						->get()
						->result_array();
		if(!$item) {
			return array('error' => '1', 'message' => 'No matching records found');	
		}

		$param['registrant_id'] = $user_id;
		$param['lamp_event_id'] = $id;
		$param['name'] = $name;
		$this->db->insert('tbl_lamp_guestlists', $param);

		return array('error' => '0', 'message' => 'Success', 'data' => 1);

	}

	private function check_user($user_id) {
		$user = $this->db->select('*')
						 ->from('tbl_registrants')
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row();
		return $user;	
	}

}