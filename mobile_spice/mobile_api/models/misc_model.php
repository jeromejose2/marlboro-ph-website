<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Misc_Model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		define('BASE_URL', base_url());
	}

	private function check_lamp($lamp_id) {
		$lamp = $this->db->select('lamp_id')
					 	 ->from('tbl_lamps')
					 	 ->where('lamp_id', $lamp_id)
					 	 ->get()
					 	 ->row();
		return $lamp;	
	}

	private function check_event($event_id) {
		$lamp = $this->db->select('backstage_event_id')
					 	 ->from('tbl_backstage_events')
					 	 ->where('backstage_event_id', $event_id)
					 	 ->get()
					 	 ->row();
		return $lamp;	
	}

	public function check_in_user($lamp_id, $user_id, $type, &$message = '') {

		$user = $this->db->select('registrant_id')
						 ->from('tbl_registrants')
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row();
		if(!$user) {
			$message = 'Invalid uid';
			return false;
		}
		if
		($type != TYPE_EVENT && $type != TYPE_BAR) {
			$message = "Invalid type";
			return false;	
		}

		if($type == 0) {
			$event = $this->db->select('*')
							  ->from('tbl_backstage_events')
							  ->where('backstage_event_id', $lamp_id)
							  ->get()
							  ->row_array();
			if(!$event) {
				$message = "Invalid id";
				return false;
			}
			if($event && strtotime($event['end_date']) < date('Y-m-d H:i:s')) {
				$message = 'This event is over';
				return false;
			}
		}

		$lamp = $this->check_lamp($lamp_id);
		$event = $this->check_event($lamp_id);
		if(!$lamp && !$event) {
			$message = 'Invalid id';
			return false;
		}

		$check_in = $this->get_checkin_count($lamp_id, false, $user_id);
		if($check_in) {
			if($check_in[0]['id'] == $lamp_id && $check_in[0]['type'] == $type) {
				$message  = "Checkin is not allowed for the same item consecutively";
				return false;
			}
			$t1 = strtotime("now");
			$t2 = strtotime ($check_in[0]['date_created']);
			$diff = abs($t1 - $t2);
			$hours = $diff / ( 60 * 60 );
			if($hours <= 3) {
				$message = "You can only check in every 3 hours";
				return false;
			}
		}

		$dateNow = date('Y-m-d H:i:s');

		/* Spice trackAction */
		$this->load->library('Spice');
		$this->spice->loginAdmin('MobilePHSysUser', 'MobilePHDefault#14');

		$section_name = $type == TYPE_EVENT ? 'CHECK_IN_EVENT' : 'CHECK_IN_LAMP';
		// $cell_id = 1848;
		// $activity_id = $type == TYPE_EVENT ? 1145 : 1150;

		$sec_settings = $this->db->select()->from('tbl_section_settings')->where('name', $section_name)->get()->row();
		$cell_id = $sec_settings->setting_section_id;

		$act_settings = $this->db->select()->from('tbl_activity_settings')->where('name', $section_name)->get()->row();
		$activity_id = $act_settings->setting_activity_id;

		$title = (array) $this->db->select()
							->from('tbl_backstage_events')
							->where('backstage_event_id', $lamp_id)
							->get()
							->row();

		if($title) {
			$track_action_data = array(
				'CellId' => $cell_id,
				'ActionList' => array(
					array(
						'ActivityId' => $activity_id,
						'ActionValue' => $title['title'] . '|' . $dateNow
					)
				)
			);

			$person = (array) $this->db->select()
								->from('tbl_registrants')
								->where('registrant_id', $user_id)
								->get()
								->row();

			if($person) {
				$response = $this->spice->trackAction($track_action_data, $person['person_id']);
				$response = $this->spice->parseJSON($response);
			}

			if($response->MessageResponseHeader->TransactionStatus != 0) {
				$message = $response->MessageResponseHeader->TransactionStatusMessage;
				return FALSE;
			}
		}
		/* Spice trackAction */

		$this->db->insert('tbl_checkins', array('id' => $lamp_id, 'registrant_id' => $user_id, 'type' => $type, 'date_created' => date('Y-m-d H:i:s')));
		return $this->get_checkin_count($lamp_id, $type, $user_id);
	}


	private function get_checkin_count($lamp_id, $type = false, $user_id) {
		if($type === false && $type !== 0) {
			$checkins = $this->db->select('checkin_id, date_created, id, type')
							 ->from('tbl_checkins')
							 ->where('registrant_id', $user_id)
							 ->order_by('checkin_id', 'DESC')
							 ->get()
							 ->result_array();
		} else {
			$checkins = $this->db->select('checkin_id')
							 ->from('tbl_checkins')
							 ->where('id', $lamp_id)
							 ->where('type', $type)
							 ->where('registrant_id', $user_id)
							 ->order_by('checkin_id', 'DESC')
							 ->get()
							 ->num_rows();
		}
		
		return $checkins;
	}

	public function get_checkedin_users($lamp_id, $type, $timestamp, &$message = '') {

		$lamp = $this->check_lamp($lamp_id);
		$event = $this->check_event($lamp_id);
		if(!$lamp && !$event) {
			$message = 'Invalid id';
			return false;
		}

		if(!$timestamp) {
			$date = date('Y-m-d');
			$checkins  = $this->db->select('c.registrant_id, first_name, third_name, current_picture AS photo_url, c.date_created AS timestamp')
							  ->from('tbl_checkins c')
							  ->join('tbl_registrants r', 'r.registrant_id = c.registrant_id', 'LEFT')
							  ->where('id', $lamp_id)
							  ->where('type', $type)
							  ->where('DATE(c.date_created)', $date)
							  ->group_by('c.registrant_id')
							  ->get()
							  ->result_array();	
		} else {
			$timestamp = $timestamp / 1000;
			$date = date('Y-m-d', $timestamp);
			$checkins  = $this->db->select('c.registrant_id, first_name, third_name, current_picture AS photo_url, c.date_created AS timestamp')
								  ->from('tbl_checkins c')
								  ->join('tbl_registrants r', 'r.registrant_id = c.registrant_id', 'LEFT')
								  ->where('id', $lamp_id)
								  ->where('DATE(c.date_created)', $date)
								  ->where('type', $type)
								  ->group_by('c.registrant_id')
								  ->get()
								  ->result_array();		
		}

		
		$items_arr = array();
		if($checkins) {
			foreach ($checkins as $key => $value) {
				$value2['photo_url'] = str_replace('mobile_spice/', '', BASE_URL) . 'uploads/profile/' . $value['registrant_id'] . '/' . $value['photo_url'];
				$value2['title'] = strtoupper($value['first_name']);
				$value2['timestamp'] = strtotime($value['timestamp']) * 1000;
				$items_arr[] = $value2;
			}
		}
		return $items_arr;
	}

	public function get_buy_photos($id, &$message = '') {
			$buy = $this->db->select('buy_item_name AS title, media_content AS photo_url')
							->from('tbl_media m')
							->join('tbl_buy_items b', 'b.buy_item_id = m.suborigin_id', 'left')
							->where('b.buy_item_id', $id)
							->where('origin_id', PERKS_BUY)
							->get()
							->result_array();
			if(!$buy) {
				$message = 'Invalid id';
				return false;
			}
			$buy_arr = array();
			if($buy) {
				foreach ($buy as $key => $value) {
					$value['photo_url'] = str_replace('mobile_spice/', '', BASE_URL) . 'uploads/buy/media/150_150_' . $value['photo_url'];
					$buy_arr[] = $value;
				}
			}

			return $buy_arr;	
	}

	public function get_bid_photos($id, &$message = '') {
			$bid = $this->db->select('bid_item_name AS title, media_content AS photo_url')
							->from('tbl_media m')
							->join('tbl_bid_items b', 'b.bid_item_id = m.suborigin_id', 'left')
							->where('b.bid_item_id', $id)
							->where('origin_id', PERKS_BUY)
							->get()
							->result_array();
			if(!$bid) {
				$message = 'Invalid id';
				return false;
			}
			$bid_arr = array();
			if($bid) {
				foreach ($bid as $key => $value) {
					$value['photo_url'] = str_replace('mobile_spice/', '', BASE_URL) . 'uploads/bid/media/150_150_' . $value['photo_url'];
					$bid_arr[] = $value;
				}
			}	
			return $bid_arr;
	}

	public function get_event_photos($id, &$message = '') {
			
			$evt = $this->db->select()
							->from('tbl_backstage_events')
							->where('backstage_event_id', $id)
							->get()
							->num_rows();
			if(!$evt) {
				$message = 'Invalid id';
				return false;
			}	

			$event = $this->db->select('title, media AS photo_url')
							->from('tbl_backstage_events_photos p')
							->join('tbl_backstage_events e', 'e.backstage_event_id = p.backstage_event_id', 'left')
							->where('p.backstage_event_id', $id)
							->where('media_type', 'photo')
							->where('p.status', '1')
							->get()
							->result_array();
			
			$event_arr = array();
			if($event) {
				foreach ($event as $key => $value) {
					$value['photo_url'] = str_replace('mobile_spice/', '', BASE_URL) . 'uploads/backstage/photos/' . $value['photo_url'];
					$event_arr[] = $value;
				}
			}	
			return $event_arr;	
	}

	public function insert_event($user_id, $id, $filename, &$message = '') {
		$user = $this->db->select('*')
						 ->from('tbl_registrants')
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row();
		if(!$user) {
			$message = 'Invalid uid';
			return false;
		}

		$event = $this->db->select('*')
						  ->from('tbl_backstage_events')
						  ->where('backstage_event_id', $id)
						  ->get()
						  ->row();
		if(!$event) {
			$message = 'Invalid id';
			return false;
		}

		$post['backstage_event_id'] = $id;
		$post['uploader_id'] = $user_id;
		$post['user_type'] = 'registrant';
		$post['uploader_name'] = $user->first_name . ' ' . $user->third_name;
		$post['media'] = $post['media_image_filename'] = $filename;
		$post['media_type'] = 'photo';
		$post['source'] = 2;
		$post['date_added'] = date('Y-m-d H:i:s');
		$this->db->insert('tbl_backstage_events_photos', $post);

		/* Spice trackAction */
		$this->load->library('Spice');
		$this->spice->loginAdmin('MobilePHSysUser', 'MobilePHDefault#14');

		// BACKSTAGE_PASS
		// $cell_id = 1848;
		// $activity_id = 1121;

		$sec_settings = $this->db->select()->from('tbl_section_settings')->where('name', 'BACKSTAGE_PASS')->get()->row();
		$cell_id = $sec_settings->setting_section_id;

		$act_settings = $this->db->select()->from('tbl_activity_settings')->where('name', 'BACKSTAGE_PASS')->get()->row();
		$activity_id = $act_settings->setting_activity_id;

		$dateTime = DateTime::createFromFormat('Y-m-d H:i:s', date("Y-m-d").' 00:00:01');
		$DateOfBirth = sprintf(
			'/Date(%s%s)/',
			$dateTime->format('U') * 1000,
			$dateTime->format('O')
		);

		$track_action_data = array(
			'CellId' => $cell_id,
			'ActionList' => array(
				array(
					'ActivityId' => $activity_id,
					'ActionValue' => $event->title . '|' . date("Y-m-d H:i:s")
				)
			)
		);

		$person = (array) $this->db->select()
						->from('tbl_registrants')
						->where('registrant_id', $user_id)
						->get()
						->row();

		if($person) {
			$response = $this->spice->trackAction($track_action_data, $person['person_id']);
			$response = $this->spice->parseJSON($response);
		}

		if($response->MessageResponseHeader->TransactionStatus != 0) {
			$message = $response->MessageResponseHeader->TransactionStatusMessage;
			return FALSE;
		}
		/* Spice trackAction */
	}

	public function insert_movefwd($user_id, $id, $filename, &$message = '') {
		$user = $this->db->select('registrant_id')
						 ->from('tbl_registrants')
						 ->where('registrant_id', $user_id)
						 ->get()
						 ->row();
		if(!$user) {
			$message = 'Invalid uid';
			return false;
		}

		$challenge = $this->db->select('*')
						  ->from('tbl_challenges')
						  ->where('challenge_id', $id)
						  ->get()
						  ->row();
		if(!$challenge) {
			$message = 'Invalid id';
			return false;
		}

		$gallery = $this->db->select('challenge_id')
						->from('tbl_move_forward_gallery')
						->where('challenge_id', $id)
						->where_in('mfg_status', array(0, 1))
						->where('registrant_id', $user_id)
						->get()
						->row();
			if($gallery) {
			$message = 'Pending or approved item exists';
			return false;
		}						

		$post['challenge_id'] = $id;
		$post['registrant_id'] = $user_id;
		$post['mfg_content'] = $filename;
		$post['source'] = 2;
		$post['mfg_date_created'] = date('Y-m-d H:i:s');
		$this->db->insert('tbl_move_forward_gallery', $post);
	}
}