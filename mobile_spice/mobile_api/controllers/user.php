<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}

	public function display_error() {
		if(!$this->login_model->check()) {
			$return = array('error' => 1,
							'message' => 'Invalid app id or secret');
			$this->output->set_content_type('application/json')->_display(json_encode($return));
			exit;
		}

		if(!$this->validate_list_input()) {
 			$return = array('error' => 1,
							'message' => strip_tags(validation_errors()));
			$this->output->set_content_type('application/json')->_display(json_encode($return));
			exit;	
 		} 
		
	}


	public function updateProfileDetails() {

		$this->display_error();

		$user_id = $this->input->post('user_id');
		$nick = $this->input->post('nickname');
		$gender = $this->input->post('gender');
		$mobile = $this->input->post('mobile');
		$street = $this->input->post('street');
		$barangay = $this->input->post('barangay');
		$province = $this->input->post('province');
		$city = $this->input->post('city');

		
		if($this->validate_form()) {
			$response = $this->user_model->update_details($user_id, $nick, $gender, $mobile, $street, $barangay, $province, $city);
			
			if($response && $response != 'Person Updated') {
				$return = array('error' => 1, 'message' => $response);
			} else {
				$return = array('error' => 0, 'message' => 'Success', 'data' => 1);
			}
		} else {
			$return = array('error' => 1, 'message' => strip_tags(validation_errors()));
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($return));	
		//$this->output->enable_profiler(TRUE);
	}

	public function getMessages() {

		$this->display_error();

		$user_id = $this->input->post('user_id');

		$messages = $this->user_model->get_messages($user_id, $message);
		if($messages === false) {
			$return = array('error' => 1, 'message' => $message);
		} else {
			$return = array('error' => 0, 'message' => 'Success', 'data' => $messages);
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($return));	
	}

	public function getNotifications() {

		$this->display_error();

		$user_id = $this->input->post('user_id');

		$messages = $this->user_model->get_notifications($user_id, $message);
		if($messages === false) {
			$return = array('error' => 1, 'message' => $message);
		} else {
			$return = array('error' => 0, 'message' => 'Success', 'data' => $messages);
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($return));	
	}

	public function readNotification() {
		$this->display_error();

		$user_id = $this->input->post('user_id');
		$id = $this->input->post('id');	
		$this->user_model->read_notification($user_id, $id, $message);
		$return = array('error' => 0, 'message' => 'Success', 'data' => 1);
		$this->output->set_content_type('application/json')->set_output(json_encode($return));	
	}

	public function getUpdates() {

		$this->display_error();

		$user_id = $this->input->post('user_id');
		$messages = $this->user_model->get_updates($user_id);
		$return = array('error' => 0, 'message' => 'Success', 'data' => $messages);
		$this->output->set_content_type('application/json')->set_output(json_encode($return));	
	}

	private function validate_form($is_update = false) {

		$this->load->library('form_validation');

		$rules = array(
			   array(
					 'field'   => 'nickname',
					 'label'   => 'Nickname',
					 'rules'   => 'required|alpha'
				  ),
			   array(
					 'field'   => 'gender',
					 'label'   => 'Gender',
					 'rules'   => 'required|numeric'
				  ),
			   array(
					 'field'   => 'mobile',
					 'label'   => 'Mobile',
					 'rules'   => 'required|numeric'
				  ),
			   array(
					 'field'   => 'street',
					 'label'   => 'Street No.',
					 'rules'   => 'required|callback_check_special'
				  ),
			   array(
					 'field'   => 'barangay',
					 'label'   => 'Barangay',
					 'rules'   => 'required|callback_check_special'
				  ),
			   array(
					 'field'   => 'province',
					 'label'   => 'Province',
					 'rules'   => 'required|callback_check_special'
				  ),
			   array(
					 'field'   => 'city',
					 'label'   => 'City',
					 'rules'   => 'required|callback_check_special'
				  )
			);	
		
 
		 $this->form_validation->set_rules($rules);
		
		 return $this->form_validation->run();
 	}

 	public function check_special($str) {
 		$pattern = '/[A-Za-z0-9(,#\s)]+/';
 		preg_match($pattern, $str, $matches);
 		if(!$matches || (isset($matches[0]) && $matches[0] != $str)) {
 			$this->form_validation->set_message('check_special', 'The %s field can not contain special characters');
 			return false;	
 		}
 		else 
 			return true;
 	}


 	public function getProfile() {

 		$this->display_error();
		 		
 		$user_id = $this->input->post('user_id');

		$userProfile = $this->user_model->getUserProfile($user_id, $message);
 		if($userProfile === false) {
			$return = array('error' => 1, 'message' => $message);
		} elseif($userProfile === 2) {
			$return = array('error' => 2, 'message' => $message);
		} else {
			if( ! is_array($userProfile)) {
				$return = array('error' => 1, 'message' => $userProfile, 'data' => NULL);
			} else {
				$return = array('error' => 0, 'message' => 'Success', 'data' => $userProfile);
			}
		}
		 
		$this->output->set_content_type('application/json')->set_output(json_encode($return));	

 	}

 	public function loginUser($is_overwrite = false) {

 		$this->display_error();
		 		
 		$username = $this->input->post('email');
		$password = $this->input->post('password');
		$device_id = $this->input->post('device_id');
		$device_name = $this->input->post('device_name');

		$login = $this->user_model->login($username, $password, $device_id, $device_name, $is_overwrite, $message);
 		if($login === false) {
			$return = array('error' => 1, 'message' => $message);
		} elseif($login === 2) {
			$return = array('error' => 2, 'message' => $message);
		} else {
			if( ! is_array($login)) {
				$return = array('error' => 1, 'message' => $login, 'data' => NULL);
			} else {
				$return = array('error' => 0, 'message' => 'Success', 'data' => $login, 'user_id' => $login['id']);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($return));	

 	}

 	// public function setActiveDevice() {
 	// 	$this->getProfile(true);
 	// }

 	public function setActiveDevice() {
 		$this->display_error();
		 		
 		$email = $this->input->post('email');
		$device_id = $this->input->post('device_id');
		$device_name = $this->input->post('device_name');

		$setActive = $this->user_model->setActiveDevice($email, $device_id, $device_name, $message);
		
		if($setActive === false) {
			$return = array('error' => 1, 'message' => $message);
		} else {
			$return = array('error' => 0, 'message' => 'Success', 'data' => $setActive);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($return));	
 	}

 	public function logOut() {
 		$this->display_error();

 		$user_id = $this->input->post('user_id');
 		$username = $this->input->post('email');
		$password = $this->input->post('password');
		$deep = $this->input->post('deep');
		
		if($deep == 0){
			$logout = $this->user_model->clearDeviceId($username, $message);
			if($logout === false) {
				$return = array('error' => 1, 'message' => $message);
			} else {
				$return = array('error' => 0, 'message' => 'Success', 'data' => 1);
			}
		}else{
			$logout = $this->user_model->logoutEndSession($user_id, $message);
			if($logout === false) {
				$return = array('error' => 1, 'message' => $message);
			} else {
				$return = array('error' => 0, 'message' => 'Success', 'data' => 1);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($return));	
 	}

 	public function bid() {

 		$this->display_error();	
 		$id = $this->input->post('id');
 		$user_id = $this->input->post('user_id');
 		$bid = $this->input->post('bid_amt');

 		$saved = $this->user_model->save_bid($user_id, $id, $bid, $message); 
 		if($saved === false) {
			$return = array('error' => 1, 'message' => $message);
		} else {
			$return = array('error' => 0, 'message' => 'Success', 'data' => $saved);
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($return));	
 	}


 	public function buy() {

 		$this->display_error();	
 		$id = $this->input->post('id');
 		$user_id = $this->input->post('user_id');
 		
 		$saved = $this->user_model->save_buy($user_id, $id, $message); 
 		if($saved === false) {
			$return = array('error' => 1, 'message' => $message);
		} else {
			$return = array('error' => 0, 'message' => 'Success', 'data' => $saved);
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($return));	
 	}

 	public function getPurchases() {
 		$this->display_error();	
 		$user_id = $this->input->post('user_id');	
 		$saved = $this->user_model->get_purchases($user_id, $message);
 		if($saved === false) {
			$return = array('error' => 1, 'message' => $message);
		} else {
			$return = array('error' => 0, 'message' => 'Success', 'data' => $saved);
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($return));		
 	}

 	public function getTasks() {
 		$this->display_error();	
 		$user_id = $this->input->post('user_id');	
 		$saved = $this->user_model->get_tasks($user_id, $message); 
 		if($saved === false) {
			$return = array('error' => 1, 'message' => $message);
		} else {
			$return = array('error' => 0, 'message' => 'Success', 'data' => $saved);
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($return));		
 	}

 	public function getCities() {
 		$this->display_error();	
 		$province_id = $this->input->post('province');
 		$return = $this->user_model->get_cities($province_id);
 		$this->output->set_content_type('application/json')->set_output(json_encode($return));	
 	}

 	public function getProvinces() {
 		$this->display_error();	
 		$return = $this->user_model->get_provinces();
 		$this->output->set_content_type('application/json')->set_output(json_encode($return));	
 	}

 	public function getMobilePrefixes() {
 		$this->display_error();	
 		$return = $this->user_model->get_mobile_prefixes();
 		$this->output->set_content_type('application/json')->set_output(json_encode($return));		
 	}

 	public function userEventSync() {
 		$this->display_error();	
 		$id = $this->input->post('id');
 		$user_id = $this->input->post('user_id');

 		$this->user_model->sync_to_calendar($id, $user_id, $message);
 		if($message) {
 			$return = array('error' => 1, 'message' => $message, 'data' => 0);
 		} else {
 			$return = array('error' => 0, 'message' => 'Success', 'data' => 1);
 		}
 		$this->output->set_content_type('application/json')->set_output(json_encode($return));		
 	}

 	private function validate_list_input($is_update = false) {

		$this->load->library('form_validation');

		$rules = array(
			   array(
					 'field'   => 'lat',
					 'label'   => 'lat',
					 'rules'   => 'decimal'
				  ),
			   array(
					 'field'   => 'lng',
					 'label'   => 'lng',
					 'rules'   => 'decimal'
				  ),
			   array(
					 'field'   => 'radius',
					 'label'   => 'radius',
					 'rules'   => 'decimal'
				  ),
			   array(
					 'field'   => 'user_id',
					 'label'   => 'user_id',
					 'rules'   => 'numeric'
				  ),
			   array(
					 'field'   => 'id',
					 'label'   => 'id',
					 'rules'   => 'numeric'
				  ),
			   array(
					 'field'   => 'name',
					 'label'   => 'name',
					 'rules'   => 'xss_clean'
				  ),
			   array(
					 'field'   => 'type',
					 'label'   => 'type',
					 'rules'   => 'numeric'
				  )
			 
			);	
		
 
		 $this->form_validation->set_rules($rules);
		
		 return $this->form_validation->run();
 	}

}

?>
