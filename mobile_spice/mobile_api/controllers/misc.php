<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Misc extends CI_Controller {

	var $_miles_to_meters;
	public function __construct()
	{
		parent::__construct();

		$this->load->model('misc_model');
		$this->_miles_to_meters = 1609.34;

	}

	public function display_error() {
		if(!$this->login_model->check()) {
			$return = array('error' => 1,
							'message' => 'Invalid app id or secret');
			$this->output->set_content_type('application/json')->_display(json_encode($return));
			exit;
		}

		if(!$this->validate_list_input()) {
 			$return = array('error' => 1,
							'message' => strip_tags(validation_errors()));
			$this->output->set_content_type('application/json')->_display(json_encode($return));
			exit;	
 		} 
		
	}

	public function checkinUserByContentId() {

		$this->display_error();

		$user_id = $this->input->post('user_id');
		$lamp_id = $this->input->post('id');
		$type = $this->input->post('type');

		$checkin = $this->misc_model->check_in_user($lamp_id, $user_id, $type, $message);
		$return = array();
		if($checkin === false) {
			$return = array('error' => 1, 'message' => $message);
		} else {
			$return = array('error' => 0, 'message' => 'Success', 'data' => $checkin);
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($return));			
	}

	public function listCheckedInUsersByContentId() {
		$this->display_error();

		$lamp_id = $this->input->post('id');
		$type = $this->input->post('type');
		$timestamp = $this->input->post('timestamp');
		
		$checkins = $this->misc_model->get_checkedin_users($lamp_id, $type, $timestamp, $message);
		if($checkins === false) {
			$return = array('error' => 1, 'message' => $message);
		} else {
			$return = array('error' => 0, 'message' => 'Success', 'data' => $checkins);
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($return));			
	}

	public function listPhotosByContentId() {

		$this->display_error();

		$type = $this->input->post('type');
		$id = $this->input->post('id');

		switch($type) {
			case TYPE_BUY: 
				$return = $this->misc_model->get_buy_photos($id, $message);
				break;
			case TYPE_BID: 
				$return = $this->misc_model->get_bid_photos($id, $message);
				break;
			case TYPE_EVENT:
				$return = $this->misc_model->get_event_photos($id, $message);
				break;
		}

		if(!isset($return) || $return === false) {
			$data = array('error' => 1, 'message' => $message);
		} else {
			$data = array('error' => 0, 'message' => 'Success', 'data' => $return);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}


	public function createMediaByContentId() {

		$this->display_error();

		$type = $this->input->post('type');
		$id = $this->input->post('id');
		$uid = $this->input->post('user_id');
		$filename = $this->input->post('media');
		
		switch($type) {
			case TYPE_EVENT:
				$return = $this->misc_model->insert_event($uid, $id, $filename, $message);
				break;
			case TYPE_MOVEFWD:
				$return = $this->misc_model->insert_movefwd($uid, $id, $filename, $message);
				break;
		}

		if($return === false) {
			$data = array('error' => 1, 'message' => $message);
		} else {
			$data = array('error' => 0, 'message' => 'Success', 'data' => $return);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}

	private function validate_list_input($is_update = false) {

		$this->load->library('form_validation');

		$rules = array(
			   array(
					 'field'   => 'lat',
					 'label'   => 'lat',
					 'rules'   => 'decimal'
				  ),
			   array(
					 'field'   => 'lng',
					 'label'   => 'lng',
					 'rules'   => 'decimal'
				  ),
			   array(
					 'field'   => 'radius',
					 'label'   => 'radius',
					 'rules'   => 'decimal'
				  ),
			   array(
					 'field'   => 'user_id',
					 'label'   => 'user_id',
					 'rules'   => 'numeric'
				  ),
			   array(
					 'field'   => 'id',
					 'label'   => 'id',
					 'rules'   => 'numeric'
				  ),
			   array(
					 'field'   => 'name',
					 'label'   => 'name',
					 'rules'   => 'xss_clean'
				  ),
			   array(
					 'field'   => 'type',
					 'label'   => 'type',
					 'rules'   => 'numeric'
				  )
			);	
		
 
		 $this->form_validation->set_rules($rules);
		
		 return $this->form_validation->run();
 	}

}

?>
