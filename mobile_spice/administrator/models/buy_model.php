<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Buy_Model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function get_user_buy($page = 1, $all = false) {
		$user = array();
		$offset = ($page - 1) * PER_PAGE;
		$where =  '';
		if(isset($_GET['search'])) {
			$where .= isset($_GET['name']) ? "HAVING CONCAT(first_name, ' ', third_name) LIKE '%$_GET[name]%'" : "";
			$where .= isset($_GET['buy_item_id']) ? " AND b.buy_item_id LIKE '%$_GET[buy_item_id]%'" : "";
			$where .= isset($_GET['from_buy']) && $_GET['from_buy'] ? " AND DATE(buy_date) >= '$_GET[from_buy]'" : "";
			$where .= isset($_GET['to_buy']) && $_GET['to_buy'] ? " AND DATE(buy_date) <= '$_GET[to_buy]'" : "";
		}
		$limit = $all ? '' : " LIMIT $offset, " . PER_PAGE;
		$query = "SELECT *, c.city as cityname, p.province as provincename, (SELECT GROUP_CONCAT(CONCAT(p.property,': ',p.value) SEPARATOR '<br>') FROM tbl_buy_properties p WHERE p.buy_id = b.buy_id) AS specs FROM tbl_buys b
				 LEFT JOIN tbl_registrants r ON r.registrant_id = b.registrant_id
				 LEFT JOIN tbl_buy_items i ON i.buy_item_id = b.buy_item_id
				 LEFT JOIN tbl_provinces p ON p.province_id = r.province
				 LEFT JOIN tbl_cities c ON c.city_id = r.city
				 $where
				$limit";
		$buys = $this->db->query($query)->result_array();
		return $buys;
	}

	public function get_user_buy_count() {
		$user = array();
		$where =  '';
		if(isset($_GET['search'])) {
			$where .= isset($_GET['name']) ? "HAVING CONCAT(first_name, ' ', third_name) LIKE '%$_GET[name]%'" : "";
			$where .= isset($_GET['buy_item_id']) ? " AND b.buy_item_id LIKE '%$_GET[buy_item_id]%'" : "";
			$where .= isset($_GET['from_buy']) && $_GET['from_buy'] ? " AND DATE(buy_date) >= '$_GET[from_buy]'" : "";
			$where .= isset($_GET['to_buy']) && $_GET['to_buy'] ? " AND DATE(buy_date) <= '$_GET[to_buy]'" : "";
		}
		$query = "SELECT *, (SELECT GROUP_CONCAT(CONCAT(p.property,': ',p.value) SEPARATOR '<br>') FROM tbl_buy_properties p WHERE p.buy_id = b.buy_id) AS specs FROM tbl_buys b
				 LEFT JOIN tbl_registrants r ON r.registrant_id = b.registrant_id
				 LEFT JOIN tbl_buy_items i ON i.buy_item_id = b.buy_item_id
				 $where";
		$buys = $this->db->query($query)->num_rows();
		return $buys;
	}
}