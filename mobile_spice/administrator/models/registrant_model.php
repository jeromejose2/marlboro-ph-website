<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrant_Model extends CI_Model {
	
	private $_table;
	public function __construct() {
		parent::__construct();
		$this->_table = 'tbl_registrants';
	}
	
	public function save_statement($is_update = false) {
		$statement = $this->input->post('statement');
		$id = $this->input->post('id');
		$post['statement'] = $statement;
		if(!$is_update) {
			$this->db->insert($this->_table, $post);
			$id = $this->db->insert_id();
		} else {
			$this->db->where('statement_id', $id)
					 ->update($this->_table, $post);
		}
		return $id;	
	}
	
	public function get_registrant($where = '', $page = 1, $per_page = PER_PAGE, &$records = 0, $all = false) {
		$user = array();
		$offset = ($page - 1) * $per_page;
		if(is_array($where)) {
			if(!$all)
				$this->db->limit($per_page, $offset);
			$user = $this->db->get_where($this->_table, $where);
			$this->db->get_where($this->_table, $where);
			$records = $this->db->count_all_results();
		} else {
			$limit = $all ? "" : "LIMIT $offset, $per_page";
			$query = "SELECT * FROM $this->_table WHERE 1 " . $where . " ORDER BY date_created DESC " . $limit;
			$user = $this->db->query($query);
			$query = "SELECT COUNT(*) AS num_rows FROM $this->_table WHERE 1 " . $where;
			$records =  $this->db->query($query)->row_array()['num_rows'];
		}
		return $user->result_array();
	}
	
	public function get_all_module() {
		$modules = $this->db->select('*')
							->order_by('group')
							->from('tbl_modules')
							->where('is_deleted', 0)
							->get();
		return $modules->result_array();
	}
	
	public function get_registrant_stats() {
		$records = array();
		$query = "SELECT COUNT(*) AS num_rows FROM tbl_registrants";
		$records['all'] =  $this->db->query($query)->row_array()['num_rows'];
		
		//$status = $this->get_status();
		// foreach($status as $k => $stat) {
		// 	$query = "SELECT COUNT(*) AS num_rows FROM tbl_registrants WHERE status = $k";
		// 	$records[$k] =  $this->db->query($query)->row_array()['num_rows'];		
		// }
		$records_temp = $this->db->select('COUNT(*) AS count, status')
								->from('tbl_registrants')
								->group_by('status')
								->get()
								->result_array();
		if($records_temp) {
			foreach($records_temp as $k => $v) {
				$records[$v['status']] = $v['count'];
			}
		}
		return $records;
	}

	public function get_profile_stats() {
		$query = "SELECT COUNT(*) AS count FROM tbl_registrants WHERE ((new_picture = '' AND picture_status = 1) OR (new_picture <> '' AND picture_status <> 1))";
		$res = $this->db->query($query)->row();
		return $res->count;

	}

	public function get_brands() {
		$param['table'] = 'tbl_brands';
		$brands = $this->global_model->get_rows($param)->result_array();
		$brands_arr = array(); 
		if($brands) {
			foreach ($brands as $key => $value) {
				$brands_arr[$value['brand_id']] = $value; 
			}
		}
		return $brands_arr;
	}

	public function get_alernate_purchases() {
		$param['table'] = 'tbl_alternate_purchase';
		$brands = $this->global_model->get_rows($param)->result_array();
		$brands_arr = array(); 
		if($brands) {
			foreach ($brands as $key => $value) {
				$brands_arr[$value['alternate_id']] = $value; 
			}
		}
		return $brands_arr;
	}

	public function get_giid_types() {
		$param['table'] = 'tbl_giid_types';
		$giid_temp = $this->global_model->get_rows($param)->result_array();
		$giid = array();
		
		if($giid_temp) {
			foreach ($giid_temp as $key => $value) {
				$giid[$value['giid_type_id']] = $value['giid_type'];
			}
		}
		return $giid;
	}
	
	public function get_status() {
		$status[VERIFIED] = 'Verified';
		$status[CSR_APPROVED] = 'CSR Approved';
		$status[PENDING_GIID] = 'Pending GIID Upload';
		$status[PENDING_CSR] = 'Pending CSR Approval';
		$status[REJECTED_NO_GIID] = 'Rejected - No GIID Upload';
		$status[REJECTED_GIID] = 'Rejected - GIID Invalid';
		$status[DEACTIVATED] = 'Deactivated';
		return $status;
	}

}