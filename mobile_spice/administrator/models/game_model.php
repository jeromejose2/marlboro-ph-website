<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Game_Model extends CI_Model {
	 
 	public function __construct()
	{
		parent::__construct(); 
	}
	
	public function get_game($where = '', $page = 1, $per_page = PER_PAGE, &$records = 0, $all = false, $order = 'date_played DESC') {
		$user = array();
		$offset = ($page - 1) * $per_page;
		if(is_array($where)) {
			if(!$all)
				$this->db->limit($per_page, $offset);
			$user = $this->db->get_where($this->_table, $where);
			
			$this->db->get_where($this->_table, $where);
			$records = $this->db->count_all_results();
		} else {
			$order = " ORDER BY " . $order;
			$limit = !$all ? " LIMIT $offset, $per_page" : ""; 
			$query = "SELECT gs.score, gs.date_played, r.first_name, r.third_name, g.name, r.registrant_id, gs.is_cheater, gs.computed_score FROM tbl_game_scores gs, tbl_games g, tbl_registrants r WHERE g.game_id = gs.game_id AND r.registrant_id = gs.registrant_id " . $where . $order . $limit;
			$user = $this->db->query($query);
			$query = "SELECT COUNT(*) AS num_rows FROM tbl_game_scores gs, tbl_games g, tbl_registrants r WHERE g.game_id = gs.game_id AND r.registrant_id = gs.registrant_id " . $where;
			$records =  $this->db->query($query)->row_array()['num_rows'];
		}
		return $user->result_array();
	}
}