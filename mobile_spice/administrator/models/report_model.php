<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_Model extends CI_Model {
	 
 	public function __construct()
	{
		parent::__construct(); 
	}
	
	public function get_page_views($section)  {
		$where['section'] = $section;
		if($this->input->get('fromdate') && $this->input->get('todate')) {
			$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
			$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
			$where['DATE(date_visited) <='] = $to;
			$where['DATE(date_visited) >='] = $from;
		}

		$views = $this->db->select('DATE(date_visited), COUNT(*)')
						  ->from('tbl_page_visists')
						  ->where($where)
						  ->group_by('DATE(date_visited)')
						  ->limit(30, 0)
						  ->order_by('date_visited')
						  ->get();
	}

	public function get_login_spread() {
		$query = "SELECT AVG(a.visits), a.daystring FROM 
					  (SELECT COUNT(*) as visits, DAYNAME(l.date_login) AS daystring 
					   FROM tbl_login l
					   GROUP BY daystring) a";
		$average_visits = $this->db->query($query)->result_array();

		$query = "SELECT AVG(a.visits), a.daystring FROM 
					  (SELECT COUNT(*) as visits, DAYNAME(l.date_login) AS daystring 
					   FROM tbl_login l
					   GROUP BY daystring, registrant_id) a";
		$average_visitor = $this->db->query($query)->result_array();
		$averages = array();
		if($average_visits) {
			foreach($average_visits as $k => $v) {
				foreach($average_visitor as $vk => $vv) {
					if($v['daystring'] == $vv['daystring']) {
						$v['visitor'] = $vv['visits'];
						$averages[] = $v;
					}
				}
			}
		}
		return $averages;

	}

	public function get_page_demographics($section, &$from = false, &$to = false) { 
		$like['section'] = $section;
		$where = array();
		if($this->input->get('fromdate') && $this->input->get('todate')) {
			$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
			$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
			$where['DATE(date_visited) <='] = $to;
			$where['DATE(date_visited) >='] = $from;
		}
		$records = $this->db->select('DATE(date_visited) AS dv, COUNT(*) AS count, (SELECT COUNT(DISTINCT(registrant_id)) FROM tbl_page_visits LEFT JOIN tbl_login ON tbl_login.login_id = tbl_page_visits.login_id WHERE DATE(date_visited) = dv AND section LIKE \'%' . $section . '%\') AS visitors')
							->from('tbl_page_visits')
							->where($where)
							->order_by('date_visited')
							->limit(30, 0)
							->like($like)
							->group_by('dv')
							->get(); 
		return $records;
	}

	public function get_move_fwd_stats() {
		$records = array();
		$query = "SELECT COUNT(*) AS num_rows FROM tbl_move_forward_gallery";
		$records['all'] =  $this->db->query($query)->row_array()['num_rows'];
		
		$records_temp = $this->db->select('COUNT(*) AS count, mfg_status')
								->from('tbl_move_forward_gallery')
								->group_by('mfg_status')
								->get()
								->result_array();
		if($records_temp) {
			foreach($records_temp as $k => $v) {
				$records[$v['mfg_status']] = $v['count'];
			}
		}
		return $records;
	}

	public function get_median() {
		$this->db->query("SET @rownum := 0;");
		$query = "SELECT
				   AVG(t.mark) AS med_points
				FROM
				(
				   SELECT
				      @rownum := @rownum + 1 AS rownum,
				      tbl_registrants.total_points AS mark
				   FROM
				      tbl_registrants
				   ORDER BY tbl_registrants.total_points
				) AS t
				WHERE
				   t.rownum IN (
				      CEIL(@rownum/2),
				      FLOOR(@rownum/2)
				   )
				";
		$res = $this->db->query($query);
		return $res->result_array();
	}
}