<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statement_Model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function save_statement($is_update = false) {
		$statement = $this->input->post('statement');
		$status = $this->input->post('status');
		$id = $this->input->post('id');
		$post['statement'] = $statement;
		$post['status'] = $status;
		if(!$is_update) {
			if($post['status'] == APPROVED)
				$post['date_published'] = date('Y-m-d H:i:s');
			else
				$post['date_unpublished'] = date('Y-m-d H:i:s');
			$this->db->insert('tbl_statements', $post);
			$id = $this->db->insert_id();
		} else {
			if($post['status'] == APPROVED) { 
				$post['date_published'] = date('Y-m-d H:i:s');
				$post['date_unpublished'] = '0000-00-00 00:00:00';
			} else {
				$post['date_unpublished'] = date('Y-m-d H:i:s');
				$post['date_published'] = '0000-00-00 00:00:00';
			}
			$this->db->where('statement_id', $id)
					 ->update('tbl_statements', $post);
		}
		return $id;	
	}
	
	public function get_statement($where = '', $page = 1, $per_page = PER_PAGE, &$records = 0, $all = false) {
		$user = array();
		$offset = ($page - 1) * $per_page;
		if(is_array($where)) {
			$where['is_deleted'] = 0;
			if(!$all)
				$this->db->limit($per_page, $offset);
			$this->db->order_by('date_created','DESC');
			$user = $this->db->get_where('tbl_statements', $where);
			$this->db->get_where('tbl_statements', $where);
			$records = $this->db->count_all_results();
		} else {
			$limit = !$all ? " ORDER BY date_created DESC LIMIT $offset, $per_page" : " ORDER BY date_created DESC";
			$query = "SELECT * FROM tbl_statements WHERE is_deleted = 0 " . $where . $limit;
			$user = $this->db->query($query);
			$query = "SELECT COUNT(*) AS num_rows FROM tbl_statements WHERE is_deleted = 0 " . $where;
			$records =  $this->db->query($query)->row_array()['num_rows'];
		}
		return $user->result_array();
	}

	public function get_user_statement($where = '', $page = 1, $per_page = PER_PAGE, &$records = 0, $all = false) {
		$user = array();
		$offset = ($page - 1) * $per_page;
		$limit = !$all ? " ORDER BY date_created DESC LIMIT $offset, $per_page" : "  ORDER BY date_created DESC ";
		$query = "SELECT us.*, r.first_name, r.third_name FROM tbl_user_statements us, tbl_registrants r WHERE r.registrant_id = us.registrant_id " . $where . $limit;
		$user = $this->db->query($query);
		$query = "SELECT COUNT(us.user_statement_id) AS num_rows FROM tbl_user_statements us, tbl_registrants r WHERE r.registrant_id = us.registrant_id " . $where;
		$records =  $this->db->query($query)->row_array()['num_rows'];
		return $user->result_array();
	}
	
	public function get_all_module() {
		$modules = $this->db->select('*')
							->order_by('group')
							->from('tbl_modules')
							->where('is_deleted', 0)
							->get();
		return $modules->result_array();
	}

}