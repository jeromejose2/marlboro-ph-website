<?php

class Arclight_Model extends CI_Model
{
	private $sftp = null;
	private $logfile;

	public function __construct()
	{
		parent::__construct();
		set_time_limit(0);
		ini_set('memory_limit', '3000M');
		error_reporting(E_ALL);
		ini_set('display_errors', true);

		$libDir = dirname(dirname(__FILE__)).'/libraries/';
		set_include_path(get_include_path().PATH_SEPARATOR.$libDir.'phpseclib0.3.5/');
		require_once $libDir.'phpseclib0.3.5/Net/SFTP.php';
		define('NET_SFTP_LOGGING', NET_SFTP_LOG_COMPLEX);
	}

	public function push()
	{
		$sqlNow = date('Y-m-d H:i:s');
		if (!is_dir('./uploads/arclight')) {
			mkdir('./uploads/arclight');
		}
		file_put_contents('./uploads/arclight/push.txt', "PUSH -- {$sqlNow}\n", FILE_APPEND);
		$this->logfile = './uploads/arclight/push.txt';
		$timeNow = explode(' ', $sqlNow);
		$now = $timeNow[0];
		// if ($timeNow[1] != '23:23') {
		// 	die('Not yet');
		// }
		list($hour, $min, $ss) = $timeNow = explode(':', $timeNow[1]);
		$timeNow = $hour.$min;

		$inputNow = $now;
		if ($this->input->get('from')) {
			$inputNow = $this->input->get('from');
		}
		$sql = "SELECT r.*, p.*, c.*, g.giid_type_code, IF(r.date_created >= '{$inputNow} 00:00:00' AND r.date_created <= '{$now} 23:59:59', 1, 0) as is_new, ".
				"IF(r.date_modified >= '{$inputNow} 00:00:00' AND r.date_modified <= '{$now} 23:23:59', 1, 0) as is_updated FROM tbl_registrants r ".
				"JOIN tbl_provinces p ON r.province = p.province_id ".
				"JOIN tbl_giid_types g ON r.government_id_type = g.giid_type_id ".
				"JOIN tbl_cities c ON r.city = c.city_id ".
				"WHERE (r.status = ".CSR_APPROVED.' OR r.status = '.ARCLIGHT_FAILED.') AND arclight_in_process = 0 '.
				"HAVING is_new OR is_updated";
		$registrants = $this->db->query($sql)->result_array();
		if (!$registrants) {
			die('No registrants candidate for arclight');
		}
		$inboundSpecialKeys = array(
			'new' => array(
				'CampaignNumber' => ARCLIGHT_INBOUND_CAMPAIGN_CODE,
				'CampaignPhase' => 'XXXA',
				'Audience' => 'XX01',
				'MediaCategory' => 'OFF',
				'OfferCategory' => '400',
				'OfferCode' => '400403'
			),
			'update' => array(
				'CampaignNumber' => ARCLIGHT_INBOUND_CAMPAIGN_CODE,
				'CampaignPhase' => 'XXXA',
				'Audience' => 'XX01',
				'MediaCategory' => 'OFF',
				'OfferCategory' => '400',
				'OfferCode' => '400403'
			)
		);
		
		$inbound = array(
			'IndividualID' => 'individual_id',
			'Prefix' => 'prefix',
			'FirstName' => 'first_name',
			'MiddleInitial' => 'middle_initial',
			'ThirdName' => 'third_name',
			'NickName' => 'nick_name',
			'Gender' => 'gender',
			'CurrentAddress' => array(
				'HouseNumber' => 'house_number',
				'StreetName' => 'street_name',
				'City' => 'city',
				'Village' => 'village',
				'District' => 'district',
				'Block' => 'block',
				'State' => 'province',
				'PostalCode' => 'zip_code',
				'Country' => 'country'
			),
			'OfficePhone' => 'office_phone',
			'OfficePhoneExtension' => 'office_phone_extension',
			'HomePhone' => 'home_phone',
			'MobilePhone' => 'mobile_phone',
			'MobileServiceProvider' => 'mobile_service_provider',
			'EmailAddress' => 'email_address',
			'DateOfBirth' => 'date_of_birth',
			'DateOfCapture' => 'date_of_capture',
			'SignatureReasonCode' => 'signature_reason_code',
			'AgeVerificationType' => 'age_verification_type',
			'GovernmentIDType' => 'giid_type_code',
			'GovernmentIDNumber' => 'government_id_number',
			'ContactStatus' => 'contact_status',
			'OptOffDirectMail' => 'opt_off_direct_mail',
			'OptOffHomePhone' => 'opt_off_home_phone',
			'OptOffMobilePhone' => 'opt_off_mobile_phone',
			'OptOffDigital' => 'opt_off_digital',
			'OptOffAllChannels' => 'opt_off_all_channels',
			'OptOffTelemarketing' => 'opt_off_telemarketing',
			'OptOffOfficePhone' => 'opt_off_office_phone',
			'DMDeliverabilityCode' => 'dm_deliverability_code',
			'HomePhoneDeliverabilityCode' => 'home_phone_deliverability_code',
			'MobilePhoneDeliverabilityCode' => 'mobile_phone_deliverability_code',
			'DigitalDeviceDeliverabilityCode' => 'digital_device_deliverability_code',
			'OfficePhoneDeliverabilityCode' => 'office_phone_deliverability_code',
			'HomePhoneDeliverabilityReason' => 'home_phone_deliverability_reason',
			'MobilePhoneDeliverabilityReason' => 'mobile_phone_deliverability_reason',
			'OfficePhoneDeliverabilityReason' => 'office_phone_deliverability_reason',
			'DigitalDeviceDeliverabilityReason' => 'digital_device_deliverability_reason',
			'DMDeliverabilityReason' => 'dm_deliverability_reason',
			'CurrentBrand' => 'current_brand_code',
			'CurrentBrandAffinity' => 'current_brand_affinity',
			'CurrentBrandFlavor' => 'current_brand_flavor',
			'CurrentBrandTarLevel' => 'current_brand_tar_level',
			'BrandBuyKind' => 'brand_buy_kind',
			'BrandPurchaseTimespan' => 'brand_purchase_timespan',
			'AlternatePurchaseIndicator' => 'alternate_purchase_indicator',
			'FirstAlternateBrand' => 'first_alternate_brand_code',
			'FirstAlternateBrandAffinity' => 'first_alternate_brand_affinity',
			'SecondAlternateBrand' => '',
			'SecondAlternateBrandAffinity' => '',
			'FirstBrandFlavor' => 'first_brand_flavor',
			'FirstBrandTarLevel' => 'first_brand_tar_level',
			'CampaignNumber' => '',
			'CampaignPhase' => '',
			'Audience' => '',
			'MediaCategory' => '',
			'OfferCategory' => '',
			'OfferCode' => '',
			'RAFIndividualID' => 'raf_individual_id',
			'EventConfirmation' => 'event_confirmation',
			'CollectionCity' => 'collection_city',
			'PromoterSignature' => 'promoter_signature',
			'PromoterSignatureDate' => 'promoter_signature_date',
			'MMSCapability' => 'mms_capability',
			'ThreeGCapability' => 'three_g_capability',
			'CollectionPlace' => 'collection_place',
			'PromoterCode' => 'promoter_code',
			'WebId' => 'registrant_id',
			'PreferredCommunicationModeCode' => '',
			'OfflineDataEntryRegistrationId' => '',
			'TransactionalCode' => ''
		);
	
		$xml = '<?xml version="1.0" encoding="utf-16" standalone="yes"?>'.
			'<InboundDMFilePH xmlns="http://pmiap-arclight.arcww2.com/Inbound/2009-01" xmlns:xs="http://www.w3.org/2001/XMLSchema">';
		$nowFilename = str_replace('-', '', $now);
		$count = 0;
		foreach ($registrants as $key => &$val) {
			$xml .= '<Record>'.
				'<VendorCode>'.ARCLIGHT_VENDOR_CODE.'</VendorCode>'.
				'<DocumentReferenceNumber></DocumentReferenceNumber>'.
				'<DateOfProcessing>'.$now.'</DateOfProcessing>'.
				'<LocalMarketCode>'.ARCLIGHT_MARKET_CODE.'</LocalMarketCode>';
			$action = 'new';
			if (!$val['is_new'] && $val['is_updated']) {
				$action = 'update';
			}
			foreach ($inbound as $xmlFieldName => $fieldDbName) {
				$xml .= '<'.$xmlFieldName.'>';
				if (isset($inboundSpecialKeys[$action][$xmlFieldName])) {
					$xml .= $inboundSpecialKeys[$action][$xmlFieldName];
				} elseif (is_array($fieldDbName)) {
					foreach ($fieldDbName as $x => $y) {
						$xml .= '<'.$x.'>';
						if (isset($val[$y])) {
							$xml .= $val[$y];
						}
						$xml .= '</'.$x.'>';
					}
				} elseif (isset($val[$fieldDbName])) {
					$xml .= $val[$fieldDbName];
				}
				$xml .= '</'.$xmlFieldName.'>';
			}
			$xml .= '</Record>';
			$count++;
			$val = $val['registrant_id'];
		}
		$xml .= '</InboundDMFilePH>';
		$xml = mb_convert_encoding($xml, ARCLIGHT_ENCODING);
		$filename = ARCLIGHT_CLIENT_CODE.'_'.ARCLIGHT_MARKET_CODE.'_'.ARCLIGHT_VENDOR_CODE.'_InboundDMFilePH_'.$nowFilename.'_'.$timeNow.'_'.$count.'_'.mb_strlen($xml).'.xml';
		if (!is_dir(ARCLIGHT_INBOUND_FILES_PATH)) {
			mkdir(ARCLIGHT_INBOUND_FILES_PATH);
		}
		file_put_contents(ARCLIGHT_INBOUND_FILES_PATH.$filename, $xml) or die("Can't create file");
		$xmlFilepath = realpath(ARCLIGHT_INBOUND_FILES_PATH.$filename);

		$sftp = $this->get_sftp_instance();

		if ($sftp->put(ARCLIGHT_INBOUND_DROPOFF_PATH.$filename, $xmlFilepath, NET_SFTP_LOCAL_FILE)) {
			$this->db->insert('tbl_arclight_inbound', array(
				'inbound_filename' => pathinfo($filename, PATHINFO_FILENAME),
				'inbound_records' => $count,
				'date_of_processing' => $sqlNow
			));
			if ($this->db->affected_rows()) {
				$this->db->where_in('registrant_id', $registrants)
					->set('arclight_in_process', 1)
					->update('tbl_registrants');
				if ($this->db->affected_rows()) {
					file_put_contents('./uploads/arclight/push.txt', $filename."\n", FILE_APPEND);
					return $filename;
				}
			}			
		}
		return false;
	}

	private function check_failed_inbound($inbound)
	{
		if ($inbound && !$inbound->inbound_failed && ($sftp = $this->get_sftp_instance())) {
			$xml = $sftp->get(ARCLIGHT_INBOUND_FAILED_PATH.$inbound->inbound_filename.'_Failed_Records.xml');
			if ($xml) {
				$xml = new SimpleXMLElement($xml);
				foreach ($xml->children() as $record) {
					$errors = array();
					foreach ($record->InboundValidationErrors->children() as $error) {
						$info = array();
						foreach ($error->attributes() as $key => $val) {
							$info[$key] = (string) $val;
						}
						$info['Message'] = (string) $error;
						$errors[] = $info;
					}
					$this->db->where('registrant_id', $record->WebId)
						->set('arclight_reject_reason', json_encode($errors))
						->set('status', ARCLIGHT_FAILED)
						->set('arclight_in_process', 0)
						->update('tbl_registrants');
				}
				$this->db->where('inbound_id', $inbound->inbound_id)
					->set('inbound_failed', 1)
					->update('tbl_arclight_inbound');
			}
		}
	}

	private function get_sftp_instance()
	{
		if (!$this->sftp) {
			$sftp = new Net_SFTP(ARCLIGHT_SERVER);
			if ($sftp->login(ARCLIGHT_USERNAME, ARCLIGHT_PASSWORD)) {
				$this->sftp = $sftp;
			} else {
				if ($this->logfile) {
					@file_put_contents($this->logfile, "SFTP Login failed\n", FILE_APPEND);
				}
				die('SFTP Login failed');
			}
		}
		return $this->sftp;
	}

	public function get_outbound_records($filename)
	{
		$sftp = $this->get_sftp_instance();
		$content = $sftp->get(ARCLIGHT_OUTBOUND_PICKUP_PATH.$filename);
		if (!$content) {
			return null;
		}
		return new SimpleXMLElement($content);
	}

	public function pull($filename, $withEdm = true)
	{
		$this->check_outbound($filename, null, $withEdm);
	}

	public function pull_with_inbound_failed($withEdm = true)
	{
		$inbound = $this->db->select()
			->from('tbl_arclight_inbound')
			->order_by('date_of_processing', 'DESC')
			->limit(1)
			->get()
			->row();
		if (!$inbound) {
			die('No uploaded inbound file');
		}

		if (!is_dir('./uploads/arclight')) {
			mkdir('./uploads/arclight');
		}
		file_put_contents('./uploads/arclight/pull.txt', $inbound->inbound_filename."\n", FILE_APPEND);
		$this->logfile = './uploads/arclight/pull.txt';

		$this->check_failed_inbound($inbound);
		$this->check_outbound(null, $inbound, $withEdm);	
	}

	private function check_outbound($outboundFilename = null, $inbound = null, $withEdm = true)
	{
		if (!$outboundFilename) {
			if (!$inbound) {
				die('Inbound record must be specified');
			}
			$outboundFilename = ARCLIGHT_CLIENT_CODE.'_'.ARCLIGHT_MARKET_CODE.'_'.ARCLIGHT_VENDOR_CODE.'_PmiPhFileEmail_'.ARCLIGHT_OUTBOUND_CAMPAIGN_CODE.'_'.date('Ymd', strtotime($inbound->date_of_processing));
			$hasOutbound = $this->db->from('tbl_arclight_outbound')
				->where('outbound_inbound_id', $inbound->inbound_id)
				->count_all_results();
			if ($hasOutbound) {
				die('Already have outbound files for '.$inbound->inbound_filename.'.xml');
			}
		} else {
			$exists = $this->db->from('tbl_arclight_outbound')
				->where('outbound_filename', $outboundFilename)
				->count_all_results();
			if ($exists) {
				die($outboundFilename.' already exists in fetched outbound files');
			}
		}

		$inboundId = $inbound && $inbound->inbound_id ? $inbound->inbound_id : 0;
		$sftp = $this->get_sftp_instance();
		$sftp->chdir(ARCLIGHT_OUTBOUND_PICKUP_PATH);
		$list = $sftp->nlist();
		if (!$list) {
			die('No outbound files');
		}

		foreach ($list as $item) {

			if (strpos($item, $outboundFilename) !== 0) {
				continue;
			}

			$xml = $sftp->get($item);
			if ($xml) {

				$regIds = array();
				$xml = new SimpleXMLElement($xml);
				foreach ($xml->children() as $record) {
					$a = (string) $record->WebId;
					$b = (string) $record->IndividualID;
					$c = (string) $record->AudienceDropDate;
					$this->db->where('registrant_id', $a)
						->set('arclight_reject_reason', null)
						->set('individual_id', $b)
						->set('arclight_date_approved', $c)
						->set('status', VERIFIED)
						->set('arclight_in_process', 0)
						->update('tbl_registrants');
					if ($this->db->affected_rows()) {
						$regIds[] = $a;
					}
				}

				if ($regIds) {

					$this->load->library('Encrypt');
					require_once 'application/libraries/Edm.php';
					$regs = $this->db->select()
						->from('tbl_registrants')
						->where_in('registrant_id', $regIds)
						->get()
						->result();

					$edm = new Edm();
					foreach ($regs as $r) {
						
						$dataForUpdate = array();
						if ($r->from_migration && !$r->encryption_changed) {
							$r->salt = generate_password(20);
							$r->password = generate_password();
							$dataForUpdate = array(
								'encryption_changed' => 1,
								'salt' => $r->salt,
								'password' => $this->encrypt->encode($r->password, $r->salt)
							);
						} else {
							$r->password = $this->encrypt->decode($r->password, $r->salt);
						}

						if ($withEdm) {
							$edm->add_recipient(EDM_MARLBORO_WELCOME_LIST_ID, $r->email_address, $r->email_address, $r->first_name, $r->third_name, $r->password);
							$sendReturn = $edm->send(EDM_MARLBORO_WELCOME_MAIL_ID, $r->email_address, $r->email_address, $r->first_name, $r->password);
							if ($sendReturn !== true) {
								$dataForUpdate['edm_error_message'] = json_encode($sendReturn);
							}
						}

						if ($dataForUpdate) {
							$this->db->where('registrant_id', $r->registrant_id);
							$this->db->update('tbl_registrants', $dataForUpdate);
						}

					}

				}

				$this->db->insert('tbl_arclight_outbound', array(
					'outbound_filename' => $item,
					'outbound_inbound_id' => $inboundId,
					'date_fetched' => date('Y-m-d H:i:s')
				));
			}
		}
	}
}