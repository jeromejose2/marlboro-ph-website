<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Move_fwd_model extends CI_Model {
	
	public function is_complete_all_activity($registrant_id, &$move_forward = 0, &$gallery = 0) {
		
		// $pledged = $this->db->select('*')
		// 					->from('tbl_move_forward_choice')
		// 					->where('move_forward_choice_status', 1)
		// 					->get()
		// 					->num_rows();


		$move_forward = $this->db->select('*')
								 ->from('tbl_move_forward')
								 ->join('tbl_challenges', 'tbl_challenges.move_forward_id = tbl_move_forward.move_forward_id')
								 ->where('is_deleted', 0)
								 ->where('status', 1)
								 ->get()
								 ->num_rows();
		
		$gallery = $this->db->select('*')
							->from('tbl_move_forward_gallery')
							->where('mfg_status', 1)
							->where('registrant_id', $registrant_id)
							->group_by('challenge_id')
							->get()
							->num_rows();
		return $move_forward == $gallery;

	}

}