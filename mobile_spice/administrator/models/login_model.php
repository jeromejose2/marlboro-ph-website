<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_Model extends CI_Model {
	 
 	public function __construct()
	{
		parent::__construct(); 
		$this->load->library('encrypt');
		//die($this->encrypt_password('p@ssw0rd123'));
		define('BASE_URL', base_url());
		define('SITE_URL', site_url());
		if($this->uri->segment(2) != 'login' && $this->uri->segment(2) != 'logout' && $this->uri->segment(2) != 'encode' && $this->uri->segment(2) != 'decode' && $this->uri->segment(1) != 'cron') {
			$this->check_user_login();
		}
		
	}
	
	# check if user is logged in
	private function check_user_login() {
		if(!$_SESSION['logged']) {
			redirect('auth/login');
		} 
		
		if(isset($_SESSION['logged'])) {
			$user = $this->extract_user_details();
			$now = time(); // or your date as well
			$date_created = strtotime($user['date_password_created']);
			$datediff = date_differ($now, $date_created);
			if($this->uri->segment(2) != 'my_account' && $datediff >= PASSWORD_EXPIRATION) {
				redirect('accounts/my_account?ref=login');
			}
			
		}
	}
	
	public function login($username, $password, &$is_blocked = false) {
		$where = array('username'		=> $username, 
						'is_deleted'	=> 0);
		$user = $this->db->select('*')
				->from('tbl_cms_users')
				->where($where)
				->get()
				->result_array();
		if(!$user)
			return false;
		if($user[0]['password'] == md5($password.$this->config->item('encryption_key'))) {
			
			$details = serialize($user[0]);
			$now = time(); // or your date as well
			$date_created = strtotime($user[0]['last_login']);
			$datediff = date_differ($now, $date_created);
			$_SESSION['logged'] = $this->encrypt->encode($details);

			$date_blocked = strtotime($user[0]['date_blocked']);
			$datediff_blocked = (BLOCK_AUTO_REMOVAL_MINUTES/60) - date_differ($now, $date_blocked);

			if($datediff < ACCOUNT_EXPIRATION && $datediff_blocked <= 0) {
				$post['last_login'] = date('Y-m-d H:i:s');
				$post['is_blocked'] = 0;
				$post['date_blocked'] = date('0000-00-00 00:00:00');
				$post['login_attempts'] = 0;
				$this->global_model->update_record('tbl_cms_users', array('cms_user_id'	=> $user[0]['cms_user_id']), $post);		
			} elseif($datediff >= ACCOUNT_EXPIRATION) {
				redirect('auth/login?ref=ae');
			}
			
			if(!$user[0]['is_blocked'])
				return true;
		} else {
			$now = time();
			$last_attempt = strtotime($user[0]['date_blocked']);
			if($user[0]['login_attempts'] + 1 >= MAX_LOGIN_ATTEMPTS && $user[0]['is_blocked'] == 0) {
				$post['is_blocked'] = 1;
				$post['date_blocked'] = date('Y-m-d H:i:s');
				$is_blocked = true;
			} 
			
			if($user[0]['is_blocked'] == 1) {
				if(date_differ($now, $last_attempt, 'i') >= BLOCK_AUTO_REMOVAL_MINUTES) {
					$post['is_blocked'] = 0;
					$is_blocked = false;
					$post['date_blocked'] = date('0000-00-00 00:00:00');
					$user[0]['login_attempts'] = 0;
				} else {
					$is_blocked = true;
				}
			}
			$post['login_attempts'] = $user[0]['login_attempts'] + 1;
			$this->global_model->update_record('tbl_cms_users', array('username'	=> $username), $post);
		}
			
		return false;
	}

	public function get_user_by_email($email) {
		$user = $this->db->select('*')
						->from('tbl_cms_users')
						->where('username', $email)
						->get()
						->row();
		return (array)$user;
	}
	
	public function extract_user_details() {
		if(isset($_SESSION['logged'])) {
			$user_details = $this->encrypt->decode($_SESSION['logged']);
			return unserialize($user_details);
		} else 
			return false;
	}
	
	
	public function encrypt_password($password) {
		return md5($password . $this->config->item('encryption_key'));
	} 
	
	public function logout() {
		session_destroy();
	}
	
	public function is_used_password($id, $password) {
		$limit = 12;
		$passwords = $this->db->select('*')
				 ->from('tbl_user_passwords')
				 ->where('cms_user_id', $id)
				 ->limit(12)
				 ->order_by('date_created', 'desc')
				 ->get()
				 ->result_array();
		if($passwords) {
			foreach($passwords as $k => $v) {
				
				if($v['password'] == $this->encrypt_password($password)) {
					return true;
				}	
			}
			return false;
		}
		return false;
		
	}
	
	
	public function update_account($post, $id) {
		$now = date('Y-m-d H:i:s');
		if(isset($post['password']))
			$post['date_password_created'] = $now;
		$this->db->where('cms_user_id', $id);
		$this->db->update('tbl_cms_users', $post);
		
		if(isset($post['password'])) {
			$params['cms_user_id'] = $id;
			$params['password'] = $post['password'];
			$params['date_created'] = $now;
			$this->db->insert('tbl_user_passwords', $params);
		}
	}
	
	
	
	
	
}