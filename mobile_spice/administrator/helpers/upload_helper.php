<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function upload($param)
{	
	$config['upload_path'] = $param['upload_path'];
	$config['allowed_types'] = $param['allowed_types'];
	$config['overwrite'] = TRUE;
	$config['file_name'] = $param['file_name'];
	$config['max_size']	= $param['max_size']	; # 2MB
	$config['max_width']  = $param['max_width'];
	$config['max_height']  = $param['max_height'];
	
	$CI =& get_instance();
	$CI->load->library('upload', $config);
	
	if($CI->upload->do_upload($param['do_upload'])){
		
		return $CI->upload->data();
		
 	}else{

		return $CI->upload->display_errors();
		
	}
	
}
