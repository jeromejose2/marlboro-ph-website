<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function paging($total_rows,$limit,$offset=0,$base_url)
{
	
	$CI =& get_instance();
	$CI->load->library('pagination');
 	$config['base_url'] = $base_url;
	$config['per_page'] = $limit;
	$config['total_rows'] =  $total_rows;
	$config['num_links'] = 8;
	$config['enable_query_strings'] = TRUE;
	$config['page_query_string'] = TRUE;
	$config['prev_link'] = 'Prev';
	$config['prev_tag_open'] ='<li>';
	$config['prev_tag_close'] = '</li>';
	$config['next_link'] = 'Next';
	$config['next_tag_open'] = '<li>';
	$config['next_tag_close'] = '</li>';
	$config['num_tag_open'] = '<li>';
	$config['num_tag_close'] = '</li>';
	$config['cur_tag_open'] = '<li class="active"><a href="">';
	$config['cur_tag_close'] = '</a></li>';
	$config['last_tag_open'] = '<li>';
	$config['last_tag_close'] = '</li>';
	$config['first_tag_open'] = '<li>';
	$config['first_tag_close'] = '</li>';
	$CI->pagination->initialize($config);
	$pagination = $CI->pagination->create_links();
	return $pagination;
	
}