<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrant extends CI_Controller {
	
	private $_user;
	public function __construct() {
		parent::__construct();
		$this->load->model('registrant_model');
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {

		$where = ' AND is_cm = 0';
		if(isset($_GET['search'])) {
			$where =  " AND CONCAT(first_name, ' ', third_name) LIKE '%$_GET[name]%'";
			$where .=  " AND email_address LIKE '%$_GET[email]%'";
			$where .=  " AND referral_code LIKE '%$_GET[referral_code]%'";
			//$where .=  " AND tracking_code LIKE '%$_GET[tracking_code]%'";
			$where .=  " AND outlet_code LIKE '%$_GET[outlet_code]%'";
			$where .= $_GET['from'] ? " AND DATE(date_created) >= '$_GET[from]'" : "";
			$where .= $_GET['to'] ? " AND DATE(date_created) <= '$_GET[to]'" : "";
			$where .= $_GET['status'] != '' ? " AND status = '$_GET[status]'" : "";
		}

		$data['users'] = $this->registrant_model->get_registrant($where, $this->uri->segment(2, 1), PER_PAGE, $records);
		$page = $this->uri->segment(2, 1);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/registrant', PER_PAGE);
		$data['provinces'] = $this->global_model->get_provinces();
		$data['brands'] = $this->registrant_model->get_brands();
		$data['alternate'] = $this->registrant_model->get_alernate_purchases();
		$data['offset'] = ($page - 1) * PER_PAGE;
		$access = $this->module_model->check_access('registrant');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['status'] = $this->global_model->get_status();
		$data['total'] = $records;
		$data['giid'] = $this->registrant_model->get_giid_types();
		$this->load->library('Encrypt');
		return $this->load->view('registrant/index', $data, true);		
	}
	
	private function validate_fields() {
		$email = $this->input->post('username');
		$id = $this->input->post('id');
		$where['username'] = $email;
		$user = $this->user_model->get_user($where);
		if($user) {
			if($id && $user[0]['cms_user_id'] == $id)
				return true;
			return false;
		}
		
		return true;
	}
	
	
	public function approve() {
		
		$table = 'tbl_registrants';
		$id = $this->uri->segment(3);
		$field = 'registrant_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/registrant') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->update_status($id, CSR_APPROVED, 'approved a registrant GIID upload');

			$user = $this->db->select()
				->from($table)
				->where($field, $id)
				->limit(1)
				->get()
				->row();
			if ($user && $user->status == ACCESS_GRANTED_STATUS) {
				require_once 'application/libraries/Edm.php';
				require_once 'application/helpers/site_helper.php';
				$edm = new Edm();
				$email = $user->email_address;
				$username = $user->email_address;
				$firstname = $user->first_name;
				$lastname = $user->third_name;
				$this->load->library('Encrypt');
<<<<<<< HEAD
				if ($user->from_migration && !$user->encryption_changed) {
=======

				$individual_id = $this->global_model->get_row(array('table' => 'tbl_individual_id'));

				//if ($user->from_migration && !$user->encryption_changed) {
>>>>>>> master
					$salt = generate_password(20);
					$password = generate_password();
					$encoded_password = $this->encrypt->encode($password, $salt);
					$this->global_model->update_record('tbl_registrants', ' WHERE registrant_id = ' . $user->registrant_id, array(
						'encryption_changed' => 1,
						'salt' => $salt,
						'password' => $encoded_password,
						'individual_id' => $individual_id->individual_id + 1
					));
					// $this->db->where('registrant_id', $user->registrant_id);
					// $this->db->update('tbl_registrants', array(
					// 	'encryption_changed' => 1,
					// 	'salt' => $salt,
					// 	'password' => $encoded_password,
					// 	'individual_id' => $individual_id->individual_id;
					// ));
					// $user->salt = $salt;
					// $user->password = $encoded_password;

					$this->global_model->update_record('tbl_individual_id', '', array(
						'individual_id' => $individual_id->individual_id + 1
					));
<<<<<<< HEAD
					$user->salt = $salt;
					$user->password = $encoded_password;
				}
				$password = $this->encrypt->decode($user->password, $user->salt);
=======

				//}
				//$password = $this->encrypt->decode($encoded_password, $user->salt);


>>>>>>> master

				$edm->add_recipient(EDM_MARLBORO_WELCOME_LIST_ID, $email, $username, $firstname, $lastname, $password);
				$edm->send(EDM_MARLBORO_WELCOME_MAIL_ID, $email, $username, $firstname, $password);
			}
			
		}
		redirect('registrant');
	}
	
	public function disapprove() {
		$table = 'tbl_registrants';
		$id = $this->uri->segment(3);
		$field = 'registrant_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/registrant') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->update_status($id, REJECTED_GIID, 'rejected a registrant GIID upload', $this->input->post('message'));
			
		}
		redirect('registrant');
	}
	
	public function activate() {
		
		$table = 'tbl_registrants';
		$id = $this->uri->segment(3);
		$field = 'registrant_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/registrant') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->update_status($id, VERIFIED, 'activated a registrant account');
			
		}
		redirect('registrant');
	}
	
	public function deactivate() {
		$table = 'tbl_registrants';
		$id = $this->uri->segment(3);
		$field = 'registrant_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/registrant') !== false && $token == md5($id . ' ' . $this->config->item('encryption_key'))) {
			$this->update_status($id, DEACTIVATED, 'deactivated a registrant account', $this->input->post('message'));
		}
		redirect('registrant');
	}
	
	public function edit() {
		
		$table = 'tbl_registrants';
		$id = $this->uri->segment(3);
		$field = 'registrant_id';
		$token = $this->uri->segment(4);
		$new_content = array();
		$old_content = array();
		if((strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/registrant') !== false || strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/profile') !== false) && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$where[$field] = $id;
			$user = $this->registrant_model->get_registrant($where)[0];
			if(!$user)
				redirect('registrant');

			$set = $this->input->post();
			
			if(!$this->check_email($set['email_address'], $id)) {
				$data['error'] = 'Sorry, the email address you entered is already in use';
				$this->load->view('registrant/edit', $data, false);
				return;	
			}

			if($_FILES['giid']['name']) {
				$file_name = $this->upload_giid($id, $error);
				if($error) {
					$data['error'] = $error;
					$this->load->view('registrant/edit', $data, false);
					return;
				}
						
				$set['giid_file'] = $file_name;	
			}
			
			/*$set['street_name'] = $this->input->post('street_name');
			$set['barangay'] = $this->input->post('barangay');
			$set['city'] = $this->input->post('city');
			$set['province'] = $this->input->post('province');
			$set['zip_code'] = $this->input->post('zip_code');*/
			
			unset($set['id']);
			unset($set['submit']);
			$this->global_model->update_record($table, $where, $set);
			
			
			$fields = array('street_name', 'barangay', 'city', 'province', 'zip_code');
			foreach($user as $k => $v) {
				if(in_array($k, $fields)) {
					if($user[$k] != $this->input->post($k)) {
						$new_content[$k] = $this->input->post($k);
						$old_content[$k] = $user[$k];
					}
				}
			}
			
			$post['url'] = SITE_URL . '/statement/edit/' . $id;
			$post['description'] = 'updated a registrant account';
			$post['table'] = 'tbl_registrants';
			$post['record_id'] = $id;
			$post['type'] = 'edit';
			$post['field_changes'] = serialize(array('old'	=> $old_content,
											  		  'new'		=> $new_content));
			$this->module_model->save_audit_trail($post);
			
		}
		$this->load->view('registrant/edit', null, false);	
		//redirect('registrant');
	}

	private function check_email($email, $id) {
		$param['where'] = array('email_address'	=> $email, 'registrant_id !='	=> $id);
		$param['table'] = 'tbl_registrants';
		$registrant = $this->global_model->get_row($param);
		return !$registrant;
	}
	
	private function update_status($id, $new_status, $action_text = '', $message = '') {
		$where['registrant_id'] = $id;
		$table = 'tbl_registrants';
		$this->_user = $this->registrant_model->get_registrant($where)[0];
		if(!$this->_user)
			redirect('registrant');
		
		#update status
		$set['status'] = $new_status;
		if($new_status == CSR_APPROVED) 
			$set['csr_date_approved'] = date('Y-m-d H:i:s');
		$this->global_model->update_record($table, $where, $set);
		
		#save audit trail
		$status = $this->global_model->get_status();
		$new_content['status'] = $status[$new_status];
		$old_content['status'] = $status[$this->_user['status']];
		$post['url'] = @$_SERVER['HTTP_REFERER'];
		$post['description'] = $action_text;
		$post['table'] = $table;
		$post['record_id'] = $id;
		$post['message'] = $message;
		$post['field_changes'] = serialize(array('old'	=> $old_content,
												  'new'	=> $new_content));
		$this->module_model->save_audit_trail($post);	
	}

	private function upload_giid($id, &$error = false) {
		$path = 'uploads/profile/' . $id. '/giid/';
		$config = array();
		$config['upload_path'] = $path;
		$config['allowed_types'] = GIID_FILE_TYPES;
		$config['max_size'] = '2048';
		$config['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['giid']['name'], PATHINFO_EXTENSION);

		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('giid')) {
			$error =  $this->upload->display_errors();
			return;
		}
		return $config['file_name'];
	}

	private function send_tracking_code($user) {
		include_once('application/libraries/Edm.php');
		include_once('application/helpers/site_helper.php');

		$edm = new Edm();
		$tracking_code = unique_code();
		$email = $user['email_address'];
		$firstname = $user['first_name'];
		$lastname = $user['third_name'];

		$where = array('registrant_id'	=> $user['registrant_id']);
		$set = array('tracking_code'	=> $tracking_code, 'date_tracking_code_created'	=> date('Y-m-d H:i:s'));
		$this->global_model->update_record('tbl_registrants', $where, $set);
		
		$edm->add_recipient_for_tracking_code(EDM_MARLBORO_REGISTER_LIST_ID, $email, $firstname, $lastname, $tracking_code);
		$edm->send_tracking_code(EDM_MARLBORO_REGISTER_MAIL_ID, $email, $firstname, $tracking_code);
	}

	public function generate_code($id) {
		$token = $this->uri->segment(4);
		if((strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/registrant') !== false || strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/profile') !== false) && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$param['where'] = array('registrant_id'	=> $id);
			$param['table'] = 'tbl_registrants';
			$registrant = (array)$this->global_model->get_row($param);	

			$this->send_tracking_code($registrant);
			if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/registrant') !== false)
				redirect('registrant?ref=code');
			else
				redirect('profile?ref=code');

		}
	}

	public function set_cm($id) {
		$token = $this->uri->segment(4);
		if((strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/registrant') !== false || strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/profile') !== false) && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$set['is_cm'] = 1;
			$where['registrant_id'] = $id;
			$this->global_model->update_record('tbl_registrants', $where, $set);
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	public function get_reason() {
		$id = $this->input->post('id');
		$log = $this->module_model->get_audit_trail("AND `table` = 'tbl_registrants' AND record_id = " . $id);
		$data['main_content'] = isset($log[0]['message']) && $log[0]['message'] ? $log[0]['message'] : 'No reason specified';
		$this->load->view('blank-template', $data);
	}

	public function get_registrant_details($id) {
		$param['table'] = 'tbl_registrants';
		$param['where'] = array('registrant_id' => $id);
		$registrant = (array) $this->global_model->get_row($param);
		$data['v'] = $registrant;
		$data['giid'] = $this->registrant_model->get_giid_types();
		$data['provinces'] = $this->global_model->get_provinces();
		$data['brands'] = $this->registrant_model->get_brands();
		$data['alternate'] = $this->registrant_model->get_alernate_purchases();
		$data['city'] = $this->get_city($data['v']['city']);
		$this->load->view('registrant/view', $data);
	}

	private function get_city($id) {
		//die('adf' . $id);
		$param['table'] = 'tbl_cities';
		$param['where']  = array('city_id'	=> $id);
		$city = (array)$this->global_model->get_row($param);
		return $city['city'];
	}

	public function in_process_verify_all()
	{
		set_time_limit(0);
		ini_set('memory_limit', '3000M');
		error_reporting(E_ALL);
		ini_set('display_errors', "1");
		$users = $this->db->select()
				->from('tbl_registrants')
				->where('arclight_in_process', 1)
				->where('status <>', 1)
				->where('password !=', '')
				->get()
				->result();
		$this->load->library('Encrypt');
		include_once('application/libraries/Edm.php');
		$edm = new Edm();
		if($users) {
			foreach ($users as $k => $user) {
				$password = $this->encrypt->decode($user->password, $user->salt);
				$update = array();
				
				$edm->add_recipient(EDM_MARLBORO_WELCOME_LIST_ID, $user->email_address, $user->email_address, $user->first_name, $user->third_name, $password);
				$sendReturn = $edm->send(EDM_MARLBORO_WELCOME_MAIL_ID, $user->email_address, $user->email_address, $user->first_name, $password);
				$sendReturn = true;
				if ($sendReturn !== true) {
					$update['edm_error_message'] = json_encode($sendReturn);
					$update['edm_status'] = 2;
				} else {
					$update['edm_error_message'] = null;
					$update['edm_status'] = 1;
					$update['status'] = ACCESS_GRANTED_STATUS;
				}
				
				$this->db->where('registrant_id', $user->registrant_id);
				$this->db->update('tbl_registrants', $update);
				echo $user->email_address.' - '.$update['edm_error_message']."<br>";
			}
		}
	}


	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete();
		elseif($method == 'approve')
			$this->approve();
		elseif($method == 'disapprove')
			$this->disapprove();
		elseif($method == 'add')
			$this->add();
		elseif($method == 'activate')
			$this->activate();
		elseif($method == 'deactivate')
			$this->deactivate();
		elseif($method == 'generate_code')
			$this->generate_code($this->uri->segment(3));
		elseif($method == 'get_reason')
			$this->get_reason();
		elseif($method == 'set_cm')
			$this->set_cm($this->uri->segment(3));
		elseif($method == 'get_registrant_details')
			$this->get_registrant_details($this->uri->segment(3));
<<<<<<< HEAD
=======
		elseif($method == 'ref')
			$this->ref();
		elseif($method == 'password_gen')
			$this->password_gen();
		elseif($method == 'in_process_verify_all')
			$this->in_process_verify_all();
>>>>>>> master
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
