<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Video_duration extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('video_helper');
	}

	public function index() {
		$videos = $this->db->select('*')
				 ->from('tbl_dbam')
				 ->where('dbam_type', 2)
				 ->get()
				 ->result_array();

		if($videos) {
			foreach($videos as $video) {
				$this->db->where('dbam_id', $video['dbam_id']);
				$set['dbam_duration'] = get_video_duration('/home/webuser/www/marlboro-nuworks/uploads/dbam/' . $video['dbam_content']);
				$this->db->update('tbl_dbam', $set);
				echo $video['dbam_content'] . ' ' . $this->db->last_query() . '<br>';
			}
		}
	}


	public function videos() {
		$videos = $this->db->select('*')
				 ->from('tbl_backstage_events_photos')
				 ->where('media_type', 'video')
				 ->where('status', 1)
				 ->get()
				 ->result_array();

		if($videos) {
			foreach($videos as $video) {
				$this->db->where('photo_id', $video['photo_id']);
				$set['media_duration'] = get_video_duration('/home/webuser/www/marlboro-nuworks/uploads/backstage/photos/' . $video['media']);
				$this->db->update('tbl_backstage_events_photos', $set);
				echo $video['media'] . ' ' . $this->db->last_query() . '<br>';
			}
		}
	}
		
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */