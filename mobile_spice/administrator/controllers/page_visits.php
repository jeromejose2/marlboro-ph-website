<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page_visits extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('report_model');
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	public function export()
	{
		$this->load->library('to_excel_array');
		$records = $this->report_model->get_page_demographics('', $from, $to)->result_array();
		$row[] = $from && $to ? array('Page Visits (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')') : array('Page Visits');

		$row[] = array('Day', 'Visits', 'Visitors');
		if($records) {
			foreach($records as $k => $v) {
				$row[] = array(date('F d, Y', strtotime($v['dv'])),
							   $v['count'],
							   $v['visitors']);
			}
		}
		$this->to_excel_array->to_excel($row, 'game_visits_'.date("YmdHis"));
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$data['records'] = $this->report_model->get_page_demographics('')->result_array();
		return $this->load->view('visits/page', $data, true);		
	}

	// private function get_demographics(&$from = false, &$to = false) { 
	// 	$like['section'] = '';
	// 	$where = array();
	// 	if($this->input->get('fromdate') && $this->input->get('todate')) {
	// 		$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
	// 		$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
	// 		$where['DATE(date_visited) <='] = $to;
	// 		$where['DATE(date_visited) >='] = $from;
	// 	}
	// 	$param['like'] = $like;
	// 	$param['fields'] = 'DATE(date_visited) AS dv, COUNT(*) AS count, (SELECT COUNT(*) FROM tbl_page_visits WHERE DATE(date_visited) = dv GROUP BY login_id LIMIT 1) AS visitors';
	// 	$param['where'] = $where;
	// 	$param['join'] = array('tbl_login' => 'tbl_login.login_id = tbl_page_visits.login_id')
	// 	$param['table'] = 'tbl_page_visits';
	// 	$param['order_by'] = array('field'	=> 'date_visited', 'order'	=> 'ASC');
	// 	$param['group_by'] = 'dv';
	// 	$param['offset'] = 0;
	// 	$param['limit'] = 30;
	// 	$records = $this->global_model->get_rows($param)->result_array();
	// 	return $records;
	// }
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */