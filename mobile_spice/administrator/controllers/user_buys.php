<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_buys extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('buy_model');
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	} 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$data['users'] = $this->buy_model->get_user_buy($page);
		$records = $this->buy_model->get_user_buy_count();
		$data['offset'] = ($page - 1) * PER_PAGE;
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/user_buys');
		$access = $this->module_model->check_access('user_buys');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = $records;
		$data['items'] = $this->get_buy_items();
		return $this->load->view('perks/buy/user', $data, true);		
	}

	private function get_buy_items() {
		$param['table'] = 'tbl_buy_items';
		$param['order_by'] = array('field'	=> 'buy_item_name', 'order'	=> 'ASC');
		$param['where'] = array('is_deleted'	=> 0, 'status'	=> 1);
		$items = $this->global_model->get_rows($param)->result_array();
		return $items;
	}

	public function export()
	{
		$like = array();
		$where = array();
		$this->load->library('to_excel_array');
		$records = $this->buy_model->get_user_buy(0, true);

		$row[] = array('Name', 
					   'Street',
					   'Barangay',
					   'City',
					   'Province',
					   'Mobile',
					   'Item',
					   'Properties',
					   'Date');
		if($records) {
			foreach($records as $k => $v) {
			// 	$row[] = array($v['first_name'] . ' ' . $v['third_name'],
			// 				  $v['buy_item_name'],
			// 				  $v['specs']);
				$row[] = array($v['first_name'] . ' ' . $v['third_name'], $v['street_name'], $v['barangay'], $v['cityname'], $v['provincename'], $v['mobile_phone'], $v['buy_item_name'], str_replace('<br>', "\n", $v['specs']), $v['buy_date']);
			}
		}
		$this->to_excel_array->to_excel($row, 'user_buys_'.date("YmdHis"));
	}

	private function unset_val(&$arr, $exclude) {
		foreach ($arr as $key => $value) {
			if(in_array($key, $exclude)) {
				unset($arr[$key]);
			}
		}
	} 
	
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete();
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */