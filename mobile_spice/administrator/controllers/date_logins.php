<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Date_logins extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	public function export()
	{
		$this->load->library('to_excel_array');
		$records = $this->get_demographics($from, $to);
		$row[] = $from && $to ? array('Login Spread (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')') : array('Login Spread');

		$row[] = array('Date', 'Visits', 'Visitors');
		if($records) {
			foreach($records as $k => $v) {
				$row[] = array(
							  $v['tlog'],
							  $v['count'],
							  $v['visitors']);
			}
		}
		$this->to_excel_array->to_excel($row, 'date_logins_'.date("YmdHis"));
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$data['records'] = $this->get_demographics();
		return $this->load->view('visits/date_login', $data, true);		
	}

	private function get_demographics(&$from = false, &$to = false) {
		$like = array();
		$where = array();
		if($this->input->get('fromdate') && $this->input->get('todate')) {
			$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
			$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
			$where['DATE(date_login) <='] = $to;
			$where['DATE(date_login) >='] = $from;
		}
		
		$param['like'] = $like;
		$param['fields'] = 'DATE(date_login) AS tlog, COUNT(*) AS count, COUNT(DISTINCT(registrant_id)) AS visitors';
		$param['where'] = $where;
		$param['table'] = 'tbl_login';
		$param['order_by'] = array('field'	=> 'tlog', 'order'	=> 'ASC');
		$param['group_by'] = 'tlog';
		$param['offset'] = 0;
		$param['limit'] = 30;
		$records = $this->global_model->get_rows($param)->result_array();	
		return $records;
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */