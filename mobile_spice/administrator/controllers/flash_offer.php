<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Flash_offer extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		$this->load->model('module_model');
		$this->_table = 'tbl_flash_offers';
		$this->load->helper('file');

	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
 		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		$where['is_deleted'] = 0;
		if(isset($_GET['search'])) {
			$like = $this->input->get();

			if($like['fromdate']) {
				$where['start_date >='] = $like['fromdate'];
 			}

			if($like['todate']) {
 				$where['end_date <='] = $like['todate'];
			}


			unset($like['search']);
			unset($like['fromdate']);
			unset($like['todate']);
		}
		$param['like'] = $like;
		$param['limit'] = PER_PAGE;
		$param['offset'] = $data['offset'];
		$param['table'] = $this->_table;
		$param['where'] = $where;
		$param['order_by'] = array('field'=>'date_created','order'=>'DESC');
		$data['categories'] = $this->global_model->get_rows($param)->result_array();
		$records = $this->global_model->get_total_rows($param);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/flash_offer',PER_PAGE);

		$access = $this->module_model->check_access('birthday_offer');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = $records;
		return $this->load->view('flash_offer/index', $data, true);		
	}
	
	public function add() {
		$data['main_content'] = $this->add_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function add_content() {
		$error = '';
		if($this->input->post('submit')) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules($this->get_rules());
			$valid = $this->form_validation->run();
			if($valid) {
				$post = $this->input->post();
				$user = $this->login_model->extract_user_details();
				$post['created_by'] = $user['cms_user_id'];
				$post['media'] = '';
				if($_FILES['photo']['name'] && $post['type'] == 'Photo')
					$post['media'] = $this->upload_image($error);
				if($_FILES['video']['name'] && $post['type'] == 'Video')
					$post['media'] = $this->upload_video($error);
				if(!$error) {
					$id = $this->global_model->insert($this->_table, $post);	
					$post = array();
					$post['url'] = SITE_URL . '/flash_offer/add';
					$post['description'] = 'added a new flash offer';
					$post['table'] = $this->_table;
					$post['record_id'] = $id;
					$post['type'] = 'add';
					$this->module_model->save_audit_trail($post);

					redirect('flash_offer');	
				}
				
			} else {
				$error = validation_errors();
			}
		}
		$data['error'] = $error;
		$data['record'] = $_POST;
		$data['origins'] = $this->module_model->get_origins();
		return $this->load->view('flash_offer/add', $data, true);			
	}
	
	public function edit() {
		$id = $this->uri->segment(3);
		$data['main_content'] = $this->edit_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function edit_content($id) {
		$error = '';
		$param['table'] = $this->_table;
		$param['where'] = array('prize_id'=> $id);
		$record = (array)$this->global_model->get_row($param);
		$data['record'] = $_POST ? $_POST : $record;
		if($this->input->post('submit')) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules($this->get_rules());
			$valid = $this->form_validation->run();
			if($valid) {
				$post = $this->input->post();
				$user = $this->login_model->extract_user_details();
				if($_FILES['photo']['name'] && $post['type'] == 'Photo')
					$post['media'] = $this->upload_image($error);
				if($_FILES['video']['name'] && $post['type'] == 'Video')
					$post['media'] = $this->upload_video($error);
				if(!$error) {
					$this->global_model->update($this->_table, $post, array('prize_id'	=> $id));	
					
					$fields = array('prize_name', 'description', 'prize_image', 'status', 'send_type', 'redemption_address', 'stock');
					$status = array('Unpublished', 'Published');
					$origins = $this->module_model->get_origins();
					foreach($record as $k => $v) {
						if(in_array($k, $fields)) {
							if($record[$k] != $this->input->post($k)) {
								if($k == 'status') {
									$new_content[$k] = $status[$this->input->post($k)];
									$old_content[$k] = $status[$record[$k]];
								} elseif($k == 'origin_id') {
									$new_content[$k] = $origins[$this->input->post($k)];
									$old_content[$k] = $origins[$record[$k]];
								} else {
									if($k == 'prize_image' && $this->input->post($k) == '')
										continue;
									$new_content[$k] = $this->input->post($k);
									$old_content[$k] = $record[$k];
								}
							}
						}
					}
					$post = array();
					$post['url'] = SITE_URL . '/birthday_offer/edit/' . $id;
					$post['description'] = 'updated a birthday_offer';
					$post['table'] = $this->_table;
					$post['record_id'] = $id;
					$post['type'] = 'edit';
					$post['field_changes'] = serialize(array('old'	=> $old_content,
													  		  'new'	=> $new_content));
					$this->module_model->save_audit_trail($post);				

					redirect('flash_offer');	
				}
				
			} else {
				$error = validation_errors();
			}
		}
		$data['error'] = $error;
		$data['origins'] = $this->module_model->get_origins();
		return $this->load->view('flash_offer/add', $data, true);			
	}
	
	
	public function delete() {
		$table = $this->_table;
		$id = $this->uri->segment(3);
		$field = 'prize_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/flash_offer') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$where[$field] = $id;
			$this->global_model->delete_record($table, $where);
		}
		redirect('flash_offer');
	}
	
	private function get_rules() {
		$config = array(
		   array(
				 'field'   => 'prize_name',
				 'label'   => 'prize name',
				 'rules'   => 'required'
			  )
		);
		return $config;
	}

	private function upload_video(&$error = false) {
		$this->load->helper('upload_helper');
		$config['upload_path'] = 'uploads/prize/';
		$config['allowed_types'] = 'avi|mov|wmv|mp4';
		$config['do_upload'] = 'video';
		$config['max_width'] = 0;
		$config['max_height'] = 0;
		$config['max_size'] =  0;
		$config['file_name'] = uniqid() . '.' . end(explode('.', $_FILES['video']['name']));
		$uploaded = upload($config);
		if(!is_array($uploaded))
			$error = $uploaded;
		else {
			$dir = dirname(__FILE__) . '/../../';
			$ret = system('ffmpeg -i ' . $dir . 'uploads/prize/' . $uploaded['file_name'] . ' -ss 00:00:03.000 -f image2 -vframes 1 ' . $dir . 'uploads/prize/thumb_' . $uploaded['raw_name'] . '.jpg');
 			return $uploaded['file_name'];
		}
  		
	}

	private function upload_image(&$error = false) {
		$this->load->helper('upload_helper');
		$config['upload_path'] = 'uploads/prize/';
		$config['allowed_types'] = 'gif|png|jpg';
		$config['do_upload'] = 'photo';
		$config['max_width'] = 1000;
		$config['max_height'] = 800;
		$config['max_size'] =  3072;
		$config['file_name'] = uniqid() . '.' . end(explode('.', $_FILES['photo']['name']));
		$uploaded = upload($config);
		if(!is_array($uploaded))
			$error = $uploaded;
		else
			return $uploaded['file_name'];
	}

	
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */