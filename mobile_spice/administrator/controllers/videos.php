<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Videos extends CI_Controller {
	
	var $file_name;

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
 	}
	
	public function index()
	{ 
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
 	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
		$this->load->helper('file');
		$this->load->helper('paging');
		$offset = (int)$this->input->get('per_page');
	    $limit = 10;
	    $filters = $this->get_filters();

		$access = $this->module_model->check_access('videos');
		$total_rows = $this->explore_model->get_total_rows(array('table'=>'tbl_about_videos as v',
																 'where'=>array_merge($filters['where_filters'],array('v.is_deleted'=>0)),
																 'like'=>$filters['like_filters']
																 )
														    ); 
		$data['header_text'] = 'Videos';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total_rows'] = $total_rows;

		$rows = $this->explore_model->get_rows(array('table'=>'tbl_about_videos',
													 'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
												     'like'=>$filters['like_filters'],
													 'order_by'=>array('field'=>'date_added','order'=>'DESC'),
 												     'limit'=>$limit,
												     'offset'=>$offset
												     )
												);
		$data['rows'] = $rows;
		$data['query_strings'] = $filters['query_strings'];
		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']);
 		return $this->load->view('about/'.$this->uri->segment(1).'/list',$data,TRUE);

	}


	public function add()
	{
		
		$this->load->helper('file');

		$error = '';
		$row = '';

		if($this->input->post()) {

			$post_data = $this->input->post();


 			if($this->validate_form()) { 
				
 				$data = $post_data;
 				$user = $this->login_model->extract_user_details();
				$data['date_added'] = TRUE; // Set NOW() value to date_added column field
 				$data['uploader_id'] = $user['cms_user_id'];
				$data['user_type'] = 'administrator';
				$data['url_title'] = url_title(ucwords(strtolower(trim($post_data['title']))));
				$data['video'] =  $this->session->userdata('file_name'); 
				$id = $this->explore_model->insert('tbl_about_videos',$data);	

				$this->session->unset_userdata(array('file_name'=>''));			     


	   		    $post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/add';
				$post['description'] = 'added a video';
				$post['table'] = 'tbl_about_videos';
				$post['record_id'] = $id;
				$post['type'] = 'add';
				$this->module_model->save_audit_trail($post);

	   		    redirect($this->uri->segment(1)); 

			} else {

				$post_data['video'] = '';
				$error = validation_errors();
				$row = json_decode(json_encode($post_data),false);

			}
			 
		} 

		$data['error'] = $error;
		$data['row'] = $row;
 		$data['header_text'] = 'Video';
 		$data['main_content'] = $this->load->view('about/'.$this->uri->segment(1).'/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

	}




	public function edit()
	{

		$this->load->helper('file');

		$error = '';
		$id = $this->uri->segment(3);
		$row = $this->explore_model->get_row(array('table'=>'tbl_about_videos','where'=>array('about_video_id'=>$id,'is_deleted'=>0)));


		if($this->input->post())
		{
			$post_data = $this->input->post();

			if($this->validate_form()){


				$trend = json_decode(json_encode($row),TRUE);


				$fields = array('title', 'description','status');
				$status = array('', 'Published', 'Unpublished');

				$new_content = array();
				$old_content = array();

				foreach($trend as $k => $v) {
					if(in_array($k, $fields)) {
						if($trend[$k] != $this->input->post($k)) {

							if($k == 'status') {
								
								$new_content[$k] = $status[$this->input->post($k)];
								$old_content[$k] = $status[$trend[$k]];

							}else {
								$new_content[$k] = $this->input->post($k);
								$old_content[$k] = $trend[$k];
							}

						}
					}
				}

				$post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/edit/' . $id;
				$post['description'] = 'updated a video';
				$post['table'] = 'tbl_about_videos';
				$post['record_id'] = $id;
				$post['type'] = 'edit';
				$post['field_changes'] = serialize(array('old'	=> $old_content,
														 'new'	=> $new_content));
				$this->module_model->save_audit_trail($post);

				if($this->session->userdata('file_name')){
					$post_data['video'] = $this->session->userdata('file_name');
  					$this->session->unset_userdata(array('file_name'=>''));
				}


  				$post_data['date_updated']	= TRUE; // Set NOW() value to column date_updated  
 				$post_data['url_title'] = url_title(ucwords(strtolower(trim($post_data['title']))));
				$this->explore_model->update('tbl_about_videos',$post_data,array('about_video_id'=>$id,'is_deleted'=>0));
 			    redirect($this->uri->segment(1));

			}else{

				$error = validation_errors();
 				$row = json_decode(json_encode($post_data),false);

			}

		}

		$video_url = ($this->input->post('video_url')) ?  $this->input->post('video_url') : '';
		$video_embed = ($this->validate_YoutubeURL($video_url)) ? $this->parse_youtube_url($video_url,'embed') : false;

		$data['video_url'] = $video_url;
		$data['video_embed'] = $video_embed;
 

		$data['row'] = $row;
 		$data['error'] = $error;
		$data['header_text'] = 'Video';
 		$data['main_content'] = $this->load->view('about/'.$this->uri->segment(1).'/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

 
	}


	public function delete()
	{

 		$id = $this->uri->segment(3);
 		$token = $this->uri->segment(4);
 		$path = 'uploads/about/'.$this->uri->segment(1).'/';

		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$row = $this->explore_model->get_rows(array('table'=>'tbl_about_videos','where'=>array('about_video_id'=>$id)));			 
 
 			$this->explore_model->update('tbl_about_videos',array('is_deleted'=>1),array('about_video_id'=>$id));

 			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'deleted a videos';
			$post['table'] = 'tbl_about_videos';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);

		}

		redirect($this->uri->segment(1));

	}

	public function win()
	{
		$id = $this->uri->segment(3);
 		$token = $this->uri->segment(4);

		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
 			$this->explore_model->update('tbl_about_videos',array('winner_status'=>1),array('about_video_id'=>$id));
 			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'set a video winner entry';
			$post['table'] = 'tbl_about_videos';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);


			$this->Points_Model->earn(ABOUT_VIDEOS, array('suborigin' => $id));
  		}

		redirect($this->uri->segment(1));
	}

	public function not_win()
	{
		$id = $this->uri->segment(3);
 		$token = $this->uri->segment(4);

		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
 			$this->explore_model->update('tbl_about_videos',array('winner_status'=>0),array('about_video_id'=>$id));
 			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'set a video not winner entry';
			$post['table'] = 'tbl_about_videos';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);
  		}

		redirect($this->uri->segment(1));
	}


	public function validate_form()
	{

		 $this->load->library('form_validation');

		$rules = array(
		   array(
				 'field'   => 'title',
				 'label'   => 'Title',
				 'rules'   => 'required|callback_validate_title'
			  )
		);
 
		if( $this->router->method =='add' || ($_FILES['video']['error'] < 1 && $this->router->method == 'edit')){

			$rules[] = array('field'   => 'video',
							 'label'   => 'File To Upload',
							 'rules'   => 'callback_do_upload'
						  );
 		}
 
		 $this->form_validation->set_rules($rules);
		
		 return $this->form_validation->run();
 	}

	
	public function validate_title($title)
	{	
		$url_title = url_title(ucwords(strtolower(trim($title))));
		$where = ($this->uri->segment(3)) ? array('about_video_id !='=>$this->uri->segment(3),'url_title'=>$url_title) : array('url_title'=>$url_title);
		$row = $this->explore_model->get_row(array('table'=>'tbl_about_videos','where'=>$where));
		if($row){
			$this->form_validation->set_message('validate_title','Title already exists!');
			return false;
		}else{
			return true;
		}

	}




	public function do_upload()
	{
		$this->load->helper(array('upload'));
		$upload_path = 'uploads/about/'.$this->uri->segment(1);
		$new_filename = uniqid();
		$upload_file = 'video';

		if(!is_dir($upload_path)){
			mkdir($upload_path,0777,true);
		}

		$params = array('upload_path'=>$upload_path,'allowed_types'=>'flv|mp4','file_name'=>$new_filename,'max_size'=>'0','max_width'=>0,'max_height'=>0,'do_upload'=>$upload_file);
	    $file = upload($params);

	    if(is_array($file)){

	    	$dir = dirname(__FILE__) . '/../../';
	    	if(strpos($_SERVER['SERVER_NAME'],'localhost')===false){
	    		$ret = system('ffmpeg -i ' . $dir . 'uploads/about/videos/' . $file['file_name'] . ' -ss 00:00:03.000 -f image2 -vframes 1 ' . $dir . 'uploads/about/videos/' . $file['raw_name'] . '.jpg');
	    	}else{
	    		$ret = system('C:\ffmpeg\ffmpeg-20140305-git-d08bb06-win32-static\bin\ffmpeg -i ' . $dir . 'uploads/about/videos/' . $file['file_name'] . ' -ss 00:00:03.000 -f image2 -vframes 1 ' . $dir . 'uploads/about/videos/' . $file['raw_name'] . '.jpg');
	    	}
			$this->session->set_userdata('file_name',$file['file_name']);			 			
	    	return true;

	    }else{

	    	$this->form_validation->set_message('do_upload',$file);
	    	return false;

	    }


	}


	function __get_video_dimensions($video = false) 
	{
		$ffmpeg = 'C:\ffmpeg\ffmpeg-20140305-git-d08bb06-win32-static\bin\ffmpeg';
  
		if (file_exists ( $video )) {  
			$command = $ffmpeg . ' -i ' . $video . ' -vstats 2>&1';  
			$output = shell_exec ( $command );  
			  
			$result = ereg ( '[0-9]?[0-9][0-9][0-9]x[0-9][0-9][0-9][0-9]?', $output, $regs );  
		  
			if (isset ( $regs [0] )) {  
				$vals = (explode ( 'x', $regs [0] ));  
				$width = $vals [0] ? $vals [0] : null;  
				$height = $vals [1] ? $vals [1] : null;  
				return array ('width' => $width, 'height' => $height );  
			} else {  
				return false;  
			}  
		} else {  
		  
			return false;  
		}  
	  
	} 

	public function get_filters()
	   {
		   
		   $where_filters = array('status'=>'status','winner_status'=>'winner_status','DATE(date_added) >= '=>'from_date_added','DATE(date_added) <='=>'to_date_added');
		   $like_filters = array('title'=>'title','uploader_name'=>'uploader_name');
		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
		   
	   } 

	   public function export()
	   {
	   		$this->load->library('to_excel_array');
		   	$filters = $this->get_filters();
		   	$params = 
		   	$query = $this->explore_model->get_rows(array('fields'=>"title as Title,video as Video,uploader_name as Submitted_by,date_added as Date_added,status,user_type",
		   														'table'=>'tbl_about_videos',
											   					'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
											   					'like'=>$filters['like_filters']
											   					)
		   													);
		   	$res[] = array('Title','Video', 'Submitted_by','Status','Date Added');
		   	if($query->num_rows()){
		   		foreach($query->result() as $v){
		   			$video = ($v->user_type=='administrator') ? BASE_URL.'uploads/about/videos/'.$v->Video : $v->Video;
		   			$status = ($v->status) ? 'Published' : 'Unpublished';
		   			$res[] = array($v->Title,$video,$v->Submitted_by,$status,$v->Date_added);
		   		}
		   	}

		   	$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));
	   }


	   public function parse_youtube_url($url,$return='',$width='',$height='',$rel=0){

			$urls = parse_url($url);

		    

			//url is http://youtu.be/xxxx

			if(@$urls['host'] == 'youtu.be'){

				$id = ltrim(@$urls['path'],'/');

			}

			//url is http://www.youtube.com/embed/xxxx

			else if(strpos(@$urls['path'],'embed') == 1){

				$id = end(explode('/',@$urls['path']));

			}

			 //url is xxxx only

			else if(strpos($url,'/')===false){

				$id = $url;

			}

			//http://www.youtube.com/watch?feature=player_embedded&v=m-t4pcO99gI

			//url is http://www.youtube.com/watch?v=xxxx

			else{

				parse_str(@$urls['query']);

				$id = @$v;

				

				/*if(!empty($feature)){

					$id = end(explode('v=',$urls['query']));

				}

				*/

			}

			//return embed iframe

			if($return == 'embed'){

				return '<iframe width="'.($width?$width:560).'" height="'.($height?$height:349).'" src="http://www.youtube.com/embed/'.$id.'?&wmode=opaque&autoplay=1&rel='.$rel.'&fs=0&modestbranding=1&controls=1&cc_load_policy=1" frameborder="0"></iframe>';

			}
			
			if($return == 'embed_url'){

				return 'http://www.youtube.com/embed/'.$id.'?rel='.$rel.'"';

			}

			//return normal thumb

			else if($return == 'thumb'){

				return 'http://i1.ytimg.com/vi/'.$id.'/default.jpg';

			}

			//return hqthumb

			else if($return == 'hqthumb'){

				return 'http://i1.ytimg.com/vi/'.$id.'/hqdefault.jpg';

			}

			// else return id

			else{

				return $id;

			}

	}
	

	function validate_YoutubeURL($url) {

	    // Let's check the host first
	    $parse = parse_url($url);
	    $host = @$parse['host'];
	    if (!in_array($host, array('youtube.com', 'www.youtube.com'))) {
	        return false;
	    }

	    $ch = curl_init();
	    $oembedURL = 'www.youtube.com/oembed?url=' . urlencode($url).'&format=json';
	    curl_setopt($ch, CURLOPT_URL, $oembedURL);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	    // Silent CURL execution
	    $output = curl_exec($ch);
	    unset($output);

	    $info = curl_getinfo($ch);
	    curl_close($ch);

	    if ($info['http_code'] !== 404)
	        return true;
	    else 
	        return false;
	}


	public function play_video()
	{
 
		$this->load->helper('file');
		$id = $this->uri->segment(3);
		$row = $this->explore_model->get_row(array('table'=>'tbl_about_videos','where'=>array('about_video_id'=>$id,'is_deleted'=>0)));

		$data['youtube_embed'] = ($row->user_type=='registrant' && $this->validate_YoutubeURL($row->video)) ? $this->parse_youtube_url($row->video,'embed','','','0','1') : false;
		$data['row'] = $row;
		$this->load->view('about/videos/video-view',$data);

	}

 
}