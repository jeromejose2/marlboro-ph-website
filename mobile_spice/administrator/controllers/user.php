<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('user_model');
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		
		$where = '';
		if(isset($_GET['search'])) {
			$where =  " AND name LIKE '%$_GET[name]%'";
			$where .=  " AND username LIKE '%$_GET[email]%'";
			$where .=  " AND position LIKE '%$_GET[position]%'";		
		}
		
		$data['users'] = $this->user_model->get_user($where, $this->uri->segment(2, 1), PER_PAGE, $records);
		$page = $this->uri->segment(2, 1);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/user', PER_PAGE);
		$data['offset'] = ($page - 1) * PER_PAGE;
		$access = $this->module_model->check_access('user');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = $records;
		return $this->load->view('user/index', $data, true);		
	}
	
	public function add() {
		$data['main_content'] = $this->add_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function add_content() {
		$error = '';
		if($this->input->post('submit')) {
			$valid = $this->validate_fields();
			if($valid) {
				$id = $this->user_model->save_user();	
				
				$post['url'] = SITE_URL . '/user/add';
				$post['description'] = 'added a new CMS user';
				$post['table'] = 'tbl_cms_users';
				$post['record_id'] = $id;
				$post['type'] = 'add';
				$this->module_model->save_audit_trail($post);
				redirect('user');
			} else {
				$error = 'Sorry, email is already in use';
			}
		}
		$data['error'] = $error;
		$data['user'] = $_POST;
		$data['module'] = $this->module_model->get_all_module();
		$data['mgroup'] = $this->module_model->get_module_group();
		return $this->load->view('user/add', $data, true);			
	}
	
	public function edit() {
		$id = $this->uri->segment(3);
		$data['main_content'] = $this->edit_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function edit_content($id) {
		$error = '';
		$user = $this->user_model->get_user(array('cms_user_id'	=> $id))[0];
		if(!$user)
			redirect('user');
		$new_content = array();
		$old_content = array();
		$new_permissions = array();
		$data['permissions'] = $this->user_model->get_user_permissions($id);
		
		//print_r($this->module_model->get_module_names());
		//die();
		if($this->input->post('submit')) {
			$valid = $this->validate_fields();
			$module_names = $this->module_model->get_module_names();
			if($valid) {
				$this->user_model->save_user(true);
				$fields = array('username', 'password', 'company', 'name', 'position');
				foreach($user as $k => $v) {
					if(in_array($k, $fields)) {
						if($user[$k] != $this->input->post($k)) {
							if(($k == 'password' && $this->input->post($k) != '') || $k != 'password') {
								$new_content[$k] = $this->input->post($k);
								$old_content[$k] = $user[$k];
							}
						}
					}
				}
				
				//if($data['permissions']) {
					$actions = array('add', 'edit', 'view', 'delete');
					foreach($actions as $action) {
						foreach($module_names as $k => $v) {
							/*var_dump(in_array($k, $this->input->post($action)));
							var_dump(!in_array($k, $data['permissions'][$action]));
							var_dump(!in_array($k, $this->input->post($action)));
							var_dump(in_array($k, $data['permissions'][$action]));*/
							
							if(($this->input->post($action) && in_array($k, $this->input->post($action)))) {
								if(!isset($data['permissions'][$action]) || !in_array($k, $data['permissions'][$action])) {
									$old_content[$v . '(' . $action . ')'] = 'No';
									$new_content[$v . '(' . $action . ')'] = 'Yes';
								}
							} elseif(!$this->input->post($action) || ($this->input->post($action) && !in_array($k, $this->input->post($action)))) {
								if(isset($data['permissions'][$action])) {
									if(in_array($k, $data['permissions'][$action])) {
										$old_content[$v . '(' . $action . ')'] = 'Yes';
										$new_content[$v . '(' . $action . ')'] = 'No';
										//$new_permissions[] = $v . '(' . $action . ')  Yes => No'; 
									}	
								} 
							} else {
								$old_content[$v . '(' . $action . ')'] = 'Maybe';
								$new_content[$v . '(' . $action . ')'] = 'Maybe';	
							}
						}
					}
				//}
				$post['url'] = SITE_URL . '/user/edit/' . $id;
				$post['description'] = 'updated a CMS user account';
				$post['table'] = 'tbl_cms_users';
				$post['record_id'] = $id;
				$post['type'] = 'edit';
				$post['field_changes'] = serialize(array('old'	=> $old_content,
											  			 'new'		=> $new_content,
														 'permissions'	=> $new_permissions));
				$this->module_model->save_audit_trail($post);
					
				redirect('user');
			} else {
				$error = 'Sorry, email is already in use';
			}
		}
		
		$data['error'] = $error;
		$data['user'] = $_POST ? $_POST : $user;
		$data['module'] = $this->module_model->get_all_module();
		$data['mgroup'] = $this->module_model->get_module_group();
		
		return $this->load->view('user/add', $data, true);			
	}
	
	
	private function validate_fields() {
		$email = $this->input->post('username');
		$id = $this->input->post('id');
		$where['username'] = $email;
		$user = $this->user_model->get_user($where);
		if($user) {
			if($id && $user[0]['cms_user_id'] == $id)
				return true;
			return false;
		}
		
		return true;
	}
	
	public function delete() {
		$table = 'tbl_cms_users';
		$id = $this->uri->segment(3);
		$field = 'cms_user_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/user') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$where[$field] = $id;
			$this->global_model->delete_record($table, $where);
			
			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'deleted a CMS user account';
			$post['table'] = 'tbl_cms_users';
			$post['record_id'] = $id;
			$post['type'] = 'delete';
			$this->module_model->save_audit_trail($post);
			
		}
		redirect('user');
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete();
		elseif($method == 'add')
			$this->add();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */