<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class User_Checkins extends CI_Controller {
	

	public function __construct() {

		parent::__construct();

		$this->_table = 'tbl_checkins';

	} 

 	 
 	public function index() {

 		$data = array(
 			'main_content'	=> $this->main_content(),
 			'nav'			=> $this->nav_items()
 		);
 		$this->load->view('main-template', $data);

 	}

 	public function nav_items() {

 		$data = $this->module_model->get_nav_data();
 		return $this->load->view('nav', $data, TRUE);

 	}

 	public function main_content() {

 		$access = $this->module_model->check_access('bar_checkins');
 		$page = $this->uri->segment(2, 1);
 		$data['offset'] = ( $page - 1 ) * PER_PAGE;

 		if(isset($_GET['type']) && $_GET['type'] == '5') {
 			$where = 'type = \'5\'';
 			$type_table = 'tbl_lamp_events';
 			$data['type'] = 'LAMP';
 		} else {
 			$where = 'type = \'0\'';
 			$type_table = 'tbl_backstage_events';
 			$data['type'] = 'Event';
 		}
 		$join_table = $type_table == 'tbl_lamp_events' ? 'tbl_lamp_events.lamp_event_id ' : 'tbl_backstage_events.backstage_event_id ';
 		$join_field = NULL;
 		if($type_table == 'tbl_lamp_events') {
 			$join_field = '*, tbl_checkins.date_created as date_created';
 		} else {
 			$join_field = '*, tbl_checkins.date_created as date_created, tbl_backstage_events.title as event_title';
 		}

 		if( isset($_GET['search']) ) {
 			$where .= isset($_GET['name']) && $_GET['name'] ? " AND concat(first_name, ' ', third_name) LIKE '%$_GET[name]%'" : FALSE;
 		}

 		$param = array(
 			'offset'	=> $data['offset'],
 			'limit'	=> PER_PAGE,
 			'table'	=> $this->_table,
 			'fields'	=> $join_field,
 			'where'	=> $where,
 			'order_by'	=> array(
 						'field' => 'tbl_checkins.date_created',
 						'order' => 'DESC'
 					),
 			'join'	=> array(
 						'tbl_registrants' => 'tbl_registrants.registrant_id = tbl_checkins.registrant_id',
						$type_table => $join_table . '= tbl_checkins.id'
 					)
 		);
 		$data['users'] = $this->global_model->get_rows( $param )->result_array();
 		$records = $this->global_model->get_total_rows( $param );
 		$data['pagination'] = $this->global_model->pagination( $records, $page, SITE_URL . '/user_checkins' );
 		$data['total'] = $records;
 		$data['graph'] = $this->graph();

 		return $this->load->view('checkins/checkins-lists', $data, TRUE);

 	}

 	public function export() {

 		$where = 'type = \'0\'';

 		if(isset($_GET['type']) && $_GET['type'] == '5') {
 			$where = 'type = \'5\'';
 			$type_table = 'tbl_lamp_events';
 			$data['type'] = 'LAMP';
 		} else {
 			$where = 'type = \'0\'';
 			$type_table = 'tbl_backstage_events';
 			$data['type'] = 'Event';
 		}
 		$join_table = $type_table == 'tbl_lamp_events' ? 'tbl_lamp_events.lamp_event_id ' : 'tbl_backstage_events.backstage_event_id ';
 		$join_field = NULL;
 		if($type_table == 'tbl_lamp_events') {
 			$join_field = '*, tbl_checkins.date_created as date_created';
 		} else {
 			$join_field = '*, tbl_checkins.date_created as date_created, tbl_backstage_events.title as event_title';
 		}

 		if( isset($_GET['search']) ) {
 			$where .= isset($_GET['name']) && $_GET['name'] ? " AND concat(first_name, ' ', third_name) LIKE '%$_GET[name]%'" : FALSE;
 		}

 		$param = array(
 			'offset'	=> 0,
 			'table'	=> $this->_table,
 			'fields'	=> $join_field,
 			'where'	=> $where,
 			'join'	=> array(
 						'tbl_registrants' => 'tbl_registrants.registrant_id = tbl_checkins.registrant_id',
						$type_table => $join_table . '= tbl_checkins.id'
 					)
 		);
 		$records = $this->global_model->get_rows( $param )->result_array();
 		$row[] = array(
 			'#',
 			'Name',
 			'Event',
 			'Date Checked In'
 		);
 		if( $records ) {
 			foreach( $records as $k => $v ) {
 				$row[] = array(
 					$k + 1,
 					$v['first_name'] . ' ' . $v['third_name'],
 					$v['event_title'],
 					date('F d, Y H:i:s', strtotime($v['date_created']))
 				);
 			}
 		}

 		$this->load->library('to_excel_array');
 		$this->to_excel_array->to_excel( $row, 'backstage_event_checkins_' . date('YmdHis') );

 	}

 	private function graph() {

 		$function_get_days = function($param) {
 			$date = date("t", strtotime($param));

			for($i = 1; $i <= $date; $i++) {
				$every_date = date("Y") . "-" . date("m") . "-" . str_pad($i, 2, "0", STR_PAD_LEFT);

				$data[] = array($every_date);
			}

			return $data;
 		};

 		$param = array(
 			'table' => $this->_table,
 			'where' => 'type = \'0\'',
 		);
 		$raw_result = $this->global_model->get_rows($param)->result_array();

 		foreach($raw_result as $k => $v) {
 			$final[$k]['date_created'] = $v['date_created'];
 		}

 		$date1 = @date('Y-m-d', strtotime(min($final[0])));
 		$date2 = @date('Y-m-d', strtotime(max($final[0])));

 		$dates_count = $function_get_days($date1);

 		for($i = 0; $i < count($dates_count); $i++) {
 			$param = array(
 				'table' => $this->_table,
 				'where' => "type = '0' AND date(date_created) = '" . $dates_count[$i][0] . "'",
 				'fields' => 'COUNT(*) as total'
 			);
 			$items = $this->global_model->get_rows($param)->result_array();
 			$param = array(
 				'table' => $this->_table,
 				'where' => "type = '5' AND date(date_created) = '" . $dates_count[$i][0] . "'",
 				'fields' => 'COUNT(*) as total'
 			);
 			$items2 = $this->global_model->get_rows($param)->result_array();
 			$checkins_daily['daily'][] = array($dates_count[$i][0], $items[0]['total'], $items2[0]['total']);
 		}

 		return $checkins_daily;

 	}

 	public function _remap($method) {

 		if( is_numeric($method) ) {
 			$this->index();
 		} else {
 			$this->{$method}();
 		}

 	}

}