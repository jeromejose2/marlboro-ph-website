<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Backstage_event_attendance extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
		//$this->output->enable_profiler(TRUE);
 	} 

 	 
 	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
 	}
 	 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
		$this->load->helper(array('paging','simplify_datetime_range'));
		$offset = (int)$this->input->get('per_page');
	    $limit = PER_PAGE;
	    $filters = $this->get_filters(); 

		$access = $this->module_model->check_access('backstage_event_attendance');
		$total_rows = $this->explore_model->get_rows(array('table'=>'tbl_backstage_attendance as a',
															 'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
														     'like'=>$filters['like_filters'],
														     'join'=>array(array('table'=>'tbl_backstage_events as e','on'=>'e.backstage_event_id=a.event_id'),
														     				array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=a.registrant_id')
														     			  ),
														     'fields'=>'a.attendance_id'
 														     )
														)->num_rows();
 		$data['header_text'] = 'Attendance - Backstage Pass';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total_rows'] = $total_rows;
		$data['query_strings'] = $filters['query_strings'];
 		
		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_backstage_attendance as a',
															 'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
														     'like'=>$filters['like_filters'],
														     'order_by' => array('field'=>'date_added','order'=>'DESC'),
														     'offset'=>$offset,
														     'limit'=>$limit,
														     'join'=>array(array('table'=>'tbl_backstage_events as e','on'=>'e.backstage_event_id=a.event_id'),
														     				array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=a.registrant_id')
														     			  ),
														     'fields'=>'e.title as event, e.start_date, e.end_date, r.first_name, r.third_name, a.date_added'
 														     )
														);
		$data['events'] = $this->explore_model->get_rows(array('table'=>'tbl_backstage_events','where'=>array('is_deleted'=>0)));
  
 		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']);
 		return $this->load->view('backstage/attendance/list',$data,TRUE);


	}
  

	public function get_filters()
	   {
		   
		   $where_filters = array('DATE(a.date_added) >='=>'from_date_added','DATE(a.date_added) <='=>'to_date_added','a.event_id'=>'event','DATE(e.start_date)'=>'name');
		   $like_filters = array("CONCAT(r.first_name,'',r.third_name)"=>'name');
		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}

		   }

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
		   
	   } 


	   public function export()
	   {
	   		$this->load->library('to_excel_array');
	   		$this->load->helper('simplify_datetime_range');

		   	$filters = $this->get_filters();
		   	$params = 
		   	$query = $this->explore_model->get_rows(array('table'=>'tbl_backstage_attendance as a',
															 'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
														     'like'=>$filters['like_filters'],
														     'order_by' => array('field'=>'date_added','order'=>'DESC'),
														     'join'=>array(array('table'=>'tbl_backstage_events as e','on'=>'e.backstage_event_id=a.event_id'),
														     				array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=a.registrant_id')
														     			  ),
														     'fields'=>'e.title as event, e.start_date, e.end_date, r.first_name, r.third_name, a.date_added'
 														     )
														);
		   	$res[] = array('Name','Event','Schedule','Date Added');

		   	if($query->num_rows()){
		   		foreach($query->result() as $v){
		   			$schedule = simplify_datetime_range($v->start_date,$v->end_date);
 		   			$res[] = array($v->first_name.' '.$v->third_name, $v->event,$schedule,$v->date_added);
		   		}
		   	}
		   	$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));

	   }


	   

 
}