<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Demographics extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$type = $this->input->get('type') ? $this->input->get('type') :  'age';
		$data['records'] = $this->get_demographics($type);
		$total = 0;
		if($data['records']) {
			foreach($data['records'] as $k => $v) {
				$total += $v['count'];
			}
		}
		$data['total'] = $total;
		return $this->load->view('demographics/' . $type, $data, true);		
	}

	public function export() {
		$this->load->library('to_excel_array');
		$type = $this->input->get('type') ? $this->input->get('type') :  'age';
		$records = $this->get_demographics($type, $from, $to);
		$row[] = $from && $to ? array(ucfirst($type) . ' Demographics (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')') : array(ucfirst($type) . ' Demographics');
		
		if($type == 'age') {
			$row[] = array(	'Age',
						'Count',
						 'Percentage');
			$total = 0;
			if($records) {

				foreach($records as $k => $v) {
					$total += $v['count'];
				}

				foreach($records as $k => $v) {
					$row[] = array(date('Y') - $v['yob'],
								  $v['count'],
								  number_format(($v['count'] / $total) * 100, 2) . '%');
				}
			}	
		} else {
			$row[] = array(	'Gender',
							'Count',
						 	'Percentage');
			$total = 0;
			if($records) {

				foreach($records as $k => $v) {
					$total += $v['count'];
				}

				foreach($records as $k => $v) {
					$row[] = array($v['gender'],
								  $v['count'],
								  number_format(($v['count'] / $total) * 100, 2) . '%');
				}
			}		
		}
		
		$this->to_excel_array->to_excel($row, 'demographics_'.date("YmdHis"));
	}

	private function get_demographics($type, &$from = false, &$to = false) {
		switch($type) {
			case 'age':
				$param['fields'] = 'COUNT(YEAR(date_of_birth)) AS count, YEAR(date_of_birth) AS yob';
				$param['table'] = 'tbl_registrants';
				$param['order_by'] = array('field'	=> 'yob', 'order'	=> 'ASC');
				$param['group_by'] = 'yob';
				$where = array();
				if($this->input->get('from') && $this->input->get('to')) {
					$from = $this->input->get('from') > $this->input->get('to') ?  $this->input->get('to') :  $this->input->get('from');
					$to = $this->input->get('to') < $this->input->get('from') ?  $this->input->get('from') :  $this->input->get('to');
					$where['YEAR(date_of_birth) <='] = date('Y') - $from;
					$where['YEAR(date_of_birth) >='] = date('Y') - $to;
				}
				if($this->input->get('fromdate') && $this->input->get('todate')) {
					$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
					$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
					$where['DATE(date_created) <='] = $to;
					$where['DATE(date_created) >='] = $from;
				}
				$param['where'] = $where;
				$records = $this->global_model->get_rows($param)->result_array();
				break;
			case 'gender':
				$where = '';
				if($this->input->get('fromdate') && $this->input->get('todate')) {
					$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
					$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
					$where['DATE(date_created) <='] = $to;
					$where['DATE(date_created) >='] = $from;
				}
				$param['where'] = $where;
				$param['fields'] = 'COUNT(gender) AS count, gender';
				$param['table'] = 'tbl_registrants';
				$param['order_by'] = array('field'	=> 'gender', 'order'	=> 'ASC');
				$param['group_by'] = 'gender';
				$records = $this->global_model->get_rows($param)->result_array();
				break;
		}
		return $records;
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */