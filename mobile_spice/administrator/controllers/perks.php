<?php 

class Perks extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		$this->_table = 'tbl_bid_items';
 	}
	
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$error = '';

		if($this->input->post()) {
			if($_FILES['photo_bid']['name']) {
				$image_bid = $this->upload_image('photo_bid', $error);
				$param['description'] = $image_bid;
				$where['type'] = 'bid';
				$this->global_model->update_record('tbl_settings', $where, $param);
			}

			if(!$error && $_FILES['photo_buy']['name']) {
				$image_buy = $this->upload_image('photo_buy', $error);
				$param['description'] = $image_buy;
				$where['type'] = 'buy';
				$this->global_model->update_record('tbl_settings', $where, $param);
			}

			if(!$error && $_FILES['photo_reserve']['name']) {
				$image_reserve = $this->upload_image('photo_reserve', $error);
				$param['description'] = $image_reserve;
				$where['type'] = 'reserve';
				$this->global_model->update_record('tbl_settings', $where, $param);
			}	
		}
		
		$param['where'] = array('where_in'	=> array('field'	=> 'type', 'arr'	=> array('bid', 'buy', 'reserve')));
		$param['table'] = 'tbl_settings';
		$record = $this->global_model->get_rows($param)->result_array();
		$record_arr = array();
		foreach ($record as $key => $value) {
			$record_arr[$value['type']] = $value;
		}
		
		$data['error'] = $error;
		$data['record'] = $record_arr;
		return $this->load->view('perks/index', $data, true);		
	}

	public function add() {
		$data['main_content'] = $this->add_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function add_content() {
		$error = '';
		if($this->input->post('submit')) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules($this->get_rules());
			$valid = !(strtotime($this->input->post('start_date')) > strtotime($this->input->post('end_date')));
			if(!$valid) {
				$error = "Sorry, start date must come before the end date";
			} else {
				$valid = $this->form_validation->run();	
			}
			if($valid) {
				$post = $this->input->post();
				//$post['original_slots'] = $post['slots'];
				$user = $this->login_model->extract_user_details();
				$post['created_by'] = $user['cms_user_id'];
				$post['bid_item_image'] = '';
				$t_meta['x1'] = $this->input->post('x');
				$t_meta['y1'] = $this->input->post('y');
				$t_meta['x2'] = $this->input->post('x2');
				$t_meta['y2'] = $this->input->post('y2');
				$t_meta['width'] = $this->input->post('width');
				$t_meta['height'] = $this->input->post('height');
				$post['thumbnail_meta'] = serialize($t_meta);

				if($_FILES['photo']['name']) {
					$post['bid_item_image'] = $this->upload_image($error, $config);
					if(!$error) {
						$params = array_merge($config,array('file_path'=>$config['file_path'],'full_path'=>$config['full_path'],'file_name'=>$config['file_name']));	    
						$this->generate_thumb($params);		
					}
				}
					
				if(!$error) {
					$id = $this->global_model->insert($this->_table, $post);	

					if($this->input->post('media_upload')) {
						$media = $this->input->post('media_upload');
						$title = $this->input->post('media_title');
						for($i = 0; $i < count($media); $i++) {
							$param = array();
							$param['media_title'] = $title[$i];
							$param['media_content'] = $media[$i];
							$param['media_type'] = 1;
							$param['origin_id'] = PERKS_BID;
							$param['suborigin_id'] = $id;
							$param['cms_user_id'] = $user['cms_user_id'];
							$param['media_date_created'] = date('Y-m-d H:i:s');
							$this->global_model->insert('tbl_media', $param);
						}
					}
					
					$post = array();
					$post['url'] = SITE_URL . '/move_fwd/add';
					$post['description'] = 'added a new move fwd item';
					$post['table'] = 'tbl_move_forward';
					$post['record_id'] = $id;
					$post['type'] = 'add';
					$this->module_model->save_audit_trail($post);
					redirect('bid_items');	
				}
			} else {
				$error = !$error ? validation_errors() : $error;
			}
		}
		$data['error'] = $error;
		$data['record'] = $_POST;
		$data['action'] = SITE_URL . '/bid_items/add';
		return $this->load->view('perks/bid/add', $data, true);			
	}

	private function upload_image($file_index, &$error = false, &$uploaded = false) {
		$this->load->helper(array('upload_helper', 'resize'));
		$config['upload_path'] = 'uploads/perks/';
		$config['allowed_types'] = 'gif|png|jpg';
		$config['do_upload'] = $file_index;
		$config['max_width'] = 20000;
		$config['max_height'] = 16000;
		$config['max_size'] =  8072;
		$config['file_name'] = uniqid() . '.' . end(explode('.', $_FILES[$file_index]['name']));
		$uploaded = upload($config);
		if(!is_array($uploaded)) {
			$error = $uploaded;
			return;
		} else {

			$params = array('new_filename' => 'main_' .$config['file_name'], 'width'=>319,'height'=>300,'source_image'=>$config['upload_path'].'/'.$uploaded['file_name'],'new_image_path'=>$config['upload_path'].'/','file_name'=>$config['file_name']);
	    	resize($params);
	    }
		return $uploaded['file_name'];
	}

	private function generate_thumb($uploaded) {
		list($width, $height) = getimagesize($uploaded['full_path']); 
		if($width < 300) {
			copy($uploaded['full_path'], $uploaded['file_path'].'thumb_'.$uploaded['file_name']);
			return;
		}

		$this->load->helper('crop');
		$config['x'] = $this->input->post('x');
		$config['y'] = $this->input->post('y');
		$config['x2'] = $this->input->post('x2');
		$config['y2'] = $this->input->post('y2');
		$config['width'] = $this->input->post('width');
		$config['height'] = $this->input->post('height');
		$params = array_merge($config, $uploaded);	    
		crop($params);	

	}

	

	// public function _remap($method) {
	// 	if($method == 'edit')
	// 		$this->edit($this->uri->segment(3));
	// 	elseif($method == 'delete')
	// 		$this->delete($this->uri->segment(3));
	// 	elseif($method == 'add')
	// 		$this->add();
	// 	elseif($method == 'export')
	// 		$this->export();
	// 	else
	// 		$this->index();
	// }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */