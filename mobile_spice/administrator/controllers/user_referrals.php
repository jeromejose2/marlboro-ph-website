<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Referrals extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('referrals_model');
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	} 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$data['offset'] = ($page - 1) * PER_PAGE;
		$records = $this->referrals_model->get_user_referral_count();
		$data['users'] = $this->referrals_model->get_referrals();
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/user_referals');
		$data['total'] = $records;
		return $this->load->view('referrals/user', $data, true);		
	}

	public function referrals($referer,$status){

		$data['data'] = $this->referrals_model->get_referrals_list($referer,$status);
		$this->load->view('referrals/list', $data);
	} 

	public function export()
	{
		$like = array();
		$where = array();
		$this->load->library('to_excel_array');
		$records =  $this->referrals_model->get_referral_export();

		$row[] = array('Name', 
					   'Total Referrals',
					   'Successful Referrals',
					   'Date');
		if($records) {
			foreach($records as $k => $v) {
				$questions = '';
				$row[] = array($v['first_name'] . ' ' . $v['third_name'],  $v['referrals'], $v['verified'], $v['timestamp']);
			}
		}
		$this->to_excel_array->to_excel($row, 'user_referrals_'.date("YmdHis"));
	}

	private function unset_val(&$arr, $exclude) {
		foreach ($arr as $key => $value) {
			if(in_array($key, $exclude)) {
				unset($arr[$key]);
			}
		}
	} 

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */