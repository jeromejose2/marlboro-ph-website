<?php

class Splash_settings extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('settings_model');
	}
		
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	public function index()
	{
		$data['main_content'] = $this->bid_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	private function bid_content() {
		if($this->input->post('submit')) {
			$this->settings_model->save_settings('move_fwd_splash_count', $this->input->post('movefwd'));
			$this->settings_model->save_settings('perks_splash_count', $this->input->post('perks'));
		}
		$data['movefwd'] = $this->settings_model->get_settings('move_fwd_splash_count');
		$data['perks'] = $this->settings_model->get_settings('perks_splash_count');
		$data['error'] = '';
		$data['title'] = 'Splash Settings';
		return $this->load->view('settings/splash', $data, true);		
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */