<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Move_fwd_prizes extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		$this->_table = 'tbl_move_forward_choice';
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		$where['move_forward_choice_status'] = 1;
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			if($like['fromredeem'] && $like['toredeem']) {
				$from = $like['fromredeem'] > $like['toredeem'] ?  $like['toredeem'] :  $like['fromredeem'];
				$to = $like['toredeem'] < $like['fromredeem'] ?  $like['fromredeem'] :  $like['toavail'];
				$where['DATE(prize_delivered_date) <='] = $to;
				$where['DATE(prize_delivered_date) >='] = $from;
			}

			if($like['first_name']) {
				$like['CONCAT(first_name, \' \', third_name)'] = $like['first_name'];
			}

			if($like['fromaccom'] && $like['toaccom']) {
				$where['DATE(move_forward_choice_done) <='] = $like['toaccom'];
				$where['DATE(move_forward_choice_done) >='] = $like['fromaccom'];
			}

			unset($like['search']);
			unset($like['fromredeem']);
			unset($like['toredeem']);
			unset($like['fromaccom']);
			unset($like['toaccom']);
			unset($like['prize_name']);
			unset($like['send_type']);
			unset($like['first_name']);

		}
		$param['like'] = $like;
		$param['table'] = $this->_table;
		$param['where'] = $where;
		$param['offset'] = $data['offset'];
		$param['limit'] = PER_PAGE;
		$param['fields'] = '*';
		$param['order_by'] = array('field' => 'move_forward_choice_done', 'order'	=> 'DESC');
		$param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id = ' . $this->_table . '.registrant_id',
							  'tbl_move_forward' => 'tbl_move_forward.move_forward_id = ' . $this->_table . '.move_forward_id',
							  'tbl_prizes' => 'tbl_move_forward.prize_id = tbl_prizes.prize_id');
		$data['categories'] = $this->global_model->get_rows($param)->result_array();
		$records = $this->global_model->get_total_rows($param);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/move_fwd_prizes');

		$access = $this->module_model->check_access('move_fwd_prizes');
		$data['total'] = $records;
		$data['edit'] = $access['edit'];
		return $this->load->view('move_fwd/prizes/index', $data, true);		
	}

	public function edit()
	{
		$id = $this->uri->segment(3);
		$data['main_content'] = $this->edit_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	private function edit_content($id) {
		$error = false;
		$param['table'] = $this->_table;
		$param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id = ' . $this->_table . '.registrant_id',
							  'tbl_move_forward' => 'tbl_move_forward.move_forward_id = ' . $this->_table . '.move_forward_id',
							  'tbl_prizes' => 'tbl_move_forward.prize_id = tbl_prizes.prize_id');
		$param['where'] = array($this->_table . '.move_forward_choice_id'	=> $id);
		$prize = (array)$this->global_model->get_row($param);
			
		if($this->input->post('submit')) {
			$param['prize_delivered'] = $this->input->post('prize_status');
			if($param['prize_delivered'] == 1) {
				$param['prize_delivered_date'] = date('Y-m-d H:i:s');
			}
			$where = array('move_forward_choice_id'	=> $prize['move_forward_choice_id']);
			$this->global_model->update($this->_table, $param, $where);
			redirect('move_fwd_prizes');
		}

		$data['record'] = $prize;
		$data['error'] = $error;
		return $this->load->view('move_fwd/prizes/add', $data, true);		
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */