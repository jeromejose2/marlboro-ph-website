<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prizes_redeemed extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$data['records'] = $this->get_demographics();
		return $this->load->view('visits/prizes_redeemed', $data, true);		
	}

	private function get_demographics() {
		$where = array('prize_delivered' => 1);
		if($this->input->get('fromdate') && $this->input->get('todate')) {
			$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
			$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
			$where['DATE(prize_delivered_date) <='] = $to;
			$where['DATE(prize_delivered_date) >='] = $from;
		}
		$param['fields'] = 'DATE(prize_delivered_date) AS dv, COUNT(*) AS count';
		$param['where'] = $where;
		$param['table'] = 'tbl_move_forward_choice';
		$param['order_by'] = array('field'	=> 'prize_delivered_date', 'order'	=> 'ASC');
		$param['group_by'] = 'dv';
		$param['offset'] = 0;
		$param['limit'] = 30;
		$records = $this->global_model->get_rows($param)->result_array();
		return $records;
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */