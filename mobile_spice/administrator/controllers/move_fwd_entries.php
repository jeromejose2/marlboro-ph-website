<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class move_fwd_entries extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('explore_model');
		
	}

	public function index()
	{		

		$access = $this->module_model->check_access('backstage_events');

		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
		//$this->output->enable_profiler(true);
 	}

	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}

	public function main_content()
	{

		$offset = (int)$this->input->get('per_page');
	    $limit = PER_PAGE;
	    $filters = $this->get_filters(); 

 
	    if(!$this->input->get()){
					
			$to = $this->global_model->get_row(array('table'=>'tbl_move_forward_gallery','fields'=>"DATE(MAX(mfg_date_created)) as to_date "))->to_date;
			$from = date('Y-m-d',strtotime($to.' -1 month'));
			$filters['where_filters'] = array('DATE(mfg_date_created) >='=>$from,'DATE(mfg_date_created) <='=>$to);
			$date_range = array('start_date'=>$from,'end_date'=>$to);
			$filters['query_strings'] = http_build_query($date_range);


		}else{
			 $date_range = array('start_date'=>$this->input->get('start_date'),'end_date'=>$this->input->get('end_date'));
		}

		$data['date_range'] = $date_range;
		$data['query_string'] = $filters['query_strings'];
 		$access = $this->module_model->check_access('move_fwd_entries');
 		$total_entries = $this->explore_model->get_rows(array('table'=>'tbl_move_forward_gallery as p',
																	 'where'=>$filters['where_filters'],
																     'like'=>$filters['like_filters'],
																     'order_by'=>array('field'=>'date_added','order'=>'DESC'),
																     'join'=>array(array('table'=>'tbl_challenges as c','on'=>'p.challenge_id=c.challenge_id'),
																     			   array('table'=>'tbl_move_forward as m','on'=>'m.move_forward_id=c.move_forward_id')														     		 
																     		 ),
																     'fields'=>'p.move_forward_gallery_id'
 														     		)
																)->num_rows();
 		$data['total_entries'] = $total_entries; 		
  		$data['header_text'] = 'Events';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
  		
		$data['activities'] = $this->explore_model->get_rows(array('table'=>'tbl_move_forward_gallery as p',
																	 'where'=>$filters['where_filters'],
																     'like'=>$filters['like_filters'],
																     'order_by'=>array('field'=>$this->input->get('sort_by') ? $this->input->get('sort_by') : 'activity_name','order'=>$this->input->get('sort_order') ? $this->input->get('sort_order') : 'ASC'),
																     'join'=>array(array('table'=>'tbl_challenges as c','on'=>'p.challenge_id=c.challenge_id'),
																     			   array('table'=>'tbl_move_forward as m','on'=>'m.move_forward_id=c.move_forward_id')														     		 
																     		 ),
																     'group_by'=>'c.move_forward_id',
																     'fields'=>"COUNT(case when mfg_status = '0' then 1 else null end) as pending,COUNT(case when mfg_status = '1' then 1 else null end) as approved, COUNT(case when mfg_status = '2' then 1 else null end) as rejected, COUNT(p.challenge_id) as entry_count_per_activity,m.move_forward_title as activity_name,m.move_forward_id"
 														     		)
																);

		$data['categories'] = $this->explore_model->get_rows(array('table'=>'tbl_move_forward_gallery as p',
																	 'where'=>$filters['where_filters'],
																     'like'=>$filters['like_filters'],
																     'order_by'=>array('field'=>$this->input->get('cat_sort_by') ? $this->input->get('cat_sort_by') : 'category_name','order'=>$this->input->get('sort_order') ? $this->input->get('sort_order') : 'ASC'),
																     'join'=>array(array('table'=>'tbl_challenges as c','on'=>'p.challenge_id=c.challenge_id'),
																     			   array('table'=>'tbl_move_forward as m','on'=>'m.move_forward_id=c.move_forward_id'),
																     			    array('table'=>'tbl_categories as cat','on'=>'m.category_id=cat.category_id')														     		 
																     		 ),
																     'group_by'=>'m.category_id',
																     'fields'=>"COUNT(case when mfg_status = '0' then 1 else null end) as pending,COUNT(case when mfg_status = '1' then 1 else null end) as approved, COUNT(case when mfg_status = '2' then 1 else null end) as rejected, COUNT(p.challenge_id) as entry_count_per_category,cat.category_name as category_name,cat.category_id"
 														     		)
																);

 		$data['pagination'] = 0;//paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']);
 		return $this->load->view('move_fwd/reports/entries',$data,TRUE);

	}

	public function  breakdown_by_activity()
	{
		$filters = $this->get_filters(); 

	 	$data['row'] = $this->explore_model->get_row(array('table'=>'tbl_move_forward',
																	 'where'=>array('move_forward_id'=>$this->input->get('move_forward_id')),
																	 'fields'=>'move_forward_title as title'
 														     		)
																);
		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_move_forward_gallery as p',
																	 'where'=>$filters['where_filters'],
																     'like'=>$filters['like_filters'],
																     'order_by'=>array('field'=>'r.first_name','order'=>'DESC'),
																     'join'=>array(array('table'=>'tbl_challenges as c','on'=>'p.challenge_id=c.challenge_id'),
																     			   array('table'=>'tbl_move_forward as m','on'=>'m.move_forward_id=c.move_forward_id'),
																     			   array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=p.registrant_id','type'=>'left')														     		 
																     		 ),																     
																     'fields'=>"r.registrant_id, r.first_name,r.third_name,p.mfg_date_created,p.mfg_status"
 														     		)
																);
		$this->load->view('move_fwd/reports/breakdown-entries',$data);

	}

	public function  breakdown_by_category()
	{
		$filters = $this->get_filters(); 

	 	$data['row'] = $this->explore_model->get_row(array('table'=>'tbl_categories',
																	 'where'=>array('category_id'=>$this->input->get('category_id')),
																	 'fields'=>'category_name as title'
 														     		)
																);
		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_move_forward_gallery as p',
																	 'where'=>$filters['where_filters'],
																     'like'=>$filters['like_filters'],
																     'order_by'=>array('field'=>'r.first_name','order'=>'DESC'),
																     'join'=>array(array('table'=>'tbl_challenges as c','on'=>'p.challenge_id=c.challenge_id'),
																     			   array('table'=>'tbl_move_forward as m','on'=>'m.move_forward_id=c.move_forward_id'),
																     			   array('table'=>'tbl_categories as cat','on'=>'m.category_id=cat.category_id'),
																     			   array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=p.registrant_id','type'=>'left')														     		 
																     		 ),																     
																     'fields'=>"r.registrant_id, r.first_name,r.third_name,p.mfg_date_created,p.mfg_status"
 														     		)
																);

		$this->load->view('move_fwd/reports/breakdown-entries',$data);

	}

	public function export()
	{

		$this->load->library('to_excel_array');
		$filters = $this->get_filters(); 

		$total_entries = $this->explore_model->get_rows(array('table'=>'tbl_move_forward_gallery as p',
																	 'where'=>$filters['where_filters'],
																     'like'=>$filters['like_filters'],
																     'order_by'=>array('field'=>'date_added','order'=>'DESC'),
																     'join'=>array(array('table'=>'tbl_challenges as c','on'=>'p.challenge_id=c.challenge_id'),
																     			   array('table'=>'tbl_move_forward as m','on'=>'m.move_forward_id=c.move_forward_id')														     		 
																     		 ),
																     'fields'=>'p.move_forward_gallery_id'
 														     		)
																)->num_rows();
		
		$export_by = $this->input->get('export');
		$round = 2;

		if($export_by=='activity_name'){


			$export[] = array('Activity','Pending','Approved','Rejected','Total Entries','Percentage');
			$rows = $this->explore_model->get_rows(array('table'=>'tbl_move_forward_gallery as p',
																		 'where'=>$filters['where_filters'],
																	     'like'=>$filters['like_filters'],
																	     'order_by'=>array('field'=>$this->input->get('sort_by') ? $this->input->get('sort_by') : 'activity_name','order'=>$this->input->get('sort_order') ? $this->input->get('sort_order') : 'ASC'),
																	     'join'=>array(array('table'=>'tbl_challenges as c','on'=>'p.challenge_id=c.challenge_id'),
																	     			   array('table'=>'tbl_move_forward as m','on'=>'m.move_forward_id=c.move_forward_id')														     		 
																	     		 ),
																	     'group_by'=>'c.move_forward_id',
																	     'fields'=>"COUNT(case when mfg_status = '0' then 1 else null end) as pending,COUNT(case when mfg_status = '1' then 1 else null end) as approved, COUNT(case when mfg_status = '2' then 1 else null end) as rejected, COUNT(p.challenge_id) as total_count,((COUNT(p.challenge_id) / $total_entries) * 100) as percentage,m.move_forward_title as activity_name,m.move_forward_id"
	 														     		)
																	);


		}else{

			$export[] = array('Category','Pending','Approved','Rejected','Total Entries','Percentage');
			$rows = $this->explore_model->get_rows(array('table'=>'tbl_move_forward_gallery as p',
																	 'where'=>$filters['where_filters'],
																     'like'=>$filters['like_filters'],
																     'order_by'=>array('field'=>$this->input->get('cat_sort_by') ? $this->input->get('cat_sort_by') : 'category_name','order'=>$this->input->get('sort_order') ? $this->input->get('sort_order') : 'ASC'),
																     'join'=>array(array('table'=>'tbl_challenges as c','on'=>'p.challenge_id=c.challenge_id'),
																     			   array('table'=>'tbl_move_forward as m','on'=>'m.move_forward_id=c.move_forward_id'),
																     			    array('table'=>'tbl_categories as cat','on'=>'m.category_id=cat.category_id')														     		 
																     		 ),
																     'group_by'=>'m.category_id',
																     'fields'=>"COUNT(case when mfg_status = '0' then 1 else null end) as pending,COUNT(case when mfg_status = '1' then 1 else null end) as approved, COUNT(case when mfg_status = '2' then 1 else null end) as rejected, COUNT(p.challenge_id) as total_count,((COUNT(p.challenge_id) / ".$total_entries.") * 100) as percentage, cat.category_name,cat.category_id"
 														     		)
																);
			

		} 
		 

		if($rows->num_rows()){

			foreach($rows->result() as $v){

				$export[] = array(ucwords(strtolower($v->{$export_by})),$v->pending,$v->approved,$v->rejected,$v->total_count,$v->percentage);

			}

		}

		$this->to_excel_array->to_excel($export, 'reports_entries_per_'.$this->input->get('export').date("YmdHis"));


		 


	}

	 

	public function get_filters()
	   {
		   
		   $where_filters = array('m.move_forward_id'=>'move_forward_id',
		   						  'p.mfg_status'=>'status',
		   						  'p.mfg_date_created >='=>'start_date',
		   						  'p.mfg_date_created <='=>'end_date',
		   						  'cat.category_id'=>'category_id');

		   $like_filters = array();

		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>$query_strings);		   
		   
	   } 


}
