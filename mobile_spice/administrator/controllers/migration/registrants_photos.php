<?php

class Registrants_Photos extends CI_Controller
{
	public function index()
	{
		set_time_limit(0);
		ini_set('memory_limit', '3000M');
		ini_set('display_errors', true);
		error_reporting(E_ALL);

		$config = array();
		$config['hostname'] = "10.66.16.202";
		$config['username'] = "marlboro.ph2";
		$config['password'] = "NDexb4t5235mLqn2crk3Oqw19ZIf85";
		$config['database'] = "marlboro.ph2";
		$config['dbdriver'] = "mysql";
		$config['dbprefix'] = "";
		$config['pconnect'] = TRUE;
		$config['db_debug'] = TRUE;
		$config['cache_on'] = FALSE;
		$config['cachedir'] = "";
		$config['char_set'] = "utf8";
		$config['dbcollat'] = "utf8_general_ci";

		$db = new PDO('mysql:dbname='.$config['database'].';host='.$config['hostname'], $config['username'], $config['password']);
		$stmt = $db->prepare("SELECT sm.* FROM social_member sm");
		$stmt->execute();
		$registrants = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$old_gid_path = './uploads/public_assets/user/gid/';
		$old_pic_path = './uploads/public_assets/user/photo/';
		$new_path = './uploads/profile/';
		if (!is_dir($new_path)) {
			mkdir($new_path);
		}
		echo '<pre>';
		print_r("Count: " + count($registrants));
		foreach ($registrants as $reg) {

			$profilePath = $new_path.$reg['id'].'/';
			if (!is_dir($profilePath)) {
				mkdir($profilePath);
			}
			$giidPath = $profilePath.'giid/';
			if (!is_dir($giidPath)) {
				mkdir($giidPath);
			}

			if ($reg['img'] && file_exists($old_gid_path.$reg['img'])) {
				copy($old_gid_path.$reg['img'], $giidPath.$reg['img']);
			}

			if ($reg['image_profile'] && file_exists($old_pic_path.$reg['image_profile'])) {
				copy($old_pic_path.$reg['image_profile'], $profilePath.$reg['image_profile']);
			}

		}

		$stmt = null;
		$db = null;

	}

	public function resize()
	{
		$path = './uploads/profile/';
		$this->load->helper('resize');
		$dirs = scandir($path);
		foreach ($dirs as $dir) {
			$profilePath = $path.$dir.'/';
			foreach (scandir($profilePath) as $file) {
				if (strpos($file, '46_46_') !== 0 && is_file($profilePath.$file)) {
					$params = array(
						'new_filename' => '46_46_'.$file, 
						'width' => 46,
						'height' => 46,
						'source_image' => $profilePath.$file,
						'new_image_path'=> $profilePath,
						'file_name'=> $file
					);
	    			resize($params);
				}
			}
		}
	}
}