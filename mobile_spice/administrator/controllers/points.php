<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Points extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('report_model');
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	public function export()
	{
		$this->load->library('to_excel_array');
		$records = $this->get_demographics($from, $to);
		$row[] = $from && $to ? array('Points Accumulation (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')') : array('Points Accumulation');

		$row[] = array('Week', 'Points');
		if($records) {
			foreach($records as $k => $v) {
				$row[] = array('Week ' . ($k + 1),
							  $v['total_points']);
			}
		}
		$this->to_excel_array->to_excel($row, 'points_'.date("YmdHis"));
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$data['records'] = $this->get_demographics();
		$data['stats'] = $this->get_stats();
		return $this->load->view('points/index', $data, true);		
	}

	private function get_demographics(&$from = false, &$to = false) {
		$where = '';
		$param['fields'] = 'SUM(points) AS total_points, WEEK(date_earned) as week';
		if($this->input->get('fromdate') && $this->input->get('todate')) {
			$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
			$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
			$where['DATE(date_earned) <='] = $to;
			$where['DATE(date_earned) >='] = $from;
			$param['fields'] = 'SUM(points) AS total_points, WEEK(date_earned) as week, WEEK(\'' . $to . '\') as week_last, WEEK(\'' . $from . '\') as week_first';
		}
		$param['where'] = $where;
		$param['table'] = 'tbl_points';
		$param['order_by'] = array('field'	=> 'week', 'order'	=> 'ASC');
		$param['group_by'] = 'week';
		$param['offset'] = 0;
		$param['limit'] = 12;
		$records = $this->global_model->get_rows($param)->result_array();
		$records_arr = array();
		if($records) {
			$cur_week = isset($records[0]['week_first']) ? $records[0]['week_first'] : $records[0]['week'];
			$week_count = 0;
			$last_week_num = isset($records[0]['week_last']) ? $records[0]['week_last'] : $cur_week + count($records);
			foreach($records as $k => $v) {
				if($week_count == 12)
					break;
				if($cur_week == $v['week']) {
					$records_arr[] = $v;
					$cur_week++;
				} else {
					while($cur_week != $v['week']) {
						$new_week = array('total_points'	=> 0, 'week'	=> $cur_week);
						$records_arr[] = $new_week;
						$cur_week++;
					}
					$records_arr[] = $v;
					$cur_week++;
				}
				$week_count++;
			}

			if($cur_week < $last_week_num) {
				while($cur_week <= $last_week_num) {
					if($week_count == 12)
						break;
					$new_week = array('total_points'	=> 0, 'week'	=> $cur_week);
					$records_arr[] = $new_week;
					$cur_week++;
					$week_count++;
				}	
			}

		}
		return $records_arr;
	}

	private function get_stats() {
		$param['table'] = 'tbl_registrants';
		$param['fields'] = "MAX(total_points) AS max_points";
		$max = $this->global_model->get_rows($param)->result_array();
		
		$param['table'] = 'tbl_registrants';
		$param['fields'] = "AVG(total_points) AS avg_points";
		$avg = $this->global_model->get_rows($param)->result_array();
		
		$median = $this->report_model->get_median();
		
		$status = array('mean'		=> $avg[0]['avg_points'],
						'median'	=> $median[0]['med_points'],
						'max'		=> $max[0]['max_points']);
		return $status;
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */