<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('module_model');
		$this->load->model('category_model');
		$this->load->library('to_excel_array');
	}
	public function index()
	{
		
	}
	
	public function registrants() {
		$status = $this->global_model->get_status();
		$this->load->model('registrant_model');
		$brands = $this->registrant_model->get_brands();
		$alternate = $this->registrant_model->get_alernate_purchases();
		$giid = $this->registrant_model->get_giid_types();
		$where = ' AND is_cm = 0';
		if(isset($_GET['search'])) {
			$where =  " AND CONCAT(first_name, ' ', third_name) LIKE '%$_GET[name]%'";
			$where .=  " AND email_address LIKE '%$_GET[email]%'";
			$where .= $_GET['from'] ? " AND date_created >= '$_GET[from]'" : "";
			$where .= $_GET['to'] ? " AND date_created <= '$_GET[to]'" : "";
			$where .= $_GET['status'] != '' ? " AND status = '$_GET[status]'" : "";
		}
		$registrants = $this->registrant_model->get_registrant($where, null, null, $records, true);
		$row[] = array(	'Name',
						 'Email',
						 'GIID Type',
						 'GIID Number',
						 'Registered Date',
						 'Approval Date',
						 'Status',
						 'What brand do you smoke most frequently?',
						 'What other brands do you smoke aside from your regular brand?',
						 'What would you do if your regular brand is unavailable?');
		if($registrants) {
			foreach($registrants as $k => $v) {
				$approval_date = $v['status'] == CSR_APPROVED ? date('F d, Y' , strtotime($v['csr_date_approved'])): 'N/A';
				$row[] = array($v['first_name'] . ' ' . $v['third_name'],
							 	$v['email_address'],
								@$giid[$v['government_id_type']],
								$v['government_id_number'],
								date('F d, Y' , strtotime($v['date_created'])),
								$approval_date,
								$status[$v['status']],
								$brands[$v['current_brand']]['brand_name'],
								$brands[$v['first_alternate_brand']]['brand_name'],
								$alternate[$v['alternate_purchase_indicator']]['alternate_value']);
			}
		}
		$this->to_excel_array->to_excel($row, 'registrants_'.date("YmdHis"));
	}


	public function profiles() {
		$status = $this->global_model->get_status();
		$this->load->model('registrant_model');
		$brands = $this->registrant_model->get_brands();
		$alternate = $this->registrant_model->get_alernate_purchases();
		$giid = $this->registrant_model->get_giid_types();
		$where = ' AND ((new_picture = \'\' AND picture_status = 1) OR (new_picture <> \'\' AND picture_status <> 1))';
		if(isset($_GET['search'])) {
			$where .=  " AND CONCAT(first_name, ' ', third_name) LIKE '%$_GET[name]%'";
			$where .=  " AND email_address LIKE '%$_GET[email]%'";
			$where .= $_GET['from'] ? " AND date_created >= '$_GET[from]'" : "";
			$where .= $_GET['to'] ? " AND date_created <= '$_GET[to]'" : "";
			$where .= $_GET['status'] != '' ? " AND picture_status = '$_GET[status]'" : "";
		}
		$registrants = $this->registrant_model->get_registrant($where, null, null, $records, true);
		$row[] = array(	'Name',
						 'Email',
						 'Profile Picture',
						 'Status',
						 'Registered Date',
						 );
		if($registrants) {
			foreach($registrants as $k => $v) {
				$approval_date = $v['status'] == CSR_APPROVED ? date('F d, Y' , strtotime($v['csr_date_approved'])): 'N/A';
				$row[] = array($v['first_name'] . ' ' . $v['third_name'],
							 	$v['email_address'],
								base_url() . 'uploads/profile/' . $v['registrant_id'] . '/' . $v['new_picture'],
								$v['government_id_number'],
								$status[$v['status']],
								date('F d, Y' , strtotime($v['date_created']))
								);
			}
		}
		$this->to_excel_array->to_excel($row, 'profile_photos_'.date("YmdHis"));
	}
	
	public function statements() {
		$this->load->model('statement_model');
		$where = '';
		if(isset($_GET['search'])) {
			$where =  " AND statement LIKE '%$_GET[statement]%'";
			$where .= $_GET['from_publish'] ? " AND date_published >= '$_GET[from_publish]'" : "";
			$where .= $_GET['to_publish'] ? " AND date_published <= '$_GET[to_publish]'" : "";
			$where .= $_GET['from_unpublish'] ? " AND date_unpublished >= '$_GET[from_publish]'" : "";
			$where .= $_GET['to_unpublish'] ? " AND date_unpublished <= '$_GET[to_unpublish]'" : "";
			$where .= $_GET['status'] != '' ? " AND status = '$_GET[status]'" : "";
		}
		$statements = $this->statement_model->get_statement($where, null, null, $records, true);
		$row[] = array(	'Statement',
						 'Status',
						 'Date Published',
						 'Date Unpublished');
		if($statements) {
			foreach($statements as $k => $v) {
				$date_published = $v['status'] == APPROVED ? date('F d, Y' , strtotime($v['date_published'])) : 'N/A';
				$date_unpublished = $v['status'] == DISAPPROVED ? date('F d, Y' , strtotime($v['date_unpublished'])) : 'N/A';
				$status = $v['status'] == APPROVED ? 'Published' : 'Unpublished';
				$row[] = array($v['statement'],
							 	$status,
								$date_published,
								$date_unpublished);
			}
		}
		$this->to_excel_array->to_excel($row, 'statements_'.date("YmdHis"));
	}

	public function flash_offer() {
		$like = array();
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			unset($like['search']);
		}
		$param['like'] = $like;
		$param['table'] = 'tbl_flash_offers';
		$param['where'] = array('is_deleted' => 0);
		$flash_offers = $this->global_model->get_rows($param)->result_array();

		$row[] = array(	'Offer',
						 'Type',
						 'Description',
						 'Send Type',
						 'Redemption Address',
						 'Stock',
						 'Start Date',
						 'End Date');
		if($flash_offers) {
			foreach($flash_offers as $k => $v) {
				$redemption_address = $v['send_type'] == 'Delivery' ? '' : $v['redemption_address'];
				$row[] = array($v['prize_name'],
							  $v['type'],
							  strip_tags($v['description']),
							  $v['send_type'],
							  strip_tags($redemption_address),
							  $v['stock'],
							  $v['start_date'],
							  $v['end_date']);
			}
		}
		$this->to_excel_array->to_excel($row, 'flash_offer_'.date("YmdHis"));
	}

	public function category() {
		
		$where = '';
		if(isset($_GET['search'])) {
			$where =  " AND category_name LIKE '%$_GET[name]%'";
			$where .= $_GET['origin'] != '' ? " AND origin_id = '$_GET[origin]'" : "";
			$where .= $_GET['parent'] != '' ? " AND parent = '$_GET[parent]'" : "";
		}
		$main_categories = $this->get_main_categories();
		$categories = $this->category_model->get_category($where, null, null, $records, TRUE );
		$origins =  $this->module_model->get_origins(array(BIRTHDAY_OFFERS, FLASH_OFFERS));
		$row[] = array(	'Name',
						'Image',
						'Origin',
						'Parent');
		if($categories) {
			foreach($categories as $k => $v) {
				$row[] = array($v['category_name'],
							  base_url() . 'uploads/category/resized_' . $v['category_image'],
							  !isset($origins[$v['origin_id']]) || $v['origin_id'] == 0 ? 'N/A' : $origins[$v['origin_id']],
							  !isset($main_categories[$v['parent']]) || $v['parent'] == 0 ? 'N/A' : $main_categories[$v['parent']]['category_name']);
			}
		}
		$this->to_excel_array->to_excel($row, 'category_'.date("YmdHis"));

		
	}

		private function get_main_categories() {
			$w['parent'] = 0;
			$w['is_deleted'] = 0;
			$parent_categories = $this->category_model->get_category($w, null, null, $records, true);
						
			if($parent_categories) {
				foreach($parent_categories as $k => $v) {
					$parents[$v['category_id']] = $v;
				}
			}
			return $parents;
		}

	public function prizes(){
		$like = array();
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			unset($like['search']);
		}
		$param['like'] = $like;
		$param['table'] = 'tbl_prizes';
		$param['where'] = array('is_deleted' => 0, 'origin_id !='	=> BIRTHDAY_OFFERS, 'origin_id <>' => FLASH_OFFERS);
		$prizes = $this->global_model->get_rows($param)->result_array();

		$row[] = array(	'Name',
						'Description',
						'Image',
						'Send Type',
						'Redemption Address',
						'Stock',
						'Status');
		if($prizes) {
			foreach($prizes as $k => $v) {
				$row[] = array($v['prize_name'],
								strip_tags($v['description']),
							  $v['prize_image'] ? base_url() . 'uploads/prize/' . $v['prize_image'] : 'No Image',
							  $v['send_type'],
							  strip_tags($v['redemption_address']),
							  $v['stock'],
							  $v['status'] == 1 ? 'Published' : 'Unpublished');
			}
		}
		$this->to_excel_array->to_excel($row, 'prizes_'.date("YmdHis"));

	}

	public function birthday_offer() {
		$like = array();
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			unset($like['search']);
		}
		$param['like'] = $like;
		$param['table'] = 'tbl_birthday_offers';
		$param['where'] = array('is_deleted' => 0);
		$flash_offers = $this->global_model->get_rows($param)->result_array();
		$month_names = array('', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
		$row[] = array(	'Offer',
						'Month',
						 'Description',
						 'Send Type',
						 'Redemption Address',
						 'Stock');
		if($flash_offers) {
			foreach($flash_offers as $k => $v) {
				$redemption_address = $v['send_type'] == 'Delivery' ? '' : $v['redemption_address'];
				$row[] = array($v['prize_name'],
							  $month_names[$v['month']],
							  strip_tags($v['description']),
							  $v['send_type'],
							  strip_tags($redemption_address),
							  $v['stock']);
			}
		}
		$this->to_excel_array->to_excel($row, 'birthday_offer_'.date("YmdHis"));
	}

	public function move_fwd() {
		$like = array();
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			$challenge = $like['challenges'];
			unset($like['search']);
			unset($like['challenges']);
		}

		$param['having'] = array('challenges LIKE ' => '%' . $challenge . '%', 'is_deleted' => 0);
		$param['like'] = $like;
		$param['table'] = 'tbl_move_forward';
		$param['fields'] = '*, (SELECT GROUP_CONCAT(challenge) FROM tbl_challenges WHERE tbl_challenges.move_forward_id = tbl_move_forward.move_forward_id) AS challenges';
		
		$data = $this->global_model->get_rows($param)->result_array();
		$main_categories = $this->get_main_categories();
		$row[] = array(	'Title',
						 'Description',
						 'Category',
						 'Challenges',
						 'Status',
						 'Slots',
						 'Pledge Points',);
		if($data) {
			foreach($data as $k => $v) {

				$row[] = array($v['move_forward_title'],
							  strip_tags($v['move_forward_description']),
							  $main_categories[$v['category_id']]['category_name'],
							  $v['challenges'],
							  $v['status'] > 0 ? $v['status']==1 ? 'Approved' : 'Disapproved' : 'Pending',
							  $v['slots'],
							  $v['pledge_points']);
			}
		}
		
		$this->to_excel_array->to_excel($row, 'move_fwd_'.date("YmdHis"));
	}

	public function move_fwd_gallery() {
		$like = array();
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			if($like['fromavail'] && $like['toavail']) {
				$from = $like['fromavail'] > $like['toavail'] ?  $like['toavail'] :  $like['fromavail'];
				$to = $like['toavail'] < $like['fromavail'] ?  $like['fromavail'] :  $like['toavail'];
				$where['DATE(mfg_date_created) <='] = $to;
				$where['DATE(mfg_date_created) >='] = $from;
			}
			if($like['first_name']) {
				$like["CONCAT(first_name, ' ' , third_name)"] = $like['first_name'];
			}

			if($like['fromapp'] && $like['toapp']) {
				$where['DATE(mfg_date_approved) <='] = $like['toapp'];
				$where['DATE(mfg_date_approved) >='] = $like['fromapp'];
			}

			unset($like['first_name']);
			unset($like['search']);
			unset($like['fromredeem']);
			unset($like['fromavail']);
			unset($like['toavail']);
			unset($like['toredeem']);
			unset($like['fromapp']);
			unset($like['toapp']);
		}
		$param['like'] = $like;
		$param['table'] = 'tbl_move_forward_gallery';
		$param['join'] = array('tbl_challenges' => 'tbl_challenges.challenge_id = tbl_move_forward_gallery.challenge_id',
							   'tbl_registrants' => 'tbl_registrants.registrant_id = tbl_move_forward_gallery.registrant_id',
							   'tbl_move_forward'	=> 'tbl_challenges.move_forward_id = tbl_move_forward.move_forward_id');
		$param['fields'] = '*, tbl_move_forward.status AS move_forward_status';
		$flash_offers = $this->global_model->get_rows($param)->result_array();
		$row[] = array(	'Name',
						 'Task',
						 'Activity',
						 'Status',
						 'Date Submitted',
						 'Date Approved/Rejected');
		if($flash_offers) {
			$status = array('Pending', 'Approved', 'Rejected', 'Exempted');
			foreach($flash_offers as $k => $v) {
				$the_status = $v['mfg_status'] ?  $status[$v['mfg_status']] : 'Pending';
				$date_approved = $v['mfg_status'] == 0 ? 'N/A' : $v['mfg_date_approved'];
				$row[] = array($v['first_name'] . ' ' . $v['third_name'],
							  $v['challenge'],
							  $v['move_forward_title'],
							  $the_status,
							  $v['mfg_date_created'],
							  $date_approved);
			}
		}
		
		$this->to_excel_array->to_excel($row, 'move_fwd_'.date("YmdHis"));
	}

	public function move_fwd_prizes() {
		$where['move_forward_choice_status'] = 1;
		$like = array();
		$table = 'tbl_move_forward_choice';
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			if($like['fromredeem'] && $like['toredeem']) {
				$from = $like['fromredeem'] > $like['toredeem'] ?  $like['toredeem'] :  $like['fromredeem'];
				$to = $like['toredeem'] < $like['fromredeem'] ?  $like['fromredeem'] :  $like['toavail'];
				$where['DATE(prize_delivered_date) <='] = $to;
				$where['DATE(prize_delivered_date) >='] = $from;
			}

			if($like['first_name']) {
				$like['CONCAT(first_name, \' \', third_name)'] = $like['first_name'];
			}

			if($like['fromaccom'] && $like['toaccom']) {
				$where['DATE(move_forward_choice_done) <='] = $like['toaccom'];
				$where['DATE(move_forward_choice_done) >='] = $like['fromaccom'];
			}

			unset($like['search']);
			unset($like['fromredeem']);
			unset($like['toredeem']);
			unset($like['fromaccom']);
			unset($like['toaccom']);
			unset($like['prize_name']);
			unset($like['send_type']);
			unset($like['first_name']);

		}
		$param['like'] = $like;
		$param['table'] = $table;
		$param['where'] = $where;
		$param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id = ' . $table . '.registrant_id',
							  'tbl_move_forward' => 'tbl_move_forward.move_forward_id = ' . $table . '.move_forward_id',
							  'tbl_prizes' => 'tbl_move_forward.prize_id = tbl_prizes.prize_id');
		$prizes = $this->global_model->get_rows($param)->result_array();

		$row[] = array(	'Name',
						 'Email',
						 'Move FWD Title',
						 'Prize',
						 'Status',
						 'Type',
						 'Mode',
						 'Date Accomplished',
						 'Date Redeemed/Delivered'
						);
		if($prizes) {
			foreach($prizes as $k => $v) {
				 $status = 'Pending';
	              if($v['prize_delivered'] == 1) {
	                if($v['send_type'] == 'Delivery')
	                  $status = 'Delivered';
	                else
	                  $status = 'Redeemed';                                    
	              } 
	            $mode = $v['move_forward_choice'] == 1 ? 'Play' : 'Pledge';
	            $prize_delivered = $v['prize_delivered_date'] == 0 ? 'N/A' : date('F d, Y H:i:s', strtotime($v['prize_delivered_date']));
				$row[] = array($v['first_name'] . ' ' . $v['third_name'],
							 	$v['email_address'],
								$v['move_forward_title'],
								$v['prize_name'],
								$status,
								$v['send_type'],
								$mode,
								$v['move_forward_choice_done'],
								$prize_delivered);
			}
		}
		$this->to_excel_array->to_excel($row, 'completed_movefwd_'.date("YmdHis"));
	}

	public function comment() {
		$like = array();
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			if($like['fromavail'] && $like['toavail']) {
				$from = $like['fromavail'] > $like['toavail'] ?  $like['toavail'] :  $like['fromavail'];
				$to = $like['toavail'] < $like['fromavail'] ?  $like['fromavail'] :  $like['toavail'];
				$where['DATE(date_created) <='] = $to;
				$where['DATE(date_created) >='] = $from;
			}
			if(isset($like['id'])) {
				$where['comment_id'] = $like['id'];
			}
			unset($like['search']);
			unset($like['fromredeem']);
			unset($like['fromavail']);
			unset($like['toavail']);
			unset($like['toredeem']);
			unset($like['id']);
		}
		$param['like'] = $like;
		$param['table'] = 'tbl_comments';
		$param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id = tbl_comments.registrant_id');
		$flash_offers = $this->global_model->get_rows($param)->result_array();
		$exclude = array(FLASH_OFFERS, BIRTHDAY_OFFERS);
		$origins = $this->module_model->get_origins($exclude);
		$row[] = array(	'Name',
						 'Comment',
						 'Origin',
						 'Status',
						 'Date');
		if($flash_offers) {
			$status = array('Pending', 'Approved', 'Rejected');
			foreach($flash_offers as $k => $v) {
				$the_status = $v['comment_status'] ?  $status[$v['comment_status']] : 'Pending';
				$row[] = array($v['first_name'] . ' ' . $v['third_name'],
							  $v['comment'],
							  $origins[$v['origin_id']],
							  $the_status,
							  $v['comment_date_created']);
			}
		}
		
		$this->to_excel_array->to_excel($row, 'comment_'.date("YmdHis"));
	}

	public function reply() {
		$like = array();
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			if($like['fromavail'] && $like['toavail']) {
				$from = $like['fromavail'] > $like['toavail'] ?  $like['toavail'] :  $like['fromavail'];
				$to = $like['toavail'] < $like['fromavail'] ?  $like['fromavail'] :  $like['toavail'];
				$where['DATE(date_created) <='] = $to;
				$where['DATE(date_created) >='] = $from;
			}

			if($like['comment_id']) {
				$like['tbl_comment_replies.comment_id'] = $like['comment_id'];
			}
			unset($like['search']);
			unset($like['comment_id']);
			unset($like['fromredeem']);
			unset($like['fromavail']);
			unset($like['toavail']);
			unset($like['toredeem']);
		}
		$param['like'] = $like;
		$param['table'] = 'tbl_comment_replies';
		$param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id = tbl_comment_replies.registrant_id',
							 'tbl_comments'		=> 'tbl_comments.comment_id = tbl_comment_replies.comment_id');
		$flash_offers = $this->global_model->get_rows($param)->result_array();
		$row[] = array(	'Name',
						 'Comment',
						 'Reply To',
						 'Status',
						 'Date');
		if($flash_offers) {
			$status = array('Pending', 'Approved', 'Rejected');
			foreach($flash_offers as $k => $v) {
				$the_status = $v['comment_reply_status'] ?  $status[$v['comment_reply_status']] : 'Pending';
				$row[] = array($v['first_name'] . ' ' . $v['third_name'],
							  $v['comment_reply'],
							  $v['comment'],
							  $the_status,
							  $v['comment_reply_date_created']);
			}
		}
		
		$this->to_excel_array->to_excel($row, 'reply_'.date("YmdHis"));
	}
	
	public function _remap($method) {
		if($method == 'registrants')
			$this->registrants();
		if($method == 'profiles')
			$this->profiles();
		if($method == 'statements')
			$this->statements();
		if($method == 'flash_offer')
			$this->flash_offer();
		if($method == 'category')
			$this->category();
		if($method == 'prizes')
			$this->prizes();
		if($method == 'birthday_offer')
			$this->birthday_offer();
		if($method == 'move_fwd_prizes')
			$this->move_fwd_prizes();
		if($method == 'move_fwd')
			$this->move_fwd();
		if($method == 'move_fwd_gallery')
			$this->move_fwd_gallery();
		if($method == 'comment')
			$this->comment();
		if($method == 'reply')
			$this->reply();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
