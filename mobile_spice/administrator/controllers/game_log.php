<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Game_log extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('game_model');
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$where = '';
		if(isset($_GET['search'])) {
			$where =  " AND CONCAT(first_name, ' ', third_name) LIKE '%$_GET[name]%'";
			$where .=  " AND score LIKE '%$_GET[score]%'";
			$where .= $_GET['game_id'] != '' ? " AND g.game_id = '$_GET[game_id]'" : "";
			$where .= $_GET['is_cheater'] != '' ? " AND gs.is_cheater = '$_GET[is_cheater]'" : "";
			$where .= $_GET['from'] ? " AND date_played >= '$_GET[from]'" : "";
			$where .= $_GET['to'] ? " AND date_played <= '$_GET[to]'" : "";
		}
		$data['logs'] = $this->game_model->get_game($where, $page, PER_PAGE, $records);
		$data['offset'] = ($page - 1) * PER_PAGE;
		$param = array();
		$param['table'] = 'tbl_games';
		$param['order_by'] = array('field' => 'name', 'order'	=> 'ASC');
		$param['fields'] = 'game_id, name';
		$games = $this->global_model->get_rows($param)->result_array();
		$data['games'] = $games;

		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/game_log');
		$access = $this->module_model->check_access('game_log');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = $records;
		return $this->load->view('game_log/index', $data, true);		
	}

	public function export() {
		$this->load->library('to_excel_array');
		$where = '';
		if(isset($_GET['search'])) {
			$where =  " AND CONCAT(first_name, ' ', third_name) LIKE '%$_GET[name]%'";
			$where .=  " AND score LIKE '%$_GET[score]%'";
			$where .= $_GET['game_id'] != '' ? " AND g.game_id = '$_GET[game_id]'" : "";
			$where .= $_GET['is_cheater'] != '' ? " AND gs.is_cheater = '$_GET[is_cheater]'" : "";
			$where .= $_GET['from'] ? " AND date_played >= '$_GET[from]'" : "";
			$where .= $_GET['to'] ? " AND date_played <= '$_GET[to]'" : "";
		}
		$records = $this->game_model->get_game($where, null, null, $total, true);
		$row[] = array('Name', 
					   'Game',
					   'Score',
					   'Computed Score',
					   'Cheater',
					   'Date Played');
		if($records) {
			foreach($records as $k => $v) {
				$cheater = $v['is_cheater'] ? 'Yes' : 'No';
				$row[] = array($v['first_name'] . ' ' . $v['third_name'],
							  $v['name'],
							  $v['score'],
							  $v['computed_score'],
							  $cheater,
							  $v['date_played']);
			}
		}
		$this->to_excel_array->to_excel($row, 'games_'.date("YmdHis"));
	}

	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */