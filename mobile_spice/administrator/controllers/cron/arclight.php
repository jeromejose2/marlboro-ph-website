<?php

class Arclight extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Arclight_Model');
	}

	// 23	23	*	*	*
	public function push()
	{
		if ($filename = $this->Arclight_Model->push()) {
			$this->output->set_output($filename);
		}
	}

	// */15	*	*	*	*
	public function pull()
	{
		$this->Arclight_Model->pull_with_inbound_failed();
	}
}