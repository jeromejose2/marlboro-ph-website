<?php
class Birthday extends CI_Controller
{
	var $edm;
	public function __construct()
	{
		parent::__construct();
		require_once 'application/libraries/Edm.php';
		$this->edm = new Edm();
	}

	public function send_birthday_edm($email, $firstname, $lastname, &$r_error = false, &$s_error = false) {
		$recipient_added = $this->edm->add_recipient_birthday_edm(EDM_MARLBORO_BIRTHDAY_LIST_ID, $email, $firstname, $lastname);
		$sent = $this->edm->send_birthday_edm(EDM_MARLBORO_BIRTHDAY_MAIL_ID, $email, $firstname);

		if($recipient_added !== true)
			$r_error = $recipient_added;

		if($sent !== true)
			$s_error = $sent;
	}

	public function get_celebrants() {
		$celebrants = $this->db->select('registrant_id, first_name, third_name, date_of_birth, email_address')
							   ->from('tbl_registrants')
							   ->where('MONTH(date_of_birth)', date('n'))
							   ->where('DAY(date_of_birth)', date('j'))
							   ->get()
							   ->result_array();
		$logs = "";
		if($celebrants) {
			foreach ($celebrants as $key => $celebrant) {
				$logs .= date('Y-m-d H:i:s') . ' ' . $celebrant['date_of_birth'] . ' ' . $celebrant['first_name'] . ' ' . $celebrant['third_name'] . ' ' . $celebrant['email_address']   . "\n";
				$this->send_birthday_edm($celebrant['email_address'], $celebrant['first_name'], $celebrant['third_name'], $recipient_error, $send_error);	
			}
		}

		$fp = fopen('uploads/birthday_logs.txt', 'a+');
		fwrite($fp, $logs);
		fclose($fp);

		//$this->send_birthday_edm('rex.perez@nuworks.ph', 'Rex', 'Perez', $recipient_error, $send_error);	

	   /* $this->send_birthday_edm('rex.perez@nuworks.ph', 'Rex', 'Perez', $recipient_error, $send_error);	
	    $this->send_birthday_edm('mark.barcarse@nuworks.ph', 'Mark', 'Barcarse', $recipient_error, $send_error);	
	    $this->send_birthday_edm('nuworks.client1@gmail.com', 'Client', 'Test', $recipient_error, $send_error);	*/

		// var_dump($recipient_error);
		// var_dump($send_error);
	}
}