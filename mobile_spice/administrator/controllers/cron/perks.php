<?php

class Perks extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	// 23	23	*	*	*
	public function check_bid_items() {
		echo '<pre>';
		$time = date('YmdHis');
		$param['table'] = 'tbl_bid_items';
		$param['where'] = array('DATE(end_date) <=' => date('Y-m-d'), 'status'	=> 1, 'has_ended' => 0);
		$bid_items = $this->global_model->get_rows($param)->result_array();
		print_r($bid_items);
		if($bid_items) {
			foreach ($bid_items as $key => $value) {
				if($time >= date('YmdHis', strtotime($value['end_date']))) {
					$param = array();
					$param['has_ended'] = 1;
					$param['status'] = 0;
					$where = array('bid_item_id'	=> $value['bid_item_id']);
					$this->global_model->update_record('tbl_bid_items', $where, $param);

					$bidders = $this->award_bidders($value['bid_item_id'], $value['slots'], $value['bid_item_name']);
				}
			}
		}
	}

	public function award_bidders($id, $slots, $name) {
		include_once('application/models/points_model.php');
		$this->Points_Model = new Points_Model();
		$param['table'] = 'tbl_bids';
		$param['join'] = array('tbl_registrants' => 'tbl_bids.registrant_id = tbl_registrants.registrant_id');
		$param['where'] = array('bid_item_id'	=> $id, 'total_points >='	=> 'bid');
		$param['order_by'] = array('field'	=> 'bid', 'order'	=> 'desc');
		$param['offset'] = 0;
		$param['limit'] = $slots;
		$bidders = $this->global_model->get_rows($param)->result_array(); 
		print_r($bidders);
		if($bidders) {
			foreach ($bidders as $key => $value) {
				$this->Points_Model->spend(PERKS_BID, array(
											'points' => $value['bid'],
											'suborigin' => $value['bid_id'],
											'remarks' => '{{ name }} won the bid for ' . $name . ' on Perks',
											'registrant'	=> $value['registrant_id']
											));	
			}
		}
	}
}