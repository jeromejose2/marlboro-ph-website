<?php 

class Lamp_permissions extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		$this->_table = 'tbl_lamp_permissions';
 	}
	
	public function index($page = 1)
	{
		//$this->output->enable_profiler(TRUE);
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			unset($like['search']);
		}

		//$param['having'] = $like;
		$param['offset'] = $data['offset'];
		$param['limit'] = PER_PAGE;
		$param['table'] = $this->_table;
		$param['join'] = array('tbl_cms_users' => 'tbl_lamp_permissions.cms_user_id = tbl_cms_users.cms_user_id',
								'tbl_lamps' => 'tbl_lamps.lamp_id = tbl_lamp_permissions.lamp_id'); 
		$param['order_by'] = array('field'=>'tbl_lamp_permissions.created_date','order'=>'DESC');
		//$param['where'] = array('tbl_lamp_permissions.is_deleted' => 0);
		//$param['having'] = array('challenges LIKE ' => '%' . $challenge . '%', 'is_deleted' => 0);
		$param['like'] = $like;
		$data['categories'] = $this->global_model->get_rows($param)->result_array();
		$records = $this->global_model->get_total_rows($param);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/lamp_permissions');
		$access = $this->module_model->check_access('lamp_permissions');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = $records;
		return $this->load->view('perks/reserve/lamps/permissions/index', $data, true);		
	}

	public function export() {
		$like = array();
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			unset($like['search']);
		}

		//$param['having'] = $like;
		$param['table'] = $this->_table;
		//$param['join'] = array('tbl_challenges', 'tbl_challenges.move_forward_id = tbl_move_forward.move_forward_id'); 
		$param['join'] = array('tbl_cms_users' => 'tbl_lamp_permissions.cms_user_id = tbl_cms_users.cms_user_id',
								'tbl_lamps' => 'tbl_lamps.lamp_id = tbl_lamp_permissions.lamp_id'); 
		$param['order_by'] = array('field'=>'tbl_lamp_permissions.created_date','order'=>'DESC');
		//$param['having'] = array('challenges LIKE ' => '%' . $challenge . '%', 'is_deleted' => 0);
		$param['like'] = $like;
		$records = $this->global_model->get_rows($param)->result_array();
		$row[] = array(	'Name',
						'LAMP');
		if($records) {
				foreach($records as $k => $v) {
					$row[] = array($v['name'], $v['lamp_name']);
				}
			}		
		$this->load->library('to_excel_array');
		$this->to_excel_array->to_excel($row, 'lamps_'.date("YmdHis"));
	}
	
	public function add() {
		$data['main_content'] = $this->add_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function add_content() {
		$error = '';
		if($this->input->post('submit')) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules($this->get_rules());
			$valid = $this->form_validation->run();	
			if($valid) {
				$post = $this->input->post();
				if(!$error) {
					$id = $this->global_model->insert($this->_table, $post);
					$post = array();
					$post['url'] = SITE_URL . '/move_fwd/add';
					$post['description'] = 'added a new move fwd item';
					$post['table'] = 'tbl_move_forward';
					$post['record_id'] = $id;
					$post['type'] = 'add';
					$this->module_model->save_audit_trail($post);
					redirect('lamp_permissions');	
				}
			} else {
				$error = validation_errors();
			}
		}
		$data['error'] = $error;
		$data['record'] = $_POST;
		$data['lamps'] = $this->get_lamps();
		return $this->load->view('perks/reserve/lamps/permissions/add', $data, true);			
	}

	public function get_avail_users() {
		$lamp_id = $this->input->post('id');
		$query = "SELECT * FROM tbl_cms_users 
				  WHERE cms_user_id NOT IN (SELECT cms_user_id FROM tbl_lamp_permissions WHERE lamp_id = $lamp_id)
				  ORDER BY name";
		$users = $this->global_model->custom_query($query)->result_array();
		$data['users'] = $users;
		$this->load->view('perks/reserve/lamps/permissions/user', $data);	

	}

	public function get_lamps() {
		$param['table'] = 'tbl_lamps';
		$param['order_by'] = array('field'	=> 'lamp_name', 'order'	=> 'ASC');
		$param['where'] = array('status'	=> 1, 'is_deleted'	=> 0);
		$lamps = $this->global_model->get_rows($param)->result_array();
		return $lamps;
	}

	public function delete() {
		$table = $this->_table;
		$id = $this->uri->segment(3);
		$field = 'lamp_permission_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/lamp_permissions') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$where[$field] = $id;
			$this->global_model->delete($table, $where);
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	private function get_rules() {
		$config = array(
		   array(
				 'field'   => 'cms_user_id',
				 'label'   => 'user',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'lamp_id',
				 'label'   => 'LAMP',
				 'rules'   => 'required'
			  )
		);
		return $config;
	}

	
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		elseif($method == 'export')
			$this->export();
		elseif(is_numeric($method))
			$this->index();
		else
			$this->{$method}();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */