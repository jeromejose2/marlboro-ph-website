<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mym_dump extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		$this->_table = 'tbl_mym_dumps';
		// define('DATE_OF_CAPTURE', 1);
		// define('FIRST_NAME', 14);
		// define('LAST_NAME', 15);
		// define('EMAIL_ADDRESS', 16);
		// define('STREET', 17);
		// define('CITY', 18);
		// define('PROVINCE', 19);
		// define('ZIP', 20);
		// define('LANDLINE', 22);
		// define('MOBILE', 23);
		// define('DATE_OF_BIRTH', 25);
		// define('GENDER', 27);
		// define('GIID', 31);
		// define('PRIMARY_BRAND', 37);
		// define('SECONDARY_BRAND', 38);

		define('DATE_OF_CAPTURE', 0);
		define('FIRST_NAME', 1);
		define('LAST_NAME', 2);
		define('EMAIL_ADDRESS', 3);
		define('STREET', 4);
		define('CITY', 5);
		define('PROVINCE', 6);
		define('ZIP', 7);
		define('COUNTRY', 8);
		define('MOBILE', 9);
		define('DATE_OF_BIRTH', 10);
		define('GENDER', 11);
		define('GIID', 12);
		define('PRIMARY_BRAND', 13);
		define('SECONDARY_BRAND', 14);
		define('DOCUMENT_TYPE', 15);
	}
	
	public function index() {
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		$where = array();
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			
			if($like['fromdate'] != '')
				$where['DATE(date_created) >='] = $like['fromdate'];
			if($like['todate'] != '')
				$where['DATE(date_created) <='] = $like['todate'];
			unset($like['fromdate']);
			unset($like['todate']);
			unset($like['search']);
		}
		$param['like'] = $like;
		$param['where'] = $where;
		$param['offset'] = $data['offset'];
		$param['table'] = $this->_table;
		$param['order_by'] = array('field'=>'date_created','order'=>'DESC');
		$data['categories'] = $this->global_model->get_rows($param)->result_array();
		$records = $this->global_model->get_total_rows($param);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/module');
		$access = $this->module_model->check_access('prize');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = $records;
		$data['origins'] = $this->module_model->get_origins();
		return $this->load->view('mym/dump/index', $data, true);		
	}

	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}

	private function upload_csv(&$error = false) {
		$this->load->helper('upload_helper');
		$config['upload_path'] = 'uploads/mym/';
		$config['allowed_types'] = '*';
		$config['max_size']	= '8072';
		$config['file_name'] = uniqid() . '.' . end(explode('.', $_FILES['csv']['name']));
		$this->load->library('upload', $config);

		
		//$uploaded = upload($config);
		if ( ! $this->upload->do_upload('csv')) {
			$error = array('error' => $this->upload->display_errors());
			//$this->load->view('upload_form', $error);
		}
		else
		{
			$data = $this->upload->data();
			return $data['file_name'];
			//$this->load->view('upload_success', $data);
		}
	}

	public function upload() {
		$filename = $this->upload_csv($error);
		$this->global_model->insert('tbl_mym_dumps', array('filename'	=> $filename));
		$this->insert_contents($filename);

	}

	public function insert_contents($filename) {
		// define('DATE_OF_CAPTURE', 1);
		// define('FIRST_NAME', 14);
		// define('LAST_NAME', 15);
		// define('EMAIL_ADDRESS', 16);
		// define('STREET', 17);
		// define('CITY', 18);
		// define('PROVINCE', 19);
		// define('ZIP', 20);
		// define('LANDLINE', 22);
		// define('MOBILE', 23);
		// define('DATE_OF_BIRTH', 25);
		// define('GENDER', 27);
		// define('GIID', 31);
		// define('PRIMARY_BRAND', 37);
		// define('SECONDARY_BRAND', 38);
		$filename = 'uploads/mym/' . $filename;
		$success = $failed = $duplicates = array();
		//$inserted = $duplicates = $failed = 0;
		if (($handle = fopen("$filename", "r")) !== FALSE) {
			 while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
			   $individual_id = $this->db->select('*')->from('tbl_individual_id')->get()->row_array();
			   @list($month, $day, $year) = explode('/', $data[DATE_OF_BIRTH]);
			   $post = array('date_of_capture' 		=> $data[DATE_OF_CAPTURE], 
			   	  			'first_name' 			=> $data[FIRST_NAME], 
			   	  			'third_name' 			=> $data[LAST_NAME], 
			   	  			'email_address' 		=> $data[EMAIL_ADDRESS], 
			   	  			'street_name' 			=> $data[STREET], 
			   	  			'city' 					=> $data[CITY], 
			   	  			'province' 				=> $data[PROVINCE], 
			   	  			'postal_code' 			=> $data[ZIP], 
			   	  			'country' 				=> $data[COUNTRY], 
			   	  			'mobile_phone' 			=> $data[MOBILE], 
			   	  			'date_of_birth' 		=> $year . '-' . $month . '-' . $day, 
			   	  			'gender' 				=> $data[GENDER], 
			   	  			'government_id_number' 	=> $data[GIID],
			   	  			'current_brand' 		=> $data[PRIMARY_BRAND],
			   	  			'first_alternate_brand' => $data[SECONDARY_BRAND],
			   	  			'government_id_type' 	=> $data[DOCUMENT_TYPE], 
			   	  			'status'				=> 1,
			   	  			'individual_id'			=> $individual_id['individual_id'] + 1,
			   	  			'date_created'			=> date('Y-m-d H:i:s'), 
			   	  			'from_site'				=> 0);
			    $existing = $this->db->select()->from('tbl_registrants')->where('email_address', $data[EMAIL_ADDRESS])->get()->num_rows();
			    if(!$existing) {
					require_once 'application/libraries/Edm.php';
					require_once 'application/helpers/site_helper.php';
					$edm = new Edm();
					$email = $data[EMAIL_ADDRESS];
					$username = $email;
					$firstname = $data[FIRST_NAME];
					$lastname = $data[LAST_NAME];
					$this->load->library('Encrypt');
					$salt = generate_password(20);
					$password = generate_password();
					$encoded_password = $this->encrypt->encode($password, $salt);
					$post['encryption_changed'] = 1;
					$post['salt'] = $salt;
					$post['password'] = $encoded_password;
					
					$edm->add_recipient(EDM_MARLBORO_WELCOME_LIST_ID, $email, $username, $firstname, $lastname, $password);
					$sendReturn = $edm->send(EDM_MARLBORO_WELCOME_MAIL_ID, $email, $username, $firstname, $password); 
					if ($sendReturn !== true) {
						$post['edm_error_message'] = json_encode($sendReturn);
						$failed[] = $post;
					} else {
						$post['edm_error_message'] = null;
						$post['edm_status'] = 1;
						$this->db->insert('tbl_registrants', $post);
						$this->db->update('tbl_individual_id', array('individual_id' => $individual_id['individual_id'] + 1));	
						$success[] = $post;
					}
			    } else {
			    	$duplicates[] = $post;
			    }
			   
		    }
		    fclose($handle);
		}

		$data['main_content'] = $this->insert_main_content($success, $duplicates, $failed);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	} 

	private function insert_main_content($success, $duplicates, $failed) {
		$data['duplicates'] = $duplicates;
		$data['success'] = $success;
		$data['failed'] = $failed;
		return $this->load->view('mym/dump/report', $data, true);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
