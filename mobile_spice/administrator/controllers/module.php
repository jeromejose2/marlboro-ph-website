<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Module extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$data['modules'] = $this->module_model->get_module('', $page, PER_PAGE, $records);
		
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/module');
		$data['offset'] = ($page - 1) * PER_PAGE;
		return $this->load->view('module/index', $data, true);		
	}
	
	public function add() {
		$data['main_content'] = $this->add_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function add_content() {
		$error = '';
		if($this->input->post('submit')) {
			$valid = $this->validate_fields();
			if($valid) {
				$this->module_model->save_module();	
				redirect('module');
			} else {
				$error = 'Sorry, module name or uri already exists';
			}
		}
		$data['error'] = $error;
		$data['module'] = $_POST;
		return $this->load->view('module/add', $data, true);			
	}
	
	public function edit() {
		$id = $this->uri->segment(3);
		$data['main_content'] = $this->edit_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function edit_content($id) {
		$error = '';
		if($this->input->post('submit')) {
			$valid = $this->validate_fields();
			if($valid) {
				$this->module_model->save_module(true);	
				redirect('module');
			} else {
				$error = 'Sorry, module name or uri already exists';
			}
		}
		$data['error'] = $error;
		$data['module'] = $_POST ? $_POST : $this->module_model->get_module(array('module_id'	=> $id))[0];
		
		return $this->load->view('module/add', $data, true);			
	}
	
	
	public function validate() {
		$valid = $this->validate_fields();
		$data['main_content'] = $valid ? '1' : '0';
		$this->load->view('blank-template', $data); 
	}
	
	private function validate_fields() {
		$name = $this->input->post('module_name');
		$uri = $this->input->post('uri');
		$id = $this->input->post('id');
		$where['module_name'] = $name;
		$where['uri'] = $uri;
		$module = $this->module_model->get_module($where);
		if($module) {
			if($id && $module[0]['module_id'] == $id)
				return true;
			return false;
		}
		
		return true;
	}
	
	public function delete() {
		$table = 'tbl_modules';
		$id = $this->uri->segment(3);
		$field = 'module_id';
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/module') >= 0) {
			$where[$field] = $id;
			$this->global_model->delete_record($table, $where);
		}
		redirect('module');
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */