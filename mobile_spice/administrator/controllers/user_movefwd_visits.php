<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class User_movefwd_visits extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');

 	} 

 	 
 	public function index()
	{
		if(!$this->input->get('fromdate') || !$this->input->get('todate')) {
			redirect('user_movefwd_visits?fromdate=' . date('Y-m-d', strtotime('-30 days')) .  '&todate=' . date('Y-m-d'));
		}
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
		//$this->output->enable_profiler(TRUE);

 	}
 	 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
		$this->load->helper('paging');
		$offset = (int)$this->input->get('per_page');
	    $limit = PER_PAGE;
	    $filters = $this->get_filters(); 

		$access = $this->module_model->check_access('user_games');
		$total_rows = $this->global_model->get_rows(array('table'=>'tbl_page_visits as v',
															 'where'=>array_merge($filters['where_filters'],array()),
														     'like'=>$filters['like_filters'],
														     'join'=>array('tbl_login'=>'tbl_login.login_id=v.login_id', 
														     				'tbl_registrants' => 'tbl_login.registrant_id=tbl_registrants.registrant_id'),
 														     'group_by'=>'tbl_registrants.registrant_id')
														)->num_rows();
 		$data['header_text'] = 'User MoveFWD Visits';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = $total_rows;
		$data['query_strings'] = $filters['query_strings'];
		$field = $this->input->get('field') ? $this->input->get('field') : 'date_visited';
		$sort = $this->input->get('sort') ? $this->input->get('sort') : 'desc';
 		
		$data['record'] = $this->global_model->get_rows(array('table'=>'tbl_page_visits as v',
															 'where'=>array_merge($filters['where_filters'],array()),
														     'like'=>$filters['like_filters'],
														     'join'=>array('tbl_login'=>'tbl_login.login_id=v.login_id', 
														     				'tbl_registrants' => 'tbl_login.registrant_id=tbl_registrants.registrant_id'),
 														     'offset'=>$offset,
														     'limit'=>$limit,
														     'order_by'=>array('field'=>$field,'order'=>$sort),
														     'fields'=>'v.*,tbl_registrants.first_name,tbl_registrants.third_name,tbl_registrants.registrant_id,COUNT(v.visit_id) AS count',
														     'group_by'=>'tbl_registrants.registrant_id')
														)->result_array(); 
		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']);
 		return $this->load->view('move_fwd/user',$data,TRUE);
	} 

	

	public function get_filters()
	   {
		   
		   $where_filters = array('DATE(date_visited) >='=>'fromdate','DATE(date_visited) <='=>'todate','registrant_id'=>'registrant_id');
		   $like_filters = array("CONCAT(first_name,' ',third_name)"=>'name', 'uri'	=> 'uri');
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		    foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 				}
									 
				  
 		   } 
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 				}

 				if($column_field == 'uri' && !$this->input->get($filter)) {
 					$valid_like_filters[$column_field] = 'move_forward';	
 				}
					
				 
				   
		   } 

 		   $query_strings = $this->input->get() ? http_build_query($this->input->get()) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?' . $query_strings);		   
		   
	   } 

	   public function view() {
	   		$registrant_id = $this->input->get('id');
	   		$filters = $this->get_filters(); 
	   		$data['registrant'] = $this->explore_model->get_row(array('table'=>'tbl_registrants',
	   																	'where'=>array('registrant_id'=>$registrant_id)
	   																)
 	   															);
	   		$record = $this->global_model->get_rows(array('table'=>'tbl_page_visits',
														 'where'=>array_merge($filters['where_filters'],array('registrant_id'=>$registrant_id)),
													     'like'=>$filters['like_filters'],
													     'join'=>array('tbl_login'=>'tbl_login.login_id=tbl_page_visits.login_id'),
													     'order_by'=>array('field'=>'date_visited','order'=>'asc')));
	   		$data['rows'] = $record;
	   		$this->load->view('move_fwd/user_views',$data);
	   }


	   public function export()
	   {
	   		$this->load->library('to_excel_array');

		   	$filters = $this->get_filters();
		   	$field = $this->input->get('field') ? $this->input->get('field') : 'date_visited';
			$sort = $this->input->get('sort') ? $this->input->get('sort') : 'desc';
		   	$query = $this->global_model->get_rows(array('table'=>'tbl_page_visits as v',
															 'where'=>array_merge($filters['where_filters'],array()),
														     'like'=>$filters['like_filters'],
														     'join'=>array('tbl_login'=>'tbl_login.login_id=v.login_id', 
														     				'tbl_registrants' => 'tbl_login.registrant_id=tbl_registrants.registrant_id'),
 														     'order_by'=>array('field'=>$field,'order'=>$sort),
														     'fields'=>'v.*,tbl_registrants.first_name,tbl_registrants.third_name,tbl_registrants.registrant_id,COUNT(v.visit_id) AS count',
														     'group_by'=>'tbl_registrants.registrant_id')
														);
		   	$from = $this->input->get('fromdate');
		   	$to = $this->input->get('todate');
		   	if($from && $to) 
		   		$res[] = array('User MoveFWD Visits (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')');
		   	$res[] = array('Name','Count');

		   	if($query->num_rows()){
		   		foreach($query->result() as $v){
 		   			$res[] = array($v->first_name.' '.$v->third_name,$v->count);
		   		}
		   	}
		   	$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));

	   }
}
