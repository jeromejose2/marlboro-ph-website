<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct() {
		
		parent::__construct();
		$this->load->model(array('registrant_model','explore_model','report_model'));
 	} 

		
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$data = $this->module_model->get_nav_data();
		$data['stats'] = $this->registrant_model->get_registrant_stats();
		$data['movefwd'] = $this->report_model->get_move_fwd_stats();
		$data['audit_trail'] = $this->module_model->get_audit_trail('', 1, PER_PAGE, $audit_trail_count);
		$data['status'] = $this->registrant_model->get_status();

		$event_photos_pending = (int)$this->explore_model->get_rows(array('table'=>'tbl_about_events_photos as p',
													 'where'=>array('p.is_deleted'=>0,'p.status'=>0, 'e.is_deleted'=>0),
													 'join'=>array('table'=>'tbl_about_events as e','on'=>'e.about_event_id=p.about_event_id'),
												     'order_by'=>array('field'=>'p.date_added','order'=>'DESC'),
												     'fields'=>'p.photo_id'
 													)
												)->num_rows();
		$event_photos_approved = (int)$this->explore_model->get_rows(array('table'=>'tbl_about_events_photos as p',
													 'where'=>array('p.is_deleted'=>0,'p.status'=>1, 'e.is_deleted'=>0),
													 'join'=>array('table'=>'tbl_about_events as e','on'=>'e.about_event_id=p.about_event_id'),
												     'order_by'=>array('field'=>'p.date_added','order'=>'DESC'),
												     'fields'=>'p.photo_id'
 													)
												)->num_rows();

		$event_photos_disapproved = (int)$this->explore_model->get_rows(array('table'=>'tbl_about_events_photos as p',
													 'where'=>array('p.is_deleted'=>0,'p.status'=>2, 'e.is_deleted'=>0),
													 'join'=>array('table'=>'tbl_about_events as e','on'=>'e.about_event_id=p.about_event_id'),
												     'order_by'=>array('field'=>'p.date_added','order'=>'DESC'),
												     'fields'=>'p.photo_id'
 													)
												)->num_rows();

		$maybe_statements = (int)$this->explore_model->get_rows(array('table'=>'tbl_statements',
													 'where'=>array('is_deleted'=>0),
													 'fields'=>'statement_id'
 													)
												)->num_rows();
		
		$about_videos_pending = (int)$this->explore_model->get_rows(array('table'=>'tbl_about_videos','where'=>array('is_deleted'=>0,'status'=>0)))->num_rows();

		$about_videos_approved = (int)$this->explore_model->get_rows(array('table'=>'tbl_about_videos','where'=>array('is_deleted'=>0,'status'=>1)))->num_rows();

		$about_videos_disapproved = (int)$this->explore_model->get_rows(array('table'=>'tbl_about_videos','where'=>array('is_deleted'=>0,'status'=>2)))->num_rows();
		

		$data['audit_trails'] = $audit_trail_count;
		$data['maybe_statements'] = $maybe_statements;
		$data['profiles'] = $this->registrant_model->get_profile_stats();
		$data['about_pending'] = $event_photos_pending + $about_videos_pending;
		$data['about_approved'] = $event_photos_approved + $about_videos_approved;
 		$data['about_disapproved'] = $event_photos_disapproved + $about_videos_disapproved;

		return $this->load->view('welcome', $data, true);		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */