<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Lamp_Promotions extends CI_Controller {
	
	public function __construct() {

		parent::__construct();

		$this->_table = 'tbl_lamp_promotions';

 	}

 	public function index() {

 		$data = array(
 			'main_content'	=> $this->main_content(),
 			'nav'			=> $this->nav_items()
 		);
 		$this->load->view('main-template', $data);

 	}

 	public function nav_items() {

 		$data = $this->module_model->get_nav_data();
 		return $this->load->view('nav', $data, TRUE);

 	}

 	public function main_content() {

 		$page = $this->uri->segment(2, 1);
 		$data['offset'] = ( $page - 1 ) * PER_PAGE;
 		$where = "tbl_lamp_promotions.is_deleted = 0";

 		if( isset($_GET['search']) ) {
 			$where .= isset($_GET['title']) && $_GET['title'] ? " AND promotion_title LIKE '%$_GET[title]%'" : FALSE;
 		}

 		$param = array(
 			'offset'	=> $data['offset'],
 			'limit'	=> PER_PAGE,
 			'table'	=> $this->_table,
 			'fields'	=> '*, tbl_lamp_promotions.date_created as date_created',
 			'where'	=> $where,
 			'join'	=> array(
 						'tbl_lamps' => 'tbl_lamps.lamp_id = tbl_lamp_promotions.lamp_id'
 					)
 		);
 		$data['users'] = $this->global_model->get_rows( $param )->result_array();
 		$records = $this->global_model->get_total_rows( $param );
 		$data['pagination'] = $this->global_model->pagination( $records, $page, SITE_URL . '/lamp_promotions' );
 		$data['total'] = $records;
 		$data['graph'] = $this->graph();

 		return $this->load->view('perks/reserve/lamps/promotions/lamp-promotions-list', $data, TRUE);

 	}

 	public function export() {

 		$where = "tbl_lamp_promotions.is_deleted = 0";

 		if( isset($_GET['search']) ) {
 			$where .= isset($_GET['title']) && $_GET['title'] ? " AND promotion_title LIKE '%$_GET[title]%'" : FALSE;
 		}

 		$param = array(
 			'offset'	=> 0,
 			'table'	=> $this->_table,
 			'fields'	=> '*, tbl_lamp_promotions.date_created as date_created',
 			'where'	=> $where,
 			'join'	=> array(
 						'tbl_lamps' => 'tbl_lamps.lamp_id = tbl_lamp_promotions.lamp_id'
 					)
 		);
 		$records = $this->global_model->get_rows( $param )->result_array();

 		$row[] = array(
 			'#',
 			'LAMP Name',
 			'Promotion Title',
 			'Date Created'
		);
		if( $records ) {
			foreach( $records as $k => $v ) {
				$row[] = array(
					$k + 1,
					$v['lamp_name'],
					$v['promotion_title'],
					date('F d, Y H:i:s', strtotime($v['date_created']))
				);
			}
		}

		$this->load->library('to_excel_array');
		$this->to_excel_array->to_excel( $row, 'lamp_promotions_' . date('YmdHis') );

 	}

 	public function add() {

 		$data = array(
 			'main_content' => $this->add_content(),
 			'nav' => $this->nav_items()
 		);
 		$this->load->view('main-template', $data);

 	}



 	public function add_content() {

 		$error = '';

 		if($this->input->post('submit')) {
 			$this->load->library('form_validation');
 			$this->form_validation->set_rules($this->get_rules());
 			$valid = $this->form_validation->run();
 			if($valid) {
 				$post = $this->input->post();
 				$id = $this->global_model->insert($this->_table, $post);

 				$trail_data = array(
 					'url' => SITE_URL . '/lamp_promotions/add',
 					'description' => 'added a new lamp promotion',
 					'table' => 'tbl_lamp_promotions',
 					'record_id' => $id,
 					'type' => 'add'
 				);
 				$this->module_model->save_audit_trail($trail_data);

 				redirect('lamp_promotions');
 			} else {
 				$error = validation_errors();
 			}
 		}

 		$data['error'] = $error;

 		$param = array(
 			'table' => 'tbl_lamps',
 			'order_by' => array(
 				'field' => 'lamp_name',
 				'order' => 'ASC'
 			)
 		);

 		$data['lamps'] = $this->global_model->get_rows($param)->result_array();
 		$data['action'] = SITE_URL . '/lamp_promotions/add';

 		return $this->load->view('perks/reserve/lamps/promotions/add', $data, TRUE);

 	}

 	public function edit() {

 		$data = array(
 			'main_content'	=> $this->edit_content($this->uri->segment(3)),
 			'nav'			=> $this->nav_items()
 		);
 		$this->load->view('main-template', $data);

 	}

 	private function edit_content($id) {

 		$error = '';
 		$param = array(
 			'table' => $this->_table,
 			'where' => array(
 					'lamp_promotion_id' => $id
 				)
 		);
 		$record = (array)$this->global_model->get_row($param);

 		$param = array(
 			'table' => 'tbl_lamps',
 			'order_by' => array(
 				'field' => 'lamp_name',
 				'order' => 'ASC'
 			)
 		);

 		$data['lamps'] = $this->global_model->get_rows($param)->result_array();
 		$data['action'] = SITE_URL . '/lamp_promotions/edit/' . $id;

 		$old_content = $new_content = array();

 		if($this->input->post('submit')) {
 			$this->load->library('form_validation');
 			$this->form_validation->set_rules($this->get_rules());
 			$valid = $this->form_validation->run();
 			if($valid) {
 				$post = $this->input->post();
 				$this->global_model->update($this->_table, $post, array('lamp_promotion_id' => $id));

 				$trail_data = array(
 					'url' => SITE_URL . '/lamp_promotions/edit/' . $id,
 					'description' => 'updated a lamp promotion',
 					'table' => 'tbl_lamp_promotions',
 					'record_id' => $id,
 					'type' => 'edit'
 				);
 				$this->module_model->save_audit_trail($trail_data);

 				redirect('lamp_promotions');
 			} else {
 				$error = validation_errors();
 			}
 		}

 		$data['error'] = $error;
 		$data['record'] = $record;

 		return $this->load->view('perks/reserve/lamps/promotions/add', $data, TRUE);

 	}

 	private function get_rules() {

 		$config = array(
 			array(
 				'field' => 'lamp_id',
 				'label' => 'lamp id',
 				'rules' => 'required'
 			),
 			array(
 				'field' => 'promotion_title',
 				'label' => 'promotion title',
 				'rules' => 'required'
 			),
 			array(
 				'field' => 'status',
 				'label' => 'status',
 				'rules' => 'required'
 			)
 		);

 		return $config;

 	}

 	public function delete() {

 		$table = $this->_table;
		$id = $this->uri->segment(3);
		$field = 'lamp_promotion_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/lamp_promotions') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$where[$field] = $id;
			$this->global_model->delete_record($table, $where);

			$trail_data = array(
				'url' => @$_SERVER['HTTP_REFERER'],
				'description' => 'deleted a lamp promotion',
				'table' => 'tbl_lamp_promotions',
				'record_id' => $id
			);
			$this->module_model->save_audit_trail($trail_data);
		}
		redirect('lamp_promotions');

 	}

 	private function graph() {

 		$function_get_days = function($param) {
 			$date = date("t", strtotime($param));

 			for($i = 1; $i <= $date; $i++) {
 				$every_date = date("Y") . "-" . date("m") . "-" . str_pad($i, 2, "0", STR_PAD_LEFT);

 				$data[] = array($every_date);
 			}

 			return $data;
 		};

 		$where = "tbl_lamp_promotions.is_deleted = 0";

 		$param = array(
 			'table'	=> $this->_table,
 			'fields'	=> '*, tbl_lamp_promotions.date_created as date_created',
 			'where'	=> $where,
 			'join'	=> array(
 						'tbl_lamps' => 'tbl_lamps.lamp_id = tbl_lamp_promotions.lamp_id'
 					)
 		);
 		$raw_result = $this->global_model->get_rows($param)->result_array();

 		foreach($raw_result as $k => $v) {
 			$final[$k]['date_created'] = $v['date_created'];
 		}

 		$date1 = @date('Y-m-d', strtotime(min($final[0])));
 		$date2 = @date('Y-m-d', strtotime(max($final[0])));

 		$dates_count = $function_get_days($date1);

 		for($i = 0; $i < count($dates_count); $i++) {
 			$param = array(
 				'table' => $this->_table,
 				'where' => "tbl_lamp_promotions.is_deleted = 0 AND date(date_created) = '" . $dates_count[$i][0] . "'",
 				'fields' => 'COUNT(*) as total'
 			);
 			$items = $this->global_model->get_rows($param)->result_array();
 			if($items) {
 				$checkins_daily['daily'][] = array($dates_count[$i][0], $items[0]['total']);
 			} else {
 				$checkins_daily["daily"][] = array($dates_count[$i][0], 0);
 			}
 		}

 		return $checkins_daily;

 	}

 	public function _remap($method) {

 		if( is_numeric($method) ) {
 			$this->index();
 		} elseif($method == 'edit') {
 			$this->edit();
 		} else {
 			$this->{$method}();
 		}

 	}
 
}