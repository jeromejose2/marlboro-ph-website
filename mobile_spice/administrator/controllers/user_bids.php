<?php 

class User_bids extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->_table = 'tbl_bids';
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	} 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		$where =  array();
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			$like['CONCAT(first_name, \' \', third_name)'] = $like['name'];
			if(!empty($like['bid_item_id']))
				$where['tbl_bids.bid_item_id'] = $like['bid_item_id'];

			if($like['from_bid'] && $like['to_bid']) {
				$where['DATE(bid_date) <='] = $like['to_bid'];
				$where['DATE(bid_date) >='] = $like['from_bid'];
			}
			$this->unset_val($like, array('search', 'name', 'bid_item_id', 'from_bid', 'to_bid'));
		}

		$param['offset'] = $data['offset'];
		$param['limit'] = PER_PAGE;
		$param['table'] = $this->_table;
		$param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id = ' . $this->_table . '.registrant_id',
							   'tbl_bid_items' =>	'tbl_bid_items.bid_item_id = tbl_bids.bid_item_id'); 
		$param['order_by'] = array('field'=>'bid_date','order'=>'DESC');
		$param['where'] = $where;
		$param['like'] = $like;
		$data['users'] = $this->global_model->get_rows($param)->result_array();
		//die($this->db->last_query());
		$records = $this->global_model->get_total_rows($param);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/user_bids');
		$access = $this->module_model->check_access('user_bids');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = $records;
		$data['items'] = $this->get_bid_items();
		return $this->load->view('perks/bid/user', $data, true);		
	}

	private function unset_val(&$arr, $exclude) {
		foreach ($arr as $key => $value) {
			if(in_array($key, $exclude)) {
				unset($arr[$key]);
			}
		}
	} 

	private function get_bid_items() {
		$param['table'] = 'tbl_bid_items';
		$param['order_by'] = array('field'	=> 'bid_item_name', 'order'	=> 'ASC');
		$param['where'] = array('is_deleted'	=> 0, 'status'	=> 1);
		$items = $this->global_model->get_rows($param)->result_array();
		return $items;
	}

	public function export()
	{
		$like = array();
		$where = array();
		$this->load->library('to_excel_array');
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			$like['CONCAT(first_name, \' \', third_name)'] = $like['name'];
			if(!empty($like['bid_item_id']))
				$where['tbl_bids.bid_item_id'] = $like['bid_item_id'];

			if($like['from_bid'] && $like['to_bid']) {
				$where['DATE(bid_date) <='] = $like['to_bid'];
				$where['DATE(bid_date) >='] = $like['from_bid'];
			}
			$this->unset_val($like, array('search', 'name', 'bid_item_id', 'from_bid', 'to_bid'));
		}

		$param['table'] = $this->_table;
		$param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id = ' . $this->_table . '.registrant_id',
							   'tbl_bid_items' =>	'tbl_bid_items.bid_item_id = tbl_bids.bid_item_id',
							   'tbl_provinces'	=> 'tbl_provinces.province_id = tbl_registrants.province',
							   'tbl_cities'	=> 'tbl_cities.city_id = tbl_registrants.city'); 
		$param['order_by'] = array('field'=>'bid_date','order'=>'DESC');
		$param['where'] = $where;
		$param['like'] = $like;
		$param['fields'] = '*, tbl_cities.city as cityname, tbl_provinces.province as provincename';
		$records = $this->global_model->get_rows($param)->result_array();

		$row[] = array('Name', 
						'Street',
					   'Barangay',
					   'City',
					   'Province',
					   'Mobile',
					   'Item',
					   'Bid',
					   'Current Points',
					   'Bid Start',
					   'Bid End',
					   'Bid Date');
		if($records) {
			foreach($records as $k => $v) {
				$row[] = array($v['first_name'] . ' ' . $v['third_name'],
							  $v['street_name'], 
							  $v['barangay'], 
							  $v['cityname'], 
							  $v['provincename'],
							  $v['mobile_phone'],
							  $v['bid_item_name'],
							  $v['bid'],
							  $v['total_points'],
							  $v['start_date'],
							  $v['end_date'],
							  $v['bid_date']);
			}
		}
		$this->to_excel_array->to_excel($row, 'user_bids_'.date("YmdHis"));
	}

	public function set_winner() {
		$id = $this->uri->segment(3);
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/user_bids') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->update_winner_status($id, 1, 'set a user as bid winner');
		}
		redirect('user_bids');
	}

	public function unset_winner() {
		$id = $this->uri->segment(3);
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/user_bids') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->update_winner_status($id, 0, 'unset a user as bid winner');
		}
		redirect('user_bids');
	}

	private function update_winner_status($id, $new_status, $action_text = '', $message = '') {
		
		$set['is_winner'] = $new_status;
		$where['bid_id'] = $id;
		$this->global_model->update_record($this->_table, $where, $set);

		$param['table'] = $this->_table;
		$param['where'] = $where;
		$param['join'] = array('tbl_bid_items i' => 'i.bid_item_id = ' . $this->_table . '.bid_item_id');
		$bid = (array) $this->global_model->get_row($param);


		include_once('application/models/points_model.php');
		include_once('application/models/notification_model.php');
		$this->Points_Model = new Points_Model();
		$this->notification_model = new Notification_Model();

		if($new_status == 1) {
			$this->global_model->update_record($this->_table, $where, array('set_winner_date' => date('Y-m-d H:i:s')));
			$this->Points_Model->spend(PERKS_BID, array(
											'points' => $bid['bid'],
											'suborigin' => $bid['bid_id'],
											'remarks' => '{{ name }} won the bid for ' . $bid['bid_item_name'] . ' on Perks',
											'registrant'	=> $bid['registrant_id']
											));	

			$param = array();
			$param['table'] = 'tbl_bids';
			$param['where'] = array('bid_item_id' => $bid['bid_item_id'], 'registrant_id !=' => $bid['registrant_id'], 'is_winner'	=> 0);
			$param['group_by'] = 'registrant_id';
			$non_winners = $this->global_model->get_rows($param)->result_array();
			
			if($non_winners) {
				foreach ($non_winners as $key => $value) {
					$message = 'Sorry! You did not win the ' . $bid['bid_item_name'] . ', but please do check out the other Bid items available here: <a href="' . BASE_URL . 'perks/bid">' . BASE_URL . 'perks/bid</a>';
					$param = array('message'=>$message,'suborigin'=>$id);
					$this->notification_model->notify($value['registrant_id'],PERKS_BID,$param);	
				}
			}
				

		} else {
			$this->Points_Model->earn(PERKS_BID, array(
											'points' => $bid['bid'],
											'suborigin' => $bid['bid_id'],
											'remarks' => '',
											'registrant'	=> $bid['registrant_id']
											));		
		}

		// if($new_status == 1) {
		// 	$type = $gallery['type'] == 'Photo' ? 1 : 2;
		// 	$this->points_model->earn(MOVE_FWD_WINNER, array('registrant'	=> $gallery['registrant_id'], 'suborigin'	=> $id, 'type'	=> $type));
		// } else {
		// 	$where = array();
		// 	$set['total_points'] = $gallery['total_points'] - MOVE_FWD_WINNER_BASE_POINT;
		// 	$where['registrant_id'] = $gallery['registrant_id'];
		// 	$this->global_model->update('tbl_registrants', $set, $where);

		// 	$where = array();
		// 	$where['origin_id'] = MOVE_FWD_WINNER;
		// 	$where['suborigin_id'] = $id;
		// 	$this->global_model->delete('tbl_points', $where);

		// }

		#save audit trail
		$new_content['is_winner'] = $new_status == 1 ? 'Yes' : 'No';
		$old_content['is_winner'] = $new_status == 1 ? 'No' : 'Yes';
		$post['url'] = @$_SERVER['HTTP_REFERER'];
		$post['description'] = $action_text;
		$post['table'] = $this->_table;
		$post['record_id'] = $id;
		$post['message'] = $message;
		$post['field_changes'] = serialize(array('old'	=> $old_content,
												  'new'	=> $new_content));
		$this->module_model->save_audit_trail($post);	

	}

	public function _remap($method) {
		if(is_numeric($method))
			$this->index();
		else 
			$this->{$method}();
	}
	
	// public function _remap($method) {
	// 	if($method == 'edit')
	// 		$this->edit($this->uri->segment(3));
	// 	elseif($method == 'delete')
	// 		$this->delete();
	// 	elseif($method == 'export')
	// 		$this->export();
	// 	else
	// 		$this->index();
	// }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */