<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Move_fwd_gallery extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		include_once('application/models/points_model.php');
		include_once('application/models/notification_model.php');
		$this->points_model = new Points_Model();
		$this->notification_model = new Notification_Model();
		$this->_table = 'tbl_move_forward_gallery';

		$this->load->model('move_fwd_model');
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		$where = '';
		$param = array();
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			if($like['fromavail'] && $like['toavail']) {
				$from = $like['fromavail'] > $like['toavail'] ?  $like['toavail'] :  $like['fromavail'];
				$to = $like['toavail'] < $like['fromavail'] ?  $like['fromavail'] :  $like['toavail'];
				$where['DATE(mfg_date_created) <='] = $to;
				$where['DATE(mfg_date_created) >='] = $from;
			}
			if($like['first_name']) {
				$like["CONCAT(first_name, ' ' , third_name)"] = $like['first_name'];
			}

			if($like['fromapp'] && $like['toapp']) {
				$where['DATE(mfg_date_approved) <='] = $like['toapp'];
				$where['DATE(mfg_date_approved) >='] = $like['fromapp'];
			}

			unset($like['first_name']);
			unset($like['search']);
			unset($like['fromredeem']);
			unset($like['fromavail']);
			unset($like['toavail']);
			unset($like['toredeem']);
			unset($like['fromapp']);
			unset($like['toapp']);

		}
		$param['like'] = $like;
		$param['table'] = $this->_table;
		$param['where'] = $where;
		$param['offset'] = $data['offset'];
		$param['limit'] = PER_PAGE;
		$param['order_by'] = array('field'	=> 'mfg_date_created', 'order'	=> 'DESC');
		//$param['join'] = @array_merge($param['join'], array('tbl_registrants' => 'tbl_registrants.registrant_id = ' . $this->_table . '.registrant_id'));
		$param['join'] = array('tbl_challenges' => 'tbl_challenges.challenge_id = ' . $this->_table . '.challenge_id',
							   'tbl_registrants' => 'tbl_registrants.registrant_id = ' . $this->_table . '.registrant_id',
							   'tbl_move_forward'	=> 'tbl_challenges.move_forward_id = tbl_move_forward.move_forward_id');
		$comments = $this->global_model->get_rows($param)->result_array();
		$records = $this->global_model->get_total_rows($param);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/move_fwd_gallery');
		$data['status'] = array('Pending', 'Approved', 'Rejected');
		
		$data['categories'] = $comments;
		$access = $this->module_model->check_access('move_fwd_gallery');
		$data['total'] = $records;
		$data['edit'] = $access['edit'];
		return $this->load->view('move_fwd/gallery/index', $data, true);		
	}

	private function get_title_field($origin_id) {
		$title = array(MOVE_FWD => 'move_forward_title');
		return $title[$origin_id];
	}

	private function get_title($origin_id, $suborigin_id) {
		$title = '';
		switch($origin_id) {
			case MOVE_FWD:
				$table = 'tbl_move_forward';
				$param['table'] = $table;
				$param['where'] = array('move_forward_id' => $suborigin_id);
				$row = (array)$this->global_model->get_row($param);
				$title = $row['move_forward_title'];
				break;
		}
		return $title;

	}

	private function get_join($origin_id) {
		$join = array();
		switch($origin_id) {
			case MOVE_FWD:
				$join = array('tbl_move_forward'	=> 'tbl_move_forward.move_forward_id = ' . $this->_table . '.suborigin_id');
				break;
		}
		return $join;

	}

	public function approve() {
		$id = $this->uri->segment(3);
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/move_fwd_gallery') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->update_status($id, 1, 'approved a move forward gallery');
		}
		redirect('move_fwd_gallery');
	}

	public function disapprove() {
		$id = $this->uri->segment(3);
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/move_fwd_gallery') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->update_status($id, 2, 'rejected a comment',  $this->input->post('message'));
		}
		redirect('move_fwd_gallery');
	}

	public function set_winner() {
		$id = $this->uri->segment(3);
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/move_fwd_gallery') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->update_winner_status($id, 1, 'set a move forward gallery as winner');
		}
		redirect('move_fwd_gallery');
	}

	public function unset_winner() {
		$id = $this->uri->segment(3);
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/move_fwd_gallery') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->update_winner_status($id, 0, 'unset a move forward gallery as winner');
		}
		redirect('move_fwd_gallery');
	}

	private function update_winner_status($id, $new_status, $action_text = '', $message = '') {
		$where['move_forward_gallery_id'] = $id;
		$param['where'] = $where;
		$param['table'] = $this->_table;
		$param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id  = ' . $this->_table . '.registrant_id',
							   'tbl_challenges'	=> 'tbl_challenges.challenge_id = ' . $this->_table . '.challenge_id');
		$gallery = (array)$this->global_model->get_row($param);
		if(!$gallery)
			redirect('move_fwd_gallery');

		$set['mfg_winner_status'] = $new_status;
		$this->global_model->update_record($this->_table, $where, $set);

		if($new_status == 1) {
			$type = $gallery['type'] == 'Photo' ? 1 : 2;
			$this->points_model->earn(MOVE_FWD_WINNER, array('registrant'	=> $gallery['registrant_id'], 'suborigin'	=> $id, 'type'	=> $type));
		} else {
			$where = array();
			$set['total_points'] = $gallery['total_points'] - MOVE_FWD_WINNER_BASE_POINT;
			$where['registrant_id'] = $gallery['registrant_id'];
			$this->global_model->update('tbl_registrants', $set, $where);

			$where = array();
			$where['origin_id'] = MOVE_FWD_WINNER;
			$where['suborigin_id'] = $id;
			$this->global_model->delete('tbl_points', $where);

		}

		#save audit trail
		$new_content['mfg_winner_status'] = $new_status == 1 ? 'Yes' : 'No';
		$old_content['mfg_winner_status'] = $new_status == 1 ? 'No' : 'Yes';
		$post['url'] = @$_SERVER['HTTP_REFERER'];
		$post['description'] = $action_text;
		$post['table'] = $this->_table;
		$post['record_id'] = $id;
		$post['message'] = $message;
		$post['field_changes'] = serialize(array('old'	=> $old_content,
												  'new'	=> $new_content));
		$this->module_model->save_audit_trail($post);	

	}

	private function update_status($id, $new_status, $action_text = '', $message = '') {
		$where['move_forward_gallery_id'] = $id;
		$param['where'] = $where;
		$param['table'] = $this->_table;
		$param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id  = ' . $this->_table . '.registrant_id');
		$gallery = (array)$this->global_model->get_row($param);
		if(!$gallery)
			redirect('move_fwd_gallery');

		#update status
		$set['mfg_status'] = $new_status;
		if($new_status == 1) {
			$user_details = $this->login_model->extract_user_details($_SESSION['logged']);
			$set['mfg_approved_by'] = $user_details['cms_user_id'];
		}
		$set['mfg_date_approved'] = date('Y-m-d H:i:s');	
		$this->global_model->update_record('tbl_move_forward_gallery', $where, $set);
		
		#save audit trail
		$status = $this->global_model->get_status();
		$new_content['mfg_status'] = $status[$new_status];
		$old_content['mfg_status'] = $status[$gallery['mfg_status']];
		$post['url'] = @$_SERVER['HTTP_REFERER'];
		$post['description'] = $action_text;
		$post['table'] = $this->_table;
		$post['record_id'] = $id;
		$post['message'] = $message;
		$post['field_changes'] = serialize(array('old'	=> $old_content,
												  'new'	=> $new_content));
		$this->module_model->save_audit_trail($post);	
		
		# points
		$points = 0;
		$challenge = $this->get_move_forward($gallery['challenge_id']);
		$nth = $this->get_n($gallery['challenge_id'],$gallery['registrant_id']);
		if($gallery['mfg_status'] == 0 && $new_status == 1) {
			$points = $this->points_model->earn(MOVE_FWD_GALLERY, array('suborigin'	=> $id,
														      'registrant'			=> $gallery['registrant_id'],
														      'remarks'				=> '{{ name }} accomplished ' . $nth . ' task of ' . $challenge['move_forward_title']  . ' for MoveFWD'));	
			/*if($challenge['type'] == 'Photo') {
				$this->points_model->earn(MOVE_FWD_UPLOAD_PHOTO, array('suborigin'	=> $id,
														      'registrant'			=> $gallery['registrant_id'],
														      'remarks'				=> '{{ name }} uploaded a photo on ' . $challenge['move_forward_title'] . ' on MoveFWD'));			
			} else {
				$this->points_model->earn(MOVE_FWD_UPLOAD_VIDEO, array('suborigin'	=> $id,
														      'registrant'			=> $gallery['registrant_id'],
														      'remarks'				=> '{{ name }} uploaded a video on ' . $challenge['move_forward_title'] . ' on MoveFWD'));	
			}*/
			
		}
		$this->is_complete_activity($id, $gallery['registrant_id'], $new_status, $gallery['mfg_status']);
		$this->is_complete_all_activity($id, $gallery['registrant_id'], $gallery['mfg_status']);

		# check if approved
		if($new_status == 1 && $points) {
			$where = array('move_forward_id'	=>  $challenge['move_forward_id'], 'registrant_id'	=> $gallery['registrant_id']);
			$set = array('move_forward_choice_started'	=> 1);
			$this->global_model->update_record('tbl_move_forward_choice', $where, $set);	
		
			$this->notification_model->notify($gallery['registrant_id'], MOVE_FWD_GALLERY, array(
															'message' => 'Your entry for ' . $challenge['challenge'] . ' on ' . $challenge['move_forward_title'] .  ' on MoveFWD has been approved. You gained ' . $points . ' points. Thank you!',
															'suborigin' => $id
															));

		} elseif($new_status == 2) {
			if($nth == '0') {
				//$where = array('move_forward_id'	=>  $challenge['move_forward_id'], 'registrant_id'	=> $gallery['registrant_id']);
				// $set = array('move_forward_choice_started'	=> 0);
				//$this->global_model->update_record('tbl_move_forward_choice', $where, $set);	
			}

			$this->notification_model->notify($gallery['registrant_id'], MOVE_FWD_GALLERY, array(
															'message' => 'Your entry for ' . $challenge['challenge'] . ' on ' . $challenge['move_forward_title'] .  ' on MoveFWD has been disapproved. Please try again!',
															'suborigin' => $id
															));
		}
	}

	private function get_n($challenge_id, $registrant_id) {
		$param['table'] = 'tbl_challenges';
		$param['where'] = array('challenge_id'	=> $challenge_id);
		$challenge = (array) $this->global_model->get_row($param);

		$param = array();
		$param['table'] = $this->_table;
		$param['join'] = array('tbl_challenges' => 'tbl_challenges.challenge_id = ' . $this->_table . '.challenge_id');
		$param['where'] = array('move_forward_id'	=> $challenge['move_forward_id'], 'mfg_status' => 1, 'registrant_id'	=> $registrant_id);
		$gallery = $this->global_model->get_rows($param)->result_array();
		$n = count($gallery);
		if($n == 1)
			return 'first';
		elseif($n == 2)
			return 'second';
		elseif($n == 3)
			return 'final';
		else
			return 0;

	}

	private function get_move_forward($challenge_id) {
		$param['table'] = 'tbl_challenges';
		$param['where'] = array('challenge_id'	=> $challenge_id);
		$param['join'] = array('tbl_move_forward' => 'tbl_challenges.move_forward_id = tbl_move_forward.move_forward_id');
		$challenge = (array) $this->global_model->get_row($param);	
		return $challenge;
	}

	private function is_complete_activity($id, $registrant_id, $status, $old_status) {
		$param['table'] = 'tbl_challenges';
		$param['where'] = array('move_forward_gallery_id'	=> $id);
		$param['join'] = array('tbl_move_forward_gallery' => 'tbl_challenges.challenge_id = ' . $this->_table . '.challenge_id',
							   'tbl_move_forward'		  => 'tbl_move_forward.move_forward_id = tbl_challenges.move_forward_id');
		$challenge = (array) $this->global_model->get_row($param);

		$param = array();
		$param['table'] = 'tbl_challenges';
		$param['where'] = array('move_forward_id'	=> $challenge['move_forward_id']);
		$challenges = $this->global_model->get_rows($param)->result_array();
		$challege_ids = array();
		if($challenges) {
			foreach ($challenges as $key => $value) {
				$challege_ids[] = $value['challenge_id'];
			}
		}

		$param = array();

		$param['table'] = $this->_table;
		$where_in = array('field' => 'challenge_id',	
						  'arr'	 => $challege_ids);
		$param['where'] = array('mfg_status'		=> 1,
								'registrant_id'		=> $registrant_id,
								'where_in'			=> $where_in);
		
		$completed = $this->global_model->get_rows($param)->num_rows();
		$is_complete = ($completed == count($challege_ids));
		$is_disapproved = ($completed == count($challege_ids) - 1);

		# get move fwd details
		$param = array();

		$param['table'] = 'tbl_move_forward';
		$param['where'] = array('move_forward_id'	=> $challenge['move_forward_id']);
		$move_fwd = (array) $this->global_model->get_row($param);
			if($is_complete) {

			$where = array('move_forward_id'	=>  $challenge['move_forward_id'], 'registrant_id'	=> $registrant_id);
			$set = array('move_forward_choice_done'	=> date('Y-m-d H:i:s'), 'move_forward_choice_status'	=> 1);
			$this->global_model->update_record('tbl_move_forward_choice', $where, $set);
			
			if($status == 1) {
				$this->db->where('move_forward_id', $challenge['move_forward_id']);
				$this->db->update('tbl_move_forward', array('slots'	=> $move_fwd['slots'] - 1));	
			}

			if($old_status == 2) {
				$this->points_model->activate($registrant_id, array(
					array(
						'origin' => MOVE_FWD_GALLERY,
						'suborigin' => $id
					),
					array('origin' => MOVE_FWD,
						'suborigin' => $challenge['move_forward_id']
					)
				));		
			
			} elseif($old_status == 1) {
				$this->points_model->deactivate($registrant_id, array(
					array(
						'origin' => MOVE_FWD_GALLERY,
						'suborigin' => $id
					),
					array('origin' => MOVE_FWD,
						'suborigin' => $challenge['move_forward_id']
					)
				));		
			} else {
				$points = $this->points_model->earn(MOVE_FWD, array('registrant'	 	=> $registrant_id, 'suborigin'	=> $challenge['move_forward_id'], 
																	'remarks'			=> '{{ name }} accomplished ' . $challenge['move_forward_title'] . ' for MoveFWD'));
			}	
				
		} elseif($is_disapproved) {
			$where = array('move_forward_id'	=>  $challenge['move_forward_id'], 'registrant_id'	=> $registrant_id);
			$set = array('move_forward_choice_done'	=> null, 'move_forward_choice_status'	=> 0);
			$this->global_model->update_record('tbl_move_forward_choice', $where, $set);

			if($old_status == 1) {
				if($status == 2) {
					$this->db->where('move_forward_id', $challenge['move_forward_id']);
					$this->db->update('tbl_move_forward', array('slots'	=> $move_fwd['slots'] + 1));	
				}
				$this->points_model->deactivate($registrant_id, array(
					array(
						'origin' => MOVE_FWD_GALLERY,
						'suborigin' => $id
					),
					array('origin' => MOVE_FWD,
						'suborigin' => $challenge['move_forward_id']
					),
					array('origin' => MOVE_FWD_COMPLETE)
				));		
			} 
			
		} else {
			$where = array('move_forward_id'	=>  $challenge['move_forward_id'], 'registrant_id'	=> $registrant_id);
			$set = array('move_forward_choice_done'	=> null, 'move_forward_choice_status'	=> 0);
			$this->global_model->update_record('tbl_move_forward_choice', $where, $set);

			if($old_status == 2) {
				$this->points_model->activate($registrant_id, array(
					array(
						'origin' => MOVE_FWD_GALLERY,
						'suborigin' => $id
					)
				));			
			} elseif($old_status == 1) {
				$this->points_model->deactivate($registrant_id, array(
					array(
						'origin' => MOVE_FWD_GALLERY,
						'suborigin' => $id
					)
				));		
			} 	
		}
		return $is_complete;

	}

	private function is_complete_all_activity($id, $registrant_id, $old_status) {
		$is_complete =  $this->move_fwd_model->is_complete_all_activity($registrant_id, $challenges, $gallery);
		$is_disapproved = $gallery == $challenges - 1;

		if($is_complete) {
			if($old_status == 0)
				$points = $this->points_model->earn(MOVE_FWD_COMPLETE, array('registrant'	=> $registrant_id, 'remarks'	=> '{{ name }} accomplished ' . $challenge['move_forward_title'] . ' for MoveFWD'));	
			elseif($old_status == 2) {
				$points = $this->points_model->activate($registrant_id, array(
					array(
						'origin' => MOVE_FWD_GALLERY,
						'suborigin' => $id
					),
					array('origin' => MOVE_FWD_COMPLETE)
				));			
			} elseif($old_status == 1) {
				$points = $this->points_model->deactivate($registrant_id, array(
					array(
						'origin' => MOVE_FWD_GALLERY,
						'suborigin' => $id
					),
					array('origin' => MOVE_FWD_COMPLETE)
				));			
			}
		} elseif($is_disapproved) {
			if($old_status == 1) {
				$points = $this->points_model->deactivate($registrant_id, array(
					array(
						'origin' => MOVE_FWD_GALLERY,
						'suborigin' => $id
					),
					array('origin' => MOVE_FWD_COMPLETE)
				));			
			}
		} else {
			if($old_status == 2) {
				$points = $this->points_model->activate($registrant_id, array(
					array(
						'origin' => MOVE_FWD_GALLERY,
						'suborigin' => $id
					)
				));			
			} elseif($old_status == 1) {
				$points = $this->points_model->deactivate($registrant_id, array(
					array(
						'origin' => MOVE_FWD_GALLERY,
						'suborigin' => $id
					)
				));			
			}	
		}
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		elseif($method == 'approve')
			$this->approve();
		elseif($method == 'disapprove')
			$this->disapprove();
		elseif($method == 'set_winner')
			$this->set_winner();
		elseif($method == 'unset_winner')
			$this->unset_winner();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */