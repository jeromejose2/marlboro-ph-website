<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	public function login()
	{
		$this->load->library('Spice');
		$this->spice->createSession('MIPPHSysUser', 'PmiPH@2015');
		exit;
		$is_blocked = false;
		if($this->input->post('username')) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			if($username && $password)
			{
				$valid = $this->login_model->login($username, $password, $is_blocked);
				
 				if( $valid ){
 					$user_details = $this->login_model->extract_user_details($_SESSION['logged']);
 					$now = time(); // or your date as well
					$last_login = strtotime($user_details['last_login']);
					$datediff = date_differ($now, $last_login);

					if($datediff >= ACCOUNT_EXPIRATION) {
						$return = "Sorry, your account has been disabled. Please contact Philip Morris for details on how to restore it.";
					} elseif($is_blocked) {
						$last_login = strtotime($user_details['date_blocked']);
						$datediff = (BLOCK_AUTO_REMOVAL_MINUTES/60) -  date_differ($now, $last_login, 'h');
						$return = "Sorry, your account has been blocked. You may retry logging in after $datediff hours";	
					} else {
						$post['url'] = SITE_URL . '/auth/login';
						$post['description'] = 'logged in';
						$this->module_model->save_audit_trail($post);
						redirect('welcome');
					}
 				} else  {
 					$post['url'] = SITE_URL . '/auth/login';
					$post['description'] = $username . ' failed logging in';
					$this->module_model->save_audit_trail($post);

 					$user_details = $this->login_model->get_user_by_email($username);
					if($user_details && $user_details['is_blocked']) {

						$post['url'] = SITE_URL . '/auth/login';
						$post['description'] = $username . ' \'s account has been blocked';
						$this->module_model->save_audit_trail($post);

						$now = time(); // or your date as well
						$last_login = strtotime($user_details['date_blocked']);
						$datediff = (BLOCK_AUTO_REMOVAL_MINUTES/60) - date_differ($now, $last_login, 'h');
						$return = "Sorry, your account has been blocked. You may retry logging in after $datediff hours";
					} else {
						$return = "Sorry, the username or password you entered is invalid";	
					}
 				}

			} else {
				$return =  !$is_blocked ? "Sorry, the username or password you entered is invalid" : "Sorry, your account has been blocked. You may retry logging in after 72 hours";
			}
		} else {
			$return = "";
 		}
		$data['error_msg'] = $return;

		$this->load->view('login-template', $data, FALSE);
 	}
	
	public function logout() {
		$post['url'] = SITE_URL . '/auth/login';
		$post['description'] = 'logged out';
		$this->module_model->save_audit_trail($post);
		$this->login_model->logout();
		redirect('auth/login');
	}

	public function encode($password) {
		echo $this->login_model->encrypt_password($password);
	}
	
	public function decode($password) {
		echo $this->login_model->decrypt_password($password);
	}

}

