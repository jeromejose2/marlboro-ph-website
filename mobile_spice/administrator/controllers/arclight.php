<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Arclight extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		set_time_limit(0);
		ini_set('memory_limit', '3000M');
		error_reporting(E_ALL);
		ini_set('display_errors', "1");
	}

	public function inbound()
	{
		$data = array();
		$page = $this->uri->segment(3, 1);
		$data['offset'] = ($page - 1) * PER_PAGE;
		$data['search'] = array();
		$data['search']['filename'] = (string) $this->input->get('filename');
		if ($data['search']['filename'] && pathinfo($data['search']['filename'], PATHINFO_EXTENSION) == 'xml')
			$data['search']['filename'] = pathinfo($data['search']['filename'], PATHINFO_FILENAME);

		$data['search']['from_date'] = (string) $this->input->get('from_date');
		$data['search']['to_date'] = (string) $this->input->get('to_date');
		$this->db->start_cache();
		$this->db->select()
			->from('tbl_arclight_inbound');
		if ($data['search']['filename']) {
			$this->db->like('inbound_filename', $data['search']['filename']);
		}
		if ($data['search']['from_date']) {
			$this->db->where('date_of_processing >=', $data['search']['from_date']);
		}
		if ($data['search']['to_date']) {
			$this->db->where('date_of_processing <=', $data['search']['to_date']);
		}
		$data['total'] = $this->db->count_all_results();
		$this->db->order_by('date_of_processing', 'DESC');
		$this->db->limit(PER_PAGE, $data['offset']);
		$data['records'] = $this->db->get()->result();
		$this->db->stop_cache();
		$this->db->flush_cache();

		$data['pagination'] = $this->global_model->pagination($data['total'], $page , SITE_URL.'/arclight/inbound');

		$data['nav'] = $this->nav_items();
		$data['main_content'] = $this->load->view('arclight/inbound', $data, true);
		$this->load->view('main-template', $data);
	}

	public function outbound()
	{
		$data = array();
		$page = $this->uri->segment(3, 1);
		$data['offset'] = ($page - 1) * PER_PAGE;
		$data['search'] = array();
		$data['search']['filename'] = (string) $this->input->get('filename');
		if ($data['search']['filename'] && !pathinfo($data['search']['filename'], PATHINFO_EXTENSION))
			$data['search']['filename'] = $data['search']['filename'].'.xml';

		$data['search']['from_date'] = (string) $this->input->get('from_date');
		$data['search']['to_date'] = (string) $this->input->get('to_date');
		$this->db->start_cache();
		$this->db->select()
			->from('tbl_arclight_outbound');
		if ($data['search']['filename']) {
			$this->db->like('outbound_filename', $data['search']['filename']);
		}
		if ($data['search']['from_date']) {
			$this->db->where('date_fetched >=', $data['search']['from_date']);
		}
		if ($data['search']['to_date']) {
			$this->db->where('date_fetched <=', $data['search']['to_date']);
		}
		$data['total'] = $this->db->count_all_results();
		$this->db->order_by('date_fetched', 'DESC');
		$this->db->limit(PER_PAGE, $data['offset']);
		$data['records'] = $this->db->get()->result();
		$this->db->stop_cache();
		$this->db->flush_cache();

		$data['pagination'] = $this->global_model->pagination($data['total'], $page , SITE_URL.'/arclight/outbound');

		$data['nav'] = $this->nav_items();
		$data['main_content'] = $this->load->view('arclight/outbound', $data, true);
		$this->load->view('main-template', $data);
	}

	public function outbound_fetch()
	{
		$filename = $this->input->post('outbound_filename');
		if ($filename) {
			$withEmail = $this->input->post('outbound_with_email') ? true : false;
			$this->load->model('Arclight_Model');
			$this->Arclight_Model->pull($filename, $withEmail);
		}
		redirect('arclight/outbound');
	}

	public function outbound_match()
	{
		$data = array(
			'outbound_records_count' => 0,
			'registrants_inserted' => array(),
			'registrants_not_inserted' => array(),
			'not_found' => null,
			'duplicates' => array()
		);
		$data['nav'] = $this->nav_items();
		$filename = $this->input->get('outbound_filename');
		if ($filename) {
			$dbConn = null;
			if (!PROD_MODE && $this->input->get('prod')) {
				require 'administrator/config/database.php';
				$dbConn = $this->load->database($db['prod'], true);
			} else {
				$dbConn = $this->db;
			}

			$this->load->model('Arclight_Model');
			$registrants = $dbConn->select()
				->from('tbl_registrants')
				->get()
				->result_array();
			$records = array();
			if (!file_exists('./uploads/outbound/'.$filename) || $this->input->get('reload')) {
				$records = $this->Arclight_Model->get_outbound_records($filename);
				if (!is_dir('./uploads/outbound')) {
					mkdir('./uploads/outbound');
				}
				if ($records) {
					$records->asXml('./uploads/outbound/'.$filename);
				} else {
					$data['not_found'] = false;
				}
			} else {
				$records = new SimpleXMLElement(file_get_contents('./uploads/outbound/'.$filename));
			}
			
			$registrantsFilterField = 'email_address';
			$outboundFilterField = 'EmailAddress';
			if ($records) {
				$individuals = array();
				$individualIds = array();
				$outboundRecords = array();
				$records = $records->children();
				$data['outbound_records_count'] = count($records);
				foreach ($records as $rec) {
					$id = (string) $rec->IndividualID;
					$email = strtolower((string) $rec->EmailAddress);
					$rec = (array) $rec;
					foreach ($rec as &$r) {
						if (is_object($r)) {
							$r = (array) $r;
							if (!$r) {
								$r = '';
							}
						}
					}
					if ($email) {
						$individuals[$email] = 0;
						$outboundRecords[$email] = $rec;
					}
					if ($id) {
						$individualIds[$id] = $rec;
					}
				}

				if ($individuals) {

					foreach ($registrants as $registrant) {
						$registrantFilterId = strtolower($registrant[$registrantsFilterField]);
						if (isset($individuals[$registrantFilterId])) {
							$data['registrants_inserted'][] = $outboundRecords[$registrantFilterId];
							$individuals[$registrantFilterId]++;
						}
					}

					foreach ($individuals as $id => $val) {
						if (!$val) {
							$data['registrants_not_inserted'][] = $outboundRecords[$id];
						}
					}

					foreach ($individualIds as $id => $val) {
						if (!isset($data['duplicates'][$val[$outboundFilterField]])) {
							$data['duplicates'][$val[$outboundFilterField]] = 0;
						} else {
							$data['duplicates'][$val[$outboundFilterField]]++;
						}
					}
					
					foreach ($data['duplicates'] as $email => $duplicate) {
						if (!$duplicate) {
							unset($data['duplicates'][$email]);
						}
					}

				}
			}
		}

		$data['main_content'] = $this->load->view('arclight/outbound_match', $data, true);
		$this->load->view('main-template', $data);
	}
	
	private function nav_items()
	{
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
}

/* End of file arclight.php */
/* Location: ./application/controllers/arclight.php */