<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Passion extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
 	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

		$this->output->enable_profiler(TRUE);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
		$this->load->helper('paging');
		$offset = (int)$this->input->get('per_page');
	    $limit = 10;
	    $filters = $this->get_filters();

		$access = $this->module_model->check_access('registrant');
		$total_rows = $this->explore_model->get_total_rows(array('table'=>'tbl_explore_passions as b',
																 'where'=>array_merge($filters['where_filters'],array('b.is_deleted'=>0)),
																 'like'=>$filters['like_filters']																 
																 )
														    );
 		$data['header_text'] = 'Passion';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total_rows'] = $total_rows;
		
		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_explore_passions as a',
															 'where'=>array_merge($filters['where_filters'],array('a.is_deleted'=>0)),
														     'like'=>$filters['like_filters'],
														     'join'=>array('table'=>'tbl_categories as b',
													     					'on'=>'a.category_id=b.category_id',
													     				   ),
														     'fields'=>'a.title,a.description,a.status,a.passion_id,a.image,b.category_name'
														     )
														);
		$parent_category = $this->explore_model->get_row(array('table'=>'tbl_categories','fields'=>'category_id','where'=>array('LCASE(category_name)'=>'passion')));
		$parent_id = ($parent_category) ? $parent_category->category_id : null;

		$data['categories'] = $this->explore_model->get_rows(array('table'=>'tbl_categories','where'=>array('parent'=>$parent_id)));
		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']);
 		return $this->load->view('explore/'.$this->uri->segment(1).'/list',$data,TRUE);

	}


	public function add()
	{

		$error = '';
		$row = '';

		if($this->input->post()) {

			$post_data = $this->input->post();		

			if($this->validate_form()) {
 
				
 				$data = $post_data; 
				$data['date_added'] = TRUE; // Set NOW() value to date_added column field
				$data['image_crop_coordinates'] = serialize(array('x1'=>$this->input->post('x'),
 															 'y1'=>$this->input->post('y'),
 															 'x2'=>$this->input->post('x2'),
 															 'y2'=>$this->input->post('y2'),
 															 'width'=>$this->input->post('width'),
 															 'height'=>$this->input->post('height')
 															 ));
 
 
				$id = $this->explore_model->insert('tbl_explore_passions',$data);
				$image_filename = $this->do_upload($id,'image');

				if($image_filename) 
	   		    	$this->explore_model->update('tbl_explore_passions',array('image'=>$image_filename),array('passion_id'=>$id));


	   		    $post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/add';
				$post['description'] = 'added a new explore - '.$this->uri->segment(1);
				$post['table'] = 'tbl_explore_passions';
				$post['record_id'] = $id;
				$post['type'] = 'add';
				$this->module_model->save_audit_trail($post);

	   		   	redirect($this->uri->segment(1));

			} else {

				$post_data['image'] = '';
				$error = validation_errors();
				$row = json_decode(json_encode($post_data),false);

			}


			
 
		}


		$cat = $this->explore_model->get_row(array('table'=>'tbl_categories','where'=>array('LCASE(category_name)'=>'passion'),'fields'=>'category_id'));
		$parent_id = ($cat) ? $cat->category_id : null;
		

		$data['error'] = $error;
		$data['row'] = $row;
 		$data['header_text'] = 'Passion';

 		$parent_category = $this->explore_model->get_row(array('table'=>'tbl_categories','fields'=>'category_id','where'=>array('LCASE(category_name)'=>'passion')));
		$parent_id = ($parent_category) ? $cat->category_id : null;

		$data['categories'] = $this->explore_model->get_rows(array('table'=>'tbl_categories','where'=>array('parent'=>$parent_id)));
		$data['main_content'] = $this->load->view('explore/'.$this->uri->segment(1).'/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
		$this->output->enable_profiler(TRUE);

	}


	public function edit()
	{

		$error = '';
		$id = $this->uri->segment(3);
		$row = $this->explore_model->get_row(array('table'=>'tbl_explore_passions','where'=>array('passion_id'=>$id,'is_deleted'=>0)));

		if($this->input->post())
		{
			$post_data = $this->input->post();

			if($this->validate_form()){

				$passion = json_decode(json_encode($row),TRUE);
				  				


				$fields = array('title', 'description','category_id','status');
				$status = array('', 'Published', 'Unpublished');

				$new_content = array();
				$old_content = array();

				foreach($passion as $k => $v) {
					if(in_array($k, $fields)) {
						if($passion[$k] != $this->input->post($k)) {

							if($k == 'status') {
								
								$new_content[$k] = $status[$this->input->post($k)];
								$old_content[$k] = $status[$passion[$k]];

							}else {
								$new_content[$k] = $this->input->post($k);
								$old_content[$k] = $passion[$k];
							}

						}
					}
				}

				$post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/edit/' . $id;
				$post['description'] = 'updated an explore - '.$this->uri->segment(1);
				$post['table'] = 'tbl_explore_passions';
				$post['record_id'] = $id;
				$post['type'] = 'edit';
				$post['field_changes'] = serialize(array('old'	=> $old_content,
														 'new'	=> $new_content));
				$this->module_model->save_audit_trail($post);

				$post_data['date_updated']	= TRUE; // Set NOW() value to column date_updated 
 				$post_data['image_crop_coordinates'] = serialize(array('x1'=>$this->input->post('x'),
 															 'y1'=>$this->input->post('y'),
 															 'x2'=>$this->input->post('x2'),
 															 'y2'=>$this->input->post('y2'),
 															 'width'=>$this->input->post('width'),
 															 'height'=>$this->input->post('height')
 															 ));
				$this->explore_model->update('tbl_explore_passions',$post_data,array('passion_id'=>$id,'is_deleted'=>0));
				$this->do_upload($id,'image');

				redirect($this->uri->segment(1));

			}else{

				$error = validation_errors();
				$post_data['image'] = $post_data['current_image'];
				$row = json_decode(json_encode($post_data),false);

			}


		}

 		
		$cat = $this->explore_model->get_row(array('table'=>'tbl_categories','fields'=>'category_id','where'=>array('LCASE(category_name)'=>'passion')));
		$parent_id = ($cat) ? $cat->category_id : null;

		$data['row'] = $row;
		$data['error'] = $error;
		$data['header_text'] = 'Passion';
		$data['categories'] = $this->explore_model->get_rows(array('table'=>'tbl_categories','where'=>array('parent'=>$parent_id)));
		$data['main_content'] = $this->load->view('explore/passion/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

		$this->output->enable_profiler(TRUE);

	}


	public function delete()
	{

 		$id = $this->uri->segment(3);
 		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
 			$this->explore_model->update('tbl_explore_passions',array('is_deleted'=>1),array('passion_id'=>$id));
		}
		redirect($this->uri->segment(1));

	}

	public function validate_form()
	{

		$this->load->library('form_validation');

		$rules = array(
		   array(
				 'field'   => 'title',
				 'label'   => 'Title',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'description',
				 'label'   => 'Description',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'category_id',
				 'label'   => 'Category',
				 'rules'   => 'required'
			  )
		);

		$this->form_validation->set_rules($rules);
		
		return $this->form_validation->run();
	}



	public function do_upload($new_filename,$upload_file)
	{
		$this->load->helper(array('upload','resize','crop'));

		$params = array('upload_path'=>'uploads/explore/'.$this->uri->segment(1).'/','allowed_types'=>'png|jpg|gif','file_name'=>$new_filename,'max_size'=>'2097152','max_width'=>0,'max_height'=>0,'do_upload'=>$upload_file);
	    $file = upload($params);

	    $params = array('x'=>$this->input->post('x'),
	    				'y'=>$this->input->post('y'),
	    				'x2'=>$this->input->post('x2'),
	    				'y2'=>$this->input->post('y2'),
						'width'=>$this->input->post('width'),
						'height'=>$this->input->post('height')
					);


	    if(is_array($file)){

	    	$params = array_merge($params,array('file_path'=>$file['file_path'],'full_path'=>$file['full_path'],'file_name'=>$file['file_name']));	    	

	    }else{

	    	$this->load->helper('file');
	    	$file = get_file_info('uploads/explore/'.$this->uri->segment(1).'/'.$this->input->post('current_image'));
	    	$file['file_name'] = $this->input->post('current_image');
	    	$params = array_merge($params,array('file_path'=>'uploads/explore/'.$this->uri->segment(1),'full_path'=>$file['server_path'],'file_name'=>$this->input->post('current_image')));
	    }
 
	    crop($params);	    

	    $params = array('width'=>50,'height'=>50,'source_image'=>'uploads/explore/'.$this->uri->segment(1).'/thumb_'.$file['file_name'],'new_image_path'=>'uploads/explore/'.$this->uri->segment(1).'/','file_name'=>$file['file_name']);
	    resize($params);

	    return (is_array($file) && count($file) > 0) ? $file['file_name'] : '';

	}

	public function get_filters()
	   {
		   
		   $where_filters = array('a.status'=>'status','b.category_id'=>'category_id');
		   $like_filters = array('a.title'=>'title','a.description'=>'description');
		   $query_strings = $this->input->get();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) === '0')
					$valid_where_filters[$column_field] = trim($this->input->get($filter));				 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter))
					$valid_like_filters[$column_field] = trim($this->input->get($filter));
				 
				   
		   }

		   if(isset($query_strings['per_page']))
		   		unset($query_strings['per_page']);

		   	if(isset($query_strings['default']))
		   		unset($query_strings['default']);


 		   $query_strings = ($this->input->get()) ? http_build_query(array_filter($this->input->get())) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?default=0'.$query_strings);		   
		   
	   } 


	   public function export()
	   {

		   	$filters = $this->get_filters();
		   	$params = 
		   	$data['query'] = $this->explore_model->get_rows(array('fields'=>"a.title,a.description,b.category_name,date_added",
		   														'table'=>'tbl_explore_passions as a',
											   					'where'=>$filters['where_filters'],
											   					'like'=>$filters['like_filters'],
											   					'join'=>array('table'=>'tbl_categories as b',
											   								'on'=>'a.category_id = b.category_id')
											   					)
		   													);
		   	$data['filename'] = $this->uri->segment(1).'_'.date('M-d-Y');
		    $this->load->view('explore/export-to-excell',$data);
	   }

 
}