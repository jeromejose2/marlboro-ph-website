<?php 

class User_activities extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$this->load->model('report_model');
		$this->report_model->get_user_activities();
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	} 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		$where =  array();
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			$like['CONCAT(first_name, \' \', third_name)'] = $like['name'];
			if(!empty($like['bid_item_id']))
				$where['tbl_bids.bid_item_id'] = $like['bid_item_id'];

			if($like['from_bid'] && $like['to_bid']) {
				$where['DATE(bid_date) <='] = $like['to_bid'];
				$where['DATE(bid_date) >='] = $like['from_bid'];
			}
			$this->unset_val($like, array('search', 'name', 'bid_item_id', 'from_bid', 'to_bid'));
		}

		$param['offset'] = $data['offset'];
		$param['limit'] = PER_PAGE;
		$param['table'] = $this->_table;
		$param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id = ' . $this->_table . '.registrant_id',
							   'tbl_bid_items' =>	'tbl_bid_items.bid_item_id = tbl_bids.bid_item_id'); 
		$param['order_by'] = array('field'=>'bid_date','order'=>'DESC');
		$param['where'] = $where;
		$param['like'] = $like;
		$data['users'] = $this->global_model->get_rows($param)->result_array();
		//die($this->db->last_query());
		$records = $this->global_model->get_total_rows($param);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/user_bids');
		$access = $this->module_model->check_access('user_bids');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = $records;
		$data['items'] = $this->get_bid_items();
		return $this->load->view('user/activities', $data, true);		
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */