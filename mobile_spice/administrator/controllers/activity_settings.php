<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Activity_settings extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
		//$this->output->enable_profiler(TRUE);
 	} 

 	 
 	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

 	}
 	 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
		$this->load->helper('paging');
		$offset = (int)$this->input->get('per_page');
	    $limit = PER_PAGE;
	    $filters = $this->get_filters(); 

		$access = $this->module_model->check_access('activity_settings');
		$total_rows = $this->explore_model->get_rows(array('table'=>'tbl_activity_settings',
																 'where'=>$filters['where_filters'],
																 'like'=>$filters['like_filters']																 
																 )
														    )->num_rows();
 		$data['header_text'] = 'Activity Settings';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total_rows'] = $total_rows;
		$data['query_strings'] = $filters['query_strings'];
 		
		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_activity_settings',
															 'where'=>$filters['where_filters'],
														     'like'=>$filters['like_filters'],
														     'order_by' => array('field'=>'date_added','order'=>'DESC'),
														     'offset'=>$offset,
														     'limit'=>$limit
 														     )
														);
  
 		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']);
 		return $this->load->view($this->uri->segment(1).'/list',$data,TRUE);


	}
 

	public function add()
	{

		$error = '';
		$row = '';

		if($this->input->post()) {

			$post_data = $this->input->post();	
 

			if($this->validate_form()) {
 
				
 				$data = $post_data; 
				$data['date_added'] = TRUE; // Set NOW() value to date_added column field
  				$id = $this->explore_model->insert('tbl_activity_settings',$data); 

	   		    $post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/add';
				$post['description'] = 'added a new activity setting.';
				$post['table'] = 'tbl_activity_settings';
				$post['record_id'] = $id;
				$post['type'] = 'add';
				$this->module_model->save_audit_trail($post);

	   		    redirect($this->uri->segment(1)); 

			} else {

				$post_data['image'] = '';
				$error = validation_errors();
				$row = json_decode(json_encode($post_data),false);

			}
 
		} 
		

		$data['error'] = $error;
		$data['row'] = $row;
		$data['row_photos'] =  '';
 		$data['header_text'] = 'Activity Setting'; 

 		$data['main_content'] = $this->load->view($this->uri->segment(1).'/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
 
	} 



	public function edit()
	{

		$error = '';
		$id = $this->uri->segment(3);
		$row = $this->explore_model->get_row(array('table'=>'tbl_activity_settings','where'=>array('setting_id'=>$id)));

		if($this->input->post())
		{
			$post_data = $this->input->post();

			if($this->validate_form()){

				$photo = json_decode(json_encode($row),TRUE);


				/* Insert action history */

				$fields = array('name','setting_activity_id','status');
				$status = array('', 'Published', 'Unpublished');

				$new_content = array();
				$old_content = array();

				foreach($photo as $k => $v) {
					if(in_array($k, $fields)) {
						if($photo[$k] != $this->input->post($k)) {

							if($k == 'status') {
								
								$new_content[$k] = $status[$this->input->post($k)];
								$old_content[$k] = $status[$photo[$k]];

							}else {
								$new_content[$k] = $this->input->post($k);
								$old_content[$k] = $photo[$k];
							}

						}
					}
				}

				$post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/edit/' . $id;
				$post['description'] = 'updated an activity setting.';
				$post['table'] = 'tbl_activity_settings';
				$post['record_id'] = $id;
				$post['type'] = 'edit';
				$post['field_changes'] = serialize(array('old'	=> $old_content,
														 'new'	=> $new_content));
				$this->module_model->save_audit_trail($post);

				/* End Insert action history */
				$post_data['date_updated']	= TRUE; // Set NOW() value to column date_updated
 				$this->explore_model->update('tbl_activity_settings',$post_data,array('setting_id'=>$id));
				redirect($this->uri->segment(1));

			}else{

				$error = validation_errors();
 				$row = json_decode(json_encode($post_data),false);

			}


		}
 		
  
		$data['row'] = $row; 
		$data['error'] = $error;
		$data['header_text'] = 'Activity Setting';
		$data['main_content'] = $this->load->view($this->uri->segment(1).'/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

		//$this->output->enable_profiler(TRUE);

	}


	public function delete()
	{

 		$id = $this->uri->segment(3);
 		$token = $this->uri->segment(4);

		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
 			
 			$this->explore_model->delete('tbl_activity_settings',array('setting_id'=>$id));

 			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'deleted an activity setting.';
			$post['table'] = 'tbl_activity_settings';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);

		}
		redirect($this->uri->segment(1));

	}

	 

	public function validate_form()
	{

		$this->load->library('form_validation');

		$rules = array(
		   array(
				 'field'   => 'name',
				 'label'   => 'Activity Name',
				 'rules'   => 'required|callback_checkName'
			  ),
		   array(
				 'field'   => 'setting_activity_id',
				 'label'   => 'Setting Activity ID',
				 'rules'   => 'required|callback_checkID'
			  )
		);

		$this->form_validation->set_rules($rules);
		
		return $this->form_validation->run();
	}


	public function checkName($name){

		$row = $this->explore_model->get_row(array('table'=>'tbl_activity_settings','where'=>array('setting_id !='=>$this->uri->segment(3),'LOWER(name)'=>strtolower($name))));
		if($row){
			$this->form_validation->set_message('checkName','Error, activity name already exists.');
			return false;
		}
		return true;

	}

	public function checkID($id){

		$row = $this->explore_model->get_row(array('table'=>'tbl_activity_settings','where'=>array('setting_id !='=>$this->uri->segment(3),'setting_activity_id'=>$id)));
		if($row){
			$this->form_validation->set_message('checkID','Error, activity ID already exists.');
			return false;
		}
		return true;

	}

	 



	

	public function get_filters()
	   {
		   
		   $where_filters = array('DATE(date_added) >='=>'from_date_added','DATE(date_added) <='=>'to_date_added','setting_activity_id'=>'id');
		   $like_filters = array('name'=>'name');
		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
		   
	   } 


	   public function export()
	   {
	   		$this->load->library('to_excel_array');

		   	$filters = $this->get_filters();
		   	$params = 
		   	$query = $this->explore_model->get_rows(array('fields'=>"name,setting_activity_id,date_added,date_modified",
		   														'table'=>'tbl_activity_settings',
											   					'where'=>$filters['where_filters'],
											   					'like'=>$filters['like_filters'],
 											   					)
		   													);
		   	$res[] = array('Name','Setting Activity ID', 'Date Added', 'Date Modified');

		   	if($query->num_rows()){
		   		foreach($query->result() as $v){
 		   			$res[] = array($v->name,$v->setting_activity_id, $v->date_added,$v->date_modified);
		   		}
		   	}
		   	$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));

	   }


	   

 
}