<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_quiz extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('quiz_model');
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	} 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$data['offset'] = ($page - 1) * PER_PAGE;
		$data['users'] = $this->process_quiz_array($this->quiz_model->get_user_quiz($page));
		//die($this->db->last_query());
		$records = $this->quiz_model->get_user_quiz_count();

		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/user_quiz');
		$access = $this->module_model->check_access('user_buys');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total'] = $records;
		return $this->load->view('quiz/user', $data, true);		
	} 

	private function process_quiz_array($quizzes) {
		$quiz_arr = array();
		if($quizzes) {
			$cur_quiz = $quizzes[0]['user_quiz_id'];
			$choices = array();
			foreach ($quizzes as $key => $value) {
				if($cur_quiz != $value['user_quiz_id']) {
					$quizzes[$key-1]['questions'] = $choices;
					$choices = array();
					$quiz_arr[] = $quizzes[$key-1];
				} 
				$choices[] = array('question' =>$value['question'], 'answer'=>$value['choice']);
				$cur_quiz = $value['user_quiz_id'];
			}

			$quizzes[$key-1]['questions'] = $choices;
			$quiz_arr[] = $quizzes[$key-1];
		}

		return $quiz_arr;
	}

	public function export()
	{
		$like = array();
		$where = array();
		$this->load->library('to_excel_array');
		$records =  $this->process_quiz_array($this->quiz_model->get_user_quiz(0, true));

		$row[] = array('Name', 
					   'Questions',
					   'Result',
					   'Date');
		if($records) {
			foreach($records as $k => $v) {
				$questions = '';
				foreach ($v['questions'] as $qk => $qv) {
					$questions .= "Question: " . $qv['question'] . "\nAnswer: " . $qv['answer'] . "\n\n"; 
				}
				$row[] = array($v['first_name'] . ' ' . $v['third_name'], $questions, $v['result'], $v['date_created']);
			}
		}
		$this->to_excel_array->to_excel($row, 'user_quiz_'.date("YmdHis"));
	}

	private function unset_val(&$arr, $exclude) {
		foreach ($arr as $key => $value) {
			if(in_array($key, $exclude)) {
				unset($arr[$key]);
			}
		}
	} 
	
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete();
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */