
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {
	
	private $_user;
	public function __construct() {
		parent::__construct();
		$this->load->model('registrant_model');
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		$where = ' AND ((new_picture = \'\' AND picture_status = 1) OR (new_picture <> \'\' AND picture_status <> 1))';
		if(isset($_GET['search'])) {
			$where .=  " AND CONCAT(first_name, ' ', third_name) LIKE '%$_GET[name]%'";
			$where .=  " AND email_address LIKE '%$_GET[email]%'";
			$where .= $_GET['from'] ? " AND DATE(date_created) >= '$_GET[from]'" : "";
			$where .= $_GET['to'] ? " AND DATE(date_created) <= '$_GET[to]'" : "";
			$where .= $_GET['status'] != '' ? " AND picture_status = '$_GET[status]'" : "";
		}
		
		$data['users'] = $this->registrant_model->get_registrant($where, $this->uri->segment(2, 1), PER_PAGE, $records);
		$page = $this->uri->segment(2, 1);
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/profile', PER_PAGE);
		$data['offset'] = ($page - 1) * PER_PAGE;
		$access = $this->module_model->check_access('registrant');
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['status'] = $this->global_model->get_status('mod');
		$data['provinces'] = $this->global_model->get_provinces();
		$data['total'] = $records;
		$data['giid'] = $this->registrant_model->get_giid_types();
		$data['brands'] = $this->registrant_model->get_brands();
		$data['alternate'] = $this->registrant_model->get_alernate_purchases();

		return $this->load->view('profile/index', $data, true);		
	}
	
	// public function edit() {
		
	// 	$table = 'tbl_registrants';
	// 	$id = $this->uri->segment(3);
	// 	$field = 'registrant_id';
	// 	$token = $this->uri->segment(4);
	// 	$new_content = array();
	// 	$old_content = array();
	// 	if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/profile') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
	// 		$where[$field] = $id;
	// 		$user = $this->registrant_model->get_registrant($where)[0];
	// 		if(!$user)
	// 			redirect('profile');
			
	// 		$set['street_name'] = $this->input->post('street_name');
	// 		$set['barangay'] = $this->input->post('barangay');
	// 		$set['city'] = $this->input->post('city');
	// 		$set['province'] = $this->input->post('province');
	// 		$set['zip_code'] = $this->input->post('zip_code');
	// 		$this->global_model->update_record($table, $where, $set);
			
			
	// 		$fields = array('street_name', 'barangay', 'city', 'province', 'zip_code');
	// 		foreach($user as $k => $v) {
	// 			if(in_array($k, $fields)) {
	// 				if($user[$k] != $this->input->post($k)) {
	// 					$new_content[$k] = $this->input->post($k);
	// 					$old_content[$k] = $user[$k];
	// 				}
	// 			}
	// 		}
			
	// 		$post['url'] = SITE_URL . '/statement/edit/' . $id;
	// 		$post['description'] = 'updated a registrant details';
	// 		$post['table'] = 'tbl_registrants';
	// 		$post['record_id'] = $id;
	// 		$post['type'] = 'edit';
	// 		$post['field_changes'] = serialize(array('old'	=> $old_content,
	// 										  		  'new'		=> $new_content));
	// 		$this->module_model->save_audit_trail($post);
			
	// 	}
	// 	redirect('profile');
	// }
	
	private function validate_fields() {
		$email = $this->input->post('username');
		$id = $this->input->post('id');
		$where['username'] = $email;
		$user = $this->user_model->get_user($where);
		if($user) {
			if($id && $user[0]['cms_user_id'] == $id)
				return true;
			return false;
		}
		
		return true;
	}
	
	
	public function approve() {
		
		$table = 'tbl_registrants';
		$id = $this->uri->segment(3);
		$field = 'registrant_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/profile') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->update_status($id, 1, 'approved a registrant GIID upload');
		}
		redirect('profile');
	}
	
	public function disapprove() {
		$table = 'tbl_registrants';
		$id = $this->uri->segment(3);
		$field = 'registrant_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/profile') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->update_status($id, 2, 'rejected a registrant GIID upload', $this->input->post('message'));
			
			#send notification email
			$params['name'] = 'Philip Morris, Inc.';
			$params['email'] = 'no-reply@marlboro.ph';
			$params['to'] = $this->_user['email_address'];
			$params['subject'] = 'This is a test';
			$content = file_get_contents('mail/reject_photo.html');
			$search = array('{reason}');
			$replace = array($this->input->post('message'));
			$content = str_replace($search, $replace, $content);
			$params['content'] = $content;
			
			$this->global_model->send_email($params);
		
		}
		redirect('profile');
	}
	
	private function update_status($id, $new_status, $action_text = '', $message = '') {

		include_once('application/models/notification_model.php');
		$this->notification_model = new Notification_Model();

		$where['registrant_id'] = $id;
		$table = 'tbl_registrants';
		$this->_user = $this->registrant_model->get_registrant($where)[0];
		if(!$this->_user)
			redirect('profile');

		#update status
		$set['picture_status'] = $new_status;
		$set['current_picture'] = $this->_user['new_picture'];
		if($new_status == 1) {
			$this->notification_model->notify($id, PROFILE_PHOTO, array(
															'message' => 'Your profile photo has been approved',
															'suborigin' => $id
															));
			$set['new_picture'] = '';
		}
			
		if($new_status == 2) {
			$this->notification_model->notify($id, PROFILE_PHOTO, array(
															'message' => 'Your profile photo has been disapproved',
															'suborigin' => $id
															));
			$set['current_picture'] = '';
			$set['new_picture'] = $this->_user['current_picture'];
		}
		$this->global_model->update_record($table, $where, $set);
		
		#save audit trail
		$status = $this->global_model->get_status();
		$new_content['status'] = $status[$new_status];
		$old_content['status'] = $status[$this->_user['status']];
		$post['url'] = @$_SERVER['HTTP_REFERER'];
		$post['description'] = $action_text;
		$post['table'] = $table;
		$post['record_id'] = $id;
		$post['message'] = $message;
		$post['field_changes'] = serialize(array('old'	=> $old_content,
												  'new'	=> $new_content));
		$this->module_model->save_audit_trail($post);	
	}

	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete();
		elseif($method == 'approve')
			$this->approve();
		elseif($method == 'disapprove')
			$this->disapprove();
		elseif($method == 'add')
			$this->add();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */