<?php

class Faq extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('settings_model');
	}
		
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}
	
	private function main_content() {
		if($this->input->post('submit')) {
			$this->settings_model->save_settings('faq');
		}
		$data['settings'] = $this->settings_model->get_settings('faq');
		$data['error'] = '';
		$data['title'] = 'Frequently Asked Questions';
		return $this->load->view('settings/index', $data, true);		
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */