<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('PROD_MODE',	$_SERVER['SERVER_NAME'] == 'marlboro.ph' || $_SERVER['SERVER_NAME'] == 'www.marlboro.ph');
define('DEV_MODE', !in_array($_SERVER['SERVER_NAME'], array('marlboro-stage2.yellowhub.com')) && !PROD_MODE);
define('FILE_UPLOAD_ALLOWED_TYPES', 'png|gif|jpg|jpeg');
define('NUWORKS_IP', '124.107.151.158');
define('HTML_ESCAPE_CONFIG', ENT_NOQUOTES);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('PER_PAGE',								isset($_GET['limit']) ? $_GET['limit'] : 10);

/*
|--------------------------------------------------------------------------
| Account status
|--------------------------------------------------------------------------
|
| These statuses are used for user accounts 
|
*/

define('VERIFIED',								1);
define('CSR_APPROVED',							2);
define('PENDING_GIID',							0);
define('PENDING_CSR',							4);
define('REJECTED_NO_GIID',						5);
define('REJECTED_GIID',							6);
define('DEACTIVATED',							7);
define('ARCLIGHT_FAILED',						8);
define('DELETED',								3);
define('MYM_APPROVED',							9);
define('ARCLIGHT_IN_PROCESS',					10);

define('PENDING',								0);
define('APPROVED',								1);
define('DISAPPROVED',							2);

define('ACCESS_GRANTED_STATUS',					VERIFIED);

/*
|--------------------------------------------------------------------------
| Module Groups
|--------------------------------------------------------------------------
|
| Group IDs for Modules
|
*/

define('USER_MANAGEMENT',						1);
define('CONTENT_MANAGEMENT',					2);
define('MODERATOR',								3);
define('REPORTS',								4);

/*
|--------------------------------------------------------------------------
| Admin account configuration
|--------------------------------------------------------------------------
|
| These constants are used for admin accounts
|
*/
define('PASSWORD_EXPIRATION',					90);
define('ACCOUNT_EXPIRATION',					45);
define('MAX_LOGIN_ATTEMPTS',					5);
define('BLOCK_AUTO_REMOVAL_MINUTES',			5);

/*
|--------------------------------------------------------------------------
| ORIGIN IDS
|--------------------------------------------------------------------------
|
*/
define('MOVE_FWD',								1);
define('EXPLORE',								2);
define('BIRTHDAY_OFFERS',						3);
define('FLASH_OFFERS',							4);
define('WEBGAMES',								5);
define('COMMENT',								6);
define('PERKS_BUY',								7);
define('PERKS_BID', 							8);
define('PERKS_RESERVE', 						9);
define('HIDDEN_MARLBORO',						10);
define('USER_STATEMENT',						11);
define('REGISTRATION',							12);
define('LOGIN',									13);
define('ABOUT_NEWS',							14);
define('ABOUT_VIDEOS',							15);
define('ABOUT_PHOTOS',							24);
define('COMMENT_REPLY',							17);
define('MOVE_FWD_GALLERY',						18);
define('MOVE_FWD_COMPLETE',						19);
define('MOVE_FWD_PLEDGE_ENTRY',					20);
define('COMMENT_RECEIVE',						21);
define('MOVE_FWD_WINNER',						22);
define('COMMENT_REPLY_RECEIVE',					23);
define('BACKSTAGE_PHOTOS',						ABOUT_PHOTOS);
define('PROFILE_PHOTO',							25);
define('MOVE_FWD_COMMENTS_PHOTOS',				26);
define('MOVE_FWD_COMMENT_REPLIES_PHOTOS',		27);
define('PREMIUM_HIDDEN_MARLBORO',				28);
define('BACKSTAGE_VIDEOS',						29);
define('REFERRAL',								30);
define('PROFILING',								31);
define('REFEREE_FIRSTLOGIN',					32);
define('REFERER_FIRSTLOGIN',					33);
/*
|--------------------------------------------------------------------------
| Site Configuration
|--------------------------------------------------------------------------
|
*/
define('TRACKING_CODE_EXPIRATION',				'+7 day');
define('FORGOT_TOKEN_EXPIRATION',				3600); // 1 hour
define('DEFAULT_IMAGE', 						'images/no_image.png');
define('GIID_FILE_TYPES',						FILE_UPLOAD_ALLOWED_TYPES.'|pdf');
define('VIEW_LIMIT_PER_DAY',					1);
define('SESSION_NAMESPACE',						'marlboro_front_sess');
define('LOGIN_ATTEMPTS',						7);
define('LOGIN_RETURN',							'+3 day');
define('FILE_UPLOAD_LIMIT',						'3072');
define('VIDEO_UPLOAD_LIMIT',					'307200');
define('MINIMUM_AGE',							18);
define('CAPTCHA_FONT',							'./fonts/captcha_font.ttf');
define('ERROR_MESSAGE',							'An Error Occured');
define('LAUNCH_DATE',							'2014-04-02');

/*
|--------------------------------------------------------------------------
| Pointing System Configuration
|--------------------------------------------------------------------------
|
*/
define('POINT_ACTIVE_STATUS',						1);
define('POINT_REJECTED_STATUS',						0);

define('GAME_FINISH_LEVEL',							1);
define('GAME_REACH_LEADERBOARD',					2);
define('GAME_TOP_SCORER',							3);

define('GAME_FINISH_LEVEL_BASE_POINT',				30);
define('GAME_FINISH_LEVEL_DAILY_LIMIT',				100);
define('GAME_FINISH_LEVEL_MONTHLY_LIMIT',			3000);

define('GAME_REACH_LEADERBOARD_BASE_POINT',			25);
define('GAME_REACH_LEADERBOARD_WEEKLY_LIMIT',		25);

define('GAME_TOP_SCORER_BASE_POINT',				50);
define('GAME_TOP_SCORER_WEEKLY_LIMIT',				50);

define('LOGIN_BASE_POINT',							5);
define('LOGIN_DAILY_LIMIT',							5);
define('LOGIN_MONTHLY_LIMIT',						150);

define('REGISTRATION_BASE_POINT',					100);
define('REGISTRATION_LIMIT',						100);

define('USER_STATEMENT_BASE_POINT',					10);
define('USER_STATEMENT_LIMIT',						10);

define('COMMENT_BASE_POINT',						10);
define('COMMENT_DAILY_LIMIT',						250);
define('COMMENT_MONTHLY_LIMIT',						30000);

define('COMMENT_REPLY_BASE_POINT',					5);
define('COMMENT_REPLY_DAILY_LIMIT',					125);
define('COMMENT_REPLY_MONTHLY_LIMIT',				15000);

define('COMMENT_RECEIVE_BASE_POINT',				5);
define('COMMENT_RECEIVE_DAILY_LIMIT',				125);
define('COMMENT_RECEIVE_MONTHLY_LIMIT',				15000);

define('COMMENT_REPLY_RECEIVE_BASE_POINT',			5);
define('COMMENT_REPLY_RECEIVE_DAILY_LIMIT',			125);
define('COMMENT_REPLY_RECEIVE_MONTHLY_LIMIT',		15000);

define('MOVE_FWD_WINNER_PHOTO_BASE_POINT',			25);
define('MOVE_FWD_WINNER_VIDEO_BASE_POINT',			50);

define('MOVE_FWD_FINISH_CHALLENGE_BASE_POINT',		25);
define('MOVE_FWD_FINISH_CHALLENGE_DAILY_LIMIT',		25);
define('MOVE_FWD_FINISH_CHALLENGE_MONTHLY_LIMIT',	75);

define('MOVE_FWD_COMPLETE_ACTIVITY_BASE_POINT',		50);
define('MOVE_FWD_COMPLETE_ACTIVITY_MONTHLY_LIMIT',	50);

define('MOVE_FWD_COMPLETE_ALL_BASE_POINT',			75);
define('MOVE_FWD_COMPLETE_ALL_MONTHLY_LIMIT',		75);

define('ABOUT_PHOTOS_APPROVE_BASE_POINT',			10);
define('ABOUT_PHOTOS_APPROVE_DAILY_LIMIT',			200);
define('ABOUT_PHOTOS_APPROVE_MONTHLY_LIMIT',		6000);

define('BACKSTAGE_PHOTOS_APPROVE_BASE_POINT',		10);
define('BACKSTAGE_PHOTOS_APPROVE_DAILY_LIMIT',		200);
define('BACKSTAGE_PHOTOS_APPROVE_MONTHLY_LIMIT',	6000);

define('ABOUT_VIDEOS_APPROVE_BASE_POINT',			25);
define('ABOUT_VIDEOS_APPROVE_DAILY_LIMIT',			500);
define('ABOUT_VIDEOS_APPROVE_MONTHLY_LIMIT',		15000);

define('HIDDEN_MARLBORO_BASE_POINT',				10);
define('HIDDEN_MARLBORO_DAILY_LIMIT',				10);
define('HIDDEN_MARLBORO_MONTHLY_LIMIT',				300);

define('PREMIUM_HIDDEN_MARLBORO_BASE_POINT',		30);
define('PREMIUM_HIDDEN_MARLBORO_DAILY_LIMIT',		30);
define('PREMIUM_HIDDEN_MARLBORO_MONTHLY_LIMIT',		900);

define('REFERER_BASE_POINT',						100);
define('REFERER_FIRST_LOGIN_POINT',					300);
define('REFEREE_FIRST_LOGIN_POINT',					150);

/*
|--------------------------------------------------------------------------
| Move Forward Configuration
|--------------------------------------------------------------------------
|
*/
define('MOVE_FWD_PLAY',								1);
define('MOVE_FWD_PLEDGE',							2);

define('MOVE_FWD_ENTRY_LIMIT',						3);
define('MOVE_FWD_ALLOWED_TYPES',					FILE_UPLOAD_ALLOWED_TYPES);

/*
|--------------------------------------------------------------------------
| Brand History Configuration
|--------------------------------------------------------------------------
|
*/
define('BRAND_HISTORY_FILE_UPLOAD_TYPES',			FILE_UPLOAD_ALLOWED_TYPES.'|mp4');

/*
|--------------------------------------------------------------------------
| Product Info Configuration
|--------------------------------------------------------------------------
|
*/
define('PRODUCT_INFO_FILE_UPLOAD_TYPES',			BRAND_HISTORY_FILE_UPLOAD_TYPES);

/*
|--------------------------------------------------------------------------
| DBAM Configuration
|--------------------------------------------------------------------------
|
*/
define('DBAM_FILE_UPLOAD_TYPES',					BRAND_HISTORY_FILE_UPLOAD_TYPES);

/*
|--------------------------------------------------------------------------
| Perks Reserve Configuration
|--------------------------------------------------------------------------
|
*/
define('PERKS_RESERVE_SCHEDULES_MONTH_LIMIT',		2);

/*
|--------------------------------------------------------------------------
| EDM API Configuration
|--------------------------------------------------------------------------
|
*/
define('EDM_ADMIN_USERNAME',					'mark.barcarse@nuworks.ph');
define('EDM_ADMIN_PASSWORD',					'Pass123!');
define('EDM_HOST',								'api2.silverpop.com');
define('EDM_SERVLET',							'XMLAPI');
define('EDM_PORT',								80);
define('EDM_TIMEOUT',							20);

define('EDM_MARLBORO_WELCOME_MAIL_ID',			5872118);
define('EDM_MARLBORO_RESET_MAIL_ID',			5872104);
define('EDM_MARLBORO_REGISTER_MAIL_ID',			5872068);
define('EDM_MARLBORO_REGISTER_LIST_ID',			3493478);
define('EDM_MARLBORO_BIRTHDAY_MAIL_ID',			7167788);
define('EDM_MARLBORO_BIRTHDAY_LIST_ID',			4129501);
define('EDM_MARLBORO_WELCOME_LIST_ID',			3493516);
define('EDM_MARLBORO_RESET_LIST_ID',			3493504);

define('EDM_MARLBORO_REFER_MAIL_ID',			7200572);
define('EDM_MARLBORO_REFER_LIST_ID',			4140465);


/*
|--------------------------------------------------------------------------
| Arclight Configuration
|--------------------------------------------------------------------------
|
*/
define('ARCLIGHT_CLIENT_CODE',					'PMA');
define('ARCLIGHT_VENDOR_CODE',					'A10');
define('ARCLIGHT_MARKET_CODE',					'PH');
define('ARCLIGHT_INBOUND_CAMPAIGN_CODE',		'PH14000403N01');
define('ARCLIGHT_OUTBOUND_CAMPAIGN_CODE',		'PH14000421W01');

define('ARCLIGHT_ENCODING',						'UTF-16');
define('ARCLIGHT_SERVER',						'transfer.pconnect.biz');
define('ARCLIGHT_USERNAME',						'mdjesus');
define('ARCLIGHT_PASSWORD',						'Welcome01');

define('ARCLIGHT_INBOUND_FILES_PATH',			'./uploads/inbound/');
define('ARCLIGHT_OUTBOUND_FILES_PATH',			'./uploads/outbound/');
define('ARCLIGHT_INBOUND_DROPOFF_PATH',			'/Distribution/Hippodrome/ArcLight Inbound - AP/drop_off/');
define('ARCLIGHT_INBOUND_FAILED_PATH',			'/Distribution/Hippodrome/ArcLight Inbound Failed/'.ARCLIGHT_VENDOR_CODE.'/pick_up/');
define('ARCLIGHT_OUTBOUND_PICKUP_PATH',			'/Distribution/Hippodrome/ArcLight Outbound/'.ARCLIGHT_VENDOR_CODE.'/pick_up/');

/*
|--------------------------------------------------------------------------
| Page Views
|--------------------------------------------------------------------------
|
*/

define('PAGES',									'games/wallbreaker,games/crossout,games/wordhunt,games/doubtcrasher,games,move_forward,perks,profile,about/videos,about/photos,about/news,about,');

/*
|--------------------------------------------------------------------------
| MoveFWD Status Codes
|--------------------------------------------------------------------------
|
*/
define('NORMAL',								0);
define('HAS_APPROVED',							1);
define('ALL_APPROVED',							2);
define('PLEDGED',								3);

/*
|--------------------------------------------------------------------------
| PLATFORM Codes
|--------------------------------------------------------------------------
|
*/
define('WEB_ONLY',								1);
define('MOBILE_ONLY',							2);
define('BOTH_PLATFORM',							3);

/*
|--------------------------------------------------------------------------
| MOBILE API TYPES
|--------------------------------------------------------------------------
|
*/
define('TYPE_EVENT',							0);
define('TYPE_BUYBID',							1);
define('TYPE_MOVEFWD',							2);
define('TYPE_GLIST',							3);
define('TYPE_PACK',								4);
define('TYPE_BAR',								5);
define('TYPE_BUY',								6);
define('TYPE_BID',								7);

/*
|--------------------------------------------------------------------------
| MOBILE API TYPE STRINGS
|--------------------------------------------------------------------------
|
*/
define('TYPE_STRING_EVENT',						'EVENTS');
define('TYPE_STRING_BUYBID',					'BUY/BID');
define('TYPE_STRING_MOVEFWD',					'MOVE FWD');
define('TYPE_STRING_GLIST',						'GUEST LIST');
define('TYPE_STRING_PACK',						'FIND A PACK');
define('TYPE_STRING_BAR',						'FIND A BAR');
define('TYPE_STRING_BUY',						'BUY');
define('TYPE_STRING_BID',						'BID');
/* End of file constants.php */
/* Location: ./application/config/constants.php */