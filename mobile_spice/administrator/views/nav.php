<!--start header-->
 <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo SITE_URL ?>">Marlboro</a>
      </div>
      
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           <ul class="nav navbar-nav">
                <?php if(isset($modules[USER_MANAGEMENT])): ?>
                <li>
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">User Management <span class="badge badge-important"><?php echo isset($modules[USER_MANAGEMENT]['total']) && $modules[USER_MANAGEMENT]['total'] > 0 ?  $modules[USER_MANAGEMENT]['total'] : ''?></span> <b class="caret"></b></a>
                     <ul class="dropdown-menu">
                          <?php if(in_array('registrant', $modules[USER_MANAGEMENT], true)): ?><li><a href="<?php echo SITE_URL ?>/registrant">User Profile <span class="badge badge-important"><?php echo $counts['registrant'] > 0 ? $counts['registrant'] : ''?></span></a></li><?php endif;  ?>
                          <?php if(in_array('profile', $modules[USER_MANAGEMENT], true)): ?><li><a href="<?php echo SITE_URL ?>/profile">User Profile - Photo <span class="badge badge-important"><?php echo $counts['profile'] > 0 ? $counts['profile'] : ''?></span></a></li><?php endif;  ?>
                          <?php if(in_array('community_manager', $modules[USER_MANAGEMENT], true)): ?><li><a href="<?php echo SITE_URL ?>/community_manager">Community Managers</a></li><?php endif;  ?>
                          <?php if(in_array('arclight', $modules[USER_MANAGEMENT], true)): ?>
                            <li class="dropdown-submenu"><a tabindex="-1" href="#">Arclight</a>
                             <ul class="dropdown-menu">
                                  <li><a href="<?php echo SITE_URL; ?>/arclight/inbound">Inbound</a></li>
                                  <li><a href="<?php echo SITE_URL; ?>/arclight/outbound">Outbound</a></li>
                                  <li><a href="<?php echo SITE_URL; ?>/arclight/outbound_match">Match</a></li>
                             </ul>
                           </li>
                           <?php endif ?>
                     </ul>
                </li>
                <?php endif; ?>
                <?php if(isset($modules[CONTENT_MANAGEMENT])): ?>
                <li>
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">Content Management <span class="badge badge-important"><?php echo isset($modules[CONTENT_MANAGEMENT]['total']) && $modules[CONTENT_MANAGEMENT]['total'] > 0 ?  $modules[CONTENT_MANAGEMENT]['total'] : ''?></span> <b class="caret"></b></a>
                     <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2">
                            <?php if(in_array('statement', $modules[CONTENT_MANAGEMENT], true)): ?><li><a href="<?php echo SITE_URL ?>/statement">Maybe Statements <span class="badge badge-important"><?php echo $counts['statement'] > 0 ? $counts['statement'] : ''?></span></a></li><?php endif;  ?>
                            <?php if(in_array('category', $modules[CONTENT_MANAGEMENT], true)): ?><li><a href="<?php echo SITE_URL ?>/category">Category Management</a></li><?php endif;  ?>
                            <?php if(in_array('game', $modules[CONTENT_MANAGEMENT], true)): ?><li><a href="<?php echo SITE_URL ?>/game">Games</a></li><?php endif;  ?>
                            <?php if(in_array('prize', $modules[CONTENT_MANAGEMENT], true)): ?><li><a href="<?php echo SITE_URL ?>/prize">Prizes</a></li><?php endif;  ?>
                            <?php if(in_array('move_fwd', $modules[CONTENT_MANAGEMENT], true)): ?><li><a href="<?php echo SITE_URL ?>/move_fwd">MoveFWD</a></li><?php endif;  ?>
                            <?php if(in_array('move_fwd', $modules[CONTENT_MANAGEMENT], true)): ?><li><a href="<?php echo SITE_URL ?>/settings">MoveFWD Mechanics</a></li><?php endif;  ?>
                            <?php if(in_array('hidden_pack', $modules[CONTENT_MANAGEMENT], true) || in_array('birthday_offer', $modules[CONTENT_MANAGEMENT], true) || in_array('flash_offer', $modules[CONTENT_MANAGEMENT], true) ): ?><li class="dropdown-submenu"><a tabindex="-1" href="#">System Notifications</a><?php endif;  ?>
                             <ul class="dropdown-menu">
                                   <?php if(in_array('hidden_pack', $modules[CONTENT_MANAGEMENT], true)): ?><li><a href="<?php echo SITE_URL ?>/hidden_pack">Hidden Marlboro Pack</a></li><?php endif;  ?>
                                   <?php if(in_array('birthday_offer', $modules[CONTENT_MANAGEMENT], true)): ?><li><a href="<?php echo SITE_URL ?>/birthday_offer">Birthday Offers</a></li><?php endif;  ?>
                                   <?php if(in_array('flash_offer', $modules[CONTENT_MANAGEMENT], true)): ?><li><a href="<?php echo SITE_URL ?>/flash_offer">Flash Offers</a></li><?php endif;  ?>
                              </ul>
                            </li>
                             <?php if(in_array('news', $modules[CONTENT_MANAGEMENT], true) || in_array('videos', $modules[CONTENT_MANAGEMENT], true) || in_array('events', $modules[CONTENT_MANAGEMENT], true)): ?><li class="dropdown-submenu"><a tabindex="-1" href="#">About</a><?php endif;  ?>
                             <ul class="dropdown-menu">
                                   <?php if(in_array('news', $modules[CONTENT_MANAGEMENT], true)): ?><li><a href="<?php echo SITE_URL; ?>/news">News</a></li><?php endif;  ?>
                                   <?php if(in_array('videos', $modules[CONTENT_MANAGEMENT], true)): ?><li><a href="<?php echo SITE_URL; ?>/videos">Videos</a></li><?php endif;  ?>
                                  <?php if(in_array('events', $modules[CONTENT_MANAGEMENT], true)): ?> <li><a href="<?php echo SITE_URL; ?>/events">Events</a></li><?php endif;  ?>
                             </ul>
                           </li>
                           <?php if(in_array('backstage_events', $modules[CONTENT_MANAGEMENT], true)): ?><li class="dropdown-submenu"><a tabindex="-1" href="#">Backstage Pass</a><?php endif;  ?>
                             <ul class="dropdown-menu">
                                  <?php if(in_array('backstage_events', $modules[CONTENT_MANAGEMENT], true)): ?><li><a href="<?php echo SITE_URL; ?>/backstage_events">Events</a></li><?php endif;  ?>
                                  <?php if(in_array('backstage_event_types', $modules[CONTENT_MANAGEMENT], true)): ?><li><a href="<?php echo SITE_URL; ?>/backstage_event_types">Events Types</a></li><?php endif;  ?>
                                  <?php if(in_array('regions', $modules[CONTENT_MANAGEMENT], true)): ?><li><a href="<?php echo SITE_URL; ?>/regions">Regions</a></li><?php endif;  ?>
                                  <?php if(in_array('venues', $modules[CONTENT_MANAGEMENT], true)): ?><li><a href="<?php echo SITE_URL; ?>/venues">Venues</a></li><?php endif;  ?>
                             </ul>
                           </li>
                     </ul>
                </li>
                <?php endif; ?>
                <?php if(isset($modules[MODERATOR])): ?>
                <li>
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">Moderation <span class="badge badge-important"><?php echo isset($modules[MODERATOR]['total']) && $modules[MODERATOR]['total'] > 0 ?  $modules[MODERATOR]['total'] : ''?></span> <b class="caret"></b></a>
                     <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2">
                       <?php if(in_array('event_photos', $modules[MODERATOR], true) || in_array('user_videos', $modules[MODERATOR], true)): ?>
                                <li class="dropdown-submenu">
                                <a tabindex="-1" href="#">About
                                  <?php if ($counts['event_photos'] > 0 || $counts['user_videos'] > 0) {?><span class="badge badge-important"><?php echo $counts['event_photos'] + $counts['user_videos'] ?> </span><?php } ?></a>
                            <?php endif;  ?>
                        <ul class="dropdown-menu">
                          <?php if(in_array('event_photos', $modules[MODERATOR], true)): ?><li><a href="<?php echo SITE_URL ?>/event_photos">User Photos <span class="badge badge-important"><?php echo $counts['event_photos'] > 0 ? $counts['event_photos'] : ''?></span></a></li><?php endif;  ?>
                          <?php if(in_array('user_videos', $modules[MODERATOR], true)): ?><li><a href="<?php echo SITE_URL ?>/user_videos">User Videos <span class="badge badge-important"><?php echo $counts['user_videos'] > 0 ? $counts['user_videos'] : ''?></span></a></li><?php endif;  ?>
                       </ul>
                       </li>
                       <?php if(in_array('backstage_event_photos', $modules[MODERATOR], true)): ?><li class="dropdown-submenu"><a tabindex="-1" href="#">Backstage Pass <span class="badge badge-important"><?php echo $counts['backstage_event_photos'] > 0 ? $counts['backstage_event_photos'] : ''?></span></a><?php endif;  ?>
                        <ul class="dropdown-menu">
                        <?php if(in_array('backstage_event_photos', $modules[MODERATOR], true)): ?><li><a href="<?php echo SITE_URL ?>/backstage_event_photos">User Photos <span class="badge badge-important"><?php echo $counts['backstage_event_photos'] > 0 ? $counts['backstage_event_photos'] : ''?></span></a></li><?php endif;  ?>

                        </ul>
                        </li>
                        <?php if(in_array('comment', $modules[MODERATOR], true)): ?><li><a href="<?php echo SITE_URL ?>/comment">Comments <span class="badge badge-important"><?php echo $counts['comment'] > 0 ? $counts['comment'] : ''?></span></a></li><?php endif;  ?>
                        <?php if(in_array('reply', $modules[MODERATOR], true)): ?><li><a href="<?php echo SITE_URL ?>/reply">Comment Replies <span class="badge badge-important"><?php echo $counts['reply'] > 0 ? $counts['reply'] : ''?></span></a></li><?php endif;  ?>
                        <?php if(in_array('move_fwd_gallery', $modules[MODERATOR], true)): ?><li><a href="<?php echo SITE_URL ?>/move_fwd_gallery">MoveFWD Gallery <span class="badge badge-important"><?php echo $counts['move_fwd_gallery'] > 0 ? $counts['move_fwd_gallery'] : ''?></span></a></li><?php endif;  ?>

                         
                    </ul>
                </li>
                <?php endif; ?>

                <?php if(isset($modules[REPORTS])): ?>
                <li>
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">Report <span class="badge badge-important"><?php echo isset($modules[REPORTS]['total']) && $modules[REPORTS]['total'] > 0 ?  $modules[REPORTS]['total'] : ''?></span> <b class="caret"></b></a>
                     <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2">
                        <?php if(in_array('activity_log', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL ?>/activity_log">Admin Activity <span class="badge badge-important"><?php echo $counts['activity_log'] > 0 ? $counts['activity_log'] : ''?></span></a></li><?php endif;  ?>
                        <?php if(in_array('user_statement', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL ?>/user_statement">User Statements <span class="badge badge-important"><?php echo $counts['user_statement'] > 0 ? $counts['user_statement'] : ''?></span></a></li><?php endif;  ?>
                        <?php if(in_array('user_birthday', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL ?>/user_birthday">User Birthday Prizes <span class="badge badge-important"><?php echo $counts['user_birthday'] > 0 ? $counts['user_birthday'] : ''?></a></li><?php endif;  ?>
                        <?php if(in_array('user_flash_offers', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL ?>/user_flash_offers">User Flash Prizes <span class="badge badge-important"><?php echo $counts['user_flash_offers'] > 0 ? $counts['user_flash_offers'] : ''?></a></li><?php endif;  ?>
                        <?php if(in_array('user_statements_summary', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL ?>/user_statements_summary">User Statements Summary</a></li><?php endif;  ?>
                        <?php if(in_array('user_birthday_redeemed', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL ?>/user_birthday_redeemed">User Birthday Prizes Redeemed</a></li><?php endif;  ?>
                       
                        <?php if(in_array('logins', $modules[REPORTS], true) || in_array('day_logins', $modules[REPORTS], true) ): ?>
                          <li class="dropdown-submenu" >
                            <a href="<?php echo SITE_URL ?>/logins">Logins </a>
                             <ul class="dropdown-menu" >
                                  <?php if(in_array('logins', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL; ?>/logins">Time of Day</a></li><?php endif;  ?>
                                  <?php if(in_array('day_logins', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL; ?>/day_logins">Spread (Days)</a></li><?php endif;  ?>
                                  <?php if(in_array('date_logins', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL; ?>/date_logins">Spread (Dates)</a></li><?php endif;  ?>
                             </ul> 
                          </li>
                        <?php endif;  ?>
                        <?php if(in_array('move_fwd_prizes', $modules[REPORTS], true) || in_array('prizes_redeemed', $modules[REPORTS], true) || in_array('move_fwd_visits', $modules[REPORTS], true)): ?>
                          <li class="dropdown-submenu" >
                            <a href="<?php echo SITE_URL ?>/move_fwd_prizes">MoveFWD <span class="badge badge-important"><?php echo $counts['move_fwd_prizes'] > 0 && in_array('move_fwd_prizes', $modules[REPORTS], true) ? $counts['move_fwd_prizes'] : ''?></a>
                             <ul class="dropdown-menu" >
                                  <?php if(in_array('move_fwd_prizes', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL; ?>/move_fwd_prizes">Accomplished Activities <span class="badge badge-important"><?php echo $counts['move_fwd_prizes'] > 0 && in_array('move_fwd_prizes', $modules[REPORTS], true) ? $counts['move_fwd_prizes'] : ''?></a></li><?php endif;  ?>
                                  <?php if(in_array('prizes_redeemed', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL; ?>/prizes_redeemed">Prizes Redeemed</a></li><?php endif;  ?>
                                  <?php if(in_array('move_fwd_visits', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL; ?>/move_fwd_visits">Visits</a></li><?php endif;  ?>
                             </ul> 
                          </li>
                        <?php endif;  ?>
                        <?php if(in_array('demographics', $modules[REPORTS], true) || in_array('reach', $modules[REPORTS], true) || in_array('reach_city', $modules[REPORTS], true)): ?>
                          <li class="dropdown-submenu" >
                            <a href="<?php echo SITE_URL ?>/demographics">Demographics </a>
                             <ul class="dropdown-menu" >
                                  <?php if(in_array('demographics', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL; ?>/demographics">Age</a></li><?php endif;  ?>
                                  <?php if(in_array('demographics', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL; ?>/demographics?type=gender">Gender</a></li><?php endif;  ?>
                                  <?php if(in_array('reach', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL ?>/reach">Provincial Reach</a></li><?php endif;  ?>
                                  <?php if(in_array('reach_city', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL ?>/reach_city">City Reach</a></li><?php endif;  ?>
                             </ul> 
                          </li>
                        <?php endif;  ?>
                         <?php if(in_array('game_log', $modules[REPORTS], true) || in_array('gameplay', $modules[REPORTS], true) || in_array('game_visits', $modules[REPORTS], true)): ?>
                          <li class="dropdown-submenu" >
                            <a href="<?php echo SITE_URL ?>/game_log">Games <span class="badge badge-important"><?php echo $counts['game_log'] > 0 ? $counts['game_log'] : ''?></a>
                             <ul class="dropdown-menu" >
                                  <?php if(in_array('game_log', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL; ?>/game_log">Logs <span class="badge badge-important"><?php echo $counts['game_log'] > 0 ? $counts['game_log'] : ''?></span></a></li><?php endif;  ?>
                                  <?php if(in_array('gameplay', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL; ?>/gameplay">Summary</a></li><?php endif;  ?>
                                  <?php if(in_array('game_visits', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL; ?>/game_visits">Visits</a></li><?php endif;  ?>
                             </ul> 
                          </li>
                        <?php endif;  ?>
                        <?php if(in_array('points', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL ?>/points">Points Accumulation</a></li><?php endif;  ?>
                        <?php if(in_array('page_visits', $modules[REPORTS], true)): ?><li><a href="<?php echo SITE_URL ?>/page_visits">Page Visits</a></li><?php endif;  ?>
                    </ul>
                </li>
                <?php endif; ?>


           </ul>

           <ul class="nav navbar-nav navbar-right">
                <li class="dropdown <?php echo $this->uri->segment(1) == 'accounts' ? 'active' : '' ?>">
                     <a href="#" class="dropdown-toggle>" data-toggle="dropdown">Settings <b class="caret"></b></a>
                     <ul class="dropdown-menu">
                          <li><a href="<?php echo SITE_URL ?>/accounts/my_account">My Account</a></li>
                          <?php if(@$admin): ?><li><a href="<?php echo SITE_URL ?>/user">Administration</a></li><?php endif;  ?>
                          <li><a href="<?php echo SITE_URL ?>/auth/logout">Logout</a></li>
                     </ul>
                </li>
           </ul>
      </div><!-- /.navbar-collapse -->
 </nav>
 <!--end header-->
