<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>City Reach</h3>
               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/reach_city/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
               </div>
		  </div>

          <!-- Nav tabs --> 
          <ul class="nav nav-tabs">
            <li class="active"><a href="#graph" data-toggle="tab">Graph</a></li>
            <li class=""><a href="#summary" data-toggle="tab">Summary</a></li>
          </ul>  

          <!-- Tab panes -->
          <div class="tab-content">
               <div class="tab-pane" id="summary">
                    <table class="table table-bordered" style="border-top:none;">
                         <thead>
                              <tr>
                                   <th width="1%">#</th>
                                   <th>City</th>
                                   <th>Count</th>
                                   <th>Percentage</th>
                                   <td>Operation</td>
                              </tr>
                         </thead>
                         <tbody>
                              <form>
                                   <tr>
                                        <td></td>
                                        <td>
                                          Province: 
                                          <select name="province" class="form-control">
                                            <option></option>
                                            <?php if($provinces): 
                                              foreach($provinces as $k => $v) {
                                                $selected = $this->input->get('province') && $this->input->get('province') == $v['province_id'] ? 'selected' : '';
                                                echo "<option $selected value='$v[province_id]'>$v[province]</option>";
                                              }
                                            endif;?>
                                          </select>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td>From:<input name="fromdate" class="form-control from" value="<?php echo isset($_GET['fromdate']) ? $_GET['fromdate'] : '' ?>" /><br> To:<input name="todate" class="form-control to" value="<?php echo isset($_GET['todate']) ? $_GET['todate'] : '' ?>" /><br><button class="btn btn-primary" name="search" value="1">Go</button></td>
                                   </tr>
                              </form>
                         		<?php if($records): 
          					foreach($records as $k => $v): ?>
                              <tr>
                                   <td><?php echo $k + 1 ?></td>
                                   <td><?php echo $v['city'] ?></td>
                                   <td><?php echo $v['count'] ?></td>
                                   <td><?php echo number_format(($v['count'] / $total) * 100, 2) ?>%</td>
                                   <td></td>
                              </tr>
                              <?php 
          					endforeach;
          					else:
          						echo '<tr><td colspan="7">No records found</td></tr>';
          					endif; ?>
                         </tbody>
                    </table>
               </div>
               <div class="tab-pane active" id="graph">
                    <div id="chart_entries" style="border:1px solid #DDDDDD; border-top:none;height:300px;">
                         
                    </div>
                    <br >
                 </div>
           </div>
           
     </div>
     <!--end main content -->

     <script type="text/javascript" src="<?php echo BASE_URL ?>admin_assets/js/google-api.js"></script>
     <script type="text/javascript">
     google.load("visualization", "1", {packages:["corechart"]});
     function drawChart() {
          var data = google.visualization.arrayToDataTable([
          ['Status', 'Value'],
          <?php foreach($records as $k => $v): ?>
          ['<?php echo $v["city"] ?>', <?php echo $v['count'] ?>],
          <?php endforeach; ?>
        ]);
          
          var options = {
          title: 'Reach <?php echo $this->input->get("fromdate") && $this->input->get("todate") ? "(" . date("F d, Y", strtotime($this->input->get("fromdate"))) . " - " .  date("F d, Y", strtotime($this->input->get("todate"))) . ")" : ""?>',
            is3D: true,
            /*colors:['#DDDDDD', '#90BC56','#3366CC', '#f00000'],*/
            // sliceVisibilityThreshold: 0       
          };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_entries'));
        chart.draw(data, options);
     }
     google.setOnLoadCallback(drawChart);
     $(window).resize(drawChart);
</script> 
  
