<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>User Quiz <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/user_quiz/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Questions</th>
                         <th>Result</th>
                         <th>Date</th>
                         <?php if($edit || $delete) : ?>
                         <th>Operation</th>
                         <?php endif; ?>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/user_quiz">
                         <tr>
                             <td></td>
                             <td><input name="name" class="form-control" value="<?php echo isset($_GET['name']) ? $_GET['name'] : '' ?>" /></td>
                             <td>
                                
                             </td>
                             <td><input name="result" class="form-control" value="<?php echo isset($_GET['result']) ? $_GET['result'] : '' ?>" /></td>
                             <td>
                             
                              <div class="row">
                                <div class="col-md-4">
                                     <input name="from_buy" placeholder="From" class="form-control from" value="<?php echo isset($_GET['from_buy']) ? $_GET['from_buy'] : '' ?>" />
                                </div>
                                <div class="col-md-4">
                                     <input name="to_buy" placeholder="To" class="to form-control" value="<?php echo isset($_GET['to_buy']) ? $_GET['to_buy'] : '' ?>" />
                                </div>
                              </div>
                            </td>
                             <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                        </tr>
                        </form>
                        
                    <?php if($users): 
          					foreach($users as $k => $v): 
                     
                     ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><a href="#" onclick="showRegistrantDetails(<?php echo $v['registrant_id'] ?>)"><?php echo $v['first_name'] . ' ' . $v['third_name'] ?></a></td>
                         <td>
                          <?php foreach ($v['questions'] as $qk => $qv) {?>
                          <strong>Question:</strong> <?php echo $qv['question'] ?><br >
                          <strong>Answer:</strong> <?php echo $qv['answer'] ?><br><br>
                          <?php } ?>
                         </td>
                         <td><?php echo $v['result'] ?></td>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['date_created']))  ?></td>
                         <td></td>
                     </tr>
                    <?php 
					endforeach;
					else:
						echo '<tr><td colspan="5">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->