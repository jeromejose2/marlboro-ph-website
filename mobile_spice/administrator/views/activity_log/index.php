<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Admin Activity Log <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>
               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/activity_log/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
               </div>
		  </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>User</th>
                         <th>Activity</th>
                         <th>IP</th>
                         <th>Date</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
               		<form action="<?php echo SITE_URL ?>/activity_log">
                     <tr>
                         <td></td>
                         <td>
                        	<select class="form-control" name="user">
                            	<option></option>
                                <?php 
									foreach($cms_users as $k => $v) {
										$selected =  isset($_GET['user']) && $_GET['user'] != '' && $_GET['user'] == $v['cms_user_id'] ? 'selected' : '';
										echo '<option value="' . $v['cms_user_id'] . '"' . $selected . '>' . $v['name'] . '</option>';	
									}
								?>
                            	
                            </select>
                         </td>
                         <td>
                         	<select class="form-control" name="activity">
                            	<option></option>
                                <?php 
									foreach($activities as $k => $v) {
										$selected =  isset($_GET['activity']) && $_GET['activity'] != '' && $_GET['activity'] == $v['description'] ? 'selected' : '';
										echo '<option value="' . $v['description'] . '"' . $selected . '>' . $v['description'] . '</option>';	
									}
								?>
                            	
                            </select>
                         </td>
                         <td></td>
                         <td>
                         <div class="row">
                              <div class="col-md-6 col-lg-4">
                                   <input name="from" placeholder="From" class="form-control from" value="<?php echo isset($_GET['from']) ? $_GET['from'] : '' ?>" />
                              </div>
                              <div class="col-md-6 col-lg-4">
                                   <input name="to" placeholder="To" class="form-control to" value="<?php echo isset($_GET['to']) ? $_GET['to'] : '' ?>" />
                              </div>
                         </div>
                        </td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
                    <?php if($users): 
					foreach($users as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['cms_user_name'] ?></td>
                         <td><?php echo $v['description'] ?> <?php echo isset($v['record_name']) ? '<a href="#" onclick="showLogDetails(' . $v['audit_trail_id'] . ')">Show Details</a>' : '' ?></td>
                         <td><?php echo $v['ip'] ?></td>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['timestamp'])) ?></td>
                         <td></td>
                         <td class="modal">
                             <div class="modal" id="log-details-<?php echo $v['audit_trail_id'] ?>">
                                <?php $changes = unserialize($v['field_changes']);?>
                                <strong>Record ID:</strong> <?php echo $v['record_id'] ?><br>
                                <strong>Name:</strong> <?php echo $v['record_name'] ?><br>
                                <?php if($v['message']): ?><strong>Message:</strong> <?php echo $v['message'] ?><br><?php endif; ?><br>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Field</th>
                                            <th>Old Value</th>
                                            <th>New Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if($changes['old']): 
											foreach($changes['old'] as $k => $v): ?>
											<tr>
                                                <td><?php echo $k  ?></td>
                                                <td><?php echo $changes['old'][$k]  ?></td>
                                                <td><?php echo $changes['new'][$k]  ?></td>
                                            </tr>
										<?php 
										endforeach;
										else:
										echo '<tr><td colspan="3">No changes found</td></tr>';
										endif; ?>	
                                    </tbody>
                                </table>
                             </div>
                         </td>
                    </tr>
                    <?php 
					endforeach;
					else:
						echo '<tr><td colspan="7">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->
     
   <script>
 	function showLogDetails(id) {
		bootbox.dialog({'message': $('#log-details-' + id).html(),
						'title': 'Admin Activity Log Details'});
	}
 </script>
