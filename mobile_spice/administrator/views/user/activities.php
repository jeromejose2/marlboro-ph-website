<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>User Accounts <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL ?>/user/add" class="btn btn-primary">Add User</a>
                   	<?php endif; ?>
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Account Name</th>
                         <th>Email</th>
                         <th>Position</th>
                         <?php if($edit || $delete) : ?>
                         <th>Operation</th>
                         <?php endif; ?>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/user">
                     <tr>
                         <td></td>
                         <td><input name="name" class="form-control" value="<?php echo isset($_GET['name']) ? $_GET['name'] : '' ?>" /></td>
                         <td><input name="email" class="form-control" value="<?php echo isset($_GET['email']) ? $_GET['email'] : '' ?>" /></td>
                         <td><input name="position" class="form-control" value="<?php echo isset($_GET['position']) ? $_GET['position'] : '' ?>" /></td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
                    <?php if($users): 
					foreach($users as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['name'] ?></td>
                         <td><?php echo $v['username'] ?></td>
                         <td><?php echo $v['position'] ?></td>
                         <?php if($edit || $delete) : ?>
                         <td>
                         	<?php if($edit): ?>
                            <a href="<?php echo SITE_URL ?>/user/edit/<?php echo $v['cms_user_id'] ?>" class="btn btn-primary">Edit</a>
                           <?php endif; ?>
                            <?php if($delete) : ?>
                            <a href="<?php echo SITE_URL ?>/user/delete/<?php echo $v['cms_user_id'] ?>/<?php echo md5($v['cms_user_id'] . ' ' . $this->config->item('encryption_key')) ?>" onclick="return confirmDeletion(this, 'user account')" class="btn btn-danger">Delete</a>
                            <?php endif; ?>
                         </td>
                         <?php endif; ?>
                    </tr>
                    <?php 
					endforeach;
					else:
						echo '<tr><td colspan="5">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->