<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3>Account Details</h3>
      </div>
       <div class="alert alert-danger" <?php if(!$error) echo 'style="display:none;"' ?>><?php echo $error; ?></div>
       <form class="form-horizontal" role="form" method="post" onsubmit="return submitFrm()">
           <div class="form-group">
                <label class="col-sm-2 control-label">Account Name</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo isset($user['name']) ? $user['name'] : ''  ?>" id="name" name="name" class="form-control required" data-name="account name">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo isset($user['username']) ? $user['username'] : ''  ?>" name="username" class="form-control required" id="email" data-name="email address">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Company</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo isset($user['company']) ? $user['company'] : ''  ?>" name="company" class="form-control required" data-name="ccompany">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Position</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo isset($user['position']) ? $user['position'] : ''  ?>" name="position" class="form-control required" data-name="position">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Password</label>
                <div class="col-sm-4">
                     <input type="password" value="" class="form-control <?php echo !isset($permissions) ? 'required' : '' ?>" name="password" id="password1" data-name="password">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Confirm Password</label>
                <div class="col-sm-4">
                     <input type="password" value="" class="form-control <?php echo !isset($permissions) ? 'required' : '' ?>" data-name="password confirmation" id="password2">
                </div>
           </div>
           <br ><br >
           <div class="page-header">
               <h3>Permissions</h3>
          </div>
          <?php if($module):
		  $cur_group = '';
		  foreach($module as $k => $v): 
		  	if($cur_group != $v['group']):
				echo '<div class="form-group"> <label style="font-size:16px;" class="col-sm-2 control-label">' . $mgroup[$v['group']] . '</label>
						<div class="col-sm-4">
							<input type="checkbox" class="form-control-static group-label" value="' . $v['group'] . '">	
						</div>
					</div>';
			endif;
			$cur_group = $v['group'];
		  ?>
           <div class="form-group">
                <label style="font-weight:normal;"  class="col-sm-2 control-label"><?php echo $v['module_name'] ?></label>
                <div class="col-sm-4">
                    <?php if($v['view']): ?>
                    <input type="checkbox" data-group="<?php echo $v['group'] ?>" name="view[]" value="<?php echo $v['module_id'] ?>" <?php echo isset($permissions) && @in_array($v['module_id'],$permissions['view']) ? 'checked="checked"': '' ?>> View
                    <?php endif; ?>
                    <?php if($v['add']): ?>
                    <input type="checkbox" data-group="<?php echo $v['group'] ?>" name="add[]" value="<?php echo $v['module_id'] ?>" <?php echo isset($permissions) && @in_array($v['module_id'],$permissions['add']) ? 'checked="checked"': '' ?>> Add
                    <?php endif; ?>
                    <?php if($v['edit']): ?>
                    <input type="checkbox" data-group="<?php echo $v['group'] ?>" name="edit[]" value="<?php echo $v['module_id'] ?>" <?php echo isset($permissions) && @in_array($v['module_id'],$permissions['edit']) ? 'checked="checked"': '' ?>> Edit
                    <?php endif; ?>
                    <?php if($v['delete']): ?>
                    <input type="checkbox" data-group="<?php echo $v['group'] ?>" name="delete[]" value="<?php echo $v['module_id'] ?>" <?php echo isset($permissions) && @in_array($v['module_id'],$permissions['delete']) ? 'checked="checked"': '' ?>> Delete
                    <?php endif; ?>
                </div>
           </div>
           <?php endforeach;
		   endif; ?>
           <div class="form-group"> <label style="font-size:16px;" class="col-sm-2 control-label">Settings</label></div>
           <div class="form-group">
                <label style="font-weight:normal;"  class="col-sm-2 control-label">CMS User Management</label>
                <div class="col-sm-4">
                    <input type="checkbox" name="view[]" value="2" <?php echo isset($permissions) && @in_array(2,$permissions['view']) ? 'checked="checked"': '' ?>> View
                    <input type="checkbox" name="add[]" value="2" <?php echo isset($permissions) && @in_array(2,$permissions['add']) ? 'checked="checked"': '' ?>> Add
                    <input type="checkbox" name="edit[]" value="2" <?php echo isset($permissions) && @in_array(2,$permissions['edit']) ? 'checked="checked"': '' ?>> Edit
                    <input type="checkbox" name="delete[]" value="2" <?php echo isset($permissions) && @in_array(2,$permissions['delete']) ? 'checked="checked"': '' ?>> Delete
                </div>
           </div>     
            
           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <input type="hidden" name="submit" value="1" />
                     <input type="hidden" name="id" value="<?php echo $this->uri->segment(3) ?>" />
                     <button type="submit" class="btn btn-primary">Submit</button>
                </div>
           </div>
           
           
      </form>
      
 </div>
 <!--end main content -->
 
 <script>
 	$(function() {
		$('.group-label').click(function() {
			group = $(this).val();
			checked = $(this).is(':checked');
			$('input[data-group="'+group+'"]').each(function() {
				$(this).prop('checked', checked);
			});
		});

    $('.group-label').each(function() {
      group = $(this).val();
      count = 0;
      theGroup = $(this);
      $('input[data-group="'+group+'"]').each(function() {
        if($(this).is(':checked'))
          count++;
      });
      if(count == $('input[data-group="'+group+'"]').length)
        theGroup.prop('checked', true);
    });
	})
 </script>
