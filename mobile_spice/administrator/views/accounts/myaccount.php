<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3>My Account</h3>
      </div>
       <div class="alert alert-danger" <?php if(!$error && !isset($_GET['ref'])) echo 'style="display:none;"' ?>><?php echo isset($_GET['ref']) ? 'Sorry, you need to update your password to proceed' : $error; ?></div>
       <form class="form-horizontal" role="form" method="post" onsubmit="return submitFrm()">
           <div class="form-group">
                <label class="col-sm-2 control-label">Account Name</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo $user_details['name'] ?>" id="name" name="name" class="form-control required" data-name="account name">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Old Password</label>
                <div class="col-sm-4">
                     <input type="password" class="form-control" name="opassword">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">New Password</label>
                <div class="col-sm-4">
                     <input type="password" class="form-control" name="npassword" id="password1">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Confirm New Password</label>
                <div class="col-sm-4">
                     <input type="password" class="form-control" name="cpassword" id="password2">
                </div>
           </div>
           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <input type="hidden" name="submit" value="1" />
                     <button type="submit" class="btn btn-primary">Submit</button>
                </div>
           </div>
      </form>
      
 </div>
 <!--end main content -->