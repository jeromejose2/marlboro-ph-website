<?php 
  $controller = $this->uri->segment(1);
  $label = '';
  switch($controller) {
    case 'birthday_offer':
      $label = 'Birthday Offer';
      break;
    case 'flash_offer':
      $label = 'Flash Offer';
      break;
    default:
      $label = 'Prize';
      break;

  }

?>
<?php $this->load->view('editor.php') ?>
<script src="<?php echo BASE_URL ?>admin_assets/js/jquery.Jcrop.js"></script>
<script type="text/javascript">
var jscrop_api;
function initJCrop(elem) {
  <?php if(isset($record['thumbnail_meta'])):  
    $points = isset($record['thumbnail_meta']) ? unserialize($record['thumbnail_meta']) : null; ?>
    x1 = <?php echo $points['x1'] != '' ? $points['x1'] : 0 ?>;
    x2 = <?php echo $points['x2'] != '' ? $points['x2'] : 0 ?>;
    y1 = <?php echo $points['y1'] != '' ? $points['y1'] : 0 ?>;
    y2 = <?php echo $points['y2'] != '' ? $points['y2'] : 0 ?>;
  <?php else: ?>  
    x1 = 0;
    y1 = 0;
    x2 = 200;
    y2 = 200;     
  <?php endif; ?>;
  
  setTimeout(function() {
    if(jscrop_api)
      jcrop_api.destroy();
    $('#preview').Jcrop({
      onChange:   showCoords,
      onSelect:   showCoords,
      setSelect: [x1, y1, x2, y2],
      maxSize: [ 300, 300 ]
    },function(){
      jcrop_api = this;
    }); 
    $('#width').val($('#preview').width());
    $('#height').val($('#preview').height());
  }, 500);
}

function showCoords(c) {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
};

$(function() {
  <?php if(isset($record['prize_image']) && $record['prize_image'] != ''): ?>
  initJCrop($('#photo'));
  <?php endif; ?>

  $('#type').change(function() {
    if($(this).val() == 'Redemption') {
      $('.r-address-container').show();
    } else {
      $('#r-address-container').hide();
    }
  });

});

</script>
<link rel="stylesheet" href="<?php echo BASE_URL ?>admin_assets/css/jquery.Jcrop.css" type="text/css" />

<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3><?php echo $label ?></h3>
      </div>
       <div class="alert alert-danger" <?php if(!$error) echo 'style="display:none;"' ?>><?php echo $error; ?></div>
       <form class="form-horizontal" role="form" method="post" onsubmit="return submitFrm()" enctype="multipart/form-data">
           <div class="form-group">
                <label class="col-sm-2 control-label">Name</label>
                <div class="col-sm-4">
                     <input type="text" name="prize_name" value="<?php echo isset($record['prize_name']) ? $record['prize_name'] : '' ?>" data-name="prize name" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Month</label>
                <div class="col-sm-4">
                    <select class="form-control" name="month">
                      <option value="1" <?php echo isset($record['month']) && $record['month'] == 1 ? 'selected' : '' ?>>January</option>
                      <option value="2" <?php echo isset($record['month']) && $record['month'] == 2 ? 'selected' : '' ?>>February</option>
                      <option value="3" <?php echo isset($record['month']) && $record['month'] == 3 ? 'selected' : '' ?>>March</option>
                      <option value="4" <?php echo isset($record['month']) && $record['month'] == 4 ? 'selected' : '' ?>>April</option>
                      <option value="5" <?php echo isset($record['month']) && $record['month'] == 5 ? 'selected' : '' ?>>May</option>
                      <option value="6" <?php echo isset($record['month']) && $record['month'] == 6 ? 'selected' : '' ?>>June</option>
                      <option value="7" <?php echo isset($record['month']) && $record['month'] == 7 ? 'selected' : '' ?>>July</option>
                      <option value="8" <?php echo isset($record['month']) && $record['month'] == 8 ? 'selected' : '' ?>>August</option>
                      <option value="9" <?php echo isset($record['month']) && $record['month'] == 9 ? 'selected' : '' ?>>September</option>
                      <option value="10" <?php echo isset($record['month']) && $record['month'] == 10 ? 'selected' : '' ?>>October</option>
                      <option value="11" <?php echo isset($record['month']) && $record['month'] == 11 ? 'selected' : '' ?>>November</option>
                      <option value="12" <?php echo isset($record['month']) && $record['month'] == 12 ? 'selected' : '' ?>>December</option>
                    </select>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Image</label>
                <div class="col-sm-4">
                     <input type="file" id="photo" name="photo" value="" data-name="module name" class="" onchange="previewImage('photo', 'preview', initJCrop, this);">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Image Preview</label>
                <div class="col-sm-4">
                   <img id="preview" class="preview" <?php echo isset($record['prize_image']) ? "style='max-width:500px;' src='" . BASE_URL . 'uploads/prize/' . $record['prize_image'] . "'" : '' ?>  />
                </div>
           </div>
          <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-8">
                     <textarea name="description"><?php echo isset($record['description']) ? $record['description'] : '' ?></textarea>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Stock</label>
                <div class="col-sm-4">
                     <input type="text" name="stock" value="<?php echo isset($record['stock']) ? $record['stock'] : '' ?>" onkeydown="return checkDigit(event)" data-name="prize stock" class="form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Status</label>
                <div class="col-sm-4">
                    <select class="form-control" name="status">
                      <option <?php echo isset($record['status']) && $record['status'] == 1 ? 'selected' : '' ?> value="1">Published</option>
                      <option <?php echo isset($record['status']) && $record['status'] == 0 ? 'selected' : '' ?> value="0">Unpublished</option>   
                    </select>
                </div>
           </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Type</label>
                <div class="col-sm-4">
                    <select class="form-control" name="send_type" id="type">
                      <option <?php echo isset($record['send_type']) && $record['send_type'] == 'Delivery' ? 'selected' : '' ?>>Delivery</option> 
                      <option <?php echo isset($record['send_type']) && $record['send_type'] == 'Redemption' ? 'selected' : '' ?>>Redemption</option>  
                    </select>
                </div>
           </div>
           <div class="form-group r-address-container" <?php if(!isset($record['send_type']) || (isset($record['send_type']) && $record['send_type'] == 'Delivery')): ?>style="display:none;" <?php endif; ?>>
                <label class="col-sm-2 control-label">Redemption Address</label>
                <div class="col-sm-8">
                     <textarea name="redemption_address"><?php echo isset($record['redemption_address']) ? $record['redemption_address'] : '' ?></textarea>
                </div>                
           </div>

           <div class="form-group r-address-container" <?php if(!isset($record['send_type']) || (isset($record['send_type']) && $record['send_type'] == 'Delivery')): ?>style="display:none;" <?php endif; ?>>
                <label class="col-sm-2 control-label">Redemption Instructions</label>
                <div class="col-sm-8">
                     <textarea name="redemption_instruction"><?php echo isset($record['redemption_instruction']) ? $record['redemption_instruction'] : '' ?></textarea>
                </div>
            </div>
          
           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <input type="hidden" name="submit" value="1" />
                     <input type="hidden" id="x" name="x" />
                     <input type="hidden" id="y" name="y" />
                     <input type="hidden" id="x2" name="x2" />
                     <input type="hidden" id="y2" name="y2" />
                     <input type="hidden" id="width" name="width" />
                     <input type="hidden" id="height" name="height"/>
                     <input type="hidden" name="id" value="<?php echo isset($record['prize_id']) ? $record['prize_id'] : '' ?>" />
                     <input type="hidden" name="filename" value="<?php echo isset($record['prize_image']) ? $record['prize_image'] : '' ?>" />
                     <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                </div>
           </div>
      </form>
      
 </div>
 <!--end main content -->
 