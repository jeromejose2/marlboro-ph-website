<?php 
  $controller = $this->uri->segment(1);
  $label = '';
  switch($controller) {
    case 'birthday_offer':
      $label = 'Birthday Offer';
      break;
    case 'flash_offer':
      $label = 'Flash Offer';
      break;
    default:
      $label = 'Prize';
      break;

  }
?>
<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3><?php echo $label ?>s <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>
               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL ?>/export/birthday_offer?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
                    <?php endif; ?>
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Thumbnail</th>
                         <th>Type</th>
                         <th>Stock</th>
                         <th>Status</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
               		  <?php if($categories): 
					foreach($categories as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['prize_name'] ?></td>
                         <td><img src="<?php echo $v['prize_image'] ? BASE_URL . 'uploads/prize/' . $v['prize_image'] : BASE_URL . 'images/no_image.jpg' ?>?uniqid=<?php echo uniqid() ?>" width="150" /></td>
                         <td><?php echo $v['send_type'] ?></td>
                         <td><?php echo $v['stock'] ?></td>
                         <td><?php echo $v['status'] == 1 ? 'Published' : 'Unpublished' ?></td>
                         <td>
                            <?php if($edit): ?>
                            <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/edit/<?php echo $v['prize_id'] ?>" class="btn btn-primary">Edit</a>
                            <?php endif; ?>
                         </td>
                    </tr>
                    <?php 
					endforeach;
					endif; ?>
               </tbody>
          </table>

          

     </div>
     <!--end main content -->