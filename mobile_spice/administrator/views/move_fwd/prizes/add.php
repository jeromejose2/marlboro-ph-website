<?php 
  $controller = $this->uri->segment(1);
  $label = 'MoveFWD Prize';
?>
<?php $this->load->view('editor.php') ?>

<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3><?php echo $label ?></h3>
      </div>
       <div class="alert alert-danger" <?php if(!$error) echo 'style="display:none;"' ?>><?php echo $error; ?></div>
       <form class="form-horizontal" role="form" method="post" onsubmit="return submitFrm()" enctype="multipart/form-data">
           <div class="form-group">
                <label class="col-sm-2 control-label">Move Forward Title</label>
                <div class="col-sm-4">
                    <?php echo isset($record['move_forward_title']) ? $record['move_forward_title'] : '' ?>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Prize</label>
                <div class="col-sm-4">
                    <?php echo isset($record['prize_name']) ? $record['prize_name'] : '' ?>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Name</label>
                <div class="col-sm-4">
                    <?php echo isset($record['first_name']) ? $record['first_name'] . ' ' . $record['third_name'] : '' ?>
                </div>
           </div>
          <div class="form-group">
                <label class="col-sm-2 control-label">Prize Preview</label>
                <div class="col-sm-4">
                   <img id="preview" class="preview" <?php echo isset($record['prize_image']) ? "style='max-width:500px;' src='" . BASE_URL . 'uploads/prize/' . $record['prize_image'] . "'" : '' ?>  />
                </div>
           </div>
          <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-8">
                     <?php echo isset($record['description']) ? $record['description'] : '' ?>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Stock</label>
                <div class="col-sm-4">
                     <?php echo isset($record['slots']) ? $record['slots'] : '' ?>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Status</label>
                <div class="col-sm-4">
                    <select class="form-control" name="prize_status">
                      <option <?php echo isset($record['prize_delivered']) && $record['prize_delivered'] == 0 ? 'selected' : '' ?> value="0">Pending</option>
                      <option <?php echo isset($record['prize_delivered']) && $record['prize_delivered'] == 1 ? 'selected' : '' ?> value="1"><?php echo $record['send_type'] == 'Delivery' ? 'Delivered' : 'Redeemed' ?></option>
                    </select>
                </div>
           </div>
           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <input type="hidden" name="submit" value="1" />
                    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                </div>
           </div>
      </form>
      
 </div>
 <!--end main content -->
 