<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>MoveFWD Gallery<span class="badge badge-important"><?php echo (int)$total_rows; ?></span></h3>

               <div class="actions">
                  <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/add_media?backstage_event_id='.$this->input->get('backstage_event_id'); ?>" class="btn btn-primary media-upload">Add Media</a>
                  <a href="<?php echo SITE_URL.'/backstage_events/export_photos'.$query_strings; ?>" class="btn btn-primary" target="_blank">Export</a>
                </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                       <th><center>Photo</center></th>
                          <th>MoveFWD Offer</th>
                          <th>Uploaded By</th>
                          <th>Date Added</th>                       
                          <th>Operation</th>                         
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>">
                     <tr>
                        <th></th>
                        <th><select name="move_fwd_offer" class="form-control" >
                            <option value=""></option>
                            <?php if($move_fwd_offers->num_rows()){
                              foreach($move_fwd_offers->result() as $e){
                                      if($this->input->get('move_fwd_offer')==$e->move_forward_id){ ?>
                                        <option value="<?=$e->move_forward_id?>" selected><?=$e->move_forward_title?></option>
                                <?php }else{ ?>
                                        <option value="<?=$e->move_forward_id?>"><?=$e->move_forward_title?></option>
                                <?php } 
                                 }
                            } ?>
                          </select></th>
                        <th><input type="text" class="form-control" name="submitted_by" value="<?=$this->input->get('submitted_by')?>"></th>
                         
                       <th>
                          <div class="row">
                            <div class="col-md-6 col-lg-4">
                                 <input name="from_date_added" placeholder="From" class="form-control from" value="<?=$this->input->get('from_date_added')?>" />
                            </div>
                            <div class="col-md-6 col-lg-4">
                                 <input name="to_date_added" placeholder="To" class="form-control to" value="<?=$this->input->get('to_date_added')?>" />
                            </div>
                          </div>
                      </th>         
                      <th><button type="submit" class="btn btn-primary">Go</button></th>                     
                     </tr>
                  </form>

                  <?php



                   if($rows->num_rows()){
 
 
                            foreach($rows->result() as $v){  

                              if($v->registrant_id){
                              
                                $uploader_id = $v->registrant_id;
                                $uploader_name = $v->first_name.' '.$v->third_name;
                              
                              }else{
                              
                                $uploader_id = $v->admin_id;
                                $uploader_name = $v->admin_name;
                              
                              }?>
                              <tr>
                                  
                                  <td><center>
                                    <a href="<?=SITE_URL.'/'.$this->router->class.'/view_photo/'.$v->comment_id?>" title="Click to Full view" class="view-media">
                                    <img src="<?=BASE_URL.'uploads/profile/admin_'.$uploader_id.'/comments/150_150_'.$v->comment_image?>" />
                                  </a></center></td>
                                  <td><?=$v->move_forward_title?></td>
                                  <td><?=$uploader_name?></td>
                                  <td><?=$v->comment_date_created?></td> 
                                                                
                                  <td>
                                  <?php if($edit){ ?>
                                      <a href="<?php echo SITE_URL.'/'.$this->router->class;?>/edit_media/<?=$v->comment_id?>" class="btn btn-primary edit-media" target="_blank">Edit</a>
                                  <?php } ?>
                                  <?php if($delete){ ?>
                                    <a href="<?php echo SITE_URL?>" class="btn btn-primary" target="_blank">Delete</a>
                                  <?php } ?>
                                   </td>
                                   
                                
                              </tr>
                  <?php     } 
                        }else{ ?>
                          <tr><td colspan="6">No records found.</td></tr>
                  <?php  } ?>
                  
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->

     <div id="view-media" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content modal-content-media">
              <div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>
            </div>
          </div> 
      </div>

      <div id="media-upload" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content modal-content-media-upload">
              <div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>
            </div>
          </div> 
      </div>
     <script>
 
     $('.media-upload,.edit-media').on('click',function(e){

           $.ajax({url:$(this).attr('href'),
                  cache:false,
                  beforeSend: function(){
                    $('#media-upload').modal('show');
                  },
                  success: function(data){
                     $('.modal-content-media-upload').html(data);
                  }
                });
          e.preventDefault();
     });

     $('#media-upload').on('hidden.bs.modal',function(){
        location.reload();
     });
 
    $('.view-media').on('click',function(e){
          var href = $(this).attr('href');
          $('.modal-content-media').html('<div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>');
          $('#view-media').modal('show');
          $('.modal-content-media').load(href);
          e.preventDefault();
    });
     </script>
