
<script src="<?=BASE_URL?>admin_assets/js/angular-js/angular-file-upload-shim.js"></script>
 <script src="<?=BASE_URL?>admin_assets/js/angular-js/angular.js"></script>
<script src="<?=BASE_URL?>admin_assets/js/angular-js/angular-file-upload.js"></script>
<script src="<?=BASE_URL?>admin_assets/js/angular-js/upload-move-fwd-gallery.js"></script>
 


<div class="modal-header">
  <button  type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <h4 class="modal-title"><?=$header_title?></h4>
</div>

<div class="modal-body text-center"  ng-controller="MyCtrl">

   <form class="form-horizontal" action="<?=SITE_URL?>/move_fwd_new_gallery/save_media" role="form" method="post" enctype="multipart/form-data" id="form">
    <input type="hidden" name="myModel" ng-model="myModel" value="1"/>

        <div class="form-group">
            <label class="col-sm-3 control-label">MoveFWD Offer:</label>
            <div class="col-sm-5">
                 
                 <select name="move_fwd_offer" class="form-control" >
                            <?php if($move_fwd_offers->num_rows()){
                              foreach($move_fwd_offers->result() as $e){
                                      if($this->input->get('move_fwd_offer')==$e->move_forward_id){ ?>
                                        <option value="<?=$e->move_forward_id?>" selected><?=$e->move_forward_title?></option>
                                <?php }else{ ?>
                                        <option value="<?=$e->move_forward_id?>"><?=$e->move_forward_title?></option>
                                <?php } 
                                 }
                            } ?>
                          </select>
            </div>
       </div>

       <div class="form-group">
            <label class="col-sm-3 control-label"><?=$file_label?></label>
            <div class="col-sm-5">
                 <input type="file" ng-file-select="onFileSelect($files)" multiple class="form-control"> <br>
                      <strong>Width: 430px. Height: 235px.</strong>
                      <div class="clearfix"></div>
            </div>
       </div>
        
       <div class="form-group preview" style="display:none">
           <div class="col-md-12" >
            <div class="row" ng-show="selectedFiles != null">

              <div class="col-md-4 col-xs-3" ng-repeat="f in selectedFiles">
                <div class="thumbnail">
                  <img ng-show="dataUrls[$index]" ng-src="{{dataUrls[$index]}}">
                  <div class="caption">
                      <p class="text-danger">{{uploadError[$index]}} {{removeError[$index]}}</p>
                      <input type="text" name="media[]" ng-model="uploadResult[$index]" style="visibility:hidden;" >
                      <span class="progress progress-striped" ng-show="progress[$index] >= 0">           
                      <div class="progress-bar" style="width:{{progress[$index]}}%">{{progress[$index]}}%</div>
                    </span>
                    <div class="clearfix"></div>
                    <p>size: {{f.size}}B<br/>type: {{f.type}}</p>
                    <button type="button" class="btn" ng-click="abort($index)" ng-show="hasUploader($index) && progress[$index] < 100">Abort</button>
                    <button type="button" class="btn btn-danger " ng-click="remove($index)" ng-show="progress[$index] >= 100"><span class="glyphicon glyphicon-trash"></span> Remove</button>
                  </div>

                </div>

                <div class="{{$index + 1 % 4 == 0 ? 'clearfix' : ''}}"></div>
                 
              </div>
              <div class="clearfix"></div>
              <div class="col-md-12" ng-show="selectedFiles.length == uploadResult.length"><br/><button type="submit" class="btn btn-primary">Submit</div>

            </div> 
          </div>

       </div>
       <input type="text" ng-model="uploadURL" ng-init="uploadURL='<?=SITE_URL?>/move_fwd_new_gallery/upload_media'" style="visibility:hidden;"> 
       <input type="text" ng-model="removeURL" ng-init="removeURL='<?=SITE_URL?>/move_fwd_new_gallery/delete_media_files'" style="visibility:hidden;">
        

   </form>

</div>


<script>

    angular.element(document).ready(function() {
      angular.bootstrap('.modal-content-media-upload', ['fileUpload']);
      $('.preview').show();
 

    });
 
</script>
  
     
      