<!--start main content -->
     <div class="container main-content" ng-app="entryReport" ng-controller="ageController">
          <div class="page-header">
               <h3>MoveFWD Entries <?=@$date_range['start_date'] && @$date_range['end_date'] ? '('.date('F d, Y',strtotime(@$date_range['start_date'])).' to '.date('F d, Y',strtotime(@$date_range['end_date'])).')' : ''?></h3>
               <div class="actions">
                    <a href="{{exportTest}}" class="btn btn-primary">Export</a>
               </div>
		      </div>

          <!-- Nav tabs --> 
          <ul class="nav nav-tabs">
            <li class="active"><a href="#graph" id="graph-btn" ng-click="setExportButton('#')" data-toggle="tab">Graph</a></li>
            <li class=""><a href="#entry_per_activity" ng-click="setExportButton('<?=SITE_URL?>/move_fwd_entries/export?<?php echo $query_string; ?>&export=activity_name')" id="entry_per_activity-btn" data-toggle="tab">Activity Summary</a></li>
            <li class=""><a href="#entry_per_category" ng-click="setExportButton('<?=SITE_URL?>/move_fwd_entries/export?<?php echo $query_string; ?>&export=category_name')" id="entry_per_category-btn" data-toggle="tab">Category Summary</a></li>
          </ul>  

          <!-- Tab panes -->
          <div class="tab-content">

            <div class="tab-pane" id="entry_per_activity">
                    <table class="table table-bordered" style="border-top:none;" >
                         <thead>
                              <tr>
                                   <th><center>#</center></th>
                                   <th>Activity</th>
                                   <th>Pending</th>
                                   <th>Approved</th>
                                   <th>Rejected</th>
                                   <th>Total Entries</th>
                                   <th>Percentage</th>
                                   <td>Operation</td>
                              </tr>
                         </thead>
                         <tbody>
                              <form action="<?=SITE_URL.'/'.$this->router->class?>">
                                   <tr>
                                        <td><input type="hidden" name="current_tab" value="entry_per_activity"> </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                         <td>From:<input name="start_date" class="form-control from" value="<?=$date_range['start_date']?>" /><br> 
                                          To:<input name="end_date" class="form-control to" value="<?=$date_range['end_date']?>"/><br>                                        
                                          Sort By:
                                          <select name="sort_by" class="form-control from">
                                            <option <?=$this->input->get('sort_by')=='activity_name' ? 'selected' : ''?> value="activity_name">Activity Name</option>
                                            <option <?=$this->input->get('sort_by')=='pending' ? 'selected' : ''?> value="pending">Pending</option>
                                            <option <?=$this->input->get('sort_by')=='approved' ? 'selected' : ''?> value="approved">Approved</option>
                                            <option <?=$this->input->get('sort_by')=='rejected' ? 'selected' : ''?> value="rejected">Rejected</option>
                                            <option <?=$this->input->get('sort_by')=='entry_count_per_activity' ? 'selected' : ''?> value="entry_count_per_activity">Total Entries/Percentage</option>
                                           </select><br>
                                          Sort Order:  
                                          <select name="sort_order" class="form-control from">
                                            <option <?=$this->input->get('sort_order')=='ASC' ? 'selected' : ''?>>ASC</option>
                                            <option <?=$this->input->get('sort_order')=='DESC' ? 'selected' : ''?>>DESC</option>
                                           </select><br>
                                          <button type="submit" class="btn btn-primary" name="search" value="1">Go</button></td>
                                   </tr>
                              </form>
                              <?php if($activities->num_rows()){ $c = 0;
                                      $percentage = 0;
                                      $total_counted_entries = 0;
                                      $total_percentage = 0;
                                      $pending = 0;
                                      $approved = 0;
                                      $rejected = 0;
                                      foreach($activities->result() as $v){
                                        $c++;
                                        $total_counted_entries += $v->entry_count_per_activity; 
                                        $percentage = round(($v->entry_count_per_activity / $total_entries) * 100,2);
                                        $total_percentage += $percentage;
                                        $pending += $v->pending;
                                        $approved += $v->approved;
                                        $rejected += $v->rejected; ?>
                                        <tr>
                                            <td><center><?=$c?></center></td>
                                            <td><?=ucwords(strtolower($v->activity_name))?></td>
                                            <td><?php if($v->pending > 0){ ?>
                                                        <a class="view-photo" href="<?=SITE_URL?>/move_fwd_entries/breakdown_by_activity?move_forward_id=<?=$v->move_forward_id?>&status=0&end_date=<?=$date_range['end_date']?>&start_date=<?=$date_range['start_date']?>"><?=$v->pending?></a>
                                                <?php }else{ echo $v->pending; }?></td>
                                            <td>
                                              <?php if($v->approved > 0){ ?>
                                                        <a class="view-photo" href="<?=SITE_URL?>/move_fwd_entries/breakdown_by_activity?move_forward_id=<?=$v->move_forward_id?>&status=1&end_date=<?=$date_range['end_date']?>&start_date=<?=$date_range['start_date']?>"><?=$v->approved?></a>
                                              <?php }else{ echo $v->approved; }?></td>
                                            <td>
                                              <?php if($v->rejected > 0){ ?>
                                                      <a class="view-photo" href="<?=SITE_URL?>/move_fwd_entries/breakdown_by_activity?move_forward_id=<?=$v->move_forward_id?>&status=2&end_date=<?=$date_range['end_date']?>&start_date=<?=$date_range['start_date']?>"><?=$v->rejected?></a>                                              
                                              <?php }else{ echo $v->rejected; } ?>
                                                  </td>                                            
                                            <td><a class="view-photo" href="<?=SITE_URL?>/move_fwd_entries/breakdown_by_activity?move_forward_id=<?=$v->move_forward_id?>&end_date=<?=$date_range['end_date']?>&start_date=<?=$date_range['start_date']?>"><?=$v->entry_count_per_activity?></a></td>
                                            <td><?=$percentage?></td>
                                            <td></td>
                                        </tr>
                              <?php } ?>
                              
                              <tr>
                                <td></td>
                                <td><strong>Total</strong></td>
                                <td><?=$pending?></td>
                                <td><?=$approved?></td>
                                <td><?=$rejected?></td>
                                <td><?=$total_counted_entries?></td>
                                <td><?=round($total_percentage,0)?>%</td>
                            </tr>
                            <?php  }else{ ?>
                            <tr><td colspan="7">No result found.</td></tr>

                           <?php } ?>
                       </tbody>
                    </table>
               </div>

               <div class="tab-pane" id="entry_per_category">
                    <table class="table table-bordered" style="border-top:none;" >
                         <thead>
                              <tr>
                                   <th><center>#</center></th>
                                   <th>Category</th>
                                   <th>Pending</th>
                                   <th>Approved</th>
                                   <th>Rejected</th>
                                   <th>Total Entries</th>
                                   <th>Percentage</th>
                                   <td>Operation</td>
                              </tr>
                         </thead>
                         <tbody>
                              <form action="<?=SITE_URL.'/'.$this->router->class?>">
                                   <tr>
                                        <td><input type="hidden" name="current_tab" value="entry_per_category"> </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                         <td>From:<input name="start_date" class="form-control from" value="<?=$date_range['start_date']?>" /><br> 
                                          To:<input name="end_date" class="form-control to" value="<?=$date_range['end_date']?>"/><br>                                         
                                          Sort By:
                                          <select name="cat_sort_by" class="form-control from">
                                            <option <?=$this->input->get('cat_sort_by')=='category_name' ? 'selected' : ''?> value="category_name">Activity Name</option>
                                            <option <?=$this->input->get('cat_sort_by')=='pending' ? 'selected' : ''?> value="pending">Pending</option>
                                            <option <?=$this->input->get('cat_sort_by')=='approved' ? 'selected' : ''?> value="approved">Approved</option>
                                            <option <?=$this->input->get('cat_sort_by')=='rejected' ? 'selected' : ''?> value="rejected">Rejected</option>
                                            <option <?=$this->input->get('cat_sort_by')=='entry_per_category' ? 'selected' : ''?> value="entry_per_category">Total Entries/Percentage</option>
                                           </select><br>
                                          Sort Order:  
                                          <select name="sort_order" class="form-control from">
                                            <option <?=$this->input->get('sort_order')=='ASC' ? 'selected' : ''?>>ASC</option>
                                            <option <?=$this->input->get('sort_order')=='DESC' ? 'selected' : ''?>>DESC</option>
                                           </select><br>
                                          <button type="submit" class="btn btn-primary" name="search" value="1">Go</button></td>
                                   </tr>
                              </form>
                              <?php if($categories->num_rows()){ $c = 0;
                                      $percentage = 0;
                                      $total_counted_entries = 0;
                                      $total_percentage = 0;
                                      $pending = 0;
                                      $approved = 0;
                                      $rejected = 0;
                                      foreach($categories->result() as $v){
                                        $c++;
                                        $total_counted_entries += $v->entry_count_per_category; 
                                        $percentage = round(($v->entry_count_per_category / $total_entries) * 100,2);
                                        $total_percentage += $percentage;
                                        $pending += $v->pending;
                                        $approved += $v->approved;
                                        $rejected += $v->rejected; ?>
                                        <tr>
                                            <td><center><?=$c?></center></td>
                                            <td><?=ucwords(strtolower($v->category_name))?></td>
                                            <td><?php if($v->pending > 0){ ?>
                                                        <a class="view-photo" href="<?=SITE_URL?>/move_fwd_entries/breakdown_by_category?category_id=<?=$v->category_id?>&status=0&end_date=<?=$date_range['end_date']?>&start_date=<?=$date_range['start_date']?>"><?=$v->pending?></a>
                                                <?php }else{ echo $v->pending; }?></td>
                                            <td>
                                              <?php if($v->approved > 0){ ?>
                                                        <a class="view-photo" href="<?=SITE_URL?>/move_fwd_entries/breakdown_by_category?category_id=<?=$v->category_id?>&status=1&end_date=<?=$date_range['end_date']?>&start_date=<?=$date_range['start_date']?>"><?=$v->approved?></a>
                                              <?php }else{ echo $v->approved; }?></td>
                                            <td>
                                              <?php if($v->rejected > 0){ ?>
                                                      <a class="view-photo" href="<?=SITE_URL?>/move_fwd_entries/breakdown_by_category?category_id=<?=$v->category_id?>&status=2&end_date=<?=$date_range['end_date']?>&start_date=<?=$date_range['start_date']?>"><?=$v->rejected?></a>                                              
                                              <?php }else{ echo $v->rejected; } ?>
                                                  </td>                                            
                                            <td><a class="view-photo" href="<?=SITE_URL?>/move_fwd_entries/breakdown_by_category?category_id=<?=$v->category_id?>&end_date=<?=$date_range['end_date']?>&start_date=<?=$date_range['start_date']?>"><?=$v->entry_count_per_category?></a></td>
                                            <td><?=$percentage?></td>
                                            <td></td>
                                        </tr>
                              <?php } ?> 
                              <tr>
                                <td></td>
                                <td><strong>Total</strong></td>
                                <td><?=$pending?></td>
                                <td><?=$approved?></td>
                                <td><?=$rejected?></td>
                                <td><?=$total_counted_entries?></td>
                                <td><?=round($total_percentage,0)?>%</td>
                            </tr>
                            <?php  }else{ ?>
                            <tr><td colspan="7">No result found.</td></tr>

                           <?php } ?>
          	           </tbody>
                    </table>
               </div>

               
               <div class="tab-pane active" id="graph">
                    <div id="google_graph_1" style="border:1px solid #DDDDDD; border-top:none;height:300px;"> </div>
                    <br >
                    <div id="google_graph_2" style="border:1px solid #DDDDDD; border-top:none;height:300px;"> </div>
                </div>
           </div>
           
     </div>
     <!--end main content -->


     <div id="view-photo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content modal-content-photo">
              <div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>
            </div>
          </div> 
      </div>

 <script type="text/javascript" src="<?php echo BASE_URL?>/admin_assets/js/angular-js/angular.js"></script> 
<script type="text/javascript" src="<?php echo BASE_URL ?>admin_assets/js/google-api.js"></script>
 <script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  
  function drawChart() {
      var data = google.visualization.arrayToDataTable([
      ['Activity', 'Entries'],
      
       <?php foreach($activities->result() as $k => $v): ?>
      ['<?php echo ucwords(strtolower($v->activity_name)); ?>', <?php echo $v->entry_count_per_activity ?>],
      <?php endforeach; ?>
    ]);
      
      var options = {
      title: 'Activity\'s Entries ',
        is3D: true,
         sliceVisibilityThreshold: 0       
      };

    var chart = new google.visualization.PieChart(document.getElementById('google_graph_1'));
    chart.draw(data, options); 
  }

  function drawChart2() {
      var data = google.visualization.arrayToDataTable([
      ['Activity', 'Entries'],
      
       <?php foreach($categories->result() as $k => $v): ?>
      ['<?php echo ucwords(strtolower($v->category_name)) ?>', <?php echo $v->entry_count_per_category?>],
      <?php endforeach; ?>
    ]);
      
      var options = {
      title: 'Category\'s Entries',
        is3D: true,
         sliceVisibilityThreshold: 0       
      };

    var chart = new google.visualization.PieChart(document.getElementById('google_graph_2'));
    chart.draw(data, options);

    <?php if(count($this->input->get()) < 2){ ?>
          $('#graph').trigger('click');
    <?php }else{ ?>
          $('#<?=$this->input->get("current_tab")?>-btn').trigger('click');
    <?php } ?>
  }

  google.setOnLoadCallback(drawChart);
  google.setOnLoadCallback(drawChart2);
  $(window).resize(drawChart);
  $(window).resize(drawChart2);

   $('.view-photo').on('click',function(e){
          var href = $(this).attr('href');
          $('.modal-content-photo').html('<div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>');
          $('#view-photo').modal('show');
          $('.modal-content-photo').load(href);
          e.preventDefault();
    });


   var app = angular.module('entryReport',[]);

      app.controller('ageController',['$scope',function($scope){
                
        $scope.setExportButton = function(href){
            $scope.exportTest = href;
        };

      }]);


 </script> 
  
