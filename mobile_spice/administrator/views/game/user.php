<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>User Games <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/user_games/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>   
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>Name</th>
                         <th>Game</th>
                         <th>Date</th>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/user_games">
                     <tr>
                         <td><input name="name" class="form-control" value="<?php echo isset($_GET['name']) ? $_GET['name'] : '' ?>" /></td>
                         <td>
                          <select class="form-control" name="uri">
                              <option></option>
                              <?php if($game_names): foreach ($game_names as $key => $value) {
                                $selected = $this->input->get('uri') && $this->input->get('uri') == strtolower($value['name']) ? 'selected' : '';
                                echo '<option ' . $selected . ' value="' . strtolower($value['name']) . '">' . $value['name'] . '</option>';
                              } endif; ?>
                              
                          </select>
                         </td>
                          <td>
                              From:<input name="fromdate" class="form-control from" value="<?php echo isset($_GET['fromdate']) ? $_GET['fromdate'] : '' ?>" /><br> 
                              To:<input name="todate" class="form-control to" value="<?php echo isset($_GET['todate']) ? $_GET['todate'] : '' ?>" /><br>
                              Field: 
                              <select name="field" class="form-control">
                                  <option></option>
                                  <option value="first_name" <?php echo $this->input->get('field') && $this->input->get('field') == 'first_name' ? 'selected' :'' ?>>Name</option>
                                  <option value="uri" <?php echo $this->input->get('field') && $this->input->get('field') == 'uri' ? 'selected' :'' ?>>Game</option>
                              </select>
                            Sort: 
                                <select name="sort" class="form-control">
                                  <option></option>
                                  <option value="asc" <?php echo $this->input->get('sort') && $this->input->get('sort') == 'asc' ? 'selected' :'' ?>>Ascending</option>
                                  <option value="desc" <?php echo $this->input->get('sort') && $this->input->get('sort') == 'desc' ? 'selected' :'' ?>>Descending</option>
                              </select><br><input name="type" type="hidden" value="<?php echo $this->input->get('type') ? $this->input->get('type') : 'age' ?>" /><br><button class="btn btn-primary" name="search" value="1">Go</button>
                            </td>
                    </tr>
                    </form>
                    <?php if($games): 
					foreach($games as $k => $v): $game = str_replace('games/', '', $v['section']); ?>
                    <tr>
                         <td><a href="#" onclick="showRegistrantDetails(<?php echo $v['registrant_id'] ?>)"><?php echo $v['first_name'] . ' ' . $v['third_name'] ?></a></td>
                         <td><?php echo strtoupper($game) ?></td>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['date_visited'])) ?></td>
                    </tr>
                    <?php 
					endforeach;
          else:
            echo '<tr><td colspan="7">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->