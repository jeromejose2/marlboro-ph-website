<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Demographics</h3>
		  </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Age</th>
                         <th>Count</th>
                    </tr>
               </thead>
               <tbody>
               		<?php if($records): 
					foreach($records as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['yob'] ?></td>
                         <td><?php echo $v['count'] ?></td>
                    </tr>
                    <?php 
					endforeach;
					else:
						echo '<tr><td colspan="7">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->
     
   <script>
 	function showLogDetails(id) {
		bootbox.dialog({'message': $('#log-details-' + id).html(),
						'title': 'Admin Activity Log Details'});
	}
 </script>
