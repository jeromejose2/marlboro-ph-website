<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Modules</h3>

               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/module/add" class="btn btn-primary">Add Module</a>
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Module Name</th>
                         <th>URI</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
                    <?php if($modules): 
					foreach($modules as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['module_name'] ?></td>
                         <td><?php echo $v['uri'] ?></td>
                         <td>
                         	
                            <a href="<?php echo SITE_URL ?>/module/edit/<?php echo $v['module_id'] ?>" class="btn btn-primary">Edit</a>
                            <a href="<?php echo SITE_URL ?>/module/delete/<?php echo $v['module_id'] ?>/<?php echo md5($v['module_id'] . ' ' . $this->config->item('encryption_key')) ?>" onclick="return confirmDeletion(this, 'module')" class="btn btn-danger">Delete</a>
                         </td>
                    </tr>
                    <?php 
					endforeach;
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->