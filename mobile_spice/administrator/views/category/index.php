<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Categories <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL ?>/category/add" class="btn btn-primary">Add Category</a>
                  <?php endif; ?>
                    <a href="<?php echo SITE_URL ?>/export/category?<?php echo @http_build_query($this->input->get()) ?>" class="btn btn-primary">Export</a>
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Thumbnail</th>
                         <th>Origin</th>
                         <th>Parent</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/category">
                     <tr>
                         <td></td>
                         <td><input name="name" class="form-control" value="<?php echo isset($_GET['name']) ? $_GET['name'] : '' ?>" /></td>
                         <td></td>
                         <td>
                          <select class="form-control" name="origin" id="origin-id">
                              <option></option>
                                <?php 
                                  foreach($origins as $k => $v) {
                                    $selected =  isset($_GET['origin']) && $_GET['origin'] != '' && $_GET['origin'] == $k ? 'selected' : '';
                                    echo '<option value="' . $k . '"' . $selected . '>' . $v . '</option>'; 
                                  }
                                ?>
                              
                            </select>
                         </td>
                         <td>
                         	<select class="form-control" name="parent" id="parent-id">
                            	<option value=""></option>
                                <option value="0">N/A</option>
                                <?php 
                									foreach($parent_categories as $k => $v) {
                										$selected =  isset($_GET['parent']) && $_GET['parent'] != '' && $_GET['parent'] == $v['category_id'] ? 'selected' : '';
                										echo '<option class="' . $v['origin_id'] . '" value="' . $v['category_id'] . '"' . $selected . '>' . $v['category_name'] . '</option>';	
                									}
                								?>
                            	
                            </select>
                         </td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
                    <?php if($categories): 
					foreach($categories as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['category_name'] ?></td>
                         <td><img src="<?php echo $v['category_image'] ? BASE_URL . 'uploads/category/resized_' . $v['category_image'] : BASE_URL . 'images/no_image.jpg' ?>?uniqid=<?php echo uniqid() ?>" width="150" /></td>
                         <td><?php echo !isset($origins[$v['origin_id']]) || $v['origin_id'] == 0 ? 'N/A' : $origins[$v['origin_id']] ?></td>
                         <td><?php echo !isset($main_categories[$v['parent']]) || $v['parent'] == 0 ? 'N/A' : $main_categories[$v['parent']]['category_name'] ?></td>
                         <td>
                            <?php if($edit): ?>
                            <a href="<?php echo SITE_URL ?>/category/edit/<?php echo $v['category_id'] ?>" class="btn btn-primary">Edit</a>
                            <?php endif; ?>
                            <?php if($delete): ?>
                            <a href="<?php echo SITE_URL ?>/category/delete/<?php echo $v['category_id'] ?>/<?php echo md5($v['category_id'] . ' ' . $this->config->item('encryption_key')) ?>" onclick="return confirmDeletion(this, 'category')" class="btn btn-danger">Delete</a>
                            <?php endif; ?>
                         </td>
                    </tr>
                    <?php 
					endforeach;
          else:
            echo '<tr><td colspan="7">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->

     <script>
      $(function() {
        $('#origin-id').change(function() {
          changeParentCategory($(this).val());
        });
      })

      function changeParentCategory(originId) {
        parentSelect = $('#parent-id'); 
        parentSelect.find('option').hide();
        parentSelect.find('option[value=""]').show();
        parentSelect.find('option[value=""]').prop('selected', true);
        if(originId == '') {
          parentSelect.find('option').show();
          return;
        }
        parentSelect.find('option.' + originId).show();
      }
      <?php if(isset($_GET['origin']) && $_GET['origin'] != ''): ?>changeParentCategory(<?php echo $_GET['origin'] ?>)<?php endif; ?>
     </script>