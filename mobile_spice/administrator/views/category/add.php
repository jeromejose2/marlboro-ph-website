<?php $this->load->view('editor.php') ?>
<script src="<?php echo BASE_URL ?>admin_assets/js/jquery.Jcrop.js"></script>
<script type="text/javascript">
var jscrop_api;
function initJCrop(elem) {
	<?php if(isset($category['thumbnail_meta'])):  
		$points = isset($category['thumbnail_meta']) ? unserialize($category['thumbnail_meta']) : null; ?>
		x1 = <?php echo $points['x1'] != '' ? $points['x1'] : 0 ?>;
		x2 = <?php echo $points['x2'] != '' ? $points['x2'] : 0 ?>;
		y1 = <?php echo $points['y1'] != '' ? $points['y1'] : 0 ?>;
		y2 = <?php echo $points['y2'] != '' ? $points['y2'] : 0 ?>;
	<?php else: ?>  
		x1 = 0;
		y1 = 0;
		x2 = 200;
		y2 = 200;			
	<?php endif; ?>;
	
	setTimeout(function() {
		if(jscrop_api)
			jcrop_api.destroy();
		$('#preview').Jcrop({
		  onChange:   showCoords,
		  onSelect:   showCoords,
		  setSelect: [x1, y1, x2, y2],
		  maxSize: [ 300, 300 ]
		},function(){
		  jcrop_api = this;
		});	
		$('#width').val($('#preview').width());
		$('#height').val($('#preview').height());
	}, 500);
}

function showCoords(c) {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
};

$(function() {
	<?php if(isset($category['category_image']) && $category['category_image'] != ''): ?>
	initJCrop($('#photo'));
	<?php endif; ?>
});

</script>
<link rel="stylesheet" href="<?php echo BASE_URL ?>admin_assets/css/jquery.Jcrop.css" type="text/css" />

<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3>Category</h3>
      </div>
       <div class="alert alert-danger" <?php if(!$error) echo 'style="display:none;"' ?>><?php echo $error; ?></div>
       <form class="form-horizontal" role="form" method="post" onsubmit="return submitFrm()" enctype="multipart/form-data">
           <div class="form-group">
                <label class="col-sm-2 control-label">Category Name</label>
                <div class="col-sm-4">
                     <input type="text" name="category_name" value="<?php echo isset($category['category_name']) ? $category['category_name'] : '' ?>" data-name="category name" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Parent</label>
                <div class="col-sm-4">
                    <select class="form-control" name="parent">
                    <option value="0"></option>	
					<?php if($main_categories): ?>
                    	<?php foreach($main_categories as $k => $v): ?>
                        <option value="<?php echo $v['category_id'] ?>" <?php echo isset($category['parent']) && $category['parent'] == $v['category_id'] ? 'selected="selected"' . $category['parent'] : '' ?>><?php echo $v['category_name']; ?></option>
                        <?php endforeach;  ?>
                    <?php endif; ?>
                    </select>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Origin</label>
                <div class="col-sm-4">
                    <select class="form-control" name="origin_id">
                    	<?php foreach($origins as $k => $origin): ?>
                        <option value="<?php echo $k ?>" <?php echo isset($category['origin_id']) && $category['origin_id'] == $k ? 'selected="selected"' : '' ?>><?php echo $origin; ?></option>
                        <?php endforeach;  ?>
                    </select>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Category Image</label>
                <div class="col-sm-4">
                     <input type="file" id="photo" name="photo" value="" data-name="module name" class="form-control" onchange="previewImage('photo', 'preview', initJCrop, this);">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Image Preview</label>
                <div class="col-sm-4">
                   <img id="preview" class="preview" <?php echo isset($category['category_image']) ? "style='max-width:500px;' src='" . BASE_URL . 'uploads/category/' . $category['category_image'] . "'" : '' ?>  />
                </div>
           </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Category Description</label>
                <div class="col-sm-8">
                     <textarea name="description"><?php echo isset($category['description']) ? $category['description'] : '' ?></textarea>
                </div>
           </div>
           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <input type="hidden" name="submit" value="1" />
                     <input type="hidden" id="x" name="x" />
                     <input type="hidden" id="y" name="y" />
                     <input type="hidden" id="x2" name="x2" />
                     <input type="hidden" id="y2" name="y2" />
                     <input type="hidden" id="width" name="width" />
                     <input type="hidden" id="height" name="height"/>
                     <input type="hidden" name="id" value="<?php echo isset($category['category_id']) ? $category['category_id'] : '' ?>" />
                     <input type="hidden" name="filename" value="<?php echo isset($category['category_image']) ? $category['category_image'] : '' ?>" />
                     <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                </div>
           </div>
      </form>
      
 </div>
 <!--end main content -->
 