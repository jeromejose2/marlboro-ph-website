<noscript>
  <style>
  .col-xs-4 {
    display: none;
  }
  </style>
</noscript>
<script src="<?php echo BASE_URL ?>admin_assets/js/bootstrap-tagsinput.js"></script>
<?php $this->load->view('editor.php') ?>
<script src="<?php echo BASE_URL ?>admin_assets/js/jquery.Jcrop.js"></script>
<script src="<?= BASE_URL ?>admin_assets/js/angular-js/angular-file-upload-shim.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.js"></script>    
<script src="<?= BASE_URL ?>admin_assets/js/angular-js/angular-file-upload.js"></script>
<script type="text/javascript">
var uploadURL = '<?= SITE_URL ?>/bid_items/upload_media/';
</script>
<script src="<?= BASE_URL ?>admin_assets/js/angular-js/buy-items.js"></script> 
<link rel="stylesheet" href="<?php echo BASE_URL ?>admin_assets/css/timepicker.css">
<script src="<?php echo BASE_URL ?>admin_assets/js/timepicker.js" type="text/javascript"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjxtjNyjATUWGLx-1v0zehvr0bkc0ACnE&sensor=false"></script>
<script type="text/javascript">
var jscrop_api;
function initJCrop(elem) {
  <?php if(isset($record['thumbnail_meta'])):  
    $points = isset($record['thumbnail_meta']) ? unserialize($record['thumbnail_meta']) : null; ?>
    x1 = <?php echo $points['x1'] != '' ? $points['x1'] : 0 ?>;
    x2 = <?php echo $points['x2'] != '' ? $points['x2'] : 0 ?>;
    y1 = <?php echo $points['y1'] != '' ? $points['y1'] : 0 ?>;
    y2 = <?php echo $points['y2'] != '' ? $points['y2'] : 0 ?>;
  <?php else: ?>  
    x1 = 0;
    y1 = 0;
    x2 = 200;
    y2 = 200;     
  <?php endif; ?>;
  
  setTimeout(function() {
    if(jscrop_api)
      jcrop_api.destroy();
    $('#preview').Jcrop({
      onChange:   showCoords,
      onSelect:   showCoords,
      setSelect: [x1, y1, x2, y2],
      maxSize: [ 300, 300 ]
    },function(){
      jcrop_api = this;
    }); 
    $('#width').val($('#preview').width());
    $('#height').val($('#preview').height());
  }, 500);
}

function showCoords(c) {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
};

$(function() {
  <?php if(isset($record['move_forward_image']) && $record['move_forward_image'] != ''): ?>
  initJCrop($('#photo'));
  <?php endif; ?>

  $('#type').change(function() {
    if($(this).val() == 'Redemption') {
      $('#r-address-container').show();
    } else {
      $('#r-address-container').hide();
    }
  });

  $(".datetime").datetimepicker({dateFormat:'yy-mm-dd'});

});

</script>
<link rel="stylesheet" href="<?php echo BASE_URL ?>admin_assets/css/jquery.Jcrop.css" type="text/css" />
<link rel="stylesheet" href="<?php echo BASE_URL ?>admin_assets/css/bootstrap-tagsinput.css" type="text/css" />
<!--start main content -->
 <div class="container main-content" ng-app="fileUpload" ng-controller="MyCtrl">
      <div class="page-header">
           <h3>Bid Item</h3>
      </div>
       <div class="alert alert-danger" <?php if(!$error) echo 'style="display:none;"' ?>><?php echo $error; ?></div>
       <form class="form-horizontal" role="form" method="post" id="add-form-product-info" action="<?= $action ?>" onsubmit="return submitFrm()" enctype="multipart/form-data">
           <div class="form-group">
                <label class="col-sm-2 control-label">Item Name</label>
                <div class="col-sm-4">
                     <input type="text" name="bid_item_name" value="<?php echo isset($record['bid_item_name']) ? $record['bid_item_name'] : '' ?>" data-name="item name" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Image</label>
                <div class="col-sm-4">
                     <input type="file" id="photo" name="photo" value="" data-name="module name" class="form-control" onchange="previewImage('photo', 'preview', initJCrop, this);"><br>
                     <strong>Width: 400px. Height: 400px.</strong>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Image Preview</label>
                <div class="col-sm-4">
                   <img id="preview" class="preview" <?php echo isset($record['bid_item_image']) ? "style='max-width:500px;' src='" . BASE_URL . 'uploads/bid/' . $record['bid_item_image'] . "'" : '' ?>  />
                </div>
           </div>
          <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-8">
                     <textarea name="description"><?php echo isset($record['description']) ? $record['description'] : '' ?></textarea>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Details</label>
                <div class="col-sm-8">
                     <textarea name="details"><?php echo isset($record['details']) ? $record['details'] : '' ?></textarea>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Status</label>
                <div class="col-sm-4">
                    <select class="form-control" name="status">
                      <option <?php echo isset($record['status']) && $record['status'] == 1 ? 'selected' : '' ?> value="1">Active</option>
                      <option <?php echo isset($record['status']) && $record['status'] == 0 ? 'selected' : '' ?> value="0">Inactive</option>   
                    </select>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Slots</label>
                <div class="col-sm-4">
                     <input type="text" onkeydown="return checkDigit(event)" name="slots" value="<?php echo isset($record['slots']) ? $record['slots'] : '' ?>" data-name="slots" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Starting Bid</label>
                <div class="col-sm-4">
                     <input type="text" onkeydown="return checkDigit(event)" name="starting_bid" value="<?php echo isset($record['starting_bid']) ? $record['starting_bid'] : '' ?>" data-name="starting bid" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Start</label>
                <div class="col-sm-4">
                     <input type="text" name="start_date" value="<?php echo isset($record['start_date']) ? $record['start_date'] : '' ?>" data-name="start" class="datetime required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">End</label>
                <div class="col-sm-4">
                     <input type="text" name="end_date" value="<?php echo isset($record['end_date']) ? $record['end_date'] : '' ?>" data-name="end" class="datetime required form-control">
                </div>
           </div>

           <div class="form-group" id="with-btn-add-media">
                <label class="col-sm-2 control-label">Media</label>
                <div class="col-sm-4">
                    <input type="file" name="multiple_images[]" class="form-control" ng-file-select="onFileSelect($files)" id="tmp-file-upload" multiple="true">
                </div>
           </div>

           <div class="form-group" ng-show="selectedFiles != null">
              <label class="col-sm-2 control-label">Media Preview</label>
              <div class="col-md-8">
                <div class="row">

                <?php if(isset($media)) {
                  foreach($media as $k => $v) { ?>
                  <div class="col-xs-4 col-md-3 product-media-item-container">
                  <div class="thumbnail">
                      <img src="<?= BASE_URL ?>uploads/bid/media/150_150_<?= $v['media_content'] ?>">
                      <div class="caption">
                        <input value="" type="hidden" > 
                        <p>
                          <a onclick="removeValue(this)" href="javascript:void(0)" class="btn btn-danger" role="button">&times;&nbsp;&nbsp;Remove</a>
                          <input type="hidden" name="media_ids[]" value="<?= $v['media_id'] ?>"> 
                        </p>
                      </div>
                    </div>
                  </div>
                  <?php }
                } ?>  
                <div class="col-xs-4 col-md-3" ng-repeat="f in selectedFiles">
                  <div class="thumbnail">
                    <img ng-show="dataUrls[$index]" ng-src="{{dataUrls[$index]}}">
                    <div class="caption">
                      <h5>{{f.name}}</h5>
                      <input name="media_title[{{$index}}]" type="text" style="width: 100%;" placeholder="Enter title here">
                      <input name="media_upload[{{$index}}]" type="hidden" ng-show="uploadResult[$index]" value="{{uploadResult[$index]}}">
                      <br><br>
                      <p>size: {{f.size}}B - type: {{f.type}}</p>
                      <div class="progress progress-striped" ng-show="progress[$index] >= 0">
                      <div class="progress-bar"  role="progressbar" aria-valuemin="0" aria-valuenow="{{progress[$index]}}" aria-valuemax="100" style="width: {{progress[$index]}}%">
                        <span class="sr-only">{{progress[$index]}}% Complete</span>
                      </div>
                    </div>
                      <p><a href="javascript:void(0)" class="btn btn-primary" role="button" ng-click="start($index)" ng-show="progress[$index] < 0">Start</a> 
                      <a href="javascript:void(0)" class="btn btn-warning" ng-click="abort($index)" ng-show="hasUploader($index) && progress[$index] < 100" role="button">Abort</a> 
                    <a href="javascript:void(0)" class="btn btn-danger" ng-click="remove($index)" ng-show="progress[$index] >= 100" role="button">&times;&nbsp;&nbsp;Remove</a></p>
                    </div>
                  </div>
                </div>
              </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Map Location</label>
              <div class="col-sm-4">
                    <div id="google_map_canvas" style="width: 480px; height: 480px"></div><br>
                   <input type="text" id="longitude" name="lng" value="<?php echo isset($record['lng']) ? $record['lng'] : '' ?>" data-name="longitude" class="form-control" placeholder="Longitude"><br>
                   <input type="text" id="latitude" name="lat" value="<?php echo isset($record['lat']) ? $record['lat'] : '' ?>" data-name="latitude" class="form-control" placeholder="Latitude">
              </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Platform Restriction</label>
                <div class="col-sm-4">
                    <select data-name="platform restriction" class="required form-control" name="platform_restriction">
                      <option <?php echo isset($record['platform_restriction']) && $record['platform_restriction'] == WEB_ONLY ? 'selected' : '' ?> value="<?= WEB_ONLY ?>">Website</option>
                      <option <?php echo isset($record['platform_restriction']) && $record['platform_restriction'] == MOBILE_ONLY ? 'selected' : '' ?> value="<?= MOBILE_ONLY ?>">Mobile App</option>   
                      <option <?php echo isset($record['platform_restriction']) && $record['platform_restriction'] == BOTH_PLATFORM ? 'selected' : '' ?> value="<?= BOTH_PLATFORM ?>">Mobile App & Website</option>   
                    </select>
                </div>
           </div>
           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <input type="hidden" name="submit" value="1" />
                     <input type="hidden" id="x" name="x" />
                     <input type="hidden" id="y" name="y" />
                     <input type="hidden" id="x2" name="x2" />
                     <input type="hidden" id="y2" name="y2" />
                     <input type="hidden" id="width" name="width" />
                     <input type="hidden" id="height" name="height"/>
                     <input type="hidden" name="bid_item_image" value="<?php echo isset($record['bid_item_image']) ? $record['bid_item_image'] : '' ?>" />
                     <input type="hidden" ng-model="httpMethod" ng-init="httpMethod = 'POST'" value="POST"/>
                     <input type="hidden" name="howToSend" ng-model="howToSend" value="1" ng-init="howToSend = 1">
                     <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                     <a class="btn btn-warning" href="<?php echo BASE_URL ?>admin/bid_items">Cancel</a>
                </div>
           </div>
      </form>
      
 </div>
 <script>

// $("#add-form-product-info").submit( function() {
//  bootbox.dialog({
//    message: "<center><strong>Loading</strong></center>",
//    show: true,
//    backdrop: true,
//    closeButton: false
//  });
// });

function removeValue(el) {
  elem = $(el);
  elem.parent().parent().parent().parent().remove();
}
 </script>

 <script type="text/javascript">
  $(function(){

        var LatitudeLongitudeCoordinates,
            DefaultLongitude = 122.72092948635861,
            DefaultLatitude = 11.646338452790836;
        
        <?php if( isset($record['lat']) && $record['lat'] ): ?>
          LatitudeLongitudeCoordinates = new google.maps.LatLng(<?= $record['lat'] ?>, <?= $record['lng'] ?>);
        <?php else: ?>
          LatitudeLongitudeCoordinates = new google.maps.LatLng(DefaultLatitude, DefaultLongitude);
        <?php endif; ?>

        var mapOptions = {
            zoom: <?php echo isset($record['lat']) && $record['lat'] ? 18 : 6 ?>,
            center: LatitudeLongitudeCoordinates,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById('google_map_canvas'), mapOptions);
        var infowindow = new google.maps.InfoWindow();
        var bounds = new google.maps.LatLngBounds();

        var marker = new google.maps.Marker({
              draggable: true, position: LatitudeLongitudeCoordinates, map: map
          });

        google.maps.event.addListener(marker, 'drag', function(event){
           document.getElementById("latitude").value = event.latLng.lat();
           document.getElementById("longitude").value = event.latLng.lng();
    });

        var listener = google.maps.event.addListener(map, "idle", function () {
            map.setZoom(<?php echo isset($record['lat']) && $record['lat'] ? 18 : 6 ?>);
            google.maps.event.removeListener(listener);
        });
    });
</script>
 <!--end main content -->
 