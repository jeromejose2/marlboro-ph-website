<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>User Buys <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/user_buys/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Item</th>
                         <th>Properties</th>
                         <th>Date</th>
                         <?php if($edit || $delete) : ?>
                         <th>Operation</th>
                         <?php endif; ?>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/user_buys">
                         <tr>
                             <td></td>
                             <td><input name="name" class="form-control" value="<?php echo isset($_GET['name']) ? $_GET['name'] : '' ?>" /></td>
                             <td>
                                <select name="buy_item_id" class="form-control">
                                    <option></option>
                                    <?php if($items) {
                                      foreach($items as $k => $v) {
                                        $selected = isset($_GET['buy_item_id']) && $_GET['buy_item_id'] == $v['buy_item_id'] ? 'selected' : '';
                                        echo '<option ' . $selected . ' value="' . $v['buy_item_id'] . '">' . $v['buy_item_name'] . '</option>';
                                      }
                                    } ?>
                                </select>
                             </td>
                             <td><input name="specs" class="form-control" value="<?php echo isset($_GET['specs']) ? $_GET['specs'] : '' ?>" /></td>
                             <td>
                             
                              <div class="row">
                                <div class="col-md-4">
                                     <input name="from_buy" placeholder="From" class="form-control from" value="<?php echo isset($_GET['from_buy']) ? $_GET['from_buy'] : '' ?>" />
                                </div>
                                <div class="col-md-4">
                                     <input name="to_buy" placeholder="To" class="to form-control" value="<?php echo isset($_GET['to_buy']) ? $_GET['to_buy'] : '' ?>" />
                                </div>
                              </div>
                            </td>
                             <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                        </tr>
                        </form>
                        
                    <?php if($users): 
          					foreach($users as $k => $v): 
                     
                     ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><a href="#" onclick="showRegistrantDetails(<?php echo $v['registrant_id'] ?>)"><?php echo $v['first_name'] . ' ' . $v['third_name'] ?></a></td>
                         <td><?php echo $v['buy_item_name'] ?></td>
                         <td><?php echo $v['specs'] ?></td>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['buy_date']))  ?></td>
                         <td></td>
                     </tr>
                    <?php 
					endforeach;
					else:
						echo '<tr><td colspan="5">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->