<script src="<?= BASE_URL ?>admin_assets/js/underscore-min.js"></script>
<link type="text/css" rel="stylesheet" href="<?= BASE_URL ?>admin_assets/css/fullcalendar.css">
<script src="<?= BASE_URL ?>admin_assets/js/fullcalendar.js"></script>
<script>
var lampId = '<?php echo $this->uri->segment(2, 1) ?>';
    $(function() {
        $('#calendar').fullCalendar({
            dayClick: function(date, allDay, jsEvent, view) {
                markDate(date, jsEvent);    
            },
            events: siteUrl + '/lamp_calendar/get_events/' + lampId
        });
    });
</script>


<!--start main content -->
<div class="container main-content">
  <div class="page-header">
     <h3>Schedules</h3>
  </div>

  <div id="calendar">
  </div>

  <div class="modal" id="modal-event">
    <div>
    <div class="alert alert-danger" style="display:none;"></div>
       <div class="form-group">
            <label class="col-sm-3 control-label">Start Date</label>
            <div class="col-sm-6">
               <p class="form-control-static">
               <input value="" class="form-control from" name="" data-name="">
                </p>
            </div>
       </div>
   </div>
  </div>

</div>
<script>
    $(function() {
        $(document).on('click', '#repeat-check', function() {
            checked = $(this).is(':checked');
            if(checked) {
                $('.repeat').show();
            } else  {
                $('.repeat').hide();
            }
        });
    })
    function BootboxContent(d, target, hasEvent, slots, cost){   
            var frm_str = '<form class="form-horizontal" role="form" method="post" onsubmit="return submitFrm()" id="sched-form">'
                           + '<div class="alert alert-danger" style="display:none;"></div>'
                           + '<div class="form-group">'
                              + '<label for="date" class="col-sm-3 control-label">Start Date</label>'   
                              + '<div class="col-sm-8">' 
                              + '<p class="form-control-static"><input id="date" name="start" class="date span2 form-control input-sm" size="16" type="text"></p>'
                              + '</div>'
                              + '</div>'
                              + '<div class="form-group">'
                              + '<label for="date" class="col-sm-3 control-label">Slots</label>'   
                              + '<div class="col-sm-8">' 
                              + '<p class="form-control-static"><input onkeydown="return checkDigit(event)" id="slots" name="slots" class="span2 form-control input-sm" size="16" type="text"></p>'
                              + '</div>'
                              + '</div>'
                              + '<div class="form-group">'
                              + '<label for="date" class="col-sm-3 control-label">Note</label>'   
                              + '<div class="col-sm-8">' 
                              + '<p class="form-control-static"><textarea id="note" name="note" class="form-control"></textarea></p>'
                              + '</div>'
                              + '</div>'
                              + '<div class="form-group">'
                              + '<label for="date" class="col-sm-3 control-label">Reservation Cost</label>'   
                              + '<div class="col-sm-8">' 
                              + '<p class="form-control-static"><input onkeydown="return checkDigit(event)" id="reservation_cost" name="reservation_cost" class="span2 form-control input-sm" size="16" type="text"></p>'
                              + '</div>'
                              + '</div>'
                              + '<div class="form-group">'
                              + '<label for="date" class="col-sm-3 control-label">Repeat</label>'   
                              + '<div class="col-sm-8">' 
                              + '<p class="form-control-static"><input type="checkbox" name="repeat" value="1" id="repeat-check"></p>'
                              + '</div>'
                              + '</div>'
                             + '<div class="form-group repeat">'
                              + '<label for="date" class="col-sm-3 control-label">&nbsp</label>'   
                              + '<div class="col-sm-8">' 
                              + '<p class="form-control-static"><input type="checkbox" name="day[]" value="1"> Sun <input type="checkbox" name="day[]" value="2"> Mon <input type="checkbox" name="day[]" value="3"> Tue <input type="checkbox" name="day[]" value="4"> Wed <input type="checkbox" name="day[]" value="5"> Thu <input type="checkbox" name="day[]" value="6"> Fri <input type="checkbox" name="day[]" value="7">  Sat</p>'
                              + '</div>'
                              + '</div>'
                              + '<div class="form-group repeat">'
                              + '<label for="date" class="col-sm-3 control-label">Until</label>'   
                              + '<div class="col-sm-8">' 
                              + '<p class="form-control-static"><input id="date2" name="end" class="date span2 form-control input-sm" size="16" type="text"></p>'
                              + '</div>'
                              + '</div>'
                              + '<div class="form-group control-label">'
                              + '<div class="col-sm-offset-2 col-sm-10">' 
                              + '<p class="form-control-static"><button name="search" class="btn close-bootbox" type="button">Cancel</button><button type="button" onclick="submitSched()" class="btn btn-primary">Add</button><button type="button" onclick="removeSched()" class="btn btn-danger">Remove</button></p>'
                              + '</div>'
                              + '</div>'
                           + '</form>';
                           
            var object = $('<div/>').html(frm_str).contents();
            object.find('#date').val(d);
            object.find('.repeat').hide();
            object.find('.date').datepicker({
                dateFormat:'yy-mm-dd',
                autoclose: true}).on('changeDate', function (ev) {
                   $(this).blur();
                   $(this).datepicker('hide');
            });
            if(hasEvent !== false) {
              object.find('.btn-danger').show();
              object.find('.btn-primary').hide();
              object.find('#note').val(hasEvent);
              object.find('#slots').val(slots);
              object.find('#reservation_cost').val(cost);
            } else {
              object.find('.btn-danger').hide();
              object.find('.btn-primary').show();
            }

             return object
        }
    function markDate(d,target) {
        date = d;
        var year = date.getFullYear();
        var month = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
        var day = date.getDate() < 10 ? '0' +  date.getDate() :  date.getDate();
        bootbox.dialog({'message':'<center>Loading... Please wait.</center>'});
        
        $.post(siteUrl + '/lamp_calendar/get_event', {date: year + "-" + month + "-" + day, id: lampId}, function(data) {
            bootbox.hideAll();
            if(data.length > 0) {
                bootbox.dialog({'message': BootboxContent(year + "-" + month + "-" + day, target, data[0].title, data[0].original_slots, data[0].reservation_cost),
                        'title': 'Schedule'});   
            } else {
                bootbox.dialog({'message': BootboxContent(year + "-" + month + "-" + day, target, false),
                        'title': 'Schedule'});
            }
        })

        
    }

    function submitSched() {
        $.post('<?php echo SITE_URL ?>/lamp_calendar/save_dates/<?php echo $this->uri->segment(2) ?>', $('#sched-form').serialize(), function() {
            bootbox.hideAll();
            $('#calendar').fullCalendar( 'refetchEvents' );
        })
        
    }
    function removeSched() {
        bootbox.hideAll();
        bootbox.dialog({'message':'<center>Loading... Please wait.</center>'});
        $.post('<?php echo SITE_URL ?>/lamp_calendar/remove_dates/<?php echo $this->uri->segment(2) ?>', $('#sched-form').serialize(), function(data) {
            bootbox.hideAll();
            if(data.length > 0) {
              dates = data.length > 1 ? 'The system failed to remove the following schedules because there are one or more reservations made for these dates: <ul>' : 'The system failed to remove the following schedule because there are one or more reservations made for this date: <ul>';
              for(i in data) {
                dates += '<li>' + data[i] + '</li>';
              }
              dates += '</ul>';
              bootbox.dialog({'message': dates, 'title': 'Delete Error'});
            }
            $('#calendar').fullCalendar( 'refetchEvents' );
        })
        
    }
</script>
<!--end main content -->
