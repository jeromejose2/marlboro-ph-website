<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Perks : LAMP Events <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/lamp_events/add" class="btn btn-primary">Add Lamp Event</a>
                    <a href="<?php echo SITE_URL ?>/lamp_events/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
               </div>
          </div>

                  <table class="table table-bordered">
                         <thead>
                              <tr>
                                   <th>#</th>
                                   <th>LAMP Name</th>
                                   <th>Event Name</th>
                                   <th>Date Created</th>
                                   <th>Operation</th>
                              </tr>
                         </thead>
                         <tbody>
                             <form action="<?php echo SITE_URL ?>/lamp_events">
                                   <tr>
                                       <td></td>
                                       <td>
                                          <select class="required form-control" name="lamp_id" data-name="lamp name">
                                            <option></option>
                                            <?php foreach($lamps as $k => $v): ?>
                                                <option value="<?= $v['lamp_id'] ?>" <?= isset($_GET['lamp_id']) && $_GET['lamp_id'] == $v['lamp_id'] ? 'selected' : '' ?>><?= $v['lamp_name'] ?></option>
                                            <?php endforeach; ?>
                                          </select>
                                       </td>
                                       <td><input name="event_title" class="form-control" value="<?php echo isset($_GET['event_title']) ? $_GET['event_title'] : '' ?>" /></td>
                                       <td></td>
                                      <td>
                                        <div class="row">
                                          <button class="btn btn-primary" name="search" value="1">Go</button>
                                        </div>
                                      </td>
                                  </tr>
                                  </form>
                                  
                              <?php if($users): 
                              foreach($users as $k => $v): 
                               
                               ?>
                              <tr>
                                   <td><?php echo $offset + $k + 1 ?></td>
                                   <td><?php echo $v['lamp_name']?></td>
                                   <td><?php echo $v['event_title']?></td>
                                   <td><?php echo date('F d, Y H:i:s', strtotime($v['date_created'])) ?></td>
                                   <td>
                                        <a href="<?php echo SITE_URL ?>/lamp_events/edit/<?php echo $v['lamp_event_id'] ?>" class="btn btn-primary">Edit</a>
                                        <a href="<?php echo SITE_URL ?>/lamp_events/delete/<?php echo $v['lamp_event_id'] .'/'. md5($v['lamp_event_id'] . ' ' . $this->config->item('encryption_key')) ?>" onclick="return confirmDeletion(this, 'promotion')" class="btn btn-danger">Delete</a>
                                   </td>
                               </tr>
                              <?php 
                    endforeach;
                    else:
                      echo '<tr><td colspan="5">No records found</td></tr>';
                    endif; ?>
                         </tbody>
                    </table>

                    <ul class="pagination pagination-sm pull-right">
                         <?php echo $pagination ?>
                    </ul>
          </div>
     <!--end main content -->