<!--start main content -->
<!-- <link type="text/css" rel="stylesheet" href="<?= BASE_URL ?>admin_assets/css/angular-js/common.css"> -->
<script src="<?= BASE_URL ?>admin_assets/js/angular-js/angular-file-upload-shim.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.js"></script>    
<script src="<?= BASE_URL ?>admin_assets/js/angular-js/angular-file-upload.js"></script>
<script type="text/javascript">
var uploadURL = '<?= SITE_URL ?>/lamps/upload_media/';
</script>
<script src="<?= BASE_URL ?>admin_assets/js/angular-js/buy-items.js"></script> 
<link rel="stylesheet" href="<?php echo BASE_URL ?>admin_assets/css/timepicker.css">
<script src="<?php echo BASE_URL ?>admin_assets/js/timepicker.js" type="text/javascript"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjxtjNyjATUWGLx-1v0zehvr0bkc0ACnE&sensor=false"></script>
<script>
$(function() {
  $(".datetime").datetimepicker({
    dateFormat: '', 
    timeFormat: 'hh:mm tt', 
    timeOnly: true    });
})
</script>
<?php $this->load->view('editor.php') ?>
 <div class="container main-content" ng-app="fileUpload" ng-controller="MyCtrl">
      <div class="page-header">
           <h3>LAMP</h3>
       <div class="actions">
        <!-- <a href="<?= SITE_URL ?>/product_info" class="btn btn-primary">Back</a> -->
       </div>
      </div>
      <?php if (!$error): ?>
      <div class="alert alert-danger" style="display:none;"></div>
      <?php else: ?>
      <div class="alert alert-danger"><?= $error ?></div>
      <?php endif ?>
       <form class="form-horizontal" role="form" method="post" id="add-form-product-info" action="<?= $action ?>" onsubmit="return submitFrm()" enctype="multipart/form-data">
           <div class="form-group">
                <label class="col-md-1 control-label">Name</label>
                <div class="col-md-4">
                     <input type="text" id="title" name="lamp_name" value="<?php echo isset($record['lamp_name']) ? $record['lamp_name'] : '' ?>" data-name="LAMP name" class="required form-control">
                </div>
           </div>
           <div class="form-group">
                <label class="col-md-1 control-label">Description</label>
                <div class="col-md-4">
                     <textarea name="description" data-name="description" class="form-control"><?php echo isset($record['description']) ? $record['description'] : '' ?></textarea>
                </div>
           </div>
           <div class="form-group">
                <label class="col-md-1 control-label">Image</label>
                <div class="col-md-4">
                     <input type="file" id="product-info-photo" data-name="image" name="photo" value="" class="form-control" onchange=""><br>
                     <strong>Width: 400px. Height: 400px.</strong>
                </div>
           </div>
           <div class="form-group">
                <label class="col-md-1 control-label">Image Preview</label>
                <div class="col-md-4">
                   <img src="<?php echo isset($record['lamp_image']) ? BASE_URL . 'uploads/reserve/'. $record['lamp_image'] : BASE_URL.DEFAULT_IMAGE ?>" style="width: 193px;" id="product-info-preview" class="img-thumbnail preview"  />
                </div>
           </div>
           <div class="form-group">
                <label class="col-md-1 control-label">Status</label>
                <div class="col-md-4">
                    <select class="form-control" name="status">
                      <option value="0" <?php echo isset($record['status']) && $record['status'] == 0 ? 'selected' : '' ?>>Unpublished</option>
                      <option value="1" <?php echo isset($record['status']) && $record['status'] == 1 ? 'selected' : '' ?>>Published</option>   
                    </select>
                </div>
           </div>
           <div class="form-group">
                <label class="col-md-1 control-label">Cut-off Time</label>
                <div class="col-md-4">
                     <input type="text" name="cut_off" value="<?php echo isset($record['cut_off']) ? $record['cut_off'] : '' ?>" data-name="LAMP name" class="datetime required form-control">
                </div>
           </div>
           

       <div class="form-group" id="with-btn-add-media">
      <label class="col-md-1 control-label">Media</label>
      <div class="col-md-4">
        <!-- <button type="button" class="btn btn-warning" id="add-media-btn">Add Media</button>&nbsp;&nbsp; -->
        <!-- <button type="button" class="btn btn-warning" id="add-multiple-media-btn">Browse</button>
        <button type="button" class="btn btn-danger" id="clear-uploads">Clear</button> -->
        <input type="file" name="multiple_images[]" class="form-control" ng-file-select="onFileSelect($files)" id="tmp-file-upload" multiple="true">
      </div>
    </div>
      <div class="form-group" ng-show="selectedFiles != null">
        <label class="col-md-1 control-label">Media Preview</label>
        <div class="col-md-8">
          <div class="row">

          <?php if(isset($media)) {
            foreach($media as $k => $v) { ?>
            <div class="col-xs-4 col-md-3 product-media-item-container">
            <div class="thumbnail">
                <img src="<?= BASE_URL ?>uploads/reserve/media/150_150_<?= $v['media_content'] ?>">
                <div class="caption">
                  <input value="" type="hidden" > 
                  <p>
                    <a onclick="removeValue(this)" href="javascript:void(0)" class="btn btn-danger" role="button">&times;&nbsp;&nbsp;Remove</a>
                    <input type="hidden" name="media_ids[]" value="<?= $v['media_id'] ?>"> 
                  </p>
                </div>
              </div>
            </div>
            <?php }
          } ?>  
          <div class="col-xs-4 col-md-3" ng-repeat="f in selectedFiles">
            <div class="thumbnail">
              <img ng-show="dataUrls[$index]" ng-src="{{dataUrls[$index]}}">
              <div class="caption">
                <h5>{{f.name}}</h5>
                <input name="media_title[{{$index}}]" type="text" style="width: 100%;" placeholder="Enter title here">
                <input name="media_upload[{{$index}}]" type="hidden" ng-show="uploadResult[$index]" value="{{uploadResult[$index]}}">
                <br><br>
                <p>size: {{f.size}}B - type: {{f.type}}</p>
                <div class="progress progress-striped" ng-show="progress[$index] >= 0">
                <div class="progress-bar"  role="progressbar" aria-valuemin="0" aria-valuenow="{{progress[$index]}}" aria-valuemax="100" style="width: {{progress[$index]}}%">
                  <span class="sr-only">{{progress[$index]}}% Complete</span>
                </div>
              </div>
                <p><a href="javascript:void(0)" class="btn btn-primary" role="button" ng-click="start($index)" ng-show="progress[$index] < 0">Start</a> 
                <a href="javascript:void(0)" class="btn btn-warning" ng-click="abort($index)" ng-show="hasUploader($index) && progress[$index] < 100" role="button">Abort</a> 
              <a href="javascript:void(0)" class="btn btn-danger" ng-click="remove($index)" ng-show="progress[$index] >= 100" role="button">&times;&nbsp;&nbsp;Remove</a></p>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
      <div class="form-group">
          <label class="col-md-1 control-label">Map Location</label>
          <div class="col-md-4">
                <div id="google_map_canvas" style="width: 480px; height: 480px"></div><br>
                <input type="text" id="longitude" name="lng" value="<?php echo isset($record['lng']) ? $record['lng'] : '' ?>" data-name="longitude" class="required form-control" placeholder="Longitude"><br>
                <input type="text" id="latitude" name="lat" value="<?php echo isset($record['lat']) ? $record['lat'] : '' ?>" data-name="latitude" class="required form-control" placeholder="Latitude">
          </div>
     </div>
     <div class="form-group">
        <label class="col-md-1 control-label">Platform Restriction</label>
        <div class="col-md-4">
            <select class="form-control" name="platform_restriction">
              <option value="0"></option>
              <option <?php echo isset($record['platform_restriction']) && $record['platform_restriction'] == WEB_ONLY ? 'selected' : '' ?> value="<?= WEB_ONLY ?>">Website</option>
              <option <?php echo isset($record['platform_restriction']) && $record['platform_restriction'] == MOBILE_ONLY ? 'selected' : '' ?> value="<?= MOBILE_ONLY ?>">Mobile App</option>   
              <option <?php echo isset($record['platform_restriction']) && $record['platform_restriction'] == BOTH_PLATFORM ? 'selected' : '' ?> value="<?= BOTH_PLATFORM ?>">Mobile App & Website</option>   
            </select>
        </div>
     </div>
      <br ><br >
           <div class="form-group">
            <div class="col-md-offset-1 col-md-4">
             <button type="submit" class="btn btn-primary" name="submit" value="1">Submit</button>
             <a class="btn btn-warning" href="<?php echo BASE_URL ?>admin/lamps">Cancel</a>
           </div>
          </div>
          <input type="hidden" ng-model="httpMethod" ng-init="httpMethod = 'POST'" value="POST"/>
          <input type="hidden" name="howToSend" ng-model="howToSend" value="1" ng-init="howToSend = 1">
          <!-- <input type="hidden" id="remove-files-index" name="remove-file" value=""> -->
      </form>
        <!-- <div > -->
        <!-- <div class="sel-file img-thumbnail" >
          {{($index + 1) + '.'}}
          <img ng-show="dataUrls[$index]" ng-src="{{dataUrls[$index]}}">
          <button class="button" ng-click="start($index)" ng-show="progress[$index] < 0">Start</button>
          <span class="progress" ng-show="progress[$index] >= 0">           
            <div style="width:{{progress[$index]}}%">{{progress[$index]}}%</div>
          </span>       
          <button class="button" ng-click="abort($index)" ng-show="hasUploader($index) && progress[$index] < 100">Abort</button>
          {{f.name}} - size: {{f.size}}B - type: {{f.type}}
        </div> -->
      <!-- </div>
      </div> -->
 </div>

 <script>

// $("#add-form-product-info").submit( function() {
//  bootbox.dialog({
//    message: "<center><strong>Loading</strong></center>",
//    show: true,
//    backdrop: true,
//    closeButton: false
//  });
// });

$("#product-info-photo").change( function() {
  var oFReader = new FileReader();
  oFReader.readAsDataURL($(this).prop("files")[0]);
  oFReader.onload = function (oFREvent) {
  $("#product-info-preview").prop("src", oFREvent.target.result);
  };
 });

function removeValue(el) {
  elem = $(el);
  elem.parent().parent().parent().parent().remove();
}

$(function(){

        var LatitudeLongitudeCoordinates,
            DefaultLongitude = 122.72092948635861,
            DefaultLatitude = 11.646338452790836;
        
        <?php if( isset($record['lat']) && $record['lat'] ): ?>
          LatitudeLongitudeCoordinates = new google.maps.LatLng(<?= $record['lat'] ?>, <?= $record['lng'] ?>);
        <?php else: ?>
          LatitudeLongitudeCoordinates = new google.maps.LatLng(DefaultLatitude, DefaultLongitude);
        <?php endif; ?>

        var mapOptions = {
            zoom: <?php echo isset($record['lat']) && $record['lat'] ? 18 : 6 ?>,
            center: LatitudeLongitudeCoordinates,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById('google_map_canvas'), mapOptions);
        var infowindow = new google.maps.InfoWindow();
        var bounds = new google.maps.LatLngBounds();

        var marker = new google.maps.Marker({
              draggable: true, position: LatitudeLongitudeCoordinates, map: map
          });

        google.maps.event.addListener(marker, 'drag', function(event){
           document.getElementById("latitude").value = event.latLng.lat();
           document.getElementById("longitude").value = event.latLng.lng();
    });

        var listener = google.maps.event.addListener(map, "idle", function () {
            map.setZoom(<?php echo isset($record['lat']) && $record['lat'] ? 18 : 6 ?>);
            google.maps.event.removeListener(listener);
        });
    });
 </script>
 <!--end main content 