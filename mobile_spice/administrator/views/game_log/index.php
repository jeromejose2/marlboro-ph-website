<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Webgame Logs <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>
               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/game_log/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>   
               </div>
		  </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>User</th>
                         <th>Game</th>
                         <th>Score</th>
                         <th>Computed Score</th>
                         <th>Cheater</th>
                         <th>Date</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
               		<form action="<?php echo SITE_URL ?>/game_log">
                     <tr>
                         <td></td>
                         <td>
                        	<input name="name" class="form-control"  />
                         </td>
                         <td>
                         	<select class="form-control" name="game_id">
                            	<option></option>
                                <?php 
									foreach($games as $k => $v) {
										$selected =  isset($_GET['game_id']) && $_GET['game_id'] != '' && $_GET['game_id'] == $v['game_id'] ? 'selected' : '';
										echo '<option value="' . $v['game_id'] . '"' . $selected . '>' . $v['name'] . '</option>';	
									}
								?>
                            	
                            </select>
                         </td>
                         <td><input name="score" class="form-control"  /></td>
                         <td></td>
                         <td>
                            <select class="form-control" name="is_cheater">
                                <option value=""></option>
                                <option value="1" <?php echo isset($_GET['is_cheater']) && $_GET['is_cheater'] == 1 ? 'selected' : '' ?>>Yes</option>
                                <option value="0" <?php echo isset($_GET['is_cheater']) && $_GET['is_cheater'] == '0' ? 'selected' : '' ?>>No</option>
                            </select>
                         </td>
                         <td>
                            <div class="row">
                              <div class="col-md-6 col-lg-4">
                                   <input name="from" placeholder="From" class="form-control from" value="<?php echo isset($_GET['from']) ? $_GET['from'] : '' ?>" />
                              </div>
                              <div class="col-md-6 col-lg-4">
                                   <input name="to" placeholder="To" class="form-control to" value="<?php echo isset($_GET['to']) ? $_GET['to'] : '' ?>" />
                              </div>
                         </div>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
                    <?php if($logs): 
					foreach($logs as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><a href="#" onclick="showRegistrantDetails(<?php echo $v['registrant_id'] ?>)"><?php echo $v['first_name'] . ' ' . $v['third_name'] ?></a></td>
                         <td><?php echo $v['name'] ?></td>
                         <td><?php echo $v['score'] ?></td>
                         <td><?php echo $v['computed_score'] ?></td>
                         <td><?php echo $v['is_cheater'] == 1 ? 'Yes' : 'No' ?></td>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['date_played'])) ?></td>
                         <td></td>
                         <td class="modal">
                             <div class="modal" id="log-details-<?php echo $v['audit_trail_id'] ?>">
                                <?php $changes = unserialize($v['field_changes']);?>
                                <strong>Record ID:</strong> <?php echo $v['record_id'] ?><br>
                                <strong>Name:</strong> <?php echo $v['record_name'] ?><br>
                                <?php if($v['message']): ?><strong>Message:</strong> <?php echo $v['message'] ?><br><?php endif; ?><br>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Field</th>
                                            <th>Old Value</th>
                                            <th>New Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if($changes['old']): 
											foreach($changes['old'] as $k => $v): ?>
											<tr>
                                                <td><?php echo $k  ?></td>
                                                <td><?php echo $changes['old'][$k]  ?></td>
                                                <td><?php echo $changes['new'][$k]  ?></td>
                                            </tr>
										<?php 
										endforeach;
										else:
										echo '<tr><td colspan="3">No changes found</td></tr>';
										endif; ?>	
                                    </tbody>
                                </table>
                             </div>
                         </td>
                    </tr>
                    <?php 
					endforeach;
					else:
						echo '<tr><td colspan="7">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->
     
   <script>
 	function showLogDetails(id) {
		bootbox.dialog({'message': $('#log-details-' + id).html(),
						'title': 'Admin Activity Log Details'});
	}
 </script>
