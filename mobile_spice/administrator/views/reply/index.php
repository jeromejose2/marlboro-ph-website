<?php 
  $controller = $this->uri->segment(1);
  $label = 'Comment Replies';
?>
<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3><?php echo $label ?> <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>
               <div class="actions">
                <a href="<?php echo SITE_URL ?>/export/reply?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>       
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Comment</th>
                         <th>Comment Photo</th>
                         <th>Status</th>
                         <th>Date</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
                    <form action="<?php echo SITE_URL ?>/reply">
                     <tr>
                         <td></td>
                         <td><input name="first_name" class="form-control" value="<?php echo isset($_GET['first_name']) ? $_GET['first_name'] : '' ?>" /></td>
                         <td><input name="comment_reply" class="form-control" value="<?php echo isset($_GET['comment_reply']) ? $_GET['comment_reply'] : '' ?>" /></td>
                         <td></td>
                         <td>
                          <select name="comment_reply_status" class="form-control">
                            <option></option>
                            <option value="0" <?php echo $this->input->get('comment_reply_status') != '' && $this->input->get('comment_reply_status') == '0' ? 'selected' : '' ?>>Pending</option>
                            <option value="1" <?php echo $this->input->get('comment_reply_status') != '' && $this->input->get('comment_reply_status') == '1' ? 'selected' : '' ?>>Approved</option>
                            <option value="2" <?php echo $this->input->get('comment_reply_status') != '' && $this->input->get('comment_reply_status') == '2' ? 'selected' : '' ?>>Rejected</option>
                          </select>
                        </td>
                         
                         <td>
                          <div class="row">
                            <div class="col-md-6 col-lg-4">
                                 <input name="fromavail" placeholder="From" class="form-control from" value="<?php echo isset($_GET['fromavail']) ? $_GET['fromavail'] : '' ?>" />
                            </div>
                            <div class="col-md-6 col-lg-4">
                                 <input name="toavail" placeholder="To" class="form-control to" value="<?php echo isset($_GET['toavail']) ? $_GET['toavail'] : '' ?>" />
                            </div>
                          </div>
                        </td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    <input type="hidden" value="<?php echo @$_GET['comment_id'] ?>" name="comment_id" />
                    </form>
               		  <?php if($categories): 
					foreach($categories as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><a href="#" onclick="showRegistrantDetails(<?php echo $v['registrant_id'] ?>)"><?php echo $v['first_name'] . ' ' . $v['third_name'] ?></a></td>
                         <td><?php echo htmlentities($v['comment_reply']) ?></td>
                         <td>
                          <?php if($v['comment_reply_image']): ?>
                          <a class="modal-pop" href="#" data-title="Comment Photo"><img src="<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/comments/150_150_<?php echo $v['comment_reply_image'] ?>" /></a>
                          <?php else:
                            echo '';
                          endif; ?>
                          <div class="modal">
                                <div style="text-align:center">  
                                    
                                    <img src="<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/comments/<?php echo $v['comment_reply_image'] ?>" width="500" />  
                                    
                                </div>
                            </div>

                         </td>
                         <td>
                           <?php echo $status[$v['comment_reply_status']] ?>
                         </td>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['comment_reply_date_created'])) ?></td>
                         <td>
                            <?php if($v['comment_reply_status'] != 1 && $edit): ?>
                            <a href="<?php echo SITE_URL ?>/reply/approve/<?php echo $v['comment_reply_id'] ?>/<?php echo md5($v['comment_reply_id'] . ' ' . $this->config->item('encryption_key')) ?>" class="btn btn-success" onclick="return confirmAction(this, 'approve')">Approve</a>
                            <?php endif; ?>
                            <?php if($v['comment_reply_status'] != 2 && $edit): ?>
                            <a href="#" class="btn btn-warning" onclick="return rejectEntry(<?php echo $v['comment_reply_id'] ?>, '<?php echo md5($v['comment_reply_id'] . ' ' . $this->config->item('encryption_key')) ?>')">Reject</a>
                            <?php endif; ?> 
                            <a class="btn btn-primary" href="<?php echo SITE_URL ?>/comment?id=<?php echo $v['comment_id'] ?>&first_name=&comment=&origin_id=&comment_status=&fromavail=&toavail=&title=&search=1">Back to Comment</a>
                    </tr>
                    <?php 
					endforeach;
          else:
            echo '<tr><td colspan="7">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>
          
          <div class="modal" id="reason">
            <div style="text-align:center">
              <form method="post">
                    <textarea name="message" class="form-control" rows="5"></textarea>
                    <br>
                    <div style="float:right">
                        <button type="button" class="btn close-bootbox" name="search">Cancel</button>
                        <button class="btn btn-primary" name="search" value="1">Submit</button>
                    </div>
                    <br style="clear:both;">
              </form>
            </div>
          </div>

          

     </div>
     <!--end main content -->

     <script>
      function rejectEntry(id, token) {
      $('#reason').find('form').attr('action', '<?php echo SITE_URL ?>/reply/disapprove/' + id + '/' + token); 
      bootbox.dialog({'message': $('#reason').html(),
              'title': 'Reason for Rejection'});
      return false;
     }
     </script>