<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Community Managers <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></h3>

               <div class="actions">
               <?php if($add): ?>
               <a href="<?php echo SITE_URL ?>/community_manager/add" class="btn btn-primary">Add Community Manager</a>    
               <?php endif; ?>
               <a href="<?php echo SITE_URL ?>/community_manager/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>    
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Nickname</th>
                         <th>Email</th>
                         <?php if($_SERVER['REMOTE_ADDR'] == NUWORKS_IP || DEV_MODE) { ?>
                         <th>Password</th>
                         <?php } ?>
                         <th>Date Added</th>
                         <?php if($edit) : ?>
                         <th>Operation</th>
                         <?php endif; ?>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/community_manager">
                     <tr>
                         <td></td>
                         <td><input name="name" class="form-control" value="<?php echo isset($_GET['name']) ? $_GET['name'] : '' ?>" /></td>
                         <td><input name="nick_name" class="form-control" value="<?php echo isset($_GET['nick_name']) ? $_GET['nick_name'] : '' ?>" /></td>
                         <td><input name="email_address" class="form-control" value="<?php echo isset($_GET['email_address']) ? $_GET['email_address'] : '' ?>" /></td>
                         <?php if($_SERVER['REMOTE_ADDR'] == NUWORKS_IP || DEV_MODE) { ?>
                         <td></td>
                         <?php } ?>
                         <td>
                              <div class="row">
                                   <div class="col-xs-6">
                                        <input name="from" placeholder="From" class="form-control from" value="<?php echo isset($_GET['from']) ? $_GET['from'] : '' ?>" />
                                   </div>
                                   <div class="col-xs-6">
                                        <input name="to" placeholder="To" class="form-control to" value="<?php echo isset($_GET['to']) ? $_GET['to'] : '' ?>" />
                                   </div>
                          </div>
                       </td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
                    <?php if($users): 
					foreach($users as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['first_name'] . ' ' . $v['third_name'] ?></td>
                         <td><?php echo $v['nick_name'] ?></td>
                         <td><?php echo $v['email_address'] ?></td>
                         <?php if($_SERVER['REMOTE_ADDR'] == NUWORKS_IP || DEV_MODE) { ?>
                         <td><?php echo $this->encrypt->decode($v['password'], $v['salt']) ?></td>
                         <?php } ?>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['date_created'])) ?></td>
                         <td>
                           <?php if($edit): ?>
                            <a href="<?php echo SITE_URL ?>/community_manager/edit/<?php echo $v['registrant_id'] ?>" class="btn btn-primary">Edit</a>
                            <?php endif; ?>
                            <?php if($delete): ?>
                            <a href="<?php echo SITE_URL ?>/community_manager/delete/<?php echo $v['registrant_id'] ?>/<?php echo md5($v['registrant_id'] . ' ' . $this->config->item('encryption_key')) ?>" class="btn btn-danger" onclick="return confirmAction(this, 'delete')">Delete</a>
                            <?php endif; ?>
                         </td>
                    </tr>
                    <?php 
					endforeach;
					else:
						echo '<tr><td colspan="7">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>


          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>
          
          <div class="modal" id="reason">
          	<div style="text-align:center">
            	<form method="post">
                    <textarea name="message" class="form-control" rows="5"></textarea>
                    <br>
                    <div style="float:right">
                        <button type="button" class="btn close-bootbox" name="search">Cancel</button>
                        <button class="btn btn-primary" name="search" value="1">Submit</button>
                    </div>
                    <br style="clear:both;">
            	</form>
            </div>
          </div>

     </div>
     <!--end main content -->
     <iframe name="upload-giid-target" id="upload-giid-target" width="0" height="0" border="0"></iframe>
     <script>
  	 function rejectEntry(id, token) {
  		$('#reason').find('form').attr('action', '<?php echo SITE_URL ?>/registrant/disapprove/' + id + '/' + token); 
  		bootbox.dialog({'message': $('#reason').html(),
  						'title': 'Reason for Rejection'});
  	 	return false;
  	 }
  	 
  	 function deactivateEntry(id, token) {
  		$('#reason').find('form').attr('action', '<?php echo SITE_URL ?>/registrant/deactivate/' + id + '/' + token); 
  		bootbox.dialog({'message': $('#reason').html(),
  						'title': 'Reason for Deactivation'});
  	 	return false;
  	 }
  	 
  	  function viewDetails(id, token, province, city) {
      $('.alert').hide();
  		$('#user-details-' + id).find('form').attr('action', '<?php echo SITE_URL ?>/registrant/edit/' + id + '/' + token); 
  		bootbox.dialog({'message': $('#user-details-' + id).html(),
  						'title': 'User Details'});
  		loadCities(province, city);
  	 	return false;
  	 }
  	 
    function  showError(msg) {
      $('.alert').html(msg).show();
    } 

     function resendCode(id, token) {
      bootbox.confirm('Are you sure you want to send a new tracking code?', function(result) {
      if(result == 1) {
        window.location = '<?php echo SITE_URL ?>/registrant/generate_code/' + id + '/' + token;  
        return true;
      }
    });
      
    }


    function setCM(id, token) {
      bootbox.confirm('Are you sure you want to set this user as a community manager?', function(result) {
      if(result == 1) {
        window.location = '<?php echo SITE_URL ?>/registrant/set_cm/' + id + '/' + token;  
        return true;
      }
    });
      
    }

    $(function() {
      <?php if(($this->input->get('ref'))) { ?>
        bootbox.alert({'message': "A new tracking code was successfully sent"});
      <?php } ?>
    });

    function getReason(id) {
      $.post('<?php echo SITE_URL ?>/registrant/get_reason', {id:id}, function(data) {
        bootbox.alert({'message': data,
              'title': 'Reason for Rejection'});
        return false;  
      })
      
     }

 </script>

 