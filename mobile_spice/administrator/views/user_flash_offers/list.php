<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3> <?php echo $header_text; ?> <span class="badge badge-important"><?php echo (int)$total_rows; ?></span></h3>

               <div class="actions">
                   <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/export'.$query_strings; ?>" class="btn btn-primary">Export</a> 
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                          <th>Confirmed By</th>
                         <th>Prize Name</th>
                         <th>Media</th>
                         <th>Status</th>
                          <th>Date Availed</th>
                         <th>Date Redeemed/Delivered</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
                    <form action="<?php echo SITE_URL ?>/user_flash_offers">
                     <tr>
                          <td><input name="name" class="form-control" value="<?php echo $this->input->get('name') ?>" /></td>
                         <td><input name="prize_name" class="form-control" value="<?php echo $this->input->get('prize_name') ?>" /></td>
                         <td></td>
                         <td>
                          <select name="prize_status" class="form-control">
                            <option></option>
                            <option value="0" <?php echo $this->input->get('prize_status') === '0' ? 'selected' : '' ?>>Pending</option>
                            <option value="1" <?php echo $this->input->get('prize_status') == '1' ? 'selected' : '' ?>>Delivered</option>
                            <option value="1" <?php echo $this->input->get('prize_status') == '2' ? 'selected' : '' ?>>Redeemed</option>
                          </select>
                        </td>
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                     <input name="fromavail" placeholder="From" class="form-control from" value="<?php echo isset($_GET['fromavail']) ? $_GET['fromavail'] : '' ?>" />
                                </div>
                                <div class="col-md-4">
                                     <input name="toavail" placeholder="To" class="form-control to" value="<?php echo isset($_GET['toavail']) ? $_GET['toavail'] : '' ?>" />
                                </div>
                              </div>
                        </td>
                        <td>
                            <div class="row">
                              <div class="col-md-4">
                                   <input name="fromredeem" placeholder="From" class="form-control from" value="<?php echo isset($_GET['fromredeem']) ? $_GET['fromredeem'] : '' ?>" />
                              </div>
                              <div class="col-md-4">
                                   <input name="toredeem" placeholder="To" class="form-control to" value="<?php echo isset($_GET['toredeem']) ? $_GET['toredeem'] : '' ?>" />
                              </div>
                            </div>
                        </td>
                        <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
                       <?php if($rows->num_rows()){ 
                              $controller = $this->router->class;
                              $base_path = SITE_URL.'/'.$controller.'/';
                              foreach($rows->result() as $v){ ?>
                                <tr>
                                     <td><a href="#" onclick="showRegistrantDetails(<?php echo $v['registrant_id'] ?>)"><?=$v->first_name.' '.$v->third_name?></a></td>
                                     <td><?=$v->prize_name?></td>
                                     <td>
                                      <?php if(file_exists('uploads/prize/'.$v->media)){ ?>
                                        <img src="<?=BASE_URL.'uploads/prize/'.$v->media?>"/>
                                      <?php } ?>
                                      </td>
                                     <td>
                                      <?php if($v->prize_status=='1'){
                                            echo 'Delivered';
                                      }else if($v->prize_status=='2'){
                                            echo 'Redeemed';
                                      }else{
                                          echo 'Pending';
                                      } ?>

                                     </td>
                                     <td><?=$v->date_created?></td>
                                     <td><?=$v->date_claimed?></td>
                                     <td><a class="btn btn-primary" href="<?=$base_path?>edit/4">Edit</a></td>
                                </tr>
                      <?php }
                      }else{ ?>
                      <tr>
                          <td colspan="9">No result found.</td>
                      </tr>
                      <?php } ?>                     
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->