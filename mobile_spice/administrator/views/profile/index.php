
<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>User Profile - Photos <span class="badge badge-important"><?php echo $total ?></span></h3>

               <div class="actions">
               	<a href="<?php echo SITE_URL ?>/export/profiles?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>     
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Email</th>
                         <th>Profile Picture</th>
                         <th>Status</th>
                         <th>Date Registered</th>
                         <?php if($edit) : ?>
                         <th>Operation</th>
                         <?php endif; ?>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/profile">
                     <tr>
                         <td></td>
                         <td><input name="name" class="form-control" value="<?php echo isset($_GET['name']) ? $_GET['name'] : '' ?>" /></td>
                         <td><input name="email" class="form-control" value="<?php echo isset($_GET['email']) ? $_GET['email'] : '' ?>" /></td>
                         <td></td>
                         <td>
                         	<select class="form-control" name="status">
                            	<option></option>
                                <?php 
									foreach($status as $k => $v) {
										$selected =  isset($_GET['status']) && $_GET['status'] != '' && $_GET['status'] == $k ? 'selected' : '';
										echo '<option value="' . $k . '"' . $selected . '>' . $v . '</option>';	
									}
								?>
                            	
                            </select>
                         </td>
                         <td>
                              <div class="row">
                                   <div class="col-md-5">
                                        <input name="from" placeholder="From" class="form-control from" value="<?php echo isset($_GET['from']) ? $_GET['from'] : '' ?>" />
                                   </div>
                                   <div class="col-md-5">
                                        <input name="to" placeholder="To" class="form-control to" value="<?php echo isset($_GET['to']) ? $_GET['to'] : '' ?>" />
                                   </div>
                              </div>
                        </td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
                    <?php if($users): 
					foreach($users as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['first_name'] . ' ' . $v['third_name'] ?></td>
                         <td><?php echo $v['email_address'] ?></td>
                         <td>
                         	<a class="modal-pop" href="#" data-title="Profile Picture of <?php echo $v['first_name'] . ' ' . $v['third_name'] ?>"><img src="<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/<?php echo $v['picture_status'] == 1 ? $v['current_picture'] : $v['new_picture'] ?>" width="150" /></a>
                         	<div class="modal">
                                <div style="text-align:center">  
                                    <img src="<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/<?php echo $v['picture_status'] == 1 ? $v['current_picture'] : $v['new_picture'] ?>" width="500" />  
                                </div>
                            </div>
                         </td>
                         <td><?php echo $status[$v['picture_status']] ?></td>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['date_created'])) ?></td>
						 <?php if($edit): ?>
                         <td>
                         	<?php if($v['picture_status'] != 1 && $edit): ?>
                         	<a href="<?php echo SITE_URL ?>/profile/approve/<?php echo $v['registrant_id'] ?>/<?php echo md5($v['registrant_id'] . ' ' . $this->config->item('encryption_key')) ?>" class="btn btn-success" onclick="return confirmAction(this, 'approve', 'photo')">Approve</a>
							<?php endif; ?>
                            <?php if($v['picture_status'] != 2 && $edit): ?>
                            <a href="#" class="btn btn-warning" onclick="return rejectEntry(<?php echo $v['registrant_id'] ?>, '<?php echo md5($v['registrant_id'] . ' ' . $this->config->item('encryption_key')) ?>')">Reject</a>
                            <?php endif; ?> 
                            <a href="#" class="btn btn-primary" onclick="return viewDetails(<?php echo $v['registrant_id'] ?>, '<?php echo md5($v['registrant_id'] . ' ' . $this->config->item('encryption_key')) ?>', '<?php echo $v['province'] ?>', '<?php echo $v['city'] ?>')">View Details</a>
                            <a href="#" class="btn btn-primary" type="button" onclick="resendCode(<?php echo $v['registrant_id'] ?>, '<?php echo md5($v['registrant_id'] . ' ' . $this->config->item('encryption_key')) ?>')">Resend Tracking Code</a>
                            <?php if($v['is_cm'] == 0): ?>
                            <a href="#" class="btn btn-primary" type="button" onclick="setCM(<?php echo $v['registrant_id'] ?>, '<?php echo md5($v['registrant_id'] . ' ' . $this->config->item('encryption_key')) ?>')">Set Community Manager</a>
                            <?php endif; ?>
                         </td>
                         <?php endif; ?>
                         <td class="modal" id="user-details-<?php echo $v['registrant_id'] ?>">
                         	<div>
                            	<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" onsubmit="return submitFrm()" target="upload-giid-target">
                                  <div class="alert alert-danger" style="display:none;"></div>
                                   <h4>GIID Details</h4>
                                   <div class="form-group">
                                        <label class="col-sm-3 control-label">Image</label>
                                        <div class="col-sm-6">
                                           <p class="form-control-static">
                                            <?php if($v['giid_file']): ?>
                                            <?php if(strrpos('pdf', $v['giid_file'])) :  ?>
                                                <a target="_blank" href="<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/giid/<?php echo $v['giid_file'] ?>">View PDF</a> 
                                              <?php else: ?> 
                                              <a class="modal-pop" href="#" data-title="GIID of <?php echo $v['first_name'] . ' ' . $v['third_name'] ?>"><img src="<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/giid/<?php echo $v['giid_file'] ?>" width="150" /></a> 
                                            <?php endif; ?>
                                            <br ><br >
                                            <input type="file" name="giid" class="form-control" />   
                                            <?php else: 
                                            echo 'No GIID';
                                            endif; ?>
                                            </p>
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <label class="col-sm-3 control-label">GIID Type</label>
                                        <div class="col-sm-6">
                                           <p class="form-control-static">
                                            <select class="form-control" name="government_id_type">
                                                <option value="0">NO GIID</option>
                                                <?php foreach($giid as $gk => $gv): ?>
                                                    <option <?php echo ($v['government_id_type'] == $gk) ? 'selected'  :'' ?> value="<?php echo trim($gk) ?>"><?php echo $gv?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            </p>
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <label class="col-sm-3 control-label">GIID Number</label>
                                        <div class="col-sm-6">
                                           <p class="form-control-static">
                                            <input value="<?php echo $v['government_id_number'] ?>" class="form-control" name="government_id_number" data-name="giid number">
                                           </p>
                                        </div>
                                   </div><br >
                                   <h4>Profile Details</h4>
                                   <div class="form-group">
                                        <label class="col-sm-3 control-label">Profile Picture</label>
                                        <div class="col-sm-4">
                                          <?php if($v['current_picture']): ?>
                                          <img src="<?php echo BASE_URL ?>uploads/profile/<?php echo $v['registrant_id'] ?>/<?php echo $v['current_picture'] ?>" width="200" />
                                          <?php else: 
                                            echo 'No profile picture';
                                            endif; ?>
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <label class="col-sm-3 control-label">Nickname</label>
                                        <div class="col-sm-6">
                                          <input value="<?php echo $v['nick_name'] ?>" class="form-control from" name="nick_name" data-name="nickname">
                                           <!-- <p class="form-control-static"><?php echo $v['nick_name'] ?></p> -->
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <label class="col-sm-3 control-label">First Name</label>
                                        <div class="col-sm-6">
                                            <input value="<?php echo $v['first_name'] ?>" class="form-control from" name="first_name" data-name="first name">
                                            <!-- <p class="form-control-static"><?php echo $v['first_name'] ?></p> -->
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <label class="col-sm-3 control-label">Middle Name</label>
                                        <div class="col-sm-6">
                                            <input value="<?php echo $v['middle_initial'] ?>" class="form-control from" name="middle_initial" data-name="middle name">
                                            <!-- <p class="form-control-static"><?php echo $v['middle_initial'] ?></p> -->
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <label class="col-sm-3 control-label">Last Name</label>
                                        <div class="col-sm-6">
                                            <input value="<?php echo $v['third_name'] ?>" class="form-control from" name="third_name" data-name="last name">
                                            <!-- <p class="form-control-static"><?php echo $v['third_name'] ?></p> -->
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <label class="col-sm-3 control-label">Birthday</label>
                                        <div class="col-sm-6">
                                            <input value="<?php echo $v['date_of_birth'] ?>" class="form-control from" name="date_of_birth" data-name="date of birth">
                                            <!-- <p class="form-control-static"><?php echo date('F d, Y', strtotime($v['date_of_birth'])) ?></p> -->
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <label class="col-sm-3 control-label">Mobile Number</label>
                                        <div class="col-sm-6">
                                            <input value="<?php echo $v['mobile_phone'] ?>" class="form-control" name="mobile_phone" data-name="mobile phone">
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <label class="col-sm-3 control-label">Gender</label>
                                        <div class="col-sm-6">
                                            <select class="form-control" data-name="gender" name="gender">
                                                <option <?php echo $v['gender'] == 'M' ? 'selected' : '' ?>>M</option>
                                                <option <?php echo $v['gender'] == 'F' ? 'selected' : '' ?>>F</option>
                                            </select>
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <label class="col-sm-3 control-label">Email Address</label>
                                        <div class="col-sm-6">
                                            <input value="<?php echo $v['email_address'] ?>" class="form-control" name="email_address" data-name="email address">
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <label class="col-sm-3 control-label">House and Street</label>
                                        <div class="col-sm-6">
                                            <input value="<?php echo $v['street_name'] ?>" class="form-control" name="street_name" data-name="street name">
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <label class="col-sm-3 control-label">Barangay</label>
                                        <div class="col-sm-6">
                                            <input value="<?php echo $v['barangay'] ?>" class="form-control" name="barangay" data-name="barangay">
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <label class="col-sm-3 control-label">Province</label>
                                        <div class="col-sm-6">
                                             <select class="form-control provinces" onchange="loadCities(this.value, '')" name="province">
                                              <?php foreach($provinces as $pk => $pv): ?>
                                                  <option value="<?php echo trim($pv['province_id']) ?>"><?php echo $pv['province'] ?></option>
                        <?php endforeach; ?>
                                            </select>
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <label class="col-sm-3 control-label">City</label>
                                        <div class="col-sm-6">
                                             <select class="cities form-control" name="city">
                                              <option>asdfasdf</option>
                                            </select>
                                        </div>
                                   </div>
                                  <div class="form-group">
                                        <label class="col-sm-3 control-label">Zip Code</label>
                                        <div class="col-sm-6">
                                            <input value="<?php echo $v['zip_code'] ?>" class="form-control" name="zip_code" data-name="zipcode">
                                        </div>
                                   </div>
                                  <div class="form-group">
                                        <?php $reason = json_decode($v['arclight_reject_reason']); if($reason):?>
                                        
                                        <label class="col-sm-3 control-label">Arclight Reason</label>
                                        <div class="col-sm-6">
                                            <?php 
                                            //$reason = json_decode($v['arclight_reject_reason']);
                                            if($reason) {
                                              for($i = 0; $i < count($reason); $i++) {
                                                echo $reason[$i]->AttributeName . ':' . $reason[$i]->Message . '<br >';
                                              }
                                            }
                                            ?>
                                        </div>
                                      <?php endif; ?>
                                   </div>
                                   <br >
                                   <h4>Survey Answers</h4>
                                   <div class="form-group">
                                        <span class="col-sm-6 help-block">What brand do you smoke most frequently?</span>
                    <p class="form-control-static"><?php echo @$brands[$v['current_brand']]['brand_name'] ?></p> 
                                   </div>
                                   <div class="form-group">
                                        <span class="col-sm-6 help-block">What other brands do you smoke aside from your regular brand?</span>
                    <p class="form-control-static"><?php echo @$brands[$v['first_alternate_brand']]['brand_name'] ?></p> 
                                   </div>
                                   <div class="form-group">
                                        <span class="col-sm-6 help-block">What would you do if your regular brand is unavailable?</span>
                    <p class="form-control-static"><?php echo @$alternate[$v['alternate_purchase_indicator']]['alternate_value'] ?></p>  
                                   </div>
                                   <div class="form-group control-label">
                                        <div class="col-sm-offset-2 col-sm-10">
                                             <input type="hidden" name="submit" value="1" />
                                             <input type="hidden" name="id" value="<?php echo $this->uri->segment(3) ?>" />
                                             <button name="search" class="btn close-bootbox" type="button">Cancel</button>
                                             <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                   </div>
                              </form>
                            </div>
                         </td>
                    </tr>
                    <?php 
					endforeach;
					else:
						echo '<tr><td colspan="7">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>
          
          <div class="modal" id="reason">
          	<div style="text-align:center">
            	<form method="post">
                    <textarea name="message" class="form-control" rows="5"></textarea>
                    <br>
                    <div style="float:right">
                        <button type="button" class="btn close-bootbox" name="search">Cancel</button>
                        <button class="btn btn-primary" name="search" value="1">Submit</button>
                    </div>
                    <br style="clear:both;">
            	</form>
            </div>
          </div>

     </div>
     <!--end main content -->
     <iframe name="upload-giid-target" id="upload-giid-target" width="0" height="0" border="0"></iframe>
     
     <script>
	 function rejectEntry(id, token) {
		$('#reason').find('form').attr('action', '<?php echo SITE_URL ?>/profile/disapprove/' + id + '/' + token); 
		bootbox.dialog({'message': $('#reason').html(),
						'title': 'Reason for Rejection'});
	 	return false;
	 }
	 
	  function viewDetails(id, token, province, city) {
		$('#user-details-' + id).find('form').attr('action', '<?php echo SITE_URL ?>/registrant/edit/' + id + '/' + token); 
		bootbox.dialog({'message': $('#user-details-' + id).html(),
						                     'title': 'User Details'});
		loadCities(province, city);
	 	return false;
	 }

   function  showError(msg) {
      $('.alert').html(msg).show();
    } 

    function resendCode(id, token) {
      bootbox.confirm('Are you sure you want to send a new tracking code?', function(result) {
        if(result == 1) {
          window.location = '<?php echo SITE_URL ?>/registrant/generate_code/' + id + '/' + token;  
          return true;
        }
      });

    }

    function setCM(id, token) {
      bootbox.confirm('Are you sure you want to set this user as a community manager?', function(result) {
      if(result == 1) {
        window.location = '<?php echo SITE_URL ?>/registrant/set_cm/' + id + '/' + token;  
        return true;
      }
    });
      
    }

    $(function() {
      <?php if(($this->input->get('ref'))) { ?>
        bootbox.alert({'message': "A new tracking code was successfully sent"});
      <?php } ?>
    });
	 </script>

 