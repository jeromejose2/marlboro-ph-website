<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3> <?php echo $header_text; ?> <span class="badge badge-important"><?php echo (int)$total_rows; ?></span></h3>

               <div class="actions">
                    
                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/export'.$query_strings; ?>" class="btn btn-primary">Export</a>   
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>Name</th>
                         <th>Event</th>
                         <th>Schedule</th>
                         <th>Date Added</th>                           
                         <th><?php if($edit || $delete) : ?>Operation<?php endif; ?></th>                         
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>">
                     <tr>
                        <th><input type="text" name="name" class="form-control" value="<?php echo $this->input->get('name'); ?>"></th>
                        <th><select name="event" class="form-control">
                              <option value=""></option>
                              <?php if($events->num_rows()){
                                        foreach($events->result() as $v){ 
                                          if($this->input->get('event')){ ?>
                                              <option selected="selected" value="<?=$v->backstage_event_id?>"><?=$v->title?></option>
                                        <?php }else{ ?>
                                              <option value="<?=$v->backstage_event_id?>"><?=$v->title?></option>
                                        <?php } 
                                      }
                                    }?>
                            </select></th>
                        <th><input type="text" name="schedule" class="form-control from" value="<?php echo $this->input->get('schedule'); ?>"></th>
                        <th>From:<input name="from_date_added" class="form-control from" value="<?php echo $this->input->get('from_date_added'); ?>" /><br> 
                            To:<input name="to_date_added" class="form-control to" value="<?php echo $this->input->get('to_date_added'); ?>" /></th>                       
                        <th><?php if($edit || $delete) : ?><button type="submit" class="btn btn-primary">Go</button><?php endif; ?></th>
                    </tr>
                  </form>
                  <?php if($rows->num_rows()){ 
                            foreach($rows->result() as $v){ ?>
                              <tr>
                                  <td><?php echo $v->first_name.' '.$v->third_name; ?></td>
                                  <td><?php echo $v->event; ?></td>
                                  <td><?php echo simplify_datetime_range($v->start_date,$v->end_date); ?></td>
                                  <td><?php echo $v->date_added; ?></td>
                                  <td>  </td>
                                
                              </tr>
                  <?php     }

                        }else{ ?>
                          <tr><td colspan="5">No records found.</td></tr>
                  <?php  } ?>

                  
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->