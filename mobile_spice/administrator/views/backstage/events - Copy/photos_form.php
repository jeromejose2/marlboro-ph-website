 
 
<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3><?php echo ($event) ? 'Add Photo(s) in <u>'.$event->title.'</u>' : 'Not found'; ?></h3>
      </div>

      <?php if($error){ ?>
       <div class="alert alert-danger"> <?php echo $error; ?> </div>
      <?php } ?>

       <form action="<?=SITE_URL.'/backstage_events/add_photos/'.$backstage_event_id?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="image_counter" value="1" />
  

           <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                  <a href="javascript:void(0);" class="btn add btn-success "><span class="glyphicon glyphicon-plus"></span> Add</a>
                  </div>
           </div>

           <div class="form-group"  id="add-answer">
                <label class="col-sm-2 control-label"> </label>
                <div class="col-sm-4"></div>
           </div>
           
           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4"> 
                     <a href="<?=SITE_URL.'/backstage_events/photos?backstage_event_id='.$backstage_event_id?>" class="pull-left"><< Back</a>
                      <button type="submit" class="btn btn-primary pull-right">Submit</button>
                </div>
           </div>
           
           
      </form>
      
 </div>
 <!--end main content -->
 <script>

 $(document).ready(function(){
  
 
 
    

    jQuery('.add').click(function(){
      

      var e = parseInt($('.slider-image').length);

      var html = '';

      
 

        e = e + 1;

        html = '<div class="form-group"><label class="col-sm-2 control-label"></label><div class="col-sm-4"><input onchange="PreviewImage('+e+');" type="file" id="slider-image'+e+'" data-name="module name" class="slider-image form-control" name="slider-image'+e+'"><span id="preview-'+e+'"></span><a href="#" class="remove-answer"  >Remove</a></div></div>';

       
       

      jQuery('#add-answer').before(html);

      jQuery('input[name="image_counter"]').val(e);
 
      

        jQuery('.remove-answer').off('click').on('click',function(){

          

          jQuery(this).parent('div').parent('div').remove();

          

            var i = 0;

            jQuery('.slider-image').each(function(){

              i++;

              if($(this).attr('name')!='image')

                jQuery(this).attr('name','slider-image'+i);       

            });

            jQuery('input[name="image_counter"]').val(i);
 

          });

          

      });


    jQuery('.remove-answer').on('click',function(){

        jQuery(this).parent('div').parent('div').remove();

          var i = 0;

        jQuery('.slider-image').each(function(){

          i++;

          jQuery(this).attr('name','slider-image'+i);       

        });

        jQuery('input[name="image_counter"]').val(i);


      });
 
       default_uploader(); 

    function default_uploader(){
      
      var i =0;
      
      while(i<1){
        jQuery('.add').trigger("click");
        i++;
      }

    }


});

 function PreviewImage(el) {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById('slider-image'+el).files[0]);
    oFReader.onload = function (oFREvent) {
      $('#preview-'+el).html('<img width="200" src="'+oFREvent.target.result+'"/> ').show();
    };
  }

</script>