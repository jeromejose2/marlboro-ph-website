
<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3> <?php echo $header_text; ?> <span class="badge badge-important"><?php echo (int)$total_rows; ?></span></h3>

               <div class="actions"> 
                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/export'; ?>" class="btn btn-primary">Export</a>   
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th><center>Thumbnail</center></th>
                         <th>Title</th>
                         <th>Submitted By</th> 
                         <th>Status</th>
                         <th>Date Added</th>                   
                         <th><?php if($edit || $delete) : ?>Operation<?php endif; ?></th>
                         
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>">
                     <tr>
                       <th></th>
                       <th><input type="text" name="title" class="form-control" value="<?php echo $this->input->get('title'); ?>"></th>
                       <th><input type="text" name="uploader_name" class="form-control" value="<?php echo $this->input->get('uploader_name'); ?>"></th>
                        <th><select name="status" class="form-control">
                              <option value=""></option>
                              <option value="0" <?php echo ($this->input->get('status')==='0') ? 'selected' : '' ?>>Pending</option>
                              <option value="1" <?php echo ($this->input->get('status')==APPROVED) ? 'selected' : ''; ?> >Approved</option>
                              <option value="2" <?php echo ($this->input->get('status')==DISAPPROVED) ? 'selected' : ''; ?> >Disapproved</option>
                            </select></th>
                       <th>
                        <div class="row">
                          <div class="col-md-4">
                               <input name="from_date_added" placeholder="From" class="form-control from" value="<?php echo isset($_GET['from_date_added']) ? $_GET['from_date_added'] : '' ?>" />
                          </div>
                          <div class="col-md-4">
                               <input name="to_date_added" placeholder="To" class="form-control to" value="<?php echo isset($_GET['to_date_added']) ? $_GET['to_date_added'] : '' ?>" />
                          </div>
                        </div>
                      </th>                       
                       <th><?php if($edit || $delete) : ?><button type="submit" class="btn btn-primary">Go</button><?php endif; ?></th>
                       
                     </tr>
                  </form>
                  <?php if($rows->num_rows()){

                            $edit_url = SITE_URL.'/'.$this->uri->segment(1).'/edit/';
                            $statuses = array('Pending','Approved','Disapproved');
 
                            foreach($rows->result() as $v){ ?>
                              <tr>
                                  <td><center>
                                    <a href="<?=SITE_URL?>/videos/play_video/<?=$v->about_video_id?>" class="view-video" >

                                    <?php if(file_exists('uploads/about/videos/'.$v->video) && $v->video){
                                              $key = strpos($v->video,'.');
                                              $raw = substr($v->video,0,$key);
                                              $thumbnail = file_exists('uploads/about/videos/'.substr($v->video,0,strpos($v->video,'.')).'.jpg') ? BASE_URL.'uploads/about/videos/'.substr($v->video,0,strpos($v->video,'.')).'.jpg' : BASE_URL.'uploads/about/videos/'.substr($v->video,0,strpos($v->video,'.')).'.png';
                                        ?>
                                                <img width="200" src="<?=$thumbnail?>"> 
                                    <?php }else if(preg_match('![?&]{1}v=([^&]+)!', $v->video . '&', $data)){ ?>
                                        <img src="http://img.youtube.com/vi/<?=$data[1]?>/mqdefault.jpg">
                                    <?php } ?>
                                  </a>


                                  </center></td>
                                  <td><?php echo $v->title; ?></td>
                                  <td><?=$v->uploader_name?></td>
                                  <td><?php echo $statuses[$v->status]; ?></td>
                                  <td><?php echo $v->date_added; ?></td>                                  
                                  <td> 
                                    <?php if($edit){ 

                                            if($v->status==1){?>
                                             <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/disapprove?about_video_id='.$v->about_video_id.'&token='.md5($v->about_video_id. ' ' . $this->config->item('encryption_key')); ?>"onclick="return confirmAction(this,'disapprove');" class="btn btn-danger">Disapprove</a>
                                      <?php }else if($v->status==2){ ?>
                                              <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/approve?about_video_id='.$v->about_video_id.'&token='.md5($v->about_video_id. ' ' . $this->config->item('encryption_key')); ?>" onclick="return confirmAction(this,'approve');" class="btn btn-success">Approve</a>
                                     <?php }else{ ?>
                                            <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/approve?about_video_id='.$v->about_video_id.'&token='.md5($v->about_video_id. ' ' . $this->config->item('encryption_key')); ?>" onclick="return confirmAction(this,'approve');" class="btn btn-success">Approve</a>
                                            <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/disapprove?about_video_id='.$v->about_video_id.'&token='.md5($v->about_video_id. ' ' . $this->config->item('encryption_key')); ?>"onclick="return confirmAction(this,'disapprove');" class="btn btn-danger">Disapprove</a>
                                    <?php } 
                                    
                                    } ?>
                                  </td>
                                
                              </tr>
                  <?php     }
                        }else{ ?>
                          <tr><td colspan="6">No records found.</td></tr>
                  <?php  } ?>
                  
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->
     <div id="play-video" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>
            </div>
          </div> 
      </div>

     <script>
    

     $('.view-video').on('click',function(e){
          var href = $(this).attr('href');
          $('.modal-content').html('<div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>');
          $('#play-video').modal('show');
          $('.modal-content').load(href);
          e.preventDefault();
     });
 
      $('#play-video').on('hidden.bs.modal',function(){
        $('.modal-content').html('');
     })

     </script>