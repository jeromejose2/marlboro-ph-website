<!doctype html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<title>Angular file upload sample</title>
	<link type="text/css" rel="stylesheet" href="<?=BASE_URL?>admin_assets/css/angular-js/common.css">
	<script src="<?=BASE_URL?>admin_assets/js/angular-js/angular-file-upload-shim.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.js"></script>		 
	<script src="<?=BASE_URL?>admin_assets/js/angular-js/angular-file-upload.js"></script>
	<script type="text/javascript">
	var uploadURL = '<?=SITE_URL?>/backstage_events/upload/';
	</script>
	<script src="<?=BASE_URL?>admin_assets/js/angular-js/upload.js"></script>

</head>
<body ng-app="fileUpload" ng-controller="MyCtrl">	


 <div class="upload-div">

 	<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" id="form">
 			<?php 
 			$models = json_encode(array('backstage_event_id'=>$this->input->get('backstage_event_id'))); 
 			?>
 			<input type="hidden" name="myModel" ng-model="myModel" value="<?='backstage_event_id='.$this->input->get('backstage_event_id')?>"/>

   
           <div class="form-group">
                <label class="col-sm-2 control-label"> Select Files:</label>
                <div class="col-sm-4">
                     <input type="file" ng-file-select="onFileSelect($files)" multiple>
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label"> Progress:</label>
                <div class="col-sm-4">
                     
                	<div ng-show="selectedFiles != null">
						<div class="sel-file" ng-repeat="f in selectedFiles">
							{{($index + 1) + '.'}}
							<img ng-show="dataUrls[$index]" ng-src="{{dataUrls[$index]}}">
							<button class="button" ng-click="start($index)" ng-show="progress[$index] < 0">Start</button>
							<span class="progress" ng-show="progress[$index] >= 0">						
								<div style="width:{{progress[$index]}}%">{{progress[$index]}}%</div>
							</span>				
							<button class="button" ng-click="abort($index)" ng-show="hasUploader($index) && progress[$index] < 100">Abort</button>
							{{f.name}} - size: {{f.size}}B - type: {{f.type}}
						</div>
					</div>

                </div>
           </div>
           <input type="radio" ng-model="httpMethod" ng-init="httpMethod = 'POST'" value="POST" style="visibility:hidden;"/>
		   <input type="radio" name="howToSend" ng-model="howToSend" value="1" ng-init="howToSend = 1" style="visibility:hidden;"> 		 
		   <input type="checkbox" ng-model="uploadRightAway" style="visibility:hidden;"> 

    </form>

		

	</div>
 </body>

</html>