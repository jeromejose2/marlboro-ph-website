
<script src="<?=BASE_URL?>admin_assets/js/angular-js/angular-file-upload-shim.js"></script>
<script src="<?=BASE_URL?>admin_assets/js/angular-js/angular.js"></script>
<script src="<?=BASE_URL?>admin_assets/js/angular-js/angular-file-upload.js"></script>
<script src="<?=BASE_URL?>admin_assets/js/angular-js/upload.js"></script>
 


<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <h4 class="modal-title"><?=$header_title?></h4>
</div>

<div class="modal-body text-center"  ng-controller="MyCtrl">

   <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" id="form">
    <input type="hidden" name="myModel" ng-model="myModel" value="1"/>
 
 
       <?php if($row->media_type=='video'){ ?>
       <div class="form-group">

            <div class="col-md-12">
              <div class="thumbnail"><?php 
                     $video_mime_type = get_mime_by_extension('uploads/backstage/photos/'.$row->media);  ?>
                        <video width="100%" height="100%" controls>
                            <source src="<?=BASE_URL?>uploads/backstage/photos/<?=$row->media?>" type="<?=$video_mime_type?>">
                          Your browser does not support the video tag. 
                        </video>
               </div>
            </div>
       </div>

       <div class="form-group">
            <label class="col-sm-4  control-label">Change Thumbnail:</label>
            <div class="col-sm-5">
                 <input type="file" ng-file-select="onFileSelect($files)">
            </div>
       </div>

       <input type="text" ng-model="uploadURL" ng-init="uploadURL='<?=SITE_URL.'/backstage_events/upload_media?backstage_event_id='.$backstage_event_id.'&photo_id='.$photo_id?>'" style="visibility:hidden;"> 


       <?php }else{ ?>


       <div class="form-group">
            <label class="col-sm-3 control-label"><?=$file_label?></label>
            <div class="col-sm-5">
                 <input type="file" ng-file-select="onFileSelect($files)">
            </div>
       </div>

      <input type="text" ng-model="uploadURL" ng-init="uploadURL='<?=SITE_URL.'/backstage_events/upload_media?backstage_event_id='.$backstage_event_id.'&photo_id='.$photo_id?>'" style="visibility:hidden;"> 



       <?php } ?>

       <div class="form-group preview" style="display:none">
           <div class="col-md-12" >

            <div class="row" ng-show="selectedFiles == null">

              <div class="col-md-12 col-xs-12">
                <div class="thumbnail">
                  <img src="<?=BASE_URL.'uploads/backstage/photos/thumbnails/331_176_'.$row->media_image_filename?>" style="max-height:250px">
                  <caption><?=$row->media_type=='video' ? 'Video Thumbnail' : ''?></caption>
                </div>
              </div>

            </div>

            <div class="row" ng-show="selectedFiles != null"> 

              <!-- End ng-start -->
              <div class="col-md-12 col-xs-12" ng-repeat="f in selectedFiles">
                <div class="thumbnail">
                  <img ng-show="dataUrls[$index]" ng-src="{{dataUrls[$index]}}">
                  <div class="caption">
                      <p class="text-danger">{{uploadError[$index]}} {{removeError[$index]}}</p>
                      <span class="progress progress-striped" ng-show="progress[$index] >= 0">           
                        <div class="progress-bar" style="width:{{progress[$index]}}%">{{progress[$index]}}%</div>
                      </span>
                      <div class="clearfix"></div>
                    <p>size: {{f.size}}B<br/>type: {{f.type}}</p>
                  </div>

                </div>

                <div class="{{$index + 1 % 4 == 0 ? 'clearfix' : ''}}"></div>
                 
              </div> 
              <!-- End ng-repeat -->
            </div> 

          </div>

       </div>

          

   </form>

</div>


<script>
 
    angular.element(document).ready(function() {
      angular.bootstrap(document, ['fileUpload']);
      $('.preview').show();
    });
 
</script>
  
     
      