<!DOCTYPE html>
<html>
<head>
     <title>Marlboro Content Management System</title>
     <!-- Bootstrap -->
     <link rel="stylesheet" href="<?php echo BASE_URL ?>admin_assets/css/bootstrap.min.css" >
     <link rel="stylesheet" href="<?php echo BASE_URL ?>admin_assets/css/style.css" >
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
     <![endif]-->
     <script src="<?php echo BASE_URL ?>admin_assets/js/jquery.js"></script>
     
    <!-- jquery ui -->
    <link href="<?php echo BASE_URL ?>plugins/jquery-ui/css/smoothness/jquery-ui-1.10.0.custom.css" rel="stylesheet">
    <script src="<?php echo BASE_URL ?>plugins/jquery-ui/js/jquery-1.9.0.js"></script>
    <script src="<?php echo BASE_URL ?>plugins/jquery-ui/js/jquery-ui-1.10.0.custom.js"></script>
    <!-- /jquery ui -->

    <link rel="apple-touch-icon" sizes="57x57" href="<?= BASE_URL ?>images/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= BASE_URL ?>images/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= BASE_URL ?>images/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= BASE_URL ?>images/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= BASE_URL ?>images/favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= BASE_URL ?>images/favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= BASE_URL ?>images/favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= BASE_URL ?>images/favicon/apple-touch-icon-152x152.png">
    <link rel="icon" type="image/png" href="<?= BASE_URL ?>images/favicon/favicon-196x196.png" sizes="196x196">
    <link rel="icon" type="image/png" href="<?= BASE_URL ?>images/favicon/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="<?= BASE_URL ?>images/favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="<?= BASE_URL ?>images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?= BASE_URL ?>images/favicon/favicon-16x16.png" sizes="16x16">
    
    <script>
		var siteUrl = '<?php echo SITE_URL ?>';
    </script>
     
</head>
<body>

    

     <?php echo $nav ?>
	 <?php echo $main_content ?>

     <!--start footer -->
     <footer class="main">
          <div class="container">
               <small>Philip Morris, Inc. &copy; 2013</small>
          </div>
     </footer>
     <!--end footer -->


     
     <script src="<?php echo BASE_URL ?>admin_assets/js/bootstrap.min.js"></script>
     <script src="<?php echo BASE_URL ?>admin_assets/js/main.js"></script>
     <script src="<?php echo BASE_URL ?>admin_assets/js/bootbox.min.js"></script>
</body>
</html>