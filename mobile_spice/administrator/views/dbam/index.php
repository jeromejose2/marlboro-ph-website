<!--start main content -->
<div class="container main-content">
	<div class="page-header">
		<h3>Dont Be A Maybe<span class="badge badge-important"><?= $total ?></span></h3>

		<div class="actions">
			<?php if ($access['add']): ?>
			<a href="<?= SITE_URL ?>/dbam/add" class="btn btn-primary">Add</a>
			<?php endif ?>
			<a href="<?= SITE_URL ?>/dbam/export?<?= http_build_query($this->input->get() ? $this->input->get() : array()) ?>" class="btn btn-primary">Export</a>
		</div>
	</div>
	 
	<table class="table table-bordered">
		<thead>
				<tr>
					<th>#</th>
					<th>Image</th>
					<th>Title</th>
					<th>Status</th>
					<th>Date Created</th>
					<th></th>
				</tr>
		</thead>
		<tbody>
			<form action="<?= SITE_URL ?>/dbam">
					<tr>
						<td></td>
						<td></td>
						<td><input name="title" placeholder="Title" class="form-control" value="<?= $this->input->get('title') ? $this->input->get('title') : '' ?>" /></td>
						<td>
						<select class="form-control" name="status">
							<option value=""></option>
							<option <?= (string) $this->input->get('status') == '1' ? 'selected="selected"' : '' ?> value="1">Published</option>
							<option <?= (string) $this->input->get('status') == '0' ? 'selected="selected"' : '' ?> value="0">Unpublished</option>
						</select>
						</td>
						<td></td>
						<td><button class="btn btn-primary" name="search" type="submit">Go</button></td>
					</tr>
				</form>
				<?php foreach ($records as $k => $record): ?>
				<tr>
						<td><?= $offset + $k + 1 ?></td>
						<?php if ($record->dbam_thumbnail): ?>
						<td><img class="img-thumbnail" src="<?= BASE_URL ?>uploads/dbam/150_150_<?= $record->dbam_thumbnail ?>"></td>
						<?php else: ?>
						<?php if ($record->dbam_type == 1): ?>
						<td><img class="img-thumbnail" src="<?= BASE_URL ?>uploads/dbam/150_150_<?= $record->dbam_content ?>"></td>
						<?php elseif ($record->dbam_type == 2): ?>
						<td><img class="img-thumbnail" src="<?= BASE_URL ?>uploads/dbam/150_150_thumb_<?= $record->dbam_content ?>.jpg"></td>
						<?php endif ?>
						<?php endif ?>
						<td><?= $record->dbam_title ?></td>
						<td><?= $record->dbam_status ? '<span class="label label-success">Published</span>' : '<span class="label label-warning">Unpublised</span>' ?></td>
                        <td><?= date('F d, Y l', strtotime($record->dbam_date_created)) ?></td>
						<td>
						<?php if ($access['edit']): ?>
						<a class="btn btn-primary" href="<?= SITE_URL ?>/dbam/edit?id=<?= $record->dbam_id ?>">Edit</a>&nbsp;&nbsp;
						<?php endif ?>
						<?php if ($access['delete']): ?>
						<a class="btn btn-danger brand-history-delete" href="javascript:void(0)" data-id="<?= $record->dbam_id ?>">Delete</a>&nbsp;&nbsp;
						<?php endif ?>
						<?php if (!$record->dbam_status): ?>
						<a class="btn change-status-btn btn-success" href="<?= SITE_URL ?>/dbam/change_status?id=<?= $record->dbam_id ?>&status=1">Publish</a>
						<?php else: ?>
						<a class="btn change-status-btn btn-warning" href="<?= SITE_URL ?>/dbam/change_status?id=<?= $record->dbam_id ?>&status=0">Unpublish</a>
						<?php endif ?>
						</td>
					</tr>
				<?php endforeach ?>
		</tbody>
	</table>

	<ul class="pagination pagination-sm pull-right">
		<?= !empty($pagination) ? $pagination : '' ?>
	</ul>

</div>
<!--end main content -->
<script>
	$(".change-status-btn").click( function(e) {
		var href = $(this).prop("href");
		bootbox.confirm("Are you sure?", function(confirm) {
			if (confirm) {
				window.location = href;
			}
		});
		e.preventDefault();
	});
	$(".brand-history-delete").click( function() {
		var id = $(this).data("id");
		bootbox.confirm("Are you sure?", function(confirm) {
			if (confirm) {
				window.location = "<?= SITE_URL ?>/dbam/delete?id=" + id;
			}
		});
	});
</script>