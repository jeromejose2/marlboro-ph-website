<div class="modal-header">
   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   <h4 class="modal-title"><?=$registrant ? ucwords($registrant->first_name.' '.$registrant->third_name).'\'s Logged In(s)' : ''?></h4>
  </div>

<div class="modal-body text-center">
   <table class="table table-bordered">
	   	<thead>
	        <tr>
	        	  <th>#</th>
 	              <th>Date & Time Logged In</th>                      
	             
	        </tr>
	    </thead>
	    <tbody>
	        <?php if($rows->num_rows()){
	        		$count = 0;
	        		foreach($rows->result() as $v){ $count++;?>
	        		<tr>
	        			<td align="left"><?=$count?></td>
  	        			<td align="left"><?=date('F d, Y H:i:s', strtotime($v->date_login))?></td>
	        		</tr>
	        <?php } ?>
	        <?php }else{ ?>
	        	<tr><td>No results found.</th></tr>
	    	<?php } ?>
	    </tbody>
	</table>
</div>