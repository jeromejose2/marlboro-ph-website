<?php $this->load->view('editor.php') ?>
</script>

<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3>MoveFWD Mechanics</h3>
      </div
       <div class="alert alert-danger" <?php if(!$error) echo 'style="display:none;"' ?>><?php echo $error; ?></div>
       <form class="form-horizontal" roe="form" method="post" onsubmit="return submitFrm()">
           <div class="form-group">
                <label class="col-sm-2 control-label">Mechanics</label>
                <div class="col-sm-8">
                     <textarea name="description"><?php echo $settings['description'] ?></textarea>
                </div>
           </div>
          <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <input type="hidden" name="submit" value="1" />
                     <input type="hidden" name="id" value="<?php echo isset($module['module_id']) ? $module['module_id'] : '' ?>" />
                     <button type="submit" id="submit" class="btn btn-primary">Update</button>
                </div>
           </div>
      </form>
      
 </div>
 <!--end main content -->
 