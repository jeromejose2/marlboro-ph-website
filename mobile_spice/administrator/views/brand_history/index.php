<!--start main content -->
<div class="container main-content">
	<div class="page-header">
		<h3>Brand History<span class="badge badge-important"><?= $total ?></span></h3>

		<div class="actions">
			<?php if ($access['add']): ?>
			<a href="<?= SITE_URL ?>/brand_history/add" class="btn btn-primary">Add</a>
			<?php endif ?>
			<a href="<?= SITE_URL ?>/brand_history/export?<?= http_build_query($this->input->get() ? $this->input->get() : array()) ?>" class="btn btn-primary">Export</a>
		</div>
	</div>
	 
	<table class="table table-bordered">
		<thead>
				<tr>
					<th>#</th>
					<th>Image</th>
					<th>Title</th>
					<th>Year</th>
					<th>Year Desc</th>
					<th>Description</th>
					<th>Status</th>
					<th>Date Created</th>
					<th></th>
				</tr>
		</thead>
		<tbody>
			<form action="<?= SITE_URL ?>/brand_history">
					<tr>
						<td></td>
						<td></td>
						<td><input name="title" placeholder="Title" class="form-control" value="<?= $this->input->get('title') ? $this->input->get('title') : '' ?>" /></td>
						<td></td>
						<td></td>
						<td></td>
						<td>
						<select class="form-control" name="status">
							<option value=""></option>
							<option <?= (string) $this->input->get('status') == '1' ? 'selected="selected"' : '' ?> value="1">Published</option>
							<option <?= (string) $this->input->get('status') == '0' ? 'selected="selected"' : '' ?> value="0">Unpublished</option>
						</select>
						</td>
						<td></td>
						<td><button class="btn btn-primary" name="search" type="submit">Go</button></td>
					</tr>
				</form>
				<?php foreach ($records as $k => $record): ?>
				<tr>
						<td><?= $offset + $k + 1 ?></td>
						<?php if ($record->brand_history_type == 1): ?>
						<td><img class="img-thumbnail" src="<?= BASE_URL ?>uploads/brand_history/150_150_<?= $record->brand_history_media ?>"></td>
						<?php elseif ($record->brand_history_type == 2): ?>
						<td><video width="200" height="200" controls>
								<source src="<?= BASE_URL ?>uploads/brand_history/<?= $record->brand_history_media ?>" type="video/mp4">
							Your browser does not support the video tag.
							</video></td>
						<?php else: ?>
						<td></td>
						<?php endif ?>
						<td><?= $record->brand_history_title_type == 1 ? $record->brand_history_title : '<img class="img-thumbnail" src="'.BASE_URL.'uploads/brand_history/150_150_'.$record->brand_history_title.'">' ?></td>
						<td><?= $record->brand_history_year ?></td>
						<td><?= $record->brand_history_year_desc ?></td>
						<td><?= $record->brand_history_description ?></td>
						<td><?= $record->brand_history_status ? '<span class="label label-success">Published</span>' : '<span class="label label-warning">Unpublished</span>' ?></td>
                        <td><?= date('F d, Y l', strtotime($record->brand_history_date_created)) ?></td>
						<td>
						<?php if ($access['edit']): ?>
						<a class="btn btn-primary" href="<?= SITE_URL ?>/brand_history/edit?id=<?= $record->brand_history_id ?>">Edit</a>&nbsp;&nbsp;
						<?php endif ?>
						<?php if ($access['delete']): ?>
						<a class="btn btn-danger brand-history-delete" href="javascript:void(0)" data-id="<?= $record->brand_history_id ?>">Delete</a>&nbsp;&nbsp;
						<?php endif ?>
						<?php if (!$record->brand_history_status): ?>
						<a class="btn change-status-btn btn-success" href="<?= SITE_URL ?>/brand_history/change_status?id=<?= $record->brand_history_id ?>&status=1">Publish</a>
						<?php else: ?>
						<a class="btn change-status-btn btn-warning" href="<?= SITE_URL ?>/brand_history/change_status?id=<?= $record->brand_history_id ?>&status=0">Unpublish</a>
						<?php endif ?>
						</td>
					</tr>
				<?php endforeach ?>
		</tbody>
	</table>

	<ul class="pagination pagination-sm pull-right">
		<?= !empty($pagination) ? $pagination : '' ?>
	</ul>

</div>
<!--end main content -->
<script>
	$(".change-status-btn").click( function(e) {
		var href = $(this).prop("href");
		bootbox.confirm("Are you sure?", function(confirm) {
			if (confirm) {
				window.location = href;
			}
		});
		e.preventDefault();
	});
	$(".brand-history-delete").click( function() {
		var id = $(this).data("id");
		bootbox.confirm("Are you sure?", function(confirm) {
			if (confirm) {
				window.location = "<?= SITE_URL ?>/brand_history/delete?id=" + id;
			}
		});
	});
</script>