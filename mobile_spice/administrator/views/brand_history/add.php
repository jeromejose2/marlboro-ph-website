<!--start main content -->
<?php $this->load->view('editor.php') ?>
 <div class="container main-content">
			<div class="page-header">
					 <h3>Add Brand History</h3>
		   <div class="actions">
			  <a href="<?= SITE_URL ?>/brand_history" class="btn btn-primary">Back</a>
		   </div>
			</div>
			<?php if (!$error): ?>
			<div class="alert alert-danger" style="display:none;"></div>
			<?php else: ?>
			<div class="alert alert-danger"><?= $error ?></div>
			<?php endif ?>
			 <form class="form-horizontal" role="form" method="post" action="<?= SITE_URL ?>/brand_history/add" onsubmit="return submitFrm()" enctype="multipart/form-data">
					 <div class="form-group">
								<label class="col-md-1 control-label">Title</label>
								<div class="col-md-4">
										 <input type="text" name="title" value="" id="title" data-name="title" class="required form-control">
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Year</label>
								<div class="col-md-4">
										 <input type="text" name="year" id="year" value="" data-name="year" class="required form-control" maxlength="4">
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Year Description</label>
								<div class="col-md-4">
										 <input type="text" name="year_desc" id="year-desc" value="" class="form-control">
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Description</label>
								<div class="col-md-4">
										 <textarea name="description" data-name="description" class="form-control"></textarea>
								</div>
					 </div>
					 <div class="form-group">
					 	<label class="col-md-1 control-label">Explore Label</label>
					 	<div class="col-md-4">
					 		<input type="text" class="form-control" name="brand_history_explore_copy">
					 	</div>
					 </div>
					 <div class="form-group">
					 	<label class="col-md-1 control-label">Explore Link</label>
					 	<div class="col-md-4">
					 		<input type="text" class="form-control" name="brand_history_explore_link">
					 	</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Media</label>
								<div class="col-md-4">
									<input type="file" id="brand-history-media" data-name="media" name="brand_history_upload" value="" class="form-control">
									 <br>
                     				<strong>Width: 375px. Height: 250px.</strong>
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Media Preview</label>
								<div class="col-md-4" id="brand-history-media-preview">
									
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">Status</label>
								<div class="col-md-4">
										<select class="form-control" name="status">
											<option value="0">Unpublished</option>
											<option value="1">Published</option>   
										</select>
								</div>
					 </div>
					 <div class="form-group">
						<div class="col-md-offset-1 col-md-4">
						 <button type="submit" class="btn btn-primary">Submit</button>
					 </div>
					</div>
					<!-- <input type="hidden" id="remove-files-index" name="remove-file" value=""> -->
			</form>
			
 </div>

 <script>
 	$("#year").bind('keypress', function (e) {
        return !(e.which != 8 && e.which != 0 &&
                (e.which < 48 || e.which > 57) && e.which != 46);
    });
 	
 	$("#brand-history-media").change( function() {
 		var file = $(this).prop("files")[0];
 		if (file.type.indexOf("image") == 0) {
 			var oFReader = new FileReader();
			oFReader.readAsDataURL(file);
			oFReader.onload = function (oFREvent) {
				$("#brand-history-media-preview").html("<img style='width: 483px; height: 262px;' class='img-thumbnail' src='" + oFREvent.target.result + "'>");
			};
 		}
 	});

 </script>