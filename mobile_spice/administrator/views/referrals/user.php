<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>User Referrals </h3>

               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/user_referrals/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Total Referrals</th>
                         <th>Succesful Referrals</th>
                         <th>Date</th>
                         <th></th>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/user_referrals">
                         <tr>
                            <td></td>
                             <td><input type="text" class="form-control" name="name" value="<?php echo isset($_GET['name']) ? $_GET['name'] : '' ?>"></div>
                            </td>
                            <td></td><td></td><td></td>
                            <td><button class="btn btn-primary"  type="submit" name="search" value="1">Go</button></td>
                        </tr>
                    </form>
                        
                    <?php if($users): foreach($users as $k => $v):  ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><a href="#" onclick="showRegistrantDetails(<?php echo $v['referer'] ?>)"><?php echo $v['first_name'] . ' ' . $v['third_name'] ?></a></td>
                         <td><a href="#" onclick="showReferrals(<?php echo $v['referer'] ?>,0)"><?php echo $v['referrals'] ?></a></td>
                         <td><a href="#" onclick="showReferrals(<?php echo $v['referer'] ?>,1)"><?php echo $v['verified'] ?></a></td>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['timestamp']))  ?></td>
                         <td></td>
                     </tr>
                    <?php 
					endforeach;
					else:
						echo '<tr><td colspan="5">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->