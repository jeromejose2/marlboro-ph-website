<!--start main content -->
     <div class="container main-content">
          
          <table class="table table-bordered">
               <thead>
               </thead>
               <tbody>
                    <?php if($data): ?> 

                    <thead>
                        <th>NAME</th>
                        <th>EMAIL</th>
                        <th>DATE SENT</th>
                        <th>STATUS</th>
                    </thead>
                    <?php foreach($data as $k => $v):  ?>
                    <tr>
                        <td><?=$v['name']?></td>
                        <td><?=$v['email']?></td>
                        <td><?=date('F d, Y H:i:s', strtotime($v['timestamp']))?></td>
                        <td><span <?=$v['status'] ? 'class="label label-success">successful' : 'class="label label-warning">pending'?></span></td>
                    </tr>
                    <?php endforeach; else: echo '<tr><td colspan="5">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>


     </div>
     <!--end main content -->