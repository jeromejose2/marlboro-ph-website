<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Web Analytics</h3>
		  </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>User</th>
                         <th>Activity</th>
                         <th>Date</th>
                    </tr>
               </thead>
               <tbody>
                    <?php if($users): 
					foreach($users as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['name'] ?></td>
                         <td><?php echo $v['description'] ?></td>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['timestamp'])) ?></td>
                    </tr>
                    <?php 
					endforeach;
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->