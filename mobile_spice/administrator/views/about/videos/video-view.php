
 
<?php 
if($row){ ?>

        <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         <h4 class="modal-title"><?=$row->title?></h4>
        </div>

        <div class="modal-body text-center">
        <?php if(file_exists('uploads/about/videos/'.@$row->video) && @$row->video){
                    $video_mime_type = get_mime_by_extension('uploads/about/videos/'.@$row->video);  ?>

                    <video id="video" controls="controls" width="100%" autoplay>
                      <source src="<?=BASE_URL.'uploads/about/videos/'.$row->video?>" type="<?=$video_mime_type?>" />
                      <script type="text/javascript">
                        function tryFlash(){
                          // flash fallback
                          jwplayer("video").setup({ flashplayer: "<?=BASE_URL?>scripts/jwplayer/player.swf",
                                      modes: [{ type: "html5" }, { type: "flash", src: "<?=BASE_URL?>scripts/jwplayer/player.swf", autostart:true}]});  
                        }
                        function initVideo(){
                          var video = document.getElementById("video");
                          // check if html5 video tag is supported if not fallback to flash
                          if(!video.play ? true : false)
                            tryFlash();
                        }
                        initVideo();
                      </script>
                    </video>           

          <?php }else if($youtube_embed){     
                 echo $youtube_embed;
             }

        } ?>
        </div>
 