<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>About User Photos<span class="badge badge-important"><?php echo (int)$total_rows; ?></span></h3>

               <div class="actions">
                     <a href="<?php echo SITE_URL.'/events/export_photos'.$query_strings; ?>" class="btn btn-primary" target="_blank">Export</a>
                </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                       <th><center>Photo</center></th>
                          <th>Event</th>
                         
                          <th>Submitted By</th>
                          <th>Status</th>
                          <th>Date Added</th>                       
                          <th>Operation</th> 
                         
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>">
                     <tr>
                        <th></th>
                        <th><select name="about_event_id" class="form-control" >
                            <option value=""></option>
                            <?php if($events->num_rows()){
                              foreach($events->result() as $e){
                                      if($this->input->get('about_event_id')==$e->about_event_id){ ?>
                                        <option value="<?=$e->about_event_id?>" selected><?=$e->title?></option>
                                <?php }else{ ?>
                                        <option value="<?=$e->about_event_id?>"><?=$e->title?></option>
                                <?php } 
                                 }
                            } ?>
                          </select></th>
                        <th><input type="text" class="form-control" name="submitted_by" value="<?=$this->input->get('submitted_by')?>"></th>
                        <th><select name="status" class="form-control">
                              <option value=""></option>
                              <option value="0" <?php echo ($this->input->get('status')===0) ? 'selected' : ''; ?> >Pending</option>
                              <option value="1" <?php echo ($this->input->get('status')==1) ? 'selected' : ''; ?> >Approved</option>
                              <option value="2" <?php echo ($this->input->get('status')==2) ? 'selected' : ''; ?> >Disapproved</option>                              
                            </select></th>
                       <th>
                       
                        <div class="row">
                                <div class="col-md-4">
                                     <input name="from_date_added" placeholder="From" class="form-control from" value="<?php echo isset($_GET['from_date_added']) ? $_GET['from_date_added'] : '' ?>" />
                                </div>
                                <div class="col-md-4">
                                     <input name="to_date_added" placeholder="To" class="form-control to" value="<?php echo isset($_GET['to_date_added']) ? $_GET['to_date_added'] : '' ?>" />
                                </div>
                              </div>
                        </th>         
                      <th><button type="submit" class="btn btn-primary">Go</button></th>                     
                     </tr>
                  </form>

                  <?php



                   if($rows->num_rows()){

                            $edit_url = SITE_URL.'/'.$this->uri->segment(1).'/edit/';
                            $status = array(0=>'Pending',1=>'Approved',2=>'Disapproved');
 
                            
 
                            foreach($rows->result() as $v){  ?>
                              <tr>
                                  
                                  <td><center><a href="<?=SITE_URL.'/event_photos/view_photo/'.$v->photo_id?>" title="Click to Full view" class="view-video"><img src="<?=BASE_URL.'uploads/about/photos/thumbnails/100_100_'.$v->image?>" /></a></center></td>
                                  <td><?=$v->event_title?></td>
                                  <td><?=$v->uploader_name?></td>
                                  <td><?=$status[$v->status]?></td>
                                  <td><?=$v->date_added?></td> 
                                                                
                                  <td>
                                    
                                   <?php if($edit){ ?>
                                   <?php if($v->status==1){?>
                                            <a href="<?php echo SITE_URL.'/event_photos/disapprove?photo_id='.$v->photo_id.'&about_event_id='.$v->about_event_id.'&token='.md5($v->photo_id. ' ' . $this->config->item('encryption_key')); ?>" class="btn btn-danger" onclick="return confirmAction(this,'disapprove');">Disapprove</a>
                                      <?php }else if($v->status==2){ ?>
                                             <a href="<?php echo SITE_URL.'/event_photos/approve?photo_id='.$v->photo_id.'&about_event_id='.$v->about_event_id.'&token='.md5($v->photo_id. ' ' . $this->config->item('encryption_key')); ?>" class="btn btn-success" onclick="return confirmAction(this,'approve');">Approve</a>
                                     <?php }else{ ?>
                                            <a href="<?php echo SITE_URL.'/event_photos/approve?photo_id='.$v->photo_id.'&about_event_id='.$v->about_event_id.'&token='.md5($v->photo_id. ' ' . $this->config->item('encryption_key')); ?>" class="btn btn-success" onclick="return confirmAction(this,'approve');">Approve</a>
                                            <a href="<?php echo SITE_URL.'/event_photos/disapprove?photo_id='.$v->photo_id.'&about_event_id='.$v->about_event_id.'&token='.md5($v->photo_id. ' ' . $this->config->item('encryption_key')); ?>" class="btn btn-danger" onclick="return confirmAction(this,'disapprove');">Disapprove</a>
                                    <?php } 
                                    } ?>
                                   </td>
                                   
                                
                              </tr>
                  <?php     } 
                        }else{ ?>
                          <tr><td colspan="6">No records found.</td></tr>
                  <?php  } ?>
                  
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->
     <div id="play-video" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>
            </div>
          </div> 
      </div>

     <script>
    

     $('.view-video').on('click',function(e){
          var href = $(this).attr('href');
          $('.modal-content').html('<div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>');
          $('#play-video').modal('show');
          $('.modal-content').load(href);
          e.preventDefault();
     }); 

     $('#play-video').on('hidden.bs.modal',function(){
        $('.modal-content').html('<img src="<?=BASE_URL?>images/spinner.gif"/></div>');
     });
     </script>