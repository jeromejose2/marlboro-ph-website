<!--start main content -->
   <div class="container main-content">

        <div class="page-header">
             <h3> <?php echo $header_text; ?> </h3>
             <div class="actions">
                  <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/export'.$query_strings; ?>" class="btn btn-primary">Export</a>   
             </div>
        </div>


        <ul class="nav nav-tabs">
            <li class="<?=$this->input->get('search') ? '' : 'active' ?>"><a href="#graph" id="graph-btn" data-toggle="tab">Graph</a></li>
            <li class="<?=$this->input->get('search') ? 'active' : '' ?>"><a href="#summary" id="summary-btn" data-toggle="tab">Summary</a></li>
          </ul>
         

        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane <?=$this->input->get('search') ? '' : 'active' ?>" id="graph">
                <div id="graph_1" style="border:1px solid #DDDDDD; border-top:none;height:300px;">
                     
                </div> 
          </div>

          <div class="tab-pane <?=$this->input->get('search') ? 'active' : '' ?>" id="summary">
               <form>
                <table class="table table-bordered" style="border-top:none;">
                     <thead>
                          <tr>
                               <th width="1%">#</th>
                               <th>Interest</th>
                               <th>Count</th>
                               <th>Percentage</th>
                               <td>Operation</td>
                          </tr>

                          <tr> 
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>From:<input name="from_date_added" class="form-control from" value="<?=$this->input->get('from_date_added')?>" /><br> 
                                  To:<input name="to_date_added" class="form-control to" value="<?=$this->input->get('to_date_added')?>" /><br>
                                  Sort By:
                                  <select name="sort_by" class="form-control from">
                                    <option value=""></option> 
                                    <option <?=$this->input->get('sort_by')=='interest' ? 'selected' : ''?> value="interest">Interest</option>
                                    <option <?=$this->input->get('sort_by')=='interest_count' ? 'selected' : ''?> value="interest_count">Count</option>
                                    <option <?=$this->input->get('sort_by')=='percentage' ? 'selected' : ''?> value="percentage">Percentage</option>
                                   </select><br>
                                  Sort Order:  
                                  <select name="sort_order" class="form-control from">
                                    <option <?=$this->input->get('sort_order')=='ASC' ? 'selected' : ''?>>ASC</option>
                                    <option <?=$this->input->get('sort_order')=='DESC' ? 'selected' : ''?>>DESC</option>
                                   </select><br>
                                  <input name="type" type="hidden" value="<?php echo $this->input->get('type') ? $this->input->get('type') : 'age' ?>" /><br><button type="submit" class="btn btn-primary" name="search" value="1">Go</button></td>
                             </tr>
                     </thead>
                     <body>
                      <?php if($rows->num_rows()){
                              foreach($rows->result() as $v){ ?>
                                <tr>
                                  <td></td>
                                  <td><?=ucwords($v->interest)?></td>
                                  <td><a href="<?=SITE_URL?>/profiling_graph/sub_interests<?=$query_strings?>&interest=<?=$v->interest_id?>" class="view"><?=$v->interest_count?></a></td>
                                  <td><?=round(($v->interest_count/$total_rows->num_rows()) * 100, 2)?>%</td>
                                  <td></td>
                                </tr>
                          <?php } 
                          }else{ ?>
                                <tr><td colspan="5">No results found.</th></tr>
                            <?php } ?>
                </table>
                </form
          </div>

        </div>

   </div>

   <div id="view-content" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>
            </div>
          </div> 
      </div>

   <script type="text/javascript" src="<?=BASE_URL?>admin_assets/js/google-api.js"></script>

     <script type="text/javascript">
     google.load("visualization", "1", {packages:["corechart"]});
     function drawGraph1() {
          var data = google.visualization.arrayToDataTable([
          ['Interest', 'Value'],
          
          <?php if($rows->num_rows()){
            foreach($rows->result() as $v){ ?>
               ["<?=ucwords($v->interest)?>",<?=round($v->interest_count,0)?>],
        <?php    }
          } ?>
          
         ]);
          
          var options = {
          title: '',
            is3D: true,
             sliceVisibilityThreshold: 0       
          };

        var chart = new google.visualization.PieChart(document.getElementById('graph_1'));
        chart.draw(data, options);

      }

      google.setOnLoadCallback(drawGraph1);
     $(window).resize(drawGraph1);


     $('.view').on('click',function(e){
          var href = $(this).attr('href');
          $('.modal-content').html('<div class="modal-body text-center"><img src="<?=BASE_URL?>images/spinner.gif"/></div>');
          $('#view-content').modal('show');
          $('.modal-content').load(href);
          e.preventDefault();
     });

     $('#view-content').on('hidden.bs.modal',function(){
        $('.modal-content').html('<img src="<?=BASE_URL?>images/spinner.gif"/></div>');
     });

    </script>