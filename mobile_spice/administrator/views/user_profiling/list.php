<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3> <?php echo $header_text; ?> <span class="badge badge-important"><?php echo (int)$total_rows; ?></span></h3>

               <div class="actions">
                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/export'.$query_strings; ?>" class="btn btn-primary">Export</a>   
               </div>
          </div>
          <form action="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>">
          <table class="table table-bordered">
               <thead>
                    <tr>
                          <th>Registrant</th>
                          <th>Interest</th>
                          <th>Sub Interest</th>
                          <th>Date Added</th>                          
                         <th><?php if($edit || $delete) : ?>Operation<?php endif; ?></th>
                         
                    </tr>
               </thead>
               <tbody>
               		 
                     <tr>
                        <th><input type="text" name="registrant_name" class="form-control" value="<?php echo $this->input->get('registrant_name'); ?>"></th>
                        <th>
                            <select name="interest" class="form-control" onchange="get_sub_interest(this.value)">
                                <option value=""></option>
                              <?php if($interest->num_rows()){
                                  foreach($interest->result() as $r){ ?>
                                    <option <?=$this->input->get('interest')==$r->category_id ? 'selected="selected"' : '' ?> value="<?=$r->category_id?>"><?=$r->category_name?></option>
                                  <?php }
                              } ?>
                            </select>
                        </th>
                        <th id="sub_interest_container"></th>
                      <th>From:<input name="from_date_added" class="form-control from" value="<?php echo $this->input->get('from_date_added'); ?>" /><br> 
                            To:<input name="to_date_added" class="form-control to" value="<?php echo $this->input->get('to_date_added'); ?>" /></th>                       
                        <th><?php if($edit || $delete) : ?><button type="submit" class="btn btn-primary">Go</button><?php endif; ?></th>
                       
                     </tr>
                 
                  <?php if($rows->num_rows()){

                            $edit_url = SITE_URL.'/'.$this->uri->segment(1).'/edit/';
 
                            foreach($rows->result() as $v){ ?>
                              <tr>
                                  <td><?php echo ucwords(strtolower($v->first_name.' '.$v->third_name)); ?></td>
                                  <td><?php echo $v->interest; ?></td>
                                  <td><?php echo $v->sub_interest; ?></td>
                                  <td><?php echo $v->date_added; ?></td>
                                  <td></td>                                
                              </tr>
                  <?php     }
                        }else{ ?>
                          <tr><td colspan="5" align="center">No records found.</td></tr>
                  <?php  } ?>
                  
               </tbody>
          </table>
        </form>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->
     <script type="text/javascript">
     <?php if($this->input->get('interest')){ ?>
            get_sub_interest('<?=$this->input->get("interest")?>');
     <?php }  ?>
     
      function get_sub_interest(v){

        var interest = v;


        $.ajax({url:'<?=SITE_URL?>/profiling/sub_interest',
                data:'interest='+interest,
                dataType:'JSON',
                success: function(data){                  

                  var _html = [], sub_interest = '<?=$this->input->get("sub_interest")?>',selected = '';
                   
                  $.each(data,function(k,v){
                      selected = sub_interest == v.category_id ? 'selected="selected"' : '';
                      _html.push('<option value="'+v.category_id+'" '+selected+' >'+v.category_name+'</option>');
                   });

                  if(_html.length > 0){

                    $('#sub_interest_container').html('<select class="form-control" name="sub_interest"><option value=""></option>'+_html.join('')+'</select>');;
                  
                  }else{

                    $('#sub_interest_container').html('');

                  }
                    


                }
              });
        
      }
     </script>
