<?php 
  $controller = $this->uri->segment(1);
?>
<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>MYM Files <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/add" class="btn btn-primary">Add File</a>
                    <?php endif; ?>
               </div>
          </div>
          <form class="form-horizontal" role="form" action="<?php echo SITE_URL ?>/<?php echo $controller ?>/upload" method="post"  enctype="multipart/form-data">
           <div class="form-group">
                <label class="col-sm-1 control-label" style="text-align:left;">MYM CSV File</label>
                <div class="col-sm-2">
                     <input type="file" name="csv" value="" data-name="module name" class="" >
                </div>
                <div class="col-sm-1">
                     <button class="btn btn-primary" name="search" type="submit" value="1">Submit</button>
                </div>
           </div>
          </form>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Date Created</th>
                        <!--  <th>Operation</th> -->
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/<?php echo $controller ?>">
                     <tr>
                         <td></td>
                         <td><input name="filename" class="form-control" value="<?php echo isset($_GET['filename']) ? $_GET['filename'] : '' ?>" /></td>
                         <td><div class="row">
                                <div class="col-md-4">
                                     <input name="fromdate" placeholder="From" class="form-control from" value="<?php echo isset($_GET['fromdate']) ? $_GET['fromdate'] : '' ?>" />
                                </div>
                                <div class="col-md-4">
                                     <input name="todate" placeholder="To" class="form-control to" value="<?php echo isset($_GET['todate']) ? $_GET['todate'] : '' ?>" />
                                </div>
                              </div><button class="btn btn-primary" name="search" value="1">Go</button></td>
                        <!--  <td></td> -->
                    </tr>
                    </form>
                    <?php if($categories): 
					foreach($categories as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['filename'] ?></td>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['date_created'])) ?></td>
                        <!--  <td>
                            <?php if($delete): ?>
                            <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/delete/<?php echo $v['dump_id'] ?>/<?php echo md5($v['dump_id'] . ' ' . $this->config->item('encryption_key')) ?>" onclick="return confirmDeletion(this, '<?php echo str_replace('_', ' ', $controller) ?>')" class="btn btn-danger">Delete</a>
                            <?php endif; ?>
                         </td> -->
                    </tr>
                    <?php 
					endforeach;
          else:
            echo '<tr><td colspan="7">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->