<?php 
  $controller = $this->uri->segment(1);
?>
<!--start main content -->
     <div class="container main-content">
          Success: <strong><?= count($success) ?></strong>
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>Date of Capture</th>
                         <th>First Name</th>
                         <th>Last Name</th>
                         <th>Email</th>
                         <th>Street</th>
                         <th>City</th>
                         <th>Postal Code</th>
                         <th>Country</th>
                         <th>Province</th>
                         <th>GIID</th>
                         <th>Current Brand</th>
                         <th>Secondary Brand</th>
                         <th>GIID Type</th>
                         <th>Status</th>
                         <th>Individual ID</th>
                         <th>Date Created</th>
                         
                        <!--  <th>Operation</th> -->
                    </tr>
               </thead>
               <tbody>
               		  <?php if($success): 
					foreach($success as $k => $v): ?>
                    <tr>
                         <td><?= $v['date_of_capture'] ?></td>
                         <td><?= $v['first_name'] ?></td>
                         <td><?= $v['third_name'] ?></td>
                         <td><?= $v['email_address'] ?></td>
                         <td><?= $v['street_name'] ?></td>
                         <td><?= $v['city'] ?></td>
                         <td><?= $v['postal_code'] ?></td> 
                         <td><?= $v['country'] ?></td> 
                         <td><?= $v['province'] ?></td> 
                         <td><?= $v['government_id_number'] ?></td> 
                         <td><?= $v['current_brand'] ?></td> 
                         <td><?= $v['first_alternate_brand'] ?></td> 
                         <td><?= $v['government_id_type'] ?></td> 
                         <td><?= $v['status'] ?></td> 
                         <td><?= $v['individual_id'] ?></td>
                         <td><?= $v['date_created'] ?></td>
                         
                    </tr>
                    <?php 
					endforeach;
          else:
            echo '<tr><td colspan="7">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>
          <br />
          Duplicates: <strong><?= count($duplicates) ?></strong>
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>Date of Capture</th>
                         <th>First Name</th>
                         <th>Last Name</th>
                         <th>Email</th>
                         <th>Street</th>
                         <th>City</th>
                         <th>Postal Code</th>
                         <th>Country</th>
                         <th>Province</th>
                         <th>GIID</th>
                         <th>Current Brand</th>
                         <th>Secondary Brand</th>
                         <th>GIID Type</th>
                         <th>Status</th>
                         <th>Individual ID</th>
                         <th>Date Created</th>
                        <!--  <th>Operation</th> -->
                    </tr>
               </thead>
               <tbody>
                    <?php if($duplicates): 
          foreach($duplicates as $k => $v): ?>
                    <tr>
                         <td><?= $v['date_of_capture'] ?></td>
                         <td><?= $v['first_name'] ?></td>
                         <td><?= $v['third_name'] ?></td>
                         <td><?= $v['email_address'] ?></td>
                         <td><?= $v['street_name'] ?></td>
                         <td><?= $v['city'] ?></td>
                         <td><?= $v['postal_code'] ?></td> 
                         <td><?= $v['country'] ?></td> 
                         <td><?= $v['province'] ?></td> 
                         <td><?= $v['government_id_number'] ?></td> 
                         <td><?= $v['current_brand'] ?></td> 
                         <td><?= $v['first_alternate_brand'] ?></td> 
                         <td><?= $v['government_id_type'] ?></td> 
                         <td></td> 
                         <td></td>
                         <td><?= $v['date_created'] ?></td>
                    </tr>
                    <?php 
          endforeach;
          else:
            echo '<tr><td colspan="7">No records found</td></tr>';
          endif; ?>
               </tbody>
          </table>
          <br />
          Failed: <strong><?= count($failed) ?></strong>
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>Date of Capture</th>
                         <th>First Name</th>
                         <th>Last Name</th>
                         <th>Email</th>
                         <th>Street</th>
                         <th>City</th>
                         <th>Postal Code</th>
                         <th>Country</th>
                         <th>Province</th>
                         <th>GIID</th>
                         <th>Current Brand</th>
                         <th>Secondary Brand</th>
                         <th>GIID Type</th>
                         <th>Status</th>
                         <th>Individual ID</th>
                         <th>Date Created</th>
                         <th>EDM Error</th>
                        <!--  <th>Operation</th> -->
                    </tr>
               </thead>
               <tbody>
                    <?php if($failed): 
          foreach($failed as $k => $v): ?>
                    <tr>
                         <td><?= $v['date_of_capture'] ?></td>
                         <td><?= $v['first_name'] ?></td>
                         <td><?= $v['third_name'] ?></td>
                         <td><?= $v['email_address'] ?></td>
                         <td><?= $v['street_name'] ?></td>
                         <td><?= $v['city'] ?></td>
                         <td><?= $v['postal_code'] ?></td> 
                         <td><?= $v['country'] ?></td> 
                         <td><?= $v['province'] ?></td> 
                         <td><?= $v['government_id_number'] ?></td> 
                         <td><?= $v['current_brand'] ?></td> 
                         <td><?= $v['first_alternate_brand'] ?></td> 
                         <td><?= $v['government_id_type'] ?></td> 
                         <td><?= $v['status'] ?></td> 
                         <td></td>
                         <td><?= $v['date_created'] ?></td>
                         <td><?= $v['edm_error_message'] ?></td> 
                    </tr>
                    <?php 
          endforeach;
          else:
            echo '<tr><td colspan="7">No records found</td></tr>';
          endif; ?>
               </tbody>
          </table>

   </div>
     <!--end main content -->