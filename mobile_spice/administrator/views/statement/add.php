<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3>Statement</h3>
      </div>
       <div class="alert alert-danger" <?php if(!$error) echo 'style="display:none;"' ?>><?php echo $error; ?></div>
       <form class="form-horizontal" role="form" method="post" onsubmit="return submitFrm()">
           <div class="form-group">
                <label class="col-sm-2 control-label">Statement</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo isset($statement['statement']) ? str_replace('"', '\'', $statement['statement']) : ''  ?>" name="statement" class="form-control required" data-name="statement">
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Status</label>
                <div class="col-sm-4">
                	<select class="form-control" name="status">
                     	<option value="1" <?php echo isset($statement['status']) && $statement['status'] == APPROVED ? 'selected' : '' ?>>Published</option>
                        <option value="2" <?php echo !isset($statement['status']) || (isset($statement['status']) && $statement['status'] == DISAPPROVED) ? 'selected' : '' ?>>Unpublished</option>
                    </select>
                </div>
           </div>
           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <input type="hidden" name="submit" value="1" />
                     <input type="hidden" name="id" value="<?php echo $this->uri->segment(3) ?>" />
                     <button type="submit" class="btn btn-primary">Submit</button>
                </div>
           </div>
           
           
      </form>
      
 </div>
 <!--end main content -->
 