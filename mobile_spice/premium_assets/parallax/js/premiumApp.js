function premiumApp() {
	var stage;
	var animation1;
	var animation2;
	var wait;
	var animationData = [];
	var scroll = { amount:0, frame:0};
	var imageWidth = 700;
	var imageHeight = 394;

	var canvas = document.getElementById("stage");
	var preloader = document.getElementById("preloader");
	var stageCon = document.getElementById("stage-container");
	var preloaderLength = 200;

	var currentCount = 0;
	var totalImageCount = 92;

	var currentAnim;
	
	loadScene1();
	function loadScene1() {
		loadImages("images/scene1/", 0, 93 - 1, function(event){
			currentCount++;
			preloader.style.width = ((currentCount/totalImageCount) * preloaderLength) + "px";

		},function(sprite){
		 	animation1 = sprite;

		 	//loadScene2();
		 	setTimeout(function(){
		 		preloader.style.display = "none";
		 	}, 100);
		 	
		}, 700, 394);

	}
	function loadScene2() {
		loadImages("images/scene2/", 92, 216 - 1, function(event){
			currentCount++;
			preloader.style.width = ((currentCount/totalImageCount) * preloaderLength) + "px";

		},function(sprite){
		 	animation2 = sprite;
		 	animation2.visible = false;
		 	
		 	
		}, 1024, 574 );
	}

	
	function loadImages(prefix, from, to, onTick, onComplete, width, height) {
		var loader = new createjs.LoadQueue();
		loader.on("fileload", handleFileLoad, this);
		loader.on("complete", handleComplete, this);

		for(var i = from; i <= to; ++i) {
			loader.loadManifest([{id: "animation" + i, src:prefix + i + ".jpg"}]);
		}
		function handleFileLoad(event) {
			onTick(event);
		}
		function handleComplete() {		
			animationData = [];
			for(var i = from; i <= to; ++i) {
				animationData.push(loader.getResult("animation" + i));
			}
			var data = {
			     images: animationData,
			     frames: {width:width, height:height},
			     animations: {animate:[from,to]}
			 };
			sprite = new createjs.Sprite(new createjs.SpriteSheet(data), "animate");
			stage.addChild(sprite);
			sprite.gotoAndStop(0);

			sprite.regX = width/2;
			sprite.regY = height/2;

			onComplete(sprite);
		}
	}
	createStage();
	function createStage() {
		stage = new createjs.Stage("stage");

		createjs.Touch.enable(stage);

		stage.on("mousedown", function(evt) {
			//console.log("owyea");

			oldPoint = { x:evt.stageX, y:evt.stageY};
		});
		stage.on("click", function(evt) {
			//console.log("up");
			//scroll.amount = 0;
			
		});
		
		var point;
		var oldPoint = {};
		var swipeAmount = 0;
		var distance = 0;
		stage.on("pressmove", function(evt) {
			
			point = { x:evt.stageX, y:evt.stageY};

			angle = Math.atan2(point.y - oldPoint.y, point.x - oldPoint.x);

			angle = angle * 180 / Math.PI;

			distance = lineDistance(point, oldPoint)/100;
			
			oldPoint = point;

			var mouseScroll;
			var dir;
			if(isNaN(distance))
				return;

			if(angle < 0) {
				// up
				scroll.amount += distance;
				dir = 1;
			} else {
				// down
				scroll.amount -= distance;
				dir = -1;


			}
			if(Math.abs(scroll.amount) < 0.002) {
				scroll.amount = 0;
				scroll.frame = scroll.frame + dir;
			} else {
				animate(scroll.amount * 40);
				scroll.amount = 0;
			}
		});
		
		createjs.Ticker.addEventListener("tick", handleTick);
		function handleTick(event) {
			updateScreen();

			try {
				setAnimationFrame();	
			}catch(e) {
				//console.log(e);
			}
			
			
			stage.update();
		}
		function setAnimationFrame() {
			if(!animation1)
				return;
			if(scroll.frame < 0)
				scroll.frame = 0;
			if(scroll.frame > totalImageCount)
				scroll.frame = totalImageCount;


			var currentFrame;
			if(scroll.frame > animation1.spriteSheet.getNumFrames("animate") - 1) {
				currentFrame = animation1.spriteSheet.getNumFrames("animate") - 1;
				return;

				animation1.visible = false;
				currentFrame = scroll.frame - 94;

				if(animation2) {
					animation2.visible = true;
					currentAnim = animation2;
				}

			} else {
				animation1.visible = true;
				currentAnim = animation1;
				currentFrame = scroll.frame;

				if(animation2) {
					animation2.visible = false;
				}
			}
			currentAnim.gotoAndStop(currentFrame);
			
		}

	}
	function lineDistance( point1, point2 )
	{
	  var xs = 0;
	  var ys = 0;

	  xs = point2.x - point1.x;
	  xs = xs * xs;

	  ys = point2.y - point1.y;
	  ys = ys * ys;

	  return Math.sqrt( xs + ys );
	}
	function animate(amount){

		var to = scroll.frame + (amount * 5);
		if(to > totalImageCount)
			to = totalImageCount;

		createjs.Tween.get(scroll, {override:true}).to( { frame: to }, 1000, createjs.Ease.sineOut);
	}

	//////////////////////////////////////////////////////
	//////////////////////////////////////////////////////

	document.body.addEventListener('DOMMouseScroll', onMouseWheel, false);
	document.body.onmousewheel = onMouseWheel;
	function onMouseWheel(event) {
		var mouseScroll;
		if (!event) 
            event = window.event;
		if(event.wheelDelta){
			mouseScroll = -event.wheelDelta / 120;
		}else if (event.detail) {
			mouseScroll  = event.detail / 3;
		}

		scroll.amount += mouseScroll;
		clearTimeout(wait);
		wait = setTimeout(function(){
			if(Math.abs(scroll.amount) != 1) {
				animate(scroll.amount);
			} else {
				scroll.frame = scroll.frame + mouseScroll;
			}
			scroll.amount = 0;
		}, 100);
	}
	function updateScreen() {
		//if(canvas.width == document.body.clientWidth)
		//	return;
		canvas.width = stageCon.clientWidth;
		canvas.height = stageCon.clientHeight;
		if(!animation1)
				return;
		animation1.scaleX = animation1.scaleY = canvas.width/imageWidth;

		animation1.x = canvas.width/2;
		animation1.y = canvas.height/2;

		if(!animation2)
				return;
		animation2.scaleX = animation2.scaleY = canvas.width/1024;

		animation2.x = canvas.width/2;
		animation2.y = canvas.height/2;
	}
}
function initPremiumApp() {
	premiumApp();
}