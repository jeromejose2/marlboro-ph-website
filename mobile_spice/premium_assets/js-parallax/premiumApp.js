function premiumApp() {
	var stage;
	var animation1;
	var animation2;
	var returnSite;
	var discover;

	var wait;
	var animationData = [];
	var scroll = { amount:0, frame:0};

	var maxScroll = 20;
	
	var scrollDown = document.getElementById("scroll-container");
	var canvas = document.getElementById("stage");
	var preloader = document.getElementById("preloader");
	var preloaderCon = document.getElementById("preloader-container");
	var stageCon = document.getElementById("stage-container");
	var preloaderLength = 150;

	var currentCount = 0;
	var totalImageCount = 325;
	var partImageCount = Math.floor(totalImageCount * .7);
	var partImageCount2 = Math.floor(totalImageCount * .3);

	var currentAnim;
	
	unSelectable(scrollDown, false);
	unSelectable(stageCon, false);
	unSelectable(preloaderCon, false);

	var baseDesktop = "premium_assets/images/1024x576_70/";
	var widthDesktop = 1024;
	var heightDeskTop = 576;

	var baseTablet = "premium_assets/images/700x394_72/";
	var widthTablet = 700;
	var heightTablet = 394;

	var baseMobile = "premium_assets/images/515x290_72/";
	var widthMobile = 512;
	var heightMobile = 290;

	var base;
	var imageWidth;
	var imageHeight;

	var isMobile = false;
	var isReady = false;

	//console.log(baseUrl);
	setup();
	function setup() {
		if(stageCon.clientWidth <= 767) {
			isMobile = true;

			base = baseMobile;
			imageWidth = widthMobile;
			imageHeight = heightMobile;
		} else if(stageCon.clientWidth >= 768 && stageCon.clientWidth <= 1024) {
			base = baseTablet;
			imageWidth = widthTablet;
			imageHeight = heightTablet;

		} else {
			base = baseDesktop;
			imageWidth = widthDesktop;
			imageHeight = heightDeskTop;
		}
		preloaderCon.style.marginTop = (stageCon.clientHeight/2) - 48 + "px";

		loadPart1();
	}
	function loadPart1() {
		loadImages(baseUrl + base, 0, partImageCount - 1, function(event){
			currentCount++;
			preloader.style.width = ((currentCount/partImageCount) * preloaderLength) + "px";
		},function(sprite){
		 	preloaderCon.style.display = "none";
	 		animation1 = sprite;
	 		animation1.visible = true;
	 		currentCount = 0;
	 		loadPart2();

	 		isReady = true;
		}, imageWidth, imageHeight);

	}
	function loadPart2() {
		

		loadImages(baseUrl + base, partImageCount, totalImageCount - 1, function(event){
			currentCount++;
			preloader.style.width = ((currentCount/partImageCount2) * preloaderLength) + "px";
		},function(sprite){
		 	animation2 = sprite;
		 	animation2.visible = false;

		 	loadButtons();

		}, imageWidth, imageHeight );
	}
	function loadButtons() {
		//var loader = new createjs.LoadQueue();
		//loader.on("fileload", handleFileLoad, this);
		//loader.on("complete", handleComplete, this);

		//loader.loadManifest([{id: "discover-normal", src:baseUrl + "premium_assets/images/premium-btns/discover-normal.png"}]);
		///loader.loadManifest([{id: "discover-over", src:baseUrl + "premium_assets/images/premium-btns/discover-over.png"}]);
		//loader.loadManifest([{id: "discover-upon", src:baseUrl + "premium_assets/images/premium-btns/discover-upon.png"}]);
		
		//loader.loadManifest([{id: "return-normal", src:baseUrl + "premium_assets/images/premium-btns/return-normal.png"}]);
		//loader.loadManifest([{id: "return-over", src:baseUrl + "premium_assets/images/premium-btns/return-over.png"}]);
		//loader.loadManifest([{id: "return-upon", src:baseUrl + "premium_assets/images/premium-btns/return-upon.png"}]);

		discover = new createjs.Container();
		stage.addChild(discover);


		var discoverNormal = new createjs.Bitmap(baseUrl + "premium_assets/images/premium-btns/discover-normal.png");
		discover.addChild(discoverNormal);
		//discoverNormal.x = 2;
		var discoverOver = new createjs.Bitmap(baseUrl + "premium_assets/images/premium-btns/discover-over.png");
		discover.addChild(discoverOver);
		discoverOver.visible = false;
		var discoverUpon = new createjs.Bitmap(baseUrl + "premium_assets/images/premium-btns/discover-upon.png");
		discover.addChild(discoverUpon);
		//discoverUpon.x = 1;
		discoverUpon.visible = false;
		

		discover.regX = 236/2;
		discover.regY = 27/2;



		discover.on("mousedown", function(evt) {
			discoverNormal.visible = false;
			discoverOver.visible = false;
			discoverUpon.visible = true;
			setTimeout(function(){
				window.location = baseUrl + "premium/find-out-more";
			}, 200)
		});
		discover.on("mouseover", function(evt) {
			discoverNormal.visible = false;
			discoverOver.visible = true;
			discoverUpon.visible = false;
		});
		discover.on("mouseout", function(evt) {
			discoverNormal.visible = true;
			discoverOver.visible = false;
			discoverUpon.visible = false;
		});
		discover.on("click", function(evt) {
			
		});


		returnSite = new createjs.Container();
		stage.addChild(returnSite);


		var returnSiteNormal = new createjs.Bitmap(baseUrl + "premium_assets/images/premium-btns/return-normal.png");
		returnSite.addChild(returnSiteNormal);
		//discoverNormal.x = 2;
		var returnSiteOver = new createjs.Bitmap(baseUrl + "premium_assets/images/premium-btns/return-over.png");
		returnSite.addChild(returnSiteOver);
		returnSiteOver.visible = false;
		var returnSiteUpon = new createjs.Bitmap(baseUrl + "premium_assets/images/premium-btns/return-upon.png");
		returnSite.addChild(returnSiteUpon);
		//discoverUpon.x = 1;
		returnSiteUpon.visible = false;
		

		returnSite.regX = 236/2;
		returnSite.regY = 27/2;

		returnSite.on("mousedown", function(evt) {
			returnSiteNormal.visible = false;
			returnSiteOver.visible = false;
			returnSiteUpon.visible = true;
			setTimeout(function(){
				window.location = baseUrl;
			}, 200)
		});
		returnSite.on("mouseover", function(evt) {
			returnSiteNormal.visible = false;
			returnSiteOver.visible = true;
			returnSiteUpon.visible = false;
		});
		returnSite.on("mouseout", function(evt) {
			returnSiteNormal.visible = true;
			returnSiteOver.visible = false;
			returnSiteUpon.visible = false;
		});
		returnSite.on("click", function(evt) {
			
		});

	}
	function loadImages(prefix, from, to, onTick, onComplete, width, height) {
		var loader = new createjs.LoadQueue();
		loader.on("fileload", handleFileLoad, this);
		loader.on("complete", handleComplete, this);

		for(var i = from; i <= to; ++i) {
			loader.loadManifest([{id: "animation" + i, src:prefix + i + ".jpg"}]);
		}
		function handleFileLoad(event) {
			onTick(event);
		}
		function handleComplete() {		
			animationData = [];
			for(var i = from; i <= to; ++i) {
				animationData.push(loader.getResult("animation" + i));
			}
			var data = {
			     images: animationData,
			     frames: {width:width, height:height},
			     animations: {animate:[from,to]}
			 };
			sprite = new createjs.Sprite(new createjs.SpriteSheet(data), "animate");
			sprite.visible = false;
			stage.addChild(sprite);
			sprite.gotoAndStop(0);

			sprite.regX = width/2;
			sprite.regY = height/2;

			onComplete(sprite);
		}
	}
	
	var point;
	var oldPoint = {};
	var distance = 0;
	var swipeAmount = 0;
	function pressReset() {
		distance = 0;
		point = {};
		oldPoint = {};
	}
	createStage();
	function createStage() {
		stage = new createjs.Stage("stage");
		stage.enableMouseOver(10);
		createjs.Touch.enable(stage);

		stage.on("mousedown", function(evt) {
			//console.log("owyea");

			oldPoint = { x:evt.stageX, y:evt.stageY};
		});
		stage.on("click", function(evt) {
			//alert("up");
			//scroll.amount = 0;
			
		});

		stage.on("pressmove", function(evt) {
			
			point = { x:evt.stageX, y:evt.stageY};

			angle = Math.atan2(point.y - oldPoint.y, point.x - oldPoint.x);

			angle = angle * 180 / Math.PI;

			distance = lineDistance({ x:oldPoint.x, y:point.y }, oldPoint)/100;
			
			oldPoint = point;

			var mouseScroll;
			var dir;
			if(isNaN(distance))
				return;
			if(angle < 0) {
				// up
				scroll.amount += distance;
				dir = 0.5;
			} else {
				// down
				scroll.amount -= distance;
				dir = -0.5;
			}

			if(Math.abs(scroll.amount) < 0.05) {
				scroll.amount = 0;
				scroll.frame = scroll.frame + dir;
			} else {
				animate(scroll.amount * 100);
				scroll.amount = 0;
			}
		});

		createjs.Ticker.addEventListener("tick", handleTick);
	}
	function handleTick(event) {
		if(!isReady)
			return;
		try {
			showHint();
			updateScreen();
			setAnimationFrame();	
			stage.update();
		} catch(e) {

		}
	}
	var oldScrollFrame;
	var isFullscreen = false;
	var hint;
	function showHint() {
		if(!isMobile) {
			if(scroll.frame <= 0) {
				scrollDown.style.display = "block";
			} else {
				scrollDown.style.display = "none";
			}
		}
		
		if(Math.abs(oldScrollFrame - scroll.frame) < 1){
			clearTimeout(hint);
			return;
		}
		if(!isFullscreen && oldScrollFrame < scroll.frame) {
			pressReset();
			
			hint = setTimeout(function(){
				if(!isFullscreen) {
					Fullscreen.enter();
					isFullscreen = true;	
					console.log("fullScreen enter");
				}
			}, 500);
		}
		if(isFullscreen && oldScrollFrame > scroll.frame) {
			pressReset();
			
			hint = setTimeout(function(){
				if(isFullscreen) {
					Fullscreen.exit();
					isFullscreen = false;	
					console.log("fullscreen exit");
				}
			}, 500);
		}
	}
	function showBtn(visibility) {
		if(returnSite) {
			returnSite.visible = visibility;
		}
		if(discover) {
			discover.visible = visibility;
		}
	}
	function checkFrame() {
		if(!animation1)
			return;
		if(scroll.frame < 0)
			scroll.frame = 0;
		if(scroll.frame > totalImageCount - 1) {
			scroll.frame = totalImageCount - 1;

			if(isFullscreen) {
				pressReset();
				Fullscreen.exit();
				isFullscreen = false;	
				console.log("fullScreen exit");
			}
		}
		if(scroll.frame > totalImageCount - 10 && scroll.frame < totalImageCount) {
			showBtn(true);
		} else {
			showBtn(false);
		}
	}
	function setAnimationFrame() {
		//scroll.frame = totalImageCount;
		checkFrame();

		//console.log(scroll.frame);
		var currentFrame;
		if(scroll.frame > partImageCount - 1) {
			//currentFrame = partImageCount - 1;

			animation1.visible = false;
			currentFrame = scroll.frame - partImageCount;

			if(animation2) {
				animation2.visible = true;
				currentAnim = animation2;
				preloaderCon.style.display = "none";
			} else {
				scroll.frame = partImageCount;
				preloaderCon.style.display = "block";
			}
		} else {
			preloaderCon.style.display = "none";
			animation1.visible = true;
			currentAnim = animation1;
			currentFrame = scroll.frame;

			if(animation2) {
				animation2.visible = false;
			}
		}
		oldScrollFrame = scroll.frame;

		currentAnim.gotoAndStop(currentFrame);
		
	}
	function lineDistance( point1, point2 )
	{
	  var xs = 0;
	  var ys = 0;

	  xs = point2.x - point1.x;
	  xs = xs * xs;

	  ys = point2.y - point1.y;
	  ys = ys * ys;

	  return Math.sqrt( xs + ys );
	}
	function animate(amount){
		if(Math.abs(amount) > maxScroll)
			amount = maxScroll * (amount/Math.abs(amount));

		var to = scroll.frame + (amount);
		if(to > totalImageCount)
			to = totalImageCount;
		createjs.Tween.get(scroll, {override:true}).to( { frame: to }, 1500, createjs.Ease.sineOut);
	}

	//////////////////////////////////////////////////////
	//////////////////////////////////////////////////////

	document.body.onkeydown = onKeyDown;
	function onKeyDown(e){
		var unicode= e.keyCode ? e.keyCode : e.charCode
		var scrollDir;
		if(unicode == 38) {
			scrollDir = -0.5;
			//scroll.amount += scrollDir;
			scroll.frame += scrollDir;
			onScroll(scrollDir, 0.5)
		} else if(unicode == 40) {
			scrollDir = 0.5;
			//scroll.amount += scrollDir;
			scroll.frame += scrollDir;
			onScroll(scrollDir, 0.5)
		}
		
	}
	document.body.addEventListener('DOMMouseScroll', onMouseWheel, false);
	document.body.onmousewheel = onMouseWheel;
	function onMouseWheel(event) {
		var mouseScroll;
		if (!event) 
            event = window.event;
		if(event.wheelDelta){
			mouseScroll = -event.wheelDelta / 120;
		}else if (event.detail) {
			mouseScroll  = event.detail / 3;
		}
		scroll.frame += mouseScroll;
		onScroll(mouseScroll, 5)

	}
	function onScroll(mouseScroll, slideAmount) {
		scroll.amount += mouseScroll;
		clearTimeout(wait);
		createjs.Tween.removeTweens(scroll);
		wait = setTimeout(function(){
			if(Math.abs(scroll.amount) != 1) {
				animate(scroll.amount * slideAmount);
			} else {
				//scroll.frame += mouseScroll;
				
			}
			scroll.amount = 0;
		}, 100);
	}
	function updateScreen() {
		updateAnimation();
		updateBtn();
	}
	function updateAnimation() {
		var scaleTarget;
		
		if(canvas.height/imageHeight < canvas.width/imageWidth) {
			scaleTarget = canvas.height/imageHeight;
		} else {
			scaleTarget = canvas.width/imageWidth;
		}
		canvas.width = stageCon.clientWidth;
		canvas.height = stageCon.clientHeight;

		if(!animation1)
				return;
		animation1.scaleX = animation1.scaleY = scaleTarget;

		animation1.x = canvas.width/2;
		animation1.y = canvas.height/2;

		if(!animation2)
				return;
		animation2.scaleX = animation2.scaleY = scaleTarget;

		animation2.x = canvas.width/2;
		animation2.y = canvas.height/2;
	}
	function updateBtn() {
		var btnScaleTarget;

		if(canvas.height/576 < canvas.width/1024) {
			btnScaleTarget = canvas.height/576;
		} else {
			btnScaleTarget = canvas.width/1024;
		}
		//console.log(btnScaleTarget);
		btnScaleTarget *= 2;
		if(returnSite) {
			returnSite.scaleX = returnSite.scaleY = btnScaleTarget;
			returnSite.x = canvas.width/2 + (100 * btnScaleTarget);
			returnSite.y = canvas.height/2 + (60 * btnScaleTarget);
		}
		if(discover) {
			discover.scaleX = discover.scaleY = btnScaleTarget;
			discover.x = canvas.width/2 + (100 * btnScaleTarget);
			discover.y = canvas.height/2;
		}
	}
	function unSelectable(target, e){
		if (typeof target.onselectstart!="undefined")
			target.onselectstart=function(){return e}
		else if (typeof target.style.MozUserSelect!="undefined")
			target.style.MozUserSelect="none"
		else 
			target.onmousedown=function(){return e}
	}
}
function initPremiumApp() {
	premiumApp();

}