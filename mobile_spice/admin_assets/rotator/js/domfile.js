Customizer = function(container) {
	//html
	var htmlwrapper = $(container).append(
		"<div id='img-container-rotator' style='position: absolute; top: -10000px; visibility: hidden; z-index: -1000;'>"+
			"<canvas id='h5uExtension' width='200' height='30'></canvas>"+
			"<button class='tleft' type='button'>LEFT</button>"+
			"<button class='tright' type='button'>RIGHT</button>"+
			"<button class='tsubmit' type='button'>SUBMIT</button>"+
			"</div>"
	);

	var newImage;
	var stage;
	var bmpWidth;
	var bmpHeight;
	var delay = 0;
	var bmp;
	var orientation = 'not_set';
	var currRot = 0;
	var currOrient = '';
	
	var imgCont = new createjs.Container();
	var canvas = document.getElementById('h5uExtension');
		stage = new createjs.Stage(canvas);
		stage.addChild(imgCont);

	this.setImage = function( value ){
		newImage = new Image();
		newImage.onload = self.onSourceReady;
		newImage.src = value;
	}

	this.onSourceReady = function(){
		bmp       = new createjs.Bitmap(newImage);
		bmpWidth  = bmp.getBounds().width;
		bmpHeight = bmp.getBounds().height;

		if( bmpWidth > bmpHeight ){
			orientation = 'landscape';
		} else {
			orientation = 'portrait';
			self.updateOrientation();
		}

		stage.canvas.width = bmpWidth;
		stage.canvas.height = bmpHeight;
		delay = setTimeout( self.updateStage, 100 );
		console.log( bmpWidth, bmpHeight, orientation );
	}

	this.updateStage = function(){
		console.log('adjustment complete');
		clearTimeout(delay);
		self.updateBitmap();
		self.addbtn();
	}

	this.updateBitmap = function(){
		imgCont.addChild(bmp);
		imgCont.regX = imgCont.getBounds().width/2;
		imgCont.regY = imgCont.getBounds().height/2;
		imgCont.x = canvas.width/2;
		imgCont.y = canvas.height/2;
		stage.update();
	}

	this.updateOrientation = function(){
		if( orientation == 'portrait' ){
				stage.canvas.width = bmpWidth;
				stage.canvas.height = bmpHeight;
				htmlwrapper.find('#h5uExtension').width(bmpWidth);
				htmlwrapper.find('#h5uExtension').height(bmpHeight);
				orientation = 'landscape';
		} else {
			stage.canvas.width = bmpHeight;
			stage.canvas.height = bmpWidth;
			htmlwrapper.find('#h5uExtension').width(bmpHeight);
			htmlwrapper.find('#h5uExtension').height(bmpWidth);
			orientation = 'portrait';
		}
	}

	this.rotateLeft = function () {
		imgCont.rotation-= 90;
		self.updateOrientation();
		self.updateBitmap();
		stage.update();
	}

	this.rotateRight = function () {
		imgCont.rotation+= 90;
		self.updateOrientation();
		self.updateBitmap();
		stage.update();
	}

	this.submit = function () {
		if (typeof self.submitCallback != "undefined") {
			self.submitCallback(canvas);
		}
	}

	this.addbtn = function() {
		htmlwrapper.find('.tleft').on("mouseup",function( evt ){
			self.rotateLeft();
		});
		htmlwrapper.find('.tright').on("mouseup",function( evt ){
			self.rotateRight();
		});
		htmlwrapper.find('.tsubmit').on("mouseup",function( evt ){
			self.submit();
		});
	}
	var self = this;
}