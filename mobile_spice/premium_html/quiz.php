<?php 
	include 'header.php';
?>
<link rel="stylesheet" type="text/css" href="css/animate.css">
<!-- main content starts here  -->
<section class="main">
	<div class="wrapper quiz">	
		<div class="container">
				
				<!-- <div class="page-ctrl">
					<button class="prev disable" data-next="0">&lt;</button>
					<button class="next"  data-next="2">&gt;</button>
				</div> -->

				<div class="premium-quiz">
						
					<div class="quiz quiz-start">
						<!-- <h3 class="visible-xs">WHAT TYPE OF PREMIUM ARE YOU?</h3>
						<img src="images/what-type-of-premium-are-you.png" class="hidden-xs m-auto">

						<p>Take our quiz and find out what your premium personality is.</p> -->
						<h3>Take our quiz and find out what your premium personality is.</h3>
						<p>WHAT TYPE OF PREMIUM ARE YOU?</p>
						<div class="line-separator"></div>

						<button class="btn"><i>START</i></button>

						<div class="bg-start"></div>
					</div>


					<div class="quiz  quiz-01">
						<span class="question">1. You sit down at a restaurant, and the first thing you look for on the menu is...</span>
						<ul class="answers">
							<li><span>The most prime cut of steak</span></li>
							<li><span> The freshest sushi</span></li>
							<li><span> The classic pizza</span></li>
						</ul>
					</div>

					<div class="quiz  quiz-02">
						<span class="question">2. You hit the town on a night out with your friends. Where do you end up in?</span>
						<ul class="answers">
							<li><span> A posh club rubbing elbows with the elite</span></li>
							<li><span> At a bar with a beer in hand and watching live bands perform</span></li>
							<li><span> At the movies, followed by a quiet nightcap</span></li>
						</ul>
					</div>

					<div class="quiz  quiz-03">
						<span class="question">3. You're on an airplane on your way to a holiday. Where are you seated?</span>
						<ul class="answers">
							<li><span> No seating assignments. I have the whole private jet to myself</span></li>
							<li><span> Business class, where there is ample leg room</span></li>
							<li><span> Economy. I enjoy complementary peanuts</span></li>
						</ul>
					</div>

					<div class="quiz  quiz-04">
						<span class="question">4.  A long weekend is coming. You'll most likely end up...</span>
						<ul class="answers">
							<li><span> Partying all day in Ibiza, Spain</span></li>
							<li><span> At a jam-packed music festival singing your heart out</span></li>
							<li><span> Biking and hiking in the mountains</span></li>
						</ul>
					</div>

					<div class="quiz  quiz-05">
						<span class="question">5. What best describes your fashion style?</span>
						<ul class="answers">
							<li><span> Designer dreams</span></li>
							<li><span> Casual charm</span></li>
							<li><span> Comfy couture</span></li>
						</ul>
					</div>

					<div class="quiz  quiz-06">
						<span class="question">6. CHEERS! What are you holding in your hand?</span>
						<ul class="answers">
							<li><span> A glass of finest wine</span></li>
							<li><span> Whiskey on the rocks</span></li>
							<li><span> A pint of Irish ale</span></li>
						</ul>
					</div>

					<div class="quiz  result">

						<div class="row">
							<div class="col-sm-5 col-sm-push-7">
								<img src="images/quiz-result-watch.jpg" class="img-responsive">
								<br>
							</div>
							<div class="col-sm-7 col-sm-pull-5 txtleft">
								<span class="question">SIMPLE AND SHARP</span>
								<p>You are at the top of your game, and you expect the same from everyone and everything around you. 
									While down-to-earth and easygoing, you know how to indulge and you appreciate the fine details. 
									This is why Marlboro Premium Black is best for you: With a pack specially designed by Pininfarina, 
									the makers of Ferrari, a pro-fresh seal to preserve freshness, and the flavor that comes from the 
									blend of the finest tobaccos, Marlboro Premium Black gives you perfection in every detail.
								</p>
								<a href="#" class="btn"><i>FIND PREMIUM BLACK</i></a>
							</div>
						</div>
					
					</div>

				</div>
				
		</div>

	</div>

</section>
<!-- main content ends here  -->


<script>
	var Quiz = new PremiumQuiz();
	Quiz.init();


function PremiumQuiz(){
	
	var thisClass = this;
	var page = 1;
	var el = {
		answers : $('.answers li'),
		prev : $('.page-ctrl .prev'),
		next : $('.page-ctrl .next'),
		start : $('.quiz-start .btn'),
		quiz : $('.premium-quiz'),
		quizes : $('.premium-quiz .quiz'),
	}

	this.init = function(){

		el.next.click(function(){
			thisClass.navigate('next');
			
		});

		el.prev.click(function(){
			thisClass.navigate('prev');
		});

		el.answers.click(function(){
			var me = $(this);
			me.addClass('selected');
			me.siblings().removeClass('selected');

			setTimeout(function(){thisClass.navigate('next')},300)
		});

		el.start.click(function(){
			thisClass.navigate('next');
		})
	}

	this.navigate = function(move){
			
		if(move=='next'){
			el.prev.removeClass('disable');

			if(page==el.quizes.length-1){
				el.next.addClass('disable');
			}
				
			if(page==el.quizes.length)
				return;
		}

		if(move=='prev'){
			el.next.removeClass('disable');

			if(page==2){
				el.prev.addClass('disable');
			}
				
			if(page==1) return;
		}

		el.quiz.children(':nth-child('+page+')').removeClass('show animated fadeIn').addClass('hidden');
			
		page = (move == 'next') ? page + 1 : page - 1 ;

		el.quiz.children(':nth-child('+page+')').addClass('show animated fadeIn').removeClass('hidden');
	}

}
</script>
<?php include 'footer.php';?>

</body>
</html>
