<!DOCTYPE html>
<html lang="en">
<head>
	<title>Marlboro</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet/less" type="text/css" href="css/style-base.less?r=<?=rand()?>">
	<link rel="stylesheet" type="text/css" href="css/lytebox.css">
	<script src="js/less.js"></script>
	<script src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/lytebox.2.2.js"></script>
	<body>
	<header class="main">
		<div class="container">
			<a class="logo" href="index.php"><span class="sr-only">MARLBORO LOGO</span></a>
			<button class="navbar-toggle"  data-toggle="collapse" data-target="#nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="collapse navbar-collapse" id="nav-collapse">
				<ul class="main-nav nav navbar-nav ">
					<li><a href="index.php">HOME</a></li>
					<li><a href="findout.php">FIND OUT MORE</a></li>
					<li><a href="quiz.php">QUIZ</a></li>
					<li><a href="findpremium.php">FIND PREMIUM BLACK</a></li>
					<li><a href="#">RETURN TO MAIN SITE</a></li>
				</ul>
			</div>
			
		</div>
		
	</header>		