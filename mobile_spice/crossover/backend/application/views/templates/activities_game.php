<!-- PLAY THE GAME -->
<section class="section teaserGame fullscreen">
	<div class="background" style="background-image:url(<?=ASSETS_URL?>img/alaska/activities_game.jpg)"></div>
	
	<div class="pageName">
		<span class="icon"></span>
	</div>

	<div class="title">	
		<span class="ico"></span>
		<div class="stroked" data-i18n="activities.game.subtitle"></div>

		<h3><span data-i18n="[html]activities.game.title"></span></h3>

		<p>
			<span data-i18n="[html]activities.game.teaser"></span>
		</p>

		<button data-i18n="activities.game.playBtn"></button>
	</div>

</section>