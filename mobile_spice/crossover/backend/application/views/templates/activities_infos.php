<!-- EXPLORE YOUR CHOICE -->
<section class="section details info">
	<div class="pageName text">
		<a href="#act_360">
			<span data-i18n="activities.explore.pageNameLeft"></span>
			<span class="icon"></span>
			<span data-i18n="activities.explore.pageNameRight"></span>
		</a>
	</div>

	<?php if($rows): $i = 1; foreach($rows as $k => $v): ?>
		<?php if($i % 2): ?>
			<div class="line">
				<div class="picture" style="background-image:url(<?=$v->media?>)"></div>
				<div class="contentTxt">
					<h3><?=$v->media_title?></h3>
					<p class="subtitle"><?=$v->media_sub_title?></p>
					<div><?=$v->media_description?></div>
				</div>
			</div>
		<?php else: ?>
			<div class="line">						
				<div class="contentTxt">
					<h3><?=$v->media_title?></h3>
					<p class="subtitle"><?=$v->media_sub_title?></p>
					<div><?=$v->media_description?></div>
				</div>
				<div class="picture" style="background-image:url(<?=$v->media?>)"></div>
			</div>
		<?php endif; ?>
	<?php $i++; endforeach; endif; ?>
	
	<?php /*<div class="line">
		<div class="picture">
			<!-- button to call a video -->
			<button style="background-image:url(<?=ASSETS_URL?>img/alaska/explorations/exploration1_video.jpg)" data-video="red">
				<span data-i18n="[html]alaska.one.explorations.thermal.video"></span>
				<span class="time" data-i18n="[html]alaska.one.explorations.thermal.videoTime"></span>
			</button>
		</div>
		<div class="contentTxt">
			<h3 data-i18n="[html]alaska.one.explorations.thermal.title"></h3>
			<p class="subtitle" data-i18n="[html]alaska.one.explorations.thermal.subtitle"></p>
			<div data-i18n="[html]alaska.one.explorations.thermal.detail"></div>
		</div>
	</div>

	<div class="line">						
		<div class="contentTxt">
			<h3 data-i18n="[html]alaska.one.explorations.prepare.title"></h3>
			<p class="subtitle" data-i18n="[html]alaska.one.explorations.thermal.subtitle"></p>
			<div data-i18n="[html]alaska.one.explorations.thermal.detail"></div>
		</div>
		<div class="picture" style="background-image:url(<?=ASSETS_URL?>img/alaska/explorations/exploration2.jpg)"></div>
	</div>

	<div class="line">
		<div class="picture" style="background-image:url(img/alaska/explorations/exploration3.jpg)"></div>
		<div class="contentTxt">
			<h3 data-i18n="[html]alaska.one.explorations.sea.title"></h3>
			<p class="subtitle" data-i18n="[html]alaska.one.explorations.thermal.subtitle"></p>
			<div data-i18n="[html]alaska.one.explorations.thermal.detail"></div>
		</div>
	</div> */ ?>

</section>