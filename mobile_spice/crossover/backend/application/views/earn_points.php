<div class="wrapper earnpoints fixed">
	<div class="holder-earnpoints">
		<h1 class="page">EARN POINTS</h1>
		<p>Earn points as you discover how far making decisions can take you</p>
		
		<div class="row">

			<div class="col-xs-7 col-sm-8 txtright webgames">
				<h3>CHALLENGE YOURSELF WITH OUR FUN WEB GAMES</h3>
			</div>
			<div class="col-xs-5 col-sm-4 webgames">
				<a href="<?= BASE_URL ?>games" class="button"><i>PLAY NOW</i></a>
			</div>
			<div class="clearfix"></div>

			<div class="col-xs-7 col-sm-8 txtright">
				<h3>SHARE YOUR EXPERIENCES BY POSTING COMMENTS AND UPLOADING PHOTOS</h3>
			</div>
			<div class="col-xs-5 col-sm-4">
				<a href="<?= BASE_URL ?>move_forward" class="button"><i>MoveFWD</i></a>
			</div>
			<div class="clearfix"></div>

			<!-- <div class="col-xs-7 col-sm-8 txtright">
				<h3>INVITE YOUR FRIENDS AND INCREASE YOUR CHANCES OF WINNING</h3>
			</div>
			<div class="col-xs-5 col-sm-4">
				<a href="<?= BASE_URL ?>refer" class="button"><i>REFER A FRIEND</i></a>
			</div> -->
			<div class="col-xs-7 col-sm-8 txtright">
				<h3>WATCH OUR VIDEOS TO LEARN HOW TO CROSS "MAYBE" OUT OF YOUR LIFE</h3>
			</div>
			<div class="col-xs-5 col-sm-4">
				<a href="<?= BASE_URL ?>about/dont-be-a-maybe" class="button"><i>WATCH VIDEOS</i></a>
			</div>
			<div class="clearfix"></div>

		</div>

		<p>Visit the website everyday to keep earning points and reward yourself.</p>
	</div>
</div>
