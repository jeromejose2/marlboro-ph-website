<div class="wrapper fixed">
	<div class="container">
		<div class="leather-content">
			<form autocomplete="off" class="content" id="reset-form" action="<?= BASE_URL ?>user/reset/<?= $token ?>" method="POST">
				<h2>RESET PASSWORD</h2>
				<p class="uc"></p>
				<div id="error"><?= $invalid_forgot_token ? '<span>Already expired</span>' :  $message ?></div>
				<?php if (!$invalid_forgot_token): ?>
				<input autocomplete="off" type="password" id="new-password" name="new_password" title="Enter your new password" placeholder="New Password" class="req">
				<br>
				<input autocomplete="off" type="password" id="confirm-password" name="confirm_password" title="Password doesn't match" placeholder="Confirm Password" class="req">				
				<br>
				<button type="submit" class="button"><i>SUBMIT</i></button>
				<?php endif ?>
			</div>
		</div>
	</div>
	<br class="clearfix">
</div>
<script type="text/javascript">
	$("#reset-form").submit( function(e) {
		var newPassword = $("#new-password").val();
		var confirmPassword = $("#confirm-password").val();
		var error = $("#error");
		if (!newPassword) {
			error.html("<span>Please enter your new password</span>");
			return false;
		} else if (!confirmPassword || newPassword != confirmPassword) {
			error.html("<span>Password doesn't match</span>");
			return false;
		} else if (newPassword.length < 8) {
			error.html("<span>Password must be 8 characters or more than</span>");
			return false;
		}
	});
</script>