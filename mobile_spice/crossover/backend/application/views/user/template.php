<!DOCTYPE html>
<html lang="en">
<head>
	<title>Marlboro</title>
	<meta name="viewport" content="width=device-width">
	<link href='http://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>assets/css/lytebox.css">
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>plugins\jquery-ui\css\smoothness\jquery-ui-1.10.0.custom.css">
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>assets/css/style.min.css">
	<script type="text/javascript" src="<?= BASE_URL ?>assets/js/jquery-1.11.2.min.js"></script>
	
	<script>
		var BASE_URL = "<?= BASE_URL ?>";
	</script>
</head>
<body>
		<section class="main">
			<?= $main_content ?>
		</section>

		<footer>
			<div class="footnote">
				<small>This website is for adult smokers 18 years or older residing in the Philippines.
					Access to this website is subject to age verification </small>

					<nav>
						<a onclick="popup.load({url:'<?= BASE_URL ?>assets/popups/terms.html'})">Terms and Conditions</a>
					 	<a onclick="popup.load({url:'<?= BASE_URL ?>assets/popups/privacy-content.html'})">Privacy Statement and Consent</a> 
					 	<a onclick="popup.load({url:'<?= BASE_URL ?>assets/popups/contactus.html'})">Contact Us</a>
					 	<a target="_blank" href="http://www.pmi.com/eng/our_products/health_effects_of_smoking/pages/health_effects_of_smoking.aspx">Smoking and Health</a>
					</nav>

					<small>Copyright &copy; 2015 PMFTC INC. All rights reserved</small>
				</div>
			</div>
			<div class="container">
					<h1>GOVERNMENT WARNING: CIGARETTE SMOKING IS DANGEROUS TO YOUR HEALTH</h1>
			</div>
		</footer>


<!-- Latest compiled and minified JavaScript -->
<script src="<?= BASE_URL ?>plugins/jquery-ui/js/jquery-ui-1.10.0.custom.min.js"></script>

<script src="<?= BASE_URL ?>assets/js/less.js"></script>
<script src="<?= BASE_URL ?>assets/js/lytebox.2.3.js"></script>
<script src="<?= BASE_URL ?>assets/js/app.js"></script>
<script src="<?= BASE_URL ?>assets/js/jquery.slimscroll.min.js"></script>
<script src="<?= BASE_URL ?>assets/swf/flash.js"></script>
<script src="<?= BASE_URL ?>assets/swf/swfobject.js"></script>

<script src="<?= BASE_URL ?>scripts/bootstrap.min.js"></script>
<script src="<?= BASE_URL ?>scripts/exec.js"></script>
<script src="<?= BASE_URL ?>scripts/registration.js"></script>

<script src="<?= BASE_URL ?>swf/registration-webcam.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="<?= BASE_URL ?>scripts/shiv.js"></script>
  <script src="<?= BASE_URL ?>scripts/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
	function validate(form){
		var error = false;
		$('.req',form).each(function(){
			var el = $(this);
			var val = $.trim(el.val());
			var typ = el.attr('type');
			var name = el.attr('name');
			if( val == '' ){
				error = 'Please input your '+name;
				return false;
			}

			if( typ == 'checkbox'){
				if( !el.is(':checked') ){
					error = 'You need to tick both statements in order to log in';
					return false;
				}
			}
		});
		if( error ){
			$('#error').html('<span>'+error+'</span>');
			$('html, body').animate({scrollTop:0})
			return false;
		}
	}
</script>
</body>
</html>
