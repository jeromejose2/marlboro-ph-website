<div class="wrapper registration">
	<div class="container">
		<div class="leather-content">
			<form autocomplete="off" onsubmit="return submit_form()" class="content" method="POST">
				<h2>UPLOAD GIID</h2>
				<p class="uc">ENTER YOUR E-MAIL TO UPLOAD YOUR GIID DOCUMENT</p>
				<div id="error"></div>
				<input autocomplete="off" type="text" name="email" placeholder="E-MAIL" class="req">
				<br>
				<a href="<?= BASE_URL ?>user/login" class="small">Back to login page</a> <br><br>
				<button type="submit"  class="button"><i>SUBMIT</i></button>
			</form>
			</div>
		</div>
	</div>
	<br class="clearfix">
</div>
<script type="text/javascript">


function submit_form(){

	popup.loading();

	$.ajax({
	  type: "POST",
	  url: BASE_URL + "user/submit_track_code",
	  data: {email:$('input[name="email"]').val()},
	  dataType: 'json',
	  success: function(response){
 
			popup.close();

			$('input[name="email"]').val('');
 
			if(response.error==1){

				$('#error').html("<span>"+response.message+"</span>");
 
			}else{
				
				popup.open({
						title : "Success",
						align : "center",
						message : response.message,
						type: "alert",
						buttonAlign : 'center',					 
						onClose : function(){

							window.location = BASE_URL + "user/login";

						}
					});

			}
	

		}
	 
	});

	 
	return false;

}
 
</script>