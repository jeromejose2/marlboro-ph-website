<!DOCTYPE html>
<html>
<head>
	<title>Error</title>
	<link rel="shortcut icon" href="<?= BASE_URL ?>error_assets/images/favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="<?= BASE_URL ?>error_assets/css/style.css">
	<script>
		window.location = "<?= BASE_URL ?>user/login";
	</script>
</head>
<body>
	<header><div></div></header>

	<section>
		<h2>This site requires Javascript</h2>
	</section>


	<footer>
		<div></div>
		<span>GOVERNMENT WARNING: CIGARETTE SMOKING IS DANGEROUS TO YOUR HEALTH</span>
	</footer>

</body>
</html>