<html ng-app="spiceModule">
<head>
	<title>Failed Request Logs</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
	<script src="<?=BASE_URL?>admin_assets/js/angular-js/angular.js"></script>
</head>


<body>

	<div class="container" ng-controller="spiceController">
		<h2>Failed Request Logs</h2><br/><br/>
 
		<form role="form-inline">
			<div class="row">
				<div class="col-md-4">
  		    		<input type="text" class="form-control" placeholder="Search" ng-model="query.$">
  		    	</div>
  			</div>
 		</form>

		

		<table class="table table-striped">
			<thead>
				<tr>
 					<th>Process</th>
					<th>WebMethod</th>
					<th>Response Message</th>
					<th>DateTime</th>
					<th><span class="text-primary">Input</span> & <span class="text-danger">Response</span></th>
 				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="item in items | filter:query">
 					<td>{{item.transaction}}</td>
					<td>{{item.method}}</td>
					<td>{{item.response_message}}</td>
					<td>{{item.date_created}}</th>
					<td><p class="text-primary">{{item.input}}</p><p class="text-danger">{{item.response}}</p></td>
 				</tr>
 				
 				<tr ng-show="!items">
 					<td colspan="4" align="center"><img src="../images/preloader.gif" /></td>
 				</tr>
			</tbody>
		</table>

	</div>

	
	<script type="text/javascript">

	var app = angular.module('spiceModule',[]);

	app.controller('spiceController',['$scope','$http',function($scope,$http){

		$scope.items = false;

		$scope.loadItems = function(){		

			$http.get('<?=BASE_URL?>spice_api/getErrorlogs').success(function(response){
				$scope.items = response;				 
 			});
		}
		
		$scope.loadItems();

	}]);

	</script>


</body>

</html>