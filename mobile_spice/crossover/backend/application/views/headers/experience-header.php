<div class="mainWrapper scrollable">		
	
	<!-- HEADER -->
	<header>
		<div class="container">
			
			<!-- Main navigation -->
			<ul class="nav">
				<li class="home">
					<a href="index.html">
						<img src="<?=ASSETS_URL?>img/crossover.png" alt="Malboro - Cross|Over" />
					</a>
				</li>
				<li>
					<a href="index.html" data-i18n="header.links.home"></a>
				</li>
				<li>
					<a href="map.html" data-i18n="header.links.map"></a>
				</li>
				<li>
					<a href="gifts.html" data-i18n="header.links.gifts"></a>
				</li>
				<li>
					<a href="events.html" data-i18n="header.links.events"></a>
				</li>
				<li>
					<a href="howTo.html" data-i18n="header.links.howTo"></a>
				</li>
				<!-- activities links call a new panel of menu: Cfr below sub-menu .activities  -->
				<li class="sliderActivities active">
					<a href="#" data-i18n="header.links.activities"></a>
				</li>

				<!-- data user + Call modal Profile-->
				<li class="navProfile">
					<ul>
						<!-- open modal Profile + add focus on input addCode -->
						<li class="addCode">
							<p><strong class="collected_km_points"><?= $collected_km_points ?></strong>km</p>
							<p><a href="#" class="addCodeTo" data-i18n="header.addCode"></a></p>				
						</li>
						<!-- open modal Profile  -->
						<li class="profileBtn">
							<a href="#" data-toggle="modal" data-target="#myProfileInfo">
								<span class="border">
									<img src="<?=ASSETS_URL?>img/user.png" alt"user name" />
								</span>
								<span class="arrowDown"></span>
							</a>
						</li>
					</ul>
				</li>
			</ul>	
			
		</div>

		<!-- sub-menu: display a slider with all the activities -->
		<div class="activities">
			<div class="slider">
				<?php if($experience): foreach($experience as $k => $v): ?>
					<!-- Need to condition if region is already unlocked -->
						<div class="<?= (int)$collected_km_points <= (int)$v->km_points ? 'locked' : 'checked' ?>">
							<a href="activities.html">
								<div class="illu">
									<img src="<?=$v->media?>">
								</div>
								<div>
									<span class="icon bicycle"></span>
									<h4><?=$v->title?></h4>
									<span class="location"><span data-i18n="california.surname"></span> / <span data-i18n="california.location"></span></span>
								</div>
							</a>
						</div>
					<!-- Need to condition if region is already unlocked -->
				<?php endforeach; endif; ?>
				<div class="checked">
					<a href="activities.html">
						<div class="illu">
							<img data-i18n="[src]california.one.sliderImg" />
						</div>
						<div>
							<span class="icon bicycle"></span>
							<h4 data-i18n="california.one.name"></h4>
							<span class="location"><span data-i18n="california.surname"></span> / <span data-i18n="california.location"></span></span>
						</div>
					</a>
				</div>
				<div class="checked">
					<a href="activities.html">
						<div class="illu">
							<img data-i18n="[src]california.two.sliderImg" />
						</div>
						<div>
							<span class="icon bicycle"></span>
							<h4 data-i18n="california.two.name"></h4>
							<span class="location"><span data-i18n="california.surname"></span> / <span data-i18n="california.location"></span></span>
						</div>
					</a>
				</div>
				<div>
					<a href="activities.html">
						<div class="illu">
							<img data-i18n="[src]california.three.sliderImg" />
						</div>
						<div>
							<span class="icon bicycle"></span>
							<h4 data-i18n="california.three.name"></h4>
							<span class="location"><span data-i18n="california.surname"></span> / <span data-i18n="california.location"></span></span>
						</div>
					</a>
				</div>
				<div class="locked">
					<a href="activities.html">
						<div class="illu">
							<img data-i18n="[src]alaska.one.sliderImg" />
						</div>
						<div>
							<span class="icon bicycle"></span>
							<h4 data-i18n="canada.one.name"></h4>
							<span class="location"><span data-i18n="canada.surname"></span> / <span data-i18n="canada.location"></span></span>
						</div>
					</a>
				</div>
				<div class="locked">
					<a href="activities.html">
						<div class="illu">
							<img data-i18n="[src]alaska.two.sliderImg" />
						</div>
						<div>
							<span class="icon bicycle"></span>
							<h4 data-i18n="canada.two.name"></h4>
							<span class="location"><span data-i18n="canada.surname"></span> / <span data-i18n="canada.location"></span></span>
						</div>
					</a>
				</div>
				<div class="locked">
					<a href="activities.html">
						<div class="illu">
							<img data-i18n="[src]alaska.three.sliderImg" />
						</div>
						<div>
							<span class="icon bicycle"></span>
							<h4 data-i18n="canada.three.name"></h4>
							<span class="location"><span data-i18n="canada.surname"></span> / <span data-i18n="canada.location"></span></span>
						</div>
					</a>
				</div>
				<div class="locked">
					<a href="activities.html">
						<div class="illu">
							<img data-i18n="[src]alaska.one.sliderImg" />
						</div>
						<div>
							<span class="icon bicycle"></span>
							<h4 data-i18n="alaska.one.name"></h4>
							<span class="location"><span data-i18n="alaska.surname"></span> / <span data-i18n="alaska.location"></span></span>
						</div>
					</a>
				</div>
				<div class="locked">
					<a href="activities.html">
						<div class="illu">
							<img data-i18n="[src]alaska.two.sliderImg" />
						</div>
						<div>
							<span class="icon bicycle"></span>
							<h4 data-i18n="alaska.two.name"></h4>
							<span class="location"><span data-i18n="alaska.surname"></span> / <span data-i18n="alaska.location"></span></span>
						</div>
					</a>
				</div>
				<div class="locked">
					<a href="activities.html">
						<div class="illu">
							<img data-i18n="[src]alaska.three.sliderImg" />
						</div>
						<div>
							<span class="icon bicycle"></span>
							<h4 data-i18n="alaska.three.name"></h4>
							<span class="location"><span data-i18n="alaska.surname"></span> / <span data-i18n="alaska.location"></span></span>
						</div>
					</a>
				</div>
				
			</div>
		</div>
		
	</header>

<!-- MODAL NAV PROFILE -->
	<div class="modal fade profile" id="myProfileInfo">
		<div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label"=Close"><span aria-hidden="true">&times;</span></button>
			    </div>
			    <div class="modal-body">
			      	<div class="inputAddCode">
			      		<form method="POST" action="#" onsubmit="return validateCode()">
			      			<!-- input text to add code -->
			      			<div class="inputContainer">		      			
				      			<input type="text" name="personalCodes" placeholder="Your code here" value="Your code here" maxlength="8" >		      	
				      			<button type="submit" disabled></button>		
				      		</div>	      		
			
							<!-- message to display if ...  -->
							<div class="notifMsg" style="display: none">
								<!-- the code is correct -->
								<div class="congrats" style="display: none">
									<span></span> 
									<strong></strong>
								</div>
								<!-- the code is wrong -->
								<div class="error" style="display: none">
									<span></span> 
									<strong></strong>
								</div>
							</div>
			      		</form>	
			      	</div><!-- end .inputAddCode-->

			      	<!-- data user (KM) -->
			        <div class="dataKm">
			        	<div class="col-md-6 col-sm-6">
			        		<span><strong class="collected_km_points"><?= $collected_km_points ?></strong>&nbsp;km</span>
			        		<p data-i18n="modalProfile.counterCollect"></p>
			        	</div>
			        	<div class="col-md-6 col-sm-6">
			        		<span><strong class="credit_km_points"><?= $credit_km_points ?></strong>km</span>
			        		<p data-i18n="modalProfile.counterCredit"></p>
			        	</div>
			        	<div class="clearfix"></div>
			        </div>
					
					<!-- list Link -->
			        <ul class="listLink">
			        	<li><a href="" data-i18n="[html]modalProfile.history"></a></li>
			        	<li><a href="" data-i18n="[html]modalProfile.gifts"></a></li>
			        </ul>

			    </div>
			    <div class="modal-footer">
			      	<a href="profile.htlm" class="btn arrowed" data-i18n="modalProfile.goProfile"></a>
			    </div>
		    </div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->