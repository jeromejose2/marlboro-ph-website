<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Gifts extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
 	}

 	public function index()
 	{
 		$data = array();
 		$this->load->view('gifts', $data);
 	}

 	public function content()
 	{
 		$data = array();
 		$this->load->view('content/gifts', $data);
 	}
 
}