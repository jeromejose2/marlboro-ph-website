<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Activities extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
 	}

 	public function index()
 	{
 		$data = array();
 		$this->header_model->get_header();
 		$data['region'] = $this->header_model->get_region($this->uri->segment(2));

 		// $this->header_model->get_experience_header();
 		$this->load->view('activities', $data);
 	}

 	public function content()
 	{
 		$experience_id = $this->uri->segment(3);
 		if($experience_id) {
 			$data['row'] = $this->db->select('*, tbl_experience.title as experience_title')->from('tbl_experience')->where('tbl_experience.experience_id', $experience_id)->join('tbl_activity', 'tbl_activity.experience_id = tbl_experience.experience_id')->get()->row();
 			$data['region'] = $data['row']->category_id;
 		}

 		$this->header_model->get_experience_header();
 		$this->load->view('content/activities', $data);
 	}

 	public function info()
 	{
 		if($this->input->is_ajax_request()) {
 			$data['rows'] = $this->db->select()->from('tbl_activity_media')->where('decision_title', $this->input->get('choice'))->get()->result();

 			$this->load->view('templates/activities_infos', $data);	
 		} 		
 	}

 	public function gifts()
 	{
 		if($this->input->is_ajax_request()) {
 			$data['rows'] = $this->db->select()->from('tbl_gift')->where('region', $_GET['region'])->order_by('order', 'ASC')->get()->result();
	 		$data['next'] = $this->db->select()->from('tbl_experience')->limit(1)->where('experience_id >', $this->input->get('id'))->get()->row();
	 		
	 		$this->load->view('templates/activities_gifts', $data);	
 		} 		
 	}

 	public function game()
 	{
 		$data = array();
 		$this->load->view('templates/activities_game', $data);
 	}

 	public function _remap($method)
 	{
 		if(is_numeric($method)) {
 			$this->index();
 		} else {
 			$this->$method();
 		}
 	}

}