<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


require APPPATH.'/libraries/REST_Controller.php';


class Api extends REST_Controller {


	public function experience_post(){ 
 		
 		$_POST = $this->post();

		$rules = array(
			 
			array(
				 'field'   => 'category_id',
				 'label'   => 'Category',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'title',
				 'label'   => 'Title',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'km_points',
				 'label'   => 'Kilometer Points',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'media',
				 'label'   => 'Media Content',
				 'rules'   => 'required'
			  )

		);

		if($this->validate_form($rules)) { 
			
			$data = $this->post();
			
			if(isset($data['experience_id'])){

				$data['set'] = array('date_updated'=>'NOW()');
				$this->global_model->update('experience',$data,array('experience_id'=>$data['experience_id']));

			}else{

				$data['set'] = array('date_created'=>'NOW()');
				$this->global_model->insert('experience',$data);

			}
    		
    		$response = array('success'=>1,'message'=>'Sucess');

		} else {

			$response = array('success'=>0,'message'=>validation_errors());

		}

	   $this->response($response, 200);
 
 
	}

	public function activity_post(){ 
 		
 		$_POST = $this->post();

		$rules = array(
			 
			 
		   array(
				 'field'   => 'title',
				 'label'   => 'Title',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'description',
				 'label'   => 'Description',
				 'rules'   => 'required'
			  )

		);

		if($this->validate_form($rules)) { 
			
			$data = $this->post();
			$medias = $this->post('medias');
			
			if(isset($data['activity_id'])){

				$data['set'] = array('date_updated'=>'NOW()');
				$this->global_model->update('activity',$data,array('activity_id'=>$data['activity_id']));
				$id = $data['activity_id'];

			}else{

				$data['set'] = array('date_created'=>'NOW()');
				$id = $this->global_model->insert('activity',$data);

			}

			$this->global_model->delete('activity_media',array('activity_id'=>$id));
			if($medias){
				foreach($medias as $v){
					$file = explode('.', $v);
					$media_type = strpos('jpg jpeg png',$file[1]) !== false ? 1 : 2;
					$this->global_model->insert('activity_media',array('media'=>$v,'media_type'=>$media_type,'activity_id'=>$id)); 
				}
			}
    		
    		$response = array('success'=>1,'message'=>'Sucess');

		} else {

			$response = array('success'=>0,'message'=>validation_errors());

		}

	   $this->response($response, 200);
 
 
	}

	public function validate_form($rules)
	{

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('', '');	 

		$this->form_validation->set_rules($rules);
		
		return $this->form_validation->run();
	}




	public function login_post(){

 		$where = array('username'=>$this->post('username'),'password'=>md5($this->post('password')));
		$row = $this->global_model->get_row(array('table'=>'cms_user','where'=>$where));
		$response = array('success'=>0,'message'=>'Invalid credentials');
 		if($row){
			$response = array('success'=>1,'message'=>'Successfully signed in.');
			$this->session->set_userdata(array('admin_logged_in'=>1));
		}

		$this->response($response, 200);
		
	}

	public function experiences_get(){

		$rows = $this->global_model->get_rows(array('table'=>'experience'));
		$this->response($rows->result_array(),200);
 	}

}