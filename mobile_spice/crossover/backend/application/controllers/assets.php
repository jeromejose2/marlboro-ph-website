<?php

class Assets extends CI_Controller
{
	private $ext;
	private $content;
	private $file;

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('user_id')) {
			show_404();
		}
		$this->file = $this->uri->uri_string();
		$this->content = file_get_contents($this->file);
		$this->ext = strtolower(pathinfo($this->file, PATHINFO_EXTENSION));
	}

	public function fonts()
	{
		switch ($this->ext) {
			case 'svg':
				$this->output->set_content_type('image/svg+xml');
				break;
			case 'ttf':
				$this->output->set_content_type('application/x-font-ttf');
				break;
			case 'otf':
				$this->output->set_content_type('application/x-font-opentype');
				break;
			case 'woff':
				$this->output->set_content_type('application/font-woff');
				break;
			case 'eot':
				$this->output->set_content_type('application/vnd.ms-fontobject');
				break;
		}
		$this->output->set_output($this->content);
	}

	public function images()
	{
		switch ($this->ext) {
			case 'png':
				$this->output->set_content_type('image/png');
				break;
			case 'gif':
				$this->output->set_content_type('image/gif');
				break;
			case 'jpg':
			case 'jpeg':
				$this->output->set_content_type('image/jpeg');
				break;
		}
		$this->output->set_output($this->content);
	}

	public function plugins()
	{
		switch ($this->ext) {
			case 'css':
				$this->output->set_content_type('text/css');
				break;
			case 'js':
				$this->output->set_content_type('text/javascript');
				break;
		}
		$this->output->set_output($this->content);
	}

	public function popups()
	{
		$this->output->set_content_type('text/html')
			->set_output($this->content);
	}

	public function scripts()
	{
		$this->output->set_content_type('text/javascript')
			->set_output($this->content);
	}

	public function styles()
	{
		$this->output->set_content_type('text/css')
			->set_output($this->content);
	}

	public function swf()
	{
		$this->output->set_content_type('application/x-shockwave-flash')
			->set_output($this->content);
	}

	public function uploads()
	{
		switch ($this->ext) {
			case 'png':
				$this->output->set_content_type('image/png');
				break;
			case 'gif':
				$this->output->set_content_type('image/gif');
				break;
			case 'jpg':
			case 'jpeg':
				$this->output->set_content_type('image/jpeg');
				break;
		}
		$this->output->set_output($this->content);
	}
}