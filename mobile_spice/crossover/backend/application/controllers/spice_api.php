<?php if(!defined("BASEPATH")){ exit("No direct script access allowed!"); }

class Spice_api extends CI_Controller {

    
    public function __construct()
    {
        parent:: __construct();
    }

    public function checkAction()
    {
        $this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);

        $PersonId = $this->input->get('personid');
        $CellId = $this->input->get('cellid');

        $response = $this->spice->checkAction(array('CellId' => $CellId, 'PersonId' => $PersonId));
        $response = $this->spice->parseJSON($response);

        echo "<pre>";
        print_r($response);
    }
    
    public function getPerson()
    {   
        $this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);

        $response = $this->spice->GetPerson($this->input->get());
        $response_json = $this->spice->parseJSON($response);
        $DateOfBirth = $this->spice->extractSpiceDate($response_json->ConsumerProfiles->PersonDetails->DateOfBirth);
        echo $DateOfBirth;

        echo "<pre>";
        //print_r($response);
        echo "<br>";
        print_r($response_json);
        echo "<br>";
        print_r(count($response_json->ConsumerProfiles->Derivations));
    }

    public function searchPerson()
    {
        $this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);

        //LoginName , PersonId 

        $response = $this->spice->SearchPerson($this->input->get());
        $response_json = $this->spice->parseJSON($response);

        echo "<pre>";
        print_r($response);
        echo "<br>";
        print_r($response_json);
        echo "<br>";
        print_r($this->session->userdata('spice_input')); 
    }

    public function getBulkx()
    {
        $response = $this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);

        print_r($response); 
        
        $table = $this->input->get('table');

        $response = $this->spice->getBulk($table);
        $response = $this->spice->parseJSON($response);

        echo "<pre>";
        print_r($response);
    }

    public function getBulkConsumerAttribute()
    {
        $this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);

        $response = $this->spice->getBulk('ConsumerAttribute');
        $response_json = $this->spice->parseJSON($response);
        $arr_brand = $this->spice->fromXMLtoObject($response_json->MessageBody);

        echo "<pre>";
        $count = count($arr_brand->Table);
        
        for($i = 0; $i<$count; $i++){
                $data[] = array(
                    'attribute_id' => $arr_brand->Table[$i]->AttributeId,
                    'attribute_code' => $arr_brand->Table[$i]->AttributeCode,
                    'attribute_name' => $arr_brand->Table[$i]->AttributeName,
                    );
        }

        // print_r($data); die();
        $this->db->empty_table('tbl_consumer_attr_spice'); 
        $this->db->insert_batch('tbl_consumer_attr_spice', $data); 

        // print_r($data);
    }

    public function getBulkBrand()
    {
        $this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);

        $response = $this->spice->getBulk('Brand');
        $response_json = $this->spice->parseJSON($response);
        $arr_brand = $this->spice->fromXMLtoObject($response_json->MessageBody);
        
        echo "<pre>";
        $count = count($arr_brand->Table);
        
        for($i = 0; $i<$count; $i++){
                $data[] = array(
                    'competitor_flag' => $arr_brand->Table[$i]->CompetitorFlag,
                    'brand_name' => $arr_brand->Table[$i]->BrandName,
                    'consumer_flag' => $arr_brand->Table[$i]->ConsumerFlag,
                    'fieldmarketer_flag' => $arr_brand->Table[$i]->FieldMarketerFlag,
                    'campaign_flag' => $arr_brand->Table[$i]->CampaignFlag,
                    'brand_id' => $arr_brand->Table[$i]->BrandId,
                    );
        }

        $this->db->empty_table('tbl_brands_spice'); 
        $this->db->insert_batch('tbl_brands_spice', $data); 

        // print_r($data);
    }

    public function getBulkChannel()
    {
        $this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);

        $response = $this->spice->getBulk('ReferenceData');
        $response_json = $this->spice->parseJSON($response);
        $arr_channel = $this->spice->fromXMLtoObject($response_json->MessageBody);
        
        echo "<pre>";
        // print_r($arr_channel); die();
        $count = count($arr_channel->Table);
        
        for($i = 0; $i<$count; $i++){
            if( ($arr_channel->Table[$i]->GroupName == 'Channel') ){
                $data[] = array(
                    'channel_code' => $arr_channel->Table[$i]->ClassCode,
                    'channel_name' => $arr_channel->Table[$i]->ClassName,
                    );
            }
        }

        $this->db->empty_table('tbl_channel_codes'); 
        $this->db->insert_batch('tbl_channel_codes', $data); 

        print_r($data);
    }

    public function changePass()
    {   
        $this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);

        $NewPassword = $this->input->get('new');
        $LoginName = $this->input->get('email');
        $CurrentPassword = $this->input->get('current');

        $response = $this->spice->changePassword(array('LoginName' => $LoginName, 'NewPassword' => $NewPassword, 'CurrentPassword' => $CurrentPassword));
        $response = $this->spice->parseJSON($response);

        echo "<pre>";
        print_r($response);
    }

    public function getBrand()
    {
        $this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);

        $filter = $this->input->get('filter');

        $response = $this->spice->getBulk($filter);
        $response = $this->spice->parseJSON($response);

        echo "<pre>";
        print_r($response);
    }

    public function login()
    {
        $username = $this->input->get('username');
        $password = $this->input->get('password');

        $accessGranted = $this->spice->loginMember($username,$password);

        echo "<pre>";
        print_r($accessGranted);
        $test = $accessGranted ? '' : 'Invalid username/password.';
        echo $test;

    }

     public function noEncryptionLogin()
    {
        $username = $this->input->get('username');
        $password = $this->input->get('password');

        $accessGranted = $this->spice->createSessionNoEncryption($username,$password);

        echo "<pre>";
        print_r($accessGranted);
        echo $accessGranted ? '' : 'Invalid username/password.';

    }

    public function displayDate(){

        $date_time = $this->input->get('date_time'); # '/Date(1365004652303-0500)/'

 
        $match = preg_match('/\/Date\((\d+)([-+])(\d+)\)\//', $date_time, $date);

        $timestamp = $date[1]/1000;
        $operator = $date[2];
        $hours = $date[3]*36; // Get the seconds

        $datetime = new DateTime();

        $datetime->setTimestamp($timestamp);
        $datetime->modify($operator . $hours . ' seconds');
        var_dump($datetime->format('Y-m-d H:i:s'));


    }


    public function getDateOfBirth(){

        $dateTime = DateTime::createFromFormat('Y-m-d H:i:s', $this->input->get('birthday').' 00:00:00');
        $DateOfBirth = sprintf('/Date(%s%s)/',
                               $dateTime->format('U') * 1000,
                               $dateTime->format('O')
                            );
 
        $match = preg_match('/\/Date\((\d+)([-+])(\d+)\)\//', $DateOfBirth, $date);

        $timestamp = $date[1]/1000;
        $operator = $date[2];
        $hours = $date[3]*36; // Get the seconds

        $datetime = new DateTime();

        $datetime->setTimestamp($timestamp);
        $datetime->modify($operator . $hours . ' seconds');
        var_dump($datetime->format('Y-m-d H:i:s'));

    }

    public function updateProfile(){
        
        // $marketBrands = $this->db->select()
        //         ->from('tbl_brands_spice')
        //         ->where(array('fieldmarketer_flag' => 'true'))
        //         ->get()
        //         ->result();

        // foreach($marketBrands as $v){
        //     $marketingPreference[] = array('BrandId'=>$v->brand_id, 'OptValue' => '1');
        // }

        // $marketChannels = $this->db->select()
        //         ->from('tbl_channel_codes')
        //         ->get()
        //         ->result();
        
        // foreach($marketChannels as $v){
        //     $marketingPreference[] = array('ChannelCode'=>$v->channel_code, 'OptValue' => '1');
        // }

        $params = array('person_id'=> 617195,
                        'updates'=> array(
                            'AddressType'=>array(
                                array(
                                    'AddressLine1' => 'rtert',
                                    'Area' => 'ewrt',
                                    'Locality' => 'BATAAN',
                                    'City' => 'ABUCAY', 
                                    'Country' => 'US',
                                    'Premise' => 'PH',
                                    'PostalCode' => '1234'
                                ),
                            )
                        )
                    );

        $response = $this->spice->updatePerson($params);
        $response = $this->spice->parseJSON($response);

        echo "<pre>";
        print_r($params);
        echo "<br>";
        print_r($response);
    }


    public function createLoginToken()
    {
      
       $this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);
 
        $PersonId = $this->input->get('person_id');
        $dlTemplateId = 1033;
        $cellId = 1394;
        $login = $this->spice->createLoginToken($PersonId,$dlTemplateId, $cellId);
        $login_json = $this->spice->parseJSON($login);
        echo "<pre>";

        echo "<br/><br/>";
        print_r($login_json);
        echo "<br/><br/>";

        $email = 'ron.virtudazo@nuworks.ph';
        $code = 'M00000274';
        $base_url = str_replace(array('http://','https://'), '', BASE_URL);
        $base_url = $base_url.'user/track_submit'.'?token=' .$login_json->LoginToken;


        $params = ['Email' => $email,
                                'MailingCode' => $code,
                                'IsSMTPMail' => false,
                                'Fields' => [   
                                                ['Name' => 'ph_campaignurlpersonal', "ValueDataType"=>"String", 'ValueAsString' => $base_url],
                                                ['Name' => 'ph_firstname', "ValueDataType"=>"String", 'ValueAsString' => 'Ron'],
                                                ['Name' => 'ph_lastname', "ValueDataType"=>"String", 'ValueAsString' => 'Virtudazo'],
                                                ['Name' => 'ph_emailaddress', "ValueDataType"=>"String", 'ValueAsString' => $email]
                                            ]
                                ];

        $response = $this->spice->sendMailing($params);
        $response = $this->spice->parseJSON($response);

           print_r($response);

        
          
    }


    public function ave_verification_request(){

       if($this->input->post()){

        $this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);

            $this->load->helper('upload');


            $path = 'uploads/registration/';

            $this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);

            $filename = uniqid();
            $max_size = 2048;
            $params = array('upload_path'=>$path,'allowed_types'=>FILE_UPLOAD_ALLOWED_TYPES,'file_name'=>$filename,'max_size'=>$max_size,'max_width'=>0,'max_height'=>0,'do_upload'=>'giid');
            $file = upload($params);

           if(is_array($file)){

                $image = BASE_URL.'uploads/registration/'.$file['file_name'];
                echo "Uploaded Image URL: ".$image;
                $imageString = base64_encode(file_get_contents($image));

                $person_id = $this->input->get('person_id');

                $inputs = ['PersonId'=>$person_id,
                              'VerificationImages'=>[
                                        'IdDocumentFileType'=>'3',
                                        'IdDocumentMarketCode'=>'PH',
                                        'IdDocumentTypeCode'=>'1',
                                        'IdDocumentCountryCode'=>'PH',
                                        'IdDocumentImage'=>$imageString
                                    ]
                            ];
                //$inputs = stripslashes(json_encode($inputs));

                echo "Inputs:<br/>";
                $response = $this->spice->AgeVerificationRequest2($inputs);
                $response_json = $this->spice->parseJSON($response);
               //$inputs['IdDocumentImage'] = '(base 64 image)';
         
                echo "<br/><br/><pre><strong>Response:</strong><br/>";
                print_r($response_json);
                echo "<br>";
                echo empty($response) ? $response.' is empty' : $response;


            }else{

                echo $file;

            }

               
        }

        $this->load->view('user/upload-form');

    }

    public function resetPass()
    {
        $email = $this->input->get('email');
        $NewPassword = $this->input->get('newpw');

        $response = $this->spice->resetPassword(array('EmailAddress' => $email, 'NewPassword' => $NewPassword));
        $response = $this->spice->parseJSON($response);

        echo "<pre>";
        print_r($response);
    }

    public function get_giid_types(){

        $this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);
        $giid_types = $this->spice->getGIIDTypes('IdDocumentType');

        $this->db->empty_table('tbl_giid_types_spice'); 
        $this->db->insert_batch('tbl_giid_types_spice', $giid_types);

        echo "<pre>";
        print_r($giid_types);

    }

    public function get_brands(){

        $this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);

        $brands = $this->spice->getBrand(array('LiteMode'=>true));
        
        $this->db->empty_table('tbl_brands_spice'); 
        $this->db->insert_batch('tbl_brands_spice', $brands); 
        echo 'Count: '.count($brands);
        echo "<pre>";
        print_r($brands);

    }


    public function loginAdmin()
    {
        $response = $this->spice->loginAdminTest(ADMIN_USERNAME, ADMIN_PASSWORD);    
        echo "<pre>";
        print_r($response);
    }

    public function loginMember()
    {
        $this->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);
        $response = $this->spice->loginMemberTest($this->input->get('username'),$this->input->get('password'));
        $response = $this->spice->parseJSON($response);
        echo "<pre>";
        print_r($response);
    }

    public function GetBulk()
    {
        $this->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);
        $table = $this->input->get('table');

        $response = $this->spice->getBulk($table);
        $response = $this->spice->parseJSON($response);
        $MessageBody = $this->spice->parseXML($response->MessageBody);

        echo "<pre>";
        print_r($MessageBody);
        

    }    

    public function ManagePerson(){

        $PersonId = $this->input->get('person_id');
        $spice_userinfo = $this->spice->GetPerson(array('PersonId' => $PersonId));
        $spice_userinfo = $this->spice->parseJSON($spice_userinfo);
        $spice_userinfo = $spice_userinfo->ConsumerProfiles;

        $data = [
                'ProfileType' => 'C',
    
                'PersonalDetails' => [
                        'FirstName' => $spice_userinfo->PersonDetails->FirstName,
                        'MiddleName'=> $spice_userinfo->PersonDetails->MiddleName,
                        'LastName' => $spice_userinfo->PersonDetails->LastName,
                        'Gender' =>  $spice_userinfo->PersonDetails->Gender,
                        'MobileTelephoneNumber' => $spice_userinfo->PersonDetails->MobileTelephoneNumber,
                        'EmailAddress'=> $spice_userinfo->PersonDetails->EmailAddress,
                        'DateOfBirth' => $spice_userinfo->PersonDetails->DateOfBirth,
                    ],
                    'AddressType' => [
                        [
                            'AddressLine2' => $spice_userinfo->Addresses[0]->AddressLine2,
                            'Area' => $spice_userinfo->Addresses[0]->Area,
                            'Locality' => $spice_userinfo->Addresses[0]->Locality,
                            'City' => $spice_userinfo->Addresses[0]->City, 
                            'Country' => 'PH',
                            'Premise'=>'PH',
                            'PostalCode' => $spice_userinfo->Addresses[0]->PostalCode
                        ]
                    ],
                    'Derivations' => [
                        [
                            'AttributeCode' => 'ca_nickname',
                            'AttributeValue' =>  $spice_userinfo->Derivations[0]->AttributeValue
                        ],
                        [
                            'AttributeCode' => 'ca_outletcode',
                            'AttributeValue' =>  $spice_userinfo->Derivations[1]->AttributeValue
                        ],
                        [
                            'AttributeCode' => 'ca_reaction',
                            'AttributeValue' => $spice_userinfo->Derivations[2]->AttributeValue
                        ],
                        [
                            'AttributeCode' => 'ca_points',
                            'AttributeValue' =>  $spice_userinfo->Derivations[3]->AttributeValue
                        ],
                        [
                            'AttributeCode' => 'ca_main_interest1',
                            'AttributeValue' =>  $spice_userinfo->Derivations[4]->AttributeValue
                        ],
                        [
                            'AttributeCode' => 'ca_main_interest2',
                            'AttributeValue' =>  $spice_userinfo->Derivations[5]->AttributeValue
                        ],
                        [
                            'AttributeCode' => 'ca_main_interest3',
                            'AttributeValue' =>  $spice_userinfo->Derivations[6]->AttributeValue
                        ],
                        [
                            'AttributeCode' => 'ca_mi1_subinterest1',
                            'AttributeValue' =>  $spice_userinfo->Derivations[7]->AttributeValue
                        ],
                        [
                            'AttributeCode' => 'ca_mi1_subinterest2',
                            'AttributeValue' =>  $spice_userinfo->Derivations[8]->AttributeValue
                        ],
                        [
                            'AttributeCode' => 'ca_mi2_subinterest1',
                            'AttributeValue' =>  $spice_userinfo->Derivations[9]->AttributeValue
                        ],
                        [
                            'AttributeCode' => 'ca_mi2_subinterest2',
                            'AttributeValue' =>  $spice_userinfo->Derivations[10]->AttributeValue
                        ],
                        [
                            'AttributeCode' => 'ca_mi3_subinterest1',
                            'AttributeValue' =>  $spice_userinfo->Derivations[11]->AttributeValue
                        ],
                        [
                            'AttributeCode' => 'ca_mi3_subinterest2',
                            'AttributeValue' =>  $spice_userinfo->Derivations[12]->AttributeValue
                        ],
                    ]
            ];

        $response = $this->spice->ManagePerson($data);
        $response = $this->spice->parseJSON($response);

        echo "<pre>";
        print_r($data);
        echo "<pre>";
        print_r(stripslashes($this->spice->toJSONFormat($response)));
    }

    public function getPersonIds()
    {
        $sql = 'SELECT * FROM tbl_login JOIN tbl_registrants ON tbl_registrants.registrant_id = tbl_login.registrant_id WHERE DATE(date_login) = "'.date('Y-m-d').'" GROUP BY tbl_registrants.person_id';
        $result = $this->db->query($sql)->result_array();

        $this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);

        foreach ($result as $k => $v) {
            $person_id = $v['person_id'];
            $total_points = $v['total_points'];

            $spice_userinfo = $this->spice->GetPerson(array('PersonId' => $person_id));
            $spice_userinfo = $this->spice->parseJSON($spice_userinfo);
            $spice_userinfo = $spice_userinfo->ConsumerProfiles;

            echo "<pre>";
            print_r($person_id.' - '.$total_points);

            echo "<br><br><pre>";
            print_r($spice_userinfo);
        }
        
        
    }


    public function checkSession(){

        $sessionKey = $this->input->get('sessionkey');
        $response = $this->spice->checkSessionTest($sessionKey);
        $response_json = $this->spice->parseJSON($response);
        echo "<pre>";
        print_r($response_json);
    }


    public function logout(){

        $sessionKey = $this->input->get('sessionkey');
        $response = $this->spice->endSession($sessionKey);
        $response_json = $this->spice->parseJSON($response);
        echo "<pre>";
        print_r($response_json);


    }


    public function errorlogs(){

        $this->load->view('spice/error-logs');

    }

    public function getErrorlogs(){
        $this->db->limit(20);
        $this->db->order_by("date_created", "desc"); 
        $row = $this->db->get('tbl_spice_error_logs');
        echo json_encode($row->result_array());
       // $this->output->set_content_type('application/json')->set_output($row->result_array());

    }

    public function marketPref(){
        $marketBrands = $this->db->select()
                ->from('tbl_brands_spice')
                ->where(array('fieldmarketer_flag' => 'true'))
                ->get()
                ->result();

        foreach($marketBrands as $v){
            $marketingPreference[] = array('BrandId'=>$v->brand_id, 'OptValue' => '1');
        }

        $marketChannels = $this->db->select()
                ->from('tbl_channel_codes')
                ->get()
                ->result();
        
        foreach($marketChannels as $v){
            $marketingPreference[] = array('ChannelCode'=>$v->channel_code, 'OptValue' => '1');
        }

        echo "<pre>";
        print_r($marketingPreference);
    }

    public function setSession()
    {
        return $this->spice->setSessionKey('');
    }
} 
