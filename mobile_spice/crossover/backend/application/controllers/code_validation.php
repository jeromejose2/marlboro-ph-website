<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Code_Validation extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if( ! $this->input->is_ajax_request()) {
			exit('not ajax');
		}
		if( ! $_SERVER['REQUEST_METHOD'] == 'POST') {
			exit('not post');
		}
		if( ! $this->session->userdata('registrant_id')) {
			exit('no session');
		}
	}

	public function index()
	{
		if($_POST) {
			$input_code = $this->input->post('code');
			$registrant_id = $this->session->userdata('registrant_id');
			$validate_code_and_return_points = $this->validate_code($input_code);
			if($validate_code_and_return_points) {
				if( ! $this->code_already_taken($input_code)) {
					$this->db->insert('tbl_registrant_pack_code', array('pack_code'=>$input_code, 'registrant_id'=>$registrant_id, 'earned_km_points'=>$validate_code_and_return_points));
					$this->db->set('total_km_points', 'total_km_points + '.(int)$validate_code_and_return_points, FALSE);
					$this->db->set('credit_km_points', 'credit_km_points + '.(int)$validate_code_and_return_points, FALSE);
					$this->db->where('registrant_id', $registrant_id);
					$this->db->update('tbl_registrants');

					echo (int) $validate_code_and_return_points;
				} else {
					echo 'Code already taken';
				}
			} else {
				echo 'Invalid code';
			}
		}
	}

	private function validate_code($code)
	{
		$valid = $this->db->select()->from('tbl_pack_code')->where('code', $code)->get()->row();
		if($valid) {
			return $valid->km_points;
		} else {
			return FALSE;
		}
	}

	private function code_already_taken($code)
	{
		$valid = $this->db->select()->from('tbl_registrant_pack_code')->where(array('pack_code'=>$code))->get()->row();
		if($valid) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}