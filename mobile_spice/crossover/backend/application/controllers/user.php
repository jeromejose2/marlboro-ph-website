<?php

class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct(); 
		
		define('SITE_URL', site_url());
	}

	public function index()
	{
		//redirect('user/login');
		redirect('user/register');
	}

	public function swf()
	{
		$this->load->view('user/swf_xml');
	}
	 
	public function reset_success()
	{
		if (!$this->session->flashdata('forgot_submit')) {
			redirect('user/login');
		}
		$this->load->view('user/reset_success', array(), 'user/template');
	}

	public function register()
	{		
		if($this->input->get('token')){

			$response = $this->spice->validateLoginLoken($this->input->get('token'));
			$response_json = $this->spice->parseJSON($response);

			if ($response_json->MessageResponseHeader->TransactionStatus) {

				$this->session->set_userdata('response_message_error', $response_json->MessageResponseHeader->TransactionStatusMessage);
				redirect('user/register');
				
			}

		}
		
 		$data = array();
		$data['token'] = generate_token();
		$this->session->set_userdata('reg_token', $data['token']);

		$data['mobile_prefixes'] = $this->Registration_Model->get_mobile_prefixes();
		$data['provinces'] = $this->Registration_Model->get_provinces();
		$data['alternate_purchase'] = $this->Registration_Model->get_alternate_purchase();
		// $data['referral_code'] = $this->session->userdata('refer_friend')['referral_code_from_refer'];
		// $data['email'] = $this->session->userdata('refer_friend')['email_from_refer'];

		$this->load->view('user/steps', $data, 'user/template');

	}
	 
	public function validate_step_1($funcCall = false)
	{
		if (!isset($this->form_validation))
			$this->load->library('form_validation'); 

		$_POST['mobile_number'] = $this->input->post('mobile_prefix').$this->input->post('mobile_number');

		$this->form_validation->set_rules('smoker','Smoker','required|numeric|callback_smoker_check');
		$this->form_validation->set_rules('fname', 'First name', 'required|alpha_space');
		$this->form_validation->set_rules('lname', 'Last name', 'required|alpha_space');
		$this->form_validation->set_rules('mname', 'Middle name', 'alpha_space');
		$this->form_validation->set_rules('nname', 'Nick name', 'required|alpha_space');
		$this->form_validation->set_rules('birthday', 'Birthdate', 'required|callback_birthdate_check');
		$this->form_validation->set_rules('gender', 'Gender', 'required|callback_check_gender');
		$this->form_validation->set_rules('mobile_prefix', 'Mobile Prefix number', 'required|numeric|min_length[4]|max_length[4]|callback_check_mobile_prefix');
		$this->form_validation->set_rules('mobile_number', 'Mobile number', 'required|numeric|min_length[11]|max_length[11]');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('street', 'Street', 'required');
		$this->form_validation->set_rules('brgy', 'Barangay', 'required');
		$this->form_validation->set_rules('province', 'Province', 'required|numeric|callback_province_check');
		$this->form_validation->set_rules('city', 'City', 'required|numeric|callback_city_check');
		$this->form_validation->set_rules('zip_code', 'Zip Code', 'required|numeric|min_length[4]|max_length[4]');
		$this->form_validation->set_rules('referred_code', 'Referral Code', 'callback_validate_referral_code');
		$this->form_validation->set_rules('outlet_code', 'Outlet Code', 'callback_outlet_code_check');
		//$this->form_validation->set_message('email_check', 'E-mail already exists in database');
		$this->form_validation->set_message('outlet_code_check','Outlet code is not existing');
		//$this->form_validation->set_message('birthdate_check', 'Sorry, You need to be a legal aged (18 years old and above) smoker in order to register.');

		if (!$funcCall) {
			$result = array(
				'success' => false,
				'error' => ''
			);
			$this->output->set_content_type('application/json');
			if (!$this->form_validation->run()) {
				$result['error'] = validation_errors();
				$this->output->set_output(json_encode($result));
			} else {
				$result['success'] = true;
				$this->output->set_output(json_encode($result));
			}
		}
	}

	public function validate_referral_code($referral_code){

		if($referral_code){

			$row = $this->db->get_where('tbl_referrals',array('email'=>$this->input->post('email'),'code'=>$referral_code))->row();

			if(!$row){

				$this->form_validation->set_message('validate_referral_code','Referral code not valid.');
				return false;

			}

		}

		return true;

	}

	public function check_mobile_prefix($val)
	{

		$row = $this->db->get_where('tbl_mobile_prefix',array('mobile_prefix'=>$val))->row();
		if(!$row){
			$this->form_validation->set_message('check_mobile_prefix','Mobile Prefix number does not exists.');
			return false;
		}else{
			return true;
		}

	}

	public function check_gender($val)
	{
		if($val !='F' && $val != 'M')
		{
			$this->form_validation->set_message('check_gender','Gender field may only contain "F" character for female or "M" character for male.');
			return false;
		}else{
			return true;
		}

	}

	public function smoker_check($val)
	{

		if($val != 1){
			$this->form_validation->set_message('smoker_check','Smoker field may only contain "Yes" character.');
			return false;
		}else{
			return true;
		}

	}

	public function token_check($token)
	{
		$success = false;
		if ($token && $this->session->userdata('reg_token') === $token) {
			$success = true;
		} else {
			//$this->form_validation->set_message('token_check', 'Invalid form request. correct:'.$this->session->userdata('reg_token'));
			$this->form_validation->set_message('token_check', 'Invalid form request.');
		}
		return $success;
	}

	public function outlet_code_check($code)
	{
		if (!$code) {
			return true;
		}

		$outlet = $this->db->select()
			->from('tbl_outlet_codes')
			->where('outlet_code', $code)
			->limit(1)
			->get()
			->row();

		return $outlet ? true : false;
	}

	public function email_check($email)
	{
		$exists = $this->db->select()
			->from('tbl_registrants')
			->where('email_address', $email)
			->limit(1)
			->get()
			->row();
		return $exists ? false : true;
	}

	public function birthdate_check($date)
	{
		$date_arr = explode('-',$date);

		$year = (isset($date_arr[0])) ? $date_arr[0] : 0;
		$month = (isset($date_arr[1])) ? $date_arr[1] : 0;
		$day = (isset($date_arr[2])) ? $date_arr[2] : 0;

		if(checkdate($month, $day, $year)){

			$date = new DateTime($date);
			$age = $date->diff(new DateTime);
			if (!$age || $age->y < MINIMUM_AGE) {
				$this->form_validation->set_message('birthdate_check',' Sorry, You need to be a legal aged ('.MINIMUM_AGE.' years old and above) smoker in order to register.');
				return false;
			}
			return true;

		}else{
			$this->form_validation->set_message('birthdate_check','Invalid Birthdate format.');
			return false;
		}

		/*
		$date = new DateTime($date);
		$age = $date->diff(new DateTime);
		if (!$age || $age->y < MINIMUM_AGE) {
			return false;
		}
		return true;
		*/
	}

	public function province_check($p)
	{
		$province = $this->db->select()
			->from('tbl_provinces')
			->where('province_id', $p)
			->limit(1)
			->get()
			->row();
		if (!$province) {
			$this->form_validation->set_message('province_check','Province does not exists.');
			return false;
		}
		return true;
	}

	public function city_check($id)
	{
		
		$where = array('city_id'=>$id,'province_id'=>$this->input->post('province'));
		 
		$city = $this->db->select()
			->from('tbl_cities')
			->where($where)
			->limit(1)
			->get()
			->row();
 
		if (!$city) {
			$this->form_validation->set_message('city_check','City does not exists.');
			return false;
		}
		return true;
	}

	public function validate_step_2($funcCall = false)
	{

 		$current_brand = $this->db->get_where('tbl_brands_spice',array('brand_id'=>$this->input->post('current_brand')))->row();
		$first_alternate_brand = $this->db->get_where('tbl_brands_spice',array('brand_id'=>$this->input->post('first_alternate_brand')))->row();
		$error = '';
		$count_error = 0;

		if(!$current_brand){
			$error = 'Your primary brand does not exists.';
			$count_error++;
		}

		if(!$first_alternate_brand){
			$error .= '<br/><br/>Your secondary brand does not exists.';
			$count_error++;
		}

		if (!$funcCall) {
			$result = array(
				'success' => false,
				'error' => ''
			);
			$this->output->set_content_type('application/json');
			if ($count_error) {
				$result['error'] = $error;
				$this->output->set_output(json_encode($result));
			} else {
				$result['success'] = true;
				$this->output->set_output(json_encode($result));
			}
		}
 

	}

	public function validate_step_3($funcCall = false, $withGiid = true)
	{

		if (!isset($this->form_validation))
			$this->load->library('form_validation');

		$this->form_validation->set_rules('__token', 'Token', 'callback_token_check');
		if ($withGiid) {
			$this->form_validation->set_rules('giid_number', 'GIID Number', 'required');
			$this->form_validation->set_rules('giid_type', 'GIID Type', 'required');
		}


		if (!$funcCall) {
			$result = array(
				'success' => false,
				'error' => ''
			);
			$this->output->set_content_type('application/json');
			if (!$this->form_validation->run()) {
				$result['error'] = validation_errors();
				$this->output->set_output(json_encode($result));
			} else {
				$imageString = $this->input->post('captured_image_ba');
				if (empty($_FILES['giid']['name']) && !$imageString) {
					$result['error'] = 'Please upload a file.';
					$this->output->set_output(json_encode($result));
					return;
				}
				$result['success'] = true;
				$this->output->set_output(json_encode($result));
			}
		}

	}

	

	public function confirm()
	{

		$this->load->library(array('spice','Encrypt'));
		$this->load->helper('upload');
 
		$imageString = $this->input->post('captured_image_ba');		 
		$salt = generate_password(20);
		$fileTypes = array('.jpeg'=>1,'.png'=>2,'.jpg'=>3,'.bmp'=>4,'.pdf'=>5);	

 		$this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);

		$city = $this->db->select()
			->from('tbl_cities')
			->where(array('city_id'=>$this->input->post('city')))
			->limit(1)
			->get()
			->row();

		$province = $this->db->select()
			->from('tbl_provinces')
			->where(array('province_id'=>$this->input->post('province')))
			->limit(1)
			->get()
			->row();

		$city = $city ? $city->city : '';
		$province = $province ? $province->province : '';
		$path = './uploads/registration/';
		$f = '';


		if(!is_dir($path)){
			mkdir($path,0777,true);
		}
 

		if(!$imageString){

 			/*$file_tmp = $_FILES['giid']['tmp_name'];
		    $type = pathinfo($file_tmp, PATHINFO_EXTENSION);
			$imageString = base64_encode(file_get_contents($file_tmp));*/

			$filename = uniqid();
			$max_size = 2048;
			$params = array('upload_path'=>$path,'allowed_types'=>SPICE_FILE_UPLOAD_ALLOWED_TYPES,'file_name'=>$filename,'max_size'=>$max_size,'max_width'=>0,'max_height'=>0,'do_upload'=>'giid');
	    	$file = upload($params);

	    	if(is_array($file)){ 

	    		$image = BASE_URL.'uploads/registration/'.$file['file_name'];
	    		$imageString = base64_encode(file_get_contents($image));
				unlink($path.$file['file_name']);

	    	}else{

	    		$this->output->set_output('<br/>ERROR UPLOAD: '.$file);
	    		return;

	    	}

	    	$IdDocumentFileType = $fileTypes[$file['file_ext']];
			
		}else{

			if(!is_dir($path)){
				mkdir($path,0777,true);
			}
			$filename = uniqid().'.jpg';
			$IdDocumentFileType = $fileTypes['.jpg'];

			file_put_contents($path.$filename, gzuncompress(base64_decode($imageString)));
			$imageString = base64_encode(file_get_contents(BASE_URL.$path.$filename));
			unlink($path.$filename);

		}

			$marketBrands = $this->db->select()
                    ->from('tbl_brands_spice')
                    ->where(array('fieldmarketer_flag' => 'true'))
                    ->get()
                    ->result();

	        foreach($marketBrands as $v){
	            $marketingPreference[] = array('BrandId'=>$v->brand_id, 'OptValue' => '1');
	        }

            $marketChannels = $this->db->select()
					->from('tbl_channel_codes')
					->get()
					->result();
			
			foreach($marketChannels as $v){
                $marketingPreference[] = array('ChannelCode'=>$v->channel_code, 'OptValue' => '1');
            }

			$email_pre = '';
			$MultibrandSmoker = $this->input->post('current_brand') !== $this->input->post('first_alternate_brand') ? '1' : '0';

			$dateTime = DateTime::createFromFormat('Y-m-d H:i:s', $this->input->post('birthday').' 13:00:00');
			$DateOfBirth = sprintf(
			   '/Date(%s%s)/',
			   $dateTime->format('U') * 1000,
			   $dateTime->format('O')
			);

			/*jpeg - 1
			png - 2
			jpg - 3
			bmp - 4
			pdf - 5*/

			$data = array(  'ProfileType'=>'C',
							'ConsumerProfile'=> array( 'PersonalDetails'=>array(
																				'FirstName' => $this->input->post('fname'),
																			    'MiddleName'=> $this->input->post('mname'),
																			    'LastName' => $this->input->post('lname'),
																			    'Gender'=> $this->input->post('gender'),
	 																		    'EmailAddress'=> strtolower($this->input->post('email')),
																			    'MobileTelephoneNumber'=> ltrim($this->input->post('mobile_prefix'), '0').$this->input->post('mobile_number'),
																			    'DateOfBirth'=>$DateOfBirth
																			    ),
														'AddressType'=>array(array( 
																			        'AddressLine1' => $this->input->post('street'),
																					'Area' => $this->input->post('brgy'),
																					'Locality' => $province,
																			        'City' => $city, 
																			        'Country' => 'PH',
																			        'Premise'=>'PH',
																			        'PostalCode' => $this->input->post('zip_code')
																			    )
																    ),
														'Derivations'=>array(																		
																    	[
																    		'AttributeCode' => 'ca_nickname',
																    		'AttributeValue' =>  $this->input->post('nname')
																    	],
																    	[
																    		'AttributeCode' => 'ca_outletcode',
																    		'AttributeValue' => $this->input->post('outlet_code') ? $this->input->post('outlet_code') : '0' //$spice_userinfo->Derivations[1]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_reaction',
																    		'AttributeValue' =>  $this->input->post('alternate_purchase') //$spice_userinfo->Derivations[2]->AttributeValue
																    	],														    	
																    	[
																    		'AttributeCode' => 'ca_points',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[3]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_main_interest1',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[4]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_main_interest2',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[5]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_main_interest3',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[6]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi1_subinterest1',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[7]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi1_subinterest2',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[8]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi2_subinterest1',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[9]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi2_subinterest2',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[10]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi3_subinterest1',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[11]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi3_subinterest2',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[12]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_referral_code',
																    		'AttributeValue' => $this->input->post('referred_code') ? $this->input->post('referred_code') : '0' //$spice_userinfo->Derivations[12]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_personal_referral_code',
																    		'AttributeValue' => '0'
																    	]

															),
														'BrandPreferences'=>array(
											    								'PrimaryBrand'=>array('BrandId'=>$this->input->post('current_brand')),
																				'SecondaryBrand'=>array('BrandId'=>$this->input->post('first_alternate_brand')),
																				'MultibrandSmoker'=>$MultibrandSmoker
																			),
														'LoginType'=>array(
										    						'EncryptedPassword' => $this->spice->encryptPassword(strtolower($this->input->post('email')),uniqid()),
																    'LoginName' => strtolower($this->input->post('email')),
																    'PasswordResetEmail' => strtolower($this->input->post('email'))
										    					),
														'TermsAndConditionsAcceptedList'=>array(
	 																					        'TermsAndConditionsCode' => 'TAC1002',
																						        'TermsAndConditionsVersionNumber' => '2'
											    											),
														'MarketingPreference' => $marketingPreference,
														'IdDocumentNumber'=>$this->input->post('giid_number'),
				    									'IdDocumentTypeCode'=>$this->input->post('giid_type'),
				    									'VerificationImage'=>[
				    														'IdDocumentImage'=>"$imageString",
				    														'IdDocumentFileType'=>$IdDocumentFileType,
				    														'IdDocumentMarketCode'=>'PH',
				    														'IdDocumentTypeCode'=>$this->input->post('giid_type'),
				    														'IdDocumentCountryCode'=>'PH'
				    														],
				    									'VerificationInfo'=>[
				    														'VerificationStatus'=>'0',
				    														'VerificationTypeCode'=>'7',
				    														'VerificationSource'=>'0',
				    														'VerificationSubStatusCode'=>'0'
				    													]
												   			    									   
												)
							);
			

			$manageperson = $this->spice->ManagePerson($data);
			$response_json = $this->spice->parseJSON($manageperson);

			if(!$manageperson || $response_json->MessageResponseHeader->TransactionStatus != 14 ){

				$data = array('origin_id'=>REGISTRATION,
								'method'=>'ManagePerson',
								'transaction'=>'Upload now registration',
								'input'=>$this->session->userdata('spice_input'),
								'response'=>$manageperson,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
				$this->spice->saveError($data);

				$response = $manageperson ? $response_json->MessageResponseHeader->TransactionStatusMessage : 'Response is empty. Please try again.';
				$this->output->set_output('<br/>ERROR UPLOAD: '.$response);
				return;

			}else{

				$sec_settings = $this->db->select()
                    ->from('tbl_section_settings')
                    ->where(array('name' => 'REGISTRATION'))
                    ->get()
                    ->row();

                $act_settings = $this->db->select()
                    ->from('tbl_activity_settings')
                    ->where(array('name' => 'REGISTRATION'))
                    ->get()
                    ->row();

                $cell_id = $sec_settings->setting_section_id;
				$act_id = $act_settings->setting_activity_id;

				$dateTime = DateTime::createFromFormat('Y-m-d H:i:s', date("Y-m-d").' 00:00:01');
				$DateOfBirth = sprintf(
				   '/Date(%s%s)/',
				   $dateTime->format('U') * 1000,
				   $dateTime->format('O')
				);

				//SPICE
				$this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);
				
				$param = array('CellId' => $cell_id, 'ActionList' =>array(array('ActivityId'   => $act_id, 'ActionValue' => '')));
				$response = $this->spice->trackAction($param);
				$response_json = $this->spice->parseJSON($response);

				if($response_json->MessageResponseHeader->TransactionStatus != 0){
						$data['message'] = '<h3>'.$response_json->MessageResponseHeader->TransactionStatusMessage.'</h3>';

						$data_error = array('origin_id'=>REGISTRATION,
								'method'=>'trackAction',
								'transaction'=>$this->router->method,
								'input'=> $this->session->userdata('spice_input'),
								'response'=>$response,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
						$this->spice->saveError($data_error);
				}
			}
		 
	}


	public function upload_later(){

		$this->load->library(array('spice','encrypt'));


		$this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);

 		$city = $this->db->select()
			->from('tbl_cities')
			->where(array('city_id'=>$this->input->post('city')))
			->limit(1)
			->get()
			->row();

		$province = $this->db->select()
			->from('tbl_provinces')
			->where(array('province_id'=>$this->input->post('province')))
			->limit(1)
			->get()
			->row();

		$city = $city ? $city->city : '';
		$province = $province ? $province->province : '';

		$marketBrands = $this->db->select()
                    ->from('tbl_brands_spice')
                    ->where(array('fieldmarketer_flag' => 'true'))
                    ->get()
                    ->result();

        foreach($marketBrands as $v){
            $marketingPreference[] = array('BrandId'=>$v->brand_id, 'OptValue' => '1');
        }

        $marketChannels = $this->db->select()
				->from('tbl_channel_codes')
				->get()
				->result();
		
		foreach($marketChannels as $v){
            $marketingPreference[] = array('ChannelCode'=>$v->channel_code, 'OptValue' => '1');
        }

		$email_pre = '';
		$MultibrandSmoker = $this->input->post('current_brand') !== $this->input->post('first_alternate_brand') ? '1' : '0';

		$dateTime = DateTime::createFromFormat('Y-m-d H:i:s', $this->input->post('birthday').' 13:00:00');
		$DateOfBirth = sprintf(
		   '/Date(%s%s)/',
		   $dateTime->format('U') * 1000,
		   $dateTime->format('O')
		);

		$data = array(  'ProfileType'=>'C',
						'ConsumerProfile'=> array( 'PersonalDetails'=>array(
																			'FirstName' => $this->input->post('fname'),
																		    'MiddleName'=> $this->input->post('mname'),
																		    'LastName' => $this->input->post('lname'),
																		    'Gender'=> $this->input->post('gender'),
 																		    'EmailAddress'=>strtolower($this->input->post('email')),
																		    'MobileTelephoneNumber'=>ltrim($this->input->post('mobile_prefix'), '0').$this->input->post('mobile_number'),
																		    'DateOfBirth'=>$DateOfBirth
																		    ),
													'AddressType'=>array(array( 
																    			'AddressLine1' => $this->input->post('street'),
																				'Area' => $this->input->post('brgy'),
																				'Locality' => $province,
																		        'City' => $city, 
																		        'Country' => 'PH',
																		        'Premise'=>'PH',
																		        'PostalCode' => $this->input->post('zip_code')
																		    )
															    ),
													'Derivations'=>array(
																		[
																    		'AttributeCode' => 'ca_nickname',
																    		'AttributeValue' =>  $this->input->post('nname')
																    	],
																    	[
																    		'AttributeCode' => 'ca_outletcode',
																    		'AttributeValue' =>  $this->input->post('outlet_code') ? $this->input->post('outlet_code') : '0' //$spice_userinfo->Derivations[1]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_reaction',
																    		'AttributeValue' =>  $this->input->post('alternate_purchase') //$spice_userinfo->Derivations[2]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_points',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[3]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_main_interest1',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[4]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_main_interest2',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[5]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_main_interest3',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[6]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi1_subinterest1',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[7]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi1_subinterest2',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[8]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi2_subinterest1',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[9]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi2_subinterest2',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[10]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi3_subinterest1',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[11]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_mi3_subinterest2',
																    		'AttributeValue' =>  '0' //$spice_userinfo->Derivations[12]->AttributeValue
																    	],
																    	[
																    		'AttributeCode' => 'ca_referral_code',
																    		'AttributeValue' => $this->input->post('referred_code') ? $this->input->post('referred_code') : '0'
																    	],
																    	[
																    		'AttributeCode' => 'ca_personal_referral_code',
																    		'AttributeValue' => '0'
																    	]

														),
													'BrandPreferences'=>array(
										    								'PrimaryBrand'=>array('BrandId'=>$this->input->post('current_brand')),
																			'SecondaryBrand'=>array('BrandId'=>$this->input->post('first_alternate_brand')),
																			'MultibrandSmoker'=>$MultibrandSmoker
																		),
													'LoginType'=>array(
									    						'EncryptedPassword' => $this->spice->encryptPassword(strtolower($this->input->post('email')),uniqid()),
															    'LoginName' => strtolower($this->input->post('email')),
															    'PasswordResetEmail' => strtolower($this->input->post('email'))
									    					),
													'TermsAndConditionsAcceptedList'=>array(
 																					        'TermsAndConditionsCode' => 'TAC1002',
																					        'TermsAndConditionsVersionNumber' => '2'
										    											),
													'MarketingPreference' => $marketingPreference
											   			    									
											)
						);
		$manageperson = $this->spice->ManagePerson($data);
		$response_json = $this->spice->parseJSON($manageperson);
        

		if(!$manageperson || $response_json->MessageResponseHeader->TransactionStatus != 14){

			$data = array('origin_id'=>REGISTRATION,
								'method'=>'ManagePerson',
								'transaction'=>'Upload later registration',
								'input'=>$this->session->userdata('spice_input'),
								'response'=>$manageperson,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
			$this->spice->saveError($data);

			$this->output->set_content_type('application/json')->set_output(json_encode(array('error'=>$response_json->MessageResponseHeader->TransactionStatusMessage,'success'=>'')));
			return;

		}else{

			$sec_settings = $this->db->select()
                ->from('tbl_section_settings')
                ->where(array('name' => 'REGISTRATION'))
                ->get()
                ->row();

            $act_settings = $this->db->select()
                ->from('tbl_activity_settings')
                ->where(array('name' => 'REGISTRATION'))
                ->get()
                ->row();

            $cell_id = $sec_settings->setting_section_id;
			$act_id = $act_settings->setting_activity_id;

			$dateTime = DateTime::createFromFormat('Y-m-d H:i:s', date("Y-m-d").' 00:00:01');
			$DateOfBirth = sprintf(
			   '/Date(%s%s)/',
			   $dateTime->format('U') * 1000,
			   $dateTime->format('O')
			);

			//SPICE
			$this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);
			
			$param = array('CellId' => $cell_id, 'ActionList' =>array(array('ActivityId'   => $act_id, 'ActionValue' => '')));
			$response = $this->spice->trackAction($param);
			$response_json = $this->spice->parseJSON($response);

			if($response_json->MessageResponseHeader->TransactionStatus != 0){
					$data['message'] = '<h3>'.$response_json->MessageResponseHeader->TransactionStatusMessage.'</h3>';

					$data_error = array('origin_id'=>REGISTRATION,
							'method'=>'trackAction',
							'transaction'=>$this->router->method,
							'input'=> $this->session->userdata('spice_input'),
							'response'=>$response,
							'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
						);
					$this->spice->saveError($data_error);
			}
		}

 
		$PersonId = $response_json->PersonId;
		$dlTemplateId = 1028;
		$cellId = 1394;
		$login = $this->spice->createLoginToken($PersonId,$dlTemplateId, $cellId);
		$login_json = $this->spice->parseJSON($login);

		if($login_json->MessageResponseHeader->TransactionStatus!==0){

			$this->output->set_content_type('application/json')->set_output(json_encode(array('error'=>$login_json->MessageResponseHeader->TransactionStatusMessage,'success'=>'')));
			$data = array('origin_id'=>REGISTRATION,
								'method'=>'createLoginToken',
								'transaction'=>'Create login token failed for GIID upload later.',
								'input'=>$this->session->userdata('spice_input'),
								'response'=>$login,
								'response_message'=>$login_json->MessageResponseHeader->TransactionStatusMessage
							);
			$this->spice->saveError($data);
			return;

		}
 
 		$code = 'M00000274';
 		$base_url = str_replace(array('http://','https://'), '', BASE_URL);
		$base_url = $base_url.'user/validate_giid_upload_token'.'?token=' .$login_json->LoginToken;

		$params = ['Email' => strtolower($this->input->post('email')),
		            'MailingCode' => $code,
		            'IsSMTPMail' => false,
		            'Fields' => [
		            				['Name' => 'ph_campaignurlpersonal', "ValueDataType"=>"String", 'ValueAsString' => $base_url],
		                            ['Name' => 'ph_firstname', "ValueDataType"=>"String", 'ValueAsString' => $this->input->post('fname')],
		                            ['Name' => 'ph_lastname', "ValueDataType"=>"String", 'ValueAsString' => $this->input->post('lname')],
		                            ['Name' => 'ph_emailaddress', "ValueDataType"=>"String", 'ValueAsString' => $this->input->post('email')]
		                        ]
		            ];

	   $sendMailing = $this->spice->sendMailing($params);
	   $response_json = $this->spice->parseJSON($sendMailing);

	   if($response_json->MessageResponseHeader->TransactionStatus){ 	  

	   		$data = array('origin_id'=>REGISTRATION,
								'method'=>'sendMailing',
								'transaction'=>$this->router->method,
								'input'=>$this->session->userdata('spice_input'),
								'response'=>$sendMailing,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
			$this->spice->saveError($data);

 			$this->output->set_content_type('application/json')->set_output(json_encode(array('error'=>'Inputs:  <br/>'.$this->session->userdata('spice_input').'<br/><br/> Response: <br/>'.$sendMailing,'success'=>'')));

        } 

 
	}

	public function resend_email_upload_later(){

		if($_POST){
			$this->load->library(array('spice','encrypt'));

			$this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);

			$email = $this->input->post('email');
			$submit_response = array('error'=>1,'message'=>'');
		
			$response =  $this->spice->GetPerson(array('LoginName'=>$email));
			$response_json = $this->spice->parseJSON($response);
				
			$PersonId = $response_json->ConsumerProfiles->PersonId ? $response_json->ConsumerProfiles->PersonId : '';
			$dlTemplateId = 1028;
			$cellId = 1394;
			$login = $this->spice->createLoginToken($PersonId,$dlTemplateId, $cellId);
			$login_json = $this->spice->parseJSON($login);

			if($login_json->MessageResponseHeader->TransactionStatus!==0){

				$this->output->set_content_type('application/json')->set_output(json_encode(array('error'=>$login_json->MessageResponseHeader->TransactionStatusMessage,'success'=>'')));
				$data = array('origin_id'=>REGISTRATION,
									'method'=>'createLoginToken',
									'transaction'=>'Create login token failed for GIID upload later.',
									'input'=>$this->session->userdata('spice_input'),
									'response'=>$login,
									'response_message'=>$login_json->MessageResponseHeader->TransactionStatusMessage
								);
				$this->spice->saveError($data);
				return;

			}
	 
	 		$code = 'M00000274';
	 		$base_url = str_replace(array('http://','https://'), '', BASE_URL);
			$base_url = $base_url.'user/validate_giid_upload_token'.'?token=' .$login_json->LoginToken;

			$params = ['Email' => strtolower($this->input->post('email')),
			            'MailingCode' => $code,
			            'IsSMTPMail' => false,
			            'Fields' => [
			            				['Name' => 'ph_campaignurlpersonal', "ValueDataType"=>"String", 'ValueAsString' => $base_url],
			                            ['Name' => 'ph_firstname', "ValueDataType"=>"String", 'ValueAsString' => $this->input->post('fname')],
			                            ['Name' => 'ph_lastname', "ValueDataType"=>"String", 'ValueAsString' => $this->input->post('lname')],
			                            ['Name' => 'ph_emailaddress', "ValueDataType"=>"String", 'ValueAsString' => $this->input->post('email')]
			                        ]
			            ];

		   $sendMailing = $this->spice->sendMailing($params);
		   $response_json = $this->spice->parseJSON($sendMailing);

		   if($response_json->MessageResponseHeader->TransactionStatus){ 	  

		   		$data = array('origin_id'=>REGISTRATION,
									'method'=>'sendMailing',
									'transaction'=>$this->router->method,
									'input'=>$this->session->userdata('spice_input'),
									'response'=>$sendMailing,
									'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
								);
				$this->spice->saveError($data);

	 			$this->output->set_content_type('application/json')->set_output(json_encode(array('error'=>'Inputs:  <br/>'.$this->session->userdata('spice_input').'<br/><br/> Response: <br/>'.$sendMailing,'success'=>'')));

	        } 
	    }
 		
	}

	public function tracking_code()
	{

		$data = array();
		$data['invalid'] = $this->session->userdata('invalid');
		$message =  $this->session->userdata('message');

		$this->session->set_userdata('invalid','');
		$this->session->set_userdata('message','');

		$layout = $message ? 'user/reset_success' : 'user/tracking_code';
		$data['success_message'] = $message;
		$this->load->view($layout, $data, 'user/template');

	}

	public function submit_track_code()
	{

		$email = $this->input->post('email');
		$submit_response = array('error'=>1,'message'=>'');


		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			
			$this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);
			$response =  $this->spice->GetPerson(array('LoginName'=>$email));
			$response_json = $this->spice->parseJSON($response);

			if (!$response_json->MessageResponseHeader->TransactionStatus) {

				if($response_json->ConsumerProfiles->AgeVerificationStatus === 1){

 					echo json_encode(array('error'=>1,'message'=>'Your account has already been approved. You may now login to your account.'));
 					exit();
 
				}
				
				$ConsumerProfiles = $response_json->ConsumerProfiles;
				$ConsumerProfiles = $this->spice->parseJSON($this->spice->toJSONFormat((array)$ConsumerProfiles));	
 				$PersonId = $ConsumerProfiles->PersonId;
				$dlTemplateId = 1028;
				$cellId = 1394;
				$login = $this->spice->createLoginToken($PersonId,$dlTemplateId, $cellId);
				$login_json = $this->spice->parseJSON($login);
			

				if($login_json->MessageResponseHeader->TransactionStatus===0){

					$code = 'M00000274';
 					$base_url = str_replace(array('http://','https://'), '', BASE_URL);
					$base_url = $base_url.'user/validate_giid_upload_token'.'?token=' .$login_json->LoginToken;

					$params = ['Email' => $email,
					            'MailingCode' => $code,
					            'IsSMTPMail' => false,
					            'Fields' => [   
					            				['Name' => 'ph_campaignurlpersonal', "ValueDataType"=>"String", 'ValueAsString' => $base_url],
					                            ['Name' => 'ph_firstname', "ValueDataType"=>"String", 'ValueAsString' => $ConsumerProfiles->PersonDetails->FirstName],
					                            ['Name' => 'ph_lastname', "ValueDataType"=>"String", 'ValueAsString' => $ConsumerProfiles->PersonDetails->LastName],
					                            ['Name' => 'ph_emailaddress', "ValueDataType"=>"String", 'ValueAsString' => $email]
					                        ]
					            ];



				    $sendMailing = $this->spice->sendMailing($params);
	 			    $response_json = $this->spice->parseJSON($sendMailing);

	 			   

	 			    if($response_json->MessageResponseHeader->TransactionStatus){

	 			    	$data = array('origin_id'=>REGISTRATION,
								'method'=>'sendMailing',
								'transaction'=>'Resend email of GIID upload',
								'input'=> $this->session->userdata('spice_input'),
								'response'=>$sendMailing,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
						$this->spice->saveError($data);

 	 			    	$submit_response['error'] = 1;
						$submit_response['message'] = $response_json->MessageResponseHeader->TransactionStatusMessage.'(sendMailing)';


	 			    }else{


	 			    	$submit_response['error'] = 0;
						$submit_response['message'] = 'Email has been sent for your GIID upload. Please don’t forget to submit a copy of a valid government-issued ID within seven (7) days. Your registration will only be processed upon submission of a valid government-issued ID.';


	 			    }			   

				}else{

					$data = array('origin_id'=>REGISTRATION,
								'method'=>'createLoginToken',
								'transaction'=>'Resend email for GIID upload',
								'input'=> $this->session->userdata('spice_input'),
								'response'=>$login,
								'response_message'=>$login_json->MessageResponseHeader->TransactionStatusMessage
							);
					$this->spice->saveError($data);

					$submit_response['error'] = 1;
					$submit_response['message'] =$login_json->MessageResponseHeader->TransactionStatusMessage.'(createLoginToken)';

				}
 				 

			} else {

 				$submit_response['error'] = 1;
				$submit_response['message'] = $response_json->MessageResponseHeader->TransactionStatusMessage;

				
			}

		}else{


			$submit_response['error'] = 1;
			$submit_response['message'] = ' The email address you entered does not exist. Please try again.';

		}

		$this->output->set_content_type('application/json')->set_output(json_encode($submit_response));

	 	//redirect('submit_giid');

	}

	public function validate_giid_upload_token(){

		$this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);
		$token = $this->input->get('token');

		$response = $this->spice->validateLoginLoken($token);
		$response_json = $this->spice->parseJSON($response);

	 
		if ($response_json->MessageResponseHeader->TransactionStatus) {

			$data = array('origin_id'=>REGISTRATION,
								'method'=>'validateLoginToken',
								'transaction'=>$this->router->method,
								'input'=> $this->session->userdata('spice_input'),
								'response'=>$response,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
			$this->spice->saveError($data);

			$this->session->set_userdata('response_message_error', $response_json->MessageResponseHeader->TransactionStatusMessage);
			redirect('user/login');

		
		}else{

			$redirect_url = $response_json->RedirectUrlParameters;
			redirect($redirect_url.$token);

		}

 	}

	public function giid_upload()
	{

		$token = $this->input->get('token');
		$data = array();
		$data['token'] = $token;
		$this->session->set_userdata('reg_token', $token);
		$this->load->view('user/track_submit', $data, 'user/template');
		  		
	}

	
	public function submit_late_giid()
	{

		$this->load->helper('upload');

		if ($this->input->server('REQUEST_METHOD') != 'POST') {
			$this->output->set_output('Invalid request');
			return;
		}

		if($this->session->userdata('reg_token')!==$this->input->post('__token')){

			$this->output->set_output('Invalid request');
			return;

		}

		$this->spice->loginAdmin(ADMIN_USERNAME, ADMIN_PASSWORD);
		$token = $this->session->userdata('reg_token');
		$token = $this->spice->validateLoginLoken($token);
		$token_json = $this->spice->parseJSON($token);

 
		if ($token_json->MessageResponseHeader->TransactionStatus) {

			$data = array('origin_id'=>REGISTRATION,
								'method'=>'validateLoginToken',
								'transaction'=>$this->router->method,
								'input'=> $this->session->userdata('spice_input'),
								'response'=>$token,
								'response_message'=>$token_json->MessageResponseHeader->TransactionStatusMessage
							);
			$this->spice->saveError($data);

			$this->session->set_userdata('response_message_error', $token_json->MessageResponseHeader->TransactionStatusMessage);
			redirect('user/login');
		}


		$imageString = $this->input->post('captured_image_ba');
		$path = './uploads/registration/';
		$fileTypes = array('.jpeg'=>1,'.png'=>2,'.jpg'=>3,'.bmp'=>4,'.pdf'=>5);	

		if(!$imageString){
 
			$filename = uniqid();
			$max_size = 2048;
			$params = array('upload_path'=>$path,'allowed_types'=>SPICE_FILE_UPLOAD_ALLOWED_TYPES,'file_name'=>$filename,'max_size'=>$max_size,'max_width'=>0,'max_height'=>0,'do_upload'=>'giid');
	    	$file = upload($params);

	    	if(is_array($file)){ 

	    		$imageString = base64_encode(file_get_contents($path.$file['file_name']));
				unlink($path.$file['file_name']);

	    	}else{

	    		$this->output->set_output('<br/>ERROR UPLOAD: '.$file);
	    		return;

	    	}

	    	$IdDocumentFileType = $fileTypes[$file['file_ext']];
			
		}else{

			
			if(!is_dir($path)){
				mkdir($path,0777,true);
			}
			$filename = uniqid().'.jpg';

			file_put_contents($path.$filename, gzuncompress(base64_decode($imageString)));
			$imageString = base64_encode(file_get_contents(BASE_URL.$path.$filename));
			unlink($path.$filename);

			$IdDocumentFileType = $fileTypes['.jpg'];

		}
		
		$updates = array('IdDocumentNumber'=>$this->input->post('giid_number'),
						'IdDocumentTypeCode'=>$this->input->post('giid_type'),
						'VerificationImage'=>[
												'IdDocumentImage'=>"$imageString",
												'IdDocumentFileType'=>$IdDocumentFileType,
												'IdDocumentMarketCode'=>'PH',
												'IdDocumentTypeCode'=>$this->input->post('giid_type'),
												'IdDocumentCountryCode'=>'PH'
											],
						'VerificationInfo'=>[
											'VerificationStatus'=>'0',
											'VerificationTypeCode'=>'7',
											'VerificationSource'=>'0',
											'VerificationSubStatusCode'=>'0'
										]
						);

		$response = $this->spice->updatePerson(array('person_id'=>$token_json->PersonId,'updates'=>$updates));
		$response_json = $this->spice->parseJSON($response);		 

 		
        if(!$response || $response_json->MessageResponseHeader->TransactionStatus != 5){

        	$data = array('origin_id'=>REGISTRATION,
								'method'=>'validateLoginToken',
								'transaction'=>$this->router->method,
								'input'=> $this->session->userdata('spice_input'),
								'response'=>$response,
								'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
							);
			$this->spice->saveError($data);

 			$this->output->set_output('ERROR UPLOAD: '.$response);
 		}

 		

		 
	}

	public function get_brands(){

		$this->db->select('brand_name, brand_id');
		$this->db->order_by('brand_name');
		$this->db->where('consumer_flag', 'true');
 		$rows = $this->db->get('tbl_brands_spice');
 		$this->output->set_output(json_encode($rows->result_array()));
 		
	}

	public function get_giid_types(){

		$this->db->select('ClassName, ClassCode');
  		$this->db->order_by('ClassName');
 		$rows = $this->db->get('tbl_giid_types_spice');
 		$this->output->set_output(json_encode($rows->result_array()));

	}

	public function cities()
	{
		$provinceId = $this->input->get('province');
		if (!$provinceId) {
			return;
		}
		$cities = json_encode($this->Registration_Model->get_cities($provinceId));
		$this->output->set_content_type('application/json')->set_output($cities);
	}
}