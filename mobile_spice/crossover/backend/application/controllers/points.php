<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Points extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
 	}

 	public function user_points()
 	{
 		if($this->input->is_ajax_request()) {
 			$registrant_id = $this->session->userdata('registrant_id');
 			$row = $this->db->select('credit_km_points, total_km_points')->from('tbl_registrants')->where('registrant_id', $registrant_id)->get()->result_array();
 			echo json_encode($row);
 		}
	}

	public function earn_points_activities()
	{
		if($this->input->is_ajax_request()) {
			// $validate = $this->db->select()->from('tbl_registrant_activities')->where('activity_id', $this->input->post('activity_id'))->get();

			if($this->input->post('type') == WATCH_VIDEO) {
				$row = $this->db->select()->from('tbl_registrant_activities')->where('registrant_id', $this->session->userdata('registrant_id'))->where('activity_id', $this->input->post('activity_id'))->get();
				if( ! $row->num_rows()) {
					$this->db->set('total_km_points', 'total_km_points + '.ACTIVITIES_EARN_POINTS, FALSE);
					$this->db->set('credit_km_points', 'credit_km_points + '.ACTIVITIES_EARN_POINTS, FALSE);
					$this->db->where('registrant_id', $this->session->userdata('registrant_id'));
					$this->db->update('tbl_registrants');

					$this->db->set('registrant_id', $this->session->userdata('registrant_id'));
					$this->db->set('activity_id', $this->input->post('activity_id'));
					$this->db->set('activity_type', $this->input->post('type'));
					$this->db->set('points_earned', ACTIVITIES_EARN_POINTS);
					$this->db->set('decision_name', $this->input->post('decision'));
					$this->db->insert('tbl_registrant_activities');
				}
			} else if($this->input->post('type') == READ_CONTENT) {
				$row = $this->db->select()->from('tbl_registrant_activities')->where('registrant_id', $this->session->userdata('registrant_id'))->where('decision_name', $this->input->post('decision'))->get();
				$row_validate = $this->db->select()->from('tbl_registrant_activities')->where('registrant_id', $this->session->userdata('registrant_id'))->where('activity_type', READ_CONTENT)->get();
				if( ! $row->num_rows() && $row_validate < 3) {
					$this->db->set('total_km_points', 'total_km_points + '.ACTIVITIES_EARN_POINTS, FALSE);
					$this->db->set('credit_km_points', 'credit_km_points + '.ACTIVITIES_EARN_POINTS, FALSE);
					$this->db->where('registrant_id', $this->session->userdata('registrant_id'));
					$this->db->update('tbl_registrants');

					$this->db->set('registrant_id', $this->session->userdata('registrant_id'));
					$this->db->set('activity_id', $this->input->post('activity_id'));
					$this->db->set('activity_type', $this->input->post('type'));
					$this->db->set('points_earned', ACTIVITIES_EARN_POINTS);
					$this->db->set('decision_name', $this->input->post('decision'));
					$this->db->insert('tbl_registrant_activities');
				}
			}
		}
	}

}