<?php

class Header_Model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		/* THIS IS A FAKE SESSION DATA! */
		$this->session->set_userdata('collected_km_points', 500);
		$this->session->set_userdata('credit_km_points', 250);
		$this->session->set_userdata('registrant_id', 1);
	}

	public function get_header()
	{
		$data['data_content'] = $this->router->class;
		$data['collected_km_points'] = $this->session->userdata('collected_km_points');
		$data['credit_km_points'] = $this->session->userdata('credit_km_points');
		$data['experience'] = $this->db->select()->from('tbl_experience')->get()->result();
		$view = $this->load->view('headers/main-header', $data);

		return $view;
	}

	public function get_experience_header()
	{
		$data['collected_km_points'] = $this->session->userdata('collected_km_points');
		$data['credit_km_points'] = $this->session->userdata('credit_km_points');
		$data['experience'] = $this->db->select()->from('tbl_experience')->get()->result();
		$view = $this->load->view('headers/experience-header', $data);

		return $view;
	}

	public function get_region($experience_id)
	{
		if($experience_id) {
			$data = $this->db->select('*, tbl_experience.title as experience_title')->from('tbl_experience')->where('tbl_experience.experience_id', $experience_id)->get()->row();
			if($data) {
				return $data->category_id;
			} else {
				return NULL;
			}
		} else {
			return NULL;
		}
	}

}