<?php

class Constant_Model extends CI_Model {
	
	function __construct()
	{
		/* Site Url */
			define('SITE_URL', site_url());

		/* Assets Url */
			define('ASSETS_URL', base_url().'assets/');

		/* Uploads Url */
			define('UPLOADS_URL', base_url().'uploads/');

		/* Gift Types */
			define('GIFT_TYPE_ACTIVITY', 1);
			define('GIFT_TYPE_SPECIAL', 2);
		/* Redemption Types */
			define('GIFT_REDEMPTION_DELIVERY', 1);
			define('GIFT_REDEMPTION_REDEMPTION', 2);
		/* Regions */
			define('REGION_ALASKA', 1);
			define('REGION_CALIFORNIA', 2);
			define('REGION_CANADA', 3);

		/* Points */
			/* Activities */
				define('ACTIVITIES_EARN_POINTS', 40);

				define('READ_CONTENT', 1);
				define('WATCH_VIDEO', 2);
	}
	
}