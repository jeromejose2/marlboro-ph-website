<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {


	public function index(){

		$data['main_content'] = $this->load->view('home','',true);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

	}

	private function nav_items() {

		return $this->load->view('navigations', '', true);

	}

}