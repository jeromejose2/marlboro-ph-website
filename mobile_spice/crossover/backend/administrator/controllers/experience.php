<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Experience extends CI_Controller {
	

	public function __construct()
	{
		parent::__construct();
 	} 

 	 
 	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
 	}
 	 
	
	private function nav_items()
	{
 		return $this->load->view('navigations', '', true);		
	}


	public function main_content()
	{
		$limit = isset($_GET['psize']) ? $_GET['psize'] : 15;
		$curpage = $this->uri->segment(3, 1);
		$data['offset'] = ($curpage - 1) * $limit;

		$data['items'] = $this->db->select()->get('tbl_experience', $limit, $data['offset'])->result_array();
		$data['pagination'] = $this->global_model->pagination(count($data['items']), $curpage, site_url('experience/index'), $curpage, $limit);

		$data['header_text'] = 'Experience';

		$data['header_css_assets'] = array(base_url().'assets/css/ngDialog.min.css',base_url().'assets/css/ngDialog-theme-default.min.css');
 		$data['header_js_assets'] = array(base_url().'assets/js/angular/libraries/angular.min.js');
		$data['footer_js_assets'] = array(base_url().'assets/js/angular/services/dialog.js',
											base_url().'assets/js/angular/services/ngDialog.min.js');

		return $this->load->view('experience-list', $data, TRUE);
	}
 

	public function add()
	{		 
		$data['row_photos'] =  '';
 		$data['header_text'] = 'Add Experience';
 		$data['category'] = $this->global_model->get_rows(array('table'=>'category'));
 		$data['prerequisite'] = $this->global_model->get_rows(array('table'=>'experience'));
 		$data['row'] = false;

 		$data['main_content'] = $this->load->view($this->router->class.'-form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$data['header_js_assets'] = array(base_url().'assets/js/angular/libraries/angular.min.js');
		$data['footer_js_assets'] = array(base_url().'assets/admin/js/angular/modules/experienceModule.js',
											base_url().'assets/js/angular/services/angular-file-upload.min.js',
											base_url().'assets/js/angular/services/angular-file-upload-shim.js');
		$this->load->view('main-template', $data);
 
	}


	public function edit(){

		$data['row_photos'] =  '';
 		$data['header_text'] = 'Edit Experience';
 		$data['category'] = $this->global_model->get_rows(array('table'=>'category'));
 		$data['prerequisite'] = $this->global_model->get_rows(array('table'=>'experience','where'=>array('experience_id !='=>$this->input->get('experience_id'))));
 		$data['row'] = $this->global_model->get_row(array('table'=>'experience','where'=>$this->input->get()));

 		$data['main_content'] = $this->load->view($this->router->class.'-form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$data['header_js_assets'] = array(base_url().'assets/js/angular/libraries/angular.min.js');
		$data['footer_js_assets'] = array(base_url().'assets/admin/js/angular/modules/experienceModule.js',
											base_url().'assets/js/angular/services/angular-file-upload.min.js',
											base_url().'assets/js/angular/services/angular-file-upload-shim.js');
		$this->load->view('main-template', $data);

	}


	public function upload(){

		$this->load->helper('upload');

		$response = array('new_image'=>null,'thumbnail'=>null,'error'=>null);
		$upload_path = './uploads/experience';

		if(!is_dir($upload_path)){
			mkdir($upload_path,0777,true);
		}

		$params = array('upload_path'=>$upload_path,'allowed_types'=>'png|jpg|gif','file_name'=>uniqid(),'max_size'=>'2097152','max_width'=>0,'max_height'=>0,'do_upload'=>'image');
	    $file = upload($params);
 
	    if(is_array($file)){

	    	$this->load->helper('resize');
	    	$params = array('width'=>50,'height'=>50,'source_image'=>$upload_path.'/'.$file['file_name'],'new_image_path'=>$upload_path.'/resized/','file_name'=>$file['file_name']);
 		    //resize($params);

 		    $response['new_image'] = $file['file_name'];
 
	    }else
	    	$response['error'] = $file;
	    
	    $this->load->view('json',array('data'=>$response));

	}

	public function save()
	{
		$config['upload_path'] = './uploads/experience/';
		$config['allowed_types'] = '*';
		$this->load->library('upload', $config);
		if( ! $_POST['experience_id']) {
			$image = 'media';
			$video = 'video';
			if($this->upload->do_upload($image)) {
				$image_data = $this->upload->data();
				unset($_POST['media']);
				$_POST['media'] = base_url().'uploads/experience/'.$image_data['file_name'];
			}
			if($this->upload->do_upload($video)) {
				$video_data = $this->upload->data();
				unset($_POST['video']);
				$_POST['video'] = base_url().'uploads/experience/'.$video_data['file_name'];
			}
			$this->db->insert('tbl_experience', $_POST);
		} else {
			$image = 'media';
			$video = 'video';
			if($this->upload->do_upload($image)) {
				$image_data = $this->upload->data();
				unset($_POST['media']);
				$_POST['media'] = base_url().'uploads/experience/'.$image_data['file_name'];
			}
			if($this->upload->do_upload($video)) {
				$video_data = $this->upload->data();
				unset($_POST['video']);
				$_POST['video'] = base_url().'uploads/experience/'.$video_data['file_name'];
			}
			$this->db->update('tbl_experience', $_POST, array('experience_id'=>$_POST['experience_id']));
		}

		redirect('experience');
	}

	public function delete()
	{

 		$this->global_model->delete('experience',$this->input->get());
		redirect($_SERVER["HTTP_REFERER"]);

	}

	 

	public function validate_form()
	{

		$this->load->library('form_validation');

		$rules = array(
		   array(
				 'field'   => 'title',
				 'label'   => 'Title',
				 'rules'   => 'required'
			  )

		);

		$this->form_validation->set_rules($rules);
		
		return $this->form_validation->run();
	}

	 



	

	public function get_filters()
	   {
		   
		   $where_filters = array('DATE(date_added) >='=>'from_date_added','DATE(date_added) <='=>'to_date_added');
		   $like_filters = array('name'=>'name');
		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
		   
	   } 


	   public function export()
	   {
	   		$this->load->library('to_excel_array');

		   	$filters = $this->get_filters();
		   	$params = 
		   	$query = $this->global_model->get_rows(array('fields'=>"name,date_added",
		   														'table'=>'experience',
											   					'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
											   					'like'=>$filters['like_filters'],
 											   					)
		   													);
		   	$res[] = array('Regions,Date Added');

		   	if($query->num_rows()){
		   		foreach($query->result() as $v){
 		   			$res[] = array($v->name,$v->date_added);
		   		}
		   	}
		   	$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));

	   }


	   

 
}