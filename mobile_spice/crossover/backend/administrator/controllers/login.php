<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{

		$response = array('success'=>1,'message'=>'');
		if($this->input->post()){

			$where = array('username'=>$this->input->post('username'),'password'=>md5($this->input->post('password')));
			$row = $this->global_model->get_row(array('table'=>'tbl_cms_user','where'=>$where));

	 		if($row){
				$response = array('success'=>1,'message'=>'Successfully signed in.');
				$this->session->set_userdata(array('admin_logged_in'=>1));
				redirect('home');
			}

			$response = array('success'=>0,'message'=>'Invalid credentials.');
			
		}

		$this->load->view('login',$response);		

	}

	public function destroy(){
		$array = array('admin_logged_in'=>'');
		$this->session->unset_userdata($array);
		redirect('login');
	}

	
}