<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gifts extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->table = 'tbl_gift';
	}

	public function index()
	{
		$data = array(
			"title" => "Gifts",
			"main_content" => $this->main_content(),
			"nav" => $this->nav_items()
		);
		$this->load->view("main-template", $data);
	}


	public function add()
	{
		$data = array(
			"title" => "Add Gifts",
			"main_content" => $this->add_content(),
			"nav" => $this->nav_items()
		);
 		$this->load->view('main-template', $data);
	}

	private function nav_items() {
 		return $this->load->view('navigations', '', true);		
	}

	// public function delete()
	// {
	// 	if($_POST) {
	// 		foreach($_POST['ids'] as $k => $v) {
	// 			$this->db->where('pack_code_id', $v)->delete('tbl_pack_code');
	// 		}
	// 	}
	// }

	public function edit($id)
	{
		$data = array(
			'title' => 'Edit User',
			'main_content' => $this->edit_content($id),
			'nav' => $this->nav_items()
		);
 		$this->load->view('main-template', $data);
	}

	public function delete($id)
	{
		$this->db->where('gift_id', $id)->delete($this->table);
		redirect('gifts');
	}

	// public function delete($id, $token = ''){
	// 	$user = $this->db->get_where('tbl_cms_users', array('user_id'	=> $id))->result_array();	
	// 	if(!is_numeric($id)) {
	// 		die('Oops... Sorry, the page you are looking for is not available at the moment');
	// 	}
		
	// 	if($token == md5($this->config->item('encryption_key') . $id)) {
	// 		$this->db->where('user_id', $id);
	// 		$this->db->delete('tbl_cms_users');
	// 	}
		
	// 	$params['page'] = 'accounts';
	// 	$params['action_text'] = $this->session->userdata('user_name') . ' deleted an account: ' . $user[0]['uname'];
	// 	$this->main_model->save_audit_log($params);
		
 // 		redirect('accounts');
	// }
	
	public function export()
	{
		$this->_export();
	}


	/******************************************************/

	public function main_content()
	{
		$page = $this->uri->segment(2, 1);
		$limit = isset($_GET['limit']) ? $_GET['limit'] : 15;
		$data['offset'] = ($page - 1) * $limit;

		$this->db->start_cache();
		$this->db->select()->from($this->table);
		if(isset($_GET['name']) && $_GET['name']) {
			$this->db->like('name', $_GET['name']);
		}
		if(isset($_GET['km_points']) && $_GET['km_points']) {
			$this->db->where('km_points', $_GET['km_points']);
		}
		if(isset($_GET['stock']) && $_GET['stock']) {
			$this->db->where('stock', (int)$_GET['stock']);
		}
		$this->db->order_by('gift_id', 'DESC');
		$data['count'] = $this->db->count_all_results();

		$this->db->limit($limit, $data['offset']);
		$data['items'] = $this->db->get()->result_array();
		$this->db->stop_cache();
		$this->db->flush_cache();
		$data['pagination'] = $this->global_model->pagination($data['count'], $page, site_url().'/gifts', $limit);

		$content = $this->load->view('gifts/index', $data, TRUE);
		return $content;
	}

	public function add_content()
	{
		if($this->input->post()) {
			$image = 'image';
			$do_upload = $this->_upload($image);
			if($do_upload) {
				$this->db->set('image', 'uploads/gifts/'.$do_upload['file_name']);
				$this->db->set('name', $this->input->post('name'));
				$this->db->set('km_points', $this->input->post('km_points'));
				$this->db->set('stock', $this->input->post('stock'));
				$this->db->set('description', $this->input->post('description'));
				$this->db->set('date_created', date('Y-m-d H:i:s'));
				$this->db->set('type', $this->input->post('type'));
				$this->db->set('order', $this->input->post('order'));
				$this->db->set('redemption_type', $this->input->post('redemption_type'));
				$this->db->set('region', $this->input->post('region'));
				$this->db->insert($this->table);
			}
			redirect('gifts');
		}

		$content = $this->load->view('gifts/add', NULL, TRUE);
		return $content;
	}

	public function edit_content($id)
	{
		if($_POST) {
			$param['name'] = $this->input->post('name');
			$param['km_points'] = $this->input->post('km_points');
			$param['stock'] = $this->input->post('stock');
			$param['description'] = $this->input->post('description');
			$param['type'] = $this->input->post('type');
			$param['order'] = $this->input->post('order');
			$param['redemption_type'] = $this->input->post('redemption_type');
			$param['region'] = $this->input->post('region');
			if($_FILES['image']['name']) {
				$image = 'image';
				$do_upload = $this->_upload($image);
				if($do_upload) {
					$param['image'] = 'uploads/gifts/'.$do_upload['file_name'];
				}
			}
			$this->db->update($this->table, $param, array('gift_id'=>$id));
			redirect('gifts');
		}		

		$data['data'] = $this->db->select()->from($this->table)->where('gift_id', $id)->get()->result_array();

		if($data) {
			return $this->load->view('gifts/edit', $data['data'][0], TRUE);
		} else {
			redirect('gifts');
		}
	}

	public function _upload($image)
	{
		$config['upload_path'] = './uploads/gifts/';
		$config['allowed_types'] = '*';
		$this->load->library('upload', $config);

		if( ! $this->upload->do_upload($image)) {
			return FALSE;
		} else {
			return $this->upload->data();
		}
	}

	public function _export()
	{
		$this->load->library('to_excel_array');

		$this->db->start_cache();
		$this->db->select()->from($this->table);
		if(isset($_GET['name']) && $_GET['name']) {
			$this->db->like('name', $_GET['name']);
		}
		if(isset($_GET['km_points']) && $_GET['km_points']) {
			$this->db->where('km_points', $_GET['km_points']);
		}
		if(isset($_GET['stock']) && $_GET['stock']) {
			$this->db->where('stock', (int)$_GET['stock']);
		}
		$this->db->order_by('gift_id', 'DESC');
		$export_items = $this->db->get()->result_array();
		$this->db->stop_cache();
		$this->db->flush_cache();

		$row[] = array(
			'#',
			'Title',
			'Required Points(KM)',
			'Stock',
			'Image',
			'Description'
		);
		if($export_items) {
			$i = 0;
			foreach($export_items as $k => $v) {
				$i++;
				$row[] = array(
					$i,
					$v['name'],
					$v['km_points'],
					$v['stock'],
					$v['image'],
					$v['description']
				);
			}
		}
		$this->to_excel_array->to_excel($row, 'gifts_'.date('YmdHis'));
	}

	public function _remap($method)
	{
		if(is_numeric($method)) {
			$this->index();
		} else if($method == 'index') {
			$this->index();
		} else if($method == 'add') {
			$this->add();
		} else if($method == 'edit') {
			$id = $this->uri->segment(3);
			$this->edit($id);
		} else if($method == 'delete') {
			$id = $this->uri->segment(3);
			$this->delete($id);
		}
	}

}

?>
