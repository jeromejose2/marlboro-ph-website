<div class="page-header">

               <h3><?=$header_text?></h3>



               <div class="actions">

                    <a href="<?=site_url($this->router->class.'/add')?>" class="btn btn-primary">New Item</a>

               </div>

          </div>

          <div ng-app="dialogModule" ng-controller="activityList" >

          <table class="table table-bordered">
               <thead>
                    <tr>
                      <th><center>Image</center></th>
                         <th><center>Name</center></th>
                         <th>Description</th>
                         <th>Address</th>
                         <th>Points</th>
                         <th>Date Created</th>
                         <th>Action</th>
                    </tr>
               </thead>

               <tbody>

                    <?php if($rows){

                         foreach($rows->result() as $row){ ?>

                         <tr>
                          <td><img src="<?=base_url('uploads/event/50_50_'.$row->image)?>"></td>
                              <td><?=$row->name?></td>
                              <td><?=$row->description?></td>
                              <td><?=$row->address?></td>
                              <td><?=$row->km_points?></td>
                              <td><?=$row->date_created?></td>
                              <td>
                                
                                   <a href="<?=site_url($this->router->class.'/edit?event_id='.$row->event_id)?>" class="btn btn-default">
                                        <span class="glyphicon glyphicon-pencil"></span> Edit</a>
                                   <a ng-click="openConfirm('<?=site_url($this->router->class.'/delete?event_id='.$row->event_id)?>')" class="btn btn-danger">
                                        <span class="glyphicon glyphicon-trash"></span> Remove</a>
                              </td>
                         </tr>

                    <?php }
                    } ?>                    

               </tbody>



          </table>

          <script type="text/ng-template" id="modalDialogId">
              <div class="ngdialog-message">        
                <p>Are you sure you want to remove this?</p>
              </div>
              <div class="ngdialog-buttons">
                <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(confirmValue)">Confirm</button>
                <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog('button')">Cancel</button>
              </div>
          </script>

     </div>

          