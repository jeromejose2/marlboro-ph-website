<!DOCTYPE html>
<html>
<head>
     <title>CMS</title>
     <!-- Bootstrap -->
     <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/bootstrap.min.css" >
     <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/style.css" >
     <script src="<?=base_url()?>assets/admin/js/jquery.js"></script>
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
     <![endif]-->
     <?php if(@$header_css_assets){ 
               foreach($header_css_assets as $v){
                    echo '<link rel="stylesheet" href="'.$v.'">';
               }
          }?>

     <?php if(@$header_js_assets){ 
               foreach($header_js_assets as $v){
                    echo '<script src="'.$v.'"></script>';
               }
          }?>
</head>
<body>


     <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <?=$nav?>
     </div>
     
     <div class="container">          
          <?=$main_content?>         
     </div>

     

     <footer  class="main">
          <small>Nuworks Interactive Labs &copy; 2014</small>
     </footer>
     
     <script src="<?=base_url()?>assets/admin/js/bootstrap.min.js"></script>

     <?php if(@$footer_js_assets){ 
          foreach($footer_js_assets as $v){
               echo '<script src="'.$v.'"></script>';
          }
     }?>

</body>
</html>