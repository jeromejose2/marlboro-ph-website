<div class="page-header">

               <h3><?=$header_text?></h3>

          </div>

          <form class="form-horizontal" role="form" method="POST" action="<?=site_url('experience/save')?>" enctype="multipart/form-data">
              <input type="hidden" name="experience_id" value="<?=isset($row->experience_id) ? $row->experience_id : ''?>">

               <div class="form-group">

                    <label class="col-sm-2 control-label">Region</label>

                    <div class="col-sm-4">

                         <select name="category_id" class="form-control">
                              <option value="<?=REGION_CALIFORNIA?>" <?=isset($row->category_id) && $row->category_id == REGION_CALIFORNIA ? 'selected' : ''?>>California</option>
                              <option value="<?=REGION_ALASKA?>" <?=isset($row->category_id) && $row->category_id == REGION_ALASKA ? 'selected' : ''?>>Alaska</option>
                              <option value="<?=REGION_CANADA?>" <?=isset($row->category_id) && $row->category_id == REGION_CANADA ? 'selected' : ''?>>Canada</option>
                         </select>

                    </div>

               </div>

               <div class="form-group">

                    <label class="col-sm-2 control-label">Order</label>

                    <div class="col-sm-4">

                         <select name="order" class="form-control">
                              <option value="1" <?=isset($row->order) && $row->order == 1 ? 'selected' : ''?>>1st</option>
                              <option value="2" <?=isset($row->order) && $row->order == 2 ? 'selected' : ''?>>2nd</option>
                              <option value="3" <?=isset($row->order) && $row->order == 3 ? 'selected' : ''?>>3rd</option>
                         </select>

                    </div>

               </div>


               <div class="form-group">

                    <label class="col-sm-2 control-label">Title</label>

                    <div class="col-sm-4">

                         <input type="text" name="title" class="form-control" value="<?=isset($row->title) ? $row->title : ''?>">

                    </div>

               </div>

               <div class="form-group">

                    <label class="col-sm-2 control-label">Landing Image</label>

                    <div class="col-sm-4">

                         <input type="file" name="media" class="form-control" id="landing-image" onchange="Image(this, 'landing-image-preview')">

                    </div>

               </div>

               <div class="form-group">

                    <label class="col-sm-2 control-label"></label>

                    <div class="col-sm-4">

                         <img src="<?=isset($row->media) ? $row->media : ''?>" id="landing-image-preview">

                    </div>

               </div>

               <div class="form-group">

                    <label class="col-sm-2 control-label">Points in KM</label>

                    <div class="col-sm-4">

                         <input type="text" name="km_points" class="form-control" value="<?=isset($row->km_points) ? $row->km_points : ''?>">

                    </div>

               </div>

               

               <div class="form-group">

                    <label class="col-sm-2 control-label">Video</label>

                    <div class="col-sm-4">

                         <input type="file" name="video" class="form-control">

                    </div>

               </div>

               <div class="form-group">

                    <div class="col-sm-offset-2 col-sm-4">
                         <a href="<?=site_url('experience')?>" class="btn btn-default">Cancel</a>
                         <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

               </div>

          </form>


<script>
  function Image(input, target) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#'+target).attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]);
      }
  }
</script>