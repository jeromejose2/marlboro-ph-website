<div class="page-header">

               <h3><?=$header_text?></h3>



               <div class="actions">

                    <a href="<?=site_url()?>/experience/add" class="btn btn-primary">New Item</a>

               </div>

          </div>

          <div ng-app="dialogModule" ng-controller="experienceList" >

          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th><center>Category</center></th>
                         <th>Title</th>
                         <th>Points</th>
                         <th>Date Created</th>
                         <th>Action</th>
                    </tr>
               </thead>

               <tbody>

                    <?php if($items){
                          $regions = array('', 'Alaska', 'Canada', 'California');
                         foreach($items as $k => $v){ ?>

                         <tr>
                              <td><?=$regions[$v['category_id']]?></td>
                              <td><?=$v['title']?></td>
                              <td><?=$v['km_points']?>km</td>
                              <td><?=$v['date_created']?></td>
                              <td>
                                   <a href="<?=site_url($this->router->class.'/edit?experience_id='.$v['experience_id'])?>" class="btn btn-default">
                                        <span class="glyphicon glyphicon-pencil"></span> Edit</a>
                                   <a ng-click="openConfirm('<?=site_url($this->router->class.'/delete?experience_id='.$v['experience_id'])?>')" class="btn btn-danger">
                                        <span class="glyphicon glyphicon-trash"></span> Remove</a>
                              </td>
                         </tr>

                    <?php }
                    } ?>                    

               </tbody>



          </table>

          <script type="text/ng-template" id="modalDialogId">
              <div class="ngdialog-message">        
                <p>Are you sure you want to remove this?</p>
              </div>
              <div class="ngdialog-buttons">
                <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(confirmValue)">Confirm</button>
                <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog('button')">Cancel</button>
              </div>
          </script>

     </div>

          