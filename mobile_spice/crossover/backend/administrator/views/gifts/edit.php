<!-- js -->
<script>
  $(function() {
    $("#startdate, #enddate").datepicker({dateFormat : "yy-mm-dd"});
    $(".number-only").keypress(validateNumber);
    $('.alert').hide();
  });

  function submitForm() {
    $('.alert').hide();
    theForm = $('form');
    error = validateForm(theForm);
    data = false;
    if(error) {
      $('.alert').show().html(error);
      $(document).scrollTop(0);
      return false;
    } else {
      return true;
    }
    return false;
  }

  function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;

    if (event.keyCode == 8 || event.keyCode == 46
     || event.keyCode == 37 || event.keyCode == 39) {
        return true;
    }
    else if ( key < 48 || key > 57 ) {
        return false;
    }
    else return true;
}
</script>
<!-- /js -->
<noscript>
  <style>
  .col-xs-4 {
    display: none;
  }
  </style>
</noscript>
<script src="<?php echo BASE_URL ?>admin_assets/js/bootstrap-tagsinput.js"></script>
<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3>Edit Gift</h3>
      </div>

       <!-- <div class="alert alert-danger">  </div> -->

       <form class="form-horizontal" role="form" method="post" id="add-form-product-info" action="<?= site_url() ?>/gifts/edit/<?= $gift_id ?>" onsubmit="return submitForm()" enctype="multipart/form-data">

          <div class="form-group">
                <label class="col-sm-2 control-label">Region</label>
                <div class="col-sm-4">
                     <select class="form-control" name="region">
                        <option value="<?=REGION_CALIFORNIA?>" <?=$region == REGION_CALIFORNIA ? 'selected' : ''?>>California</option>
                        <option value="<?=REGION_ALASKA?>" <?=$region == REGION_ALASKA ? 'selected' : ''?>>Alaska</option>
                        <option value="<?=REGION_CANADA?>" <?=$region == REGION_CANADA ? 'selected' : ''?>>Canada</option>
                     </select>
                </div>
           </div>

           <div class="form-group">
              <label class="col-sm-2 control-label"></label>
              <div class="col-sm-4" id="preview-container">
                  <?php if($image): ?>
                    <img width="200" src="<?=base_url().$image?>">
                  <?php endif; ?>
              </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Image</label>
                <div class="col-sm-4">
                     <input type="file" name="image" data-name="image" id="upload-image" onchange="imagePreview(this)" class="" value="1">
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Title</label>
                <div class="col-sm-4">
                     <input type="text" name="name" data-name="title" class="required form-control" value="<?=$name?>">
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Gift Type</label>
                <div class="col-sm-4">
                     <select class="form-control" name="type">
                        <option value="<?=GIFT_TYPE_ACTIVITY?>" <?=$type == GIFT_TYPE_ACTIVITY ? 'selected' : ''?>>Activity</option>
                        <option value="<?=GIFT_TYPE_SPECIAL?>" <?=$type == GIFT_TYPE_SPECIAL ? 'selected' : ''?>>Special</option>
                     </select>
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Order</label>
                <div class="col-sm-4">
                     <input type="text" name="order" data-name="order" class="required form-control" value="<?=$order?>">
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Required KM</label>
                <div class="col-sm-4">
                     <input type="text" name="km_points" data-name="required km" class="required form-control number-only" value="<?=$km_points?>">
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Stock</label>
                <div class="col-sm-4">
                     <input type="text" name="stock" data-name="stock" class="required form-control number-only" value="<?=$stock?>">
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-4">
                    <textarea class="form-control" name="description"><?=$description?></textarea>
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Redemption Type</label>
                <div class="col-sm-4">
                     <select class="form-control" name="redemption_type">
                        <option value="<?=GIFT_REDEMPTION_DELIVERY?>" <?=$redemption_type == GIFT_REDEMPTION_DELIVERY ? 'selected' : ''?>>Delivery</option>
                        <option value="<?=GIFT_REDEMPTION_REDEMPTION?>" <?=$redemption_type == GIFT_REDEMPTION_REDEMPTION ? 'selected' : ''?>>Redemption</option>
                     </select>
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                      <a href="<?= site_url() ?>/gifts" class="btn btn-primary">Back</a>
                     <button type="submit" class="btn btn-primary" name="submit" value="1" id="start-generation">Submit</button>
                     <img style="display:none" src="<?=base_url()?>/admin_assets/images/spinner.gif" id="loader">
                </div>
           </div>
      </form>
      
 </div>
 <!--end main content -->
 
<!-- js -->
<script>
  function imagePreview() {
    var FR = new FileReader();
    FR.readAsDataURL(document.getElementById('upload-image').files[0]);
    FR.onload = function(FREvent) {
      $('#preview-container').html('<img width="200" src="'+FREvent.target.result+'">').show();
    }
  }
</script>
<!-- /js -->