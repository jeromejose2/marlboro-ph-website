
          <!-- Brand and toggle get grouped for better mobile display -->

          <div class="navbar-header">

          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">

          <span class="sr-only">Toggle navigation</span>

          <span class="icon-bar"></span>

          <span class="icon-bar"></span>

          <span class="icon-bar"></span>

          </button>

          <a class="navbar-brand" href="#">NuWorks</a>

          </div>



          <!-- Collect the nav links, forms, and other content for toggling -->

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

               <ul class="nav navbar-nav">

                    <li class="<?=$this->router->class=='home'? 'active' :''?>"><a href="<?=site_url('home')?>">Home</a></li>                    
                    <li class="<?=$this->router->class=='experience'? 'active' :''?>"><a href="<?=site_url('experience')?>">Experiences</a></li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Activities <b class="caret"></b></a>
                        <ul class="dropdown-menu" role="menu">
                          <li class="<?=$this->router->class=='activity'? 'active' :''?>"> <a href="<?=site_url('activity')?>">Activities</a></li>
                          <li class="<?=$this->router->class=='activity_media'? 'active' :''?>"> <a href="<?=site_url('activity_media')?>">Medias</a></li>
                        </ul>
                    </li>
                    <li class="<?=$this->router->class=='gifts'? 'active' :''?>"><a href="<?=site_url('gifts')?>" >Gifts</a></li>                                    
                    <li class="<?=$this->router->class=='event'? 'active' :''?>"><a href="<?=site_url('event')?>" >Events</a></li>                                    
                    
               </ul>
                <ul class="nav navbar-nav navbar-right">
                <li><a href="<?=base_url('login/destroy')?>">Logout</a></li>
                     </ul>
          </div><!-- /.navbar-collapse -->
     <!--end header-->