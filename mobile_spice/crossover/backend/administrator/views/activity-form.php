<!-- js -->
<script type="text/javascript">
  $(function() {
    $('#left-image-preview').hide();
  });

  function uploadImage(pos) {
    
  }
</script>
<!-- js -->

<div class="page-header">

               <h3><?=$header_text?></h3>

          </div>

              <form class="form-horizontal" role="form" method="POST" action="<?=site_url('activity/save')?>" enctype="multipart/form-data">
                              <input type="hidden" name="activity_id" value="<?=$row && $row->activity_id ? $row->activity_id : ''?>">

               <div class="form-group">

                    <label class="col-sm-2 control-label">Experience</label>

                    <div class="col-sm-4">

                                               <select class="form-control" name="experience_id">

                              <option value=""></option>

                             <?php if($experiences->num_rows()){
                                        foreach($experiences->result() as $v){ ?>   
                                             <option value="<?=$v->experience_id?>" <?=isset($row->experience_id) && $row->experience_id == $v->experience_id ? 'selected' : ''?>><?=$v->title?></option>
                                  <?php }
                             } ?>
                         </select>

                    </div>

               </div>

               <div class="form-group">

                    <label class="col-sm-2 control-label">Title</label>

                    <div class="col-sm-4">

                                                    <input type="text" name="title" class="form-control" value="<?=isset($row->title) ? $row->title : ''?>">


                    </div>

               </div>

               <div class="form-group">

                    <label class="col-sm-2 control-label">Description</label> 
 
                    <div class="col-sm-4">  
 
 
                                                  <textarea name="description" class="form-control"><?=isset($row->description) ? $row->description : ''?></textarea>

                    </div>

               </div>


               <div class="form-group">

                    <label class="col-sm-2 control-label">Decision Left Title</label> 
 
                    <div class="col-sm-4">  
 
 
                                                  <input type="text" name="decision_left_title" class="form-control" value="<?=isset($row->decision_left_title) ? $row->decision_left_title : ''?>">

                    </div>

               </div>

               <div class="form-group">

                    <label class="col-sm-2 control-label">Decision Left Description</label> 
 
                    <div class="col-sm-4">  
 
 
                                                  <textarea type="text" name="decision_left_description" class="form-control"><?=isset($row->decision_left_description) ? $row->decision_left_description : ''?></textarea>

                    </div>

               </div>

                              <div class="form-group">

                    <label class="col-sm-2 control-label">Decision Left Image</label> 
 
                    <div class="col-sm-4">  
 
 
                          <input type="file" class="form-control" name="left_image" onchange="Image(this, 'left-image')">
                    </div>

               </div>

                  <div class="form-group">

                    <label class="col-sm-2 control-label"></label> 

                  <div class="col-sm-4">

                    <img src="<?=isset($row->left_image) && $row->left_image ? $row->left_image : ''?>" id="left-image">

                  </div>

               </div>



               <div class="form-group">

                    <label class="col-sm-2 control-label">Decision Right Title</label> 
 
                    <div class="col-sm-4">  
 
 
                                                  <input type="text" name="decision_right_title" class="form-control" value="<?=isset($row->decision_right_title) ? $row->decision_right_title : ''?>">

                    </div>

               </div>

               <div class="form-group">

                    <label class="col-sm-2 control-label">Decision Right Description</label> 
 
                    <div class="col-sm-4">  
 
 
                                                  <textarea type="text" name="decision_right_description" class="form-control"><?=isset($row->decision_right_description) ? $row->decision_right_description : ''?></textarea>

                    </div>

               </div>

               <div class="form-group">

                    <label class="col-sm-2 control-label">Decision Right Image</label> 
 
                    <div class="col-sm-4">  
 
 
                                                                            <input type="file" class="form-control" name="right_image" onchange="Image(this, 'right-image')">


                    </div>

               </div>

               <!-- -->
                              <div class="form-group">

                    
                    <label class="col-sm-2 control-label"></label> 

                              
                  <div class="col-sm-4">

                                   
                    <img src="<?=isset($row->right_image) && $row->right_image ? $row->right_image : ''?>" id="right-image">

                                  
                  </div>

                                        
               </div>
               <!-- -->
              




               <div class="form-group">

                    <div class="col-sm-offset-2 col-sm-4">
                         <a href="<?=base_url($this->router->class)?>" class="btn btn-default">Cancel</a>
                         <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

               </div>

          </form>

     </div>