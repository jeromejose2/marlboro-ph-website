'use strict';

var CMSApp = angular.module('eventModule',['angularFileUpload']);


CMSApp.controller('eventCtrl',['$rootScope','$scope','$upload','$timeout','$http','$window',function($rootScope,$scope,$upload,$timeout,$http,$window){

 
 	$scope.assetBaseURL = '../assets/';
  	$scope.siteURL = '../';
   	$scope.selectedFiles =[];

 

	$scope.abort = function(index) {
		$scope.upload[index].abort(); 
		$scope.upload[index] = null;
	};

	$scope.test = function(){
		alert('test');
	}

	$scope.onFileSelect = function($files)
	{
		$scope.selectedFiles = $files;
		$scope.upload = [];		
		$scope.dataUrls = [];
		$scope.progress = [];
		$scope.errorUpload = [];

		if ($scope.upload && $scope.upload.length > 0) {
			for (var i = 0; i < $scope.upload.length; i++) {
				if ($scope.upload[i] != null) {
					$scope.upload[i].abort();
				}
			}
		}

		for ( var i = 0; i < $files.length; i++) {
			var $file = $files[i];
			if (window.FileReader && $file.type.indexOf('image') > -1) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[i]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
						});
					}
				}(fileReader, i);
			}
			$scope.progress[i] = -1;
 			$scope.start(i);
		}


	};

	 
	$scope.start = function(index) {

		$scope.progress[index] = 0;	

		$scope.upload[index] = $upload.upload({
			url : $scope.siteURL+'event/upload',
			method: 'POST',
			headers: {'my-header': 'my-header-value'},
			file: $scope.selectedFiles[index],
			fileFormDataName: 'image'
		}).then(function(response) {
			var response = response.data;
			
			if(!response.error){
				$scope.item.image = response.new_image;
			}else{
				$scope.errorUpload[index] = response.error;
			}
 
		}, null, function(evt) {
			$scope.progress[index] = parseInt(100.0 * evt.loaded / evt.total);
		}).xhr(function(xhr){
			xhr.upload.addEventListener('abort', function(){console.log('aborted complete')}, false);
		});

	}


	$scope.remove = function(index) {

		
		 
		$scope.selectedFiles.splice(index, 1);
		$scope.dataUrls.splice(index, 1);
		$scope.progress.splice(index, 1);
		$scope.errorUpload.splice(index, 1);
		$scope.media_files.splice(index,1);
		console.log($scope.media_files);
 
	}

	$scope.removeCurrent = function(index){

 		$scope.current_media_files.splice(index,1);


	}


	$scope.addItem = function(){

		
		 
 

		$http.post($scope.siteURL+'api/event',$scope.item).success(function(response){
				if(response.success){
					$window.location.href = $scope.siteURL+'event';
				}else{
					$scope.errorUpload = response.message;
				}
		});

	}


}])
.directive('datepicker',function(){

	return {
		restrict : "A",
		link: function(scope,element,attrs){
			console.log(element);
		}
	}

});