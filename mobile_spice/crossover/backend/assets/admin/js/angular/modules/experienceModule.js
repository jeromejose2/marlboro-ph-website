'use strict';

var CMSApp = angular.module('experienceModule',['angularFileUpload']);

CMSApp.controller('experienceCtrl',['$rootScope','$scope','$upload','$timeout','$http','$window',function($rootScope,$scope,$upload,$timeout,$http,$window){

	$scope.selectedFiles = [];
	$scope.assetBaseURL = '../assets/';
	$scope.siteURL = '../';
	$scope.preLoader = $scope.assetBaseURL+'img/loading.gif';
	$scope.errorUpload = false;
 	$scope.progress = -1;
 	$scope.item = {};


	$scope.onFileSelect = function($files){


		$scope.selectedFiles = $files;
		$scope.errorUpload = false;
		$scope.progress = 0;
		var i = 0;
		var $file = $files[i];
		if (window.FileReader) {
			var fileReader = new FileReader();
			fileReader.readAsDataURL($files[i]);
			var loadFile = function(fileReader, index) {
				fileReader.onload = function(e) {
					$timeout(function() {
						$scope.dataUrl = e.target.result;
					});
				}
			}(fileReader, i);

		}else{
			$scope.errorUpload = 'The filetype you are attempting to upload is not allowed.';
			return;
		}



		$upload.upload({
				url : $scope.siteURL+'experience/upload',
				method: 'POST',
				headers: {'my-header': 'my-header-value'},
				file: $scope.selectedFiles[i],
				fileFormDataName: 'image'
			}).then(function(response) {
				var response = response.data;
				if(!response.error){
					$scope.item.media = response.new_image;
 				}else
					$scope.errorUpload = response.error;

			}, null, function(evt) {				
				$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
			}).xhr(function(xhr){
				xhr.upload.addEventListener('abort', function(){console.log('aborted complete')}, false);
			});

	};


	$scope.addItem = function(){


		$http.post($scope.siteURL+'api/experience',$scope.item).success(function(response){
				if(response.success){
					$window.location.href = $scope.siteURL+'experience';
				}else{
					$scope.errorUpload = response.message;
				}
		});

	}


	 


}]);