'use strict';

var CMSApp = angular.module('AppModule',['ngRoute','ngResource','AppControllers']);

CMSApp.config(['$routeProvider','$resourceProvider',function($routeProvider,$resourceProvider){

	$routeProvider
	.when('/login',{
		templateUrl:'login.html',
		controller:'loginCtrl',
		title:'Login'
	})
	.when('/home',{
		templateUrl:'home.html',
		controller:'homeCtrl',
		title:'Home'
	})
	.when('/experiences',{
		templateUrl:'experience-list.html',
		controller:'experienceCtrl',
		title:'Experiences'
	})
	.when('/experiences/add',{
		templateUrl:'experience-add.html',
		controller:'experienceAddCtrl',
		title:'Experiences'
 	})
	.otherwise({
		redirectTo:'/login'
	});

}]);
