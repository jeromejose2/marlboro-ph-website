$(window).load(function(){
	setInterval(function(){
		updateUserPoints();
	}, 3000);
});

function updateUserPoints()
{
	$.getJSON(siteUrl+'points/user_points', function (response) {
		$('.credit_km_points').html(response[0].credit_km_points);
		$('.collected_km_points').html(response[0].total_km_points);
	});
}

function validateCode()
{
	var formCode = $('form').find('input:first-child').val();
	var error_container = $('.notifMsg');
	var congrats_container = $('.congrats');
	var congrats_text = congrats_container.find('span');
	var err_container = $('.error');
	var err_text = err_container.find('span');

	$.post(siteUrl+'code_validation', {code : formCode}, function (response) {
		error_container.hide();
		congrats_container.hide();
		err_container.hide();
		if(typeof response !== 'number') {
			error_container.show();
			err_container.show();
			err_text.html(response);
			return false;
		} else {
			error_container.show();
			congrats_container.show();
			congrats_text.html('Congrats, you earned points!');
		}
		return false;
	});
	return false;
}

function earnPoints(type, name)
{
	$.post(siteUrl+'points/earn_points_activities', {type:type, activity_id:segments, decision:name}, function(response) {
		console.log(response);
	});
}