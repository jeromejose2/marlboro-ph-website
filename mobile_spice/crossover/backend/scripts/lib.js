popup.loading = function(message) {
	if (message == undefined) {
	message = '<div class="preload"></div>';
	}
	this.dialog({
		align 	 : "center",
		addClass : 'lytebox-preload',
		message  : message,
		bgClose : false,
		escClose : false,
		close : false
	});
}

function User() {

	var fromMobile = $(window).width() < 768;
	var pointsUrl = BASE_URL + "api/points";
	var notificationsUrl = BASE_URL + "api/notifications";
	var newsfeedUrl = BASE_URL + "api/newsfeed";
	var moveforwardUrl = BASE_URL + "api/move_forward_choose";
	var visitsUrl = BASE_URL + "api/visits";
	var signedOutCallback = null;
	var signedOut = false;
	var multipleSessionDetectedCallback = null;
	var topInterest = BASE_URL + "profile/get_myinterest"

	this.getNotifications = function(onSuccess) {
		if (signedOut)
			return;
		$.ajax({
			url : notificationsUrl,
			type : "GET",
			statusCode : {
				404 : function() {
					if (typeof signedOutCallback == "function")
						signedOutCallback();
					signedOut = true;
				}
			},
			dataType : "JSON",
			success : function(response) {
				if (typeof onSuccess == "function")
					onSuccess(response);
			}
		});
	}

	this.playMoveForward = function(moveForwardId, onSuccess) {
		$.post(
			moveforwardUrl,
			{
				action : MoveFwd.PLAY,
				offer : moveForwardId
			},
			function(response) {
				if (typeof onSuccess == "function")
					onSuccess(response);
			}
		);
	}

	this.pledgeMoveForward = function(moveForwardId, onSuccess) {
		$.post(
			moveforwardUrl,
			{
				action : MoveFwd.PLEDGE,
				offer : moveForwardId
			},
			function(response) {
				if (typeof onSuccess == "function")
					onSuccess(response);
			}
		);
	}

	this.visit = function(origin, suborigin, onSuccess) {
		$.post(visitsUrl, { origin : origin, suborigin : suborigin }, function(response) {
			if (typeof onSuccess == "function")
				onSuccess(response);
		});
	}

	this.getVisitsCount = function(origin, suborigin) {
		$.get(visitsUrl, { origin : origin, suborigin : suborigin }, function(response) {
			if (typeof onSuccess == "function")
				onSuccess(response);
		});
	}

	this.signedOut = function(callback) {
		signedOutCallback = callback;
	}

	this.multipleSessionDetected = function(callback) {
		if (callback == undefined) {
			multipleSessionDetectedCallback();
			return;
		}
		multipleSessionDetectedCallback = callback;
	}

	this.fromMobile = function() {
		return fromMobile;
	}

	this.earn = function(origin, params, onSuccess) {
		if (typeof params != "object") {
			params = {};
		}
		params.origin = origin;
		$.post(pointsUrl, params, function(response) {
			if (typeof onSuccess == "function")
				onSuccess(response);
		});
	}

	this.getNewsfeed = function(limit, onSuccess) {
		if (onSuccess == undefined) {
			onSuccess = limit;
			limit = 10;
		}
		$.get(newsfeedUrl, function(response) {
			if (typeof onSuccess == "function")
				onSuccess(response);
		});
	}
	this.getTopInterest = function(){

  		$.ajax({url:topInterest,
				dataType:'JSON',
				data:{limit:1},
				success: function(obj) {
 					var obj = obj.selectedInterests;

					$.each(obj, function(i,val){
						$('#profiling_nav').html('<a href="'+BASE_URL+'profile/interest" style="background-image: url('+val.image+')"></a>');
					});

				}
			 
			});		
	}

	$(window).resize( function() {
		fromMobile = $(this).width() < 768;
	});
	var self = this;
}

function Task() {

	this.notify = function(callback, interval) {
		if (typeof callback != "function") {
			console.log("Task.notify parameter must be a function");
			return;
		}
		if (interval == undefined) {
			interval = 5000;
		}
		setTimeout( function() {
			User.getNotifications( function(response) {
				if (typeof response.force_logout != "undefined" && response.force_logout) {
					User.multipleSessionDetected();
				} else {
					callback(response);
					self.notify(callback);
				}
			});
		}, interval);
	}

	var self = this;
}

function CommentBox() {

	function CommentsList(container, params) {

		if (params == undefined) {
			params = { base_url : BASE_URL };
		} else if (typeof params.base_url == "undefined") {
			params.base_url = BASE_URL;
		}

		var commentsUrl = params.base_url + "api/comments";
		var idPair = params.origin + "-" + params.suborigin;
		var replyIdPair = idPair + "-reply";
		var hasCommentAttachment = false;
		var hasReplyAttachment = false;
		var currentCommentsPage = 1;
		var recipientId = 0;
		var currentReplyCommentId = 0;
		var sending = false;
		var commentsParentBody = $("body");

		this.ga = {}

		this.templates = {};
		this.templates.list = $("#comments-list").html();
		this.templates.container = $("#comments-container").html();
		this.templates.form = $("#comments-target-form").html();

		this.el = {};
		this.el.container = $(container);
		this.el.container.html(Handlebars.compile(this.templates.container)(params));

		this.el.showAttachBtn = this.el.container.find(".comments-show-attach-popup");
		this.el.showAttachBtn.click( function() {
			popup.open({
				url : BASE_URL + "comment/attach?origin=" + params.origin + "&suborigin=" + params.suborigin,
				onClose : function() {
					if (typeof params.onCloseAttachPopup == "function") {
						params.onCloseAttachPopup();
					}
				},
				onConfirm : function() {
					if (typeof params.onCloseAttachPopup == "function") {
						params.onCloseAttachPopup();
					}
				}
			});
		});

		this.el.container.on("click", ".comments-reply-show-attach-popup", function() {
			popup.open({
				url : BASE_URL + "comment/attach?reply=1&origin=" + params.origin + "&suborigin=" + params.suborigin,
				onClose : function() {
					if (typeof params.onCloseAttachPopup == "function") {
						params.onCloseAttachPopup();
					}
				},
				onConfirm : function() {
					if (typeof params.onCloseAttachPopup == "function") {
						params.onCloseAttachPopup();
					}
				}
			});
		});

		this.el.list = this.el.container.find(".comments-ul");
		this.el.list.on("keydown", ".comment-reply-text", function(e) {
			if ((e.keyCode ? e.keyCode : e.which) == 13) {
				e.preventDefault();
				thisClass.el.currentReplyDiv.find(".comment-submit-reply-btn").trigger("click");
			}
		});
		this.el.list.on("click", ".comment-submit-reply-btn", function() {
			if (thisClass.el.currentReplyTextarea.val()) {
				thisClass.disableReplyButtons();
				if (hasReplyAttachment) {
					thisClass.el.replyAttachmentPreview
						.find(".remove")
						.data("sending", true)
						.text("Sending...");
					thisClass.el.replyAttachment.submit();
				} else {
					thisClass.submitReply( function() {
						thisClass.enableReplyButtons();
					});
				}
			}
		});

		this.el.commentAttachmentPreview = $("#comments-attached-preview-" + idPair);
		this.el.list.on("click", ".remove", function() {
			if (!$(this).data("sending")) {
				var parentPreview = $(this).parent().parent();
				if (parentPreview.data("type") == "comment") {
					hasCommentAttachment = false;
				} else if (parentPreview.data("type") == "reply") {
					hasReplyAttachment = false;
				}
				parentPreview.hide();
			}
		});

		function fileUploadChange() {
			var oFReader = new FileReader();
			oFReader.readAsDataURL($(this).prop("files")[0]);
			$("#comment-browse-input-" + idPair).val($(this).val());
			$("#comment-attach-preview-" + idPair).find("span").html("LOADING PREVIEW...");
			oFReader.onload = function(oFREvent) {
				$("#comment-attach-preview-" + idPair).html('<img src="' + oFREvent.target.result + '"/>').show();
			};
		}

		function fileUploadReplyChange() {
			var oFReader = new FileReader();
			oFReader.readAsDataURL($(this).prop("files")[0]);
			$("#comment-browse-input-" + replyIdPair).val($(this).val());
			$("#comment-attach-preview-" + replyIdPair).find("span").html("LOADING PREVIEW...");
			oFReader.onload = function(oFREvent) {
				$("#comment-attach-preview-" + replyIdPair).html('<img src="' + oFREvent.target.result + '"/>').show();
			};
		}

		if (!$("#comment-form-upload-" + idPair).length) {
			commentsParentBody.append(Handlebars.compile(this.templates.form)(params));
		}
		commentsParentBody.off("click", "#comment-browse-" + idPair)
			.on("click", "#comment-browse-" + idPair, function() {
				$("#comment-attach-" + idPair)
					.unbind("change")
					.bind("change", fileUploadChange)
					.trigger("click");
			})
			.off("click", "#comment-attach-upload-" + idPair)
			.on("click", "#comment-attach-upload-" + idPair, function() {
				var imgAttachment = $("#comment-attach-preview-" + idPair + " img");
				if (imgAttachment.length) {
					$("#comment-error-attach-" + idPair).html("");
					thisClass.el.commentAttachmentPreview.show()
						.find("img").prop("src", imgAttachment.attr("src"));
					hasCommentAttachment = true;
					if (typeof params.onCloseAttachPopup == "function") {
						popup.close(params.onCloseAttachPopup);
					} else {
						popup.close();
					}
				} else {
					$("#comment-error-attach-" + idPair).html("<span>Please choose a file</span>");
				}
			});

		if (!$("#comment-form-upload-" + replyIdPair).length) {
			var replyFormParams = {
				origin : params.origin,
				suborigin : params.suborigin,
				reply : "-reply"
			}
			commentsParentBody.append(Handlebars.compile(this.templates.form)(replyFormParams));
		}
		commentsParentBody.off("click", "#comment-browse-" + replyIdPair)
			.on("click", "#comment-browse-" + replyIdPair, function() {
				$("#comment-attach-" + replyIdPair)
					.unbind("change")
					.bind("change", fileUploadReplyChange)
					.trigger("click");
			})
			.off("click", "#comment-attach-upload-" + replyIdPair)
			.on("click", "#comment-attach-upload-" + replyIdPair, function() {
				var imgAttachment = $("#comment-attach-preview-" + replyIdPair + " img");
				if (imgAttachment.length) {
					$("#comment-error-attach-" + replyIdPair).html("");
					thisClass.el.replyAttachmentPreview.show()
						.find("img").prop("src", imgAttachment.attr("src"));
					hasReplyAttachment = true;
					if (typeof params.onCloseAttachPopup == "function") {
						popup.close(params.onCloseAttachPopup);
					} else {
						popup.close();
					}
				} else {
					$("#comment-error-attach-" + replyIdPair).html("<span>Please choose a file</span>");
				}
			});

		this.el.commentAttachment = $("#comment-form-upload-" + idPair);
		$("#comment-form-target-" + idPair).load( function() {
			var filename = $(this).contents().text();
			if (filename) {
				if (filename.indexOf("ERROR: ") == 0) {
					thisClass.el.commentTextarea.prop("disabled", false);
					popup.open({
						title : "Sorry",
						align : "center",
						message : filename.replace("ERROR: ", ""),
						onOpen : function() {
							thisClass.el.commentAttachmentPreview.hide();
							hasCommentAttachment = false;
						}
					});
				} else {
					thisClass.submitCommentWithAttachment(filename, function() {
						thisClass.el.commentAttachmentPreview
							.hide()
							.find(".remove")
							.data("sending", false)
							.text("Remove Attachment");
						hasCommentAttachment = false;
					});
				}
			}
		});

		this.el.replyAttachment = $("#comment-form-upload-" + replyIdPair);
		$("#comment-form-target-" + replyIdPair).load( function() {
			var filename = $(this).contents().text();
			if (filename) {
				if (filename.indexOf("ERROR: ") == 0) {
					thisClass.el.currentReplyTextarea.prop("disabled", false);
					popup.open({
						title : "Sorry",
						align : "center",
						message : filename.replace("ERROR: ", ""),
						onOpen : function() {
							thisClass.el.replyAttachmentPreview.hide();
							hasReplyAttachment = false;
						}
					});
				} else {
					thisClass.submitReplyWithAttachment(filename, function() {
						thisClass.el.replyAttachmentPreview
							.hide()
							.find(".remove")
							.data("sending", false)
							.text("Remove Attachment");
						hasReplyAttachment = false;
						thisClass.enableReplyButtons();
					});
				}
			}
		});

		this.el.showMoreCommentsBtn = this.el.container.find(".comments-next-btn");
		this.el.showMoreCommentsBtn.click( function() {
			var thisBtn = $(this);
			var origText = thisBtn.text()
			currentCommentsPage = thisBtn.text("LOADING...").prop("disabled", true).data("page");
			thisClass.loadComments( function() {
				thisBtn.text(origText).prop("disabled", false);
			});
		});

		this.el.commentTextarea = this.el.container.find(".comment-textarea");
		this.el.commentTextarea.keydown( function(e) {
			if ((e.keyCode ? e.keyCode : e.which) == 13) {
				e.preventDefault();
				thisClass.el.submitCommentBtn.trigger("click");
			}
		});

		this.el.submitCommentBtn = this.el.container.find(".comment-submit-btn");
		this.el.submitCommentBtn.click( function() {
			if (thisClass.el.commentTextarea.val()) {
				if (hasCommentAttachment) {
					thisClass.el.commentTextarea.prop("disabled", true);
					thisClass.el.commentAttachmentPreview
						.find(".remove")
						.data("sending", true)
						.text("Sending...");
					thisClass.el.commentAttachment.submit();
				} else {
					thisClass.submitComment();
				}
			}
		});

		this.el.currentReplyDiv = null;
		this.el.replyAttachmentPreview = null;
		this.el.currentReplyTextarea = null;
		this.el.container.on("click", "button.reply", function() {
			$("button.reply").show(0);
			$(this).hide(0);
			$("div.reply").hide();
			$(this).siblings("div.reply").fadeIn();
			$(".comments-reply-attached-preview").hide();
			thisClass.el.currentReplyDiv = $(this).next();
			thisClass.el.currentReplyTextarea = thisClass.el.currentReplyDiv.find(".comment-reply-text");
			currentReplyCommentId = thisClass.el.currentReplyDiv.data("comment-id");
			thisClass.el.replyAttachmentPreview = $("#comments-reply-attached-preview-" + currentReplyCommentId);
			hasReplyAttachment = false;
		});

		this.setRecipient = function(recipient) {
			recipientId = recipient;
			return thisClass;
		}

		this.submitCommentWithAttachment = function(media, onSuccess) {
			params.media = media;
			this.submitComment( function(response) {
				delete params.media;
				if (typeof onSuccess == "function")
					onSuccess(response);
			});
		}

		this.submitComment = function(onSuccess, onClosePopup) {
			if (!thisClass.el.commentTextarea.val())
				return;
			thisClass.el.commentTextarea.prop("disabled", true);
			$.post(
				commentsUrl,
				{ 
					text : thisClass.el.commentTextarea.val(), 
					origin : params.origin, 
					recipient : recipientId,
					suborigin : params.suborigin,
					media : typeof params.media != "undefined" ? params.media : ""
				},
				function(response) {
					if (typeof onSuccess == "function") {
						onSuccess(response);
					}
					popup.open({
						align : "center",
						title : "Thank you for your comment",
						message : "Your comment submission was successfully sent and is now subject to moderation. <br>Please allow 24-48 hours for approval.",
						okCaption : "CLOSE",
						type : "alert",
						buttonAlign : "center",
						onOpen : function() {
							GA.sendEvent(thisClass.ga);
						},
						onClose : function() {
							if (typeof params.onCloseSuccessPopup == "function") {
								params.onCloseSuccessPopup();
							}
							thisClass.el.commentTextarea.prop("disabled", false).focus();
						},
						onConfirm : function() {
							if (typeof params.onCloseSuccessPopup == "function") {
								params.onCloseSuccessPopup();
							}
							thisClass.el.commentTextarea.prop("disabled", false).focus();
						}
					});
					thisClass.el.commentTextarea.val("");
				}
			);
		}

		this.submitReplyWithAttachment = function(media, onSuccess) {
			params.media = media;
			this.submitReply( function(response) {
				delete params.media;
				if (typeof onSuccess == "function")
					onSuccess(response);
			});
		}

		this.submitReply = function(onSuccess) {
			if (thisClass.el.currentReplyTextarea.val()) {
				thisClass.el.currentReplyTextarea.prop("disabled", true);
				$.post(
					commentsUrl,
					{
						text : thisClass.el.currentReplyTextarea.val(),
						is_reply : 1,
						comment_id : currentReplyCommentId,
						origin : params.origin,
						suborigin : params.suborigin,
						media : typeof params.media != "undefined" ? params.media : ""
					},
					function(response) {
						if (typeof onSuccess == "function") {
							onSuccess(response);
						}
						thisClass.el.currentReplyTextarea.prop("disabled", false).val("");
						thisClass.el.currentReplyDiv.hide().prev().show();
						popup.open({
							align : "center",
							title : "Thank you for your reply",
							message : "Your reply submission was successfully sent and is now subject to moderation. <br>Please allow 24-48 hours for approval.",
							okCaption : "CLOSE",
							buttonAlign : "center",
							type : "alert",
							onClose : function() {
								if (typeof params.onCloseSuccessPopup == "function") {
									params.onCloseSuccessPopup();
								}
							},
							onConfirm : function() {
								if (typeof params.onCloseSuccessPopup == "function") {
									params.onCloseSuccessPopup();
								}
							}
						});
					}
				);
			}
		}

		this.disableReplyButtons = function() {
			this.el.list.find("button.reply").css("visibility", "hidden");
		}

		this.enableReplyButtons = function() {
			this.el.list.find("button.reply").css("visibility", "");
		}

		this.getComments = function(page, onSuccess) {
			if (typeof page == "function") {
				callback = page;
				page = 1;
			}
			$.get(
				commentsUrl,
				{ 
					origin : params.origin, 
					suborigin : params.suborigin,
					page : page 
				},
				function (response) {
					if (typeof onSuccess == "function")
						onSuccess(response);
				}
			);
		}

		var doneFirstLoad = false;
		this.loadComments = function(callback) {
			this.getComments(currentCommentsPage, function(response) {
				params.comments = response.comments;
				params.total = response.total;
				if (currentCommentsPage > 1) {
					thisClass.el.list.append(Handlebars.compile(thisClass.templates.list)(params));
				} else {
					$("#comments-preloader-" + idPair).remove();
					thisClass.el.list.append(Handlebars.compile(thisClass.templates.list)(params));
					doneFirstLoad = true;
				}
				if (response.next) {
					thisClass.el.showMoreCommentsBtn.data("page", response.next).show();
				} else {
					thisClass.el.showMoreCommentsBtn.data("page", false).hide();
				}
				if (typeof callback == "function")
					callback(response.comments, response.total, response.next);
			});
		}

		this.setReload = function(interval) {
			setTimeout( function() {
				if (doneFirstLoad) {
					thisClass.getComments(1, function(response) {
						var x = 0;
						var children = thisClass.el.list.find(" > li");
						params.comments = response.comments;
						params.total = response.total;
						for (var commentId in response.comments) {
							var child = children.eq(x);
							if (commentId == child.attr("id")) {
								
							} else {
								
							}
							x++;
						}
						thisClass.setReload(interval);
					});
				} else {
					thisClass.setReload(interval);
				}
			}, interval);
		}

		this.setAnalytics = function(ga) {
			this.ga = ga;
			return this;
		}

		var thisClass = this;
	}

	this.create = function(container, params) {
		return new CommentsList(container, params);
	}

	var self = this;
}

function GA() {
	this.sendEvent = function(params) {
		if (params == undefined)
			return;
		gaParams = {
			'hitType': 'event',
			'eventCategory': params.category,
			'eventAction': params.action,
			'eventLabel': params.label,
			'eventValue': params.value
		}
		// if (PROD_MODE) {
		// 	if (typeof params.callback == "function")
		// 		gaParams['hitCallback'] = params.callback;
		// 	ga('send', gaParams);
		// } else if (typeof params.callback == "function") {
		// 	params.callback();
		// }
		if (typeof params.callback == "function") {
			params.callback();
		}
	}
}

window.User = new User();
window.Task = new Task();
window.CommentBox = new CommentBox();
window.GA = new GA();