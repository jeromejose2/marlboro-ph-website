

$(function(){

	$('.edit-profile,.cancel-profile-update').click(function(){
 
		$('.profile-details').toggleClass('edit');
		$('.profile-details').jScrollPane({verticalGutter: 50, showArrows: true});
		$('#profile-error').html('');

		return false;

	});

	$('select[name="province"]').change(function(){
		var province = $(this).val();
		var cities = $('select[name="city"]');
		cities.html("<option>Loading cities</option>");

		$.getJSON(BASE_URL+'profile/get_cities?province='+province,function(cities){
			var items = [''];
 
			 $.each(cities, function(i,obj) {
 				items.push('<option value="'+obj.city_id+'">'+obj.city+'</option>');
			});
			 $('select[name="city"]').html(items.join(''));
		});
	});	 

});

function profile_update_details()
{
	$('#update-profile').html('<i><img src="'+BASE_URL+'images/spinner.gif" /> Saving...</i>');
	return true;
}

function form_response(msg)
{
	popup.open({type:'alert','message':msg,buttonAlign:'center',align:'center',okCaption:'CONTINUE',onConfirm:function(){ location.href=BASE_URL+'profile';  }});
	$('#update-profile').html('<i>SAVE</i>');
}