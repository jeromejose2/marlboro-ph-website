
function Lytebox(params){

	var This 	= this;
	var Options = new Object();
	var El 		= new Object();
	var Btn 	= new Object();
	var Config  = 	{
						/*base config*/
						paddingTop  : 50,
						paddingBottom: 50,
						transitionOut : 100,
						transitionIn : 100,

						/*overwritable config*/
						top 		: null,
						width 		: 360,
						align 		: 'left',
						okCaption 	: 'Ok',
						cancelCaption : 'Cancel', 
						btnAlign 	: 'right',

						close 		: true,
						escClose 	: false,
						bgClose 	: true,
					}

	$.extend(Config, params);


	This.dialog = function(params){
		

		Options = {
			message		: '',
			title 		: null,
			align 		: Config.align,
			close 		: Config.close,
			
			type 		: 'message', //'alert/confirm', default
			okCaption 	: Config.okCaption,
			cancelCaption : Config.cancelCaption, 
			btnAlign 	: Config.btnAlign,
			top  		: Config.top,
			width 		: Config.width, 
			escClose 	: Config.escClose,
			bgClose 	: Config.bgClose,

			onOpen		: function(){}, 
			onClose		: function(){},
			onConfirm	: function(){}, 
			onCancel	: function(){}
		}

		if(typeof(params)=='string'){
			This.dialog({message:params});
			return;
		}
		
		initialize(params);
		createBase();

		El.Content.html('');
		El.Content.addClass(Options.addClass);
		El.Content.addClass('lytebox-dialog');

		if(Options.title){
			var title = createElement('h1',Options.title)
					.addClass('lytebox-title')
					.appendTo(El.Content); 
		}
		
		
			
		var message = createElement('div',Options.message)
						.addClass('lytebox-message')
						.html(Options.message)
						.appendTo(El.Content); 
		
		if (Options.type == 'message'){
			if( Options.close ){
				var close = createElement('div','&times;')
							.addClass('lytebox-dialog-close')
							.prependTo(El.Content);
			}
		}else{

			var buttons = createElement()
						.addClass('lytebox-buttons')
						.addClass(Options.btnAlign)
						.appendTo(El.Content);


			Btn.Okay = createElement('button', '<i>'+Options.okCaption+'</i>')
						.addClass('lytebox-ok')
						.attr('type','button')
						.appendTo(buttons);

			if (Options.type == 'confirm' ){		
				Btn.Cancel = createElement('button', '<i>'+Options.cancelCaption+'</i>')
							.addClass('lytebox-cancel')
							.attr('type','button')
							.appendTo(buttons);
			}
		}

		El.Content.wrapInner('<div class="content"></div>');

		El.Modal.fadeIn(Config.transitionIn);
		install();
		bindButtons();
	}

	This.load = function(obj){
		Options = {
			url 		: null,
			data 		: {},

			top  		: Config.top,
			escClose 	: Config.escClose,
			bgClose 	: Config.bgClose,

			onOpen		: function(){}, 
			onClose		: function(){},
			onConfirm	: function(){}, 
			onCancel	: function(){}

		}

		initialize(obj);
		createBase();

		El.Modal.fadeIn(Config.transitionIn);
		El.Content.html('<div class="preload"></div>');
		install();

		var getPage = $.get(Options.url, 
						  Options.data,
							function(result){

								$('.preload').fadeOut(function(){
									El.Content.html(result);
										install();
										$('.popup:not(.lytebox-wrapped-content)')
											.hide()
											.fadeIn();
											bindButtons();
								})
								
					});
		
	}

	This.show = function(obj){

		Options = {
			html 		: null,

			top  		: Config.top,
			escClose 	: Config.escClose,
			bgClose 	: Config.bgClose,

			onOpen		: function(){}, 
			onClose		: function(){},
			onConfirm	: function(){}, 
			onCancel	: function(){}

		}

		initialize(obj);

		setTimeout(function(){
			El.Modal 	=  $('.lytebox-wrap').addClass('lytebox');
			El.Holder 	=  $('.lytebox-content-holder');
			El.InlineContent  =   $(Options.html);

			if(!El.InlineContent.parent().hasClass('lytebox-content'))
					El.InlineContent .wrap('<div class="lytebox-content"></div>');

			El.Content 	= $('.lytebox-content');

			El.InlineContent.show(0);
			El.Modal.css({height : El.Document.height()});
			El.Modal.fadeIn(Config.transitionIn);
			install();
			bindButtons();
		},Config.transitionOut+50)

	}

	This.open = function(obj){
		/*backward compatibility*/
		Options = {
			
			title			: obj.title ? obj.title : '&nbsp;',
			message			: '',
			url				: null,
			html			: null,
			data			: {},
			buttonAlign	 	: Config.btnAlign,
			okCaption		: Config.okCaption,
			cancelCaption	: Config.cancelCaption,
			onOpen			: obj.onOpen ?  obj.onOpen : function(){},
			onClose			: obj.onClose ?  obj.onClose : function(){},
			onConfirm		: obj.onConfirm ?  obj.onConfirm : function(){},
			onCancel		: obj.onCancel ?  obj.onCancel : function(){},
			escButton		: Config.escClose,
			addClass		: '',
			removeClass		: '',

			//conflicts
			align 		: obj.align ? obj.align : Config.align,
			close 		: Config.close,
			type 		: obj.type ? obj.type : Config.type,  
			btnAlign 	: obj.buttonAlign ? obj.buttonAlign : Config.btnAlign,
			top  		: obj.top ? obj.top : Config.top,
			width 		: obj.width ? obj.width : Config.width, 
			escClose 	: obj.escButton ? obj.escButton : Config.escClose,
			bgClose 	: obj.blurClose ? obj.blurClose : Config.bgClose,
		}
		initialize(obj);
		if(typeof(obj)=='string'){
			This.dialog({message:obj});
		}else{

			if( obj.url ){
				This.load(Options);
			}else if( obj.html ){
				This.show(Options);
			}
			else{
				This.dialog(Options);
			}

		}

	}


	This.close = function( callback ){
		
		El.Body.css({overflow:'auto'})
		El.Modal.fadeOut(Config.transitionOut, function(){
			var modal = $(this);

			if( modal.hasClass('lytebox-wrap') ){
				
				$('.lytebox-wrapped-content',modal).each(function(){
					var el = $(this);

					el.hide();
					
					if( el.parent().hasClass('lytebox-content') ){
						el.unwrap();
					}
				});

				modal.removeClass('lytebox');
			}
			else{
				$(this).remove();
			}
			
			
		});

		if( callback ) callback();

	}


	/*private functions*/

	function initialize(params){

		if( $('.lytebox').length > 0 ){
			This.close( false );
		}

		$.extend(Options, params);
		El.Window 	= $(window);
		El.Document = $(document); 
		El.Body 	= $('body');

		El.Body.css({overflow:'hidden'});
		
	}

	function createBase(){

		El.Modal 	=  createElement().addClass('lytebox');
		El.Holder 	= createElement().addClass('lytebox-content-holder');
		El.Content 	= createElement().addClass('lytebox-content').addClass(Options.align);

		El.Content.appendTo(El.Holder);
		El.Holder.appendTo(El.Modal);
		El.Modal.appendTo( $('section.main') );
		
		El.Modal.css({height : El.Document.height()});
	}

	function install(){

		if( $(window).width() > 767 ){
			$('.lytebox .scrollable-small').jScrollPane({showArrows: true, verticalGutter: 20});
			$('.lytebox .scrollable').jScrollPane({showArrows: true, verticalGutter: 20});
		}
		This.stabilize();
		El.Window.resize(This.stabilize);
		Options.onOpen();
	}

	This.stabilize = function(){
		
		if(Options.url){
			El.Content.css({width :  El.Content.children(':first-child').outerWidth() });
		}
		else if(Options.html){
			El.Content.css({width :  El.InlineContent.outerWidth() });
		}else{
			El.Content.css({width :  Options.width });
		}


		// Config.paddingBottom = $('.government-warning').outerHeight() + 70 ;
		Config.paddingBottom = $('.government-warning').outerHeight() + 150 ;


		var windowHeight  = El.Window.height();
		var contentHeight = El.Content.outerHeight();
		var windowWidth   = El.Window.width();
		var contentWidth  = El.Content.outerWidth();

		var positionTop 	= Options.top ? Options.top : (windowHeight-(Config.paddingTop+Config.paddingBottom));
			positionTop 	= positionTop - contentHeight;
			positionTop 	=(positionTop/2 ) + Config.paddingTop;
			positionTop 	= positionTop < Config.paddingTop ? Config.paddingTop : positionTop;


		var positionLeft 	= (windowWidth-contentWidth)/2;
		var paddingBottom  	= Config.paddingTop + Config.paddingBottom;
	
		El.Content.css({
			left: positionLeft,
			top: positionTop,
			marginBottom : paddingBottom
		});

		El.Modal.css({height : El.Document.height() });
	}

	function bindButtons(){
		Btn.Close = $('.lytebox-dialog-close, .lytebox-close, .popup-close, .close-x');

		/* close popup by clicking the background */
		if( Options.bgClose ){
			El.Holder.unbind('click');
			El.Holder.bind('click', function(e){
				if( $(e.target).hasClass('lytebox-content-holder') )
				This.close( Options.onClose );
			});
		}

		/* close popup by esc key */
		if ( Options.escClose ){
			El.Document.unbind('keydown.escClose');
			El.Document.bind('keydown.escClose',function(e){
				if (e.keyCode == 27) { 
					This.close( Options.onClose );
				}
			});
		}

		/* close buttons */
		if( Options.close || Options.url || Options.html ){
			Btn.Close.unbind('click');
			Btn.Close.bind('click', function(e){
				This.close( Options.onClose );
			});
		}

		/*options for dialog alert/confirm*/
		if(Options.type && Options.type != 'message'){

			/*remove closing options*/
			El.Holder.unbind('click');
			El.Document.unbind('keydown.escClose');

			if(Btn.Okay){
				Btn.Okay.unbind('click');
				Btn.Okay.bind('click',function(){
					This.close( Options.onClose );
					Options.onConfirm();
				});
			} 
			if(Btn.Cancel){
				Btn.Cancel.unbind('click');
				Btn.Cancel.bind('click',function(){
					This.close( Options.onClose );
					Options.onCancel();
				});
			} 

		}


	}

	function createElement(element,html){
		element = element==undefined ? 'div' : element;
		html    = html==undefined ? '' : html;
		return $('<'+element+'>'+html+'</'+element+'>');
	}

}


var popup = new Lytebox({
		paddingTop : 120,
		transitionOut : 200,
		transitionIn : 200,
		width : 600
	});

