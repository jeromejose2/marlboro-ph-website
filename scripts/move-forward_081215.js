$(document).ready( function() {	
	toggleMobileSubNav();

	$("#upload-photo-challenge .browse").click(function(){
		$("#upload-task").trigger("click");
		$("#upload-photo-challenge .upload-preview-fixed").html("");
	});

	$("#upload-video-challenge .browse input").bind("keydown keyup change",function(){
		videoPreview($(this).val());
	});

	var showGalleryh3 = $("#show-gallery-h3");
	var showGalleryOrigText = showGalleryh3.text();
	showGalleryh3.text("Loading Gallery...");
	$.get(BASE_URL + "move_forward/gallery/" + MoveFwd.currentOffer, function(html) {
		showGalleryh3.text(showGalleryOrigText);
		$("#move-fwd-gallery-popup").html(html);
		$("#move-fwd-show-gallery").click( function() {
			popup.open({html : "#move-fwd-gallery-popup", onOpen : function() {
				if (galleryIsNotEmpty) {
					User.visit(Origin.MOVE_FWD, MoveFwd.currentOffer, function(response) {
						$("#move-fwd-visits-count").text("VIEWS: " + response.visits);
					});
				}
			}});
		});
	});

	$("#play-challenge-btn").click( function() {
		if (!MoveFwd.choice) {
			popup.loading();
			setTimeout( function() {
				popup.open({html:"#play-challenge"});
			}, 300);
		}
	});

	var submittedEntry = false;
	var isVideo = false;
	var entryType = 'photo';
	$("#move-fwd-photo-upload-submit").click( function() {
		popup.loading();
		$("#move-fwd-photo-upload").submit();
		submittedEntry = true;
	});

	$("#move-fwd-video-upload-submit").click( function() {
		$("#move-fwd-video-upload").submit();
	});

	$("#move-fwd-text-submit").click( function() {
		$("#move-fwd-text-upload").submit();
	});

	$("#pledge-challenge-btn").click( function() {
		if (!MoveFwd.choice) {
			popup.loading();
			setTimeout( function() {
				popup.open({html:"#pledge-challenge"});
			}, 300);
		}
	});

	$("#play-challenge-continue-btn").click( function() {
		popup.show({html:".submit-challenge-entry-popup"})
	});

	$("#move-forward-upload").load( function() {
		var content = $(this).contents().text();
		if (content && submittedEntry) {
			popup.open({
				title : "Sorry",
				align : "center",
				message : content,
				onClose : function() {
					window.location = window.location;
				}
			});
		} else if (submittedEntry) {
			User.playMoveForward(MoveFwd.currentOffer, function(response) {
				MoveFwd.choice = response.choice;
				popup.open({
					title : "Thank you for your entry",
					align : "center",
					buttonAlign : "center",
					message : "Your " + entryType + " submission was successfully sent and is now subject to moderation.<br>Please allow 24-48 hours for approval",
					type : "alert",
					okCaption : "Continue",
					onClose : function() {
						window.location = window.location;
					},
					onOpen : function() {
						isVideo = false;
						entryType = 'photo';
					}
				});
			});
		}
	});

	$("#play-challenge-later-btn").click( function() {
		popup.close();
	});

	$("#_takechallenge").click( function() {
		popup.loading();
		$.get(BASE_URL + "move_forward/slots", { mfid : MoveFwd.currentOffer }, function(response) {
			setTimeout( function() {
				// if (response) {
					if (!MoveFwd.choice) {
						popup.open({html:"#popup-task-challenge"});
					} else if (!MoveFwd.alreadyDone) {
						if (MoveFwd.choice == MoveFwd.PLAY) {
							popup.open({html:"#play-challenge"});
						} else if (MoveFwd.choice == MoveFwd.PLEDGE) {
							popup.open({html:"#pledge-challenge"});
						}
					}
				// } else {
				// 	popup.open({
				// 		align : "center",
				// 		message : "<h2>NO MORE SLOTS</h2><br>",
				// 		type : "alert",
				// 		buttonAlign : "center",
				// 		okCaption : "CLOSE",
				// 		onClose : function() {
				// 			window.location = window.location;
				// 		}
				// 	});
				// }
			}, 500);
		});
	});

	$("#pledge-challenge-continue").click( function() {
		popup.loading();
		$.post(BASE_URL + "move_forward/pledge", { mfid : MoveFwd.currentOffer }, function(response) {
			var pledgedPoints = response.points;
			setTimeout( function() {
				if (response.success) {
					User.pledgeMoveForward(MoveFwd.currentOffer, function(response) {
						MoveFwd.choice = response.choice;
						popup.open({
							title : "Thank you!",
							align : "center",
							buttonAlign : "center",
							message : "You have successfully pledged " + pledgedPoints + " points for " + MoveFwd.currentOfferName + ".",
							type : "alert",
							okCaption : "CLOSE",
							onClose : function() {
								window.location = window.location;
							}
						});
					});
				} else if (response.error) {
					popup.open({
						title : "Pledge Unsuccessful",
						align : "center",
						buttonAlign : "center",
						message : response.error,
						type : "alert",
						okCaption : "CLOSE",
						onClose : function() {
							window.location = window.location;
						}
					});
				}
			}, 500);
		});
	});

	$("#participants-prev").click( function() {
		if (!participantsLoading) {
			var self = $(this);
			if (self.data("page")) {
				getParticipants(self.data("page"), function(response) {
					self.data("page", response.prev);
				});
			}
		}
	});

	$("#participants-next").click( function() {
		if (!participantsLoading) {
			var self = $(this);
			if (self.data("page")) {
				getParticipants(self.data("page"), function(response) {
					self.data("page", response.next);
				});
			}
		}
	});

	$("#move-fwd-video-upload").submit( function(e) {
		var videoUrl = $("#video-url").val();
		var errMsg = "<br><span>Invalid Youtube URL</span>";
		$("#error-video-preview").hide();
		if (!videoUrl) {
			$("#error-video-preview").html(errMsg).show();
			return false;
		}

		var pos = videoUrl.indexOf("www.youtube.com");
		var start = videoUrl.indexOf("?v=");
		if (pos < 0 || start < 0 || pos > start) {
			$("#error-video-preview").html(errMsg).show();
			return false;
		}

		popup.loading();
		isVideo = true;
		entryType = 'video';
		submittedEntry = true;
	});

	$('#move-fwd-text-upload').submit(function() {
		var errMsg = "<br><span>Please input your entry</span>";
		var empty = /^\s*$/;
		$("#error-text").hide();
		if(empty.test($('#text').val())) {
			$("#error-text").html(errMsg).show();	
			return false;
		}

		popup.loading();
		entryType = 'text';
		submittedEntry = true;
	});

	$('#text').keyup(function() {
		$('#error-text').hide();
	});

	if (User.fromMobile()) {
		participantsListLimit = 6;
	}
	getParticipants();
});

var participantsListLimit = 9;
var participantsThumblist = $("#participants-thumblist");
var participantsLoading = false;
function getParticipants(page, onSuccess) {
	if (page == undefined) {
		page = 1;
	}
	participantsLoading = true;
	$("#participants-preloader").html("LOADING...");
	$.get(
		BASE_URL + "move_forward/participants", 
		{ mfid : MoveFwd.currentOffer, page : page, limit : participantsListLimit },
		function(response) {
			participantsThumblist.html("");
			for (var u in response.participants) {
				var profile_pic = null;
				if (response.participants[u].current_picture) {
					profile_pic = BASE_URL + "uploads/profile/" + response.participants[u].registrant_id + "/" + response.participants[u].current_picture;
				} else {
					profile_pic = BASE_URL + DEFAULT_IMAGE;
				}
				participantsThumblist.append(
					"<div data-id=\"" + response.participants[u].registrant_id + "\" class=\"send-message\" style=\"background-image:url("+profile_pic+")\"><span></span></div>"
				);
			}

			previewSendMessageForm();
			$("#participants-preloader").html("PARTICIPANTS");
			participantsLoading = false;
			$("#participants-next").data("page", response.next);
			$("#participants-prev").data("page", response.prev);
			if (typeof onSuccess == "function") {
				onSuccess(response);
			}
		}
	);
}

function toggleMobileSubNav(){
	if ($(window).width() < 768) {
		$(".move-fwd-inner .active").click( function() {
			$(this).parent().toggleClass("drop");
			return false;
		});
	}
}

function PreviewImage() {
	var oFReader = new FileReader();
	oFReader.readAsDataURL(document.getElementById("upload-task").files[0]);

	$("#upload-task").siblings(".browse").children().val( $("#upload-task").val() );

	oFReader.onload = function (oFREvent) {
		$(".upload-preview-fixed").html('<img src="'+oFREvent.target.result+'"/>').show();
	};
};

function videoPreview(str){

	$("#upload-video-challenge .upload-preview-fixed").html("");
	var start = str.indexOf("?v=");
	start = start + 3;
	var end   = str.indexOf("&");

	var videoID = end<0 ? str.substring(start) : str.substring(start,end);
	var obj = '<object width="100%" height="100%">';
		obj +='<param name="movie" value="//www.youtube.com/v/'+videoID+'?hl=en_US&amp;version=3"></param>';
		obj +='<param name="allowFullScreen" value="true"></param>';
		obj +='<param name="allowscriptaccess" value="always"></param>';
		obj +='<param name="wmode" value="opaque"></param>';
		obj +='<embed src="//www.youtube.com/v/'+videoID+'?hl=en_US&amp;version=3" type="application/x-shockwave-flash" width="100%" height="100%" allowscriptaccess="always" allowfullscreen="true">';
		obj +='</embed>';
		obj +='</object>';

	$('#upload-video-challenge .upload-preview-fixed').html(obj);
}


function previewSendMessageForm(){

	$('.send-message').on('click',function(){
		var participant = $(this).attr('data-id');
		popup.open({url:BASE_URL+'messages/compose?participant='+participant+'&move_forward='+MoveFwd.currentOffer});
	});
}