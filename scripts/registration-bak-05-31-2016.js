popup.loading = function(message) {
	if (message == undefined) {
	message = '<div class="preload"></div>';
	}
	this.dialog({
		align 	 : "center",
		addClass : 'lytebox-preload',
		message  : message,
		blurClose : false,
		escButton : false,
		close : false
	});
}

$(function(){
	setTimeout(function(){
		registrationPages();
	},500);

	$(window).resize(function(){
		$('#error-msg').hide();
	});

	$('.browse').click(function(){
		$('#giid').trigger('click');
	});

	setBrands();
	setGIIDTypes();
	

});


function setGIIDTypes(){

	 $.getJSON( "get_giid_types", function( data ) {
		var items = ["<option value=''></option>"];

  		$.each( data, function( key, val ) {
 			items.push( "<option value='" + val.ClassCode + "'>" + val.ClassName + "</option>" );
		});
 
    	$('#giid_type').html(items.join(""));


	});

}

function setBrands(){

	$.getJSON( "get_brands", function( data ) {
		var items = ["<option value=''></option>"];
		$.each( data, function( key, val ) {
			items.push( "<option value='" + val.brand_id + "'>" + val.brand_name + "</option>" );
		});

		$('#current_brand').html(items.join(""));
		$('#first_alternate_brand').html(items.join(""));

	});

}

function checkPasswordStrength(elem) {
	password = $(elem).val();
	var hasUpperCase = /[A-Z]/.test(password);
	var hasLowerCase = /[a-z]/.test(password);
	var hasNumbers = /\d/.test(password);
	var hasNonalphas = /\W/.test(password);
	if (password.length < 8)
		return 0;
	else {
		return 10 + (hasUpperCase * 15) + (hasLowerCase * 10) + (hasNumbers * 25) + (hasNonalphas * 40);
	}
	///if (hasUpperCase + hasLowerCase + hasNumbers + hasNonalphas < 3)
}

function validatePassword(password) {

	var hasUpperCase = /[A-Z]/.test(password);
	var hasLowerCase = /[a-z]/.test(password);
	var hasNumbers = /\d/.test(password);
	var hasNonalphas = /\W/.test(password);
	if (password.length < 8)
		return 0;
	else {
		complexity = hasUpperCase + hasLowerCase + hasNumbers + hasNonalphas;
		return complexity >= 3;
	}
	///if (hasUpperCase + hasLowerCase + hasNumbers + hasNonalphas < 3)
}

function checkPasswordComponents(password, name) {
	var excludedWords = new RegExp("/(\\b" + name + "\\b)|user|guest|admin|sys|test|pass|super|/i");
	return excludedWords.test(password);

}

var hasUploadedFile = false;
function PreviewImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("giid").files[0]);

    $("#giid").siblings('.browse').children().val( $("#giid").val() );
    
    oFReader.onload = function (oFREvent) {
    	hasUploadedFile = true;
    	$('#error-msg').hide();
    	$("#captured-image-ba").val("");
    	$('.upload-preview').html('<img src="'+oFREvent.target.result+'"/>').show();
    	popup.stabilize();
    };
};

function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

$("#not-smoker").click( function() {
	var self = $(this);
	popup.open({
		message : "<h4><center>Sorry, You need to be a legal aged (" + MINIMUM_AGE + " years old and above) smoker in order to register.</center></h4>",
		type : 'alert', 
		align : 'center',
		buttonAlign : 'center',
		onConfirm : function() {
			self.prop("checked", false);
		},
		onClose : function() {
			self.prop("checked", false);
		}
	})
});

function showTakePhotoBtn() {
	$("#webcam-take-photo").html("<i>TAKE PHOTO</i>").data("change", 0);
	$("#webcam-photo-submit").hide();
}

$("#show-webcam").click( function() {
	popup.open({html:'#popup4', onClose : showTakePhotoBtn, onConfirm : showTakePhotoBtn});
	initFlash();
});

$("#province-select").change( function() {
	var self = $(this);
	var id = self.val();
	var cities = $("#cities-select");
	cities.html("<option>Loading cities</option>");
	$.get(BASE_URL + "user/cities", { province : id },
		function(response) {
			var select = "<option value=''>CITY</option>";
			for (var i in response) {
				select += "<option value='" + response[i].city_id + "'>" + response[i].city + "</option>";
			}
			cities.html(select);
		}
	);
});

$("#form-reg").submit( function(e) {
	if (!validateRegistration('.registration-step-3')) {
		return false;
	}
	popup.loading();
	var step1 = $('.registration-step-1');
	var step3 = $('.registration-step-3');
	var body  = $('html, body');
	var steps = $('nav.reg-page a');

	$("#" + $(this).attr("target"))
		.unbind("load")
		.bind("load", function() {
			popup.close();
			var content = $(this).contents().text();
			if (!content) {
				popup.open({html : "#popup5", onClose: function() {
					window.location = BASE_URL + "user/login";
				}});
			} else {
				console.log(content);
				if (content.indexOf("ERROR UPLOAD: ") < 0) {
					popup.open({
						title : "Sorry",
						align : "center",
						message : "Invalid form request"
					});
					/*setTimeout( function() {
						window.location = window.location;
					}, 3000);*/
				} else {
					popup.open({
						title : "Sorry",
						align : "center",
						message : content.replace("ERROR UPLOAD: ", ""),
						type: "alert",
						buttonAlign:"center",
						onOpen : function() {
							hasUploadedFile = false;
							$('.upload-preview').html("");
							$("#giid").siblings('.browse').children().val("");					

						},
						onClose : function(){

							step3.hide();
							step1.show();		
							steps.removeClass('active');
							steps.eq(1).addClass('active');
							body.animate({scrollTop:0});

						}
					});
				}
			}
		});
});

$("#upload-gid-later").click( function() {

	var step1 = $('.registration-step-1');
	var step3 = $('.registration-step-3');
	var body  = $('html, body');
	var steps = $('nav.reg-page a');

	popup.loading();
	$.post(BASE_URL + "user/upload_later",
		$("#form-reg").serialize(), 
		function(response) {

			console.log(response);

			if (!response.success && response.error) {
				popup.open({
					title : "Sorry",
					align : "center",
					message : response.error,
					type: "alert",
					buttonAlign:"center",
					onClose : function(){
						popup.close();
					}
				});

				step3.hide();
				step1.show();		
				steps.removeClass('active');
				steps.eq(1).addClass('active');
				body.animate({scrollTop:0});
				

			} else {
				popup.close();
				popup.open({html: "#popup3", onClose: function() {
					window.location = BASE_URL + "user/login";
				}});
			}
		}
	);
});

function closePopup(redirectTo) {
	popup.close();
	window.location = BASE_URL + redirectTo;
}

$("#mobile-number").keydown( function(e) {
    var key = e.charCode || e.keyCode || 0;
    // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
    // home, end, period, and numpad decimal
    return (
        key == 8 || 
        key == 9 ||
        key == 46 ||
        key == 110 ||
        key == 190 ||
        (key >= 35 && key <= 40) ||
        (key >= 48 && key <= 57) ||
        (key >= 96 && key <= 105));
    
});

var d = new Date();
var y = d.getFullYear();
var t = d.getDate();
var m = d.getMonth() + 1;
var req_yr = y - 18;

$("#birthdate").datepicker({
	dateFormat : "yy-mm-dd",
	changeYear : true,
	changeMonth : true,
	yearRange : '1921:' + req_yr,
	maxDate : req_yr + '-'+ m +'-' + t
});

function registrationPages(){

	var step1 = $('.registration-step-1');
	var step2 = $('.registration-step-2');
	var step3 = $('.registration-step-3');
	var body  = $('html, body');
	var steps = $('nav.reg-page a');

	$('#reg-part-1').click(function(e){
		e.preventDefault();
		if(!validateRegistration('.registration-step-1')){
			return;
		}
		popup.loading();
		$.post(BASE_URL + "user/validate_step_1",
			{ 
				smoker: $("input[name='smoker']").val(),
				nname : $("input[name='nname']").val(),
				fname : $("input[name='fname']").val(),
				lname : $("input[name='lname']").val(),
				mname : $("input[name='mname']").val(),
				birthday : $("input[name='birthday']").val(),
				gender : $("input[name='gender']:checked").val(),
				email : $("input[name='email']").val(),
				mobile_prefix : $("select[name='mobile_prefix']").val(),
				mobile_number : $("input[name='mobile_number']").val(),
				street : $("input[name='street']").val(),
				brgy : $("input[name='brgy']").val(),
				province : $("select[name='province']").val(),
				city : $("select[name='city']").val(),
				outlet_code : $("input[name='outlet_code']").val(),
				zip_code : $("input[name='zip_code']").val(),
				referred_code : $("input[name='referred_code']").val()
			},
			function (response) {
				if (response.success) {
					step1.hide();
					step2.show();
					steps.removeClass('active');
					steps.eq(2).addClass('active');
					body.animate({scrollTop:0});
					setTimeout( function() {
						popup.close();
					}, 500);
				} else {
					popup.open({
						type : 'alert', 
						align : 'center',
						buttonAlign : 'center',
						message : "<h4><center>" + response.error + "</h4></center>"
					});
				}
			}
		);
	});

	$('#reg-part-2').click(function(e){
		e.preventDefault();
		if(!validateRegistration('.registration-step-2')){
			return;
		}
		popup.loading();
		$.post(BASE_URL + "user/validate_step_2",
			{ 
				current_brand : $("select[name='current_brand']").val(),
				first_alternate_brand : $("select[name='first_alternate_brand']").val(),
				alternate_purchase : $("input[name='alternate_purchase']").val()
			},
			function (response) {
				if (response.success) {
					step2.hide();
					step3.show();
					steps.removeClass('active');
					steps.eq(3).addClass('active');
					body.animate({scrollTop:0});
					setTimeout( function() {
						popup.close();
					}, 500);
				} else {
					popup.open({
						type : 'alert', 
						align : 'center',
						buttonAlign : 'center',
						message : "<h4><center>" + response.error + "</h4></center>"
					});
				}
			}
		);
	});

	steps.click(function(e){
		e.preventDefault();

	});
}

function validateRegistration(section){
	var valid = true;
	var registration = $('.registration');
	var errorHolder = $('#error-msg');
	var message;
	var position;
	var posTop;
	var top;
	var posLeft;
	var winW = $(window).width();

	$(section+' .req', registration).each(function(){
		var el		= $(this);
		var offset	= el.offsetRelative(registration);
			top 	= el.offset().top - 120;
		 	posTop 	= offset.top - 50;
			posLeft = offset.left + 50;
			posLeft = winW < 768 ? (posLeft > winW/2 ? winW/2 : posLeft) : posLeft;
		var type    = el.attr('type');
		var name    = el.attr('name');
			message = el.attr('title');

		errorHolder.hide();

		if (name == "upload_giid_button") {
			if (!$("#captured-image-ba").val() && !hasUploadedFile) {
				valid = false;
				return false;
			}
		} else if(!el.val()){
			valid = false;
			position = { top : posTop, left : posLeft};
			return false;
		} else if ((name == "fname" || name == "mname" || name == "nname" || name == "lname") && !(/^[a-zA-Z ]*$/.test(el.val()))) {
			var nameErrorMessages = {
				fname : 'The first name field may only contain alphabetical characters and space.',
				mname : 'The middle initial field may only contain alphabetical characters and space.',
				lname : 'The last name field may only contain alphabetical characters and space.',
				nname : 'The nick name field may only contain alphabetical characters and space.'
			}
			valid = false;
			position = { top : posTop, left : posLeft};
			message = nameErrorMessages[name];
			return false;
		} else if (name == "zip_code") {
			if (!(/^[0-9]*$/.test(el.val()))) {
				valid = false;
				position = { top : posTop, left : posLeft};
				message = 'The zip Code field must contain only numbers.';
				return false;
			} else if (el.val().length > 4) {
				valid = false;
				position = { top : posTop, left : posLeft};
				message = 'The zip Code field can not exceed 4 characters in length.';
				return false;
			}
		} else if (name == "mobile_number" && el.val().length < 7) {
			valid = false;
			position = { top : posTop, left : posLeft};
			return false;
		} else if (name == "email" && !validateEmail(el.val())) {
			valid = false;
			message = "Please enter valid email address";
			return false;
		} else if(type=='radio'){
			if($('input[name="'+name+'"]:checked').length < 1 ){
				valid = false;
				return false;
			}
		}
	});

	if(!valid){
		errorHolder.css({ top : posTop, left : posLeft}).show();
		$('span',errorHolder).html( message );
		$('html, body').animate({scrollTop:top});
	}

	return valid;
}

$("#webcam-take-photo").click( function() {
	getFlash("flash").captureImage();
	if (!$(this).data("change")) {
		$(this).html("<i>CHANGE</i>").data("change", 1);
		$("#webcam-photo-submit").show();
	} else {
		initFlash();
		$("#captured-image-ba").val("");
		$(this).html("<i>TAKE PHOTO</i>").data("change", 0);
		$("#webcam-photo-submit").hide();
	}
	$('#error-msg').hide();
});

$("#webcam-photo-submit").click( function() {
	$("#captured-alert").addClass("hide");
	popup.close();
});

$("#upload-track-giid-later").click( function() {
	location = BASE_URL + "user/login";
});