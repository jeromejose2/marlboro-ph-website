(function(window, document, $){
	
	'use strict';

	var backstage_pass = window.backstage_pass = {};
	/* hey your the one */

	backstage_pass.past_events = {

		init: function(obj){
			this.properties = obj;
  			this.retrieveData();
  			this.filterRows();
 		},
		httpRequest: function(obj){
			$.ajax(obj);
		},
		getfilters: function(){
			return $('#filters').serialize()+'&track_group='+this.properties.track_group;
 		},
		retrieveData: function(){
			
			var data = this.getfilters();
			var that = this;

 			this.httpRequest({
			 	url: this.properties.url,
			 	data:data,
			 	dataType: 'json',
			 	beforeSend: function(){
 			 		that.properties.retrieving_data = true;
			 	},
			 	success: function(data){	
 			 		that.renderPage(data);
			 		that.properties.retrieving_data = false;
			 		that.properties.track_group++;
			 	}
			 });

		},
		renderPage: function(data){

			var tpl = jQuery(this.properties.handlebar_template).html();
			var template = Handlebars.compile(tpl);
			var rows = this.properties.rows;
			var index = (rows.length > 0) ? parseInt(rows.length) : 0;

			for(var i in data){
				rows[index] = data[i];
				index++;
 				
			}

			this.properties.rows = rows;
 			 
 			$(this.properties.template_content).html(template({albums:this.properties.rows}));
			this.parseScrool();

		},
		parseScrool: function(){

			var that = this;
			$(window).scroll(function(){
 
			 	if($(window).scrollTop() == $(document).height() - $(window).height() && that.isScrolledIntoView(that.properties.load_more)==true && that.properties.retrieving_data == false && that.properties.track_group < that.properties.total_group)
			    {
			    	
					that.retrieveData();

			    }

			});
		},
		isScrolledIntoView: function(elem){

			var docViewTop = $(window).scrollTop();
		    var docViewBottom = docViewTop + $(window).height();

		    var elemTop = $(elem).offset().top;
		    var elemBottom = elemTop + $(elem).height();

		    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));

		},
		filterRows: function(){

			var that = this;

			$('select').on('change',function(){			

  				 that.properties.rows = [];
				 that.properties.track_group = 0;
				 that.retrieveData();
 				 
 				 that.httpRequest({
 				 	url:that.properties.url,
 				 	data:$.extend(that.getfilters(),{total_rows:1}),
 				 	dataType: 'json',
 				 	success: function(data){
 				 		that.properties.total_group = data.total_group;
 				 	}
 				 });

			});

		},
		log: function(data){
			console.log(data);
		}

	};	


}(window, document, jQuery));