<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once '../../src/Google_Client.php';
require_once '../../src/contrib/Google_CalendarService.php';
session_start();

$client = new Google_Client();
$cal = new Google_CalendarService($client);

if(isset($_GET['authURL'])){
 
	$authUrl = $client->createAuthUrl();
  	print $authUrl;
  	exit();
}


if (isset($_GET['logout'])) {
  unset($_SESSION['token']);
}

if (isset($_GET['code'])) {
  $client->authenticate($_GET['code']);
  $_SESSION['token'] = $client->getAccessToken();
  header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
}

if (isset($_SESSION['token'])) {
  $client->setAccessToken($_SESSION['token']);
}

if ($client->getAccessToken()) {	 

	$_SESSION['token'] = $client->getAccessToken();
	
	print "<script>window.opener.markCalendar('".$client->getAccessToken()."'); window.close();</script>";
	 
} 